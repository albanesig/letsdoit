import React from 'react';
import './App.css';
import Header from "./components/header/Header";
import {AppLayout} from "./components/AppLayout";
import {AppProvider} from "./state/AppProvider";
import PageProvider from './pageProvider/PageProvider';


function App() {
  return(
      <div className="App">
        <AppLayout>
          <AppProvider>
            <Header/>
              <PageProvider/>
          </AppProvider>
        </AppLayout>
      </div>
  );
}

export default App;
