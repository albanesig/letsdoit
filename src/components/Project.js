import React from 'react';
import styled from 'styled-components';
import ShareIcon from "./icons/ShareIcon";
import LikeIcon from "./icons/LikeIcon";
import { AppContext } from '../state/AppProvider';
import { mainGreen } from '../style/styles'
import { ContentBox } from './generics/generics';

export const Title = styled.h2`
  color: ${mainGreen};
`;

export const LayoutIcons = styled.div`
  display: grid;
  grid-gap: 15px;
  grid-template-columns: 20px 20px 80px;
`;

export const LinkToTasks = styled.span`
  font-family: 'Pacifico', cursive;
  font-size: 16px;
  cursor: pointer;
`;

export default function ({project}) {
  return(
    <AppContext.Consumer>
      {({fetchProjectTasks}) => (
        <ContentBox>
          <Title>{project.title}</Title>
          <p>{project.description}</p>
          <LayoutIcons>
            <LikeIcon/>
            <ShareIcon/>
            <LinkToTasks
              onClick={() => fetchProjectTasks(project)}>
              Do some!
            </LinkToTasks>
          </LayoutIcons>
        </ContentBox>
      )}
    </AppContext.Consumer>
  )
}

