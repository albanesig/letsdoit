import React from 'react';
import styled from 'styled-components';

const ShareIcon = styled.i`
  color: #20c997;
  margin-top: 0.5em;
`;

export default function() {
  return(
    <ShareIcon className="far fa-paper-plane"/>
  )
}
