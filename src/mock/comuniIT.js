export const comuniIT = [
    {
      "1": 1210,
      "roma": "RACINES",
      "rm": "BZ",
      "1234": 404021070
    },
    {
      "1": 1845,
      "roma": "BISTAGNO",
      "rm": "AL",
      "1234": 401006017
    },
    {
      "1": 1846,
      "roma": "BISUSCHIO",
      "rm": "VA",
      "1234": 403012015
    },
    {
      "1": 1847,
      "roma": "BOCCIOLETO",
      "rm": "VC",
      "1234": 401002014
    },
    {
      "1": 1848,
      "roma": "BOCENAGO",
      "rm": "TN",
      "1234": 404022018
    },
    {
      "1": 1849,
      "roma": "BITRITTO",
      "rm": "BA",
      "1234": 416072012
    },
    {
      "1": 1850,
      "roma": "BITTI",
      "rm": "NU",
      "1234": 420091009
    },
    {
      "1": 1851,
      "roma": "BOFFALORA D'ADDA",
      "rm": "LO",
      "1234": 403098003
    },
    {
      "1": 1852,
      "roma": "BOFFALORA SOPRA TICINO",
      "rm": "MI",
      "1234": 403015026
    },
    {
      "1": 1853,
      "roma": "BOGLIASCO",
      "rm": "GE",
      "1234": 407010004
    },
    {
      "1": 1854,
      "roma": "BORGO LARES",
      "rm": "TN",
      "1234": 404022239
    },
    {
      "1": 1855,
      "roma": "BOGNANCO",
      "rm": "VB",
      "1234": 401103012
    },
    {
      "1": 1856,
      "roma": "BOGOGNO",
      "rm": "NO",
      "1234": 401003021
    },
    {
      "1": 1857,
      "roma": "BOBBIO",
      "rm": "PC",
      "1234": 408033005
    },
    {
      "1": 1858,
      "roma": "BOBBIO PELLICE",
      "rm": "TO",
      "1234": 401001026
    },
    {
      "1": 1859,
      "roma": "BOCA",
      "rm": "NO",
      "1234": 401003019
    },
    {
      "1": 1860,
      "roma": "BOCCHIGLIERO",
      "rm": "CS",
      "1234": 418078018
    },
    {
      "1": 1861,
      "roma": "BOLGARE",
      "rm": "BG",
      "1234": 403016028
    },
    {
      "1": 1862,
      "roma": "BODIO LOMNAGO",
      "rm": "VA",
      "1234": 403012016
    },
    {
      "1": 1863,
      "roma": "BOLLENGO",
      "rm": "TO",
      "1234": 401001027
    },
    {
      "1": 1864,
      "roma": "BOLOGNA",
      "rm": "BO",
      "1234": 408037006
    },
    {
      "1": 1865,
      "roma": "BOLOGNANO",
      "rm": "PE",
      "1234": 413068003
    },
    {
      "1": 1866,
      "roma": "BOLOGNETTA",
      "rm": "PA",
      "1234": 419082011
    },
    {
      "1": 1867,
      "roma": "BOLOGNOLA",
      "rm": "MC",
      "1234": 411043005
    },
    {
      "1": 1868,
      "roma": "BOLOTANA",
      "rm": "NU",
      "1234": 420091010
    },
    {
      "1": 1869,
      "roma": "BORGOMEZZAVALLE",
      "rm": "VB",
      "1234": 401103078
    },
    {
      "1": 1870,
      "roma": "BOLSENA",
      "rm": "VT",
      "1234": 412056008
    },
    {
      "1": 1871,
      "roma": "BOLTIERE",
      "rm": "BG",
      "1234": 403016029
    },
    {
      "1": 1872,
      "roma": "BOLZANO",
      "rm": "BZ",
      "1234": 404021008
    },
    {
      "1": 1873,
      "roma": "BOJANO",
      "rm": "CB",
      "1234": 414070003
    },
    {
      "1": 1874,
      "roma": "BOISSANO",
      "rm": "SV",
      "1234": 407009011
    },
    {
      "1": 1875,
      "roma": "BOLANO",
      "rm": "SP",
      "1234": 407011004
    },
    {
      "1": 1876,
      "roma": "BOMPORTO",
      "rm": "MO",
      "1234": 408036002
    },
    {
      "1": 1877,
      "roma": "BONARCADO",
      "rm": "OR",
      "1234": 420095015
    },
    {
      "1": 1878,
      "roma": "BONASSOLA",
      "rm": "SP",
      "1234": 407011005
    },
    {
      "1": 1879,
      "roma": "BONATE DI SOTTO",
      "rm": "BG",
      "1234": 403016031
    },
    {
      "1": 1880,
      "roma": "BOLLATE",
      "rm": "MI",
      "1234": 403015027
    },
    {
      "1": 1881,
      "roma": "BONDENO",
      "rm": "FE",
      "1234": 408038003
    },
    {
      "1": 1882,
      "roma": "BONDONE",
      "rm": "TN",
      "1234": 404022021
    },
    {
      "1": 1883,
      "roma": "BONEA",
      "rm": "BN",
      "1234": 415062009
    },
    {
      "1": 1884,
      "roma": "BONEFRO",
      "rm": "CB",
      "1234": 414070004
    },
    {
      "1": 1885,
      "roma": "BONEMERSE",
      "rm": "CR",
      "1234": 403019006
    },
    {
      "1": 1886,
      "roma": "BONIFATI",
      "rm": "CS",
      "1234": 418078019
    },
    {
      "1": 1887,
      "roma": "BOLZANO NOVARESE",
      "rm": "NO",
      "1234": 401003022
    },
    {
      "1": 1888,
      "roma": "BOLZANO VICENTINO",
      "rm": "VI",
      "1234": 405024013
    },
    {
      "1": 1889,
      "roma": "BOMARZO",
      "rm": "VT",
      "1234": 412056009
    },
    {
      "1": 1890,
      "roma": "BOMBA",
      "rm": "CH",
      "1234": 413069006
    },
    {
      "1": 1891,
      "roma": "BOMPENSIERE",
      "rm": "CL",
      "1234": 419085002
    },
    {
      "1": 1892,
      "roma": "BOMPIETRO",
      "rm": "PA",
      "1234": 419082012
    },
    {
      "1": 1893,
      "roma": "BORBONA",
      "rm": "RI",
      "1234": 412057006
    },
    {
      "1": 1894,
      "roma": "BORCA DI CADORE",
      "rm": "BL",
      "1234": 405025007
    },
    {
      "1": 1895,
      "roma": "BORDANO",
      "rm": "UD",
      "1234": 406030012
    },
    {
      "1": 1896,
      "roma": "BORDIGHERA",
      "rm": "IM",
      "1234": 407008008
    },
    {
      "1": 1897,
      "roma": "BONATE SOPRA",
      "rm": "BG",
      "1234": 403016030
    },
    {
      "1": 1898,
      "roma": "BONAVIGO",
      "rm": "VR",
      "1234": 405023009
    },
    {
      "1": 1899,
      "roma": "BORE",
      "rm": "PR",
      "1234": 408034005
    },
    {
      "1": 1900,
      "roma": "BORETTO",
      "rm": "RE",
      "1234": 408035005
    },
    {
      "1": 1901,
      "roma": "BORGARELLO",
      "rm": "PV",
      "1234": 403018015
    },
    {
      "1": 1902,
      "roma": "BORGARO TORINESE",
      "rm": "TO",
      "1234": 401001028
    },
    {
      "1": 1903,
      "roma": "BORGETTO",
      "rm": "PA",
      "1234": 419082013
    },
    {
      "1": 1904,
      "roma": "BORGHETTO D'ARROSCIA",
      "rm": "IM",
      "1234": 407008009
    },
    {
      "1": 1905,
      "roma": "BORGHETTO DI BORBERA",
      "rm": "AL",
      "1234": 401006018
    },
    {
      "1": 1906,
      "roma": "BORGHETTO DI VARA",
      "rm": "SP",
      "1234": 407011006
    },
    {
      "1": 1907,
      "roma": "BORGHETTO LODIGIANO",
      "rm": "LO",
      "1234": 403098004
    },
    {
      "1": 1908,
      "roma": "BONITO",
      "rm": "AV",
      "1234": 415064012
    },
    {
      "1": 1909,
      "roma": "BONNANARO",
      "rm": "SS",
      "1234": 420090011
    },
    {
      "1": 1910,
      "roma": "BONO",
      "rm": "SS",
      "1234": 420090012
    },
    {
      "1": 1911,
      "roma": "BONORVA",
      "rm": "SS",
      "1234": 420090013
    },
    {
      "1": 1912,
      "roma": "BONVICINO",
      "rm": "CN",
      "1234": 401004023
    },
    {
      "1": 1913,
      "roma": "BORGO A MOZZANO",
      "rm": "LU",
      "1234": 409046004
    },
    {
      "1": 1914,
      "roma": "BORGO D'ALE",
      "rm": "VC",
      "1234": 401002015
    },
    {
      "1": 1915,
      "roma": "BORGO DI TERZO",
      "rm": "BG",
      "1234": 403016032
    },
    {
      "1": 1916,
      "roma": "BORGO PACE",
      "rm": "PS",
      "1234": 411041006
    },
    {
      "1": 1917,
      "roma": "BORDOLANO",
      "rm": "CR",
      "1234": 403019007
    },
    {
      "1": 1918,
      "roma": "BORGO SAN DALMAZZO",
      "rm": "CN",
      "1234": 401004025
    },
    {
      "1": 1919,
      "roma": "BORGO SAN GIACOMO",
      "rm": "BS",
      "1234": 403017020
    },
    {
      "1": 1920,
      "roma": "BORGO SAN GIOVANNI",
      "rm": "LO",
      "1234": 403098005
    },
    {
      "1": 1921,
      "roma": "BORGO SAN LORENZO",
      "rm": "FI",
      "1234": 409048004
    },
    {
      "1": 1922,
      "roma": "BORGO SAN MARTINO",
      "rm": "AL",
      "1234": 401006020
    },
    {
      "1": 1923,
      "roma": "BORGO SAN SIRO",
      "rm": "PV",
      "1234": 403018018
    },
    {
      "1": 1924,
      "roma": "BORGO TICINO",
      "rm": "NO",
      "1234": 401003025
    },
    {
      "1": 1925,
      "roma": "BORGO TOSSIGNANO",
      "rm": "BO",
      "1234": 408037007
    },
    {
      "1": 1926,
      "roma": "BORGO VAL DI TARO",
      "rm": "PR",
      "1234": 408034006
    },
    {
      "1": 1927,
      "roma": "BORGO VALSUGANA",
      "rm": "TN",
      "1234": 404022022
    },
    {
      "1": 1928,
      "roma": "BORGHETTO SANTO SPIRITO",
      "rm": "SV",
      "1234": 407009012
    },
    {
      "1": 1929,
      "roma": "BORGHI",
      "rm": "FO",
      "1234": 408040004
    },
    {
      "1": 1930,
      "roma": "BORGIA",
      "rm": "CZ",
      "1234": 418079011
    },
    {
      "1": 1931,
      "roma": "BORGIALLO",
      "rm": "TO",
      "1234": 401001029
    },
    {
      "1": 1932,
      "roma": "BORGIO-VEREZZI",
      "rm": "SV",
      "1234": 407009013
    },
    {
      "1": 1933,
      "roma": "BORGOLAVEZZARO",
      "rm": "NO",
      "1234": 401003023
    },
    {
      "1": 1934,
      "roma": "BORGOMALE",
      "rm": "CN",
      "1234": 401004024
    },
    {
      "1": 1935,
      "roma": "BORGOMANERO",
      "rm": "NO",
      "1234": 401003024
    },
    {
      "1": 1936,
      "roma": "BORGOMARO",
      "rm": "IM",
      "1234": 407008010
    },
    {
      "1": 1937,
      "roma": "BORGOMASINO",
      "rm": "TO",
      "1234": 401001031
    },
    {
      "1": 1938,
      "roma": "BORGO PRIOLO",
      "rm": "PV",
      "1234": 403018016
    },
    {
      "1": 1939,
      "roma": "BORGONOVO VAL TIDONE",
      "rm": "PC",
      "1234": 408033006
    },
    {
      "1": 1940,
      "roma": "BORGORATTO ALESSANDRINO",
      "rm": "AL",
      "1234": 401006019
    },
    {
      "1": 1941,
      "roma": "BORGORATTO MORMOROLO",
      "rm": "PV",
      "1234": 403018017
    },
    {
      "1": 1942,
      "roma": "BORGORICCO",
      "rm": "PD",
      "1234": 405028013
    },
    {
      "1": 1943,
      "roma": "BORGOROSE",
      "rm": "RI",
      "1234": 412057007
    },
    {
      "1": 1944,
      "roma": "BORGOSATOLLO",
      "rm": "BS",
      "1234": 403017021
    },
    {
      "1": 1945,
      "roma": "BORGOSESIA",
      "rm": "VC",
      "1234": 401002016
    },
    {
      "1": 1946,
      "roma": "BORMIDA",
      "rm": "SV",
      "1234": 407009014
    },
    {
      "1": 1947,
      "roma": "BORMIO",
      "rm": "SO",
      "1234": 403014009
    },
    {
      "1": 1948,
      "roma": "BORGO CHIESE",
      "rm": "TN",
      "1234": 404022238
    },
    {
      "1": 1949,
      "roma": "BORGO VELINO",
      "rm": "RI",
      "1234": 412057008
    },
    {
      "1": 1950,
      "roma": "BORGO VERCELLI",
      "rm": "VC",
      "1234": 401002017
    },
    {
      "1": 1951,
      "roma": "BORGOFRANCO D'IVREA",
      "rm": "TO",
      "1234": 401001030
    },
    {
      "1": 1952,
      "roma": "BORONEDDU",
      "rm": "OR",
      "1234": 420095016
    },
    {
      "1": 1953,
      "roma": "BORORE",
      "rm": "NU",
      "1234": 420091011
    },
    {
      "1": 1954,
      "roma": "BORRELLO",
      "rm": "CH",
      "1234": 413069007
    },
    {
      "1": 1955,
      "roma": "BORRIANA",
      "rm": "BI",
      "1234": 401096006
    },
    {
      "1": 1956,
      "roma": "BORGONE DI SUSA",
      "rm": "TO",
      "1234": 401001032
    },
    {
      "1": 1957,
      "roma": "BORTIGALI",
      "rm": "NU",
      "1234": 420091012
    },
    {
      "1": 1958,
      "roma": "BORTIGIADAS",
      "rm": "SS",
      "1234": 420090014
    },
    {
      "1": 1959,
      "roma": "BORUTTA",
      "rm": "SS",
      "1234": 420090015
    },
    {
      "1": 1960,
      "roma": "BORZONASCA",
      "rm": "GE",
      "1234": 407010005
    },
    {
      "1": 1961,
      "roma": "BOSA",
      "rm": "NU",
      "1234": 420091013
    },
    {
      "1": 1962,
      "roma": "BOSARO",
      "rm": "RO",
      "1234": 405029007
    },
    {
      "1": 1963,
      "roma": "BOSCHI SANT'ANNA",
      "rm": "VR",
      "1234": 405023010
    },
    {
      "1": 1964,
      "roma": "BOSCO CHIESANUOVA",
      "rm": "VR",
      "1234": 405023011
    },
    {
      "1": 1965,
      "roma": "BORNASCO",
      "rm": "PV",
      "1234": 403018019
    },
    {
      "1": 1966,
      "roma": "BORGO VIRGILIO",
      "rm": "MN",
      "1234": 403020071
    },
    {
      "1": 1967,
      "roma": "ALTA VAL TIDONE",
      "rm": "PC",
      "1234": 408033049
    },
    {
      "1": 1968,
      "roma": "BORNO",
      "rm": "BS",
      "1234": 403017022
    },
    {
      "1": 1969,
      "roma": "BOSIA",
      "rm": "CN",
      "1234": 401004026
    },
    {
      "1": 1970,
      "roma": "BOSIO",
      "rm": "AL",
      "1234": 401006022
    },
    {
      "1": 1971,
      "roma": "BOSISIO PARINI",
      "rm": "LC",
      "1234": 403097009
    },
    {
      "1": 1972,
      "roma": "BOSNASCO",
      "rm": "PV",
      "1234": 403018020
    },
    {
      "1": 1973,
      "roma": "BOSSICO",
      "rm": "BG",
      "1234": 403016033
    },
    {
      "1": 1974,
      "roma": "BORSO DEL GRAPPA",
      "rm": "TV",
      "1234": 405026004
    },
    {
      "1": 1975,
      "roma": "BOTRUGNO",
      "rm": "LE",
      "1234": 416075009
    },
    {
      "1": 1976,
      "roma": "BOTTANUCO",
      "rm": "BG",
      "1234": 403016034
    },
    {
      "1": 1977,
      "roma": "BOTTICINO",
      "rm": "BS",
      "1234": 403017023
    },
    {
      "1": 1978,
      "roma": "BOTTIDDA",
      "rm": "SS",
      "1234": 420090016
    },
    {
      "1": 1979,
      "roma": "BOSCO MARENGO",
      "rm": "AL",
      "1234": 401006021
    },
    {
      "1": 1980,
      "roma": "BOSCONERO",
      "rm": "TO",
      "1234": 401001033
    },
    {
      "1": 1981,
      "roma": "BOSCOREALE",
      "rm": "NA",
      "1234": 415063008
    },
    {
      "1": 1982,
      "roma": "BOSCOTRECASE",
      "rm": "NA",
      "1234": 415063009
    },
    {
      "1": 1983,
      "roma": "BOVEZZO",
      "rm": "BS",
      "1234": 403017025
    },
    {
      "1": 1984,
      "roma": "BOVILLE ERNICA",
      "rm": "FR",
      "1234": 412060014
    },
    {
      "1": 1985,
      "roma": "BOVINO",
      "rm": "FG",
      "1234": 416071007
    },
    {
      "1": 1986,
      "roma": "BOVOLENTA",
      "rm": "PD",
      "1234": 405028014
    },
    {
      "1": 1987,
      "roma": "BOSSOLASCO",
      "rm": "CN",
      "1234": 401004027
    },
    {
      "1": 1988,
      "roma": "BOTRICELLO",
      "rm": "CZ",
      "1234": 418079012
    },
    {
      "1": 1989,
      "roma": "BOZZOLE",
      "rm": "AL",
      "1234": 401006023
    },
    {
      "1": 1990,
      "roma": "BOZZOLO",
      "rm": "MN",
      "1234": 403020007
    },
    {
      "1": 1991,
      "roma": "BRA",
      "rm": "CN",
      "1234": 401004029
    },
    {
      "1": 1992,
      "roma": "BRACCA",
      "rm": "BG",
      "1234": 403016035
    },
    {
      "1": 1993,
      "roma": "BRACCIANO",
      "rm": "RM",
      "1234": 412058013
    },
    {
      "1": 1994,
      "roma": "BRACIGLIANO",
      "rm": "SA",
      "1234": 415065016
    },
    {
      "1": 1995,
      "roma": "BRAIES",
      "rm": "BZ",
      "1234": 404021009
    },
    {
      "1": 1996,
      "roma": "BRALLO DI PREGOLA",
      "rm": "PV",
      "1234": 403018021
    },
    {
      "1": 1997,
      "roma": "BRANCALEONE",
      "rm": "RC",
      "1234": 418080014
    },
    {
      "1": 1998,
      "roma": "BOVA",
      "rm": "RC",
      "1234": 418080011
    },
    {
      "1": 1999,
      "roma": "BOVA MARINA",
      "rm": "RC",
      "1234": 418080013
    },
    {
      "1": 2000,
      "roma": "BOVALINO",
      "rm": "RC",
      "1234": 418080012
    },
    {
      "1": 2001,
      "roma": "BOVEGNO",
      "rm": "BS",
      "1234": 403017024
    },
    {
      "1": 2002,
      "roma": "BOVES",
      "rm": "CN",
      "1234": 401004028
    },
    {
      "1": 2003,
      "roma": "BRAONE",
      "rm": "BS",
      "1234": 403017027
    },
    {
      "1": 2004,
      "roma": "BOVISIO-MASCIAGO",
      "rm": "MB",
      "1234": 403108010
    },
    {
      "1": 2005,
      "roma": "BREBBIA",
      "rm": "VA",
      "1234": 403012017
    },
    {
      "1": 2006,
      "roma": "BREDA DI PIAVE",
      "rm": "TV",
      "1234": 405026005
    },
    {
      "1": 2007,
      "roma": "BOVOLONE",
      "rm": "VR",
      "1234": 405023012
    },
    {
      "1": 2008,
      "roma": "BREGANZE",
      "rm": "VI",
      "1234": 405024014
    },
    {
      "1": 2009,
      "roma": "BREGNANO",
      "rm": "CO",
      "1234": 403013028
    },
    {
      "1": 2010,
      "roma": "BREMBATE",
      "rm": "BG",
      "1234": 403016037
    },
    {
      "1": 2011,
      "roma": "BREMBATE DI SOPRA",
      "rm": "BG",
      "1234": 403016038
    },
    {
      "1": 2012,
      "roma": "BREMBIO",
      "rm": "LO",
      "1234": 403098006
    },
    {
      "1": 2013,
      "roma": "BRANDICO",
      "rm": "BS",
      "1234": 403017026
    },
    {
      "1": 2014,
      "roma": "BRANDIZZO",
      "rm": "TO",
      "1234": 401001034
    },
    {
      "1": 2015,
      "roma": "BRANZI",
      "rm": "BG",
      "1234": 403016036
    },
    {
      "1": 2016,
      "roma": "BRENO",
      "rm": "BS",
      "1234": 403017028
    },
    {
      "1": 2017,
      "roma": "BRENTA",
      "rm": "VA",
      "1234": 403012019
    },
    {
      "1": 2018,
      "roma": "BRENTINO BELLUNO",
      "rm": "VR",
      "1234": 405023013
    },
    {
      "1": 2019,
      "roma": "BRENTONICO",
      "rm": "TN",
      "1234": 404022025
    },
    {
      "1": 2020,
      "roma": "BRESCELLO",
      "rm": "RE",
      "1234": 408035006
    },
    {
      "1": 2021,
      "roma": "BREGANO",
      "rm": "VA",
      "1234": 403012018
    },
    {
      "1": 2022,
      "roma": "BRESIMO",
      "rm": "TN",
      "1234": 404022026
    },
    {
      "1": 2023,
      "roma": "BRESSANA BOTTARONE",
      "rm": "PV",
      "1234": 403018023
    },
    {
      "1": 2024,
      "roma": "BRESSANONE",
      "rm": "BZ",
      "1234": 404021011
    },
    {
      "1": 2025,
      "roma": "BRESSANVIDO",
      "rm": "VI",
      "1234": 405024016
    },
    {
      "1": 2026,
      "roma": "BRESSO",
      "rm": "MI",
      "1234": 403015032
    },
    {
      "1": 2027,
      "roma": "BREZ",
      "rm": "TN",
      "1234": 404022027
    },
    {
      "1": 2028,
      "roma": "BREZZO DI BEDERO",
      "rm": "VA",
      "1234": 403012020
    },
    {
      "1": 2029,
      "roma": "BREME",
      "rm": "PV",
      "1234": 403018022
    },
    {
      "1": 2030,
      "roma": "BRENDOLA",
      "rm": "VI",
      "1234": 405024015
    },
    {
      "1": 2031,
      "roma": "BRENNA",
      "rm": "CO",
      "1234": 403013029
    },
    {
      "1": 2032,
      "roma": "BRENNERO",
      "rm": "BZ",
      "1234": 404021010
    },
    {
      "1": 2033,
      "roma": "BRIENZA",
      "rm": "PZ",
      "1234": 417076013
    },
    {
      "1": 2034,
      "roma": "BRIGA ALTA",
      "rm": "CN",
      "1234": 401004031
    },
    {
      "1": 2035,
      "roma": "BRIGA NOVARESE",
      "rm": "NO",
      "1234": 401003026
    },
    {
      "1": 2036,
      "roma": "BRENZONE SUL GARDA",
      "rm": "VR",
      "1234": 405023914
    },
    {
      "1": 2037,
      "roma": "BRIGNANO GERA D'ADDA",
      "rm": "BG",
      "1234": 403016040
    },
    {
      "1": 2038,
      "roma": "BRIGNANO-FRASCATA",
      "rm": "AL",
      "1234": 401006024
    },
    {
      "1": 1211,
      "roma": "RADDA IN CHIANTI",
      "rm": "SI",
      "1234": 409052023
    },
    {
      "1": 1638,
      "roma": "VENTOTENE",
      "rm": "LT",
      "1234": 412059033
    },
    {
      "1": 1639,
      "roma": "VENZONE",
      "rm": "UD",
      "1234": 406030131
    },
    {
      "1": 1640,
      "roma": "VERANO",
      "rm": "BZ",
      "1234": 404021112
    },
    {
      "1": 1641,
      "roma": "VERBANIA",
      "rm": "VB",
      "1234": 401103072
    },
    {
      "1": 1642,
      "roma": "VENAUS",
      "rm": "TO",
      "1234": 401001291
    },
    {
      "1": 1643,
      "roma": "VENDONE",
      "rm": "SV",
      "1234": 407009066
    },
    {
      "1": 1644,
      "roma": "VENDROGNO",
      "rm": "LC",
      "1234": 403097085
    },
    {
      "1": 1645,
      "roma": "VERCELLI",
      "rm": "VC",
      "1234": 401002158
    },
    {
      "1": 1646,
      "roma": "VERCURAGO",
      "rm": "LC",
      "1234": 403097086
    },
    {
      "1": 1647,
      "roma": "VERANO BRIANZA",
      "rm": "MB",
      "1234": 403108048
    },
    {
      "1": 1648,
      "roma": "VERDELLINO",
      "rm": "BG",
      "1234": 403016232
    },
    {
      "1": 1649,
      "roma": "VERDELLO",
      "rm": "BG",
      "1234": 403016233
    },
    {
      "1": 1650,
      "roma": "VEROLANUOVA",
      "rm": "BS",
      "1234": 403017195
    },
    {
      "1": 1651,
      "roma": "VEROLAVECCHIA",
      "rm": "BS",
      "1234": 403017196
    },
    {
      "1": 1652,
      "roma": "VERDUNO",
      "rm": "CN",
      "1234": 401004238
    },
    {
      "1": 1653,
      "roma": "VERGATO",
      "rm": "BO",
      "1234": 408037059
    },
    {
      "1": 1654,
      "roma": "VERGHERETO",
      "rm": "FO",
      "1234": 408040050
    },
    {
      "1": 1655,
      "roma": "VERGIATE",
      "rm": "VA",
      "1234": 403012138
    },
    {
      "1": 1656,
      "roma": "VERBICARO",
      "rm": "CS",
      "1234": 418078153
    },
    {
      "1": 1657,
      "roma": "VERCANA",
      "rm": "CO",
      "1234": 403013239
    },
    {
      "1": 1658,
      "roma": "VERCEIA",
      "rm": "SO",
      "1234": 403014075
    },
    {
      "1": 1659,
      "roma": "VERNANTE",
      "rm": "CN",
      "1234": 401004239
    },
    {
      "1": 1660,
      "roma": "VERNASCA",
      "rm": "PC",
      "1234": 408033044
    },
    {
      "1": 1661,
      "roma": "VERNATE",
      "rm": "MI",
      "1234": 403015236
    },
    {
      "1": 1662,
      "roma": "VERNAZZA",
      "rm": "SP",
      "1234": 407011030
    },
    {
      "1": 1663,
      "roma": "VERNIO",
      "rm": "PO",
      "1234": 409100007
    },
    {
      "1": 1664,
      "roma": "VERDERIO",
      "rm": "LC",
      "1234": 403097091
    },
    {
      "1": 1665,
      "roma": "VERNOLE",
      "rm": "LE",
      "1234": 416075093
    },
    {
      "1": 1666,
      "roma": "VAL DI ZOLDO",
      "rm": "BL",
      "1234": 405025073
    },
    {
      "1": 1667,
      "roma": "VEROLENGO",
      "rm": "TO",
      "1234": 401001293
    },
    {
      "1": 1668,
      "roma": "VEROLI",
      "rm": "FR",
      "1234": 412060085
    },
    {
      "1": 1669,
      "roma": "VERONA",
      "rm": "VR",
      "1234": 405023091
    },
    {
      "1": 1670,
      "roma": "VERONELLA",
      "rm": "VR",
      "1234": 405023092
    },
    {
      "1": 1671,
      "roma": "VERRAYES",
      "rm": "AO",
      "1234": 402007072
    },
    {
      "1": 1672,
      "roma": "VERRES",
      "rm": "AO",
      "1234": 402007073
    },
    {
      "1": 1673,
      "roma": "VERRETTO",
      "rm": "PV",
      "1234": 403018174
    },
    {
      "1": 1674,
      "roma": "VERRONE",
      "rm": "BI",
      "1234": 401096076
    },
    {
      "1": 1675,
      "roma": "VERRUA PO",
      "rm": "PV",
      "1234": 403018175
    },
    {
      "1": 1676,
      "roma": "VERRUA SAVOIA",
      "rm": "TO",
      "1234": 401001294
    },
    {
      "1": 1677,
      "roma": "VERMIGLIO",
      "rm": "TN",
      "1234": 404022213
    },
    {
      "1": 1678,
      "roma": "VERTEMATE CON MINOPRIO",
      "rm": "CO",
      "1234": 403013242
    },
    {
      "1": 1679,
      "roma": "VERTOVA",
      "rm": "BG",
      "1234": 403016234
    },
    {
      "1": 1680,
      "roma": "VERUCCHIO",
      "rm": "RN",
      "1234": 408099020
    },
    {
      "1": 1681,
      "roma": "VEZZANO SUL CROSTOLO",
      "rm": "RE",
      "1234": 408035043
    },
    {
      "1": 1682,
      "roma": "VERVIO",
      "rm": "SO",
      "1234": 403014076
    },
    {
      "1": 1683,
      "roma": "VERZEGNIS",
      "rm": "UD",
      "1234": 406030132
    },
    {
      "1": 1684,
      "roma": "VERZINO",
      "rm": "KR",
      "1234": 418101027
    },
    {
      "1": 1685,
      "roma": "VERZUOLO",
      "rm": "CN",
      "1234": 401004240
    },
    {
      "1": 1686,
      "roma": "VESCOVANA",
      "rm": "PD",
      "1234": 405028097
    },
    {
      "1": 1687,
      "roma": "VESCOVATO",
      "rm": "CR",
      "1234": 403019113
    },
    {
      "1": 1688,
      "roma": "VESIME",
      "rm": "AT",
      "1234": 401005113
    },
    {
      "1": 1689,
      "roma": "VESPOLATE",
      "rm": "NO",
      "1234": 401003158
    },
    {
      "1": 1690,
      "roma": "VESSALICO",
      "rm": "IM",
      "1234": 407008066
    },
    {
      "1": 1691,
      "roma": "VESTENANOVA",
      "rm": "VR",
      "1234": 405023093
    },
    {
      "1": 1692,
      "roma": "VETRALLA",
      "rm": "VT",
      "1234": 412056057
    },
    {
      "1": 1693,
      "roma": "VETTO",
      "rm": "RE",
      "1234": 408035042
    },
    {
      "1": 1694,
      "roma": "VEZZA D'ALBA",
      "rm": "CN",
      "1234": 401004241
    },
    {
      "1": 1695,
      "roma": "VEZZA D'OGLIO",
      "rm": "BS",
      "1234": 403017198
    },
    {
      "1": 1696,
      "roma": "VEZZANO LIGURE",
      "rm": "SP",
      "1234": 407011031
    },
    {
      "1": 1697,
      "roma": "VICO EQUENSE",
      "rm": "NA",
      "1234": 415063086
    },
    {
      "1": 1698,
      "roma": "VEZZI PORTIO",
      "rm": "SV",
      "1234": 407009067
    },
    {
      "1": 1699,
      "roma": "VIADANA",
      "rm": "MN",
      "1234": 403020066
    },
    {
      "1": 1700,
      "roma": "VIADANICA",
      "rm": "BG",
      "1234": 403016235
    },
    {
      "1": 1701,
      "roma": "VIAGRANDE",
      "rm": "CT",
      "1234": 419087053
    },
    {
      "1": 1702,
      "roma": "VIALE D'ASTI",
      "rm": "AT",
      "1234": 401005114
    },
    {
      "1": 1703,
      "roma": "VIALFRE'",
      "rm": "TO",
      "1234": 401001296
    },
    {
      "1": 1704,
      "roma": "VIANO",
      "rm": "RE",
      "1234": 408035044
    },
    {
      "1": 1705,
      "roma": "VIAREGGIO",
      "rm": "LU",
      "1234": 409046033
    },
    {
      "1": 1706,
      "roma": "VIARIGI",
      "rm": "AT",
      "1234": 401005115
    },
    {
      "1": 1707,
      "roma": "VIBO VALENTIA",
      "rm": "VV",
      "1234": 418102047
    },
    {
      "1": 1708,
      "roma": "VESTIGNE'",
      "rm": "TO",
      "1234": 401001295
    },
    {
      "1": 1709,
      "roma": "VESTONE",
      "rm": "BS",
      "1234": 403017197
    },
    {
      "1": 1710,
      "roma": "VIBONATI",
      "rm": "SA",
      "1234": 415065156
    },
    {
      "1": 1711,
      "roma": "VICALVI",
      "rm": "FR",
      "1234": 412060086
    },
    {
      "1": 1712,
      "roma": "VICARI",
      "rm": "PA",
      "1234": 419082078
    },
    {
      "1": 1713,
      "roma": "VICCHIO",
      "rm": "FI",
      "1234": 409048049
    },
    {
      "1": 1714,
      "roma": "VICENZA",
      "rm": "VI",
      "1234": 405024116
    },
    {
      "1": 1715,
      "roma": "VICO DEL GARGANO",
      "rm": "FG",
      "1234": 416071059
    },
    {
      "1": 1716,
      "roma": "VICO NEL LAZIO",
      "rm": "FR",
      "1234": 412060087
    },
    {
      "1": 1717,
      "roma": "VICOFORTE",
      "rm": "CN",
      "1234": 401004242
    },
    {
      "1": 1718,
      "roma": "VICOLI",
      "rm": "PE",
      "1234": 413068045
    },
    {
      "1": 1719,
      "roma": "VICOLUNGO",
      "rm": "NO",
      "1234": 401003159
    },
    {
      "1": 1720,
      "roma": "VICOPISANO",
      "rm": "PI",
      "1234": 409050038
    },
    {
      "1": 1721,
      "roma": "VICOVARO",
      "rm": "RM",
      "1234": 412058112
    },
    {
      "1": 1722,
      "roma": "VIDDALBA",
      "rm": "SS",
      "1234": 420090082
    },
    {
      "1": 1723,
      "roma": "VIDIGULFO",
      "rm": "PV",
      "1234": 403018176
    },
    {
      "1": 1724,
      "roma": "VIDOR",
      "rm": "TV",
      "1234": 405026090
    },
    {
      "1": 1725,
      "roma": "VIDRACCO",
      "rm": "TO",
      "1234": 401001298
    },
    {
      "1": 1726,
      "roma": "VIETRI DI POTENZA",
      "rm": "PZ",
      "1234": 417076096
    },
    {
      "1": 1727,
      "roma": "VIETRI SUL MARE",
      "rm": "SA",
      "1234": 415065157
    },
    {
      "1": 1728,
      "roma": "VIGANO SAN MARTINO",
      "rm": "BG",
      "1234": 403016236
    },
    {
      "1": 1729,
      "roma": "VIGANO'",
      "rm": "LC",
      "1234": 403097090
    },
    {
      "1": 1730,
      "roma": "VIGARANO MAINARDA",
      "rm": "FE",
      "1234": 408038022
    },
    {
      "1": 1731,
      "roma": "VIGASIO",
      "rm": "VR",
      "1234": 405023094
    },
    {
      "1": 1732,
      "roma": "VIGEVANO",
      "rm": "PV",
      "1234": 403018177
    },
    {
      "1": 1733,
      "roma": "VIGGIANELLO",
      "rm": "PZ",
      "1234": 417076097
    },
    {
      "1": 1734,
      "roma": "VIGGIANO",
      "rm": "PZ",
      "1234": 417076098
    },
    {
      "1": 1735,
      "roma": "VIGGIU'",
      "rm": "VA",
      "1234": 403012139
    },
    {
      "1": 1736,
      "roma": "VIGHIZZOLO D'ESTE",
      "rm": "PD",
      "1234": 405028098
    },
    {
      "1": 1737,
      "roma": "VIESTE",
      "rm": "FG",
      "1234": 416071060
    },
    {
      "1": 1738,
      "roma": "VIGLIANO D'ASTI",
      "rm": "AT",
      "1234": 401005116
    },
    {
      "1": 1739,
      "roma": "VIGNALE MONFERRATO",
      "rm": "AL",
      "1234": 401006179
    },
    {
      "1": 1740,
      "roma": "VIGNANELLO",
      "rm": "VT",
      "1234": 412056058
    },
    {
      "1": 1741,
      "roma": "VIGNATE",
      "rm": "MI",
      "1234": 403015237
    },
    {
      "1": 1742,
      "roma": "VIGNOLA",
      "rm": "MO",
      "1234": 408036046
    },
    {
      "1": 1743,
      "roma": "VIGNOLA-FALESINA",
      "rm": "TN",
      "1234": 404022216
    },
    {
      "1": 1744,
      "roma": "VIGNOLE BORBERA",
      "rm": "AL",
      "1234": 401006180
    },
    {
      "1": 1745,
      "roma": "VIGNOLO",
      "rm": "CN",
      "1234": 401004243
    },
    {
      "1": 1746,
      "roma": "VILLA CARCINA",
      "rm": "BS",
      "1234": 403017199
    },
    {
      "1": 1747,
      "roma": "VIGNONE",
      "rm": "VB",
      "1234": 401103074
    },
    {
      "1": 1748,
      "roma": "VIGO DI CADORE",
      "rm": "BL",
      "1234": 405025065
    },
    {
      "1": 1749,
      "roma": "VIGODARZERE",
      "rm": "PD",
      "1234": 405028099
    },
    {
      "1": 1750,
      "roma": "VIGOLO",
      "rm": "BG",
      "1234": 403016237
    },
    {
      "1": 1751,
      "roma": "VIGOLZONE",
      "rm": "PC",
      "1234": 408033045
    },
    {
      "1": 1752,
      "roma": "VIGONE",
      "rm": "TO",
      "1234": 401001299
    },
    {
      "1": 1753,
      "roma": "VIGONOVO",
      "rm": "VE",
      "1234": 405027043
    },
    {
      "1": 1754,
      "roma": "VIGLIANO BIELLESE",
      "rm": "BI",
      "1234": 401096077
    },
    {
      "1": 1755,
      "roma": "VIGUZZOLO",
      "rm": "AL",
      "1234": 401006181
    },
    {
      "1": 1756,
      "roma": "VILLA BARTOLOMEA",
      "rm": "VR",
      "1234": 405023095
    },
    {
      "1": 1757,
      "roma": "VILLA BASILICA",
      "rm": "LU",
      "1234": 409046034
    },
    {
      "1": 1758,
      "roma": "VILLA BISCOSSI",
      "rm": "PV",
      "1234": 403018178
    },
    {
      "1": 1759,
      "roma": "VILLA CASTELLI",
      "rm": "BR",
      "1234": 416074020
    },
    {
      "1": 1760,
      "roma": "VILLA CELIERA",
      "rm": "PE",
      "1234": 413068046
    },
    {
      "1": 1761,
      "roma": "VILLA COLLEMANDINA",
      "rm": "LU",
      "1234": 409046035
    },
    {
      "1": 1762,
      "roma": "VILLA CORTESE",
      "rm": "MI",
      "1234": 403015248
    },
    {
      "1": 1763,
      "roma": "VILLA D'ADDA",
      "rm": "BG",
      "1234": 403016238
    },
    {
      "1": 1764,
      "roma": "VILLA D'ALME'",
      "rm": "BG",
      "1234": 403016239
    },
    {
      "1": 1765,
      "roma": "VILLA D'OGNA",
      "rm": "BG",
      "1234": 403016241
    },
    {
      "1": 1766,
      "roma": "VIGONZA",
      "rm": "PD",
      "1234": 405028100
    },
    {
      "1": 1767,
      "roma": "VILLA DI BRIANO",
      "rm": "CE",
      "1234": 415061098
    },
    {
      "1": 1768,
      "roma": "VILLA DI CHIAVENNA",
      "rm": "SO",
      "1234": 403014077
    },
    {
      "1": 1769,
      "roma": "VILLA DI SERIO",
      "rm": "BG",
      "1234": 403016240
    },
    {
      "1": 1770,
      "roma": "VILLA DI TIRANO",
      "rm": "SO",
      "1234": 403014078
    },
    {
      "1": 1771,
      "roma": "VILLA ESTENSE",
      "rm": "PD",
      "1234": 405028102
    },
    {
      "1": 1772,
      "roma": "VILLA FARALDI",
      "rm": "IM",
      "1234": 407008067
    },
    {
      "1": 1773,
      "roma": "VILLA GUARDIA",
      "rm": "CO",
      "1234": 403013245
    },
    {
      "1": 1774,
      "roma": "VILLA LAGARINA",
      "rm": "TN",
      "1234": 404022222
    },
    {
      "1": 1775,
      "roma": "VILLA LATINA",
      "rm": "FR",
      "1234": 412060088
    },
    {
      "1": 1776,
      "roma": "VILLA LITERNO",
      "rm": "CE",
      "1234": 415061099
    },
    {
      "1": 1777,
      "roma": "VILLA MINOZZO",
      "rm": "RE",
      "1234": 408035045
    },
    {
      "1": 1778,
      "roma": "VILLA SAN GIOVANNI",
      "rm": "RC",
      "1234": 418080096
    },
    {
      "1": 1779,
      "roma": "VILLA SAN GIOVANNI IN TUSCIA",
      "rm": "VT",
      "1234": 412056046
    },
    {
      "1": 1780,
      "roma": "VILLA DEL BOSCO",
      "rm": "BI",
      "1234": 401096078
    },
    {
      "1": 1781,
      "roma": "VILLA DEL CONTE",
      "rm": "PD",
      "1234": 405028101
    },
    {
      "1": 1782,
      "roma": "VILLA SAN SECONDO",
      "rm": "AT",
      "1234": 401005119
    },
    {
      "1": 1783,
      "roma": "VILLA SANT'ANGELO",
      "rm": "AQ",
      "1234": 413066105
    },
    {
      "1": 1784,
      "roma": "VILLA SANT'ANTONIO",
      "rm": "OR",
      "1234": 420095048
    },
    {
      "1": 1785,
      "roma": "VILLA SANTA LUCIA",
      "rm": "FR",
      "1234": 412060089
    },
    {
      "1": 1786,
      "roma": "VILLA SANTA LUCIA DEGLI ABRUZZI",
      "rm": "AQ",
      "1234": 413066104
    },
    {
      "1": 1787,
      "roma": "VILLA SANTA MARIA",
      "rm": "CH",
      "1234": 413069102
    },
    {
      "1": 1788,
      "roma": "VILLA SANTINA",
      "rm": "UD",
      "1234": 406030133
    },
    {
      "1": 1789,
      "roma": "VILLA SANTO STEFANO",
      "rm": "FR",
      "1234": 412060090
    },
    {
      "1": 1790,
      "roma": "VILLAFRANCA PIEMONTE",
      "rm": "TO",
      "1234": 401001300
    },
    {
      "1": 1791,
      "roma": "VILLAURBANA",
      "rm": "OR",
      "1234": 420095072
    },
    {
      "1": 1792,
      "roma": "VILLA VERDE",
      "rm": "OR",
      "1234": 420095073
    },
    {
      "1": 1793,
      "roma": "VILLABASSA",
      "rm": "BZ",
      "1234": 404021113
    },
    {
      "1": 1794,
      "roma": "VILLABATE",
      "rm": "PA",
      "1234": 419082079
    },
    {
      "1": 1795,
      "roma": "VILLACHIARA",
      "rm": "BS",
      "1234": 403017200
    },
    {
      "1": 1796,
      "roma": "VILLA SAN PIETRO",
      "rm": "CA",
      "1234": 420092099
    },
    {
      "1": 1797,
      "roma": "VILLADEATI",
      "rm": "AL",
      "1234": 401006182
    },
    {
      "1": 1798,
      "roma": "VILLADOSE",
      "rm": "RO",
      "1234": 405029048
    },
    {
      "1": 1799,
      "roma": "VILLADOSSOLA",
      "rm": "VB",
      "1234": 401103075
    },
    {
      "1": 1800,
      "roma": "VILLAFALLETTO",
      "rm": "CN",
      "1234": 401004244
    },
    {
      "1": 1801,
      "roma": "VILLAFRANCA D'ASTI",
      "rm": "AT",
      "1234": 401005117
    },
    {
      "1": 1802,
      "roma": "VILLAFRANCA DI VERONA",
      "rm": "VR",
      "1234": 405023096
    },
    {
      "1": 1803,
      "roma": "VILLAFRANCA IN LUNIGIANA",
      "rm": "MS",
      "1234": 409045016
    },
    {
      "1": 1804,
      "roma": "VILLAFRANCA PADOVANA",
      "rm": "PD",
      "1234": 405028103
    },
    {
      "1": 1805,
      "roma": "VILLAFRANCA SICULA",
      "rm": "AG",
      "1234": 419084043
    },
    {
      "1": 1806,
      "roma": "VILLAFRANCA TIRRENA",
      "rm": "ME",
      "1234": 419083105
    },
    {
      "1": 1807,
      "roma": "VILLAFRATI",
      "rm": "PA",
      "1234": 419082080
    },
    {
      "1": 1808,
      "roma": "VILLAGA",
      "rm": "VI",
      "1234": 405024117
    },
    {
      "1": 1809,
      "roma": "VILLAGRANDE STRISAILI",
      "rm": "NU",
      "1234": 420091101
    },
    {
      "1": 1810,
      "roma": "VILLALAGO",
      "rm": "AQ",
      "1234": 413066103
    },
    {
      "1": 1811,
      "roma": "VILLALBA",
      "rm": "CL",
      "1234": 419085022
    },
    {
      "1": 1812,
      "roma": "VILLALFONSINA",
      "rm": "CH",
      "1234": 413069100
    },
    {
      "1": 1813,
      "roma": "VILLALVERNIA",
      "rm": "AL",
      "1234": 401006183
    },
    {
      "1": 1814,
      "roma": "VILLAMAGNA",
      "rm": "CH",
      "1234": 413069101
    },
    {
      "1": 1815,
      "roma": "VILLAMAINA",
      "rm": "AV",
      "1234": 415064117
    },
    {
      "1": 1816,
      "roma": "VILLAMAR",
      "rm": "CA",
      "1234": 420092093
    },
    {
      "1": 1817,
      "roma": "VILLAMARZANA",
      "rm": "RO",
      "1234": 405029049
    },
    {
      "1": 1818,
      "roma": "CAMPOLONGO TAPOGLIANO",
      "rm": "UD",
      "1234": 406030138
    },
    {
      "1": 1819,
      "roma": "BINETTO",
      "rm": "BA",
      "1234": 416072008
    },
    {
      "1": 1820,
      "roma": "BIOGLIO",
      "rm": "BI",
      "1234": 401096005
    },
    {
      "1": 1821,
      "roma": "BIONAZ",
      "rm": "AO",
      "1234": 402007010
    },
    {
      "1": 1822,
      "roma": "BIONE",
      "rm": "BS",
      "1234": 403017019
    },
    {
      "1": 1823,
      "roma": "BIRORI",
      "rm": "NU",
      "1234": 420091008
    },
    {
      "1": 1824,
      "roma": "BISACCIA",
      "rm": "AV",
      "1234": 415064011
    },
    {
      "1": 1825,
      "roma": "BISACQUINO",
      "rm": "PA",
      "1234": 419082010
    },
    {
      "1": 1826,
      "roma": "BISEGNA",
      "rm": "AQ",
      "1234": 413066011
    },
    {
      "1": 1827,
      "roma": "BISENTI",
      "rm": "TE",
      "1234": 413067007
    },
    {
      "1": 1828,
      "roma": "BIENO",
      "rm": "TN",
      "1234": 404022015
    },
    {
      "1": 1829,
      "roma": "BIENTINA",
      "rm": "PI",
      "1234": 409050001
    },
    {
      "1": 1830,
      "roma": "BITETTO",
      "rm": "BA",
      "1234": 416072010
    },
    {
      "1": 1831,
      "roma": "BITONTO",
      "rm": "BA",
      "1234": 416072011
    },
    {
      "1": 1832,
      "roma": "BINASCO",
      "rm": "MI",
      "1234": 403015024
    },
    {
      "1": 1833,
      "roma": "BIVONA",
      "rm": "AG",
      "1234": 419084004
    },
    {
      "1": 1834,
      "roma": "BIVONGI",
      "rm": "RC",
      "1234": 418080010
    },
    {
      "1": 1835,
      "roma": "BIZZARONE",
      "rm": "CO",
      "1234": 403013024
    },
    {
      "1": 1836,
      "roma": "BISCEGLIE",
      "rm": "BT",
      "1234": 416110003
    },
    {
      "1": 1837,
      "roma": "BLEGGIO SUPERIORE",
      "rm": "TN",
      "1234": 404022017
    },
    {
      "1": 1838,
      "roma": "BLELLO",
      "rm": "BG",
      "1234": 403016027
    },
    {
      "1": 1839,
      "roma": "BLERA",
      "rm": "VT",
      "1234": 412056007
    },
    {
      "1": 1840,
      "roma": "BLESSAGNO",
      "rm": "CO",
      "1234": 403013025
    },
    {
      "1": 1841,
      "roma": "BLEVIO",
      "rm": "CO",
      "1234": 403013026
    },
    {
      "1": 1842,
      "roma": "BLUFI",
      "rm": "PA",
      "1234": 419082082
    },
    {
      "1": 1843,
      "roma": "BOARA PISANI",
      "rm": "PD",
      "1234": 405028012
    },
    {
      "1": 1844,
      "roma": "BISIGNANO",
      "rm": "CS",
      "1234": 418078017
    },
    {
      "1": 1212,
      "roma": "RADDUSA",
      "rm": "CT",
      "1234": 419087036
    },
    {
      "1": 1213,
      "roma": "RADICOFANI",
      "rm": "SI",
      "1234": 409052024
    },
    {
      "1": 1214,
      "roma": "RADICONDOLI",
      "rm": "SI",
      "1234": 409052025
    },
    {
      "1": 1215,
      "roma": "RAPALLO",
      "rm": "GE",
      "1234": 407010046
    },
    {
      "1": 1216,
      "roma": "RAPINO",
      "rm": "CH",
      "1234": 413069071
    },
    {
      "1": 1217,
      "roma": "RECALE",
      "rm": "CE",
      "1234": 415061067
    },
    {
      "1": 1218,
      "roma": "RECANATI",
      "rm": "MC",
      "1234": 411043044
    },
    {
      "1": 1219,
      "roma": "RECCO",
      "rm": "GE",
      "1234": 407010047
    },
    {
      "1": 1220,
      "roma": "RECETTO",
      "rm": "NO",
      "1234": 401003129
    },
    {
      "1": 1221,
      "roma": "RECOARO TERME",
      "rm": "VI",
      "1234": 405024084
    },
    {
      "1": 1222,
      "roma": "REDAVALLE",
      "rm": "PV",
      "1234": 403018120
    },
    {
      "1": 1223,
      "roma": "RAVANUSA",
      "rm": "AG",
      "1234": 419084031
    },
    {
      "1": 1224,
      "roma": "RAVARINO",
      "rm": "MO",
      "1234": 408036034
    },
    {
      "1": 1225,
      "roma": "RAVASCLETTO",
      "rm": "UD",
      "1234": 406030088
    },
    {
      "1": 1226,
      "roma": "RAVELLO",
      "rm": "SA",
      "1234": 415065104
    },
    {
      "1": 1227,
      "roma": "RAVENNA",
      "rm": "RA",
      "1234": 408039014
    },
    {
      "1": 1228,
      "roma": "RANICA",
      "rm": "BG",
      "1234": 403016178
    },
    {
      "1": 1229,
      "roma": "RANZANICO",
      "rm": "BG",
      "1234": 403016179
    },
    {
      "1": 1230,
      "roma": "RANZO",
      "rm": "IM",
      "1234": 407008048
    },
    {
      "1": 1231,
      "roma": "REALMONTE",
      "rm": "AG",
      "1234": 419084032
    },
    {
      "1": 1232,
      "roma": "REANA DEL ROIALE",
      "rm": "UD",
      "1234": 406030090
    },
    {
      "1": 1233,
      "roma": "REANO",
      "rm": "TO",
      "1234": 401001211
    },
    {
      "1": 1234,
      "roma": "RENDE",
      "rm": "CS",
      "1234": 418078102
    },
    {
      "1": 1235,
      "roma": "RENON",
      "rm": "BZ",
      "1234": 404021072
    },
    {
      "1": 1236,
      "roma": "REDONDESCO",
      "rm": "MN",
      "1234": 403020048
    },
    {
      "1": 1237,
      "roma": "REFRANCORE",
      "rm": "AT",
      "1234": 401005089
    },
    {
      "1": 1238,
      "roma": "REFRONTOLO",
      "rm": "TV",
      "1234": 405026065
    },
    {
      "1": 1239,
      "roma": "REGALBUTO",
      "rm": "EN",
      "1234": 419086016
    },
    {
      "1": 1240,
      "roma": "REGGELLO",
      "rm": "FI",
      "1234": 409048035
    },
    {
      "1": 1241,
      "roma": "REGGIO CALABRIA",
      "rm": "RC",
      "1234": 418080063
    },
    {
      "1": 1242,
      "roma": "RAVEO",
      "rm": "UD",
      "1234": 406030089
    },
    {
      "1": 1243,
      "roma": "RAVISCANINA",
      "rm": "CE",
      "1234": 415061066
    },
    {
      "1": 1244,
      "roma": "RE",
      "rm": "VB",
      "1234": 401103060
    },
    {
      "1": 1245,
      "roma": "REA",
      "rm": "PV",
      "1234": 403018119
    },
    {
      "1": 1246,
      "roma": "REITANO",
      "rm": "ME",
      "1234": 419083070
    },
    {
      "1": 1247,
      "roma": "REMANZACCO",
      "rm": "UD",
      "1234": 406030091
    },
    {
      "1": 1248,
      "roma": "REMEDELLO",
      "rm": "BS",
      "1234": 403017160
    },
    {
      "1": 1249,
      "roma": "REVINE LAGO",
      "rm": "TV",
      "1234": 405026067
    },
    {
      "1": 1250,
      "roma": "REVO'",
      "rm": "TN",
      "1234": 404022152
    },
    {
      "1": 1251,
      "roma": "REZZAGO",
      "rm": "CO",
      "1234": 403013195
    },
    {
      "1": 1252,
      "roma": "REZZATO",
      "rm": "BS",
      "1234": 403017161
    },
    {
      "1": 1253,
      "roma": "REZZO",
      "rm": "IM",
      "1234": 407008049
    },
    {
      "1": 1254,
      "roma": "REZZOAGLIO",
      "rm": "GE",
      "1234": 407010048
    },
    {
      "1": 1255,
      "roma": "RHEMES-NOTRE-DAME",
      "rm": "AO",
      "1234": 402007055
    },
    {
      "1": 1256,
      "roma": "RESANA",
      "rm": "TV",
      "1234": 405026066
    },
    {
      "1": 1257,
      "roma": "RESCALDINA",
      "rm": "MI",
      "1234": 403015181
    },
    {
      "1": 1258,
      "roma": "RESIA",
      "rm": "UD",
      "1234": 406030092
    },
    {
      "1": 1259,
      "roma": "RESIUTTA",
      "rm": "UD",
      "1234": 406030093
    },
    {
      "1": 1260,
      "roma": "REGGIO EMILIA",
      "rm": "RE",
      "1234": 408035033
    },
    {
      "1": 1261,
      "roma": "REGGIOLO",
      "rm": "RE",
      "1234": 408035032
    },
    {
      "1": 1262,
      "roma": "REINO",
      "rm": "BN",
      "1234": 415062056
    },
    {
      "1": 1263,
      "roma": "REVIGLIASCO D'ASTI",
      "rm": "AT",
      "1234": 401005090
    },
    {
      "1": 1264,
      "roma": "SANTO STEFANO DI CADORE",
      "rm": "BL",
      "1234": 405025050
    },
    {
      "1": 1265,
      "roma": "SANTO STEFANO DI CAMASTRA",
      "rm": "ME",
      "1234": 419083091
    },
    {
      "1": 1266,
      "roma": "SANTO STEFANO DI MAGRA",
      "rm": "SP",
      "1234": 407011026
    },
    {
      "1": 1267,
      "roma": "SANTO STEFANO DI ROGLIANO",
      "rm": "CS",
      "1234": 418078134
    },
    {
      "1": 1268,
      "roma": "SANTA VITTORIA D'ALBA",
      "rm": "CN",
      "1234": 401004212
    },
    {
      "1": 1269,
      "roma": "SANTADI",
      "rm": "CA",
      "1234": 420092060
    },
    {
      "1": 1270,
      "roma": "SANTO STEFANO QUISQUINA",
      "rm": "AG",
      "1234": 419084040
    },
    {
      "1": 1271,
      "roma": "SANTO STEFANO ROERO",
      "rm": "CN",
      "1234": 401004214
    },
    {
      "1": 1272,
      "roma": "SANTO STEFANO TICINO",
      "rm": "MI",
      "1234": 403015200
    },
    {
      "1": 1273,
      "roma": "SANTOMENNA",
      "rm": "SA",
      "1234": 415065131
    },
    {
      "1": 1274,
      "roma": "SANTOPADRE",
      "rm": "FR",
      "1234": 412060069
    },
    {
      "1": 1275,
      "roma": "SANTORSO",
      "rm": "VI",
      "1234": 405024095
    },
    {
      "1": 1276,
      "roma": "SANTU LUSSURGIU",
      "rm": "OR",
      "1234": 420095049
    },
    {
      "1": 1277,
      "roma": "SANZA",
      "rm": "SA",
      "1234": 415065133
    },
    {
      "1": 1278,
      "roma": "SARONNO",
      "rm": "VA",
      "1234": 403012119
    },
    {
      "1": 1279,
      "roma": "SARRE",
      "rm": "AO",
      "1234": 402007066
    },
    {
      "1": 1280,
      "roma": "SARROCH",
      "rm": "CA",
      "1234": 420092066
    },
    {
      "1": 1281,
      "roma": "SARSINA",
      "rm": "FO",
      "1234": 408040044
    },
    {
      "1": 1282,
      "roma": "SARTEANO",
      "rm": "SI",
      "1234": 409052031
    },
    {
      "1": 1283,
      "roma": "SAPRI",
      "rm": "SA",
      "1234": 415065134
    },
    {
      "1": 1284,
      "roma": "SARACENA",
      "rm": "CS",
      "1234": 418078136
    },
    {
      "1": 1285,
      "roma": "SARACINESCO",
      "rm": "RM",
      "1234": 412058101
    },
    {
      "1": 1286,
      "roma": "SARCEDO",
      "rm": "VI",
      "1234": 405024097
    },
    {
      "1": 1287,
      "roma": "SARCONI",
      "rm": "PZ",
      "1234": 417076081
    },
    {
      "1": 1288,
      "roma": "SANTO STEFANO DI SESSANIO",
      "rm": "AQ",
      "1234": 413066091
    },
    {
      "1": 1289,
      "roma": "SANTO STEFANO IN ASPROMONTE",
      "rm": "RC",
      "1234": 418080083
    },
    {
      "1": 1290,
      "roma": "SANTO STEFANO LODIGIANO",
      "rm": "LO",
      "1234": 403098051
    },
    {
      "1": 1291,
      "roma": "SAREGO",
      "rm": "VI",
      "1234": 405024098
    },
    {
      "1": 1292,
      "roma": "SARENTINO",
      "rm": "BZ",
      "1234": 404021086
    },
    {
      "1": 1293,
      "roma": "SAREZZANO",
      "rm": "AL",
      "1234": 401006158
    },
    {
      "1": 1294,
      "roma": "SAREZZO",
      "rm": "BS",
      "1234": 403017174
    },
    {
      "1": 1295,
      "roma": "SARMATO",
      "rm": "PC",
      "1234": 408033042
    },
    {
      "1": 1296,
      "roma": "SARMEDE",
      "rm": "TV",
      "1234": 405026078
    },
    {
      "1": 1297,
      "roma": "SARNANO",
      "rm": "MC",
      "1234": 411043049
    },
    {
      "1": 1298,
      "roma": "SARNICO",
      "rm": "BG",
      "1234": 403016193
    },
    {
      "1": 1299,
      "roma": "SARNO",
      "rm": "SA",
      "1234": 415065135
    },
    {
      "1": 1300,
      "roma": "SARNONICO",
      "rm": "TN",
      "1234": 404022170
    },
    {
      "1": 1301,
      "roma": "SAURIS",
      "rm": "UD",
      "1234": 406030107
    },
    {
      "1": 1302,
      "roma": "SAUZE D'OULX",
      "rm": "TO",
      "1234": 401001259
    },
    {
      "1": 1303,
      "roma": "SAUZE DI CESANA",
      "rm": "TO",
      "1234": 401001258
    },
    {
      "1": 1304,
      "roma": "SAVA",
      "rm": "TA",
      "1234": 416073026
    },
    {
      "1": 1305,
      "roma": "SAVELLI",
      "rm": "KR",
      "1234": 418101023
    },
    {
      "1": 1306,
      "roma": "SARTIRANA LOMELLINA",
      "rm": "PV",
      "1234": 403018146
    },
    {
      "1": 1307,
      "roma": "SARULE",
      "rm": "NU",
      "1234": 420091077
    },
    {
      "1": 1308,
      "roma": "SARZANA",
      "rm": "SP",
      "1234": 407011027
    },
    {
      "1": 1309,
      "roma": "SASSANO",
      "rm": "SA",
      "1234": 415065136
    },
    {
      "1": 1310,
      "roma": "SARDARA",
      "rm": "CA",
      "1234": 420092065
    },
    {
      "1": 1311,
      "roma": "SARDIGLIANO",
      "rm": "AL",
      "1234": 401006157
    },
    {
      "1": 1312,
      "roma": "SASSINORO",
      "rm": "BN",
      "1234": 415062072
    },
    {
      "1": 1313,
      "roma": "SASSO DI CASTALDA",
      "rm": "PZ",
      "1234": 417076082
    },
    {
      "1": 1314,
      "roma": "SASSO MARCONI",
      "rm": "BO",
      "1234": 408037057
    },
    {
      "1": 1315,
      "roma": "SASSOFELTRIO",
      "rm": "PS",
      "1234": 411041060
    },
    {
      "1": 1316,
      "roma": "SASSOFERRATO",
      "rm": "AN",
      "1234": 411042044
    },
    {
      "1": 1317,
      "roma": "SASSUOLO",
      "rm": "MO",
      "1234": 408036040
    },
    {
      "1": 1318,
      "roma": "SATRIANO",
      "rm": "CZ",
      "1234": 418079123
    },
    {
      "1": 1319,
      "roma": "SATRIANO DI LUCANIA",
      "rm": "PZ",
      "1234": 417076083
    },
    {
      "1": 1320,
      "roma": "SCALA",
      "rm": "SA",
      "1234": 415065138
    },
    {
      "1": 1321,
      "roma": "SCALA COELI",
      "rm": "CS",
      "1234": 418078137
    },
    {
      "1": 1322,
      "roma": "SCALDASOLE",
      "rm": "PV",
      "1234": 403018147
    },
    {
      "1": 1323,
      "roma": "SCALEA",
      "rm": "CS",
      "1234": 418078138
    },
    {
      "1": 1324,
      "roma": "SCALENGHE",
      "rm": "TO",
      "1234": 401001260
    },
    {
      "1": 1325,
      "roma": "SAVIANO",
      "rm": "NA",
      "1234": 415063076
    },
    {
      "1": 1326,
      "roma": "SAVIGLIANO",
      "rm": "CN",
      "1234": 401004215
    },
    {
      "1": 1327,
      "roma": "SAVIGNANO IRPINO",
      "rm": "AV",
      "1234": 415064096
    },
    {
      "1": 1328,
      "roma": "SASSARI",
      "rm": "SS",
      "1234": 420090064
    },
    {
      "1": 1329,
      "roma": "SASSELLO",
      "rm": "SV",
      "1234": 407009055
    },
    {
      "1": 1330,
      "roma": "SASSETTA",
      "rm": "LI",
      "1234": 409049019
    },
    {
      "1": 1331,
      "roma": "SAVIORE DELL'ADAMELLO",
      "rm": "BS",
      "1234": 403017175
    },
    {
      "1": 1332,
      "roma": "SAVOCA",
      "rm": "ME",
      "1234": 419083093
    },
    {
      "1": 1333,
      "roma": "SAVOGNA",
      "rm": "UD",
      "1234": 406030108
    },
    {
      "1": 1334,
      "roma": "SAVOGNA D'ISONZO",
      "rm": "GO",
      "1234": 406031022
    },
    {
      "1": 1335,
      "roma": "SAVOIA DI LUCANIA",
      "rm": "PZ",
      "1234": 417076084
    },
    {
      "1": 1336,
      "roma": "SAVONA",
      "rm": "SV",
      "1234": 407009056
    },
    {
      "1": 1337,
      "roma": "SCAFA",
      "rm": "PE",
      "1234": 413068039
    },
    {
      "1": 1338,
      "roma": "SCAFATI",
      "rm": "SA",
      "1234": 415065137
    },
    {
      "1": 1339,
      "roma": "SCAGNELLO",
      "rm": "CN",
      "1234": 401004216
    },
    {
      "1": 1340,
      "roma": "SCAPOLI",
      "rm": "IS",
      "1234": 414094048
    },
    {
      "1": 1341,
      "roma": "SCARLINO",
      "rm": "GR",
      "1234": 409053024
    },
    {
      "1": 1342,
      "roma": "SCARMAGNO",
      "rm": "TO",
      "1234": 401001261
    },
    {
      "1": 1343,
      "roma": "SCALETTA ZANCLEA",
      "rm": "ME",
      "1234": 419083094
    },
    {
      "1": 1344,
      "roma": "SCAMPITELLA",
      "rm": "AV",
      "1234": 415064097
    },
    {
      "1": 1345,
      "roma": "SCANDALE",
      "rm": "KR",
      "1234": 418101024
    },
    {
      "1": 1346,
      "roma": "SAVIGNANO SUL PANARO",
      "rm": "MO",
      "1234": 408036041
    },
    {
      "1": 1347,
      "roma": "SAVIGNANO SUL RUBICONE",
      "rm": "FO",
      "1234": 408040045
    },
    {
      "1": 1348,
      "roma": "SAVIGNONE",
      "rm": "GE",
      "1234": 407010057
    },
    {
      "1": 1349,
      "roma": "SCANDOLARA RIPA D'OGLIO",
      "rm": "CR",
      "1234": 403019093
    },
    {
      "1": 1350,
      "roma": "SCANDRIGLIA",
      "rm": "RI",
      "1234": 412057064
    },
    {
      "1": 1351,
      "roma": "SCANNO",
      "rm": "AQ",
      "1234": 413066093
    },
    {
      "1": 1352,
      "roma": "SCANO DI MONTIFERRO",
      "rm": "OR",
      "1234": 420095051
    },
    {
      "1": 1353,
      "roma": "SCANSANO",
      "rm": "GR",
      "1234": 409053023
    },
    {
      "1": 1354,
      "roma": "SCANZANO IONICO",
      "rm": "MT",
      "1234": 417077031
    },
    {
      "1": 1355,
      "roma": "SCANZOROSCIATE",
      "rm": "BG",
      "1234": 403016194
    },
    {
      "1": 1356,
      "roma": "SCIACCA",
      "rm": "AG",
      "1234": 419084041
    },
    {
      "1": 1357,
      "roma": "SCIARA",
      "rm": "PA",
      "1234": 419082068
    },
    {
      "1": 1358,
      "roma": "SCICLI",
      "rm": "RG",
      "1234": 419088011
    },
    {
      "1": 1359,
      "roma": "SCIDO",
      "rm": "RC",
      "1234": 418080084
    },
    {
      "1": 1360,
      "roma": "SCIGLIANO",
      "rm": "CS",
      "1234": 418078139
    },
    {
      "1": 1361,
      "roma": "SCARNAFIGI",
      "rm": "CN",
      "1234": 401004217
    },
    {
      "1": 1362,
      "roma": "SCANDIANO",
      "rm": "RE",
      "1234": 408035040
    },
    {
      "1": 1363,
      "roma": "SCANDICCI",
      "rm": "FI",
      "1234": 409048041
    },
    {
      "1": 1364,
      "roma": "SCANDOLARA RAVARA",
      "rm": "CR",
      "1234": 403019092
    },
    {
      "1": 1365,
      "roma": "SCHEGGINO",
      "rm": "PG",
      "1234": 410054047
    },
    {
      "1": 1366,
      "roma": "SCHIAVI DI ABRUZZO",
      "rm": "CH",
      "1234": 413069088
    },
    {
      "1": 1367,
      "roma": "SCHIAVON",
      "rm": "VI",
      "1234": 405024099
    },
    {
      "1": 1368,
      "roma": "SCHIGNANO",
      "rm": "CO",
      "1234": 403013211
    },
    {
      "1": 1369,
      "roma": "SCARPERIA E SAN PIERO",
      "rm": "FI",
      "1234": 409048053
    },
    {
      "1": 1370,
      "roma": "SCHILPARIO",
      "rm": "BG",
      "1234": 403016195
    },
    {
      "1": 1371,
      "roma": "SCHIO",
      "rm": "VI",
      "1234": 405024100
    },
    {
      "1": 1372,
      "roma": "SCHIVENOGLIA",
      "rm": "MN",
      "1234": 403020060
    },
    {
      "1": 1373,
      "roma": "SECINARO",
      "rm": "AQ",
      "1234": 413066097
    },
    {
      "1": 1374,
      "roma": "SECLI'",
      "rm": "LE",
      "1234": 416075074
    },
    {
      "1": 1375,
      "roma": "SECUGNAGO",
      "rm": "LO",
      "1234": 403098052
    },
    {
      "1": 1376,
      "roma": "SCILLA",
      "rm": "RC",
      "1234": 418080085
    },
    {
      "1": 1377,
      "roma": "SCILLATO",
      "rm": "PA",
      "1234": 419082081
    },
    {
      "1": 1378,
      "roma": "SCIOLZE",
      "rm": "TO",
      "1234": 401001262
    },
    {
      "1": 1379,
      "roma": "SCISCIANO",
      "rm": "NA",
      "1234": 415063077
    },
    {
      "1": 1380,
      "roma": "SCLAFANI BAGNI",
      "rm": "PA",
      "1234": 419082069
    },
    {
      "1": 1381,
      "roma": "SCENA",
      "rm": "BZ",
      "1234": 404021087
    },
    {
      "1": 1382,
      "roma": "SCERNI",
      "rm": "CH",
      "1234": 413069087
    },
    {
      "1": 1383,
      "roma": "SCHEGGIA E PASCELUPO",
      "rm": "PG",
      "1234": 410054046
    },
    {
      "1": 1384,
      "roma": "SCOPELLO",
      "rm": "VC",
      "1234": 401002135
    },
    {
      "1": 1385,
      "roma": "SCOPPITO",
      "rm": "AQ",
      "1234": 413066095
    },
    {
      "1": 1386,
      "roma": "SCORDIA",
      "rm": "CT",
      "1234": 419087049
    },
    {
      "1": 1387,
      "roma": "SCORRANO",
      "rm": "LE",
      "1234": 416075073
    },
    {
      "1": 1388,
      "roma": "SCORZE'",
      "rm": "VE",
      "1234": 405027037
    },
    {
      "1": 1389,
      "roma": "SCURCOLA MARSICANA",
      "rm": "AQ",
      "1234": 413066096
    },
    {
      "1": 1390,
      "roma": "SCURELLE",
      "rm": "TN",
      "1234": 404022171
    },
    {
      "1": 1391,
      "roma": "SCURZOLENGO",
      "rm": "AT",
      "1234": 401005103
    },
    {
      "1": 1392,
      "roma": "SEBORGA",
      "rm": "IM",
      "1234": 407008057
    },
    {
      "1": 1393,
      "roma": "SELARGIUS",
      "rm": "CA",
      "1234": 420092068
    },
    {
      "1": 1394,
      "roma": "SELCI",
      "rm": "RI",
      "1234": 412057065
    },
    {
      "1": 1395,
      "roma": "SELEGAS",
      "rm": "CA",
      "1234": 420092069
    },
    {
      "1": 1396,
      "roma": "SEDEGLIANO",
      "rm": "UD",
      "1234": 406030109
    },
    {
      "1": 1397,
      "roma": "SEDICO",
      "rm": "BL",
      "1234": 405025053
    },
    {
      "1": 1398,
      "roma": "SEDILO",
      "rm": "OR",
      "1234": 420095052
    },
    {
      "1": 1399,
      "roma": "SEDINI",
      "rm": "SS",
      "1234": 420090065
    },
    {
      "1": 1400,
      "roma": "SCONTRONE",
      "rm": "AQ",
      "1234": 413066094
    },
    {
      "1": 1401,
      "roma": "SCOPA",
      "rm": "VC",
      "1234": 401002134
    },
    {
      "1": 1402,
      "roma": "SEGARIU",
      "rm": "CA",
      "1234": 420092067
    },
    {
      "1": 1403,
      "roma": "SEGGIANO",
      "rm": "GR",
      "1234": 409053025
    },
    {
      "1": 1404,
      "roma": "SEGNI",
      "rm": "RM",
      "1234": 412058102
    },
    {
      "1": 1405,
      "roma": "SEGONZANO",
      "rm": "TN",
      "1234": 404022172
    },
    {
      "1": 1406,
      "roma": "SEGRATE",
      "rm": "MI",
      "1234": 403015205
    },
    {
      "1": 1407,
      "roma": "SEGUSINO",
      "rm": "TV",
      "1234": 405026079
    },
    {
      "1": 1408,
      "roma": "SENALE-SAN FELICE",
      "rm": "BZ",
      "1234": 404021118
    },
    {
      "1": 1409,
      "roma": "SELLA GIUDICARIE",
      "rm": "TN",
      "1234": 404022246
    },
    {
      "1": 1410,
      "roma": "SENALES",
      "rm": "BZ",
      "1234": 404021091
    },
    {
      "1": 1411,
      "roma": "SENEGHE",
      "rm": "OR",
      "1234": 420095053
    },
    {
      "1": 1412,
      "roma": "SELLANO",
      "rm": "PG",
      "1234": 410054048
    },
    {
      "1": 1413,
      "roma": "SELLERO",
      "rm": "BS",
      "1234": 403017176
    },
    {
      "1": 1414,
      "roma": "SELLIA",
      "rm": "CZ",
      "1234": 418079126
    },
    {
      "1": 1415,
      "roma": "SEDRIANO",
      "rm": "MI",
      "1234": 403015204
    },
    {
      "1": 1416,
      "roma": "SEDRINA",
      "rm": "BG",
      "1234": 403016196
    },
    {
      "1": 1417,
      "roma": "SEFRO",
      "rm": "MC",
      "1234": 411043050
    },
    {
      "1": 1418,
      "roma": "SELVA DI VAL GARDENA",
      "rm": "BZ",
      "1234": 404021089
    },
    {
      "1": 1419,
      "roma": "SELVAZZANO DENTRO",
      "rm": "PD",
      "1234": 405028086
    },
    {
      "1": 1420,
      "roma": "SELVINO",
      "rm": "BG",
      "1234": 403016197
    },
    {
      "1": 1421,
      "roma": "SEMESTENE",
      "rm": "SS",
      "1234": 420090066
    },
    {
      "1": 1422,
      "roma": "SEMIANA",
      "rm": "PV",
      "1234": 403018148
    },
    {
      "1": 1423,
      "roma": "SEMINARA",
      "rm": "RC",
      "1234": 418080086
    },
    {
      "1": 1424,
      "roma": "SEMPRONIANO",
      "rm": "GR",
      "1234": 409053028
    },
    {
      "1": 1425,
      "roma": "SENAGO",
      "rm": "MI",
      "1234": 403015206
    },
    {
      "1": 1426,
      "roma": "SERAVEZZA",
      "rm": "LU",
      "1234": 409046028
    },
    {
      "1": 1427,
      "roma": "SERDIANA",
      "rm": "CA",
      "1234": 420092071
    },
    {
      "1": 1428,
      "roma": "SEREN DEL GRAPPA",
      "rm": "BL",
      "1234": 405025055
    },
    {
      "1": 1429,
      "roma": "SERGNANO",
      "rm": "CR",
      "1234": 403019094
    },
    {
      "1": 1430,
      "roma": "SERIATE",
      "rm": "BG",
      "1234": 403016198
    },
    {
      "1": 1431,
      "roma": "SENERCHIA",
      "rm": "AV",
      "1234": 415064098
    },
    {
      "1": 1432,
      "roma": "SENIGA",
      "rm": "BS",
      "1234": 403017177
    },
    {
      "1": 1433,
      "roma": "SENIGALLIA",
      "rm": "AN",
      "1234": 411042045
    },
    {
      "1": 1434,
      "roma": "SENIS",
      "rm": "OR",
      "1234": 420095054
    },
    {
      "1": 1435,
      "roma": "SELLIA MARINA",
      "rm": "CZ",
      "1234": 418079127
    },
    {
      "1": 1436,
      "roma": "SELVA DEI MOLINI",
      "rm": "BZ",
      "1234": 404021088
    },
    {
      "1": 1437,
      "roma": "SELVA DI CADORE",
      "rm": "BL",
      "1234": 405025054
    },
    {
      "1": 1438,
      "roma": "SELVA DI PROGNO",
      "rm": "VR",
      "1234": 405023080
    },
    {
      "1": 1439,
      "roma": "SENNORI",
      "rm": "SS",
      "1234": 420090067
    },
    {
      "1": 1440,
      "roma": "SENORBI'",
      "rm": "CA",
      "1234": 420092070
    },
    {
      "1": 1441,
      "roma": "SEPINO",
      "rm": "CB",
      "1234": 414070075
    },
    {
      "1": 1442,
      "roma": "SEQUALS",
      "rm": "PN",
      "1234": 406093042
    },
    {
      "1": 1443,
      "roma": "SERRALUNGA D'ALBA",
      "rm": "CN",
      "1234": 401004218
    },
    {
      "1": 1444,
      "roma": "SERRALUNGA DI CREA",
      "rm": "AL",
      "1234": 401006159
    },
    {
      "1": 1445,
      "roma": "SERRAMANNA",
      "rm": "CA",
      "1234": 420092072
    },
    {
      "1": 1446,
      "roma": "SERRAMAZZONI",
      "rm": "MO",
      "1234": 408036042
    },
    {
      "1": 1447,
      "roma": "SERRAMEZZANA",
      "rm": "SA",
      "1234": 415065139
    },
    {
      "1": 1448,
      "roma": "SERRAMONACESCA",
      "rm": "PE",
      "1234": 413068040
    },
    {
      "1": 1449,
      "roma": "SERRAPETRONA",
      "rm": "MC",
      "1234": 411043051
    },
    {
      "1": 1450,
      "roma": "SERRARA FONTANA",
      "rm": "NA",
      "1234": 415063078
    },
    {
      "1": 1451,
      "roma": "SERINA",
      "rm": "BG",
      "1234": 403016199
    },
    {
      "1": 1452,
      "roma": "SERINO",
      "rm": "AV",
      "1234": 415064099
    },
    {
      "1": 1453,
      "roma": "SERLE",
      "rm": "BS",
      "1234": 403017178
    },
    {
      "1": 1454,
      "roma": "SENISE",
      "rm": "PZ",
      "1234": 417076085
    },
    {
      "1": 1455,
      "roma": "SENNA COMASCO",
      "rm": "CO",
      "1234": 403013212
    },
    {
      "1": 1456,
      "roma": "SENNA LODIGIANA",
      "rm": "LO",
      "1234": 403098053
    },
    {
      "1": 1457,
      "roma": "SENNARIOLO",
      "rm": "OR",
      "1234": 420095055
    },
    {
      "1": 1458,
      "roma": "SERRA DE' CONTI",
      "rm": "AN",
      "1234": 411042046
    },
    {
      "1": 1459,
      "roma": "SERRA RICCO'",
      "rm": "GE",
      "1234": 407010058
    },
    {
      "1": 1460,
      "roma": "SERRA SAN BRUNO",
      "rm": "VV",
      "1234": 418102037
    },
    {
      "1": 1461,
      "roma": "SERRA SAN QUIRICO",
      "rm": "AN",
      "1234": 411042047
    },
    {
      "1": 1462,
      "roma": "SERRA SANT'ABBONDIO",
      "rm": "PS",
      "1234": 411041061
    },
    {
      "1": 1463,
      "roma": "SERRACAPRIOLA",
      "rm": "FG",
      "1234": 416071053
    },
    {
      "1": 1464,
      "roma": "SERRADIFALCO",
      "rm": "CL",
      "1234": 419085018
    },
    {
      "1": 1465,
      "roma": "SESSA AURUNCA",
      "rm": "CE",
      "1234": 415061088
    },
    {
      "1": 1466,
      "roma": "SERVIGLIANO",
      "rm": "FM",
      "1234": 411109038
    },
    {
      "1": 1467,
      "roma": "SESSA CILENTO",
      "rm": "SA",
      "1234": 415065141
    },
    {
      "1": 1468,
      "roma": "SESSAME",
      "rm": "AT",
      "1234": 401005105
    },
    {
      "1": 1469,
      "roma": "SERRASTRETTA",
      "rm": "CZ",
      "1234": 418079129
    },
    {
      "1": 1470,
      "roma": "SERRATA",
      "rm": "RC",
      "1234": 418080087
    },
    {
      "1": 1471,
      "roma": "SERMONETA",
      "rm": "LT",
      "1234": 412059027
    },
    {
      "1": 1472,
      "roma": "SERNAGLIA DELLA BATTAGLIA",
      "rm": "TV",
      "1234": 405026080
    },
    {
      "1": 1473,
      "roma": "SERNIO",
      "rm": "SO",
      "1234": 403014059
    },
    {
      "1": 1474,
      "roma": "SEROLE",
      "rm": "AT",
      "1234": 401005104
    },
    {
      "1": 1475,
      "roma": "SERRA D'AIELLO",
      "rm": "CS",
      "1234": 418078140
    },
    {
      "1": 1476,
      "roma": "SERRAVALLE SCRIVIA",
      "rm": "AL",
      "1234": 401006160
    },
    {
      "1": 1477,
      "roma": "SERRAVALLE SESIA",
      "rm": "VC",
      "1234": 401002137
    },
    {
      "1": 1478,
      "roma": "SERRE",
      "rm": "SA",
      "1234": 415065140
    },
    {
      "1": 1479,
      "roma": "SERRENTI",
      "rm": "CA",
      "1234": 420092073
    },
    {
      "1": 1480,
      "roma": "SERRONE",
      "rm": "FR",
      "1234": 412060071
    },
    {
      "1": 1481,
      "roma": "SERSALE",
      "rm": "CZ",
      "1234": 418079130
    },
    {
      "1": 1482,
      "roma": "SESTRIERE",
      "rm": "TO",
      "1234": 401001263
    },
    {
      "1": 1483,
      "roma": "SESTU",
      "rm": "CA",
      "1234": 420092074
    },
    {
      "1": 1484,
      "roma": "SETTALA",
      "rm": "MI",
      "1234": 403015210
    },
    {
      "1": 1485,
      "roma": "SETTEFRATI",
      "rm": "FR",
      "1234": 412060072
    },
    {
      "1": 1486,
      "roma": "SETTIME",
      "rm": "AT",
      "1234": 401005106
    },
    {
      "1": 1487,
      "roma": "SETTIMO MILANESE",
      "rm": "MI",
      "1234": 403015211
    },
    {
      "1": 1488,
      "roma": "SETTIMO ROTTARO",
      "rm": "TO",
      "1234": 401001264
    },
    {
      "1": 1489,
      "roma": "SERRI",
      "rm": "CA",
      "1234": 420092120
    },
    {
      "1": 1490,
      "roma": "SISSA TRECASALI",
      "rm": "PR",
      "1234": 408034049
    },
    {
      "1": 1491,
      "roma": "SESSANO DEL MOLISE",
      "rm": "IS",
      "1234": 414094049
    },
    {
      "1": 1492,
      "roma": "SERRAVALLE A PO",
      "rm": "MN",
      "1234": 403020062
    },
    {
      "1": 1493,
      "roma": "SERRAVALLE DI CHIENTI",
      "rm": "MC",
      "1234": 411043052
    },
    {
      "1": 1494,
      "roma": "SERRAVALLE LANGHE",
      "rm": "CN",
      "1234": 401004219
    },
    {
      "1": 1495,
      "roma": "SERRAVALLE PISTOIESE",
      "rm": "PT",
      "1234": 409047020
    },
    {
      "1": 1496,
      "roma": "SESTO CAMPANO",
      "rm": "IS",
      "1234": 414094050
    },
    {
      "1": 1497,
      "roma": "SESTO ED UNITI",
      "rm": "CR",
      "1234": 403019095
    },
    {
      "1": 1498,
      "roma": "SESTO FIORENTINO",
      "rm": "FI",
      "1234": 409048043
    },
    {
      "1": 1499,
      "roma": "SESTO SAN GIOVANNI",
      "rm": "MI",
      "1234": 403015209
    },
    {
      "1": 1500,
      "roma": "SESTOLA",
      "rm": "MO",
      "1234": 408036043
    },
    {
      "1": 1501,
      "roma": "SESTRI LEVANTE",
      "rm": "GE",
      "1234": 407010059
    },
    {
      "1": 1502,
      "roma": "SIAMANNA",
      "rm": "OR",
      "1234": 420095057
    },
    {
      "1": 1503,
      "roma": "SIANO",
      "rm": "SA",
      "1234": 415065142
    },
    {
      "1": 1504,
      "roma": "SIAPICCIA",
      "rm": "OR",
      "1234": 420095076
    },
    {
      "1": 1505,
      "roma": "SICIGNANO DEGLI ALBURNI",
      "rm": "SA",
      "1234": 415065143
    },
    {
      "1": 1506,
      "roma": "SETTIMO SAN PIETRO",
      "rm": "CA",
      "1234": 420092075
    },
    {
      "1": 1507,
      "roma": "SESTA GODANO",
      "rm": "SP",
      "1234": 407011028
    },
    {
      "1": 1508,
      "roma": "SESTINO",
      "rm": "AR",
      "1234": 409051035
    },
    {
      "1": 1509,
      "roma": "SESTO",
      "rm": "BZ",
      "1234": 404021092
    },
    {
      "1": 1510,
      "roma": "SESTO AL REGHENA",
      "rm": "PN",
      "1234": 406093043
    },
    {
      "1": 1511,
      "roma": "SESTO CALENDE",
      "rm": "VA",
      "1234": 403012120
    },
    {
      "1": 1512,
      "roma": "SEZZADIO",
      "rm": "AL",
      "1234": 401006161
    },
    {
      "1": 1513,
      "roma": "SEZZE",
      "rm": "LT",
      "1234": 412059028
    },
    {
      "1": 1514,
      "roma": "SFRUZ",
      "rm": "TN",
      "1234": 404022173
    },
    {
      "1": 1515,
      "roma": "SGONICO",
      "rm": "TS",
      "1234": 406032005
    },
    {
      "1": 1516,
      "roma": "SGURGOLA",
      "rm": "FR",
      "1234": 412060073
    },
    {
      "1": 1517,
      "roma": "SIAMAGGIORE",
      "rm": "OR",
      "1234": 420095056
    },
    {
      "1": 1518,
      "roma": "SILIGO",
      "rm": "SS",
      "1234": 420090068
    },
    {
      "1": 1519,
      "roma": "SILIQUA",
      "rm": "CA",
      "1234": 420092078
    },
    {
      "1": 1520,
      "roma": "SILIUS",
      "rm": "CA",
      "1234": 420092079
    },
    {
      "1": 1521,
      "roma": "SILLAVENGO",
      "rm": "NO",
      "1234": 401003138
    },
    {
      "1": 1522,
      "roma": "SILVANO D'ORBA",
      "rm": "AL",
      "1234": 401006162
    },
    {
      "1": 1523,
      "roma": "SILVANO PIETRA",
      "rm": "PV",
      "1234": 403018149
    },
    {
      "1": 1524,
      "roma": "SETTIMO TORINESE",
      "rm": "TO",
      "1234": 401001265
    },
    {
      "1": 1525,
      "roma": "SETTIMO VITTONE",
      "rm": "TO",
      "1234": 401001266
    },
    {
      "1": 1526,
      "roma": "SETTINGIANO",
      "rm": "CZ",
      "1234": 418079131
    },
    {
      "1": 1527,
      "roma": "SETZU",
      "rm": "CA",
      "1234": 420092076
    },
    {
      "1": 1528,
      "roma": "SEUI",
      "rm": "NU",
      "1234": 420091081
    },
    {
      "1": 1529,
      "roma": "SEULO",
      "rm": "NU",
      "1234": 420091082
    },
    {
      "1": 1530,
      "roma": "SIGNA",
      "rm": "FI",
      "1234": 409048044
    },
    {
      "1": 1531,
      "roma": "SILANDRO",
      "rm": "BZ",
      "1234": 404021093
    },
    {
      "1": 1532,
      "roma": "SILANUS",
      "rm": "NU",
      "1234": 420091083
    },
    {
      "1": 1533,
      "roma": "SILEA",
      "rm": "TV",
      "1234": 405026081
    },
    {
      "1": 1534,
      "roma": "SINIO",
      "rm": "CN",
      "1234": 401004220
    },
    {
      "1": 1535,
      "roma": "SINISCOLA",
      "rm": "NU",
      "1234": 420091085
    },
    {
      "1": 1536,
      "roma": "SINNAI",
      "rm": "CA",
      "1234": 420092080
    },
    {
      "1": 1537,
      "roma": "SINOPOLI",
      "rm": "RC",
      "1234": 418080089
    },
    {
      "1": 1538,
      "roma": "SIRACUSA",
      "rm": "SR",
      "1234": 419089017
    },
    {
      "1": 1539,
      "roma": "SIRIGNANO",
      "rm": "AV",
      "1234": 415064100
    },
    {
      "1": 1540,
      "roma": "SILVI",
      "rm": "TE",
      "1234": 413067040
    },
    {
      "1": 1541,
      "roma": "SEVESO",
      "rm": "MB",
      "1234": 403108040
    },
    {
      "1": 1542,
      "roma": "SICULIANA",
      "rm": "AG",
      "1234": 419084042
    },
    {
      "1": 1543,
      "roma": "SIDDI",
      "rm": "CA",
      "1234": 420092077
    },
    {
      "1": 1544,
      "roma": "SIDERNO",
      "rm": "RC",
      "1234": 418080088
    },
    {
      "1": 1545,
      "roma": "SIENA",
      "rm": "SI",
      "1234": 409052032
    },
    {
      "1": 1546,
      "roma": "SIGILLO",
      "rm": "PG",
      "1234": 410054049
    },
    {
      "1": 1547,
      "roma": "SINAGRA",
      "rm": "ME",
      "1234": 419083095
    },
    {
      "1": 1548,
      "roma": "SINALUNGA",
      "rm": "SI",
      "1234": 409052033
    },
    {
      "1": 1549,
      "roma": "SINDIA",
      "rm": "NU",
      "1234": 420091084
    },
    {
      "1": 1550,
      "roma": "SINI",
      "rm": "OR",
      "1234": 420095060
    },
    {
      "1": 1551,
      "roma": "SIZIANO",
      "rm": "PV",
      "1234": 403018150
    },
    {
      "1": 1552,
      "roma": "SIZZANO",
      "rm": "NO",
      "1234": 401003139
    },
    {
      "1": 1553,
      "roma": "SLUDERNO",
      "rm": "BZ",
      "1234": 404021094
    },
    {
      "1": 1554,
      "roma": "SIRIS",
      "rm": "OR",
      "1234": 420095061
    },
    {
      "1": 1555,
      "roma": "SIMALA",
      "rm": "OR",
      "1234": 420095058
    },
    {
      "1": 1556,
      "roma": "SILLANO GIUNCUGNANO",
      "rm": "LU",
      "1234": 409046037
    },
    {
      "1": 1557,
      "roma": "SIMAXIS",
      "rm": "OR",
      "1234": 420095059
    },
    {
      "1": 1558,
      "roma": "SIMBARIO",
      "rm": "VV",
      "1234": 418102038
    },
    {
      "1": 1559,
      "roma": "SIMERI E CRICHI",
      "rm": "CZ",
      "1234": 418079133
    },
    {
      "1": 1560,
      "roma": "VALMOREA",
      "rm": "CO",
      "1234": 403013232
    },
    {
      "1": 1561,
      "roma": "VALMOZZOLA",
      "rm": "PR",
      "1234": 408034044
    },
    {
      "1": 1562,
      "roma": "VALNEGRA",
      "rm": "BG",
      "1234": 403016227
    },
    {
      "1": 1563,
      "roma": "VALPELLINE",
      "rm": "AO",
      "1234": 402007069
    },
    {
      "1": 1564,
      "roma": "VENTASSO",
      "rm": "RE",
      "1234": 408035046
    },
    {
      "1": 1565,
      "roma": "VALPERGA",
      "rm": "TO",
      "1234": 401001287
    },
    {
      "1": 1566,
      "roma": "VALPRATO SOANA",
      "rm": "TO",
      "1234": 401001288
    },
    {
      "1": 1567,
      "roma": "VALTOPINA",
      "rm": "PG",
      "1234": 410054059
    },
    {
      "1": 1568,
      "roma": "VALTORTA",
      "rm": "BG",
      "1234": 403016229
    },
    {
      "1": 1569,
      "roma": "VALTOURNENCHE",
      "rm": "AO",
      "1234": 402007071
    },
    {
      "1": 1570,
      "roma": "VALVA",
      "rm": "SA",
      "1234": 415065155
    },
    {
      "1": 1571,
      "roma": "VARAZZE",
      "rm": "SV",
      "1234": 407009065
    },
    {
      "1": 1572,
      "roma": "VARCO SABINO",
      "rm": "RI",
      "1234": 412057073
    },
    {
      "1": 1573,
      "roma": "VARENA",
      "rm": "TN",
      "1234": 404022211
    },
    {
      "1": 1574,
      "roma": "VALVERDE",
      "rm": "CT",
      "1234": 419087052
    },
    {
      "1": 1575,
      "roma": "VALVESTINO",
      "rm": "BS",
      "1234": 403017194
    },
    {
      "1": 1576,
      "roma": "VANDOIES",
      "rm": "BZ",
      "1234": 404021110
    },
    {
      "1": 1577,
      "roma": "VALSAMOGGIA",
      "rm": "BO",
      "1234": 408037061
    },
    {
      "1": 1578,
      "roma": "VANZAGHELLO",
      "rm": "MI",
      "1234": 403015249
    },
    {
      "1": 1579,
      "roma": "VANZAGO",
      "rm": "MI",
      "1234": 403015229
    },
    {
      "1": 1580,
      "roma": "VALSAVARENCHE",
      "rm": "AO",
      "1234": 402007070
    },
    {
      "1": 1581,
      "roma": "VALSINNI",
      "rm": "MT",
      "1234": 417077030
    },
    {
      "1": 1582,
      "roma": "VALSOLDA",
      "rm": "CO",
      "1234": 403013234
    },
    {
      "1": 1583,
      "roma": "VARANO BORGHI",
      "rm": "VA",
      "1234": 403012132
    },
    {
      "1": 1584,
      "roma": "VARANO DE' MELEGARI",
      "rm": "PR",
      "1234": 408034045
    },
    {
      "1": 1585,
      "roma": "VARAPODIO",
      "rm": "RC",
      "1234": 418080095
    },
    {
      "1": 1586,
      "roma": "VASTO",
      "rm": "CH",
      "1234": 413069099
    },
    {
      "1": 1587,
      "roma": "VASTOGIRARDI",
      "rm": "IS",
      "1234": 414094051
    },
    {
      "1": 1588,
      "roma": "VARENNA",
      "rm": "LC",
      "1234": 403097084
    },
    {
      "1": 1589,
      "roma": "VARESE",
      "rm": "VA",
      "1234": 403012133
    },
    {
      "1": 1590,
      "roma": "VARESE LIGURE",
      "rm": "SP",
      "1234": 407011029
    },
    {
      "1": 1591,
      "roma": "VARISELLA",
      "rm": "TO",
      "1234": 401001289
    },
    {
      "1": 1592,
      "roma": "VARMO",
      "rm": "UD",
      "1234": 406030130
    },
    {
      "1": 1593,
      "roma": "VAREDO",
      "rm": "MB",
      "1234": 403108045
    },
    {
      "1": 1594,
      "roma": "VANZONE CON SAN CARLO",
      "rm": "VB",
      "1234": 401103070
    },
    {
      "1": 1595,
      "roma": "VAPRIO D'ADDA",
      "rm": "MI",
      "1234": 403015230
    },
    {
      "1": 1596,
      "roma": "VAPRIO D'AGOGNA",
      "rm": "NO",
      "1234": 401003153
    },
    {
      "1": 1597,
      "roma": "VARALLO",
      "rm": "VC",
      "1234": 401002156
    },
    {
      "1": 1598,
      "roma": "VARALLO POMBIA",
      "rm": "NO",
      "1234": 401003154
    },
    {
      "1": 1599,
      "roma": "VASANELLO",
      "rm": "VT",
      "1234": 412056055
    },
    {
      "1": 1600,
      "roma": "VASIA",
      "rm": "IM",
      "1234": 407008064
    },
    {
      "1": 1601,
      "roma": "VELESO",
      "rm": "CO",
      "1234": 403013236
    },
    {
      "1": 1602,
      "roma": "VELEZZO LOMELLINA",
      "rm": "PV",
      "1234": 403018172
    },
    {
      "1": 1603,
      "roma": "VAUDA CANAVESE",
      "rm": "TO",
      "1234": 401001290
    },
    {
      "1": 1604,
      "roma": "VAZZANO",
      "rm": "VV",
      "1234": 418102046
    },
    {
      "1": 1605,
      "roma": "VAZZOLA",
      "rm": "TV",
      "1234": 405026088
    },
    {
      "1": 1606,
      "roma": "VECCHIANO",
      "rm": "PI",
      "1234": 409050037
    },
    {
      "1": 1607,
      "roma": "VEDANO OLONA",
      "rm": "VA",
      "1234": 403012134
    },
    {
      "1": 1608,
      "roma": "VEDELAGO",
      "rm": "TV",
      "1234": 405026089
    },
    {
      "1": 1609,
      "roma": "VARNA",
      "rm": "BZ",
      "1234": 404021111
    },
    {
      "1": 1610,
      "roma": "VARSI",
      "rm": "PR",
      "1234": 408034046
    },
    {
      "1": 1611,
      "roma": "VARZI",
      "rm": "PV",
      "1234": 403018171
    },
    {
      "1": 1612,
      "roma": "VARZO",
      "rm": "VB",
      "1234": 401103071
    },
    {
      "1": 1613,
      "roma": "VEGGIANO",
      "rm": "PD",
      "1234": 405028096
    },
    {
      "1": 1614,
      "roma": "VEGLIE",
      "rm": "LE",
      "1234": 416075092
    },
    {
      "1": 1615,
      "roma": "VEGLIO",
      "rm": "BI",
      "1234": 401096075
    },
    {
      "1": 1616,
      "roma": "VEJANO",
      "rm": "VT",
      "1234": 412056056
    },
    {
      "1": 1617,
      "roma": "VENEZIA",
      "rm": "VE",
      "1234": 405027042
    },
    {
      "1": 1618,
      "roma": "VELLETRI",
      "rm": "RM",
      "1234": 412058111
    },
    {
      "1": 1619,
      "roma": "VELLEZZO BELLINI",
      "rm": "PV",
      "1234": 403018173
    },
    {
      "1": 1620,
      "roma": "VELO D'ASTICO",
      "rm": "VI",
      "1234": 405024115
    },
    {
      "1": 1621,
      "roma": "VELO VERONESE",
      "rm": "VR",
      "1234": 405023090
    },
    {
      "1": 1622,
      "roma": "VELTURNO",
      "rm": "BZ",
      "1234": 404021116
    },
    {
      "1": 1623,
      "roma": "VENAFRO",
      "rm": "IS",
      "1234": 414094052
    },
    {
      "1": 1624,
      "roma": "VENARIA REALE",
      "rm": "TO",
      "1234": 401001292
    },
    {
      "1": 1625,
      "roma": "VENAROTTA",
      "rm": "AP",
      "1234": 411044073
    },
    {
      "1": 1626,
      "roma": "VENASCA",
      "rm": "CN",
      "1234": 401004237
    },
    {
      "1": 1627,
      "roma": "VEDESETA",
      "rm": "BG",
      "1234": 403016230
    },
    {
      "1": 1628,
      "roma": "VEDANO AL LAMBRO",
      "rm": "MB",
      "1234": 403108046
    },
    {
      "1": 1629,
      "roma": "VEDUGGIO CON COLZANO",
      "rm": "MB",
      "1234": 403108047
    },
    {
      "1": 1630,
      "roma": "VENEGONO INFERIORE",
      "rm": "VA",
      "1234": 403012136
    },
    {
      "1": 1631,
      "roma": "VENEGONO SUPERIORE",
      "rm": "VA",
      "1234": 403012137
    },
    {
      "1": 1632,
      "roma": "VENETICO",
      "rm": "ME",
      "1234": 419083104
    },
    {
      "1": 1633,
      "roma": "VENIANO",
      "rm": "CO",
      "1234": 403013238
    },
    {
      "1": 1634,
      "roma": "VENOSA",
      "rm": "PZ",
      "1234": 417076095
    },
    {
      "1": 1635,
      "roma": "VENTICANO",
      "rm": "AV",
      "1234": 415064116
    },
    {
      "1": 1636,
      "roma": "VENTIMIGLIA",
      "rm": "IM",
      "1234": 407008065
    },
    {
      "1": 1637,
      "roma": "VENTIMIGLIA DI SICILIA",
      "rm": "PA",
      "1234": 419082077
    },
    {
      "1": 1000,
      "roma": "PONTESTURA",
      "rm": "AL",
      "1234": 401006133
    },
    {
      "1": 1001,
      "roma": "PONTEVICO",
      "rm": "BS",
      "1234": 403017149
    },
    {
      "1": 1002,
      "roma": "PORCIA",
      "rm": "PN",
      "1234": 406093032
    },
    {
      "1": 1003,
      "roma": "PORTO CERESIO",
      "rm": "VA",
      "1234": 403012113
    },
    {
      "1": 1004,
      "roma": "PORTO CESAREO",
      "rm": "LE",
      "1234": 416075097
    },
    {
      "1": 1005,
      "roma": "PORTO EMPEDOCLE",
      "rm": "AG",
      "1234": 419084028
    },
    {
      "1": 1006,
      "roma": "PORTO MANTOVANO",
      "rm": "MN",
      "1234": 403020045
    },
    {
      "1": 1007,
      "roma": "PORTO RECANATI",
      "rm": "MC",
      "1234": 411043042
    },
    {
      "1": 1008,
      "roma": "PORTO TOLLE",
      "rm": "RO",
      "1234": 405029039
    },
    {
      "1": 1009,
      "roma": "PORTO TORRES",
      "rm": "SS",
      "1234": 420090058
    },
    {
      "1": 1010,
      "roma": "PORTO VALTRAVAGLIA",
      "rm": "VA",
      "1234": 403012114
    },
    {
      "1": 1011,
      "roma": "PORTACOMARO",
      "rm": "AT",
      "1234": 401005087
    },
    {
      "1": 1012,
      "roma": "PORTALBERA",
      "rm": "PV",
      "1234": 403018118
    },
    {
      "1": 1013,
      "roma": "PONZONE",
      "rm": "AL",
      "1234": 401006136
    },
    {
      "1": 1014,
      "roma": "POPOLI",
      "rm": "PE",
      "1234": 413068033
    },
    {
      "1": 1015,
      "roma": "POPPI",
      "rm": "AR",
      "1234": 409051031
    },
    {
      "1": 1016,
      "roma": "PONZANO DI FERMO",
      "rm": "FM",
      "1234": 411109032
    },
    {
      "1": 1017,
      "roma": "PORANO",
      "rm": "TR",
      "1234": 410055028
    },
    {
      "1": 1018,
      "roma": "PORCARI",
      "rm": "LU",
      "1234": 409046026
    },
    {
      "1": 1019,
      "roma": "PORTO AZZURRO",
      "rm": "LI",
      "1234": 409049013
    },
    {
      "1": 1020,
      "roma": "PORTULA",
      "rm": "BI",
      "1234": 401096048
    },
    {
      "1": 1021,
      "roma": "POSADA",
      "rm": "NU",
      "1234": 420091073
    },
    {
      "1": 1022,
      "roma": "POSINA",
      "rm": "VI",
      "1234": 405024080
    },
    {
      "1": 1023,
      "roma": "POSITANO",
      "rm": "SA",
      "1234": 415065100
    },
    {
      "1": 1024,
      "roma": "POSSAGNO",
      "rm": "TV",
      "1234": 405026061
    },
    {
      "1": 1025,
      "roma": "POSTA",
      "rm": "RI",
      "1234": 412057057
    },
    {
      "1": 1026,
      "roma": "POSTA FIBRENO",
      "rm": "FR",
      "1234": 412060057
    },
    {
      "1": 1027,
      "roma": "POSTAL",
      "rm": "BZ",
      "1234": 404021066
    },
    {
      "1": 1028,
      "roma": "POSTALESIO",
      "rm": "SO",
      "1234": 403014053
    },
    {
      "1": 1029,
      "roma": "PORTO VIRO",
      "rm": "RO",
      "1234": 405029052
    },
    {
      "1": 1030,
      "roma": "PORTOBUFFOLE'",
      "rm": "TV",
      "1234": 405026060
    },
    {
      "1": 1031,
      "roma": "PORTOCANNONE",
      "rm": "CB",
      "1234": 414070055
    },
    {
      "1": 1032,
      "roma": "PORTOFERRAIO",
      "rm": "LI",
      "1234": 409049014
    },
    {
      "1": 1033,
      "roma": "PORTE",
      "rm": "TO",
      "1234": 401001200
    },
    {
      "1": 1034,
      "roma": "PORTICI",
      "rm": "NA",
      "1234": 415063059
    },
    {
      "1": 1035,
      "roma": "PORTICO DI CASERTA",
      "rm": "CE",
      "1234": 415061062
    },
    {
      "1": 1036,
      "roma": "PORTICO E SAN BENEDETTO",
      "rm": "FO",
      "1234": 408040031
    },
    {
      "1": 1037,
      "roma": "PORTE DI RENDENA",
      "rm": "TN",
      "1234": 404022244
    },
    {
      "1": 1038,
      "roma": "PORTIGLIOLA",
      "rm": "RC",
      "1234": 418080062
    },
    {
      "1": 1039,
      "roma": "POZZAGLIA SABINO",
      "rm": "RI",
      "1234": 412057058
    },
    {
      "1": 1040,
      "roma": "POZZAGLIO ED UNITI",
      "rm": "CR",
      "1234": 403019077
    },
    {
      "1": 1041,
      "roma": "POZZALLO",
      "rm": "RG",
      "1234": 419088008
    },
    {
      "1": 1042,
      "roma": "POZZILLI",
      "rm": "IS",
      "1234": 414094038
    },
    {
      "1": 1043,
      "roma": "POZZO D'ADDA",
      "rm": "MI",
      "1234": 403015177
    },
    {
      "1": 1044,
      "roma": "POZZOL GROPPO",
      "rm": "AL",
      "1234": 401006137
    },
    {
      "1": 1045,
      "roma": "POZZOLENGO",
      "rm": "BS",
      "1234": 403017151
    },
    {
      "1": 1046,
      "roma": "POSTIGLIONE",
      "rm": "SA",
      "1234": 415065101
    },
    {
      "1": 1047,
      "roma": "POSTUA",
      "rm": "VC",
      "1234": 401002102
    },
    {
      "1": 1048,
      "roma": "POTENZA",
      "rm": "PZ",
      "1234": 417076063
    },
    {
      "1": 1049,
      "roma": "PORTOFINO",
      "rm": "GE",
      "1234": 407010044
    },
    {
      "1": 1050,
      "roma": "PORTOGRUARO",
      "rm": "VE",
      "1234": 405027029
    },
    {
      "1": 1051,
      "roma": "PORTOMAGGIORE",
      "rm": "FE",
      "1234": 408038019
    },
    {
      "1": 1052,
      "roma": "PORTOPALO DI CAPO PASSERO",
      "rm": "SR",
      "1234": 419089020
    },
    {
      "1": 1053,
      "roma": "PORTOSCUSO",
      "rm": "CA",
      "1234": 420092049
    },
    {
      "1": 1054,
      "roma": "PORTO SAN GIORGIO",
      "rm": "FM",
      "1234": 411109033
    },
    {
      "1": 1055,
      "roma": "PORTOVENERE",
      "rm": "SP",
      "1234": 407011022
    },
    {
      "1": 1056,
      "roma": "PRAIA A MARE",
      "rm": "CS",
      "1234": 418078101
    },
    {
      "1": 1057,
      "roma": "PRAIANO",
      "rm": "SA",
      "1234": 415065102
    },
    {
      "1": 1058,
      "roma": "PRALBOINO",
      "rm": "BS",
      "1234": 403017152
    },
    {
      "1": 1059,
      "roma": "PRALI",
      "rm": "TO",
      "1234": 401001202
    },
    {
      "1": 1060,
      "roma": "PRALORMO",
      "rm": "TO",
      "1234": 401001203
    },
    {
      "1": 1061,
      "roma": "PRALUNGO",
      "rm": "BI",
      "1234": 401096049
    },
    {
      "1": 1062,
      "roma": "PRAMAGGIORE",
      "rm": "VE",
      "1234": 405027030
    },
    {
      "1": 1063,
      "roma": "PRAMOLLO",
      "rm": "TO",
      "1234": 401001204
    },
    {
      "1": 1064,
      "roma": "PRAROLO",
      "rm": "VC",
      "1234": 401002104
    },
    {
      "1": 1065,
      "roma": "PRAROSTINO",
      "rm": "TO",
      "1234": 401001205
    },
    {
      "1": 1066,
      "roma": "POZZOLEONE",
      "rm": "VI",
      "1234": 405024082
    },
    {
      "1": 1067,
      "roma": "POZZOLO FORMIGARO",
      "rm": "AL",
      "1234": 401006138
    },
    {
      "1": 1068,
      "roma": "POZZOMAGGIORE",
      "rm": "SS",
      "1234": 420090059
    },
    {
      "1": 1069,
      "roma": "POZZONOVO",
      "rm": "PD",
      "1234": 405028070
    },
    {
      "1": 1070,
      "roma": "POZZUOLI",
      "rm": "NA",
      "1234": 415063060
    },
    {
      "1": 1071,
      "roma": "POTENZA PICENA",
      "rm": "MC",
      "1234": 411043043
    },
    {
      "1": 1072,
      "roma": "POVE DEL GRAPPA",
      "rm": "VI",
      "1234": 405024081
    },
    {
      "1": 1073,
      "roma": "POVEGLIANO",
      "rm": "TV",
      "1234": 405026062
    },
    {
      "1": 1074,
      "roma": "POVEGLIANO VERONESE",
      "rm": "VR",
      "1234": 405023060
    },
    {
      "1": 1075,
      "roma": "POVIGLIO",
      "rm": "RE",
      "1234": 408035029
    },
    {
      "1": 1076,
      "roma": "POVOLETTO",
      "rm": "UD",
      "1234": 406030078
    },
    {
      "1": 1077,
      "roma": "PRATO ALLO STELVIO",
      "rm": "BZ",
      "1234": 404021067
    },
    {
      "1": 1078,
      "roma": "PRATO CARNICO",
      "rm": "UD",
      "1234": 406030081
    },
    {
      "1": 1079,
      "roma": "PRATO SESIA",
      "rm": "NO",
      "1234": 401003122
    },
    {
      "1": 1080,
      "roma": "PRATOLA PELIGNA",
      "rm": "AQ",
      "1234": 413066075
    },
    {
      "1": 1081,
      "roma": "PRATOLA SERRA",
      "rm": "AV",
      "1234": 415064075
    },
    {
      "1": 1082,
      "roma": "PRAVISDOMINI",
      "rm": "PN",
      "1234": 406093035
    },
    {
      "1": 1083,
      "roma": "PRASCO",
      "rm": "AL",
      "1234": 401006139
    },
    {
      "1": 1084,
      "roma": "PRASCORSANO",
      "rm": "TO",
      "1234": 401001206
    },
    {
      "1": 1085,
      "roma": "PRATA CAMPORTACCIO",
      "rm": "SO",
      "1234": 403014054
    },
    {
      "1": 1086,
      "roma": "PRATA D'ANSIDONIA",
      "rm": "AQ",
      "1234": 413066074
    },
    {
      "1": 1087,
      "roma": "POZZUOLO DEL FRIULI",
      "rm": "UD",
      "1234": 406030079
    },
    {
      "1": 1088,
      "roma": "POZZUOLO MARTESANA",
      "rm": "MI",
      "1234": 403015178
    },
    {
      "1": 1089,
      "roma": "PRADALUNGA",
      "rm": "BG",
      "1234": 403016173
    },
    {
      "1": 1090,
      "roma": "PRADAMANO",
      "rm": "UD",
      "1234": 406030080
    },
    {
      "1": 1091,
      "roma": "PRADLEVES",
      "rm": "CN",
      "1234": 401004173
    },
    {
      "1": 1092,
      "roma": "PRAGELATO",
      "rm": "TO",
      "1234": 401001201
    },
    {
      "1": 1093,
      "roma": "PRAY",
      "rm": "BI",
      "1234": 401096050
    },
    {
      "1": 1094,
      "roma": "PREGNANA MILANESE",
      "rm": "MI",
      "1234": 403015179
    },
    {
      "1": 1095,
      "roma": "PRELA'",
      "rm": "IM",
      "1234": 407008047
    },
    {
      "1": 1096,
      "roma": "PRATOVECCHIO STIA",
      "rm": "AR",
      "1234": 409051041
    },
    {
      "1": 1097,
      "roma": "PREMANA",
      "rm": "LC",
      "1234": 403097069
    },
    {
      "1": 1098,
      "roma": "PREMARIACCO",
      "rm": "UD",
      "1234": 406030083
    },
    {
      "1": 1099,
      "roma": "PREMENO",
      "rm": "VB",
      "1234": 401103055
    },
    {
      "1": 1100,
      "roma": "PREMIA",
      "rm": "VB",
      "1234": 401103056
    },
    {
      "1": 1101,
      "roma": "PRAZZO",
      "rm": "CN",
      "1234": 401004174
    },
    {
      "1": 1102,
      "roma": "PRE'-SAINT-DIDIER",
      "rm": "AO",
      "1234": 402007053
    },
    {
      "1": 1103,
      "roma": "PRECENICCO",
      "rm": "UD",
      "1234": 406030082
    },
    {
      "1": 1104,
      "roma": "PRECI",
      "rm": "PG",
      "1234": 410054043
    },
    {
      "1": 1105,
      "roma": "PRATA DI PORDENONE",
      "rm": "PN",
      "1234": 406093034
    },
    {
      "1": 1106,
      "roma": "PRATA DI PRINCIPATO ULTRA",
      "rm": "AV",
      "1234": 415064074
    },
    {
      "1": 1107,
      "roma": "PRATA SANNITA",
      "rm": "CE",
      "1234": 415061063
    },
    {
      "1": 1108,
      "roma": "PRATELLA",
      "rm": "CE",
      "1234": 415061064
    },
    {
      "1": 1109,
      "roma": "PRATIGLIONE",
      "rm": "TO",
      "1234": 401001207
    },
    {
      "1": 1110,
      "roma": "PRATO",
      "rm": "PO",
      "1234": 409100005
    },
    {
      "1": 1111,
      "roma": "PRESICCE",
      "rm": "LE",
      "1234": 416075062
    },
    {
      "1": 1112,
      "roma": "PRESSANA",
      "rm": "VR",
      "1234": 405023061
    },
    {
      "1": 1113,
      "roma": "PRETORO",
      "rm": "CH",
      "1234": 413069069
    },
    {
      "1": 1114,
      "roma": "PREVALLE",
      "rm": "BS",
      "1234": 403017155
    },
    {
      "1": 1115,
      "roma": "PREZZA",
      "rm": "AQ",
      "1234": 413066076
    },
    {
      "1": 1116,
      "roma": "PREMILCUORE",
      "rm": "FO",
      "1234": 408040033
    },
    {
      "1": 1117,
      "roma": "PREMOLO",
      "rm": "BG",
      "1234": 403016175
    },
    {
      "1": 1118,
      "roma": "PREMOSELLO-CHIOVENDA",
      "rm": "VB",
      "1234": 401103057
    },
    {
      "1": 1119,
      "roma": "PREDAPPIO",
      "rm": "FO",
      "1234": 408040032
    },
    {
      "1": 1120,
      "roma": "PREDAZZO",
      "rm": "TN",
      "1234": 404022147
    },
    {
      "1": 1121,
      "roma": "PREDOI",
      "rm": "BZ",
      "1234": 404021068
    },
    {
      "1": 1122,
      "roma": "PREDORE",
      "rm": "BG",
      "1234": 403016174
    },
    {
      "1": 1123,
      "roma": "PREDOSA",
      "rm": "AL",
      "1234": 401006140
    },
    {
      "1": 1124,
      "roma": "PREGANZIOL",
      "rm": "TV",
      "1234": 405026063
    },
    {
      "1": 1125,
      "roma": "PRIOLO GARGALLO",
      "rm": "SR",
      "1234": 419089021
    },
    {
      "1": 1126,
      "roma": "PRIVERNO",
      "rm": "LT",
      "1234": 412059019
    },
    {
      "1": 1127,
      "roma": "PRIZZI",
      "rm": "PA",
      "1234": 419082060
    },
    {
      "1": 1128,
      "roma": "PROCENO",
      "rm": "VT",
      "1234": 412056044
    },
    {
      "1": 1129,
      "roma": "PROCIDA",
      "rm": "NA",
      "1234": 415063061
    },
    {
      "1": 1130,
      "roma": "PROPATA",
      "rm": "GE",
      "1234": 407010045
    },
    {
      "1": 1131,
      "roma": "PROSERPIO",
      "rm": "CO",
      "1234": 403013192
    },
    {
      "1": 1132,
      "roma": "PRIMIERO SAN MARTINO DI CASTROZZA",
      "rm": "TN",
      "1234": 404022245
    },
    {
      "1": 1133,
      "roma": "PROSSEDI",
      "rm": "LT",
      "1234": 412059020
    },
    {
      "1": 1134,
      "roma": "PROVAGLIO D'ISEO",
      "rm": "BS",
      "1234": 403017156
    },
    {
      "1": 1135,
      "roma": "PRIERO",
      "rm": "CN",
      "1234": 401004175
    },
    {
      "1": 1136,
      "roma": "PRIGNANO CILENTO",
      "rm": "SA",
      "1234": 415065103
    },
    {
      "1": 1137,
      "roma": "PRIGNANO SULLA SECCHIA",
      "rm": "MO",
      "1234": 408036033
    },
    {
      "1": 1138,
      "roma": "PRIMALUNA",
      "rm": "LC",
      "1234": 403097070
    },
    {
      "1": 1139,
      "roma": "PREONE",
      "rm": "UD",
      "1234": 406030084
    },
    {
      "1": 1140,
      "roma": "PREPOTTO",
      "rm": "UD",
      "1234": 406030085
    },
    {
      "1": 1141,
      "roma": "PRESEGLIE",
      "rm": "BS",
      "1234": 403017153
    },
    {
      "1": 1142,
      "roma": "PRESENZANO",
      "rm": "CE",
      "1234": 415061065
    },
    {
      "1": 1143,
      "roma": "PRESEZZO",
      "rm": "BG",
      "1234": 403016176
    },
    {
      "1": 1144,
      "roma": "PULSANO",
      "rm": "TA",
      "1234": 416073022
    },
    {
      "1": 1145,
      "roma": "PUMENENGO",
      "rm": "BG",
      "1234": 403016177
    },
    {
      "1": 1146,
      "roma": "PUSIANO",
      "rm": "CO",
      "1234": 403013193
    },
    {
      "1": 1147,
      "roma": "PUTIFIGARI",
      "rm": "SS",
      "1234": 420090060
    },
    {
      "1": 1148,
      "roma": "PUTIGNANO",
      "rm": "BA",
      "1234": 416072036
    },
    {
      "1": 1149,
      "roma": "QUADRELLE",
      "rm": "AV",
      "1234": 415064076
    },
    {
      "1": 1150,
      "roma": "RAPAGNANO",
      "rm": "FM",
      "1234": 411109035
    },
    {
      "1": 1151,
      "roma": "QUADRI",
      "rm": "CH",
      "1234": 413069070
    },
    {
      "1": 1152,
      "roma": "PROVAGLIO VAL SABBIA",
      "rm": "BS",
      "1234": 403017157
    },
    {
      "1": 1153,
      "roma": "PROVES",
      "rm": "BZ",
      "1234": 404021069
    },
    {
      "1": 1154,
      "roma": "PROVVIDENTI",
      "rm": "CB",
      "1234": 414070056
    },
    {
      "1": 1155,
      "roma": "PRIOCCA",
      "rm": "CN",
      "1234": 401004176
    },
    {
      "1": 1156,
      "roma": "PRIOLA",
      "rm": "CN",
      "1234": 401004177
    },
    {
      "1": 1157,
      "roma": "PULFERO",
      "rm": "UD",
      "1234": 406030086
    },
    {
      "1": 1158,
      "roma": "QUARTO",
      "rm": "NA",
      "1234": 415063063
    },
    {
      "1": 1159,
      "roma": "QUARTO D'ALTINO",
      "rm": "VE",
      "1234": 405027031
    },
    {
      "1": 1160,
      "roma": "QUARTU SANT'ELENA",
      "rm": "CA",
      "1234": 420092051
    },
    {
      "1": 1161,
      "roma": "QUARTUCCIU",
      "rm": "CA",
      "1234": 420092105
    },
    {
      "1": 1162,
      "roma": "QUASSOLO",
      "rm": "TO",
      "1234": 401001209
    },
    {
      "1": 1163,
      "roma": "QUAGLIUZZO",
      "rm": "TO",
      "1234": 401001208
    },
    {
      "1": 1164,
      "roma": "QUALIANO",
      "rm": "NA",
      "1234": 415063062
    },
    {
      "1": 1165,
      "roma": "QUARANTI",
      "rm": "AT",
      "1234": 401005088
    },
    {
      "1": 1166,
      "roma": "PRUNETTO",
      "rm": "CN",
      "1234": 401004178
    },
    {
      "1": 1167,
      "roma": "PUEGNAGO SUL GARDA",
      "rm": "BS",
      "1234": 403017158
    },
    {
      "1": 1168,
      "roma": "PUGLIANELLO",
      "rm": "BN",
      "1234": 415062055
    },
    {
      "1": 1169,
      "roma": "PULA",
      "rm": "CA",
      "1234": 420092050
    },
    {
      "1": 1170,
      "roma": "QUARRATA",
      "rm": "PT",
      "1234": 409047017
    },
    {
      "1": 1171,
      "roma": "QUART",
      "rm": "AO",
      "1234": 402007054
    },
    {
      "1": 1172,
      "roma": "QUINTO VICENTINO",
      "rm": "VI",
      "1234": 405024083
    },
    {
      "1": 1173,
      "roma": "QUINZANO D'OGLIO",
      "rm": "BS",
      "1234": 403017159
    },
    {
      "1": 1174,
      "roma": "QUISTELLO",
      "rm": "MN",
      "1234": 403020047
    },
    {
      "1": 1175,
      "roma": "RONCO BRIANTINO",
      "rm": "MB",
      "1234": 403108038
    },
    {
      "1": 1176,
      "roma": "QUATTORDIO",
      "rm": "AL",
      "1234": 401006142
    },
    {
      "1": 1177,
      "roma": "QUATTRO CASTELLA",
      "rm": "RE",
      "1234": 408035030
    },
    {
      "1": 1178,
      "roma": "QUILIANO",
      "rm": "SV",
      "1234": 407009052
    },
    {
      "1": 1179,
      "roma": "QUINCINETTO",
      "rm": "TO",
      "1234": 401001210
    },
    {
      "1": 1180,
      "roma": "QUARGNENTO",
      "rm": "AL",
      "1234": 401006141
    },
    {
      "1": 1181,
      "roma": "QUARNA SOPRA",
      "rm": "VB",
      "1234": 401103058
    },
    {
      "1": 1182,
      "roma": "QUARNA SOTTO",
      "rm": "VB",
      "1234": 401103059
    },
    {
      "1": 1183,
      "roma": "QUARONA",
      "rm": "VC",
      "1234": 401002107
    },
    {
      "1": 1184,
      "roma": "QUINTO VERCELLESE",
      "rm": "VC",
      "1234": 401002108
    },
    {
      "1": 1185,
      "roma": "RAGOGNA",
      "rm": "UD",
      "1234": 406030087
    },
    {
      "1": 1186,
      "roma": "RAGUSA",
      "rm": "RG",
      "1234": 419088009
    },
    {
      "1": 1187,
      "roma": "RAIANO",
      "rm": "AQ",
      "1234": 413066077
    },
    {
      "1": 1188,
      "roma": "RAMACCA",
      "rm": "CT",
      "1234": 419087037
    },
    {
      "1": 1189,
      "roma": "RABBI",
      "rm": "TN",
      "1234": 404022150
    },
    {
      "1": 1190,
      "roma": "RACALE",
      "rm": "LE",
      "1234": 416075063
    },
    {
      "1": 1191,
      "roma": "QUERO VAS",
      "rm": "BL",
      "1234": 405025070
    },
    {
      "1": 1192,
      "roma": "RACALMUTO",
      "rm": "AG",
      "1234": 419084029
    },
    {
      "1": 1193,
      "roma": "RACCONIGI",
      "rm": "CN",
      "1234": 401004179
    },
    {
      "1": 1194,
      "roma": "RACCUJA",
      "rm": "ME",
      "1234": 419083069
    },
    {
      "1": 1195,
      "roma": "QUINDICI",
      "rm": "AV",
      "1234": 415064077
    },
    {
      "1": 1196,
      "roma": "QUINGENTOLE",
      "rm": "MN",
      "1234": 403020046
    },
    {
      "1": 1197,
      "roma": "QUINTANO",
      "rm": "CR",
      "1234": 403019078
    },
    {
      "1": 1198,
      "roma": "QUINTO DI TREVISO",
      "rm": "TV",
      "1234": 405026064
    },
    {
      "1": 1199,
      "roma": "RAFFADALI",
      "rm": "AG",
      "1234": 419084030
    },
    {
      "1": 1200,
      "roma": "RAGALNA",
      "rm": "CT",
      "1234": 419087058
    },
    {
      "1": 1201,
      "roma": "RAPOLANO TERME",
      "rm": "SI",
      "1234": 409052026
    },
    {
      "1": 1202,
      "roma": "RAPOLLA",
      "rm": "PZ",
      "1234": 417076064
    },
    {
      "1": 1203,
      "roma": "RAPONE",
      "rm": "PZ",
      "1234": 417076065
    },
    {
      "1": 1204,
      "roma": "RASSA",
      "rm": "VC",
      "1234": 401002110
    },
    {
      "1": 1205,
      "roma": "RASUN ANTERSELVA",
      "rm": "BZ",
      "1234": 404021071
    },
    {
      "1": 1206,
      "roma": "RASURA",
      "rm": "SO",
      "1234": 403014055
    },
    {
      "1": 1207,
      "roma": "RANCIO VALCUVIA",
      "rm": "VA",
      "1234": 403012115
    },
    {
      "1": 1208,
      "roma": "RANCO",
      "rm": "VA",
      "1234": 403012116
    },
    {
      "1": 1209,
      "roma": "RANDAZZO",
      "rm": "CT",
      "1234": 419087038
    },
    {
      "1": 3065,
      "roma": "VITTUONE",
      "rm": "MI",
      "1234": 403015243
    },
    {
      "1": 3066,
      "roma": "VITULANO",
      "rm": "BN",
      "1234": 415062077
    },
    {
      "1": 3067,
      "roma": "VITULAZIO",
      "rm": "CE",
      "1234": 415061100
    },
    {
      "1": 3068,
      "roma": "VIU'",
      "rm": "TO",
      "1234": 401001313
    },
    {
      "1": 3069,
      "roma": "VIVARO",
      "rm": "PN",
      "1234": 406093050
    },
    {
      "1": 3070,
      "roma": "VIVARO ROMANO",
      "rm": "RM",
      "1234": 412058113
    },
    {
      "1": 3071,
      "roma": "VIVERONE",
      "rm": "BI",
      "1234": 401096080
    },
    {
      "1": 3072,
      "roma": "VIZZINI",
      "rm": "CT",
      "1234": 419087054
    },
    {
      "1": 3073,
      "roma": "VOLTERRA",
      "rm": "PI",
      "1234": 409050039
    },
    {
      "1": 3074,
      "roma": "VIZZOLO PREDABISSI",
      "rm": "MI",
      "1234": 403015244
    },
    {
      "1": 3075,
      "roma": "VO",
      "rm": "PD",
      "1234": 405028105
    },
    {
      "1": 3076,
      "roma": "VOBARNO",
      "rm": "BS",
      "1234": 403017204
    },
    {
      "1": 3077,
      "roma": "VOBBIA",
      "rm": "GE",
      "1234": 407010066
    },
    {
      "1": 3078,
      "roma": "VOCCA",
      "rm": "VC",
      "1234": 401002166
    },
    {
      "1": 3079,
      "roma": "VODO DI CADORE",
      "rm": "BL",
      "1234": 405025066
    },
    {
      "1": 3080,
      "roma": "VOGHERA",
      "rm": "PV",
      "1234": 403018182
    },
    {
      "1": 3081,
      "roma": "VOGHIERA",
      "rm": "FE",
      "1234": 408038023
    },
    {
      "1": 3082,
      "roma": "VITTORIA",
      "rm": "RG",
      "1234": 419088012
    },
    {
      "1": 3083,
      "roma": "VOLANO",
      "rm": "TN",
      "1234": 404022224
    },
    {
      "1": 3084,
      "roma": "VOLLA",
      "rm": "NA",
      "1234": 415063089
    },
    {
      "1": 3085,
      "roma": "VOLONGO",
      "rm": "CR",
      "1234": 403019114
    },
    {
      "1": 3086,
      "roma": "VOLPAGO DEL MONTELLO",
      "rm": "TV",
      "1234": 405026093
    },
    {
      "1": 3087,
      "roma": "VOLPARA",
      "rm": "PV",
      "1234": 403018183
    },
    {
      "1": 3088,
      "roma": "VOLPEDO",
      "rm": "AL",
      "1234": 401006188
    },
    {
      "1": 3089,
      "roma": "VOLPEGLINO",
      "rm": "AL",
      "1234": 401006189
    },
    {
      "1": 3090,
      "roma": "VOLPIANO",
      "rm": "TO",
      "1234": 401001314
    },
    {
      "1": 3091,
      "roma": "VOLTA MANTOVANA",
      "rm": "MN",
      "1234": 403020070
    },
    {
      "1": 3092,
      "roma": "VOLTAGGIO",
      "rm": "AL",
      "1234": 401006190
    },
    {
      "1": 3093,
      "roma": "VOLTAGO AGORDINO",
      "rm": "BL",
      "1234": 405025067
    },
    {
      "1": 3094,
      "roma": "VOLTIDO",
      "rm": "CR",
      "1234": 403019115
    },
    {
      "1": 3095,
      "roma": "VOLTURARA APPULA",
      "rm": "FG",
      "1234": 416071061
    },
    {
      "1": 3096,
      "roma": "VOLTURARA IRPINA",
      "rm": "AV",
      "1234": 415064119
    },
    {
      "1": 3097,
      "roma": "VOLTURINO",
      "rm": "FG",
      "1234": 416071062
    },
    {
      "1": 3098,
      "roma": "VOLVERA",
      "rm": "TO",
      "1234": 401001315
    },
    {
      "1": 3099,
      "roma": "VOTTIGNASCO",
      "rm": "CN",
      "1234": 401004250
    },
    {
      "1": 3100,
      "roma": "VOGOGNA",
      "rm": "VB",
      "1234": 401103077
    },
    {
      "1": 3101,
      "roma": "ZACCANOPOLI",
      "rm": "VV",
      "1234": 418102048
    },
    {
      "1": 3102,
      "roma": "ZAFFERANA ETNEA",
      "rm": "CT",
      "1234": 419087055
    },
    {
      "1": 3103,
      "roma": "ZAGARISE",
      "rm": "CZ",
      "1234": 418079157
    },
    {
      "1": 3104,
      "roma": "ZAGAROLO",
      "rm": "RM",
      "1234": 412058114
    },
    {
      "1": 3105,
      "roma": "ZAMBRONE",
      "rm": "VV",
      "1234": 418102049
    },
    {
      "1": 3106,
      "roma": "ZANDOBBIO",
      "rm": "BG",
      "1234": 403016244
    },
    {
      "1": 3107,
      "roma": "ZANE'",
      "rm": "VI",
      "1234": 405024119
    },
    {
      "1": 3108,
      "roma": "ZERBO",
      "rm": "PV",
      "1234": 403018188
    },
    {
      "1": 3109,
      "roma": "ZANICA",
      "rm": "BG",
      "1234": 403016245
    },
    {
      "1": 3110,
      "roma": "ZAPPONETA",
      "rm": "FG",
      "1234": 416071064
    },
    {
      "1": 3111,
      "roma": "ZAVATTARELLO",
      "rm": "PV",
      "1234": 403018184
    },
    {
      "1": 3112,
      "roma": "ZECCONE",
      "rm": "PV",
      "1234": 403018185
    },
    {
      "1": 3113,
      "roma": "ZELBIO",
      "rm": "CO",
      "1234": 403013246
    },
    {
      "1": 3114,
      "roma": "ZELO BUON PERSICO",
      "rm": "LO",
      "1234": 403098061
    },
    {
      "1": 3115,
      "roma": "ZEME",
      "rm": "PV",
      "1234": 403018186
    },
    {
      "1": 3116,
      "roma": "ZENEVREDO",
      "rm": "PV",
      "1234": 403018187
    },
    {
      "1": 3117,
      "roma": "ZENSON DI PIAVE",
      "rm": "TV",
      "1234": 405026094
    },
    {
      "1": 3118,
      "roma": "ZERBA",
      "rm": "PC",
      "1234": 408033047
    },
    {
      "1": 3119,
      "roma": "ZOLLINO",
      "rm": "LE",
      "1234": 416075094
    },
    {
      "1": 3120,
      "roma": "ZERBOLO'",
      "rm": "PV",
      "1234": 403018189
    },
    {
      "1": 3121,
      "roma": "ZERFALIU",
      "rm": "OR",
      "1234": 420095075
    },
    {
      "1": 3122,
      "roma": "ZERI",
      "rm": "MS",
      "1234": 409045017
    },
    {
      "1": 3123,
      "roma": "ZERMEGHEDO",
      "rm": "VI",
      "1234": 405024120
    },
    {
      "1": 3124,
      "roma": "ZERO BRANCO",
      "rm": "TV",
      "1234": 405026095
    },
    {
      "1": 3125,
      "roma": "ZEVIO",
      "rm": "VR",
      "1234": 405023097
    },
    {
      "1": 3126,
      "roma": "ZIANO DI FIEMME",
      "rm": "TN",
      "1234": 404022226
    },
    {
      "1": 3127,
      "roma": "ZIANO PIACENTINO",
      "rm": "PC",
      "1234": 408033048
    },
    {
      "1": 3128,
      "roma": "ZEDDIANI",
      "rm": "OR",
      "1234": 420095074
    },
    {
      "1": 3129,
      "roma": "ZIBIDO SAN GIACOMO",
      "rm": "MI",
      "1234": 403015247
    },
    {
      "1": 3130,
      "roma": "ZIGNAGO",
      "rm": "SP",
      "1234": 407011032
    },
    {
      "1": 3131,
      "roma": "ZIMELLA",
      "rm": "VR",
      "1234": 405023098
    },
    {
      "1": 3132,
      "roma": "ZIMONE",
      "rm": "BI",
      "1234": 401096081
    },
    {
      "1": 3133,
      "roma": "ZINASCO",
      "rm": "PV",
      "1234": 403018190
    },
    {
      "1": 3134,
      "roma": "ZOAGLI",
      "rm": "GE",
      "1234": 407010067
    },
    {
      "1": 3135,
      "roma": "ZOCCA",
      "rm": "MO",
      "1234": 408036047
    },
    {
      "1": 3136,
      "roma": "ZOGNO",
      "rm": "BG",
      "1234": 403016246
    },
    {
      "1": 3137,
      "roma": "ZOLA PREDOSA",
      "rm": "BO",
      "1234": 408037060
    },
    {
      "1": 3138,
      "roma": "ZUGLIO",
      "rm": "UD",
      "1234": 406030136
    },
    {
      "1": 3139,
      "roma": "ZONE",
      "rm": "BS",
      "1234": 403017205
    },
    {
      "1": 3140,
      "roma": "ZOPPE' DI CADORE",
      "rm": "BL",
      "1234": 405025069
    },
    {
      "1": 3141,
      "roma": "ZOPPOLA",
      "rm": "PN",
      "1234": 406093051
    },
    {
      "1": 3142,
      "roma": "ZOVENCEDO",
      "rm": "VI",
      "1234": 405024121
    },
    {
      "1": 3143,
      "roma": "ZUCCARELLO",
      "rm": "SV",
      "1234": 407009069
    },
    {
      "1": 3144,
      "roma": "ZUGLIANO",
      "rm": "VI",
      "1234": 405024122
    },
    {
      "1": 3145,
      "roma": "TERRE ROVERESCHE",
      "rm": "PS",
      "1234": 411041070
    },
    {
      "1": 3146,
      "roma": "VALFORNACE",
      "rm": "MC",
      "1234": 411043058
    },
    {
      "1": 3147,
      "roma": "COLLI AL METAURO",
      "rm": "PS",
      "1234": 411041069
    },
    {
      "1": 3148,
      "roma": "ALTA VALLE INTELVI",
      "rm": "CO",
      "1234": 403013253
    },
    {
      "1": 3149,
      "roma": "TERRE DEL RENO",
      "rm": "FE",
      "1234": 408038028
    },
    {
      "1": 3150,
      "roma": "ABETONE CUTIGLIANO",
      "rm": "PT",
      "1234": 409047023
    },
    {
      "1": 3151,
      "roma": "VAL LIONA",
      "rm": "VI",
      "1234": 405024123
    },
    {
      "1": 3152,
      "roma": "CASALI DEL MANCO",
      "rm": "CS",
      "1234": 418078156
    },
    {
      "1": 3153,
      "roma": "LUNI",
      "rm": "SP",
      "1234": 407011920
    },
    {
      "1": 3154,
      "roma": "ZUMAGLIA",
      "rm": "BI",
      "1234": 401096083
    },
    {
      "1": 3155,
      "roma": "ZUMPANO",
      "rm": "CS",
      "1234": 418078155
    },
    {
      "1": 3156,
      "roma": "ZUNGOLI",
      "rm": "AV",
      "1234": 415064120
    },
    {
      "1": 3157,
      "roma": "ZUNGRI",
      "rm": "VV",
      "1234": 418102050
    },
    {
      "1": 3158,
      "roma": "ZUBIENA",
      "rm": "BI",
      "1234": 401096082
    },
    {
      "1": 3159,
      "roma": "ABBASANTA",
      "rm": "OR",
      "1234": 420095001
    },
    {
      "1": 3160,
      "roma": "ABBATEGGIO",
      "rm": "PE",
      "1234": 413068001
    },
    {
      "1": 3161,
      "roma": "ABBIATEGRASSO",
      "rm": "MI",
      "1234": 403015002
    },
    {
      "1": 3162,
      "roma": "ABRIOLA",
      "rm": "PZ",
      "1234": 417076001
    },
    {
      "1": 3163,
      "roma": "ACCIANO",
      "rm": "AQ",
      "1234": 413066001
    },
    {
      "1": 3164,
      "roma": "ACCUMOLI",
      "rm": "RI",
      "1234": 412057001
    },
    {
      "1": 3165,
      "roma": "ACERENZA",
      "rm": "PZ",
      "1234": 417076002
    },
    {
      "1": 3166,
      "roma": "ACERNO",
      "rm": "SA",
      "1234": 415065001
    },
    {
      "1": 3167,
      "roma": "ACERRA",
      "rm": "NA",
      "1234": 415063001
    },
    {
      "1": 3168,
      "roma": "BARLASSINA",
      "rm": "MB",
      "1234": 403108005
    },
    {
      "1": 3169,
      "roma": "ACI BONACCORSI",
      "rm": "CT",
      "1234": 419087001
    },
    {
      "1": 3170,
      "roma": "ACI CASTELLO",
      "rm": "CT",
      "1234": 419087002
    },
    {
      "1": 3171,
      "roma": "ABANO TERME",
      "rm": "PD",
      "1234": 405028001
    },
    {
      "1": 3172,
      "roma": "ABBADIA CERRETO",
      "rm": "LO",
      "1234": 403098001
    },
    {
      "1": 3173,
      "roma": "ABBADIA LARIANA",
      "rm": "LC",
      "1234": 403097001
    },
    {
      "1": 3174,
      "roma": "ABBADIA SAN SALVATORE",
      "rm": "SI",
      "1234": 409052001
    },
    {
      "1": 3175,
      "roma": "ACQUAFREDDA",
      "rm": "BS",
      "1234": 403017001
    },
    {
      "1": 3176,
      "roma": "ACQUALAGNA",
      "rm": "PS",
      "1234": 411041001
    },
    {
      "1": 3177,
      "roma": "ACQUANEGRA CREMONESE",
      "rm": "CR",
      "1234": 403019001
    },
    {
      "1": 3178,
      "roma": "ACATE",
      "rm": "RG",
      "1234": 419088001
    },
    {
      "1": 3179,
      "roma": "ACCADIA",
      "rm": "FG",
      "1234": 416071001
    },
    {
      "1": 3180,
      "roma": "ACCEGLIO",
      "rm": "CN",
      "1234": 401004001
    },
    {
      "1": 3181,
      "roma": "ACCETTURA",
      "rm": "MT",
      "1234": 417077001
    },
    {
      "1": 3182,
      "roma": "ACQUARO",
      "rm": "VV",
      "1234": 418102001
    },
    {
      "1": 3183,
      "roma": "ACQUASANTA TERME",
      "rm": "AP",
      "1234": 411044001
    },
    {
      "1": 3184,
      "roma": "ACQUASPARTA",
      "rm": "TR",
      "1234": 410055001
    },
    {
      "1": 3185,
      "roma": "ACQUAVIVA COLLECROCE",
      "rm": "CB",
      "1234": 414070001
    },
    {
      "1": 3186,
      "roma": "ACQUAVIVA D'ISERNIA",
      "rm": "IS",
      "1234": 414094001
    },
    {
      "1": 3187,
      "roma": "ACQUAVIVA DELLE FONTI",
      "rm": "BA",
      "1234": 416072001
    },
    {
      "1": 3188,
      "roma": "BARLETTA",
      "rm": "BT",
      "1234": 416110002
    },
    {
      "1": 3189,
      "roma": "ACI CATENA",
      "rm": "CT",
      "1234": 419087003
    },
    {
      "1": 3190,
      "roma": "ACI SANT'ANTONIO",
      "rm": "CT",
      "1234": 419087005
    },
    {
      "1": 3191,
      "roma": "ACIREALE",
      "rm": "CT",
      "1234": 419087004
    },
    {
      "1": 3192,
      "roma": "ACQUAFONDATA",
      "rm": "FR",
      "1234": 412060001
    },
    {
      "1": 3193,
      "roma": "ACQUAFORMOSA",
      "rm": "CS",
      "1234": 418078001
    },
    {
      "1": 3194,
      "roma": "ADRANO",
      "rm": "CT",
      "1234": 419087006
    },
    {
      "1": 3195,
      "roma": "ADRARA SAN MARTINO",
      "rm": "BG",
      "1234": 403016001
    },
    {
      "1": 3196,
      "roma": "ADRARA SAN ROCCO",
      "rm": "BG",
      "1234": 403016002
    },
    {
      "1": 3197,
      "roma": "ADRIA",
      "rm": "RO",
      "1234": 405029001
    },
    {
      "1": 3198,
      "roma": "ACQUANEGRA SUL CHIESE",
      "rm": "MN",
      "1234": 403020001
    },
    {
      "1": 3199,
      "roma": "ACQUAPENDENTE",
      "rm": "VT",
      "1234": 412056001
    },
    {
      "1": 3200,
      "roma": "ACQUAPPESA",
      "rm": "CS",
      "1234": 418078002
    },
    {
      "1": 3201,
      "roma": "ACQUARICA DEL CAPO",
      "rm": "LE",
      "1234": 416075001
    },
    {
      "1": 3202,
      "roma": "AFRAGOLA",
      "rm": "NA",
      "1234": 415063002
    },
    {
      "1": 3203,
      "roma": "AFRICO",
      "rm": "RC",
      "1234": 418080001
    },
    {
      "1": 3204,
      "roma": "AGAZZANO",
      "rm": "PC",
      "1234": 408033001
    },
    {
      "1": 3205,
      "roma": "AGEROLA",
      "rm": "NA",
      "1234": 415063003
    },
    {
      "1": 3206,
      "roma": "ACQUAVIVA PICENA",
      "rm": "AP",
      "1234": 411044002
    },
    {
      "1": 3207,
      "roma": "ACQUAVIVA PLATANI",
      "rm": "CL",
      "1234": 419085001
    },
    {
      "1": 3208,
      "roma": "ACQUEDOLCI",
      "rm": "ME",
      "1234": 419083107
    },
    {
      "1": 3209,
      "roma": "ACQUI TERME",
      "rm": "AL",
      "1234": 401006001
    },
    {
      "1": 3210,
      "roma": "ACRI",
      "rm": "CS",
      "1234": 418078003
    },
    {
      "1": 3211,
      "roma": "ACUTO",
      "rm": "FR",
      "1234": 412060002
    },
    {
      "1": 3212,
      "roma": "ADELFIA",
      "rm": "BA",
      "1234": 416072002
    },
    {
      "1": 3213,
      "roma": "AGLIENTU",
      "rm": "SS",
      "1234": 420090062
    },
    {
      "1": 3214,
      "roma": "AGNA",
      "rm": "PD",
      "1234": 405028002
    },
    {
      "1": 3215,
      "roma": "AGNADELLO",
      "rm": "CR",
      "1234": 403019002
    },
    {
      "1": 3216,
      "roma": "ADRO",
      "rm": "BS",
      "1234": 403017002
    },
    {
      "1": 3217,
      "roma": "AFFI",
      "rm": "VR",
      "1234": 405023001
    },
    {
      "1": 3218,
      "roma": "AFFILE",
      "rm": "RM",
      "1234": 412058001
    },
    {
      "1": 3219,
      "roma": "AGNOSINE",
      "rm": "BS",
      "1234": 403017003
    },
    {
      "1": 3220,
      "roma": "AGORDO",
      "rm": "BL",
      "1234": 405025001
    },
    {
      "1": 3221,
      "roma": "AGOSTA",
      "rm": "RM",
      "1234": 412058002
    },
    {
      "1": 3222,
      "roma": "AGRA",
      "rm": "VA",
      "1234": 403012001
    },
    {
      "1": 3223,
      "roma": "AGRATE CONTURBIA",
      "rm": "NO",
      "1234": 401003001
    },
    {
      "1": 3224,
      "roma": "AGRIGENTO",
      "rm": "AG",
      "1234": 419084001
    },
    {
      "1": 3225,
      "roma": "AGROPOLI",
      "rm": "SA",
      "1234": 415065002
    },
    {
      "1": 3226,
      "roma": "AGGIUS",
      "rm": "SS",
      "1234": 420090001
    },
    {
      "1": 3227,
      "roma": "BELMONTE PICENO",
      "rm": "FM",
      "1234": 411109003
    },
    {
      "1": 3228,
      "roma": "AGIRA",
      "rm": "EN",
      "1234": 419086001
    },
    {
      "1": 3229,
      "roma": "AGLIANA",
      "rm": "PT",
      "1234": 409047002
    },
    {
      "1": 3230,
      "roma": "AGLIANO TERME",
      "rm": "AT",
      "1234": 401005001
    },
    {
      "1": 3231,
      "roma": "AGLIE'",
      "rm": "TO",
      "1234": 401001001
    },
    {
      "1": 3232,
      "roma": "AIDONE",
      "rm": "EN",
      "1234": 419086002
    },
    {
      "1": 3233,
      "roma": "AGNANA CALABRA",
      "rm": "RC",
      "1234": 418080002
    },
    {
      "1": 3234,
      "roma": "AGNONE",
      "rm": "IS",
      "1234": 414094002
    },
    {
      "1": 3235,
      "roma": "AIETA",
      "rm": "CS",
      "1234": 418078005
    },
    {
      "1": 3236,
      "roma": "AILANO",
      "rm": "CE",
      "1234": 415061001
    },
    {
      "1": 3237,
      "roma": "AILOCHE",
      "rm": "BI",
      "1234": 401096001
    },
    {
      "1": 3238,
      "roma": "AIRASCA",
      "rm": "TO",
      "1234": 401001002
    },
    {
      "1": 3239,
      "roma": "AIROLA",
      "rm": "BN",
      "1234": 415062001
    },
    {
      "1": 3240,
      "roma": "AIROLE",
      "rm": "IM",
      "1234": 407008001
    },
    {
      "1": 3241,
      "roma": "AIRUNO",
      "rm": "LC",
      "1234": 403097002
    },
    {
      "1": 3242,
      "roma": "AISONE",
      "rm": "CN",
      "1234": 401004002
    },
    {
      "1": 3243,
      "roma": "ALA",
      "rm": "TN",
      "1234": 404022001
    },
    {
      "1": 3244,
      "roma": "AGUGLIANO",
      "rm": "AN",
      "1234": 411042001
    },
    {
      "1": 3245,
      "roma": "AGUGLIARO",
      "rm": "VI",
      "1234": 405024001
    },
    {
      "1": 3246,
      "roma": "AGRATE BRIANZA",
      "rm": "MB",
      "1234": 403108001
    },
    {
      "1": 3247,
      "roma": "AIDOMAGGIORE",
      "rm": "OR",
      "1234": 420095002
    },
    {
      "1": 3248,
      "roma": "ALASSIO",
      "rm": "SV",
      "1234": 407009001
    },
    {
      "1": 3249,
      "roma": "AIELLI",
      "rm": "AQ",
      "1234": 413066002
    },
    {
      "1": 3250,
      "roma": "AIELLO CALABRO",
      "rm": "CS",
      "1234": 418078004
    },
    {
      "1": 3251,
      "roma": "AIELLO DEL FRIULI",
      "rm": "UD",
      "1234": 406030001
    },
    {
      "1": 3252,
      "roma": "AIELLO DEL SABATO",
      "rm": "AV",
      "1234": 415064001
    },
    {
      "1": 3253,
      "roma": "ALBAGIARA",
      "rm": "OR",
      "1234": 420095003
    },
    {
      "1": 3254,
      "roma": "ALBAIRATE",
      "rm": "MI",
      "1234": 403015005
    },
    {
      "1": 3255,
      "roma": "ALBANELLA",
      "rm": "SA",
      "1234": 415065003
    },
    {
      "1": 3256,
      "roma": "ALBANO DI LUCANIA",
      "rm": "PZ",
      "1234": 417076003
    },
    {
      "1": 3257,
      "roma": "ALBANO LAZIALE",
      "rm": "RM",
      "1234": 412058003
    },
    {
      "1": 3258,
      "roma": "ALBANO SANT'ALESSANDRO",
      "rm": "BG",
      "1234": 403016003
    },
    {
      "1": 3259,
      "roma": "ALBANO VERCELLESE",
      "rm": "VC",
      "1234": 401002003
    },
    {
      "1": 3260,
      "roma": "ALBAREDO ARNABOLDI",
      "rm": "PV",
      "1234": 403018002
    },
    {
      "1": 3261,
      "roma": "ALBAREDO D'ADIGE",
      "rm": "VR",
      "1234": 405023002
    },
    {
      "1": 3262,
      "roma": "ALBAREDO PER SAN MARCO",
      "rm": "SO",
      "1234": 403014001
    },
    {
      "1": 3263,
      "roma": "ALBARETO",
      "rm": "PR",
      "1234": 408034001
    },
    {
      "1": 3264,
      "roma": "ALBARETTO DELLA TORRE",
      "rm": "CN",
      "1234": 401004004
    },
    {
      "1": 3265,
      "roma": "ALA DI STURA",
      "rm": "TO",
      "1234": 401001003
    },
    {
      "1": 3266,
      "roma": "ALA' DEI SARDI",
      "rm": "SS",
      "1234": 420090002
    },
    {
      "1": 3267,
      "roma": "ALAGNA",
      "rm": "PV",
      "1234": 403018001
    },
    {
      "1": 3268,
      "roma": "ALAGNA VALSESIA",
      "rm": "VC",
      "1234": 401002002
    },
    {
      "1": 3269,
      "roma": "ALANNO",
      "rm": "PE",
      "1234": 413068002
    },
    {
      "1": 3270,
      "roma": "AICURZIO",
      "rm": "MB",
      "1234": 403108002
    },
    {
      "1": 3271,
      "roma": "ALANO DI PIAVE",
      "rm": "BL",
      "1234": 405025002
    },
    {
      "1": 3272,
      "roma": "ALATRI",
      "rm": "FR",
      "1234": 412060003
    },
    {
      "1": 3273,
      "roma": "ALBA",
      "rm": "CN",
      "1234": 401004003
    },
    {
      "1": 3274,
      "roma": "ALBA ADRIATICA",
      "rm": "TE",
      "1234": 413067001
    },
    {
      "1": 3275,
      "roma": "ALBESE CON CASSANO",
      "rm": "CO",
      "1234": 403013004
    },
    {
      "1": 3276,
      "roma": "ALBETTONE",
      "rm": "VI",
      "1234": 405024002
    },
    {
      "1": 3277,
      "roma": "ALBI",
      "rm": "CZ",
      "1234": 418079002
    },
    {
      "1": 3278,
      "roma": "ALBIANO",
      "rm": "TN",
      "1234": 404022002
    },
    {
      "1": 3279,
      "roma": "ALBIANO D'IVREA",
      "rm": "TO",
      "1234": 401001004
    },
    {
      "1": 2464,
      "roma": "GHILARZA",
      "rm": "OR",
      "1234": 420095021
    },
    {
      "1": 2465,
      "roma": "GIFFONI VALLE PIANA",
      "rm": "SA",
      "1234": 415065056
    },
    {
      "1": 2466,
      "roma": "GIGNESE",
      "rm": "VB",
      "1234": 401103034
    },
    {
      "1": 2467,
      "roma": "GIGNOD",
      "rm": "AO",
      "1234": 402007030
    },
    {
      "1": 2468,
      "roma": "GILDONE",
      "rm": "CB",
      "1234": 414070026
    },
    {
      "1": 2469,
      "roma": "GIMIGLIANO",
      "rm": "CZ",
      "1234": 418079058
    },
    {
      "1": 2470,
      "roma": "GINESTRA",
      "rm": "PZ",
      "1234": 417076099
    },
    {
      "1": 2471,
      "roma": "GINESTRA DEGLI SCHIAVONI",
      "rm": "BN",
      "1234": 415062036
    },
    {
      "1": 2472,
      "roma": "GINOSA",
      "rm": "TA",
      "1234": 416073007
    },
    {
      "1": 2473,
      "roma": "GIOI",
      "rm": "SA",
      "1234": 415065057
    },
    {
      "1": 2474,
      "roma": "GIOIA DEI MARSI",
      "rm": "AQ",
      "1234": 413066046
    },
    {
      "1": 2475,
      "roma": "GIOIA DEL COLLE",
      "rm": "BA",
      "1234": 416072021
    },
    {
      "1": 2476,
      "roma": "GIARRE",
      "rm": "CT",
      "1234": 419087017
    },
    {
      "1": 2477,
      "roma": "LONATO DEL GARDA",
      "rm": "BS",
      "1234": 403017992
    },
    {
      "1": 2478,
      "roma": "GIAVE",
      "rm": "SS",
      "1234": 420090030
    },
    {
      "1": 2479,
      "roma": "GIOIOSA IONICA",
      "rm": "RC",
      "1234": 418080039
    },
    {
      "1": 2480,
      "roma": "GIBELLINA",
      "rm": "TP",
      "1234": 419081010
    },
    {
      "1": 2481,
      "roma": "GIFFLENGA",
      "rm": "BI",
      "1234": 401096027
    },
    {
      "1": 2482,
      "roma": "GIFFONE",
      "rm": "RC",
      "1234": 418080037
    },
    {
      "1": 2483,
      "roma": "GIFFONI SEI CASALI",
      "rm": "SA",
      "1234": 415065055
    },
    {
      "1": 2484,
      "roma": "GIRASOLE",
      "rm": "NU",
      "1234": 420091031
    },
    {
      "1": 2485,
      "roma": "GIRIFALCO",
      "rm": "CZ",
      "1234": 418079059
    },
    {
      "1": 2486,
      "roma": "GISSI",
      "rm": "CH",
      "1234": 413069041
    },
    {
      "1": 2487,
      "roma": "GIUGGIANELLO",
      "rm": "LE",
      "1234": 416075032
    },
    {
      "1": 2488,
      "roma": "GIUGLIANO IN CAMPANIA",
      "rm": "NA",
      "1234": 415063034
    },
    {
      "1": 2489,
      "roma": "GIULIANA",
      "rm": "PA",
      "1234": 419082039
    },
    {
      "1": 2490,
      "roma": "GIULIANO DI ROMA",
      "rm": "FR",
      "1234": 412060041
    },
    {
      "1": 2491,
      "roma": "GIULIANO TEATINO",
      "rm": "CH",
      "1234": 413069042
    },
    {
      "1": 2492,
      "roma": "GIULIANOVA",
      "rm": "TE",
      "1234": 413067025
    },
    {
      "1": 2493,
      "roma": "GIUNGANO",
      "rm": "SA",
      "1234": 415065058
    },
    {
      "1": 2494,
      "roma": "GIURDIGNANO",
      "rm": "LE",
      "1234": 416075033
    },
    {
      "1": 2495,
      "roma": "GIUSSAGO",
      "rm": "PV",
      "1234": 403018072
    },
    {
      "1": 2496,
      "roma": "GIOIA SANNITICA",
      "rm": "CE",
      "1234": 415061041
    },
    {
      "1": 2497,
      "roma": "GIOIA TAURO",
      "rm": "RC",
      "1234": 418080038
    },
    {
      "1": 2498,
      "roma": "LA VALLETTA BRIANZA",
      "rm": "LC",
      "1234": 403097092
    },
    {
      "1": 2499,
      "roma": "GIUSVALLA",
      "rm": "SV",
      "1234": 407009032
    },
    {
      "1": 2500,
      "roma": "GIVOLETTO",
      "rm": "TO",
      "1234": 401001116
    },
    {
      "1": 2501,
      "roma": "GIOIOSA MAREA",
      "rm": "ME",
      "1234": 419083033
    },
    {
      "1": 2502,
      "roma": "GIOVE",
      "rm": "TR",
      "1234": 410055014
    },
    {
      "1": 2503,
      "roma": "GIOVINAZZO",
      "rm": "BA",
      "1234": 416072022
    },
    {
      "1": 2504,
      "roma": "GIOVO",
      "rm": "TN",
      "1234": 404022092
    },
    {
      "1": 2505,
      "roma": "GODRANO",
      "rm": "PA",
      "1234": 419082040
    },
    {
      "1": 2506,
      "roma": "GOITO",
      "rm": "MN",
      "1234": 403020026
    },
    {
      "1": 2507,
      "roma": "GOLASECCA",
      "rm": "VA",
      "1234": 403012077
    },
    {
      "1": 2508,
      "roma": "GOLFERENZO",
      "rm": "PV",
      "1234": 403018074
    },
    {
      "1": 2509,
      "roma": "GOLFO ARANCI",
      "rm": "SS",
      "1234": 420090083
    },
    {
      "1": 2510,
      "roma": "GOMBITO",
      "rm": "CR",
      "1234": 403019049
    },
    {
      "1": 2511,
      "roma": "GONARS",
      "rm": "UD",
      "1234": 406030044
    },
    {
      "1": 2512,
      "roma": "GONI",
      "rm": "CA",
      "1234": 420092027
    },
    {
      "1": 2513,
      "roma": "GONNESA",
      "rm": "CA",
      "1234": 420092028
    },
    {
      "1": 2514,
      "roma": "GIUSTENICE",
      "rm": "SV",
      "1234": 407009031
    },
    {
      "1": 2515,
      "roma": "GIUSTINO",
      "rm": "TN",
      "1234": 404022093
    },
    {
      "1": 2516,
      "roma": "GONNOSNO'",
      "rm": "OR",
      "1234": 420095023
    },
    {
      "1": 2517,
      "roma": "GIUSSANO",
      "rm": "MB",
      "1234": 403108024
    },
    {
      "1": 2518,
      "roma": "GIZZERIA",
      "rm": "CZ",
      "1234": 418079060
    },
    {
      "1": 2519,
      "roma": "GLORENZA",
      "rm": "BZ",
      "1234": 404021036
    },
    {
      "1": 2520,
      "roma": "GODEGA DI SANT'URBANO",
      "rm": "TV",
      "1234": 405026033
    },
    {
      "1": 2521,
      "roma": "GORGOGLIONE",
      "rm": "MT",
      "1234": 417077010
    },
    {
      "1": 2522,
      "roma": "GORGONZOLA",
      "rm": "MI",
      "1234": 403015108
    },
    {
      "1": 2523,
      "roma": "GORIANO SICOLI",
      "rm": "AQ",
      "1234": 413066047
    },
    {
      "1": 2524,
      "roma": "GORIZIA",
      "rm": "GO",
      "1234": 406031007
    },
    {
      "1": 2525,
      "roma": "GORLA MAGGIORE",
      "rm": "VA",
      "1234": 403012078
    },
    {
      "1": 2526,
      "roma": "GORLA MINORE",
      "rm": "VA",
      "1234": 403012079
    },
    {
      "1": 2527,
      "roma": "GORLAGO",
      "rm": "BG",
      "1234": 403016114
    },
    {
      "1": 2528,
      "roma": "GORLE",
      "rm": "BG",
      "1234": 403016115
    },
    {
      "1": 2529,
      "roma": "GORNATE-OLONA",
      "rm": "VA",
      "1234": 403012080
    },
    {
      "1": 2530,
      "roma": "GORNO",
      "rm": "BG",
      "1234": 403016116
    },
    {
      "1": 2531,
      "roma": "GONNOSCODINA",
      "rm": "OR",
      "1234": 420095022
    },
    {
      "1": 2532,
      "roma": "GONNOSFANADIGA",
      "rm": "CA",
      "1234": 420092029
    },
    {
      "1": 2533,
      "roma": "GONNOSTRAMATZA",
      "rm": "OR",
      "1234": 420095024
    },
    {
      "1": 2534,
      "roma": "GONZAGA",
      "rm": "MN",
      "1234": 403020027
    },
    {
      "1": 2535,
      "roma": "GODIASCO SALICE TERME",
      "rm": "PV",
      "1234": 403018973
    },
    {
      "1": 2536,
      "roma": "GORDONA",
      "rm": "SO",
      "1234": 403014032
    },
    {
      "1": 2537,
      "roma": "GORGA",
      "rm": "RM",
      "1234": 412058045
    },
    {
      "1": 2538,
      "roma": "GORGO AL MONTICANO",
      "rm": "TV",
      "1234": 405026034
    },
    {
      "1": 2539,
      "roma": "GOTTOLENGO",
      "rm": "BS",
      "1234": 403017080
    },
    {
      "1": 2540,
      "roma": "GOVONE",
      "rm": "CN",
      "1234": 401004099
    },
    {
      "1": 2541,
      "roma": "GOZZANO",
      "rm": "NO",
      "1234": 401003076
    },
    {
      "1": 2542,
      "roma": "GRADARA",
      "rm": "PS",
      "1234": 411041020
    },
    {
      "1": 2543,
      "roma": "GRADISCA D'ISONZO",
      "rm": "GO",
      "1234": 406031008
    },
    {
      "1": 2544,
      "roma": "GRADO",
      "rm": "GO",
      "1234": 406031009
    },
    {
      "1": 2545,
      "roma": "GRADOLI",
      "rm": "VT",
      "1234": 412056028
    },
    {
      "1": 2546,
      "roma": "GRAFFIGNANA",
      "rm": "LO",
      "1234": 403098028
    },
    {
      "1": 2547,
      "roma": "GRAFFIGNANO",
      "rm": "VT",
      "1234": 412056029
    },
    {
      "1": 2548,
      "roma": "GRAGLIA",
      "rm": "BI",
      "1234": 401096028
    },
    {
      "1": 2549,
      "roma": "GORO",
      "rm": "FE",
      "1234": 408038025
    },
    {
      "1": 2550,
      "roma": "GORRETO",
      "rm": "GE",
      "1234": 407010026
    },
    {
      "1": 2551,
      "roma": "GRAGNANO TREBBIENSE",
      "rm": "PC",
      "1234": 408033024
    },
    {
      "1": 2552,
      "roma": "GORZEGNO",
      "rm": "CN",
      "1234": 401004097
    },
    {
      "1": 2553,
      "roma": "GOSALDO",
      "rm": "BL",
      "1234": 405025025
    },
    {
      "1": 2554,
      "roma": "GOSSOLENGO",
      "rm": "PC",
      "1234": 408033023
    },
    {
      "1": 2555,
      "roma": "GOTTASECCA",
      "rm": "CN",
      "1234": 401004098
    },
    {
      "1": 2556,
      "roma": "LESSONA",
      "rm": "BI",
      "1234": 401096085
    },
    {
      "1": 2557,
      "roma": "GRANDOLA ED UNITI",
      "rm": "CO",
      "1234": 403013111
    },
    {
      "1": 2558,
      "roma": "GRANITI",
      "rm": "ME",
      "1234": 419083034
    },
    {
      "1": 2559,
      "roma": "GRANOZZO CON MONTICELLO",
      "rm": "NO",
      "1234": 401003077
    },
    {
      "1": 2560,
      "roma": "GRANTOLA",
      "rm": "VA",
      "1234": 403012081
    },
    {
      "1": 2561,
      "roma": "GRANTORTO",
      "rm": "PD",
      "1234": 405028042
    },
    {
      "1": 2562,
      "roma": "GRANZE",
      "rm": "PD",
      "1234": 405028043
    },
    {
      "1": 2563,
      "roma": "GRASSANO",
      "rm": "MT",
      "1234": 417077011
    },
    {
      "1": 2564,
      "roma": "GRASSOBBIO",
      "rm": "BG",
      "1234": 403016117
    },
    {
      "1": 2565,
      "roma": "GRATTERI",
      "rm": "PA",
      "1234": 419082041
    },
    {
      "1": 2566,
      "roma": "GRAVELLONA LOMELLINA",
      "rm": "PV",
      "1234": 403018075
    },
    {
      "1": 2567,
      "roma": "GRAVELLONA TOCE",
      "rm": "VB",
      "1234": 401103035
    },
    {
      "1": 2568,
      "roma": "GRAVERE",
      "rm": "TO",
      "1234": 401001117
    },
    {
      "1": 2569,
      "roma": "GRAGNANO",
      "rm": "NA",
      "1234": 415063035
    },
    {
      "1": 2570,
      "roma": "GRAMMICHELE",
      "rm": "CT",
      "1234": 419087018
    },
    {
      "1": 2571,
      "roma": "GRANA",
      "rm": "AT",
      "1234": 401005056
    },
    {
      "1": 2572,
      "roma": "GRANAROLO DELL'EMILIA",
      "rm": "BO",
      "1234": 408037030
    },
    {
      "1": 2573,
      "roma": "GRANDATE",
      "rm": "CO",
      "1234": 403013110
    },
    {
      "1": 2574,
      "roma": "GREGGIO",
      "rm": "VC",
      "1234": 401002065
    },
    {
      "1": 2575,
      "roma": "GREMIASCO",
      "rm": "AL",
      "1234": 401006083
    },
    {
      "1": 2576,
      "roma": "GRAVEDONA ED UNITI",
      "rm": "CO",
      "1234": 403013249
    },
    {
      "1": 2577,
      "roma": "GRESSAN",
      "rm": "AO",
      "1234": 402007031
    },
    {
      "1": 2578,
      "roma": "GRESSONEY-LA-TRINITE'",
      "rm": "AO",
      "1234": 402007032
    },
    {
      "1": 2579,
      "roma": "GRESSONEY-SAINT-JEAN",
      "rm": "AO",
      "1234": 402007033
    },
    {
      "1": 2580,
      "roma": "GREVE IN CHIANTI",
      "rm": "FI",
      "1234": 409048021
    },
    {
      "1": 2581,
      "roma": "GREZZAGO",
      "rm": "MI",
      "1234": 403015110
    },
    {
      "1": 2582,
      "roma": "GREZZANA",
      "rm": "VR",
      "1234": 405023038
    },
    {
      "1": 2583,
      "roma": "GRIANTE",
      "rm": "CO",
      "1234": 403013113
    },
    {
      "1": 2584,
      "roma": "GRICIGNANO DI AVERSA",
      "rm": "CE",
      "1234": 415061043
    },
    {
      "1": 2585,
      "roma": "GRAVINA DI CATANIA",
      "rm": "CT",
      "1234": 419087019
    },
    {
      "1": 2586,
      "roma": "GRAVINA IN PUGLIA",
      "rm": "BA",
      "1234": 416072023
    },
    {
      "1": 2587,
      "roma": "GRAZZANISE",
      "rm": "CE",
      "1234": 415061042
    },
    {
      "1": 2588,
      "roma": "GRAZZANO BADOGLIO",
      "rm": "AT",
      "1234": 401005057
    },
    {
      "1": 2589,
      "roma": "GRECCIO",
      "rm": "RI",
      "1234": 412057031
    },
    {
      "1": 2590,
      "roma": "GRECI",
      "rm": "AV",
      "1234": 415064037
    },
    {
      "1": 2591,
      "roma": "GRIZZANA MORANDI",
      "rm": "BO",
      "1234": 408037031
    },
    {
      "1": 2592,
      "roma": "GROGNARDO",
      "rm": "AL",
      "1234": 401006084
    },
    {
      "1": 2593,
      "roma": "GROMO",
      "rm": "BG",
      "1234": 403016118
    },
    {
      "1": 2594,
      "roma": "GRONDONA",
      "rm": "AL",
      "1234": 401006085
    },
    {
      "1": 2595,
      "roma": "GRONE",
      "rm": "BG",
      "1234": 403016119
    },
    {
      "1": 2596,
      "roma": "GRONTARDO",
      "rm": "CR",
      "1234": 403019050
    },
    {
      "1": 2597,
      "roma": "GROPELLO CAIROLI",
      "rm": "PV",
      "1234": 403018076
    },
    {
      "1": 2598,
      "roma": "GROPPARELLO",
      "rm": "PC",
      "1234": 408033025
    },
    {
      "1": 2599,
      "roma": "GROSCAVALLO",
      "rm": "TO",
      "1234": 401001118
    },
    {
      "1": 2600,
      "roma": "GROSIO",
      "rm": "SO",
      "1234": 403014033
    },
    {
      "1": 2601,
      "roma": "GROSOTTO",
      "rm": "SO",
      "1234": 403014034
    },
    {
      "1": 2602,
      "roma": "GROSSETO",
      "rm": "GR",
      "1234": 409053011
    },
    {
      "1": 2603,
      "roma": "GRIGNASCO",
      "rm": "NO",
      "1234": 401003079
    },
    {
      "1": 2604,
      "roma": "GRIGNO",
      "rm": "TN",
      "1234": 404022095
    },
    {
      "1": 2605,
      "roma": "GRIMACCO",
      "rm": "UD",
      "1234": 406030045
    },
    {
      "1": 2606,
      "roma": "GRINZANE CAVOUR",
      "rm": "CN",
      "1234": 401004100
    },
    {
      "1": 2607,
      "roma": "GRISIGNANO DI ZOCCO",
      "rm": "VI",
      "1234": 405024046
    },
    {
      "1": 2608,
      "roma": "GRISOLIA",
      "rm": "CS",
      "1234": 418078060
    },
    {
      "1": 2609,
      "roma": "GROTTE DI CASTRO",
      "rm": "VT",
      "1234": 412056030
    },
    {
      "1": 2610,
      "roma": "GROTTERIA",
      "rm": "RC",
      "1234": 418080040
    },
    {
      "1": 2611,
      "roma": "GROTTOLE",
      "rm": "MT",
      "1234": 417077012
    },
    {
      "1": 2612,
      "roma": "GROTTOLELLA",
      "rm": "AV",
      "1234": 415064039
    },
    {
      "1": 2613,
      "roma": "GRUARO",
      "rm": "VE",
      "1234": 405027018
    },
    {
      "1": 2614,
      "roma": "GRUGLIASCO",
      "rm": "TO",
      "1234": 401001120
    },
    {
      "1": 2615,
      "roma": "GROTTAZZOLINA",
      "rm": "FM",
      "1234": 411109008
    },
    {
      "1": 2616,
      "roma": "GRUMELLO CREMONESE ED UNITI",
      "rm": "CR",
      "1234": 403019051
    },
    {
      "1": 2617,
      "roma": "GRUMELLO DEL MONTE",
      "rm": "BG",
      "1234": 403016120
    },
    {
      "1": 2618,
      "roma": "GRUMENTO NOVA",
      "rm": "PZ",
      "1234": 417076037
    },
    {
      "1": 2619,
      "roma": "GRUMO APPULA",
      "rm": "BA",
      "1234": 416072024
    },
    {
      "1": 2620,
      "roma": "GRUMO NEVANO",
      "rm": "NA",
      "1234": 415063036
    },
    {
      "1": 2621,
      "roma": "GROSSO",
      "rm": "TO",
      "1234": 401001119
    },
    {
      "1": 2622,
      "roma": "GRIMALDI",
      "rm": "CS",
      "1234": 418078059
    },
    {
      "1": 2623,
      "roma": "GROTTAGLIE",
      "rm": "TA",
      "1234": 416073008
    },
    {
      "1": 2624,
      "roma": "GROTTAMINARDA",
      "rm": "AV",
      "1234": 415064038
    },
    {
      "1": 2625,
      "roma": "GROTTAMMARE",
      "rm": "AP",
      "1234": 411044023
    },
    {
      "1": 2626,
      "roma": "GROTTE",
      "rm": "AG",
      "1234": 419084018
    },
    {
      "1": 2627,
      "roma": "GUAMAGGIORE",
      "rm": "CA",
      "1234": 420092030
    },
    {
      "1": 2628,
      "roma": "GUANZATE",
      "rm": "CO",
      "1234": 403013114
    },
    {
      "1": 2629,
      "roma": "GUARCINO",
      "rm": "FR",
      "1234": 412060042
    },
    {
      "1": 2630,
      "roma": "GUARDA VENETA",
      "rm": "RO",
      "1234": 405029028
    },
    {
      "1": 2631,
      "roma": "GUARDABOSONE",
      "rm": "VC",
      "1234": 401002066
    },
    {
      "1": 2632,
      "roma": "GUARDAMIGLIO",
      "rm": "LO",
      "1234": 403098029
    },
    {
      "1": 2633,
      "roma": "GUARDAVALLE",
      "rm": "CZ",
      "1234": 418079061
    },
    {
      "1": 2634,
      "roma": "GUARDEA",
      "rm": "TR",
      "1234": 410055015
    },
    {
      "1": 2635,
      "roma": "GUARDIA LOMBARDI",
      "rm": "AV",
      "1234": 415064040
    },
    {
      "1": 2636,
      "roma": "GUARDIA PERTICARA",
      "rm": "PZ",
      "1234": 417076038
    },
    {
      "1": 2637,
      "roma": "GUARDIA PIEMONTESE",
      "rm": "CS",
      "1234": 418078061
    },
    {
      "1": 2638,
      "roma": "GUARDIA SANFRAMONDI",
      "rm": "BN",
      "1234": 415062037
    },
    {
      "1": 2639,
      "roma": "GUARDIAGRELE",
      "rm": "CH",
      "1234": 413069043
    },
    {
      "1": 2640,
      "roma": "GUARDIALFIERA",
      "rm": "CB",
      "1234": 414070027
    },
    {
      "1": 2641,
      "roma": "GUARDIAREGIA",
      "rm": "CB",
      "1234": 414070028
    },
    {
      "1": 2642,
      "roma": "GRUMOLO DELLE ABBADESSE",
      "rm": "VI",
      "1234": 405024047
    },
    {
      "1": 2643,
      "roma": "GROTTAFERRATA",
      "rm": "RM",
      "1234": 412058046
    },
    {
      "1": 2644,
      "roma": "GUALDO CATTANEO",
      "rm": "PG",
      "1234": 410054022
    },
    {
      "1": 2645,
      "roma": "GUALDO TADINO",
      "rm": "PG",
      "1234": 410054023
    },
    {
      "1": 2646,
      "roma": "GUALTIERI",
      "rm": "RE",
      "1234": 408035023
    },
    {
      "1": 2647,
      "roma": "GUALTIERI SICAMINO'",
      "rm": "ME",
      "1234": 419083035
    },
    {
      "1": 2648,
      "roma": "GUBBIO",
      "rm": "PG",
      "1234": 410054024
    },
    {
      "1": 2649,
      "roma": "GUDO VISCONTI",
      "rm": "MI",
      "1234": 403015112
    },
    {
      "1": 2650,
      "roma": "GUGLIONESI",
      "rm": "CB",
      "1234": 414070029
    },
    {
      "1": 2651,
      "roma": "GUIDIZZOLO",
      "rm": "MN",
      "1234": 403020028
    },
    {
      "1": 2652,
      "roma": "GUIDONIA MONTECELIO",
      "rm": "RM",
      "1234": 412058047
    },
    {
      "1": 2653,
      "roma": "GUIGLIA",
      "rm": "MO",
      "1234": 408036017
    },
    {
      "1": 2654,
      "roma": "GUILMI",
      "rm": "CH",
      "1234": 413069044
    },
    {
      "1": 2655,
      "roma": "GURRO",
      "rm": "VB",
      "1234": 401103036
    },
    {
      "1": 2656,
      "roma": "GUSPINI",
      "rm": "CA",
      "1234": 420092032
    },
    {
      "1": 2657,
      "roma": "GUARDISTALLO",
      "rm": "PI",
      "1234": 409050015
    },
    {
      "1": 2658,
      "roma": "GUAGNANO",
      "rm": "LE",
      "1234": 416075034
    },
    {
      "1": 2659,
      "roma": "GUALDO",
      "rm": "MC",
      "1234": 411043021
    },
    {
      "1": 2660,
      "roma": "GUASTALLA",
      "rm": "RE",
      "1234": 408035024
    },
    {
      "1": 2661,
      "roma": "GUAZZORA",
      "rm": "AL",
      "1234": 401006086
    },
    {
      "1": 2662,
      "roma": "IDRO",
      "rm": "BS",
      "1234": 403017082
    },
    {
      "1": 2663,
      "roma": "JESOLO",
      "rm": "VE",
      "1234": 405027019
    },
    {
      "1": 2664,
      "roma": "IGLESIAS",
      "rm": "CA",
      "1234": 420092033
    },
    {
      "1": 2665,
      "roma": "IGLIANO",
      "rm": "CN",
      "1234": 401004102
    },
    {
      "1": 2666,
      "roma": "ILBONO",
      "rm": "NU",
      "1234": 420091032
    },
    {
      "1": 2667,
      "roma": "ILLASI",
      "rm": "VR",
      "1234": 405023039
    },
    {
      "1": 2668,
      "roma": "ILLORAI",
      "rm": "SS",
      "1234": 420090031
    },
    {
      "1": 2669,
      "roma": "IMBERSAGO",
      "rm": "LC",
      "1234": 403097039
    },
    {
      "1": 2670,
      "roma": "IMER",
      "rm": "TN",
      "1234": 404022097
    },
    {
      "1": 2671,
      "roma": "IMOLA",
      "rm": "BO",
      "1234": 408037032
    },
    {
      "1": 2672,
      "roma": "GUSSAGO",
      "rm": "BS",
      "1234": 403017081
    },
    {
      "1": 3280,
      "roma": "ALBIDONA",
      "rm": "CS",
      "1234": 418078006
    },
    {
      "1": 3281,
      "roma": "ALBIGNASEGO",
      "rm": "PD",
      "1234": 405028003
    },
    {
      "1": 3282,
      "roma": "ALBINEA",
      "rm": "RE",
      "1234": 408035001
    },
    {
      "1": 3283,
      "roma": "ALBINO",
      "rm": "BG",
      "1234": 403016004
    },
    {
      "1": 3284,
      "roma": "ALBIOLO",
      "rm": "CO",
      "1234": 403013005
    },
    {
      "1": 3285,
      "roma": "ALBAVILLA",
      "rm": "CO",
      "1234": 403013003
    },
    {
      "1": 3286,
      "roma": "ALBENGA",
      "rm": "SV",
      "1234": 407009002
    },
    {
      "1": 3287,
      "roma": "ALBERA LIGURE",
      "rm": "AL",
      "1234": 401006002
    },
    {
      "1": 3288,
      "roma": "ALBEROBELLO",
      "rm": "BA",
      "1234": 416072003
    },
    {
      "1": 3289,
      "roma": "ALBERONA",
      "rm": "FG",
      "1234": 416071002
    },
    {
      "1": 3290,
      "roma": "ALBUZZANO",
      "rm": "PV",
      "1234": 403018004
    },
    {
      "1": 3291,
      "roma": "ALCAMO",
      "rm": "TP",
      "1234": 419081001
    },
    {
      "1": 3292,
      "roma": "ALCARA LI FUSI",
      "rm": "ME",
      "1234": 419083001
    },
    {
      "1": 3293,
      "roma": "ALDENO",
      "rm": "TN",
      "1234": 404022003
    },
    {
      "1": 3294,
      "roma": "ALDINO",
      "rm": "BZ",
      "1234": 404021001
    },
    {
      "1": 3295,
      "roma": "ALES",
      "rm": "OR",
      "1234": 420095004
    },
    {
      "1": 3296,
      "roma": "ALESSANDRIA",
      "rm": "AL",
      "1234": 401006003
    },
    {
      "1": 3297,
      "roma": "ALESSANDRIA DEL CARRETTO",
      "rm": "CS",
      "1234": 418078007
    },
    {
      "1": 3298,
      "roma": "ALESSANDRIA DELLA ROCCA",
      "rm": "AG",
      "1234": 419084002
    },
    {
      "1": 3299,
      "roma": "ALESSANO",
      "rm": "LE",
      "1234": 416075002
    },
    {
      "1": 3300,
      "roma": "ALEZIO",
      "rm": "LE",
      "1234": 416075003
    },
    {
      "1": 3301,
      "roma": "ALFANO",
      "rm": "SA",
      "1234": 415065004
    },
    {
      "1": 3302,
      "roma": "ALFEDENA",
      "rm": "AQ",
      "1234": 413066003
    },
    {
      "1": 3303,
      "roma": "ALBISOLA SUPERIORE",
      "rm": "SV",
      "1234": 407009004
    },
    {
      "1": 3304,
      "roma": "ALBISSOLA MARINA",
      "rm": "SV",
      "1234": 407009003
    },
    {
      "1": 3305,
      "roma": "ALBIZZATE",
      "rm": "VA",
      "1234": 403012002
    },
    {
      "1": 3306,
      "roma": "ALBONESE",
      "rm": "PV",
      "1234": 403018003
    },
    {
      "1": 3307,
      "roma": "ALBUGNANO",
      "rm": "AT",
      "1234": 401005002
    },
    {
      "1": 3308,
      "roma": "ALBIATE",
      "rm": "MB",
      "1234": 403108003
    },
    {
      "1": 3309,
      "roma": "ALIANO",
      "rm": "MT",
      "1234": 417077002
    },
    {
      "1": 3310,
      "roma": "ALICE BEL COLLE",
      "rm": "AL",
      "1234": 401006005
    },
    {
      "1": 3311,
      "roma": "ALTAVALLE",
      "rm": "TN",
      "1234": 404022235
    },
    {
      "1": 3312,
      "roma": "ALICE CASTELLO",
      "rm": "VC",
      "1234": 401002004
    },
    {
      "1": 3313,
      "roma": "ALIFE",
      "rm": "CE",
      "1234": 415061002
    },
    {
      "1": 3314,
      "roma": "ALIMENA",
      "rm": "PA",
      "1234": 419082002
    },
    {
      "1": 3315,
      "roma": "ALIMINUSA",
      "rm": "PA",
      "1234": 419082003
    },
    {
      "1": 3316,
      "roma": "ALLAI",
      "rm": "OR",
      "1234": 420095005
    },
    {
      "1": 3317,
      "roma": "ALLEGHE",
      "rm": "BL",
      "1234": 405025003
    },
    {
      "1": 3318,
      "roma": "ALLEIN",
      "rm": "AO",
      "1234": 402007001
    },
    {
      "1": 3319,
      "roma": "ALLERONA",
      "rm": "TR",
      "1234": 410055002
    },
    {
      "1": 3320,
      "roma": "ALLISTE",
      "rm": "LE",
      "1234": 416075004
    },
    {
      "1": 3321,
      "roma": "ALFIANELLO",
      "rm": "BS",
      "1234": 403017004
    },
    {
      "1": 3322,
      "roma": "ALFIANO NATTA",
      "rm": "AL",
      "1234": 401006004
    },
    {
      "1": 3323,
      "roma": "ALFONSINE",
      "rm": "RA",
      "1234": 408039001
    },
    {
      "1": 3324,
      "roma": "ALGHERO",
      "rm": "SS",
      "1234": 420090003
    },
    {
      "1": 3325,
      "roma": "ALBOSAGGIA",
      "rm": "SO",
      "1234": 403014002
    },
    {
      "1": 3326,
      "roma": "ALI'",
      "rm": "ME",
      "1234": 419083002
    },
    {
      "1": 3327,
      "roma": "ALI' TERME",
      "rm": "ME",
      "1234": 419083003
    },
    {
      "1": 3328,
      "roma": "ALIA",
      "rm": "PA",
      "1234": 419082001
    },
    {
      "1": 3329,
      "roma": "ALONTE",
      "rm": "VI",
      "1234": 405024003
    },
    {
      "1": 3330,
      "roma": "ALPETTE",
      "rm": "TO",
      "1234": 401001007
    },
    {
      "1": 3331,
      "roma": "ALPIGNANO",
      "rm": "TO",
      "1234": 401001008
    },
    {
      "1": 3332,
      "roma": "ALSENO",
      "rm": "PC",
      "1234": 408033002
    },
    {
      "1": 3333,
      "roma": "ALSERIO",
      "rm": "CO",
      "1234": 403013006
    },
    {
      "1": 3334,
      "roma": "ALTOPIANO DELLA VIGOLANA",
      "rm": "TN",
      "1234": 404022236
    },
    {
      "1": 3335,
      "roma": "ALTAMURA",
      "rm": "BA",
      "1234": 416072004
    },
    {
      "1": 3336,
      "roma": "ALTARE",
      "rm": "SV",
      "1234": 407009005
    },
    {
      "1": 3337,
      "roma": "ALTAVILLA IRPINA",
      "rm": "AV",
      "1234": 415064002
    },
    {
      "1": 3338,
      "roma": "ALTAVILLA MILICIA",
      "rm": "PA",
      "1234": 419082004
    },
    {
      "1": 3339,
      "roma": "ALTAVILLA MONFERRATO",
      "rm": "AL",
      "1234": 401006007
    },
    {
      "1": 3340,
      "roma": "ALTAVILLA SILENTINA",
      "rm": "SA",
      "1234": 415065005
    },
    {
      "1": 3341,
      "roma": "ALTAVILLA VICENTINA",
      "rm": "VI",
      "1234": 405024004
    },
    {
      "1": 3342,
      "roma": "ALLUMIERE",
      "rm": "RM",
      "1234": 412058004
    },
    {
      "1": 3343,
      "roma": "ALME'",
      "rm": "BG",
      "1234": 403016005
    },
    {
      "1": 3344,
      "roma": "ALGUA",
      "rm": "BG",
      "1234": 403016248
    },
    {
      "1": 3345,
      "roma": "ALTILIA",
      "rm": "CS",
      "1234": 418078008
    },
    {
      "1": 3346,
      "roma": "ALTINO",
      "rm": "CH",
      "1234": 413069001
    },
    {
      "1": 3347,
      "roma": "ALTISSIMO",
      "rm": "VI",
      "1234": 405024005
    },
    {
      "1": 3348,
      "roma": "ALTIVOLE",
      "rm": "TV",
      "1234": 405026001
    },
    {
      "1": 3349,
      "roma": "ALTO",
      "rm": "CN",
      "1234": 401004005
    },
    {
      "1": 3350,
      "roma": "ALTOFONTE",
      "rm": "PA",
      "1234": 419082005
    },
    {
      "1": 3351,
      "roma": "ALMENNO SAN BARTOLOMEO",
      "rm": "BG",
      "1234": 403016006
    },
    {
      "1": 3352,
      "roma": "ALMENNO SAN SALVATORE",
      "rm": "BG",
      "1234": 403016007
    },
    {
      "1": 3353,
      "roma": "ALMESE",
      "rm": "TO",
      "1234": 401001006
    },
    {
      "1": 3354,
      "roma": "ALVIANO",
      "rm": "TR",
      "1234": 410055003
    },
    {
      "1": 3355,
      "roma": "ALVIGNANO",
      "rm": "CE",
      "1234": 415061003
    },
    {
      "1": 3356,
      "roma": "ALVITO",
      "rm": "FR",
      "1234": 412060004
    },
    {
      "1": 3357,
      "roma": "ALZANO LOMBARDO",
      "rm": "BG",
      "1234": 403016008
    },
    {
      "1": 3358,
      "roma": "ALZANO SCRIVIA",
      "rm": "AL",
      "1234": 401006008
    },
    {
      "1": 3359,
      "roma": "ALTIDONA",
      "rm": "FM",
      "1234": 411109001
    },
    {
      "1": 3360,
      "roma": "ALZATE BRIANZA",
      "rm": "CO",
      "1234": 403013007
    },
    {
      "1": 3361,
      "roma": "AMALFI",
      "rm": "SA",
      "1234": 415065006
    },
    {
      "1": 3362,
      "roma": "AMANTEA",
      "rm": "CS",
      "1234": 418078010
    },
    {
      "1": 3363,
      "roma": "AMARO",
      "rm": "UD",
      "1234": 406030002
    },
    {
      "1": 3364,
      "roma": "AMARONI",
      "rm": "CZ",
      "1234": 418079003
    },
    {
      "1": 3365,
      "roma": "AMASENO",
      "rm": "FR",
      "1234": 412060005
    },
    {
      "1": 3366,
      "roma": "AMATO",
      "rm": "CZ",
      "1234": 418079004
    },
    {
      "1": 3367,
      "roma": "AMATRICE",
      "rm": "RI",
      "1234": 412057002
    },
    {
      "1": 3368,
      "roma": "AMBIVERE",
      "rm": "BG",
      "1234": 403016009
    },
    {
      "1": 3369,
      "roma": "ALTOMONTE",
      "rm": "CS",
      "1234": 418078009
    },
    {
      "1": 3370,
      "roma": "ALTOPASCIO",
      "rm": "LU",
      "1234": 409046001
    },
    {
      "1": 3371,
      "roma": "AMOROSI",
      "rm": "BN",
      "1234": 415062002
    },
    {
      "1": 3372,
      "roma": "AMPEZZO",
      "rm": "UD",
      "1234": 406030003
    },
    {
      "1": 3373,
      "roma": "ANACAPRI",
      "rm": "NA",
      "1234": 415063004
    },
    {
      "1": 3374,
      "roma": "ANAGNI",
      "rm": "FR",
      "1234": 412060006
    },
    {
      "1": 3375,
      "roma": "ANCARANO",
      "rm": "TE",
      "1234": 413067002
    },
    {
      "1": 3376,
      "roma": "ANCONA",
      "rm": "AN",
      "1234": 411042002
    },
    {
      "1": 3377,
      "roma": "AMANDOLA",
      "rm": "FM",
      "1234": 411109002
    },
    {
      "1": 3378,
      "roma": "ANDALI",
      "rm": "CZ",
      "1234": 418079005
    },
    {
      "1": 3379,
      "roma": "ANDALO",
      "rm": "TN",
      "1234": 404022005
    },
    {
      "1": 3380,
      "roma": "ANDALO VALTELLINO",
      "rm": "SO",
      "1234": 403014003
    },
    {
      "1": 3381,
      "roma": "ANDEZENO",
      "rm": "TO",
      "1234": 401001009
    },
    {
      "1": 3382,
      "roma": "ANDORA",
      "rm": "SV",
      "1234": 407009006
    },
    {
      "1": 3383,
      "roma": "ANDORNO MICCA",
      "rm": "BI",
      "1234": 401096002
    },
    {
      "1": 3384,
      "roma": "AMEGLIA",
      "rm": "SP",
      "1234": 407011001
    },
    {
      "1": 3385,
      "roma": "AMELIA",
      "rm": "TR",
      "1234": 410055004
    },
    {
      "1": 3386,
      "roma": "AMENDOLARA",
      "rm": "CS",
      "1234": 418078011
    },
    {
      "1": 3387,
      "roma": "AMENO",
      "rm": "NO",
      "1234": 401003002
    },
    {
      "1": 3388,
      "roma": "ANDREIS",
      "rm": "PN",
      "1234": 406093001
    },
    {
      "1": 3389,
      "roma": "ANDRETTA",
      "rm": "AV",
      "1234": 415064003
    },
    {
      "1": 3390,
      "roma": "ANDRIANO",
      "rm": "BZ",
      "1234": 404021002
    },
    {
      "1": 3391,
      "roma": "ANELA",
      "rm": "SS",
      "1234": 420090004
    },
    {
      "1": 3392,
      "roma": "ANFO",
      "rm": "BS",
      "1234": 403017005
    },
    {
      "1": 3393,
      "roma": "ANGERA",
      "rm": "VA",
      "1234": 403012003
    },
    {
      "1": 3394,
      "roma": "ANGHIARI",
      "rm": "AR",
      "1234": 409051001
    },
    {
      "1": 3395,
      "roma": "ANGIARI",
      "rm": "VR",
      "1234": 405023003
    },
    {
      "1": 3396,
      "roma": "ANGOLO TERME",
      "rm": "BS",
      "1234": 403017006
    },
    {
      "1": 3397,
      "roma": "ANGRI",
      "rm": "SA",
      "1234": 415065007
    },
    {
      "1": 3398,
      "roma": "ANDRIA",
      "rm": "BT",
      "1234": 416110001
    },
    {
      "1": 3399,
      "roma": "ANGROGNA",
      "rm": "TO",
      "1234": 401001011
    },
    {
      "1": 3400,
      "roma": "ANGUILLARA SABAZIA",
      "rm": "RM",
      "1234": 412058005
    },
    {
      "1": 3401,
      "roma": "ANGUILLARA VENETA",
      "rm": "PD",
      "1234": 405028004
    },
    {
      "1": 3402,
      "roma": "ANNICCO",
      "rm": "CR",
      "1234": 403019003
    },
    {
      "1": 3403,
      "roma": "ANDRANO",
      "rm": "LE",
      "1234": 416075005
    },
    {
      "1": 3404,
      "roma": "ANDRATE",
      "rm": "TO",
      "1234": 401001010
    },
    {
      "1": 3405,
      "roma": "ANTEGNATE",
      "rm": "BG",
      "1234": 403016010
    },
    {
      "1": 3406,
      "roma": "ANTERIVO",
      "rm": "BZ",
      "1234": 404021003
    },
    {
      "1": 3407,
      "roma": "ANTEY-SAINT-ANDRE'",
      "rm": "AO",
      "1234": 402007002
    },
    {
      "1": 3408,
      "roma": "ANTICOLI CORRADO",
      "rm": "RM",
      "1234": 412058006
    },
    {
      "1": 3409,
      "roma": "ANTIGNANO",
      "rm": "AT",
      "1234": 401005003
    },
    {
      "1": 3410,
      "roma": "ANTILLO",
      "rm": "ME",
      "1234": 419083004
    },
    {
      "1": 3411,
      "roma": "ANTONIMINA",
      "rm": "RC",
      "1234": 418080004
    },
    {
      "1": 3412,
      "roma": "ANTRODOCO",
      "rm": "RI",
      "1234": 412057003
    },
    {
      "1": 3413,
      "roma": "ANTRONA SCHIERANCO",
      "rm": "VB",
      "1234": 401103001
    },
    {
      "1": 3414,
      "roma": "ANVERSA DEGLI ABRUZZI",
      "rm": "AQ",
      "1234": 413066004
    },
    {
      "1": 3415,
      "roma": "AMBLAR-DON",
      "rm": "TN",
      "1234": 404022237
    },
    {
      "1": 3416,
      "roma": "ANZANO DEL PARCO",
      "rm": "CO",
      "1234": 403013009
    },
    {
      "1": 3417,
      "roma": "ANZANO DI PUGLIA",
      "rm": "FG",
      "1234": 416071003
    },
    {
      "1": 3418,
      "roma": "ANZI",
      "rm": "PZ",
      "1234": 417076004
    },
    {
      "1": 3419,
      "roma": "ANNONE DI BRIANZA",
      "rm": "LC",
      "1234": 403097003
    },
    {
      "1": 3420,
      "roma": "ANNONE VENETO",
      "rm": "VE",
      "1234": 405027001
    },
    {
      "1": 3421,
      "roma": "ANOIA",
      "rm": "RC",
      "1234": 418080003
    },
    {
      "1": 3422,
      "roma": "ANZOLA DELL'EMILIA",
      "rm": "BO",
      "1234": 408037001
    },
    {
      "1": 3423,
      "roma": "AOSTA",
      "rm": "AO",
      "1234": 402007003
    },
    {
      "1": 3424,
      "roma": "APECCHIO",
      "rm": "PS",
      "1234": 411041002
    },
    {
      "1": 3425,
      "roma": "APICE",
      "rm": "BN",
      "1234": 415062003
    },
    {
      "1": 3426,
      "roma": "APIRO",
      "rm": "MC",
      "1234": 411043002
    },
    {
      "1": 3427,
      "roma": "APOLLOSA",
      "rm": "BN",
      "1234": 415062004
    },
    {
      "1": 3428,
      "roma": "APPIANO GENTILE",
      "rm": "CO",
      "1234": 403013010
    },
    {
      "1": 3429,
      "roma": "APPIANO SULLA STRADA DEL VINO",
      "rm": "BZ",
      "1234": 404021004
    },
    {
      "1": 3430,
      "roma": "APPIGNANO",
      "rm": "MC",
      "1234": 411043003
    },
    {
      "1": 3431,
      "roma": "APPIGNANO DEL TRONTO",
      "rm": "AP",
      "1234": 411044005
    },
    {
      "1": 3432,
      "roma": "APRICA",
      "rm": "SO",
      "1234": 403014004
    },
    {
      "1": 3433,
      "roma": "APRICALE",
      "rm": "IM",
      "1234": 407008002
    },
    {
      "1": 3434,
      "roma": "APRICENA",
      "rm": "FG",
      "1234": 416071004
    },
    {
      "1": 3435,
      "roma": "APRIGLIANO",
      "rm": "CS",
      "1234": 418078012
    },
    {
      "1": 3436,
      "roma": "APRILIA",
      "rm": "LT",
      "1234": 412059001
    },
    {
      "1": 3437,
      "roma": "ALTO RENO TERME",
      "rm": "BO",
      "1234": 408037062
    },
    {
      "1": 3438,
      "roma": "ANZIO",
      "rm": "RM",
      "1234": 412058007
    },
    {
      "1": 3439,
      "roma": "ANZOLA D'OSSOLA",
      "rm": "VB",
      "1234": 401103002
    },
    {
      "1": 3440,
      "roma": "ARADEO",
      "rm": "LE",
      "1234": 416075006
    },
    {
      "1": 3441,
      "roma": "ARAGONA",
      "rm": "AG",
      "1234": 419084003
    },
    {
      "1": 3442,
      "roma": "ARAMENGO",
      "rm": "AT",
      "1234": 401005004
    },
    {
      "1": 3443,
      "roma": "ARBA",
      "rm": "PN",
      "1234": 406093002
    },
    {
      "1": 3444,
      "roma": "ARBOREA",
      "rm": "OR",
      "1234": 420095006
    },
    {
      "1": 3445,
      "roma": "ARBORIO",
      "rm": "VC",
      "1234": 401002006
    },
    {
      "1": 3446,
      "roma": "ARBUS",
      "rm": "CA",
      "1234": 420092001
    },
    {
      "1": 3447,
      "roma": "ARCADE",
      "rm": "TV",
      "1234": 405026002
    },
    {
      "1": 3448,
      "roma": "ARCE",
      "rm": "FR",
      "1234": 412060008
    },
    {
      "1": 3449,
      "roma": "ARCENE",
      "rm": "BG",
      "1234": 403016011
    },
    {
      "1": 3450,
      "roma": "ARCEVIA",
      "rm": "AN",
      "1234": 411042003
    },
    {
      "1": 3451,
      "roma": "AQUARA",
      "rm": "SA",
      "1234": 415065008
    },
    {
      "1": 3452,
      "roma": "AQUILA D'ARROSCIA",
      "rm": "IM",
      "1234": 407008003
    },
    {
      "1": 3453,
      "roma": "AQUILEIA",
      "rm": "UD",
      "1234": 406030004
    },
    {
      "1": 3454,
      "roma": "AQUILONIA",
      "rm": "AV",
      "1234": 415064004
    },
    {
      "1": 3455,
      "roma": "AQUINO",
      "rm": "FR",
      "1234": 412060007
    },
    {
      "1": 3456,
      "roma": "ARCISATE",
      "rm": "VA",
      "1234": 403012004
    },
    {
      "1": 3457,
      "roma": "ARCO",
      "rm": "TN",
      "1234": 404022006
    },
    {
      "1": 3458,
      "roma": "ARCOLA",
      "rm": "SP",
      "1234": 407011002
    },
    {
      "1": 3459,
      "roma": "ARCOLE",
      "rm": "VR",
      "1234": 405023004
    },
    {
      "1": 3460,
      "roma": "ARCONATE",
      "rm": "MI",
      "1234": 403015007
    },
    {
      "1": 3461,
      "roma": "ARCUGNANO",
      "rm": "VI",
      "1234": 405024006
    },
    {
      "1": 3462,
      "roma": "ARDARA",
      "rm": "SS",
      "1234": 420090005
    },
    {
      "1": 3463,
      "roma": "ARDAULI",
      "rm": "OR",
      "1234": 420095007
    },
    {
      "1": 3464,
      "roma": "ARDEA",
      "rm": "RM",
      "1234": 412058117
    },
    {
      "1": 3465,
      "roma": "ARDENNO",
      "rm": "SO",
      "1234": 403014005
    },
    {
      "1": 3466,
      "roma": "ARDESIO",
      "rm": "BG",
      "1234": 403016012
    },
    {
      "1": 3467,
      "roma": "ARDORE",
      "rm": "RC",
      "1234": 418080005
    },
    {
      "1": 3468,
      "roma": "ARENA",
      "rm": "VV",
      "1234": 418102002
    },
    {
      "1": 3469,
      "roma": "ARENA PO",
      "rm": "PV",
      "1234": 403018005
    },
    {
      "1": 3470,
      "roma": "ARENZANO",
      "rm": "GE",
      "1234": 407010001
    },
    {
      "1": 3471,
      "roma": "ARCHI",
      "rm": "CH",
      "1234": 413069002
    },
    {
      "1": 3472,
      "roma": "ALPAGO",
      "rm": "BL",
      "1234": 405025072
    },
    {
      "1": 3473,
      "roma": "ARCIDOSSO",
      "rm": "GR",
      "1234": 409053001
    },
    {
      "1": 3474,
      "roma": "ARCINAZZO ROMANO",
      "rm": "RM",
      "1234": 412058008
    },
    {
      "1": 3475,
      "roma": "ARCORE",
      "rm": "MB",
      "1234": 403108004
    },
    {
      "1": 3476,
      "roma": "ARGELATO",
      "rm": "BO",
      "1234": 408037002
    },
    {
      "1": 3477,
      "roma": "ARGENTA",
      "rm": "FE",
      "1234": 408038001
    },
    {
      "1": 3478,
      "roma": "ARGENTERA",
      "rm": "CN",
      "1234": 401004006
    },
    {
      "1": 3479,
      "roma": "ARGUELLO",
      "rm": "CN",
      "1234": 401004007
    },
    {
      "1": 3480,
      "roma": "ARGUSTO",
      "rm": "CZ",
      "1234": 418079007
    },
    {
      "1": 3481,
      "roma": "ARI",
      "rm": "CH",
      "1234": 413069003
    },
    {
      "1": 3482,
      "roma": "ARIANO IRPINO",
      "rm": "AV",
      "1234": 415064005
    },
    {
      "1": 3483,
      "roma": "ARIANO NEL POLESINE",
      "rm": "RO",
      "1234": 405029002
    },
    {
      "1": 3484,
      "roma": "ARICCIA",
      "rm": "RM",
      "1234": 412058009
    },
    {
      "1": 3485,
      "roma": "ARIELLI",
      "rm": "CH",
      "1234": 413069004
    },
    {
      "1": 3486,
      "roma": "ARIENZO",
      "rm": "CE",
      "1234": 415061004
    },
    {
      "1": 3487,
      "roma": "ARIGNANO",
      "rm": "TO",
      "1234": 401001012
    },
    {
      "1": 3488,
      "roma": "ARITZO",
      "rm": "NU",
      "1234": 420091001
    },
    {
      "1": 3489,
      "roma": "ARIZZANO",
      "rm": "VB",
      "1234": 401103003
    },
    {
      "1": 3490,
      "roma": "ARESE",
      "rm": "MI",
      "1234": 403015009
    },
    {
      "1": 3491,
      "roma": "ARGEGNO",
      "rm": "CO",
      "1234": 403013011
    },
    {
      "1": 3492,
      "roma": "ARMENO",
      "rm": "NO",
      "1234": 401003006
    },
    {
      "1": 3493,
      "roma": "ARMENTO",
      "rm": "PZ",
      "1234": 417076005
    },
    {
      "1": 3494,
      "roma": "ARMO",
      "rm": "IM",
      "1234": 407008004
    },
    {
      "1": 3495,
      "roma": "ARMUNGIA",
      "rm": "CA",
      "1234": 420092002
    },
    {
      "1": 3496,
      "roma": "ARNAD",
      "rm": "AO",
      "1234": 402007004
    },
    {
      "1": 3497,
      "roma": "ARNARA",
      "rm": "FR",
      "1234": 412060009
    },
    {
      "1": 2039,
      "roma": "BRESCIA",
      "rm": "BS",
      "1234": 403017029
    },
    {
      "1": 2040,
      "roma": "BRINDISI DI MONTAGNA",
      "rm": "PZ",
      "1234": 417076014
    },
    {
      "1": 2041,
      "roma": "BRINZIO",
      "rm": "VA",
      "1234": 403012021
    },
    {
      "1": 2042,
      "roma": "BRIONA",
      "rm": "NO",
      "1234": 401003027
    },
    {
      "1": 2043,
      "roma": "BRIONE",
      "rm": "BS",
      "1234": 403017030
    },
    {
      "1": 2044,
      "roma": "BRISIGHELLA",
      "rm": "RA",
      "1234": 408039004
    },
    {
      "1": 2045,
      "roma": "BRIAGLIA",
      "rm": "CN",
      "1234": 401004030
    },
    {
      "1": 2046,
      "roma": "BRIATICO",
      "rm": "VV",
      "1234": 418102003
    },
    {
      "1": 2047,
      "roma": "BRICHERASIO",
      "rm": "TO",
      "1234": 401001035
    },
    {
      "1": 2048,
      "roma": "BRIENNO",
      "rm": "CO",
      "1234": 403013030
    },
    {
      "1": 2049,
      "roma": "BROCCOSTELLA",
      "rm": "FR",
      "1234": 412060015
    },
    {
      "1": 2050,
      "roma": "BROGLIANO",
      "rm": "VI",
      "1234": 405024017
    },
    {
      "1": 2051,
      "roma": "BROGNATURO",
      "rm": "VV",
      "1234": 418102004
    },
    {
      "1": 2052,
      "roma": "BROLO",
      "rm": "ME",
      "1234": 419083007
    },
    {
      "1": 2053,
      "roma": "BRONDELLO",
      "rm": "CN",
      "1234": 401004032
    },
    {
      "1": 2054,
      "roma": "BRONI",
      "rm": "PV",
      "1234": 403018024
    },
    {
      "1": 2055,
      "roma": "BRINDISI",
      "rm": "BR",
      "1234": 416074001
    },
    {
      "1": 2056,
      "roma": "BRONZOLO",
      "rm": "BZ",
      "1234": 404021012
    },
    {
      "1": 2057,
      "roma": "BROSSASCO",
      "rm": "CN",
      "1234": 401004033
    },
    {
      "1": 2058,
      "roma": "BROSSO",
      "rm": "TO",
      "1234": 401001036
    },
    {
      "1": 2059,
      "roma": "BROVELLO-CARPUGNINO",
      "rm": "VB",
      "1234": 401103013
    },
    {
      "1": 2060,
      "roma": "BROZOLO",
      "rm": "TO",
      "1234": 401001037
    },
    {
      "1": 2061,
      "roma": "BRISSAGO-VALTRAVAGLIA",
      "rm": "VA",
      "1234": 403012022
    },
    {
      "1": 2062,
      "roma": "BRISSOGNE",
      "rm": "AO",
      "1234": 402007011
    },
    {
      "1": 2063,
      "roma": "BRITTOLI",
      "rm": "PE",
      "1234": 413068004
    },
    {
      "1": 2064,
      "roma": "BRIVIO",
      "rm": "LC",
      "1234": 403097010
    },
    {
      "1": 2065,
      "roma": "BRUINO",
      "rm": "TO",
      "1234": 401001038
    },
    {
      "1": 2066,
      "roma": "BRUMANO",
      "rm": "BG",
      "1234": 403016041
    },
    {
      "1": 2067,
      "roma": "BRUNATE",
      "rm": "CO",
      "1234": 403013032
    },
    {
      "1": 2068,
      "roma": "BRUNELLO",
      "rm": "VA",
      "1234": 403012023
    },
    {
      "1": 2069,
      "roma": "BRUNICO",
      "rm": "BZ",
      "1234": 404021013
    },
    {
      "1": 2070,
      "roma": "BRUNO",
      "rm": "AT",
      "1234": 401005010
    },
    {
      "1": 2071,
      "roma": "BRUSAPORTO",
      "rm": "BG",
      "1234": 403016042
    },
    {
      "1": 2072,
      "roma": "BRIOSCO",
      "rm": "MB",
      "1234": 403108011
    },
    {
      "1": 2073,
      "roma": "BRONTE",
      "rm": "CT",
      "1234": 419087009
    },
    {
      "1": 2074,
      "roma": "BRUGHERIO",
      "rm": "MB",
      "1234": 403108012
    },
    {
      "1": 2075,
      "roma": "BRUSCIANO",
      "rm": "NA",
      "1234": 415063010
    },
    {
      "1": 2076,
      "roma": "BRUSIMPIANO",
      "rm": "VA",
      "1234": 403012024
    },
    {
      "1": 2077,
      "roma": "BRUSNENGO",
      "rm": "BI",
      "1234": 401096007
    },
    {
      "1": 2078,
      "roma": "BRUSSON",
      "rm": "AO",
      "1234": 402007012
    },
    {
      "1": 2079,
      "roma": "BRUGINE",
      "rm": "PD",
      "1234": 405028015
    },
    {
      "1": 2080,
      "roma": "BRUGNATO",
      "rm": "SP",
      "1234": 407011007
    },
    {
      "1": 2081,
      "roma": "BRUGNERA",
      "rm": "PN",
      "1234": 406093007
    },
    {
      "1": 2082,
      "roma": "BUCCHIANICO",
      "rm": "CH",
      "1234": 413069008
    },
    {
      "1": 2083,
      "roma": "BUCCIANO",
      "rm": "BN",
      "1234": 415062010
    },
    {
      "1": 2084,
      "roma": "BUCCINASCO",
      "rm": "MI",
      "1234": 403015036
    },
    {
      "1": 2085,
      "roma": "BUCCINO",
      "rm": "SA",
      "1234": 415065017
    },
    {
      "1": 2086,
      "roma": "BUCINE",
      "rm": "AR",
      "1234": 409051005
    },
    {
      "1": 2087,
      "roma": "BUDDUSO'",
      "rm": "SS",
      "1234": 420090017
    },
    {
      "1": 2088,
      "roma": "BUDOIA",
      "rm": "PN",
      "1234": 406093008
    },
    {
      "1": 2089,
      "roma": "BRUSASCO",
      "rm": "TO",
      "1234": 401001039
    },
    {
      "1": 2090,
      "roma": "BUDRIO",
      "rm": "BO",
      "1234": 408037008
    },
    {
      "1": 2091,
      "roma": "BUGGERRU",
      "rm": "CA",
      "1234": 420092007
    },
    {
      "1": 2092,
      "roma": "BUGGIANO",
      "rm": "PT",
      "1234": 409047003
    },
    {
      "1": 2093,
      "roma": "CELLAMARE",
      "rm": "BA",
      "1234": 416072018
    },
    {
      "1": 2094,
      "roma": "CECIMA",
      "rm": "PV",
      "1234": 403018042
    },
    {
      "1": 2095,
      "roma": "CECINA",
      "rm": "LI",
      "1234": 409049007
    },
    {
      "1": 2096,
      "roma": "CELLE LIGURE",
      "rm": "SV",
      "1234": 407009022
    },
    {
      "1": 2097,
      "roma": "CELLE DI SAN VITO",
      "rm": "FG",
      "1234": 416071019
    },
    {
      "1": 2098,
      "roma": "CELLENO",
      "rm": "VT",
      "1234": 412056019
    },
    {
      "1": 2099,
      "roma": "CELLERE",
      "rm": "VT",
      "1234": 412056020
    },
    {
      "1": 2100,
      "roma": "CELLINO ATTANASIO",
      "rm": "TE",
      "1234": 413067015
    },
    {
      "1": 2101,
      "roma": "CELLINO SAN MARCO",
      "rm": "BR",
      "1234": 416074004
    },
    {
      "1": 2102,
      "roma": "CELLOLE",
      "rm": "CE",
      "1234": 415061102
    },
    {
      "1": 2103,
      "roma": "CENADI",
      "rm": "CZ",
      "1234": 418079024
    },
    {
      "1": 2104,
      "roma": "CENATE SOPRA",
      "rm": "BG",
      "1234": 403016068
    },
    {
      "1": 2105,
      "roma": "CORNALE E BASTIDA",
      "rm": "PV",
      "1234": 403018191
    },
    {
      "1": 2106,
      "roma": "CENATE SOTTO",
      "rm": "BG",
      "1234": 403016069
    },
    {
      "1": 2107,
      "roma": "CENCENIGHE AGORDINO",
      "rm": "BL",
      "1234": 405025010
    },
    {
      "1": 2108,
      "roma": "CENE",
      "rm": "BG",
      "1234": 403016070
    },
    {
      "1": 2109,
      "roma": "CELLARA",
      "rm": "CS",
      "1234": 418078035
    },
    {
      "1": 2110,
      "roma": "CELLARENGO",
      "rm": "AT",
      "1234": 401005033
    },
    {
      "1": 2111,
      "roma": "CELLATICA",
      "rm": "BS",
      "1234": 403017048
    },
    {
      "1": 2112,
      "roma": "CELLE DI BULGHERIA",
      "rm": "SA",
      "1234": 415065038
    },
    {
      "1": 2113,
      "roma": "CELLE DI MACRA",
      "rm": "CN",
      "1234": 401004060
    },
    {
      "1": 2114,
      "roma": "CEDEGOLO",
      "rm": "BS",
      "1234": 403017047
    },
    {
      "1": 2115,
      "roma": "CEDRASCO",
      "rm": "SO",
      "1234": 403014016
    },
    {
      "1": 2116,
      "roma": "CENTALLO",
      "rm": "CN",
      "1234": 401004061
    },
    {
      "1": 2117,
      "roma": "CENTO",
      "rm": "FE",
      "1234": 408038004
    },
    {
      "1": 2118,
      "roma": "CENTOLA",
      "rm": "SA",
      "1234": 415065039
    },
    {
      "1": 2119,
      "roma": "CENTRACHE",
      "rm": "CZ",
      "1234": 418079025
    },
    {
      "1": 2120,
      "roma": "CENTURIPE",
      "rm": "EN",
      "1234": 419086007
    },
    {
      "1": 2121,
      "roma": "CEPAGATTI",
      "rm": "PE",
      "1234": 413068011
    },
    {
      "1": 2122,
      "roma": "CEPPALONI",
      "rm": "BN",
      "1234": 415062022
    },
    {
      "1": 2123,
      "roma": "CEPPO MORELLI",
      "rm": "VB",
      "1234": 401103021
    },
    {
      "1": 2124,
      "roma": "CEPRANO",
      "rm": "FR",
      "1234": 412060025
    },
    {
      "1": 2125,
      "roma": "CERAMI",
      "rm": "EN",
      "1234": 419086008
    },
    {
      "1": 2126,
      "roma": "CERANESI",
      "rm": "GE",
      "1234": 407010014
    },
    {
      "1": 2127,
      "roma": "CEMBRA LISIGNAGO",
      "rm": "TN",
      "1234": 404022241
    },
    {
      "1": 2128,
      "roma": "CENESELLI",
      "rm": "RO",
      "1234": 405029014
    },
    {
      "1": 2129,
      "roma": "CENGIO",
      "rm": "SV",
      "1234": 407009023
    },
    {
      "1": 2130,
      "roma": "CELLE ENOMONDO",
      "rm": "AT",
      "1234": 401005034
    },
    {
      "1": 2131,
      "roma": "CERCHIO",
      "rm": "AQ",
      "1234": 413066033
    },
    {
      "1": 2132,
      "roma": "CERCINO",
      "rm": "SO",
      "1234": 403014017
    },
    {
      "1": 2133,
      "roma": "CERCIVENTO",
      "rm": "UD",
      "1234": 406030022
    },
    {
      "1": 2134,
      "roma": "CERCOLA",
      "rm": "NA",
      "1234": 415063026
    },
    {
      "1": 2135,
      "roma": "CERDA",
      "rm": "PA",
      "1234": 419082028
    },
    {
      "1": 2136,
      "roma": "CEREA",
      "rm": "VR",
      "1234": 405023025
    },
    {
      "1": 2137,
      "roma": "CEREGNANO",
      "rm": "RO",
      "1234": 405029015
    },
    {
      "1": 2138,
      "roma": "CERENZIA",
      "rm": "KR",
      "1234": 418101006
    },
    {
      "1": 2139,
      "roma": "CERES",
      "rm": "TO",
      "1234": 401001072
    },
    {
      "1": 2140,
      "roma": "CERESARA",
      "rm": "MN",
      "1234": 403020019
    },
    {
      "1": 2141,
      "roma": "CERESETO",
      "rm": "AL",
      "1234": 401006057
    },
    {
      "1": 2142,
      "roma": "CERESOLE ALBA",
      "rm": "CN",
      "1234": 401004062
    },
    {
      "1": 2143,
      "roma": "CERESOLE REALE",
      "rm": "TO",
      "1234": 401001073
    },
    {
      "1": 2144,
      "roma": "CERETE",
      "rm": "BG",
      "1234": 403016071
    },
    {
      "1": 2145,
      "roma": "CERANO",
      "rm": "NO",
      "1234": 401003049
    },
    {
      "1": 2146,
      "roma": "CERANO D'INTELVI",
      "rm": "CO",
      "1234": 403013063
    },
    {
      "1": 2147,
      "roma": "CERANOVA",
      "rm": "PV",
      "1234": 403018043
    },
    {
      "1": 2148,
      "roma": "CERASO",
      "rm": "SA",
      "1234": 415065040
    },
    {
      "1": 2149,
      "roma": "CERCEMAGGIORE",
      "rm": "CB",
      "1234": 414070017
    },
    {
      "1": 2150,
      "roma": "CERCENASCO",
      "rm": "TO",
      "1234": 401001071
    },
    {
      "1": 2151,
      "roma": "CERCEPICCOLA",
      "rm": "CB",
      "1234": 414070018
    },
    {
      "1": 2152,
      "roma": "CERCHIARA DI CALABRIA",
      "rm": "CS",
      "1234": 418078036
    },
    {
      "1": 2153,
      "roma": "CERMES",
      "rm": "BZ",
      "1234": 404021020
    },
    {
      "1": 2154,
      "roma": "CERMIGNANO",
      "rm": "TE",
      "1234": 413067016
    },
    {
      "1": 2155,
      "roma": "CERNOBBIO",
      "rm": "CO",
      "1234": 403013065
    },
    {
      "1": 2156,
      "roma": "CERNUSCO LOMBARDONE",
      "rm": "LC",
      "1234": 403097020
    },
    {
      "1": 2157,
      "roma": "CERNUSCO SUL NAVIGLIO",
      "rm": "MI",
      "1234": 403015070
    },
    {
      "1": 2158,
      "roma": "CERRETO D'ASTI",
      "rm": "AT",
      "1234": 401005035
    },
    {
      "1": 2159,
      "roma": "CERRETO D'ESI",
      "rm": "AN",
      "1234": 411042013
    },
    {
      "1": 2160,
      "roma": "CERRETO DI SPOLETO",
      "rm": "PG",
      "1234": 410054010
    },
    {
      "1": 2161,
      "roma": "CERRETO GRUE",
      "rm": "AL",
      "1234": 401006058
    },
    {
      "1": 2162,
      "roma": "CERRETO GUIDI",
      "rm": "FI",
      "1234": 409048011
    },
    {
      "1": 2163,
      "roma": "CERRETO LAZIALE",
      "rm": "RM",
      "1234": 412058027
    },
    {
      "1": 2164,
      "roma": "CERRETO SANNITA",
      "rm": "BN",
      "1234": 415062023
    },
    {
      "1": 2165,
      "roma": "CERETTO LOMELLINA",
      "rm": "PV",
      "1234": 403018044
    },
    {
      "1": 2166,
      "roma": "CERGNAGO",
      "rm": "PV",
      "1234": 403018045
    },
    {
      "1": 2167,
      "roma": "CERIALE",
      "rm": "SV",
      "1234": 407009024
    },
    {
      "1": 2168,
      "roma": "CERIANA",
      "rm": "IM",
      "1234": 407008016
    },
    {
      "1": 2169,
      "roma": "CERIGNALE",
      "rm": "PC",
      "1234": 408033015
    },
    {
      "1": 2170,
      "roma": "CERIGNOLA",
      "rm": "FG",
      "1234": 416071020
    },
    {
      "1": 2171,
      "roma": "CERISANO",
      "rm": "CS",
      "1234": 418078037
    },
    {
      "1": 2172,
      "roma": "CERMENATE",
      "rm": "CO",
      "1234": 403013064
    },
    {
      "1": 2173,
      "roma": "CERRO MAGGIORE",
      "rm": "MI",
      "1234": 403015072
    },
    {
      "1": 2174,
      "roma": "CERRO TANARO",
      "rm": "AT",
      "1234": 401005036
    },
    {
      "1": 2175,
      "roma": "CERRO VERONESE",
      "rm": "VR",
      "1234": 405023026
    },
    {
      "1": 2176,
      "roma": "CERSOSIMO",
      "rm": "PZ",
      "1234": 417076027
    },
    {
      "1": 2177,
      "roma": "CERTALDO",
      "rm": "FI",
      "1234": 409048012
    },
    {
      "1": 2178,
      "roma": "CERTOSA DI PAVIA",
      "rm": "PV",
      "1234": 403018046
    },
    {
      "1": 2179,
      "roma": "CERVA",
      "rm": "CZ",
      "1234": 418079027
    },
    {
      "1": 2180,
      "roma": "CERVARA DI ROMA",
      "rm": "RM",
      "1234": 412058028
    },
    {
      "1": 2181,
      "roma": "CERVARESE SANTA CROCE",
      "rm": "PD",
      "1234": 405028030
    },
    {
      "1": 2182,
      "roma": "CERVARO",
      "rm": "FR",
      "1234": 412060026
    },
    {
      "1": 2183,
      "roma": "CERVASCA",
      "rm": "CN",
      "1234": 401004064
    },
    {
      "1": 2184,
      "roma": "CERVATTO",
      "rm": "VC",
      "1234": 401002041
    },
    {
      "1": 2185,
      "roma": "CERVENO",
      "rm": "BS",
      "1234": 403017049
    },
    {
      "1": 2186,
      "roma": "CERVERE",
      "rm": "CN",
      "1234": 401004065
    },
    {
      "1": 2187,
      "roma": "CERVESINA",
      "rm": "PV",
      "1234": 403018047
    },
    {
      "1": 2188,
      "roma": "CERRETTO LANGHE",
      "rm": "CN",
      "1234": 401004063
    },
    {
      "1": 2189,
      "roma": "CERRINA MONFERRATO",
      "rm": "AL",
      "1234": 401006059
    },
    {
      "1": 2190,
      "roma": "CERIANO LAGHETTO",
      "rm": "MB",
      "1234": 403108018
    },
    {
      "1": 2191,
      "roma": "CERRIONE",
      "rm": "BI",
      "1234": 401096018
    },
    {
      "1": 2192,
      "roma": "CERRO AL LAMBRO",
      "rm": "MI",
      "1234": 403015071
    },
    {
      "1": 2193,
      "roma": "CERRO AL VOLTURNO",
      "rm": "IS",
      "1234": 414094014
    },
    {
      "1": 2194,
      "roma": "CESA",
      "rm": "CE",
      "1234": 415061029
    },
    {
      "1": 2195,
      "roma": "CESANA BRIANZA",
      "rm": "LC",
      "1234": 403097021
    },
    {
      "1": 2196,
      "roma": "CESANA TORINESE",
      "rm": "TO",
      "1234": 401001074
    },
    {
      "1": 2197,
      "roma": "CESANO BOSCONE",
      "rm": "MI",
      "1234": 403015074
    },
    {
      "1": 2198,
      "roma": "CESARA",
      "rm": "VB",
      "1234": 401103022
    },
    {
      "1": 2199,
      "roma": "CESARO'",
      "rm": "ME",
      "1234": 419083017
    },
    {
      "1": 2200,
      "roma": "CESATE",
      "rm": "MI",
      "1234": 403015076
    },
    {
      "1": 2201,
      "roma": "CESENA",
      "rm": "FO",
      "1234": 408040007
    },
    {
      "1": 2202,
      "roma": "CESENATICO",
      "rm": "FO",
      "1234": 408040008
    },
    {
      "1": 2203,
      "roma": "CERVETERI",
      "rm": "RM",
      "1234": 412058029
    },
    {
      "1": 2204,
      "roma": "CERVIA",
      "rm": "RA",
      "1234": 408039007
    },
    {
      "1": 2205,
      "roma": "CERVICATI",
      "rm": "CS",
      "1234": 418078038
    },
    {
      "1": 2206,
      "roma": "CERVIGNANO D'ADDA",
      "rm": "LO",
      "1234": 403098018
    },
    {
      "1": 2207,
      "roma": "CERVIGNANO DEL FRIULI",
      "rm": "UD",
      "1234": 406030023
    },
    {
      "1": 2208,
      "roma": "CERVINARA",
      "rm": "AV",
      "1234": 415064025
    },
    {
      "1": 2209,
      "roma": "CERVINO",
      "rm": "CE",
      "1234": 415061028
    },
    {
      "1": 2210,
      "roma": "CERVO",
      "rm": "IM",
      "1234": 407008017
    },
    {
      "1": 2211,
      "roma": "CERZETO",
      "rm": "CS",
      "1234": 418078039
    },
    {
      "1": 2212,
      "roma": "CESSOLE",
      "rm": "AT",
      "1234": 401005037
    },
    {
      "1": 2213,
      "roma": "CETARA",
      "rm": "SA",
      "1234": 415065041
    },
    {
      "1": 2214,
      "roma": "CETO",
      "rm": "BS",
      "1234": 403017050
    },
    {
      "1": 2215,
      "roma": "CETONA",
      "rm": "SI",
      "1234": 409052008
    },
    {
      "1": 2216,
      "roma": "CETRARO",
      "rm": "CS",
      "1234": 418078040
    },
    {
      "1": 2217,
      "roma": "CEVA",
      "rm": "CN",
      "1234": 401004066
    },
    {
      "1": 2218,
      "roma": "CEVO",
      "rm": "BS",
      "1234": 403017051
    },
    {
      "1": 2219,
      "roma": "CHALLAND-SAINT-ANSELME",
      "rm": "AO",
      "1234": 402007013
    },
    {
      "1": 2220,
      "roma": "CHALLAND-SAINT-VICTOR",
      "rm": "AO",
      "1234": 402007014
    },
    {
      "1": 2221,
      "roma": "CHAMBAVE",
      "rm": "AO",
      "1234": 402007015
    },
    {
      "1": 2222,
      "roma": "CHAMOIS",
      "rm": "AO",
      "1234": 402007016
    },
    {
      "1": 2223,
      "roma": "CHAMPDEPRAZ",
      "rm": "AO",
      "1234": 402007017
    },
    {
      "1": 2224,
      "roma": "CESINALI",
      "rm": "AV",
      "1234": 415064026
    },
    {
      "1": 2225,
      "roma": "CESIO",
      "rm": "IM",
      "1234": 407008018
    },
    {
      "1": 2226,
      "roma": "CESIOMAGGIORE",
      "rm": "BL",
      "1234": 405025011
    },
    {
      "1": 2227,
      "roma": "CESSALTO",
      "rm": "TV",
      "1234": 405026015
    },
    {
      "1": 2228,
      "roma": "CESSANITI",
      "rm": "VV",
      "1234": 418102006
    },
    {
      "1": 2229,
      "roma": "CESANO MADERNO",
      "rm": "MB",
      "1234": 403108019
    },
    {
      "1": 2230,
      "roma": "CESSAPALOMBO",
      "rm": "MC",
      "1234": 411043011
    },
    {
      "1": 2231,
      "roma": "CHIANCIANO TERME",
      "rm": "SI",
      "1234": 409052009
    },
    {
      "1": 2232,
      "roma": "CHIANNI",
      "rm": "PI",
      "1234": 409050012
    },
    {
      "1": 2233,
      "roma": "CHIANOCCO",
      "rm": "TO",
      "1234": 401001076
    },
    {
      "1": 2234,
      "roma": "CHIARAMONTE GULFI",
      "rm": "RG",
      "1234": 419088002
    },
    {
      "1": 2235,
      "roma": "CHIARAMONTI",
      "rm": "SS",
      "1234": 420090025
    },
    {
      "1": 2236,
      "roma": "CHIARANO",
      "rm": "TV",
      "1234": 405026016
    },
    {
      "1": 2237,
      "roma": "CHIARAVALLE",
      "rm": "AN",
      "1234": 411042014
    },
    {
      "1": 2238,
      "roma": "CHIARAVALLE CENTRALE",
      "rm": "CZ",
      "1234": 418079029
    },
    {
      "1": 2239,
      "roma": "CHIARI",
      "rm": "BS",
      "1234": 403017052
    },
    {
      "1": 2240,
      "roma": "CHIAROMONTE",
      "rm": "PZ",
      "1234": 417076028
    },
    {
      "1": 2241,
      "roma": "CHIAUCI",
      "rm": "IS",
      "1234": 414094015
    },
    {
      "1": 2242,
      "roma": "CHAMPORCHER",
      "rm": "AO",
      "1234": 402007018
    },
    {
      "1": 2243,
      "roma": "CHARVENSOD",
      "rm": "AO",
      "1234": 402007019
    },
    {
      "1": 2244,
      "roma": "CHATILLON",
      "rm": "AO",
      "1234": 402007020
    },
    {
      "1": 2245,
      "roma": "CHERASCO",
      "rm": "CN",
      "1234": 401004067
    },
    {
      "1": 2246,
      "roma": "CHEREMULE",
      "rm": "SS",
      "1234": 420090024
    },
    {
      "1": 2247,
      "roma": "CHIES D'ALPAGO",
      "rm": "BL",
      "1234": 405025012
    },
    {
      "1": 2248,
      "roma": "CHIALAMBERTO",
      "rm": "TO",
      "1234": 401001075
    },
    {
      "1": 2249,
      "roma": "CHIAMPO",
      "rm": "VI",
      "1234": 405024029
    },
    {
      "1": 2250,
      "roma": "CHIANCHE",
      "rm": "AV",
      "1234": 415064027
    },
    {
      "1": 2251,
      "roma": "CHIETI",
      "rm": "CH",
      "1234": 413069022
    },
    {
      "1": 2252,
      "roma": "CHIEUTI",
      "rm": "FG",
      "1234": 416071021
    },
    {
      "1": 2253,
      "roma": "CHIEVE",
      "rm": "CR",
      "1234": 403019029
    },
    {
      "1": 2254,
      "roma": "CHIGNOLO D'ISOLA",
      "rm": "BG",
      "1234": 403016072
    },
    {
      "1": 2255,
      "roma": "CHIGNOLO PO",
      "rm": "PV",
      "1234": 403018048
    },
    {
      "1": 2256,
      "roma": "CHIOGGIA",
      "rm": "VE",
      "1234": 405027008
    },
    {
      "1": 2257,
      "roma": "CHIOMONTE",
      "rm": "TO",
      "1234": 401001080
    },
    {
      "1": 2258,
      "roma": "CHIONS",
      "rm": "PN",
      "1234": 406093013
    },
    {
      "1": 2259,
      "roma": "CHIOPRIS VISCONE",
      "rm": "UD",
      "1234": 406030024
    },
    {
      "1": 2260,
      "roma": "CHIAVARI",
      "rm": "GE",
      "1234": 407010015
    },
    {
      "1": 2261,
      "roma": "CHIAVENNA",
      "rm": "SO",
      "1234": 403014018
    },
    {
      "1": 2262,
      "roma": "CHIAVERANO",
      "rm": "TO",
      "1234": 401001077
    },
    {
      "1": 2263,
      "roma": "CHIENES",
      "rm": "BZ",
      "1234": 404021021
    },
    {
      "1": 2264,
      "roma": "CHIERI",
      "rm": "TO",
      "1234": 401001078
    },
    {
      "1": 2265,
      "roma": "CHIUSA",
      "rm": "BZ",
      "1234": 404021022
    },
    {
      "1": 2266,
      "roma": "CHIESA IN VALMALENCO",
      "rm": "SO",
      "1234": 403014019
    },
    {
      "1": 2267,
      "roma": "CHIESANUOVA",
      "rm": "TO",
      "1234": 401001079
    },
    {
      "1": 2268,
      "roma": "CHIESINA UZZANESE",
      "rm": "PT",
      "1234": 409047022
    },
    {
      "1": 2269,
      "roma": "CHIUSAFORTE",
      "rm": "UD",
      "1234": 406030025
    },
    {
      "1": 2270,
      "roma": "CHIUSANICO",
      "rm": "IM",
      "1234": 407008019
    },
    {
      "1": 2271,
      "roma": "CHIUSANO D'ASTI",
      "rm": "AT",
      "1234": 401005038
    },
    {
      "1": 2272,
      "roma": "CHIUSANO DI SAN DOMENICO",
      "rm": "AV",
      "1234": 415064028
    },
    {
      "1": 2273,
      "roma": "CHIUSAVECCHIA",
      "rm": "IM",
      "1234": 407008020
    },
    {
      "1": 2274,
      "roma": "CHIUSDINO",
      "rm": "SI",
      "1234": 409052010
    },
    {
      "1": 2275,
      "roma": "CHIUSI",
      "rm": "SI",
      "1234": 409052011
    },
    {
      "1": 2276,
      "roma": "CHIUSI DELLA VERNA",
      "rm": "AR",
      "1234": 409051015
    },
    {
      "1": 2277,
      "roma": "CHIVASSO",
      "rm": "TO",
      "1234": 401001082
    },
    {
      "1": 2278,
      "roma": "CIAMPINO",
      "rm": "RM",
      "1234": 412058118
    },
    {
      "1": 2279,
      "roma": "CHITIGNANO",
      "rm": "AR",
      "1234": 409051014
    },
    {
      "1": 2280,
      "roma": "CHIUDUNO",
      "rm": "BG",
      "1234": 403016073
    },
    {
      "1": 2281,
      "roma": "CHIUPPANO",
      "rm": "VI",
      "1234": 405024030
    },
    {
      "1": 2282,
      "roma": "CHIURO",
      "rm": "SO",
      "1234": 403014020
    },
    {
      "1": 2283,
      "roma": "CICERALE",
      "rm": "SA",
      "1234": 415065042
    },
    {
      "1": 2284,
      "roma": "CHIUSA DI PESIO",
      "rm": "CN",
      "1234": 401004068
    },
    {
      "1": 2285,
      "roma": "CHIUSA DI SAN MICHELE",
      "rm": "TO",
      "1234": 401001081
    },
    {
      "1": 2286,
      "roma": "CHIUSA SCLAFANI",
      "rm": "PA",
      "1234": 419082029
    },
    {
      "1": 2287,
      "roma": "CIGLIANO",
      "rm": "VC",
      "1234": 401002042
    },
    {
      "1": 2288,
      "roma": "CIGLIE'",
      "rm": "CN",
      "1234": 401004069
    },
    {
      "1": 2289,
      "roma": "CIGOGNOLA",
      "rm": "PV",
      "1234": 403018049
    },
    {
      "1": 2290,
      "roma": "CIGOLE",
      "rm": "BS",
      "1234": 403017053
    },
    {
      "1": 2291,
      "roma": "CILAVEGNA",
      "rm": "PV",
      "1234": 403018050
    },
    {
      "1": 2292,
      "roma": "CIMADOLMO",
      "rm": "TV",
      "1234": 405026017
    },
    {
      "1": 2293,
      "roma": "CIMBERGO",
      "rm": "BS",
      "1234": 403017054
    },
    {
      "1": 2294,
      "roma": "CIANCIANA",
      "rm": "AG",
      "1234": 419084015
    },
    {
      "1": 2295,
      "roma": "CIBIANA DI CADORE",
      "rm": "BL",
      "1234": 405025013
    },
    {
      "1": 2296,
      "roma": "CICAGNA",
      "rm": "GE",
      "1234": 407010016
    },
    {
      "1": 2297,
      "roma": "CICALA",
      "rm": "CZ",
      "1234": 418079030
    },
    {
      "1": 2298,
      "roma": "CICCIANO",
      "rm": "NA",
      "1234": 415063027
    },
    {
      "1": 2299,
      "roma": "CICILIANO",
      "rm": "RM",
      "1234": 412058030
    },
    {
      "1": 2300,
      "roma": "CICOGNOLO",
      "rm": "CR",
      "1234": 403019030
    },
    {
      "1": 2301,
      "roma": "CICONIO",
      "rm": "TO",
      "1234": 401001083
    },
    {
      "1": 2302,
      "roma": "CINGIA DE' BOTTI",
      "rm": "CR",
      "1234": 403019031
    },
    {
      "1": 2303,
      "roma": "CINGOLI",
      "rm": "MC",
      "1234": 411043012
    },
    {
      "1": 2304,
      "roma": "CINIGIANO",
      "rm": "GR",
      "1234": 409053007
    },
    {
      "1": 2305,
      "roma": "CINISELLO BALSAMO",
      "rm": "MI",
      "1234": 403015077
    },
    {
      "1": 2306,
      "roma": "CINISI",
      "rm": "PA",
      "1234": 419082031
    },
    {
      "1": 2307,
      "roma": "CINO",
      "rm": "SO",
      "1234": 403014021
    },
    {
      "1": 2308,
      "roma": "CINQUEFRONDI",
      "rm": "RC",
      "1234": 418080027
    },
    {
      "1": 2309,
      "roma": "CINTANO",
      "rm": "TO",
      "1234": 401001084
    },
    {
      "1": 2310,
      "roma": "CINTE TESINO",
      "rm": "TN",
      "1234": 404022059
    },
    {
      "1": 2311,
      "roma": "CINTO CAOMAGGIORE",
      "rm": "VE",
      "1234": 405027009
    },
    {
      "1": 2312,
      "roma": "CINTO EUGANEO",
      "rm": "PD",
      "1234": 405028031
    },
    {
      "1": 2313,
      "roma": "CINZANO",
      "rm": "TO",
      "1234": 401001085
    },
    {
      "1": 2314,
      "roma": "CIORLANO",
      "rm": "CE",
      "1234": 415061030
    },
    {
      "1": 2315,
      "roma": "CIPRESSA",
      "rm": "IM",
      "1234": 407008021
    },
    {
      "1": 2316,
      "roma": "CIMINA'",
      "rm": "RC",
      "1234": 418080026
    },
    {
      "1": 2317,
      "roma": "CIMINNA",
      "rm": "PA",
      "1234": 419082030
    },
    {
      "1": 2318,
      "roma": "CIMITILE",
      "rm": "NA",
      "1234": 415063028
    },
    {
      "1": 2319,
      "roma": "CIMOLAIS",
      "rm": "PN",
      "1234": 406093014
    },
    {
      "1": 2320,
      "roma": "CIMONE",
      "rm": "TN",
      "1234": 404022058
    },
    {
      "1": 2321,
      "roma": "CIRO'",
      "rm": "KR",
      "1234": 418101007
    },
    {
      "1": 2322,
      "roma": "CINAGLIO",
      "rm": "AT",
      "1234": 401005039
    },
    {
      "1": 2323,
      "roma": "CINETO ROMANO",
      "rm": "RM",
      "1234": 412058031
    },
    {
      "1": 2324,
      "roma": "CIS",
      "rm": "TN",
      "1234": 404022060
    },
    {
      "1": 2325,
      "roma": "CISANO BERGAMASCO",
      "rm": "BG",
      "1234": 403016074
    },
    {
      "1": 2326,
      "roma": "CISANO SUL NEVA",
      "rm": "SV",
      "1234": 407009025
    },
    {
      "1": 2327,
      "roma": "CISERANO",
      "rm": "BG",
      "1234": 403016075
    },
    {
      "1": 2328,
      "roma": "CISLAGO",
      "rm": "VA",
      "1234": 403012050
    },
    {
      "1": 2329,
      "roma": "CISLIANO",
      "rm": "MI",
      "1234": 403015078
    },
    {
      "1": 2330,
      "roma": "CISON DI VALMARINO",
      "rm": "TV",
      "1234": 405026018
    },
    {
      "1": 2331,
      "roma": "CISSONE",
      "rm": "CN",
      "1234": 401004070
    },
    {
      "1": 2332,
      "roma": "CISTERNA D'ASTI",
      "rm": "AT",
      "1234": 401005040
    },
    {
      "1": 2333,
      "roma": "CISTERNA DI LATINA",
      "rm": "LT",
      "1234": 412059005
    },
    {
      "1": 2334,
      "roma": "CISTERNINO",
      "rm": "BR",
      "1234": 416074005
    },
    {
      "1": 2335,
      "roma": "CIRCELLO",
      "rm": "BN",
      "1234": 415062024
    },
    {
      "1": 2336,
      "roma": "CIRIE'",
      "rm": "TO",
      "1234": 401001086
    },
    {
      "1": 2337,
      "roma": "CIRIGLIANO",
      "rm": "MT",
      "1234": 417077005
    },
    {
      "1": 2338,
      "roma": "CIRIMIDO",
      "rm": "CO",
      "1234": 403013068
    },
    {
      "1": 2339,
      "roma": "CITTANOVA",
      "rm": "RC",
      "1234": 418080028
    },
    {
      "1": 2340,
      "roma": "CIRO' MARINA",
      "rm": "KR",
      "1234": 418101008
    },
    {
      "1": 2341,
      "roma": "CIVATE",
      "rm": "LC",
      "1234": 403097022
    },
    {
      "1": 2342,
      "roma": "CIVEZZA",
      "rm": "IM",
      "1234": 407008022
    },
    {
      "1": 2343,
      "roma": "CIVEZZANO",
      "rm": "TN",
      "1234": 404022061
    },
    {
      "1": 2344,
      "roma": "CIVIASCO",
      "rm": "VC",
      "1234": 401002043
    },
    {
      "1": 2345,
      "roma": "CIVIDALE DEL FRIULI",
      "rm": "UD",
      "1234": 406030026
    },
    {
      "1": 2346,
      "roma": "CIVIDATE AL PIANO",
      "rm": "BG",
      "1234": 403016076
    },
    {
      "1": 2347,
      "roma": "CIVIDATE CAMUNO",
      "rm": "BS",
      "1234": 403017055
    },
    {
      "1": 2348,
      "roma": "CIVITA",
      "rm": "CS",
      "1234": 418078041
    },
    {
      "1": 2349,
      "roma": "CIVITA CASTELLANA",
      "rm": "VT",
      "1234": 412056021
    },
    {
      "1": 2350,
      "roma": "CIVITA D'ANTINO",
      "rm": "AQ",
      "1234": 413066034
    },
    {
      "1": 2351,
      "roma": "CITERNA",
      "rm": "PG",
      "1234": 410054011
    },
    {
      "1": 2352,
      "roma": "CITTA' DELLA PIEVE",
      "rm": "PG",
      "1234": 410054012
    },
    {
      "1": 2353,
      "roma": "CITTA' DI CASTELLO",
      "rm": "PG",
      "1234": 410054013
    },
    {
      "1": 2354,
      "roma": "CITTA' SANT'ANGELO",
      "rm": "PE",
      "1234": 413068012
    },
    {
      "1": 2355,
      "roma": "CITTADELLA",
      "rm": "PD",
      "1234": 405028032
    },
    {
      "1": 2356,
      "roma": "CITTADUCALE",
      "rm": "RI",
      "1234": 412057016
    },
    {
      "1": 2357,
      "roma": "CIVITELLA ALFEDENA",
      "rm": "AQ",
      "1234": 413066035
    },
    {
      "1": 2358,
      "roma": "CITTAREALE",
      "rm": "RI",
      "1234": 412057017
    },
    {
      "1": 2359,
      "roma": "CITTIGLIO",
      "rm": "VA",
      "1234": 403012051
    },
    {
      "1": 2360,
      "roma": "CIVITELLA DI ROMAGNA",
      "rm": "FO",
      "1234": 408040009
    },
    {
      "1": 2361,
      "roma": "CIVITELLA IN VAL DI CHIANA",
      "rm": "AR",
      "1234": 409051016
    },
    {
      "1": 2362,
      "roma": "CIVITELLA MESSER RAIMONDO",
      "rm": "CH",
      "1234": 413069024
    },
    {
      "1": 2363,
      "roma": "CIVITELLA PAGANICO",
      "rm": "GR",
      "1234": 409053008
    },
    {
      "1": 2364,
      "roma": "CIVITELLA ROVETO",
      "rm": "AQ",
      "1234": 413066036
    },
    {
      "1": 2365,
      "roma": "CIVITELLA SAN PAOLO",
      "rm": "RM",
      "1234": 412058033
    },
    {
      "1": 2366,
      "roma": "CIVO",
      "rm": "SO",
      "1234": 403014022
    },
    {
      "1": 2367,
      "roma": "CLAINO CON OSTENO",
      "rm": "CO",
      "1234": 403013071
    },
    {
      "1": 2368,
      "roma": "CLAUT",
      "rm": "PN",
      "1234": 406093015
    },
    {
      "1": 2369,
      "roma": "CLAUZETTO",
      "rm": "PN",
      "1234": 406093016
    },
    {
      "1": 2370,
      "roma": "CIVITACAMPOMARANO",
      "rm": "CB",
      "1234": 414070019
    },
    {
      "1": 2371,
      "roma": "CIVITALUPARELLA",
      "rm": "CH",
      "1234": 413069023
    },
    {
      "1": 2372,
      "roma": "CIVITANOVA DEL SANNIO",
      "rm": "IS",
      "1234": 414094016
    },
    {
      "1": 2373,
      "roma": "CIVITANOVA MARCHE",
      "rm": "MC",
      "1234": 411043013
    },
    {
      "1": 2374,
      "roma": "CIVITAQUANA",
      "rm": "PE",
      "1234": 413068013
    },
    {
      "1": 2375,
      "roma": "CIVITAVECCHIA",
      "rm": "RM",
      "1234": 412058032
    },
    {
      "1": 2376,
      "roma": "CIVITELLA CASANOVA",
      "rm": "PE",
      "1234": 413068014
    },
    {
      "1": 2377,
      "roma": "CIVITELLA D'AGLIANO",
      "rm": "VT",
      "1234": 412056022
    },
    {
      "1": 2378,
      "roma": "CIVITELLA DEL TRONTO",
      "rm": "TE",
      "1234": 413067017
    },
    {
      "1": 2379,
      "roma": "COAZZE",
      "rm": "TO",
      "1234": 401001089
    },
    {
      "1": 2380,
      "roma": "COAZZOLO",
      "rm": "AT",
      "1234": 401005041
    },
    {
      "1": 2381,
      "roma": "COCCAGLIO",
      "rm": "BS",
      "1234": 403017056
    },
    {
      "1": 2382,
      "roma": "COCCONATO",
      "rm": "AT",
      "1234": 401005042
    },
    {
      "1": 2383,
      "roma": "COCQUIO-TREVISAGO",
      "rm": "VA",
      "1234": 403012053
    },
    {
      "1": 2384,
      "roma": "COCULLO",
      "rm": "AQ",
      "1234": 413066037
    },
    {
      "1": 2385,
      "roma": "CODEVIGO",
      "rm": "PD",
      "1234": 405028033
    },
    {
      "1": 2386,
      "roma": "CODEVILLA",
      "rm": "PV",
      "1234": 403018051
    },
    {
      "1": 2387,
      "roma": "CODIGORO",
      "rm": "FE",
      "1234": 408038005
    },
    {
      "1": 2388,
      "roma": "CODOGNE'",
      "rm": "TV",
      "1234": 405026019
    },
    {
      "1": 2389,
      "roma": "CODOGNO",
      "rm": "LO",
      "1234": 403098019
    },
    {
      "1": 2390,
      "roma": "CLAVESANA",
      "rm": "CN",
      "1234": 401004071
    },
    {
      "1": 2391,
      "roma": "CLAVIERE",
      "rm": "TO",
      "1234": 401001087
    },
    {
      "1": 2392,
      "roma": "CLES",
      "rm": "TN",
      "1234": 404022062
    },
    {
      "1": 2393,
      "roma": "CLETO",
      "rm": "CS",
      "1234": 418078042
    },
    {
      "1": 2394,
      "roma": "CLIVIO",
      "rm": "VA",
      "1234": 403012052
    },
    {
      "1": 2395,
      "roma": "CLOZ",
      "rm": "TN",
      "1234": 404022063
    },
    {
      "1": 2396,
      "roma": "COGNE",
      "rm": "AO",
      "1234": 402007021
    },
    {
      "1": 2397,
      "roma": "CLUSONE",
      "rm": "BG",
      "1234": 403016077
    },
    {
      "1": 2398,
      "roma": "COASSOLO TORINESE",
      "rm": "TO",
      "1234": 401001088
    },
    {
      "1": 2399,
      "roma": "GAZZADA SCHIANNO",
      "rm": "VA",
      "1234": 403012073
    },
    {
      "1": 2400,
      "roma": "GAZZANIGA",
      "rm": "BG",
      "1234": 403016111
    },
    {
      "1": 2401,
      "roma": "GARNIGA TERME",
      "rm": "TN",
      "1234": 404022991
    },
    {
      "1": 2402,
      "roma": "GAZZO",
      "rm": "PD",
      "1234": 405028041
    },
    {
      "1": 2403,
      "roma": "GAZZO VERONESE",
      "rm": "VR",
      "1234": 405023037
    },
    {
      "1": 2404,
      "roma": "GASSINO TORINESE",
      "rm": "TO",
      "1234": 401001112
    },
    {
      "1": 2405,
      "roma": "GATTATICO",
      "rm": "RE",
      "1234": 408035022
    },
    {
      "1": 2406,
      "roma": "GATTEO",
      "rm": "FO",
      "1234": 408040016
    },
    {
      "1": 2407,
      "roma": "GEMMANO",
      "rm": "RN",
      "1234": 408099004
    },
    {
      "1": 2408,
      "roma": "GEMONA DEL FRIULI",
      "rm": "UD",
      "1234": 406030043
    },
    {
      "1": 2409,
      "roma": "GEMONIO",
      "rm": "VA",
      "1234": 403012074
    },
    {
      "1": 2410,
      "roma": "GAVARDO",
      "rm": "BS",
      "1234": 403017077
    },
    {
      "1": 2411,
      "roma": "GAVELLO",
      "rm": "RO",
      "1234": 405029026
    },
    {
      "1": 2412,
      "roma": "GAVERINA TERME",
      "rm": "BG",
      "1234": 403016110
    },
    {
      "1": 2413,
      "roma": "GAVI",
      "rm": "AL",
      "1234": 401006081
    },
    {
      "1": 2414,
      "roma": "GAVIGNANO",
      "rm": "RM",
      "1234": 412058041
    },
    {
      "1": 2415,
      "roma": "GAVIRATE",
      "rm": "VA",
      "1234": 403012072
    },
    {
      "1": 2416,
      "roma": "GENURI",
      "rm": "CA",
      "1234": 420092023
    },
    {
      "1": 2417,
      "roma": "GENZANO DI LUCANIA",
      "rm": "PZ",
      "1234": 417076036
    },
    {
      "1": 2418,
      "roma": "GENZANO DI ROMA",
      "rm": "RM",
      "1234": 412058043
    },
    {
      "1": 2419,
      "roma": "GERA LARIO",
      "rm": "CO",
      "1234": 403013107
    },
    {
      "1": 2420,
      "roma": "GAZZOLA",
      "rm": "PC",
      "1234": 408033022
    },
    {
      "1": 2421,
      "roma": "GAZZUOLO",
      "rm": "MN",
      "1234": 403020025
    },
    {
      "1": 2422,
      "roma": "GELA",
      "rm": "CL",
      "1234": 419085007
    },
    {
      "1": 2423,
      "roma": "GERENZAGO",
      "rm": "PV",
      "1234": 403018071
    },
    {
      "1": 2424,
      "roma": "GERENZANO",
      "rm": "VA",
      "1234": 403012075
    },
    {
      "1": 2425,
      "roma": "GERGEI",
      "rm": "NU",
      "1234": 420091030
    },
    {
      "1": 2426,
      "roma": "GENAZZANO",
      "rm": "RM",
      "1234": 412058042
    },
    {
      "1": 2427,
      "roma": "GENGA",
      "rm": "AN",
      "1234": 411042020
    },
    {
      "1": 2428,
      "roma": "GENIVOLTA",
      "rm": "CR",
      "1234": 403019047
    },
    {
      "1": 2429,
      "roma": "GENOLA",
      "rm": "CN",
      "1234": 401004096
    },
    {
      "1": 2430,
      "roma": "GENONI",
      "rm": "NU",
      "1234": 420091029
    },
    {
      "1": 2431,
      "roma": "GENOVA",
      "rm": "GE",
      "1234": 407010025
    },
    {
      "1": 2432,
      "roma": "GERMIGNAGA",
      "rm": "VA",
      "1234": 403012076
    },
    {
      "1": 2433,
      "roma": "GEROCARNE",
      "rm": "VV",
      "1234": 418102016
    },
    {
      "1": 2434,
      "roma": "GEROLA ALTA",
      "rm": "SO",
      "1234": 403014031
    },
    {
      "1": 2435,
      "roma": "GERRE DE' CAPRIOLI",
      "rm": "CR",
      "1234": 403019048
    },
    {
      "1": 2436,
      "roma": "GESICO",
      "rm": "CA",
      "1234": 420092024
    },
    {
      "1": 2437,
      "roma": "GESSATE",
      "rm": "MI",
      "1234": 403015106
    },
    {
      "1": 2438,
      "roma": "LENTATE SUL SEVESO",
      "rm": "MB",
      "1234": 403108054
    },
    {
      "1": 2439,
      "roma": "GERACE",
      "rm": "RC",
      "1234": 418080036
    },
    {
      "1": 2440,
      "roma": "GERACI SICULO",
      "rm": "PA",
      "1234": 419082037
    },
    {
      "1": 2441,
      "roma": "GERANO",
      "rm": "RM",
      "1234": 412058044
    },
    {
      "1": 2442,
      "roma": "GHEDI",
      "rm": "BS",
      "1234": 403017078
    },
    {
      "1": 2443,
      "roma": "GHEMME",
      "rm": "NO",
      "1234": 401003073
    },
    {
      "1": 2444,
      "roma": "GERMAGNANO",
      "rm": "TO",
      "1234": 401001113
    },
    {
      "1": 2445,
      "roma": "GERMAGNO",
      "rm": "VB",
      "1234": 401103032
    },
    {
      "1": 2446,
      "roma": "GHISALBA",
      "rm": "BG",
      "1234": 403016113
    },
    {
      "1": 2447,
      "roma": "GHISLARENGO",
      "rm": "VC",
      "1234": 401002062
    },
    {
      "1": 2448,
      "roma": "GIACCIANO CON BARUCHELLA",
      "rm": "RO",
      "1234": 405029027
    },
    {
      "1": 2449,
      "roma": "GIAGLIONE",
      "rm": "TO",
      "1234": 401001114
    },
    {
      "1": 2450,
      "roma": "GIANICO",
      "rm": "BS",
      "1234": 403017079
    },
    {
      "1": 2451,
      "roma": "GIANO DELL'UMBRIA",
      "rm": "PG",
      "1234": 410054021
    },
    {
      "1": 2452,
      "roma": "GIANO VETUSTO",
      "rm": "CE",
      "1234": 415061040
    },
    {
      "1": 2453,
      "roma": "GIARDINELLO",
      "rm": "PA",
      "1234": 419082038
    },
    {
      "1": 2454,
      "roma": "GIARDINI-NAXOS",
      "rm": "ME",
      "1234": 419083032
    },
    {
      "1": 2455,
      "roma": "GIAROLE",
      "rm": "AL",
      "1234": 401006082
    },
    {
      "1": 2456,
      "roma": "GIARRATANA",
      "rm": "RG",
      "1234": 419088004
    },
    {
      "1": 2457,
      "roma": "GESSOPALENA",
      "rm": "CH",
      "1234": 413069040
    },
    {
      "1": 2458,
      "roma": "GESTURI",
      "rm": "CA",
      "1234": 420092025
    },
    {
      "1": 2459,
      "roma": "GESUALDO",
      "rm": "AV",
      "1234": 415064036
    },
    {
      "1": 2460,
      "roma": "GIAVENO",
      "rm": "TO",
      "1234": 401001115
    },
    {
      "1": 2461,
      "roma": "GIAVERA DEL MONTELLO",
      "rm": "TV",
      "1234": 405026032
    },
    {
      "1": 2462,
      "roma": "GIBA",
      "rm": "CA",
      "1234": 420092026
    },
    {
      "1": 2463,
      "roma": "GHIFFA",
      "rm": "VB",
      "1234": 401103033
    },
    {
      "1": 2673,
      "roma": "GUARENE",
      "rm": "CN",
      "1234": 401004101
    },
    {
      "1": 2674,
      "roma": "GUASILA",
      "rm": "CA",
      "1234": 420092031
    },
    {
      "1": 2675,
      "roma": "HONE",
      "rm": "AO",
      "1234": 402007034
    },
    {
      "1": 2676,
      "roma": "INCUDINE",
      "rm": "BS",
      "1234": 403017083
    },
    {
      "1": 2677,
      "roma": "INDUNO OLONA",
      "rm": "VA",
      "1234": 403012083
    },
    {
      "1": 2678,
      "roma": "INGRIA",
      "rm": "TO",
      "1234": 401001121
    },
    {
      "1": 2679,
      "roma": "INTRAGNA",
      "rm": "VB",
      "1234": 401103037
    },
    {
      "1": 2680,
      "roma": "INTROBIO",
      "rm": "LC",
      "1234": 403097040
    },
    {
      "1": 2681,
      "roma": "INTROD",
      "rm": "AO",
      "1234": 402007035
    },
    {
      "1": 2682,
      "roma": "MONGIUFFI MELIA",
      "rm": "ME",
      "1234": 419083055
    },
    {
      "1": 2683,
      "roma": "MONTAGNA IN VALTELLINA",
      "rm": "SO",
      "1234": 403014044
    },
    {
      "1": 2684,
      "roma": "MONTAGNANA",
      "rm": "PD",
      "1234": 405028056
    },
    {
      "1": 2685,
      "roma": "MONTAGNAREALE",
      "rm": "ME",
      "1234": 419083056
    },
    {
      "1": 2686,
      "roma": "MONTAGUTO",
      "rm": "AV",
      "1234": 415064051
    },
    {
      "1": 2687,
      "roma": "MONTAIONE",
      "rm": "FI",
      "1234": 409048027
    },
    {
      "1": 2688,
      "roma": "MONTALBANO ELICONA",
      "rm": "ME",
      "1234": 419083057
    },
    {
      "1": 2689,
      "roma": "MONTALBANO JONICO",
      "rm": "MT",
      "1234": 417077016
    },
    {
      "1": 2690,
      "roma": "MONTALDEO",
      "rm": "AL",
      "1234": 401006103
    },
    {
      "1": 2691,
      "roma": "MONSAMPIETRO MORICO",
      "rm": "FM",
      "1234": 411109012
    },
    {
      "1": 2692,
      "roma": "MONTALDO BORMIDA",
      "rm": "AL",
      "1234": 401006104
    },
    {
      "1": 2693,
      "roma": "MONTALDO DI MONDOVI'",
      "rm": "CN",
      "1234": 401004134
    },
    {
      "1": 2694,
      "roma": "MONSANO",
      "rm": "AN",
      "1234": 411042025
    },
    {
      "1": 2695,
      "roma": "MONSELICE",
      "rm": "PD",
      "1234": 405028055
    },
    {
      "1": 2696,
      "roma": "MONSERRATO",
      "rm": "CA",
      "1234": 420092109
    },
    {
      "1": 2697,
      "roma": "MONSUMMANO TERME",
      "rm": "PT",
      "1234": 409047009
    },
    {
      "1": 2698,
      "roma": "MONTA'",
      "rm": "CN",
      "1234": 401004133
    },
    {
      "1": 2699,
      "roma": "MONTALLEGRO",
      "rm": "AG",
      "1234": 419084024
    },
    {
      "1": 2700,
      "roma": "MONTALTO DELLE MARCHE",
      "rm": "AP",
      "1234": 411044032
    },
    {
      "1": 2701,
      "roma": "MONTALTO DI CASTRO",
      "rm": "VT",
      "1234": 412056035
    },
    {
      "1": 2702,
      "roma": "MONTALTO DORA",
      "rm": "TO",
      "1234": 401001160
    },
    {
      "1": 2703,
      "roma": "MONTAGNA",
      "rm": "BZ",
      "1234": 404021053
    },
    {
      "1": 2704,
      "roma": "MONTALTO PAVESE",
      "rm": "PV",
      "1234": 403018094
    },
    {
      "1": 2705,
      "roma": "MONTALTO UFFUGO",
      "rm": "CS",
      "1234": 418078081
    },
    {
      "1": 2706,
      "roma": "MONTANARO",
      "rm": "TO",
      "1234": 401001161
    },
    {
      "1": 2707,
      "roma": "MONTANASO LOMBARDO",
      "rm": "LO",
      "1234": 403098040
    },
    {
      "1": 2708,
      "roma": "MONTANERA",
      "rm": "CN",
      "1234": 401004136
    },
    {
      "1": 2709,
      "roma": "MONTANO ANTILIA",
      "rm": "SA",
      "1234": 415065070
    },
    {
      "1": 2710,
      "roma": "MONTANO LUCINO",
      "rm": "CO",
      "1234": 403013154
    },
    {
      "1": 2711,
      "roma": "MONTAQUILA",
      "rm": "IS",
      "1234": 414094028
    },
    {
      "1": 2712,
      "roma": "MONTAPPONE",
      "rm": "FM",
      "1234": 411109013
    },
    {
      "1": 2713,
      "roma": "MONTALDO ROERO",
      "rm": "CN",
      "1234": 401004135
    },
    {
      "1": 2714,
      "roma": "MONTALDO SCARAMPI",
      "rm": "AT",
      "1234": 401005074
    },
    {
      "1": 2715,
      "roma": "MONTALDO TORINESE",
      "rm": "TO",
      "1234": 401001158
    },
    {
      "1": 2716,
      "roma": "MONTALE",
      "rm": "PT",
      "1234": 409047010
    },
    {
      "1": 2717,
      "roma": "MONTALENGHE",
      "rm": "TO",
      "1234": 401001159
    },
    {
      "1": 2718,
      "roma": "MONTE ARGENTARIO",
      "rm": "GR",
      "1234": 409053016
    },
    {
      "1": 2719,
      "roma": "MONTE CASTELLO DI VIBIO",
      "rm": "PG",
      "1234": 410054029
    },
    {
      "1": 2720,
      "roma": "MONTE CAVALLO",
      "rm": "MC",
      "1234": 411043027
    },
    {
      "1": 2721,
      "roma": "MONTE CERIGNONE",
      "rm": "PS",
      "1234": 411041031
    },
    {
      "1": 2722,
      "roma": "MONTE CREMASCO",
      "rm": "CR",
      "1234": 403019058
    },
    {
      "1": 2723,
      "roma": "MONTE DI MALO",
      "rm": "VI",
      "1234": 405024063
    },
    {
      "1": 2724,
      "roma": "MONTE DI PROCIDA",
      "rm": "NA",
      "1234": 415063047
    },
    {
      "1": 2725,
      "roma": "MONTE ISOLA",
      "rm": "BS",
      "1234": 403017111
    },
    {
      "1": 2726,
      "roma": "MONTE MARENZO",
      "rm": "LC",
      "1234": 403097052
    },
    {
      "1": 2727,
      "roma": "MONTASOLA",
      "rm": "RI",
      "1234": 412057039
    },
    {
      "1": 2728,
      "roma": "MONTAURO",
      "rm": "CZ",
      "1234": 418079080
    },
    {
      "1": 2729,
      "roma": "MONTAZZOLI",
      "rm": "CH",
      "1234": 413069051
    },
    {
      "1": 2730,
      "roma": "MONTE ROMANO",
      "rm": "VT",
      "1234": 412056037
    },
    {
      "1": 2731,
      "roma": "MONTE SAN BIAGIO",
      "rm": "LT",
      "1234": 412059015
    },
    {
      "1": 2732,
      "roma": "MONTE SAN GIACOMO",
      "rm": "SA",
      "1234": 415065075
    },
    {
      "1": 2733,
      "roma": "MONTE SAN GIOVANNI IN SABINA",
      "rm": "RI",
      "1234": 412057043
    },
    {
      "1": 2734,
      "roma": "MONTE SAN GIUSTO",
      "rm": "MC",
      "1234": 411043031
    },
    {
      "1": 2735,
      "roma": "MONTE SAN MARTINO",
      "rm": "MC",
      "1234": 411043032
    },
    {
      "1": 2736,
      "roma": "MONTE SAN PIETRO",
      "rm": "BO",
      "1234": 408037042
    },
    {
      "1": 2737,
      "roma": "MONTE SAN SAVINO",
      "rm": "AR",
      "1234": 409051025
    },
    {
      "1": 2738,
      "roma": "MONTE SAN VITO",
      "rm": "AN",
      "1234": 411042030
    },
    {
      "1": 2739,
      "roma": "MONTE SANT'ANGELO",
      "rm": "FG",
      "1234": 416071033
    },
    {
      "1": 2740,
      "roma": "MONTE SANTA MARIA TIBERINA",
      "rm": "PG",
      "1234": 410054032
    },
    {
      "1": 2741,
      "roma": "MONTEBELLO DELLA BATTAGLIA",
      "rm": "PV",
      "1234": 403018095
    },
    {
      "1": 2742,
      "roma": "MONTEBELLO DI BERTONA",
      "rm": "PE",
      "1234": 413068023
    },
    {
      "1": 2743,
      "roma": "MONTEBELLO IONICO",
      "rm": "RC",
      "1234": 418080053
    },
    {
      "1": 2744,
      "roma": "MONTE PORZIO",
      "rm": "PS",
      "1234": 411041038
    },
    {
      "1": 2745,
      "roma": "MONTE GIBERTO",
      "rm": "FM",
      "1234": 411109016
    },
    {
      "1": 2746,
      "roma": "MONTE PORZIO CATONE",
      "rm": "RM",
      "1234": 412058064
    },
    {
      "1": 2747,
      "roma": "MONTE ROBERTO",
      "rm": "AN",
      "1234": 411042029
    },
    {
      "1": 2748,
      "roma": "MONTEBRUNO",
      "rm": "GE",
      "1234": 407010038
    },
    {
      "1": 2749,
      "roma": "MONTEGRANARO",
      "rm": "FM",
      "1234": 411109018
    },
    {
      "1": 2750,
      "roma": "MONTE SAN GIOVANNI CAMPANO",
      "rm": "FR",
      "1234": 412060044
    },
    {
      "1": 2751,
      "roma": "MONTECALVO IRPINO",
      "rm": "AV",
      "1234": 415064052
    },
    {
      "1": 2752,
      "roma": "MONTECALVO VERSIGGIA",
      "rm": "PV",
      "1234": 403018096
    },
    {
      "1": 2753,
      "roma": "MONTECARLO",
      "rm": "LU",
      "1234": 409046021
    },
    {
      "1": 2754,
      "roma": "MONTECAROTTO",
      "rm": "AN",
      "1234": 411042026
    },
    {
      "1": 2755,
      "roma": "MONTECASSIANO",
      "rm": "MC",
      "1234": 411043026
    },
    {
      "1": 2756,
      "roma": "MONTECASTELLO",
      "rm": "AL",
      "1234": 401006105
    },
    {
      "1": 2757,
      "roma": "MONTECASTRILLI",
      "rm": "TR",
      "1234": 410055017
    },
    {
      "1": 2758,
      "roma": "MONTECATINI VAL DI CECINA",
      "rm": "PI",
      "1234": 409050019
    },
    {
      "1": 2759,
      "roma": "MONTECATINI-TERME",
      "rm": "PT",
      "1234": 409047011
    },
    {
      "1": 2760,
      "roma": "MONTECCHIA DI CROSARA",
      "rm": "VR",
      "1234": 405023049
    },
    {
      "1": 2761,
      "roma": "MONTECCHIO",
      "rm": "TR",
      "1234": 410055018
    },
    {
      "1": 2762,
      "roma": "MONTECCHIO EMILIA",
      "rm": "RE",
      "1234": 408035027
    },
    {
      "1": 2763,
      "roma": "MONTECCHIO MAGGIORE",
      "rm": "VI",
      "1234": 405024061
    },
    {
      "1": 2764,
      "roma": "MONTECCHIO PRECALCINO",
      "rm": "VI",
      "1234": 405024062
    },
    {
      "1": 2765,
      "roma": "MONTE RINALDO",
      "rm": "FM",
      "1234": 411109021
    },
    {
      "1": 2766,
      "roma": "MONTEBELLO SUL SANGRO",
      "rm": "CH",
      "1234": 413069009
    },
    {
      "1": 2767,
      "roma": "MONTEBELLO VICENTINO",
      "rm": "VI",
      "1234": 405024060
    },
    {
      "1": 2768,
      "roma": "MONTEBELLUNA",
      "rm": "TV",
      "1234": 405026046
    },
    {
      "1": 2769,
      "roma": "MONTEBUONO",
      "rm": "RI",
      "1234": 412057040
    },
    {
      "1": 2770,
      "roma": "MONTECALVO IN FOGLIA",
      "rm": "PS",
      "1234": 411041030
    },
    {
      "1": 2771,
      "roma": "MONTECOPIOLO",
      "rm": "PS",
      "1234": 411041033
    },
    {
      "1": 2772,
      "roma": "MONTECORICE",
      "rm": "SA",
      "1234": 415065071
    },
    {
      "1": 2773,
      "roma": "MONTECORVINO PUGLIANO",
      "rm": "SA",
      "1234": 415065072
    },
    {
      "1": 2774,
      "roma": "MONTECORVINO ROVELLA",
      "rm": "SA",
      "1234": 415065073
    },
    {
      "1": 2775,
      "roma": "MONTECOSARO",
      "rm": "MC",
      "1234": 411043028
    },
    {
      "1": 2776,
      "roma": "MONTECRESTESE",
      "rm": "VB",
      "1234": 401103046
    },
    {
      "1": 2777,
      "roma": "MONTECRETO",
      "rm": "MO",
      "1234": 408036024
    },
    {
      "1": 2778,
      "roma": "MONTEDINOVE",
      "rm": "AP",
      "1234": 411044034
    },
    {
      "1": 2779,
      "roma": "MONTEDORO",
      "rm": "CL",
      "1234": 419085011
    },
    {
      "1": 2780,
      "roma": "MONTEFALCIONE",
      "rm": "AV",
      "1234": 415064053
    },
    {
      "1": 2781,
      "roma": "MONTEFALCO",
      "rm": "PG",
      "1234": 410054030
    },
    {
      "1": 2782,
      "roma": "MONTEFALCONE DI VAL FORTORE",
      "rm": "BN",
      "1234": 415062042
    },
    {
      "1": 2783,
      "roma": "MONTEFALCONE NEL SANNIO",
      "rm": "CB",
      "1234": 414070043
    },
    {
      "1": 2784,
      "roma": "MONTEFANO",
      "rm": "MC",
      "1234": 411043029
    },
    {
      "1": 2785,
      "roma": "MONTECHIARO D'ACQUI",
      "rm": "AL",
      "1234": 401006106
    },
    {
      "1": 2786,
      "roma": "MONTECHIARO D'ASTI",
      "rm": "AT",
      "1234": 401005075
    },
    {
      "1": 2787,
      "roma": "MONTECHIARUGOLO",
      "rm": "PR",
      "1234": 408034023
    },
    {
      "1": 2788,
      "roma": "MONTECILFONE",
      "rm": "CB",
      "1234": 414070042
    },
    {
      "1": 2789,
      "roma": "MONTECOMPATRI",
      "rm": "RM",
      "1234": 412058060
    },
    {
      "1": 2790,
      "roma": "MONTEFLAVIO",
      "rm": "RM",
      "1234": 412058061
    },
    {
      "1": 2791,
      "roma": "MONTEFORTE CILENTO",
      "rm": "SA",
      "1234": 415065074
    },
    {
      "1": 2792,
      "roma": "MONTEFORTE D'ALPONE",
      "rm": "VR",
      "1234": 405023050
    },
    {
      "1": 2793,
      "roma": "MONTEFORTE IRPINO",
      "rm": "AV",
      "1234": 415064054
    },
    {
      "1": 2794,
      "roma": "MONTEFRANCO",
      "rm": "TR",
      "1234": 410055019
    },
    {
      "1": 2795,
      "roma": "MONTEFREDANE",
      "rm": "AV",
      "1234": 415064055
    },
    {
      "1": 2796,
      "roma": "MONTEFUSCO",
      "rm": "AV",
      "1234": 415064056
    },
    {
      "1": 2797,
      "roma": "MONTEGABBIONE",
      "rm": "TR",
      "1234": 410055020
    },
    {
      "1": 2798,
      "roma": "MONTEGALDA",
      "rm": "VI",
      "1234": 405024064
    },
    {
      "1": 2799,
      "roma": "MONTEGALDELLA",
      "rm": "VI",
      "1234": 405024065
    },
    {
      "1": 2800,
      "roma": "MONTEGALLO",
      "rm": "AP",
      "1234": 411044038
    },
    {
      "1": 2801,
      "roma": "MONTEGIOCO",
      "rm": "AL",
      "1234": 401006107
    },
    {
      "1": 2802,
      "roma": "MONTEGIORDANO",
      "rm": "CS",
      "1234": 418078082
    },
    {
      "1": 2803,
      "roma": "MONTEFELCINO",
      "rm": "PS",
      "1234": 411041034
    },
    {
      "1": 2804,
      "roma": "MONTEFERRANTE",
      "rm": "CH",
      "1234": 413069052
    },
    {
      "1": 2805,
      "roma": "MONTEFIASCONE",
      "rm": "VT",
      "1234": 412056036
    },
    {
      "1": 2806,
      "roma": "MONTEFINO",
      "rm": "TE",
      "1234": 413067027
    },
    {
      "1": 2807,
      "roma": "MONTEFALCONE APPENNINO",
      "rm": "FM",
      "1234": 411109014
    },
    {
      "1": 2808,
      "roma": "MONTECICCARDO",
      "rm": "PS",
      "1234": 411041032
    },
    {
      "1": 2809,
      "roma": "MONTEFIORE DELL'ASO",
      "rm": "AP",
      "1234": 411044036
    },
    {
      "1": 2810,
      "roma": "MONTEFIORINO",
      "rm": "MO",
      "1234": 408036025
    },
    {
      "1": 2811,
      "roma": "MONTEGROSSO PIAN LATTE",
      "rm": "IM",
      "1234": 407008037
    },
    {
      "1": 2812,
      "roma": "MONTEGROTTO TERME",
      "rm": "PD",
      "1234": 405028057
    },
    {
      "1": 2813,
      "roma": "MONTEIASI",
      "rm": "TA",
      "1234": 416073016
    },
    {
      "1": 2814,
      "roma": "MONTELABBATE",
      "rm": "PS",
      "1234": 411041036
    },
    {
      "1": 2815,
      "roma": "MONTELANICO",
      "rm": "RM",
      "1234": 412058062
    },
    {
      "1": 2816,
      "roma": "MONTELAPIANO",
      "rm": "CH",
      "1234": 413069053
    },
    {
      "1": 2817,
      "roma": "MONTELEONE D'ORVIETO",
      "rm": "TR",
      "1234": 410055021
    },
    {
      "1": 2818,
      "roma": "MONTELEONE DI PUGLIA",
      "rm": "FG",
      "1234": 416071032
    },
    {
      "1": 2819,
      "roma": "MONTELEONE DI SPOLETO",
      "rm": "PG",
      "1234": 410054031
    },
    {
      "1": 2820,
      "roma": "MONTELEONE ROCCA DORIA",
      "rm": "SS",
      "1234": 420090040
    },
    {
      "1": 2821,
      "roma": "MONTELEONE SABINO",
      "rm": "RI",
      "1234": 412057041
    },
    {
      "1": 2822,
      "roma": "MONTELEPRE",
      "rm": "PA",
      "1234": 419082050
    },
    {
      "1": 2823,
      "roma": "MONTELIBRETTI",
      "rm": "RM",
      "1234": 412058063
    },
    {
      "1": 2824,
      "roma": "MONTELLA",
      "rm": "AV",
      "1234": 415064057
    },
    {
      "1": 2825,
      "roma": "MONTELLO",
      "rm": "BG",
      "1234": 403016139
    },
    {
      "1": 2826,
      "roma": "MONTEGRIDOLFO",
      "rm": "RN",
      "1234": 408099009
    },
    {
      "1": 2827,
      "roma": "MONTEFIORE CONCA",
      "rm": "RN",
      "1234": 408099008
    },
    {
      "1": 2828,
      "roma": "MONTEGROSSO D'ASTI",
      "rm": "AT",
      "1234": 401005076
    },
    {
      "1": 2829,
      "roma": "MONTEMAGGIORE BELSITO",
      "rm": "PA",
      "1234": 419082051
    },
    {
      "1": 2830,
      "roma": "MONTEGIORGIO",
      "rm": "FM",
      "1234": 411109017
    },
    {
      "1": 2831,
      "roma": "MONTEMAGNO",
      "rm": "AT",
      "1234": 401005077
    },
    {
      "1": 2832,
      "roma": "MONTEMALE DI CUNEO",
      "rm": "CN",
      "1234": 401004138
    },
    {
      "1": 2833,
      "roma": "MONTEMARANO",
      "rm": "AV",
      "1234": 415064058
    },
    {
      "1": 2834,
      "roma": "MONTEMARCIANO",
      "rm": "AN",
      "1234": 411042027
    },
    {
      "1": 2835,
      "roma": "MONTEMARZINO",
      "rm": "AL",
      "1234": 401006108
    },
    {
      "1": 2836,
      "roma": "MONTEMESOLA",
      "rm": "TA",
      "1234": 416073017
    },
    {
      "1": 2837,
      "roma": "MONTEMEZZO",
      "rm": "CO",
      "1234": 403013155
    },
    {
      "1": 2838,
      "roma": "MONTEMIGNAIO",
      "rm": "AR",
      "1234": 409051023
    },
    {
      "1": 2839,
      "roma": "MONTEMILETTO",
      "rm": "AV",
      "1234": 415064059
    },
    {
      "1": 2840,
      "roma": "MONTEMILONE",
      "rm": "PZ",
      "1234": 417076051
    },
    {
      "1": 2841,
      "roma": "MONTEMITRO",
      "rm": "CB",
      "1234": 414070045
    },
    {
      "1": 2842,
      "roma": "MONTEMONACO",
      "rm": "AP",
      "1234": 411044044
    },
    {
      "1": 2843,
      "roma": "MONTEMURLO",
      "rm": "PO",
      "1234": 409100003
    },
    {
      "1": 2844,
      "roma": "MONTEMURRO",
      "rm": "PZ",
      "1234": 417076052
    },
    {
      "1": 2845,
      "roma": "MONTENARS",
      "rm": "UD",
      "1234": 406030061
    },
    {
      "1": 2846,
      "roma": "MONTELONGO",
      "rm": "CB",
      "1234": 414070044
    },
    {
      "1": 2847,
      "roma": "MONTELUPO ALBESE",
      "rm": "CN",
      "1234": 401004137
    },
    {
      "1": 2848,
      "roma": "MONTELUPO FIORENTINO",
      "rm": "FI",
      "1234": 409048028
    },
    {
      "1": 2849,
      "roma": "MONTEGRINO - VALTRAVAGLIA",
      "rm": "VA",
      "1234": 403012103
    },
    {
      "1": 2850,
      "roma": "MONTEFORTINO",
      "rm": "FM",
      "1234": 411109015
    },
    {
      "1": 2851,
      "roma": "MONTEPARANO",
      "rm": "TA",
      "1234": 416073018
    },
    {
      "1": 2852,
      "roma": "MONTEPRANDONE",
      "rm": "AP",
      "1234": 411044045
    },
    {
      "1": 2853,
      "roma": "ORROLI",
      "rm": "CA",
      "1234": 420092118
    },
    {
      "1": 2854,
      "roma": "MONTEPULCIANO",
      "rm": "SI",
      "1234": 409052015
    },
    {
      "1": 2855,
      "roma": "MONTERCHI",
      "rm": "AR",
      "1234": 409051024
    },
    {
      "1": 2856,
      "roma": "MONTEREALE",
      "rm": "AQ",
      "1234": 413066056
    },
    {
      "1": 2857,
      "roma": "MONTEREALE VALCELLINA",
      "rm": "PN",
      "1234": 406093027
    },
    {
      "1": 2858,
      "roma": "MONTERENZIO",
      "rm": "BO",
      "1234": 408037041
    },
    {
      "1": 2859,
      "roma": "MONTERIGGIONI",
      "rm": "SI",
      "1234": 409052016
    },
    {
      "1": 2860,
      "roma": "MONTERODUNI",
      "rm": "IS",
      "1234": 414094030
    },
    {
      "1": 2861,
      "roma": "MONTERONI D'ARBIA",
      "rm": "SI",
      "1234": 409052017
    },
    {
      "1": 2862,
      "roma": "MONTERONI DI LECCE",
      "rm": "LE",
      "1234": 416075048
    },
    {
      "1": 2863,
      "roma": "MONTEROSI",
      "rm": "VT",
      "1234": 412056038
    },
    {
      "1": 2864,
      "roma": "MONTENERO DI BISACCIA",
      "rm": "CB",
      "1234": 414070046
    },
    {
      "1": 3498,
      "roma": "ARNASCO",
      "rm": "SV",
      "1234": 407009007
    },
    {
      "1": 3499,
      "roma": "ARNESANO",
      "rm": "LE",
      "1234": 416075007
    },
    {
      "1": 3500,
      "roma": "AROLA",
      "rm": "VB",
      "1234": 401103004
    },
    {
      "1": 3501,
      "roma": "ARONA",
      "rm": "NO",
      "1234": 401003008
    },
    {
      "1": 3502,
      "roma": "AROSIO",
      "rm": "CO",
      "1234": 403013012
    },
    {
      "1": 3503,
      "roma": "ARPAIA",
      "rm": "BN",
      "1234": 415062005
    },
    {
      "1": 3504,
      "roma": "ARPAISE",
      "rm": "BN",
      "1234": 415062006
    },
    {
      "1": 3505,
      "roma": "ARPINO",
      "rm": "FR",
      "1234": 412060010
    },
    {
      "1": 3506,
      "roma": "ARQUA' PETRARCA",
      "rm": "PD",
      "1234": 405028005
    },
    {
      "1": 3507,
      "roma": "AREZZO",
      "rm": "AR",
      "1234": 409051002
    },
    {
      "1": 3508,
      "roma": "ARLUNO",
      "rm": "MI",
      "1234": 403015010
    },
    {
      "1": 3509,
      "roma": "ARSAGO SEPRIO",
      "rm": "VA",
      "1234": 403012005
    },
    {
      "1": 3510,
      "roma": "ARSIE'",
      "rm": "BL",
      "1234": 405025004
    },
    {
      "1": 3511,
      "roma": "ARSIERO",
      "rm": "VI",
      "1234": 405024007
    },
    {
      "1": 3512,
      "roma": "ARSITA",
      "rm": "TE",
      "1234": 413067003
    },
    {
      "1": 3513,
      "roma": "ARSOLI",
      "rm": "RM",
      "1234": 412058010
    },
    {
      "1": 3514,
      "roma": "ARTA TERME",
      "rm": "UD",
      "1234": 406030005
    },
    {
      "1": 3515,
      "roma": "ARTEGNA",
      "rm": "UD",
      "1234": 406030006
    },
    {
      "1": 3516,
      "roma": "ARTENA",
      "rm": "RM",
      "1234": 412058011
    },
    {
      "1": 3517,
      "roma": "ARTOGNE",
      "rm": "BS",
      "1234": 403017007
    },
    {
      "1": 3518,
      "roma": "ARVIER",
      "rm": "AO",
      "1234": 402007005
    },
    {
      "1": 3519,
      "roma": "ARZACHENA",
      "rm": "SS",
      "1234": 420090006
    },
    {
      "1": 3520,
      "roma": "ARZAGO D'ADDA",
      "rm": "BG",
      "1234": 403016013
    },
    {
      "1": 3521,
      "roma": "ARZANA",
      "rm": "NU",
      "1234": 420091002
    },
    {
      "1": 3522,
      "roma": "ARZANO",
      "rm": "NA",
      "1234": 415063005
    },
    {
      "1": 3523,
      "roma": "ARZERGRANDE",
      "rm": "PD",
      "1234": 405028007
    },
    {
      "1": 3524,
      "roma": "ARZIGNANO",
      "rm": "VI",
      "1234": 405024008
    },
    {
      "1": 3525,
      "roma": "ARQUA' POLESINE",
      "rm": "RO",
      "1234": 405029003
    },
    {
      "1": 3526,
      "roma": "ARQUATA DEL TRONTO",
      "rm": "AP",
      "1234": 411044006
    },
    {
      "1": 3527,
      "roma": "ARLENA DI CASTRO",
      "rm": "VT",
      "1234": 412056002
    },
    {
      "1": 3528,
      "roma": "ARRE",
      "rm": "PD",
      "1234": 405028006
    },
    {
      "1": 3529,
      "roma": "ARRONE",
      "rm": "TR",
      "1234": 410055005
    },
    {
      "1": 3530,
      "roma": "ASCREA",
      "rm": "RI",
      "1234": 412057004
    },
    {
      "1": 3531,
      "roma": "ASIAGO",
      "rm": "VI",
      "1234": 405024009
    },
    {
      "1": 3532,
      "roma": "ASIGLIANO VENETO",
      "rm": "VI",
      "1234": 405024010
    },
    {
      "1": 3533,
      "roma": "ASIGLIANO VERCELLESE",
      "rm": "VC",
      "1234": 401002007
    },
    {
      "1": 3534,
      "roma": "ASOLA",
      "rm": "MN",
      "1234": 403020002
    },
    {
      "1": 3535,
      "roma": "ASOLO",
      "rm": "TV",
      "1234": 405026003
    },
    {
      "1": 3536,
      "roma": "ASSAGO",
      "rm": "MI",
      "1234": 403015011
    },
    {
      "1": 3537,
      "roma": "ASSEMINI",
      "rm": "CA",
      "1234": 420092003
    },
    {
      "1": 3538,
      "roma": "ASSISI",
      "rm": "PG",
      "1234": 410054001
    },
    {
      "1": 3539,
      "roma": "ASSO",
      "rm": "CO",
      "1234": 403013013
    },
    {
      "1": 3540,
      "roma": "ASSOLO",
      "rm": "OR",
      "1234": 420095008
    },
    {
      "1": 3541,
      "roma": "ASSORO",
      "rm": "EN",
      "1234": 419086003
    },
    {
      "1": 3542,
      "roma": "ASTI",
      "rm": "AT",
      "1234": 401005005
    },
    {
      "1": 3543,
      "roma": "ASUNI",
      "rm": "OR",
      "1234": 420095009
    },
    {
      "1": 3544,
      "roma": "ASCEA",
      "rm": "SA",
      "1234": 415065009
    },
    {
      "1": 3545,
      "roma": "ASCIANO",
      "rm": "SI",
      "1234": 409052002
    },
    {
      "1": 3546,
      "roma": "ARQUATA SCRIVIA",
      "rm": "AL",
      "1234": 401006009
    },
    {
      "1": 3547,
      "roma": "ASCOLI SATRIANO",
      "rm": "FG",
      "1234": 416071005
    },
    {
      "1": 3548,
      "roma": "ATENA LUCANA",
      "rm": "SA",
      "1234": 415065010
    },
    {
      "1": 3549,
      "roma": "ATESSA",
      "rm": "CH",
      "1234": 413069005
    },
    {
      "1": 3550,
      "roma": "ATINA",
      "rm": "FR",
      "1234": 412060011
    },
    {
      "1": 3551,
      "roma": "ATRANI",
      "rm": "SA",
      "1234": 415065011
    },
    {
      "1": 3552,
      "roma": "ATRI",
      "rm": "TE",
      "1234": 413067004
    },
    {
      "1": 3553,
      "roma": "ATRIPALDA",
      "rm": "AV",
      "1234": 415064006
    },
    {
      "1": 3554,
      "roma": "ATTIGLIANO",
      "rm": "TR",
      "1234": 410055006
    },
    {
      "1": 3555,
      "roma": "ATTIMIS",
      "rm": "UD",
      "1234": 406030007
    },
    {
      "1": 3556,
      "roma": "ATZARA",
      "rm": "NU",
      "1234": 420091003
    },
    {
      "1": 3557,
      "roma": "AUGUSTA",
      "rm": "SR",
      "1234": 419089001
    },
    {
      "1": 3558,
      "roma": "AULETTA",
      "rm": "SA",
      "1234": 415065012
    },
    {
      "1": 3559,
      "roma": "AULLA",
      "rm": "MS",
      "1234": 409045001
    },
    {
      "1": 3560,
      "roma": "AURANO",
      "rm": "VB",
      "1234": 401103005
    },
    {
      "1": 3561,
      "roma": "AURIGO",
      "rm": "IM",
      "1234": 407008005
    },
    {
      "1": 3562,
      "roma": "AURONZO DI CADORE",
      "rm": "BL",
      "1234": 405025005
    },
    {
      "1": 3563,
      "roma": "AUSONIA",
      "rm": "FR",
      "1234": 412060012
    },
    {
      "1": 3564,
      "roma": "AUSTIS",
      "rm": "NU",
      "1234": 420091004
    },
    {
      "1": 3565,
      "roma": "ATELETA",
      "rm": "AQ",
      "1234": 413066005
    },
    {
      "1": 3566,
      "roma": "ASCOLI PICENO",
      "rm": "AP",
      "1234": 411044007
    },
    {
      "1": 3567,
      "roma": "AVEGNO",
      "rm": "GE",
      "1234": 407010002
    },
    {
      "1": 3568,
      "roma": "AVELENGO",
      "rm": "BZ",
      "1234": 404021005
    },
    {
      "1": 3569,
      "roma": "AVELLA",
      "rm": "AV",
      "1234": 415064007
    },
    {
      "1": 3570,
      "roma": "AVELLINO",
      "rm": "AV",
      "1234": 415064008
    },
    {
      "1": 3571,
      "roma": "AVERARA",
      "rm": "BG",
      "1234": 403016014
    },
    {
      "1": 3572,
      "roma": "AVERSA",
      "rm": "CE",
      "1234": 415061005
    },
    {
      "1": 3573,
      "roma": "AVETRANA",
      "rm": "TA",
      "1234": 416073001
    },
    {
      "1": 3574,
      "roma": "AVEZZANO",
      "rm": "AQ",
      "1234": 413066006
    },
    {
      "1": 3575,
      "roma": "AVIANO",
      "rm": "PN",
      "1234": 406093004
    },
    {
      "1": 3576,
      "roma": "AVIATICO",
      "rm": "BG",
      "1234": 403016015
    },
    {
      "1": 3577,
      "roma": "AVIGLIANA",
      "rm": "TO",
      "1234": 401001013
    },
    {
      "1": 3578,
      "roma": "AVIGLIANO",
      "rm": "PZ",
      "1234": 417076007
    },
    {
      "1": 3579,
      "roma": "AVIGLIANO UMBRO",
      "rm": "TR",
      "1234": 410055033
    },
    {
      "1": 3580,
      "roma": "AVIO",
      "rm": "TN",
      "1234": 404022007
    },
    {
      "1": 3581,
      "roma": "AVISE",
      "rm": "AO",
      "1234": 402007006
    },
    {
      "1": 3582,
      "roma": "AVOLA",
      "rm": "SR",
      "1234": 419089002
    },
    {
      "1": 3583,
      "roma": "AVOLASCA",
      "rm": "AL",
      "1234": 401006010
    },
    {
      "1": 3584,
      "roma": "ATELLA",
      "rm": "PZ",
      "1234": 417076006
    },
    {
      "1": 3585,
      "roma": "AZZANELLO",
      "rm": "CR",
      "1234": 403019004
    },
    {
      "1": 3586,
      "roma": "AZZANO D'ASTI",
      "rm": "AT",
      "1234": 401005006
    },
    {
      "1": 3587,
      "roma": "AZZANO DECIMO",
      "rm": "PN",
      "1234": 406093005
    },
    {
      "1": 3588,
      "roma": "AZZANO MELLA",
      "rm": "BS",
      "1234": 403017008
    },
    {
      "1": 3589,
      "roma": "AZZANO SAN PAOLO",
      "rm": "BG",
      "1234": 403016016
    },
    {
      "1": 3590,
      "roma": "AZZATE",
      "rm": "VA",
      "1234": 403012006
    },
    {
      "1": 3591,
      "roma": "AZZIO",
      "rm": "VA",
      "1234": 403012007
    },
    {
      "1": 3592,
      "roma": "AZZONE",
      "rm": "BG",
      "1234": 403016017
    },
    {
      "1": 3593,
      "roma": "BACENO",
      "rm": "VB",
      "1234": 401103006
    },
    {
      "1": 3594,
      "roma": "BACOLI",
      "rm": "NA",
      "1234": 415063006
    },
    {
      "1": 3595,
      "roma": "BADALUCCO",
      "rm": "IM",
      "1234": 407008006
    },
    {
      "1": 3596,
      "roma": "BADESI",
      "rm": "SS",
      "1234": 420090081
    },
    {
      "1": 3597,
      "roma": "BADIA",
      "rm": "BZ",
      "1234": 404021006
    },
    {
      "1": 3598,
      "roma": "BADIA CALAVENA",
      "rm": "VR",
      "1234": 405023005
    },
    {
      "1": 3599,
      "roma": "BADIA PAVESE",
      "rm": "PV",
      "1234": 403018006
    },
    {
      "1": 3600,
      "roma": "BADIA POLESINE",
      "rm": "RO",
      "1234": 405029004
    },
    {
      "1": 3601,
      "roma": "BADIA TEDALDA",
      "rm": "AR",
      "1234": 409051003
    },
    {
      "1": 3602,
      "roma": "BADOLATO",
      "rm": "CZ",
      "1234": 418079008
    },
    {
      "1": 3603,
      "roma": "AYAS",
      "rm": "AO",
      "1234": 402007007
    },
    {
      "1": 3604,
      "roma": "AZEGLIO",
      "rm": "TO",
      "1234": 401001014
    },
    {
      "1": 3605,
      "roma": "BAGNARA CALABRA",
      "rm": "RC",
      "1234": 418080007
    },
    {
      "1": 3606,
      "roma": "BAGNARA DI ROMAGNA",
      "rm": "RA",
      "1234": 408039003
    },
    {
      "1": 3607,
      "roma": "BAGNARIA",
      "rm": "PV",
      "1234": 403018007
    },
    {
      "1": 3608,
      "roma": "BAGNARIA ARSA",
      "rm": "UD",
      "1234": 406030008
    },
    {
      "1": 3609,
      "roma": "BAGNASCO",
      "rm": "CN",
      "1234": 401004008
    },
    {
      "1": 3610,
      "roma": "BAGNATICA",
      "rm": "BG",
      "1234": 403016018
    },
    {
      "1": 3611,
      "roma": "BAGNI DI LUCCA",
      "rm": "LU",
      "1234": 409046002
    },
    {
      "1": 3612,
      "roma": "BAGNO A RIPOLI",
      "rm": "FI",
      "1234": 409048001
    },
    {
      "1": 3613,
      "roma": "BAGNO DI ROMAGNA",
      "rm": "FO",
      "1234": 408040001
    },
    {
      "1": 3614,
      "roma": "BAGNOLI DEL TRIGNO",
      "rm": "IS",
      "1234": 414094003
    },
    {
      "1": 3615,
      "roma": "BAGNOLI DI SOPRA",
      "rm": "PD",
      "1234": 405028008
    },
    {
      "1": 3616,
      "roma": "BAGNOLI IRPINO",
      "rm": "AV",
      "1234": 415064009
    },
    {
      "1": 3617,
      "roma": "BAGNOLO CREMASCO",
      "rm": "CR",
      "1234": 403019005
    },
    {
      "1": 3618,
      "roma": "BAGNOLO DEL SALENTO",
      "rm": "LE",
      "1234": 416075008
    },
    {
      "1": 3619,
      "roma": "BAGNOLO DI PO",
      "rm": "RO",
      "1234": 405029005
    },
    {
      "1": 3620,
      "roma": "BAGNOLO IN PIANO",
      "rm": "RE",
      "1234": 408035002
    },
    {
      "1": 3621,
      "roma": "BAGALADI",
      "rm": "RC",
      "1234": 418080006
    },
    {
      "1": 3622,
      "roma": "AYMAVILLES",
      "rm": "AO",
      "1234": 402007008
    },
    {
      "1": 3623,
      "roma": "BAGNACAVALLO",
      "rm": "RA",
      "1234": 408039002
    },
    {
      "1": 3624,
      "roma": "BAGOLINO",
      "rm": "BS",
      "1234": 403017010
    },
    {
      "1": 3625,
      "roma": "BAIA E LATINA",
      "rm": "CE",
      "1234": 415061006
    },
    {
      "1": 3626,
      "roma": "BAIANO",
      "rm": "AV",
      "1234": 415064010
    },
    {
      "1": 3627,
      "roma": "BAJARDO",
      "rm": "IM",
      "1234": 407008007
    },
    {
      "1": 3628,
      "roma": "BAIRO",
      "rm": "TO",
      "1234": 401001015
    },
    {
      "1": 3629,
      "roma": "BAISO",
      "rm": "RE",
      "1234": 408035003
    },
    {
      "1": 3630,
      "roma": "BALANGERO",
      "rm": "TO",
      "1234": 401001016
    },
    {
      "1": 3631,
      "roma": "BALDICHIERI D'ASTI",
      "rm": "AT",
      "1234": 401005007
    },
    {
      "1": 3632,
      "roma": "BALDISSERO CANAVESE",
      "rm": "TO",
      "1234": 401001017
    },
    {
      "1": 3633,
      "roma": "BALDISSERO D'ALBA",
      "rm": "CN",
      "1234": 401004010
    },
    {
      "1": 3634,
      "roma": "BALDISSERO TORINESE",
      "rm": "TO",
      "1234": 401001018
    },
    {
      "1": 3635,
      "roma": "BALESTRATE",
      "rm": "PA",
      "1234": 419082007
    },
    {
      "1": 3636,
      "roma": "BALESTRINO",
      "rm": "SV",
      "1234": 407009008
    },
    {
      "1": 3637,
      "roma": "BALLABIO",
      "rm": "LC",
      "1234": 403097004
    },
    {
      "1": 3638,
      "roma": "BAGNOLO MELLA",
      "rm": "BS",
      "1234": 403017009
    },
    {
      "1": 3639,
      "roma": "BAGNOLO PIEMONTE",
      "rm": "CN",
      "1234": 401004009
    },
    {
      "1": 3640,
      "roma": "BAGHERIA",
      "rm": "PA",
      "1234": 419082006
    },
    {
      "1": 3641,
      "roma": "BAGNONE",
      "rm": "MS",
      "1234": 409045002
    },
    {
      "1": 3642,
      "roma": "BAGNOREGIO",
      "rm": "VT",
      "1234": 412056003
    },
    {
      "1": 3643,
      "roma": "BALME",
      "rm": "TO",
      "1234": 401001019
    },
    {
      "1": 3644,
      "roma": "BALMUCCIA",
      "rm": "VC",
      "1234": 401002008
    },
    {
      "1": 3645,
      "roma": "BALOCCO",
      "rm": "VC",
      "1234": 401002009
    },
    {
      "1": 3646,
      "roma": "BALSORANO",
      "rm": "AQ",
      "1234": 413066007
    },
    {
      "1": 3647,
      "roma": "BALVANO",
      "rm": "PZ",
      "1234": 417076008
    },
    {
      "1": 3648,
      "roma": "BALZOLA",
      "rm": "AL",
      "1234": 401006011
    },
    {
      "1": 3649,
      "roma": "BANARI",
      "rm": "SS",
      "1234": 420090007
    },
    {
      "1": 3650,
      "roma": "BANCHETTE",
      "rm": "TO",
      "1234": 401001020
    },
    {
      "1": 3651,
      "roma": "BANNIO ANZINO",
      "rm": "VB",
      "1234": 401103007
    },
    {
      "1": 3652,
      "roma": "BANZI",
      "rm": "PZ",
      "1234": 417076009
    },
    {
      "1": 3653,
      "roma": "BAONE",
      "rm": "PD",
      "1234": 405028009
    },
    {
      "1": 3654,
      "roma": "BARADILI",
      "rm": "OR",
      "1234": 420095010
    },
    {
      "1": 3655,
      "roma": "BARAGIANO",
      "rm": "PZ",
      "1234": 417076010
    },
    {
      "1": 3656,
      "roma": "BARANELLO",
      "rm": "CB",
      "1234": 414070002
    },
    {
      "1": 3657,
      "roma": "BARANO D'ISCHIA",
      "rm": "NA",
      "1234": 415063007
    },
    {
      "1": 3658,
      "roma": "BAGNOLO SAN VITO",
      "rm": "MN",
      "1234": 403020003
    },
    {
      "1": 3659,
      "roma": "BARBANIA",
      "rm": "TO",
      "1234": 401001021
    },
    {
      "1": 3660,
      "roma": "BARBARA",
      "rm": "AN",
      "1234": 411042004
    },
    {
      "1": 3661,
      "roma": "BARBARANO ROMANO",
      "rm": "VT",
      "1234": 412056004
    },
    {
      "1": 3662,
      "roma": "BARBARESCO",
      "rm": "CN",
      "1234": 401004011
    },
    {
      "1": 3663,
      "roma": "BARBARIGA",
      "rm": "BS",
      "1234": 403017011
    },
    {
      "1": 3664,
      "roma": "BARBATA",
      "rm": "BG",
      "1234": 403016019
    },
    {
      "1": 3665,
      "roma": "BARANZATE",
      "rm": "MI",
      "1234": 403015250
    },
    {
      "1": 3666,
      "roma": "BARBERINO DI MUGELLO",
      "rm": "FI",
      "1234": 409048002
    },
    {
      "1": 3667,
      "roma": "BARBIANELLO",
      "rm": "PV",
      "1234": 403018008
    },
    {
      "1": 3668,
      "roma": "BARBIANO",
      "rm": "BZ",
      "1234": 404021007
    },
    {
      "1": 3669,
      "roma": "BARBONA",
      "rm": "PD",
      "1234": 405028010
    },
    {
      "1": 3670,
      "roma": "BARCELLONA POZZO DI GOTTO",
      "rm": "ME",
      "1234": 419083005
    },
    {
      "1": 3671,
      "roma": "FUBINE MONFERRATO",
      "rm": "AL",
      "1234": 401006976
    },
    {
      "1": 3672,
      "roma": "BARASSO",
      "rm": "VA",
      "1234": 403012008
    },
    {
      "1": 3673,
      "roma": "BALLAO",
      "rm": "CA",
      "1234": 420092004
    },
    {
      "1": 3674,
      "roma": "BARATILI SAN PIETRO",
      "rm": "OR",
      "1234": 420095011
    },
    {
      "1": 3675,
      "roma": "BARDELLO",
      "rm": "VA",
      "1234": 403012009
    },
    {
      "1": 3676,
      "roma": "BARDI",
      "rm": "PR",
      "1234": 408034002
    },
    {
      "1": 3677,
      "roma": "BARDINETO",
      "rm": "SV",
      "1234": 407009009
    },
    {
      "1": 3678,
      "roma": "BARDOLINO",
      "rm": "VR",
      "1234": 405023006
    },
    {
      "1": 3679,
      "roma": "BARDONECCHIA",
      "rm": "TO",
      "1234": 401001022
    },
    {
      "1": 3680,
      "roma": "BAREGGIO",
      "rm": "MI",
      "1234": 403015012
    },
    {
      "1": 3681,
      "roma": "BARENGO",
      "rm": "NO",
      "1234": 401003012
    },
    {
      "1": 3682,
      "roma": "BARESSA",
      "rm": "OR",
      "1234": 420095012
    },
    {
      "1": 3683,
      "roma": "BARETE",
      "rm": "AQ",
      "1234": 413066008
    },
    {
      "1": 3684,
      "roma": "BARGA",
      "rm": "LU",
      "1234": 409046003
    },
    {
      "1": 3685,
      "roma": "BARGAGLI",
      "rm": "GE",
      "1234": 407010003
    },
    {
      "1": 3686,
      "roma": "BARGE",
      "rm": "CN",
      "1234": 401004012
    },
    {
      "1": 3687,
      "roma": "BARGHE",
      "rm": "BS",
      "1234": 403017012
    },
    {
      "1": 3688,
      "roma": "BARI",
      "rm": "BA",
      "1234": 416072006
    },
    {
      "1": 3689,
      "roma": "BARCIS",
      "rm": "PN",
      "1234": 406093006
    },
    {
      "1": 3690,
      "roma": "BARD",
      "rm": "AO",
      "1234": 402007009
    },
    {
      "1": 3691,
      "roma": "BARISCIANO",
      "rm": "AQ",
      "1234": 413066009
    },
    {
      "1": 3692,
      "roma": "BARNI",
      "rm": "CO",
      "1234": 403013015
    },
    {
      "1": 3693,
      "roma": "BAROLO",
      "rm": "CN",
      "1234": 401004013
    },
    {
      "1": 3694,
      "roma": "BARONE CANAVESE",
      "rm": "TO",
      "1234": 401001023
    },
    {
      "1": 3695,
      "roma": "BARONISSI",
      "rm": "SA",
      "1234": 415065013
    },
    {
      "1": 3696,
      "roma": "BARRAFRANCA",
      "rm": "EN",
      "1234": 419086004
    },
    {
      "1": 3697,
      "roma": "BARRALI",
      "rm": "CA",
      "1234": 420092005
    },
    {
      "1": 3698,
      "roma": "BARREA",
      "rm": "AQ",
      "1234": 413066010
    },
    {
      "1": 3699,
      "roma": "BARUMINI",
      "rm": "CA",
      "1234": 420092006
    },
    {
      "1": 3700,
      "roma": "BARZAGO",
      "rm": "LC",
      "1234": 403097005
    },
    {
      "1": 3701,
      "roma": "BARZANA",
      "rm": "BG",
      "1234": 403016021
    },
    {
      "1": 3702,
      "roma": "BARZANO'",
      "rm": "LC",
      "1234": 403097006
    },
    {
      "1": 3703,
      "roma": "BARI SARDO",
      "rm": "NU",
      "1234": 420091005
    },
    {
      "1": 3704,
      "roma": "BARIANO",
      "rm": "BG",
      "1234": 403016020
    },
    {
      "1": 3705,
      "roma": "BARILE",
      "rm": "PZ",
      "1234": 417076011
    },
    {
      "1": 3706,
      "roma": "BASCAPE'",
      "rm": "PV",
      "1234": 403018009
    },
    {
      "1": 3707,
      "roma": "BASCHI",
      "rm": "TR",
      "1234": 410055007
    },
    {
      "1": 3708,
      "roma": "BASCIANO",
      "rm": "TE",
      "1234": 413067005
    },
    {
      "1": 3709,
      "roma": "BASELGA DI PINE'",
      "rm": "TN",
      "1234": 404022009
    },
    {
      "1": 3710,
      "roma": "BASELICE",
      "rm": "BN",
      "1234": 415062007
    },
    {
      "1": 3711,
      "roma": "BASIANO",
      "rm": "MI",
      "1234": 403015014
    },
    {
      "1": 3712,
      "roma": "BASICO'",
      "rm": "ME",
      "1234": 419083006
    },
    {
      "1": 3713,
      "roma": "BASIGLIO",
      "rm": "MI",
      "1234": 403015015
    },
    {
      "1": 3714,
      "roma": "BASILIANO",
      "rm": "UD",
      "1234": 406030009
    },
    {
      "1": 3715,
      "roma": "BASSANO BRESCIANO",
      "rm": "BS",
      "1234": 403017013
    },
    {
      "1": 2865,
      "roma": "MONTENERO SABINO",
      "rm": "RI",
      "1234": 412057042
    },
    {
      "1": 2866,
      "roma": "MONTELUPONE",
      "rm": "MC",
      "1234": 411043030
    },
    {
      "1": 2867,
      "roma": "MONTEROTONDO MARITTIMO",
      "rm": "GR",
      "1234": 409053027
    },
    {
      "1": 2868,
      "roma": "MONTESANO SALENTINO",
      "rm": "LE",
      "1234": 416075049
    },
    {
      "1": 2869,
      "roma": "MONTESANO SULLA MARCELLANA",
      "rm": "SA",
      "1234": 415065076
    },
    {
      "1": 2870,
      "roma": "MONTESARCHIO",
      "rm": "BN",
      "1234": 415062043
    },
    {
      "1": 2871,
      "roma": "MONTESCAGLIOSO",
      "rm": "MT",
      "1234": 417077017
    },
    {
      "1": 2872,
      "roma": "MONTESCANO",
      "rm": "PV",
      "1234": 403018097
    },
    {
      "1": 2873,
      "roma": "MONTESCHENO",
      "rm": "VB",
      "1234": 401103047
    },
    {
      "1": 2874,
      "roma": "MONTESCUDAIO",
      "rm": "PI",
      "1234": 409050020
    },
    {
      "1": 2875,
      "roma": "MONTESE",
      "rm": "MO",
      "1234": 408036026
    },
    {
      "1": 2876,
      "roma": "MONTESEGALE",
      "rm": "PV",
      "1234": 403018098
    },
    {
      "1": 2877,
      "roma": "MONTEROSSO AL MARE",
      "rm": "SP",
      "1234": 407011019
    },
    {
      "1": 2878,
      "roma": "MONTEROSSO ALMO",
      "rm": "RG",
      "1234": 419088007
    },
    {
      "1": 2879,
      "roma": "MONTEROSSO CALABRO",
      "rm": "VV",
      "1234": 418102023
    },
    {
      "1": 2880,
      "roma": "MONTENERO VALCOCCHIARA",
      "rm": "IS",
      "1234": 414094029
    },
    {
      "1": 2881,
      "roma": "MONTENERODOMO",
      "rm": "CH",
      "1234": 413069054
    },
    {
      "1": 2882,
      "roma": "MONTEODORISIO",
      "rm": "CH",
      "1234": 413069055
    },
    {
      "1": 2883,
      "roma": "MONTEPAONE",
      "rm": "CZ",
      "1234": 418079081
    },
    {
      "1": 2884,
      "roma": "MONTEU DA PO",
      "rm": "TO",
      "1234": 401001162
    },
    {
      "1": 2885,
      "roma": "MONTEU ROERO",
      "rm": "CN",
      "1234": 401004140
    },
    {
      "1": 2886,
      "roma": "MONTEVAGO",
      "rm": "AG",
      "1234": 419084025
    },
    {
      "1": 2887,
      "roma": "MONTEVARCHI",
      "rm": "AR",
      "1234": 409051026
    },
    {
      "1": 2888,
      "roma": "MONTEVECCHIA",
      "rm": "LC",
      "1234": 403097053
    },
    {
      "1": 2889,
      "roma": "MONTEVERDE",
      "rm": "AV",
      "1234": 415064060
    },
    {
      "1": 2890,
      "roma": "MONTEVERDI MARITTIMO",
      "rm": "PI",
      "1234": 409050021
    },
    {
      "1": 2891,
      "roma": "MONTEVIALE",
      "rm": "VI",
      "1234": 405024066
    },
    {
      "1": 2892,
      "roma": "MONTEZEMOLO",
      "rm": "CN",
      "1234": 401004141
    },
    {
      "1": 2893,
      "roma": "MONTI",
      "rm": "SS",
      "1234": 420090041
    },
    {
      "1": 2894,
      "roma": "MONTIANO",
      "rm": "FO",
      "1234": 408040028
    },
    {
      "1": 2895,
      "roma": "MONTICELLI BRUSATI",
      "rm": "BS",
      "1234": 403017112
    },
    {
      "1": 2896,
      "roma": "MONTICELLI D'ONGINA",
      "rm": "PC",
      "1234": 408033027
    },
    {
      "1": 2897,
      "roma": "MONTICELLI PAVESE",
      "rm": "PV",
      "1234": 403018099
    },
    {
      "1": 2898,
      "roma": "MONTESILVANO",
      "rm": "PE",
      "1234": 413068024
    },
    {
      "1": 2899,
      "roma": "MONTELEONE DI FERMO",
      "rm": "FM",
      "1234": 411109019
    },
    {
      "1": 2900,
      "roma": "MONTEROSSO GRANA",
      "rm": "CN",
      "1234": 401004139
    },
    {
      "1": 2901,
      "roma": "MONTEROTONDO",
      "rm": "RM",
      "1234": 412058065
    },
    {
      "1": 2902,
      "roma": "MONTIERI",
      "rm": "GR",
      "1234": 409053017
    },
    {
      "1": 2903,
      "roma": "MONTIGLIO MONFERRATO",
      "rm": "AT",
      "1234": 401005516
    },
    {
      "1": 2904,
      "roma": "MONTIGNOSO",
      "rm": "MS",
      "1234": 409045011
    },
    {
      "1": 2905,
      "roma": "MONTIRONE",
      "rm": "BS",
      "1234": 403017114
    },
    {
      "1": 2906,
      "roma": "MONTJOVET",
      "rm": "AO",
      "1234": 402007043
    },
    {
      "1": 2907,
      "roma": "MONTODINE",
      "rm": "CR",
      "1234": 403019059
    },
    {
      "1": 2908,
      "roma": "MONTOGGIO",
      "rm": "GE",
      "1234": 407010039
    },
    {
      "1": 2909,
      "roma": "MONTONE",
      "rm": "PG",
      "1234": 410054033
    },
    {
      "1": 2910,
      "roma": "MONTOPOLI DI SABINA",
      "rm": "RI",
      "1234": 412057044
    },
    {
      "1": 2911,
      "roma": "MONTOPOLI IN VAL D'ARNO",
      "rm": "PI",
      "1234": 409050022
    },
    {
      "1": 2912,
      "roma": "MONTORFANO",
      "rm": "CO",
      "1234": 403013157
    },
    {
      "1": 2913,
      "roma": "MONTORIO AL VOMANO",
      "rm": "TE",
      "1234": 413067028
    },
    {
      "1": 2914,
      "roma": "MONTORIO NEI FRENTANI",
      "rm": "CB",
      "1234": 414070047
    },
    {
      "1": 2915,
      "roma": "MONTORIO ROMANO",
      "rm": "RM",
      "1234": 412058066
    },
    {
      "1": 2916,
      "roma": "MONTERUBBIANO",
      "rm": "FM",
      "1234": 411109022
    },
    {
      "1": 2917,
      "roma": "MONTELPARO",
      "rm": "FM",
      "1234": 411109020
    },
    {
      "1": 2918,
      "roma": "MONTESPERTOLI",
      "rm": "FI",
      "1234": 409048030
    },
    {
      "1": 2919,
      "roma": "MONTICHIARI",
      "rm": "BS",
      "1234": 403017113
    },
    {
      "1": 2920,
      "roma": "MONTICIANO",
      "rm": "SI",
      "1234": 409052018
    },
    {
      "1": 2921,
      "roma": "MONTU' BECCARIA",
      "rm": "PV",
      "1234": 403018100
    },
    {
      "1": 2922,
      "roma": "MONVALLE",
      "rm": "VA",
      "1234": 403012104
    },
    {
      "1": 2923,
      "roma": "MONZAMBANO",
      "rm": "MN",
      "1234": 403020036
    },
    {
      "1": 2924,
      "roma": "MONZUNO",
      "rm": "BO",
      "1234": 408037044
    },
    {
      "1": 2925,
      "roma": "MORANO CALABRO",
      "rm": "CS",
      "1234": 418078083
    },
    {
      "1": 2926,
      "roma": "MORANO SUL PO",
      "rm": "AL",
      "1234": 401006109
    },
    {
      "1": 2927,
      "roma": "MORANSENGO",
      "rm": "AT",
      "1234": 401005079
    },
    {
      "1": 2928,
      "roma": "MORARO",
      "rm": "GO",
      "1234": 406031013
    },
    {
      "1": 2929,
      "roma": "MORAZZONE",
      "rm": "VA",
      "1234": 403012105
    },
    {
      "1": 2930,
      "roma": "MORBEGNO",
      "rm": "SO",
      "1234": 403014045
    },
    {
      "1": 2931,
      "roma": "MORBELLO",
      "rm": "AL",
      "1234": 401006110
    },
    {
      "1": 2932,
      "roma": "MONTICELLO BRIANZA",
      "rm": "LC",
      "1234": 403097054
    },
    {
      "1": 2933,
      "roma": "MONTICELLO CONTE OTTO",
      "rm": "VI",
      "1234": 405024067
    },
    {
      "1": 2934,
      "roma": "MONTICELLO D'ALBA",
      "rm": "CN",
      "1234": 401004142
    },
    {
      "1": 2935,
      "roma": "MONTRESTA",
      "rm": "NU",
      "1234": 420091049
    },
    {
      "1": 2936,
      "roma": "MORFASSO",
      "rm": "PC",
      "1234": 408033028
    },
    {
      "1": 2937,
      "roma": "MORGANO",
      "rm": "TV",
      "1234": 405026047
    },
    {
      "1": 2938,
      "roma": "MORGEX",
      "rm": "AO",
      "1234": 402007044
    },
    {
      "1": 2939,
      "roma": "MORGONGIORI",
      "rm": "OR",
      "1234": 420095030
    },
    {
      "1": 2940,
      "roma": "MORI",
      "rm": "TN",
      "1234": 404022123
    },
    {
      "1": 2941,
      "roma": "MORIAGO DELLA BATTAGLIA",
      "rm": "TV",
      "1234": 405026048
    },
    {
      "1": 2942,
      "roma": "MORICONE",
      "rm": "RM",
      "1234": 412058067
    },
    {
      "1": 2943,
      "roma": "MORIGERATI",
      "rm": "SA",
      "1234": 415065077
    },
    {
      "1": 2944,
      "roma": "MORIMONDO",
      "rm": "MI",
      "1234": 403015150
    },
    {
      "1": 2945,
      "roma": "MORINO",
      "rm": "AQ",
      "1234": 413066057
    },
    {
      "1": 2946,
      "roma": "MORIONDO TORINESE",
      "rm": "TO",
      "1234": 401001163
    },
    {
      "1": 2947,
      "roma": "MORLUPO",
      "rm": "RM",
      "1234": 412058068
    },
    {
      "1": 2948,
      "roma": "MORMANNO",
      "rm": "CS",
      "1234": 418078084
    },
    {
      "1": 2949,
      "roma": "MONTOTTONE",
      "rm": "FM",
      "1234": 411109027
    },
    {
      "1": 2950,
      "roma": "MORCIANO DI LEUCA",
      "rm": "LE",
      "1234": 416075050
    },
    {
      "1": 2951,
      "roma": "MORCIANO DI ROMAGNA",
      "rm": "RN",
      "1234": 408099011
    },
    {
      "1": 2952,
      "roma": "MONTORSO VICENTINO",
      "rm": "VI",
      "1234": 405024068
    },
    {
      "1": 2953,
      "roma": "MORENGO",
      "rm": "BG",
      "1234": 403016140
    },
    {
      "1": 2954,
      "roma": "MORES",
      "rm": "SS",
      "1234": 420090042
    },
    {
      "1": 2955,
      "roma": "MORETTA",
      "rm": "CN",
      "1234": 401004143
    },
    {
      "1": 2956,
      "roma": "MORRONE DEL SANNIO",
      "rm": "CB",
      "1234": 414070048
    },
    {
      "1": 2957,
      "roma": "MORROVALLE",
      "rm": "MC",
      "1234": 411043033
    },
    {
      "1": 2958,
      "roma": "MORSANO AL TAGLIAMENTO",
      "rm": "PN",
      "1234": 406093028
    },
    {
      "1": 2959,
      "roma": "MORSASCO",
      "rm": "AL",
      "1234": 401006112
    },
    {
      "1": 2960,
      "roma": "MORTARA",
      "rm": "PV",
      "1234": 403018102
    },
    {
      "1": 2961,
      "roma": "MORTEGLIANO",
      "rm": "UD",
      "1234": 406030062
    },
    {
      "1": 2962,
      "roma": "MORTERONE",
      "rm": "LC",
      "1234": 403097055
    },
    {
      "1": 2963,
      "roma": "MORNAGO",
      "rm": "VA",
      "1234": 403012106
    },
    {
      "1": 2964,
      "roma": "MORNESE",
      "rm": "AL",
      "1234": 401006111
    },
    {
      "1": 2965,
      "roma": "MORNICO AL SERIO",
      "rm": "BG",
      "1234": 403016141
    },
    {
      "1": 2966,
      "roma": "MORNICO LOSANA",
      "rm": "PV",
      "1234": 403018101
    },
    {
      "1": 2967,
      "roma": "MOROLO",
      "rm": "FR",
      "1234": 412060045
    },
    {
      "1": 2968,
      "roma": "MORCONE",
      "rm": "BN",
      "1234": 415062044
    },
    {
      "1": 2969,
      "roma": "MORDANO",
      "rm": "BO",
      "1234": 408037045
    },
    {
      "1": 2970,
      "roma": "MOROZZO",
      "rm": "CN",
      "1234": 401004144
    },
    {
      "1": 2971,
      "roma": "MORRA DE SANCTIS",
      "rm": "AV",
      "1234": 415064063
    },
    {
      "1": 2972,
      "roma": "MORRO D'ALBA",
      "rm": "AN",
      "1234": 411042031
    },
    {
      "1": 2973,
      "roma": "MORRO D'ORO",
      "rm": "TE",
      "1234": 413067029
    },
    {
      "1": 2974,
      "roma": "MORRO REATINO",
      "rm": "RI",
      "1234": 412057045
    },
    {
      "1": 2975,
      "roma": "MOSSO",
      "rm": "BI",
      "1234": 401096501
    },
    {
      "1": 2976,
      "roma": "MOTTA BALUFFI",
      "rm": "CR",
      "1234": 403019061
    },
    {
      "1": 2977,
      "roma": "MOTTA CAMASTRA",
      "rm": "ME",
      "1234": 419083058
    },
    {
      "1": 2978,
      "roma": "MOTTA D'AFFERMO",
      "rm": "ME",
      "1234": 419083059
    },
    {
      "1": 2979,
      "roma": "MOTTA DEI CONTI",
      "rm": "VC",
      "1234": 401002082
    },
    {
      "1": 2980,
      "roma": "MOTTA DI LIVENZA",
      "rm": "TV",
      "1234": 405026049
    },
    {
      "1": 2981,
      "roma": "VILLACIDRO",
      "rm": "CA",
      "1234": 420092092
    },
    {
      "1": 2982,
      "roma": "VILLAMIROGLIO",
      "rm": "AL",
      "1234": 401006184
    },
    {
      "1": 2983,
      "roma": "VILLANDRO",
      "rm": "BZ",
      "1234": 404021114
    },
    {
      "1": 2984,
      "roma": "VILLANOVA BIELLESE",
      "rm": "BI",
      "1234": 401096079
    },
    {
      "1": 2985,
      "roma": "VILLANOVA CANAVESE",
      "rm": "TO",
      "1234": 401001301
    },
    {
      "1": 2986,
      "roma": "VILLANOVA D'ALBENGA",
      "rm": "SV",
      "1234": 407009068
    },
    {
      "1": 2987,
      "roma": "VILLANOVA D'ARDENGHI",
      "rm": "PV",
      "1234": 403018179
    },
    {
      "1": 2988,
      "roma": "VILLANOVA D'ASTI",
      "rm": "AT",
      "1234": 401005118
    },
    {
      "1": 2989,
      "roma": "VILLANOVA DEL BATTISTA",
      "rm": "AV",
      "1234": 415064118
    },
    {
      "1": 2990,
      "roma": "VILLANOVA DEL GHEBBO",
      "rm": "RO",
      "1234": 405029050
    },
    {
      "1": 2991,
      "roma": "VILLANOVA DEL SILLARO",
      "rm": "LO",
      "1234": 403098060
    },
    {
      "1": 2992,
      "roma": "VILLANOVA DI CAMPOSAMPIERO",
      "rm": "PD",
      "1234": 405028104
    },
    {
      "1": 2993,
      "roma": "VILLANOVA MARCHESANA",
      "rm": "RO",
      "1234": 405029051
    },
    {
      "1": 2994,
      "roma": "VILLANOVA MONDOVI'",
      "rm": "CN",
      "1234": 401004245
    },
    {
      "1": 2995,
      "roma": "VILLANOVA MONFERRATO",
      "rm": "AL",
      "1234": 401006185
    },
    {
      "1": 2996,
      "roma": "VILLANOVA MONTELEONE",
      "rm": "SS",
      "1234": 420090078
    },
    {
      "1": 2997,
      "roma": "VILLANOVA SOLARO",
      "rm": "CN",
      "1234": 401004246
    },
    {
      "1": 2998,
      "roma": "VILLANOVA SULL'ARDA",
      "rm": "PC",
      "1234": 408033046
    },
    {
      "1": 2999,
      "roma": "VILLANOVA TRUSCHEDU",
      "rm": "OR",
      "1234": 420095071
    },
    {
      "1": 3000,
      "roma": "VILLANOVAFORRU",
      "rm": "CA",
      "1234": 420092095
    },
    {
      "1": 3001,
      "roma": "VILLANOVAFRANCA",
      "rm": "CA",
      "1234": 420092096
    },
    {
      "1": 3002,
      "roma": "VILLAMASSARGIA",
      "rm": "CA",
      "1234": 420092094
    },
    {
      "1": 3003,
      "roma": "VILLANTERIO",
      "rm": "PV",
      "1234": 403018180
    },
    {
      "1": 3004,
      "roma": "VILLANUOVA SUL CLISI",
      "rm": "BS",
      "1234": 403017201
    },
    {
      "1": 3005,
      "roma": "VILLAPERUCCIO",
      "rm": "CA",
      "1234": 420092104
    },
    {
      "1": 3006,
      "roma": "VILLAPIANA",
      "rm": "CS",
      "1234": 418078154
    },
    {
      "1": 3007,
      "roma": "VILLAPUTZU",
      "rm": "CA",
      "1234": 420092097
    },
    {
      "1": 3008,
      "roma": "VILLAR DORA",
      "rm": "TO",
      "1234": 401001303
    },
    {
      "1": 3009,
      "roma": "VILLAR FOCCHIARDO",
      "rm": "TO",
      "1234": 401001305
    },
    {
      "1": 3010,
      "roma": "VILLAR PELLICE",
      "rm": "TO",
      "1234": 401001306
    },
    {
      "1": 3011,
      "roma": "VILLAR PEROSA",
      "rm": "TO",
      "1234": 401001307
    },
    {
      "1": 3012,
      "roma": "VILLAR SAN COSTANZO",
      "rm": "CN",
      "1234": 401004247
    },
    {
      "1": 3013,
      "roma": "VILLARBASSE",
      "rm": "TO",
      "1234": 401001302
    },
    {
      "1": 3014,
      "roma": "VILLARBOIT",
      "rm": "VC",
      "1234": 401002163
    },
    {
      "1": 3015,
      "roma": "VILLAREGGIA",
      "rm": "TO",
      "1234": 401001304
    },
    {
      "1": 3016,
      "roma": "VILLARICCA",
      "rm": "NA",
      "1234": 415063087
    },
    {
      "1": 3017,
      "roma": "VILLAROMAGNANO",
      "rm": "AL",
      "1234": 401006186
    },
    {
      "1": 3018,
      "roma": "VILLAROSA",
      "rm": "EN",
      "1234": 419086020
    },
    {
      "1": 3019,
      "roma": "VILLASALTO",
      "rm": "CA",
      "1234": 420092098
    },
    {
      "1": 3020,
      "roma": "VILLASANTA",
      "rm": "MB",
      "1234": 403108049
    },
    {
      "1": 3021,
      "roma": "VILLASIMIUS",
      "rm": "CA",
      "1234": 420092100
    },
    {
      "1": 3022,
      "roma": "VILLASOR",
      "rm": "CA",
      "1234": 420092101
    },
    {
      "1": 3023,
      "roma": "VILLASPECIOSA",
      "rm": "CA",
      "1234": 420092102
    },
    {
      "1": 3024,
      "roma": "VILLANOVA TULO",
      "rm": "NU",
      "1234": 420091102
    },
    {
      "1": 3025,
      "roma": "VILLASTELLONE",
      "rm": "TO",
      "1234": 401001308
    },
    {
      "1": 3026,
      "roma": "VILLATA",
      "rm": "VC",
      "1234": 401002164
    },
    {
      "1": 3027,
      "roma": "VILLAVALLELONGA",
      "rm": "AQ",
      "1234": 413066106
    },
    {
      "1": 3028,
      "roma": "VILLAVERLA",
      "rm": "VI",
      "1234": 405024118
    },
    {
      "1": 3029,
      "roma": "VILLENEUVE",
      "rm": "AO",
      "1234": 402007074
    },
    {
      "1": 3030,
      "roma": "VILLETTA BARREA",
      "rm": "AQ",
      "1234": 413066107
    },
    {
      "1": 3031,
      "roma": "VILLETTE",
      "rm": "VB",
      "1234": 401103076
    },
    {
      "1": 3032,
      "roma": "VILLIMPENTA",
      "rm": "MN",
      "1234": 403020068
    },
    {
      "1": 3033,
      "roma": "VILLONGO",
      "rm": "BG",
      "1234": 403016242
    },
    {
      "1": 3034,
      "roma": "VILLORBA",
      "rm": "TV",
      "1234": 405026091
    },
    {
      "1": 3035,
      "roma": "VILMINORE DI SCALVE",
      "rm": "BG",
      "1234": 403016243
    },
    {
      "1": 3036,
      "roma": "VIMODRONE",
      "rm": "MI",
      "1234": 403015242
    },
    {
      "1": 3037,
      "roma": "VIMERCATE",
      "rm": "MB",
      "1234": 403108050
    },
    {
      "1": 3038,
      "roma": "VISTARINO",
      "rm": "PV",
      "1234": 403018181
    },
    {
      "1": 3039,
      "roma": "VINADIO",
      "rm": "CN",
      "1234": 401004248
    },
    {
      "1": 3040,
      "roma": "VINCHIATURO",
      "rm": "CB",
      "1234": 414070084
    },
    {
      "1": 3041,
      "roma": "VINCHIO",
      "rm": "AT",
      "1234": 401005120
    },
    {
      "1": 3042,
      "roma": "VINCI",
      "rm": "FI",
      "1234": 409048050
    },
    {
      "1": 3043,
      "roma": "VINOVO",
      "rm": "TO",
      "1234": 401001309
    },
    {
      "1": 3044,
      "roma": "VINZAGLIO",
      "rm": "NO",
      "1234": 401003164
    },
    {
      "1": 3045,
      "roma": "VIOLA",
      "rm": "CN",
      "1234": 401004249
    },
    {
      "1": 3046,
      "roma": "VILLESSE",
      "rm": "GO",
      "1234": 406031025
    },
    {
      "1": 3047,
      "roma": "VIPITENO",
      "rm": "BZ",
      "1234": 404021115
    },
    {
      "1": 3048,
      "roma": "VIRLE PIEMONTE",
      "rm": "TO",
      "1234": 401001310
    },
    {
      "1": 3049,
      "roma": "VISANO",
      "rm": "BS",
      "1234": 403017203
    },
    {
      "1": 3050,
      "roma": "VISCHE",
      "rm": "TO",
      "1234": 401001311
    },
    {
      "1": 3051,
      "roma": "VISCIANO",
      "rm": "NA",
      "1234": 415063088
    },
    {
      "1": 3052,
      "roma": "VISCO",
      "rm": "UD",
      "1234": 406030135
    },
    {
      "1": 3053,
      "roma": "VISONE",
      "rm": "AL",
      "1234": 401006187
    },
    {
      "1": 3054,
      "roma": "VISSO",
      "rm": "MC",
      "1234": 411043057
    },
    {
      "1": 3055,
      "roma": "VIZZOLA TICINO",
      "rm": "VA",
      "1234": 403012140
    },
    {
      "1": 3056,
      "roma": "VISTRORIO",
      "rm": "TO",
      "1234": 401001312
    },
    {
      "1": 3057,
      "roma": "VITA",
      "rm": "TP",
      "1234": 419081023
    },
    {
      "1": 3058,
      "roma": "VITERBO",
      "rm": "VT",
      "1234": 412056059
    },
    {
      "1": 3059,
      "roma": "VITICUSO",
      "rm": "FR",
      "1234": 412060091
    },
    {
      "1": 3060,
      "roma": "VITO D'ASIO",
      "rm": "PN",
      "1234": 406093049
    },
    {
      "1": 3061,
      "roma": "VITORCHIANO",
      "rm": "VT",
      "1234": 412056060
    },
    {
      "1": 3062,
      "roma": "VIONE",
      "rm": "BS",
      "1234": 403017202
    },
    {
      "1": 3063,
      "roma": "VITTORIO VENETO",
      "rm": "TV",
      "1234": 405026092
    },
    {
      "1": 3064,
      "roma": "VITTORITO",
      "rm": "AQ",
      "1234": 413066108
    },
    {
      "1": 4714,
      "roma": "CAVENAGO DI BRIANZA",
      "rm": "MB",
      "1234": 403108017
    },
    {
      "1": 4715,
      "roma": "CAVRIANA",
      "rm": "MN",
      "1234": 403020018
    },
    {
      "1": 4716,
      "roma": "CAVRIGLIA",
      "rm": "AR",
      "1234": 409051013
    },
    {
      "1": 4717,
      "roma": "CAZZAGO BRABBIA",
      "rm": "VA",
      "1234": 403012049
    },
    {
      "1": 4718,
      "roma": "CAZZAGO SAN MARTINO",
      "rm": "BS",
      "1234": 403017046
    },
    {
      "1": 4719,
      "roma": "CAZZANO DI TRAMIGNA",
      "rm": "VR",
      "1234": 405023024
    },
    {
      "1": 4720,
      "roma": "CAZZANO SANT'ANDREA",
      "rm": "BG",
      "1234": 403016067
    },
    {
      "1": 4721,
      "roma": "CECCANO",
      "rm": "FR",
      "1234": 412060024
    },
    {
      "1": 4722,
      "roma": "CAVEDAGO",
      "rm": "TN",
      "1234": 404022052
    },
    {
      "1": 4723,
      "roma": "CAVEDINE",
      "rm": "TN",
      "1234": 404022053
    },
    {
      "1": 4724,
      "roma": "CAVENAGO D'ADDA",
      "rm": "LO",
      "1234": 403098017
    },
    {
      "1": 4725,
      "roma": "CAVAGNOLO",
      "rm": "TO",
      "1234": 401001069
    },
    {
      "1": 4726,
      "roma": "CAVAION VERONESE",
      "rm": "VR",
      "1234": 405023023
    },
    {
      "1": 4727,
      "roma": "CAVERNAGO",
      "rm": "BG",
      "1234": 403016066
    },
    {
      "1": 4728,
      "roma": "CEFALA' DIANA",
      "rm": "PA",
      "1234": 419082026
    },
    {
      "1": 4729,
      "roma": "CEFALU'",
      "rm": "PA",
      "1234": 419082027
    },
    {
      "1": 4730,
      "roma": "CEGGIA",
      "rm": "VE",
      "1234": 405027007
    },
    {
      "1": 4731,
      "roma": "CEGLIE MESSAPICA",
      "rm": "BR",
      "1234": 416074003
    },
    {
      "1": 4732,
      "roma": "CELANO",
      "rm": "AQ",
      "1234": 413066032
    },
    {
      "1": 4733,
      "roma": "CELENZA SUL TRIGNO",
      "rm": "CH",
      "1234": 413069021
    },
    {
      "1": 4734,
      "roma": "CELENZA VALFORTORE",
      "rm": "FG",
      "1234": 416071018
    },
    {
      "1": 4735,
      "roma": "CELICO",
      "rm": "CS",
      "1234": 418078034
    },
    {
      "1": 4736,
      "roma": "CORNATE D'ADDA",
      "rm": "MB",
      "1234": 403108053
    },
    {
      "1": 4737,
      "roma": "CELLA DATI",
      "rm": "CR",
      "1234": 403019028
    },
    {
      "1": 4738,
      "roma": "CELLA MONTE",
      "rm": "AL",
      "1234": 401006056
    },
    {
      "1": 4739,
      "roma": "COGORNO",
      "rm": "GE",
      "1234": 407010018
    },
    {
      "1": 4740,
      "roma": "COLAZZA",
      "rm": "NO",
      "1234": 401003051
    },
    {
      "1": 4741,
      "roma": "COGLIATE",
      "rm": "MB",
      "1234": 403108020
    },
    {
      "1": 4742,
      "roma": "COLERE",
      "rm": "BG",
      "1234": 403016078
    },
    {
      "1": 4743,
      "roma": "COLFELICE",
      "rm": "FR",
      "1234": 412060027
    },
    {
      "1": 4744,
      "roma": "CODROIPO",
      "rm": "UD",
      "1234": 406030027
    },
    {
      "1": 4745,
      "roma": "CODRONGIANOS",
      "rm": "SS",
      "1234": 420090026
    },
    {
      "1": 4746,
      "roma": "COGGIOLA",
      "rm": "BI",
      "1234": 401096019
    },
    {
      "1": 4747,
      "roma": "COLLE BRIANZA",
      "rm": "LC",
      "1234": 403097024
    },
    {
      "1": 4748,
      "roma": "COGOLETO",
      "rm": "GE",
      "1234": 407010017
    },
    {
      "1": 4749,
      "roma": "COGOLLO DEL CENGIO",
      "rm": "VI",
      "1234": 405024032
    },
    {
      "1": 4750,
      "roma": "COLLE SAN MAGNO",
      "rm": "FR",
      "1234": 412060029
    },
    {
      "1": 4751,
      "roma": "COLLE SANNITA",
      "rm": "BN",
      "1234": 415062025
    },
    {
      "1": 4752,
      "roma": "COLLE SANTA LUCIA",
      "rm": "BL",
      "1234": 405025014
    },
    {
      "1": 4753,
      "roma": "COLLE UMBERTO",
      "rm": "TV",
      "1234": 405026020
    },
    {
      "1": 4754,
      "roma": "COLLEBEATO",
      "rm": "BS",
      "1234": 403017057
    },
    {
      "1": 4755,
      "roma": "COLLECCHIO",
      "rm": "PR",
      "1234": 408034009
    },
    {
      "1": 4756,
      "roma": "COLLECORVINO",
      "rm": "PE",
      "1234": 413068015
    },
    {
      "1": 4757,
      "roma": "COLLEDARA",
      "rm": "TE",
      "1234": 413067018
    },
    {
      "1": 4758,
      "roma": "COLLEDIMACINE",
      "rm": "CH",
      "1234": 413069025
    },
    {
      "1": 4759,
      "roma": "COLLEDIMEZZO",
      "rm": "CH",
      "1234": 413069026
    },
    {
      "1": 4760,
      "roma": "COLLEFERRO",
      "rm": "RM",
      "1234": 412058034
    },
    {
      "1": 4761,
      "roma": "COLLEGIOVE",
      "rm": "RI",
      "1234": 412057020
    },
    {
      "1": 4762,
      "roma": "COLI",
      "rm": "PC",
      "1234": 408033016
    },
    {
      "1": 4763,
      "roma": "COLICO",
      "rm": "LC",
      "1234": 403097023
    },
    {
      "1": 4764,
      "roma": "COLLALTO SABINO",
      "rm": "RI",
      "1234": 412057018
    },
    {
      "1": 4765,
      "roma": "COLLARMELE",
      "rm": "AQ",
      "1234": 413066038
    },
    {
      "1": 4766,
      "roma": "COLLAZZONE",
      "rm": "PG",
      "1234": 410054014
    },
    {
      "1": 4767,
      "roma": "COLLERETTO GIACOSA",
      "rm": "TO",
      "1234": 401001092
    },
    {
      "1": 4768,
      "roma": "COLLESALVETTI",
      "rm": "LI",
      "1234": 409049008
    },
    {
      "1": 4769,
      "roma": "COLLE D'ANCHISE",
      "rm": "CB",
      "1234": 414070020
    },
    {
      "1": 4770,
      "roma": "COLLE DI TORA",
      "rm": "RI",
      "1234": 412057019
    },
    {
      "1": 4771,
      "roma": "COLLE DI VAL D'ELSA",
      "rm": "SI",
      "1234": 409052012
    },
    {
      "1": 4772,
      "roma": "COLLETORTO",
      "rm": "CB",
      "1234": 414070021
    },
    {
      "1": 4773,
      "roma": "COLLEVECCHIO",
      "rm": "RI",
      "1234": 412057021
    },
    {
      "1": 4774,
      "roma": "COLLI A VOLTURNO",
      "rm": "IS",
      "1234": 414094017
    },
    {
      "1": 4775,
      "roma": "COLLI DEL TRONTO",
      "rm": "AP",
      "1234": 411044014
    },
    {
      "1": 4776,
      "roma": "COLLI SUL VELINO",
      "rm": "RI",
      "1234": 412057022
    },
    {
      "1": 4777,
      "roma": "COLLIANO",
      "rm": "SA",
      "1234": 415065043
    },
    {
      "1": 4778,
      "roma": "COLLINAS",
      "rm": "CA",
      "1234": 420092014
    },
    {
      "1": 4779,
      "roma": "COLLIO",
      "rm": "BS",
      "1234": 403017058
    },
    {
      "1": 4780,
      "roma": "COLLOBIANO",
      "rm": "VC",
      "1234": 401002045
    },
    {
      "1": 4781,
      "roma": "COLLOREDO DI MONTE ALBANO",
      "rm": "UD",
      "1234": 406030028
    },
    {
      "1": 4782,
      "roma": "COLMURANO",
      "rm": "MC",
      "1234": 411043014
    },
    {
      "1": 4783,
      "roma": "COLLEGNO",
      "rm": "TO",
      "1234": 401001090
    },
    {
      "1": 4784,
      "roma": "COLLELONGO",
      "rm": "AQ",
      "1234": 413066039
    },
    {
      "1": 4785,
      "roma": "COLLEPARDO",
      "rm": "FR",
      "1234": 412060028
    },
    {
      "1": 4786,
      "roma": "COLLEPASSO",
      "rm": "LE",
      "1234": 416075021
    },
    {
      "1": 4787,
      "roma": "COLLEPIETRO",
      "rm": "AQ",
      "1234": 413066040
    },
    {
      "1": 4788,
      "roma": "COLLERETTO CASTELNUOVO",
      "rm": "TO",
      "1234": 401001091
    },
    {
      "1": 4789,
      "roma": "COLOGNO MONZESE",
      "rm": "MI",
      "1234": 403015081
    },
    {
      "1": 4790,
      "roma": "COLOGNOLA AI COLLI",
      "rm": "VR",
      "1234": 405023028
    },
    {
      "1": 4791,
      "roma": "COLLESANO",
      "rm": "PA",
      "1234": 419082032
    },
    {
      "1": 4792,
      "roma": "COLONNA",
      "rm": "RM",
      "1234": 412058035
    },
    {
      "1": 4793,
      "roma": "COLONNELLA",
      "rm": "TE",
      "1234": 413067019
    },
    {
      "1": 4794,
      "roma": "COLONNO",
      "rm": "CO",
      "1234": 403013074
    },
    {
      "1": 4795,
      "roma": "COLORINA",
      "rm": "SO",
      "1234": 403014023
    },
    {
      "1": 4796,
      "roma": "COLORNO",
      "rm": "PR",
      "1234": 408034010
    },
    {
      "1": 4797,
      "roma": "COLOSIMI",
      "rm": "CS",
      "1234": 418078043
    },
    {
      "1": 4798,
      "roma": "COLTURANO",
      "rm": "MI",
      "1234": 403015082
    },
    {
      "1": 4799,
      "roma": "COLZATE",
      "rm": "BG",
      "1234": 403016080
    },
    {
      "1": 4800,
      "roma": "COLVERDE",
      "rm": "CO",
      "1234": 403013251
    },
    {
      "1": 4801,
      "roma": "COMABBIO",
      "rm": "VA",
      "1234": 403012054
    },
    {
      "1": 4802,
      "roma": "COMACCHIO",
      "rm": "FE",
      "1234": 408038006
    },
    {
      "1": 4803,
      "roma": "COLOBRARO",
      "rm": "MT",
      "1234": 417077006
    },
    {
      "1": 4804,
      "roma": "COLOGNA VENETA",
      "rm": "VR",
      "1234": 405023027
    },
    {
      "1": 4805,
      "roma": "COLOGNE",
      "rm": "BS",
      "1234": 403017059
    },
    {
      "1": 4806,
      "roma": "COLOGNO AL SERIO",
      "rm": "BG",
      "1234": 403016079
    },
    {
      "1": 4807,
      "roma": "COMELICO SUPERIORE",
      "rm": "BL",
      "1234": 405025015
    },
    {
      "1": 4808,
      "roma": "COMEZZANO-CIZZAGO",
      "rm": "BS",
      "1234": 403017060
    },
    {
      "1": 4809,
      "roma": "COMIGNAGO",
      "rm": "NO",
      "1234": 401003052
    },
    {
      "1": 4810,
      "roma": "COMISO",
      "rm": "RG",
      "1234": 419088003
    },
    {
      "1": 4811,
      "roma": "COMITINI",
      "rm": "AG",
      "1234": 419084016
    },
    {
      "1": 4812,
      "roma": "COMIZIANO",
      "rm": "NA",
      "1234": 415063029
    },
    {
      "1": 4813,
      "roma": "COMMESSAGGIO",
      "rm": "MN",
      "1234": 403020020
    },
    {
      "1": 4814,
      "roma": "COMMEZZADURA",
      "rm": "TN",
      "1234": 404022064
    },
    {
      "1": 4815,
      "roma": "COMO",
      "rm": "CO",
      "1234": 403013075
    },
    {
      "1": 4816,
      "roma": "COMPIANO",
      "rm": "PR",
      "1234": 408034011
    },
    {
      "1": 4817,
      "roma": "COMANO TERME",
      "rm": "TN",
      "1234": 404022228
    },
    {
      "1": 4818,
      "roma": "COMANO",
      "rm": "MS",
      "1234": 409045005
    },
    {
      "1": 4819,
      "roma": "COMAZZO",
      "rm": "LO",
      "1234": 403098020
    },
    {
      "1": 4820,
      "roma": "COMEGLIANS",
      "rm": "UD",
      "1234": 406030029
    },
    {
      "1": 4821,
      "roma": "CONCAMARISE",
      "rm": "VR",
      "1234": 405023029
    },
    {
      "1": 4822,
      "roma": "COMERIO",
      "rm": "VA",
      "1234": 403012055
    },
    {
      "1": 4823,
      "roma": "CONCESIO",
      "rm": "BS",
      "1234": 403017061
    },
    {
      "1": 4824,
      "roma": "CONCORDIA SAGITTARIA",
      "rm": "VE",
      "1234": 405027011
    },
    {
      "1": 4825,
      "roma": "CONCORDIA SULLA SECCHIA",
      "rm": "MO",
      "1234": 408036010
    },
    {
      "1": 4826,
      "roma": "CONDOFURI",
      "rm": "RC",
      "1234": 418080029
    },
    {
      "1": 4827,
      "roma": "CONDOVE",
      "rm": "TO",
      "1234": 401001093
    },
    {
      "1": 4828,
      "roma": "CONDRO'",
      "rm": "ME",
      "1234": 419083018
    },
    {
      "1": 4829,
      "roma": "CONEGLIANO",
      "rm": "TV",
      "1234": 405026021
    },
    {
      "1": 4830,
      "roma": "CONFIENZA",
      "rm": "PV",
      "1234": 403018052
    },
    {
      "1": 4831,
      "roma": "CONFIGNI",
      "rm": "RI",
      "1234": 412057024
    },
    {
      "1": 4832,
      "roma": "CONFLENTI",
      "rm": "CZ",
      "1234": 418079033
    },
    {
      "1": 4833,
      "roma": "COMUNANZA",
      "rm": "AP",
      "1234": 411044015
    },
    {
      "1": 4834,
      "roma": "COMUN NUOVO",
      "rm": "BG",
      "1234": 403016081
    },
    {
      "1": 4835,
      "roma": "CONA",
      "rm": "VE",
      "1234": 405027010
    },
    {
      "1": 4836,
      "roma": "CONCA CASALE",
      "rm": "IS",
      "1234": 414094018
    },
    {
      "1": 4837,
      "roma": "CONCA DEI MARINI",
      "rm": "SA",
      "1234": 415065044
    },
    {
      "1": 4838,
      "roma": "CONCA DELLA CAMPANIA",
      "rm": "CE",
      "1234": 415061031
    },
    {
      "1": 4839,
      "roma": "CONCERVIANO",
      "rm": "RI",
      "1234": 412057023
    },
    {
      "1": 4840,
      "roma": "CONTRADA",
      "rm": "AV",
      "1234": 415064029
    },
    {
      "1": 4841,
      "roma": "CONTROGUERRA",
      "rm": "TE",
      "1234": 413067020
    },
    {
      "1": 4842,
      "roma": "CONTRONE",
      "rm": "SA",
      "1234": 415065045
    },
    {
      "1": 4843,
      "roma": "CONTURSI",
      "rm": "SA",
      "1234": 415065046
    },
    {
      "1": 4844,
      "roma": "CONVERSANO",
      "rm": "BA",
      "1234": 416072019
    },
    {
      "1": 4845,
      "roma": "CONZA DELLA CAMPANIA",
      "rm": "AV",
      "1234": 415064030
    },
    {
      "1": 4846,
      "roma": "CONZANO",
      "rm": "AL",
      "1234": 401006061
    },
    {
      "1": 4847,
      "roma": "COPERTINO",
      "rm": "LE",
      "1234": 416075022
    },
    {
      "1": 4848,
      "roma": "COPIANO",
      "rm": "PV",
      "1234": 403018053
    },
    {
      "1": 4849,
      "roma": "COPPARO",
      "rm": "FE",
      "1234": 408038007
    },
    {
      "1": 4850,
      "roma": "CORANA",
      "rm": "PV",
      "1234": 403018054
    },
    {
      "1": 4851,
      "roma": "CORATO",
      "rm": "BA",
      "1234": 416072020
    },
    {
      "1": 4852,
      "roma": "CONCOREZZO",
      "rm": "MB",
      "1234": 403108021
    },
    {
      "1": 4853,
      "roma": "CONIOLO",
      "rm": "AL",
      "1234": 401006060
    },
    {
      "1": 4854,
      "roma": "CONSELICE",
      "rm": "RA",
      "1234": 408039008
    },
    {
      "1": 4855,
      "roma": "CONSELVE",
      "rm": "PD",
      "1234": 405028034
    },
    {
      "1": 4856,
      "roma": "CORCIANO",
      "rm": "PG",
      "1234": 410054015
    },
    {
      "1": 4857,
      "roma": "CORDENONS",
      "rm": "PN",
      "1234": 406093017
    },
    {
      "1": 4858,
      "roma": "CONTESSA ENTELLINA",
      "rm": "PA",
      "1234": 419082033
    },
    {
      "1": 4859,
      "roma": "CONTIGLIANO",
      "rm": "RI",
      "1234": 412057025
    },
    {
      "1": 4860,
      "roma": "COREGLIA ANTELMINELLI",
      "rm": "LU",
      "1234": 409046011
    },
    {
      "1": 4861,
      "roma": "COREGLIA LIGURE",
      "rm": "GE",
      "1234": 407010019
    },
    {
      "1": 4862,
      "roma": "CORENO AUSONIO",
      "rm": "FR",
      "1234": 412060030
    },
    {
      "1": 4863,
      "roma": "CORFINIO",
      "rm": "AQ",
      "1234": 413066041
    },
    {
      "1": 4864,
      "roma": "CORI",
      "rm": "LT",
      "1234": 412059006
    },
    {
      "1": 4865,
      "roma": "CORIANO",
      "rm": "RN",
      "1234": 408099003
    },
    {
      "1": 4866,
      "roma": "CORBARA",
      "rm": "SA",
      "1234": 415065047
    },
    {
      "1": 4867,
      "roma": "CORBETTA",
      "rm": "MI",
      "1234": 403015085
    },
    {
      "1": 4868,
      "roma": "CORBOLA",
      "rm": "RO",
      "1234": 405029017
    },
    {
      "1": 4869,
      "roma": "CONTA'",
      "rm": "TN",
      "1234": 404022242
    },
    {
      "1": 4870,
      "roma": "CORCHIANO",
      "rm": "VT",
      "1234": 412056023
    },
    {
      "1": 4871,
      "roma": "CORLETO PERTICARA",
      "rm": "PZ",
      "1234": 417076029
    },
    {
      "1": 4872,
      "roma": "CORMANO",
      "rm": "MI",
      "1234": 403015086
    },
    {
      "1": 4873,
      "roma": "CORMONS",
      "rm": "GO",
      "1234": 406031002
    },
    {
      "1": 4874,
      "roma": "CORNA IMAGNA",
      "rm": "BG",
      "1234": 403016082
    },
    {
      "1": 4875,
      "roma": "CORDIGNANO",
      "rm": "TV",
      "1234": 405026022
    },
    {
      "1": 4876,
      "roma": "CORDOVADO",
      "rm": "PN",
      "1234": 406093018
    },
    {
      "1": 4877,
      "roma": "CORNEDO ALL'ISARCO",
      "rm": "BZ",
      "1234": 404021023
    },
    {
      "1": 4878,
      "roma": "CORNEDO VICENTINO",
      "rm": "VI",
      "1234": 405024034
    },
    {
      "1": 4879,
      "roma": "CORNEGLIANO LAUDENSE",
      "rm": "LO",
      "1234": 403098021
    },
    {
      "1": 4880,
      "roma": "CORNELIANO D'ALBA",
      "rm": "CN",
      "1234": 401004072
    },
    {
      "1": 4881,
      "roma": "CORNIGLIO",
      "rm": "PR",
      "1234": 408034012
    },
    {
      "1": 4882,
      "roma": "CORNO DI ROSAZZO",
      "rm": "UD",
      "1234": 406030030
    },
    {
      "1": 4883,
      "roma": "CORNO GIOVINE",
      "rm": "LO",
      "1234": 403098022
    },
    {
      "1": 4884,
      "roma": "CORNOVECCHIO",
      "rm": "LO",
      "1234": 403098023
    },
    {
      "1": 4885,
      "roma": "CORIGLIANO D'OTRANTO",
      "rm": "LE",
      "1234": 416075023
    },
    {
      "1": 4886,
      "roma": "CORINALDO",
      "rm": "AN",
      "1234": 411042015
    },
    {
      "1": 4887,
      "roma": "CORIO",
      "rm": "TO",
      "1234": 401001094
    },
    {
      "1": 4888,
      "roma": "CORLEONE",
      "rm": "PA",
      "1234": 419082034
    },
    {
      "1": 4889,
      "roma": "CORLETO MONFORTE",
      "rm": "SA",
      "1234": 415065048
    },
    {
      "1": 4890,
      "roma": "CORREGGIO",
      "rm": "RE",
      "1234": 408035020
    },
    {
      "1": 4891,
      "roma": "CORREZZANA",
      "rm": "MB",
      "1234": 403108022
    },
    {
      "1": 4892,
      "roma": "CORNALBA",
      "rm": "BG",
      "1234": 403016249
    },
    {
      "1": 4893,
      "roma": "CORNAREDO",
      "rm": "MI",
      "1234": 403015087
    },
    {
      "1": 4894,
      "roma": "CORROPOLI",
      "rm": "TE",
      "1234": 413067021
    },
    {
      "1": 4895,
      "roma": "CORSANO",
      "rm": "LE",
      "1234": 416075024
    },
    {
      "1": 4896,
      "roma": "CORSICO",
      "rm": "MI",
      "1234": 403015093
    },
    {
      "1": 4897,
      "roma": "CORSIONE",
      "rm": "AT",
      "1234": 401005044
    },
    {
      "1": 4898,
      "roma": "CORTACCIA SULLA STRADA DEL VINO",
      "rm": "BZ",
      "1234": 404021024
    },
    {
      "1": 4899,
      "roma": "CORTALE",
      "rm": "CZ",
      "1234": 418079034
    },
    {
      "1": 4900,
      "roma": "CORTANDONE",
      "rm": "AT",
      "1234": 401005045
    },
    {
      "1": 4901,
      "roma": "CORTANZE",
      "rm": "AT",
      "1234": 401005046
    },
    {
      "1": 4902,
      "roma": "CORTAZZONE",
      "rm": "AT",
      "1234": 401005047
    },
    {
      "1": 4903,
      "roma": "CORTE BRUGNATELLA",
      "rm": "PC",
      "1234": 408033017
    },
    {
      "1": 4904,
      "roma": "CORNUDA",
      "rm": "TV",
      "1234": 405026023
    },
    {
      "1": 4905,
      "roma": "CORTEMAGGIORE",
      "rm": "PC",
      "1234": 408033018
    },
    {
      "1": 4906,
      "roma": "CORTEMILIA",
      "rm": "CN",
      "1234": 401004073
    },
    {
      "1": 4907,
      "roma": "CORTENO GOLGI",
      "rm": "BS",
      "1234": 403017063
    },
    {
      "1": 4908,
      "roma": "CORREZZOLA",
      "rm": "PD",
      "1234": 405028035
    },
    {
      "1": 4909,
      "roma": "CORRIDO",
      "rm": "CO",
      "1234": 403013077
    },
    {
      "1": 4910,
      "roma": "CORRIDONIA",
      "rm": "MC",
      "1234": 411043015
    },
    {
      "1": 4911,
      "roma": "CORTIGLIONE",
      "rm": "AT",
      "1234": 401005048
    },
    {
      "1": 4912,
      "roma": "CORTINA D'AMPEZZO",
      "rm": "BL",
      "1234": 405025016
    },
    {
      "1": 4913,
      "roma": "CORTINA SULLA STRADA DEL VINO",
      "rm": "BZ",
      "1234": 404021025
    },
    {
      "1": 4914,
      "roma": "CORTINO",
      "rm": "TE",
      "1234": 413067022
    },
    {
      "1": 4915,
      "roma": "CORTONA",
      "rm": "AR",
      "1234": 409051017
    },
    {
      "1": 4916,
      "roma": "CORTE DE' CORTESI CON CIGNONE",
      "rm": "CR",
      "1234": 403019032
    },
    {
      "1": 4917,
      "roma": "CORTE DE' FRATI",
      "rm": "CR",
      "1234": 403019033
    },
    {
      "1": 4918,
      "roma": "CORTE FRANCA",
      "rm": "BS",
      "1234": 403017062
    },
    {
      "1": 4919,
      "roma": "CORTE PALASIO",
      "rm": "LO",
      "1234": 403098024
    },
    {
      "1": 4920,
      "roma": "COSEANO",
      "rm": "UD",
      "1234": 406030031
    },
    {
      "1": 4921,
      "roma": "COSENZA",
      "rm": "CS",
      "1234": 418078045
    },
    {
      "1": 4922,
      "roma": "COSIO D'ARROSCIA",
      "rm": "IM",
      "1234": 407008023
    },
    {
      "1": 4923,
      "roma": "CORTENOVA",
      "rm": "LC",
      "1234": 403097025
    },
    {
      "1": 4924,
      "roma": "CORTENUOVA",
      "rm": "BG",
      "1234": 403016083
    },
    {
      "1": 4925,
      "roma": "COSSANO BELBO",
      "rm": "CN",
      "1234": 401004074
    },
    {
      "1": 4926,
      "roma": "COSSANO CANAVESE",
      "rm": "TO",
      "1234": 401001095
    },
    {
      "1": 4927,
      "roma": "COSSATO",
      "rm": "BI",
      "1234": 401096020
    },
    {
      "1": 4928,
      "roma": "COSSERIA",
      "rm": "SV",
      "1234": 407009026
    },
    {
      "1": 4929,
      "roma": "COSSIGNANO",
      "rm": "AP",
      "1234": 411044016
    },
    {
      "1": 4930,
      "roma": "COSSOGNO",
      "rm": "VB",
      "1234": 401103023
    },
    {
      "1": 4931,
      "roma": "CORVARA",
      "rm": "PE",
      "1234": 413068016
    },
    {
      "1": 4932,
      "roma": "CORVARA IN BADIA",
      "rm": "BZ",
      "1234": 404021026
    },
    {
      "1": 4933,
      "roma": "CORVINO SAN QUIRICO",
      "rm": "PV",
      "1234": 403018057
    },
    {
      "1": 4934,
      "roma": "CORZANO",
      "rm": "BS",
      "1234": 403017064
    },
    {
      "1": 4935,
      "roma": "COSTA DI ROVIGO",
      "rm": "RO",
      "1234": 405029018
    },
    {
      "1": 4936,
      "roma": "COSTA SERINA",
      "rm": "BG",
      "1234": 403016247
    },
    {
      "1": 4937,
      "roma": "COSTA MASNAGA",
      "rm": "LC",
      "1234": 403097026
    },
    {
      "1": 4938,
      "roma": "COSTA VALLE IMAGNA",
      "rm": "BG",
      "1234": 403016085
    },
    {
      "1": 4939,
      "roma": "COSIO VALTELLINO",
      "rm": "SO",
      "1234": 403014024
    },
    {
      "1": 4940,
      "roma": "CORTEOLONA E GENZONE",
      "rm": "PV",
      "1234": 403018192
    },
    {
      "1": 4941,
      "roma": "COSOLETO",
      "rm": "RC",
      "1234": 418080030
    },
    {
      "1": 4942,
      "roma": "COSTACCIARO",
      "rm": "PG",
      "1234": 410054016
    },
    {
      "1": 4943,
      "roma": "COSTANZANA",
      "rm": "VC",
      "1234": 401002047
    },
    {
      "1": 4944,
      "roma": "COSTARAINERA",
      "rm": "IM",
      "1234": 407008024
    },
    {
      "1": 4945,
      "roma": "COSTERMANO",
      "rm": "VR",
      "1234": 405023030
    },
    {
      "1": 4946,
      "roma": "COSTIGLIOLE D'ASTI",
      "rm": "AT",
      "1234": 401005050
    },
    {
      "1": 4947,
      "roma": "COSTIGLIOLE SALUZZO",
      "rm": "CN",
      "1234": 401004075
    },
    {
      "1": 4948,
      "roma": "COTIGNOLA",
      "rm": "RA",
      "1234": 408039009
    },
    {
      "1": 4949,
      "roma": "COTRONEI",
      "rm": "KR",
      "1234": 418101009
    },
    {
      "1": 4950,
      "roma": "COSSOINE",
      "rm": "SS",
      "1234": 420090027
    },
    {
      "1": 4951,
      "roma": "COSSOMBRATO",
      "rm": "AT",
      "1234": 401005049
    },
    {
      "1": 4952,
      "roma": "COSTA DE' NOBILI",
      "rm": "PV",
      "1234": 403018058
    },
    {
      "1": 4953,
      "roma": "COSTA DI MEZZATE",
      "rm": "BG",
      "1234": 403016084
    },
    {
      "1": 4954,
      "roma": "CRACO",
      "rm": "MT",
      "1234": 417077007
    },
    {
      "1": 4955,
      "roma": "CRANDOLA VALSASSINA",
      "rm": "LC",
      "1234": 403097027
    },
    {
      "1": 4956,
      "roma": "CRAVAGLIANA",
      "rm": "VC",
      "1234": 401002048
    },
    {
      "1": 4957,
      "roma": "COSTA VESCOVATO",
      "rm": "AL",
      "1234": 401006062
    },
    {
      "1": 4958,
      "roma": "COSTA VOLPINO",
      "rm": "BG",
      "1234": 403016086
    },
    {
      "1": 4959,
      "roma": "COSTABISSARA",
      "rm": "VI",
      "1234": 405024035
    },
    {
      "1": 4960,
      "roma": "CREAZZO",
      "rm": "VI",
      "1234": 405024036
    },
    {
      "1": 4961,
      "roma": "CRECCHIO",
      "rm": "CH",
      "1234": 413069027
    },
    {
      "1": 4962,
      "roma": "CREDARO",
      "rm": "BG",
      "1234": 403016088
    },
    {
      "1": 4963,
      "roma": "CREDERA RUBBIANO",
      "rm": "CR",
      "1234": 403019034
    },
    {
      "1": 4964,
      "roma": "CREMA",
      "rm": "CR",
      "1234": 403019035
    },
    {
      "1": 4965,
      "roma": "CREMELLA",
      "rm": "LC",
      "1234": 403097028
    },
    {
      "1": 4966,
      "roma": "COTTANELLO",
      "rm": "RI",
      "1234": 412057026
    },
    {
      "1": 4967,
      "roma": "COURMAYEUR",
      "rm": "AO",
      "1234": 402007022
    },
    {
      "1": 4968,
      "roma": "COVO",
      "rm": "BG",
      "1234": 403016087
    },
    {
      "1": 4969,
      "roma": "COZZO",
      "rm": "PV",
      "1234": 403018059
    },
    {
      "1": 4970,
      "roma": "CREMOLINO",
      "rm": "AL",
      "1234": 401006063
    },
    {
      "1": 4971,
      "roma": "CREMONA",
      "rm": "CR",
      "1234": 403019036
    },
    {
      "1": 4972,
      "roma": "CREMOSANO",
      "rm": "CR",
      "1234": 403019037
    },
    {
      "1": 4973,
      "roma": "CRAVANZANA",
      "rm": "CN",
      "1234": 401004076
    },
    {
      "1": 4974,
      "roma": "CRAVEGGIA",
      "rm": "VB",
      "1234": 401103024
    },
    {
      "1": 4975,
      "roma": "CRESPIATICA",
      "rm": "LO",
      "1234": 403098025
    },
    {
      "1": 4976,
      "roma": "CRESPINO",
      "rm": "RO",
      "1234": 405029019
    },
    {
      "1": 4977,
      "roma": "CRESSA",
      "rm": "NO",
      "1234": 401003055
    },
    {
      "1": 4978,
      "roma": "CREVACUORE",
      "rm": "BI",
      "1234": 401096021
    },
    {
      "1": 4979,
      "roma": "CREMENAGA",
      "rm": "VA",
      "1234": 403012056
    },
    {
      "1": 4980,
      "roma": "CREMENO",
      "rm": "LC",
      "1234": 403097029
    },
    {
      "1": 4981,
      "roma": "CREMIA",
      "rm": "CO",
      "1234": 403013083
    },
    {
      "1": 4982,
      "roma": "CRISPIANO",
      "rm": "TA",
      "1234": 416073004
    },
    {
      "1": 4983,
      "roma": "CRISSOLO",
      "rm": "CN",
      "1234": 401004077
    },
    {
      "1": 4984,
      "roma": "CROCEFIESCHI",
      "rm": "GE",
      "1234": 407010020
    },
    {
      "1": 4985,
      "roma": "CRESCENTINO",
      "rm": "VC",
      "1234": 401002049
    },
    {
      "1": 4986,
      "roma": "CRESPADORO",
      "rm": "VI",
      "1234": 405024037
    },
    {
      "1": 4987,
      "roma": "CRODO",
      "rm": "VB",
      "1234": 401103026
    },
    {
      "1": 4988,
      "roma": "CRESPINA LORENZANA",
      "rm": "PI",
      "1234": 409050041
    },
    {
      "1": 4989,
      "roma": "CROGNALETO",
      "rm": "TE",
      "1234": 413067023
    },
    {
      "1": 4990,
      "roma": "CROPALATI",
      "rm": "CS",
      "1234": 418078046
    },
    {
      "1": 4991,
      "roma": "CROPANI",
      "rm": "CZ",
      "1234": 418079036
    },
    {
      "1": 4992,
      "roma": "CROSIA",
      "rm": "CS",
      "1234": 418078047
    },
    {
      "1": 4993,
      "roma": "CREVALCORE",
      "rm": "BO",
      "1234": 408037024
    },
    {
      "1": 4994,
      "roma": "CREVOLADOSSOLA",
      "rm": "VB",
      "1234": 401103025
    },
    {
      "1": 4995,
      "roma": "CRISPANO",
      "rm": "NA",
      "1234": 415063030
    },
    {
      "1": 4996,
      "roma": "CRUCOLI",
      "rm": "KR",
      "1234": 418101011
    },
    {
      "1": 4997,
      "roma": "CUASSO AL MONTE",
      "rm": "VA",
      "1234": 403012058
    },
    {
      "1": 4998,
      "roma": "CROCETTA DEL MONTELLO",
      "rm": "TV",
      "1234": 405026025
    },
    {
      "1": 4999,
      "roma": "CUCEGLIO",
      "rm": "TO",
      "1234": 401001096
    },
    {
      "1": 5000,
      "roma": "CUGGIONO",
      "rm": "MI",
      "1234": 403015096
    },
    {
      "1": 5001,
      "roma": "CUGLIATE-FABIASCO",
      "rm": "VA",
      "1234": 403012059
    },
    {
      "1": 5002,
      "roma": "CUGLIERI",
      "rm": "OR",
      "1234": 420095019
    },
    {
      "1": 5003,
      "roma": "CUGNOLI",
      "rm": "PE",
      "1234": 413068017
    },
    {
      "1": 5004,
      "roma": "CUMIANA",
      "rm": "TO",
      "1234": 401001097
    },
    {
      "1": 5005,
      "roma": "CUMIGNANO SUL NAVIGLIO",
      "rm": "CR",
      "1234": 403019039
    },
    {
      "1": 5006,
      "roma": "CUNARDO",
      "rm": "VA",
      "1234": 403012060
    },
    {
      "1": 5007,
      "roma": "CUNEO",
      "rm": "CN",
      "1234": 401004078
    },
    {
      "1": 5008,
      "roma": "CROSIO DELLA VALLE",
      "rm": "VA",
      "1234": 403012057
    },
    {
      "1": 5009,
      "roma": "CROTONE",
      "rm": "KR",
      "1234": 418101010
    },
    {
      "1": 5010,
      "roma": "CROTTA D'ADDA",
      "rm": "CR",
      "1234": 403019038
    },
    {
      "1": 5011,
      "roma": "CROVA",
      "rm": "VC",
      "1234": 401002052
    },
    {
      "1": 5012,
      "roma": "CROVIANA",
      "rm": "TN",
      "1234": 404022068
    },
    {
      "1": 5013,
      "roma": "CUPRAMONTANA",
      "rm": "AN",
      "1234": 411042016
    },
    {
      "1": 5014,
      "roma": "CURA CARPIGNANO",
      "rm": "PV",
      "1234": 403018060
    },
    {
      "1": 5015,
      "roma": "CURCURIS",
      "rm": "OR",
      "1234": 420095077
    },
    {
      "1": 5016,
      "roma": "CUCCARO VETERE",
      "rm": "SA",
      "1234": 415065049
    },
    {
      "1": 5017,
      "roma": "CUCCIAGO",
      "rm": "CO",
      "1234": 403013084
    },
    {
      "1": 5018,
      "roma": "CURINO",
      "rm": "BI",
      "1234": 401096023
    },
    {
      "1": 5019,
      "roma": "CURNO",
      "rm": "BG",
      "1234": 403016089
    },
    {
      "1": 5020,
      "roma": "CURON VENOSTA",
      "rm": "BZ",
      "1234": 404021027
    },
    {
      "1": 5021,
      "roma": "FERMO",
      "rm": "FM",
      "1234": 411109006
    },
    {
      "1": 5022,
      "roma": "CURSI",
      "rm": "LE",
      "1234": 416075025
    },
    {
      "1": 5023,
      "roma": "CUNICO",
      "rm": "AT",
      "1234": 401005051
    },
    {
      "1": 5024,
      "roma": "CUORGNE'",
      "rm": "TO",
      "1234": 401001098
    },
    {
      "1": 5025,
      "roma": "CUPELLO",
      "rm": "CH",
      "1234": 413069028
    },
    {
      "1": 5026,
      "roma": "CUPRA MARITTIMA",
      "rm": "AP",
      "1234": 411044017
    },
    {
      "1": 5027,
      "roma": "CUSINO",
      "rm": "CO",
      "1234": 403013085
    },
    {
      "1": 5028,
      "roma": "CUSIO",
      "rm": "BG",
      "1234": 403016090
    },
    {
      "1": 5029,
      "roma": "CUSTONACI",
      "rm": "TP",
      "1234": 419081007
    },
    {
      "1": 5030,
      "roma": "CUTRO",
      "rm": "KR",
      "1234": 418101012
    },
    {
      "1": 5031,
      "roma": "CUTROFIANO",
      "rm": "LE",
      "1234": 416075026
    },
    {
      "1": 5032,
      "roma": "CUREGGIO",
      "rm": "NO",
      "1234": 401003058
    },
    {
      "1": 5033,
      "roma": "CURIGLIA CON MONTEVIASCO",
      "rm": "VA",
      "1234": 403012061
    },
    {
      "1": 5034,
      "roma": "CURINGA",
      "rm": "CZ",
      "1234": 418079039
    },
    {
      "1": 5035,
      "roma": "DAIANO",
      "rm": "TN",
      "1234": 404022070
    },
    {
      "1": 5036,
      "roma": "DAIRAGO",
      "rm": "MI",
      "1234": 403015099
    },
    {
      "1": 5037,
      "roma": "DALMINE",
      "rm": "BG",
      "1234": 403016091
    },
    {
      "1": 5038,
      "roma": "DAMBEL",
      "rm": "TN",
      "1234": 404022071
    },
    {
      "1": 5039,
      "roma": "DANTA DI CADORE",
      "rm": "BL",
      "1234": 405025017
    },
    {
      "1": 5040,
      "roma": "CURTAROLO",
      "rm": "PD",
      "1234": 405028036
    },
    {
      "1": 5041,
      "roma": "CURTATONE",
      "rm": "MN",
      "1234": 403020021
    },
    {
      "1": 5042,
      "roma": "CURTI",
      "rm": "CE",
      "1234": 415061032
    },
    {
      "1": 5043,
      "roma": "CUSAGO",
      "rm": "MI",
      "1234": 403015097
    },
    {
      "1": 5044,
      "roma": "CUSANO MILANINO",
      "rm": "MI",
      "1234": 403015098
    },
    {
      "1": 5045,
      "roma": "CUSANO MUTRI",
      "rm": "BN",
      "1234": 415062026
    },
    {
      "1": 5046,
      "roma": "DAVAGNA",
      "rm": "GE",
      "1234": 407010021
    },
    {
      "1": 5047,
      "roma": "DAVERIO",
      "rm": "VA",
      "1234": 403012064
    },
    {
      "1": 5048,
      "roma": "DAVOLI",
      "rm": "CZ",
      "1234": 418079042
    },
    {
      "1": 5049,
      "roma": "DAZIO",
      "rm": "SO",
      "1234": 403014025
    },
    {
      "1": 5050,
      "roma": "DECIMOMANNU",
      "rm": "CA",
      "1234": 420092015
    },
    {
      "1": 5051,
      "roma": "DECIMOPUTZU",
      "rm": "CA",
      "1234": 420092016
    },
    {
      "1": 5052,
      "roma": "CUVEGLIO",
      "rm": "VA",
      "1234": 403012062
    },
    {
      "1": 5053,
      "roma": "CUVIO",
      "rm": "VA",
      "1234": 403012063
    },
    {
      "1": 5054,
      "roma": "DEGO",
      "rm": "SV",
      "1234": 407009027
    },
    {
      "1": 5055,
      "roma": "DEIVA MARINA",
      "rm": "SP",
      "1234": 407011012
    },
    {
      "1": 5056,
      "roma": "DELEBIO",
      "rm": "SO",
      "1234": 403014026
    },
    {
      "1": 5057,
      "roma": "DELIA",
      "rm": "CL",
      "1234": 419085006
    },
    {
      "1": 5058,
      "roma": "DELIANUOVA",
      "rm": "RC",
      "1234": 418080031
    },
    {
      "1": 5059,
      "roma": "DELICETO",
      "rm": "FG",
      "1234": 416071022
    },
    {
      "1": 5060,
      "roma": "DELLO",
      "rm": "BS",
      "1234": 403017066
    },
    {
      "1": 5061,
      "roma": "DEMONTE",
      "rm": "CN",
      "1234": 401004079
    },
    {
      "1": 5062,
      "roma": "DENICE",
      "rm": "AL",
      "1234": 401006065
    },
    {
      "1": 5063,
      "roma": "DARFO BOARIO TERME",
      "rm": "BS",
      "1234": 403017065
    },
    {
      "1": 5064,
      "roma": "DASA'",
      "rm": "VV",
      "1234": 418102007
    },
    {
      "1": 5065,
      "roma": "DERVIO",
      "rm": "LC",
      "1234": 403097030
    },
    {
      "1": 5066,
      "roma": "DESANA",
      "rm": "VC",
      "1234": 401002054
    },
    {
      "1": 5067,
      "roma": "DESENZANO DEL GARDA",
      "rm": "BS",
      "1234": 403017067
    },
    {
      "1": 5068,
      "roma": "DECOLLATURA",
      "rm": "CZ",
      "1234": 418079043
    },
    {
      "1": 5069,
      "roma": "DIAMANTE",
      "rm": "CS",
      "1234": 418078048
    },
    {
      "1": 5070,
      "roma": "DIANO ARENTINO",
      "rm": "IM",
      "1234": 407008025
    },
    {
      "1": 5071,
      "roma": "DIANO CASTELLO",
      "rm": "IM",
      "1234": 407008026
    },
    {
      "1": 5072,
      "roma": "DIANO D'ALBA",
      "rm": "CN",
      "1234": 401004080
    },
    {
      "1": 5073,
      "roma": "DIANO MARINA",
      "rm": "IM",
      "1234": 407008027
    },
    {
      "1": 5074,
      "roma": "DIANO SAN PIETRO",
      "rm": "IM",
      "1234": 407008028
    },
    {
      "1": 5075,
      "roma": "DICOMANO",
      "rm": "FI",
      "1234": 409048013
    },
    {
      "1": 5076,
      "roma": "DENNO",
      "rm": "TN",
      "1234": 404022074
    },
    {
      "1": 5077,
      "roma": "DERNICE",
      "rm": "AL",
      "1234": 401006066
    },
    {
      "1": 5078,
      "roma": "DEROVERE",
      "rm": "CR",
      "1234": 403019040
    },
    {
      "1": 5079,
      "roma": "DERUTA",
      "rm": "PG",
      "1234": 410054017
    },
    {
      "1": 5080,
      "roma": "DESULO",
      "rm": "NU",
      "1234": 420091016
    },
    {
      "1": 5081,
      "roma": "DIVIGNANO",
      "rm": "NO",
      "1234": 401003060
    },
    {
      "1": 5082,
      "roma": "DIZZASCO",
      "rm": "CO",
      "1234": 403013087
    },
    {
      "1": 5083,
      "roma": "DOBBIACO",
      "rm": "BZ",
      "1234": 404021028
    },
    {
      "1": 5084,
      "roma": "DOBERDO' DEL LAGO",
      "rm": "GO",
      "1234": 406031003
    },
    {
      "1": 5085,
      "roma": "DOGLIANI",
      "rm": "CN",
      "1234": 401004081
    },
    {
      "1": 5086,
      "roma": "DOGLIOLA",
      "rm": "CH",
      "1234": 413069029
    },
    {
      "1": 5087,
      "roma": "DESIO",
      "rm": "MB",
      "1234": 403108023
    },
    {
      "1": 5088,
      "roma": "DIGNANO",
      "rm": "UD",
      "1234": 406030032
    },
    {
      "1": 5089,
      "roma": "DINAMI",
      "rm": "VV",
      "1234": 418102008
    },
    {
      "1": 5090,
      "roma": "DOLCEDO",
      "rm": "IM",
      "1234": 407008030
    },
    {
      "1": 5091,
      "roma": "DOLEGNA DEL COLLIO",
      "rm": "GO",
      "1234": 406031004
    },
    {
      "1": 5092,
      "roma": "DOLIANOVA",
      "rm": "CA",
      "1234": 420092017
    },
    {
      "1": 5093,
      "roma": "DOLO",
      "rm": "VE",
      "1234": 405027012
    },
    {
      "1": 5094,
      "roma": "DOLZAGO",
      "rm": "LC",
      "1234": 403097031
    },
    {
      "1": 5095,
      "roma": "DOMANICO",
      "rm": "CS",
      "1234": 418078050
    },
    {
      "1": 5096,
      "roma": "DIPIGNANO",
      "rm": "CS",
      "1234": 418078049
    },
    {
      "1": 5097,
      "roma": "DISO",
      "rm": "LE",
      "1234": 416075027
    },
    {
      "1": 5098,
      "roma": "DOMODOSSOLA",
      "rm": "VB",
      "1234": 401103028
    },
    {
      "1": 5099,
      "roma": "DOMUS DE MARIA",
      "rm": "CA",
      "1234": 420092018
    },
    {
      "1": 5100,
      "roma": "DOMUSNOVAS",
      "rm": "CA",
      "1234": 420092019
    },
    {
      "1": 5101,
      "roma": "DOGNA",
      "rm": "UD",
      "1234": 406030033
    },
    {
      "1": 5102,
      "roma": "DIMARO FOLGARIDA",
      "rm": "TN",
      "1234": 404022233
    },
    {
      "1": 5103,
      "roma": "DOLCE'",
      "rm": "VR",
      "1234": 405023031
    },
    {
      "1": 5104,
      "roma": "DOLCEACQUA",
      "rm": "IM",
      "1234": 407008029
    },
    {
      "1": 5105,
      "roma": "DONNAS",
      "rm": "AO",
      "1234": 402007023
    },
    {
      "1": 5106,
      "roma": "DONORI",
      "rm": "CA",
      "1234": 420092020
    },
    {
      "1": 5107,
      "roma": "DORGALI",
      "rm": "NU",
      "1234": 420091017
    },
    {
      "1": 5108,
      "roma": "DORIO",
      "rm": "LC",
      "1234": 403097032
    },
    {
      "1": 5109,
      "roma": "DORMELLETTO",
      "rm": "NO",
      "1234": 401003062
    },
    {
      "1": 5110,
      "roma": "DORNO",
      "rm": "PV",
      "1234": 403018061
    },
    {
      "1": 5111,
      "roma": "DOMASO",
      "rm": "CO",
      "1234": 403013089
    },
    {
      "1": 5112,
      "roma": "DOMEGGE DI CADORE",
      "rm": "BL",
      "1234": 405025018
    },
    {
      "1": 5113,
      "roma": "DOMICELLA",
      "rm": "AV",
      "1234": 415064031
    },
    {
      "1": 5114,
      "roma": "DOSSENA",
      "rm": "BG",
      "1234": 403016092
    },
    {
      "1": 5115,
      "roma": "DOSSO DEL LIRO",
      "rm": "CO",
      "1234": 403013092
    },
    {
      "1": 5116,
      "roma": "DOUES",
      "rm": "AO",
      "1234": 402007024
    },
    {
      "1": 5117,
      "roma": "DOVADOLA",
      "rm": "FO",
      "1234": 408040011
    },
    {
      "1": 5118,
      "roma": "DOVERA",
      "rm": "CR",
      "1234": 403019041
    },
    {
      "1": 5119,
      "roma": "DONATO",
      "rm": "BI",
      "1234": 401096024
    },
    {
      "1": 5120,
      "roma": "DONGO",
      "rm": "CO",
      "1234": 403013090
    },
    {
      "1": 5121,
      "roma": "DRENA",
      "rm": "TN",
      "1234": 404022078
    },
    {
      "1": 5122,
      "roma": "DRENCHIA",
      "rm": "UD",
      "1234": 406030034
    },
    {
      "1": 5123,
      "roma": "DRESANO",
      "rm": "MI",
      "1234": 403015101
    },
    {
      "1": 5124,
      "roma": "DRO'",
      "rm": "TN",
      "1234": 404022079
    },
    {
      "1": 5125,
      "roma": "DRONERO",
      "rm": "CN",
      "1234": 401004082
    },
    {
      "1": 5126,
      "roma": "DRUENTO",
      "rm": "TO",
      "1234": 401001099
    },
    {
      "1": 5127,
      "roma": "DORZANO",
      "rm": "BI",
      "1234": 401096025
    },
    {
      "1": 5128,
      "roma": "DOSOLO",
      "rm": "MN",
      "1234": 403020022
    },
    {
      "1": 5129,
      "roma": "DUALCHI",
      "rm": "NU",
      "1234": 420091018
    },
    {
      "1": 5130,
      "roma": "DUBINO",
      "rm": "SO",
      "1234": 403014027
    },
    {
      "1": 5131,
      "roma": "DUE CARRARE",
      "rm": "PD",
      "1234": 405028106
    },
    {
      "1": 5132,
      "roma": "DUEVILLE",
      "rm": "VI",
      "1234": 405024038
    },
    {
      "1": 5133,
      "roma": "DUGENTA",
      "rm": "BN",
      "1234": 415062027
    },
    {
      "1": 5134,
      "roma": "DOZZA",
      "rm": "BO",
      "1234": 408037025
    },
    {
      "1": 5135,
      "roma": "DRAGONI",
      "rm": "CE",
      "1234": 415061033
    },
    {
      "1": 5136,
      "roma": "DRAPIA",
      "rm": "VV",
      "1234": 418102009
    },
    {
      "1": 3716,
      "roma": "BASSANO DEL GRAPPA",
      "rm": "VI",
      "1234": 405024012
    },
    {
      "1": 3717,
      "roma": "BASSANO IN TEVERINA",
      "rm": "VT",
      "1234": 412056006
    },
    {
      "1": 3718,
      "roma": "BASSANO ROMANO",
      "rm": "VT",
      "1234": 412056005
    },
    {
      "1": 3719,
      "roma": "BASSIANO",
      "rm": "LT",
      "1234": 412059002
    },
    {
      "1": 3720,
      "roma": "BASSIGNANA",
      "rm": "AL",
      "1234": 401006013
    },
    {
      "1": 3721,
      "roma": "BASTIA MONDOVI'",
      "rm": "CN",
      "1234": 401004014
    },
    {
      "1": 3722,
      "roma": "BASTIA UMBRA",
      "rm": "PG",
      "1234": 410054002
    },
    {
      "1": 3723,
      "roma": "BARZIO",
      "rm": "LC",
      "1234": 403097007
    },
    {
      "1": 3724,
      "roma": "BARICELLA",
      "rm": "BO",
      "1234": 408037003
    },
    {
      "1": 3725,
      "roma": "BASALUZZO",
      "rm": "AL",
      "1234": 401006012
    },
    {
      "1": 3726,
      "roma": "BATTIFOLLO",
      "rm": "CN",
      "1234": 401004015
    },
    {
      "1": 3727,
      "roma": "BATTIPAGLIA",
      "rm": "SA",
      "1234": 415065014
    },
    {
      "1": 3728,
      "roma": "BATTUDA",
      "rm": "PV",
      "1234": 403018012
    },
    {
      "1": 3729,
      "roma": "BAUCINA",
      "rm": "PA",
      "1234": 419082008
    },
    {
      "1": 3730,
      "roma": "BAULADU",
      "rm": "OR",
      "1234": 420095013
    },
    {
      "1": 3731,
      "roma": "BAUNEI",
      "rm": "NU",
      "1234": 420091006
    },
    {
      "1": 3732,
      "roma": "BAVENO",
      "rm": "VB",
      "1234": 401103008
    },
    {
      "1": 3733,
      "roma": "BEDERO VALCUVIA",
      "rm": "VA",
      "1234": 403012010
    },
    {
      "1": 3734,
      "roma": "BEDIZZOLE",
      "rm": "BS",
      "1234": 403017014
    },
    {
      "1": 3735,
      "roma": "BEDOLLO",
      "rm": "TN",
      "1234": 404022011
    },
    {
      "1": 3736,
      "roma": "BEDONIA",
      "rm": "PR",
      "1234": 408034003
    },
    {
      "1": 3737,
      "roma": "BEDULITA",
      "rm": "BG",
      "1234": 403016022
    },
    {
      "1": 3738,
      "roma": "BEE",
      "rm": "VB",
      "1234": 401103009
    },
    {
      "1": 3739,
      "roma": "BASTIDA PANCARANA",
      "rm": "PV",
      "1234": 403018011
    },
    {
      "1": 3740,
      "roma": "BASTIGLIA",
      "rm": "MO",
      "1234": 408036001
    },
    {
      "1": 3741,
      "roma": "BATTAGLIA TERME",
      "rm": "PD",
      "1234": 405028011
    },
    {
      "1": 3742,
      "roma": "BELFORTE ALL'ISAURO",
      "rm": "PS",
      "1234": 411041005
    },
    {
      "1": 3743,
      "roma": "BELFORTE DEL CHIENTI",
      "rm": "MC",
      "1234": 411043004
    },
    {
      "1": 3744,
      "roma": "BELFORTE MONFERRATO",
      "rm": "AL",
      "1234": 401006014
    },
    {
      "1": 3745,
      "roma": "BELGIOIOSO",
      "rm": "PV",
      "1234": 403018013
    },
    {
      "1": 3746,
      "roma": "BELGIRATE",
      "rm": "VB",
      "1234": 401103010
    },
    {
      "1": 3747,
      "roma": "BELLA",
      "rm": "PZ",
      "1234": 417076012
    },
    {
      "1": 3748,
      "roma": "BELLANO",
      "rm": "LC",
      "1234": 403097008
    },
    {
      "1": 3749,
      "roma": "BELLANTE",
      "rm": "TE",
      "1234": 413067006
    },
    {
      "1": 3750,
      "roma": "BELLARIA-IGEA MARINA",
      "rm": "RN",
      "1234": 408099001
    },
    {
      "1": 3751,
      "roma": "BELLEGRA",
      "rm": "RM",
      "1234": 412058012
    },
    {
      "1": 3752,
      "roma": "BELLINO",
      "rm": "CN",
      "1234": 401004017
    },
    {
      "1": 3753,
      "roma": "BELLAGIO",
      "rm": "CO",
      "1234": 403013250
    },
    {
      "1": 3754,
      "roma": "BELLINZAGO LOMBARDO",
      "rm": "MI",
      "1234": 403015016
    },
    {
      "1": 3755,
      "roma": "BEINASCO",
      "rm": "TO",
      "1234": 401001024
    },
    {
      "1": 3756,
      "roma": "BEINETTE",
      "rm": "CN",
      "1234": 401004016
    },
    {
      "1": 3757,
      "roma": "BELFIORE",
      "rm": "VR",
      "1234": 405023007
    },
    {
      "1": 3758,
      "roma": "BELLUNO",
      "rm": "BL",
      "1234": 405025006
    },
    {
      "1": 3759,
      "roma": "BELMONTE CALABRO",
      "rm": "CS",
      "1234": 418078013
    },
    {
      "1": 3760,
      "roma": "BELMONTE CASTELLO",
      "rm": "FR",
      "1234": 412060013
    },
    {
      "1": 3761,
      "roma": "BELMONTE DEL SANNIO",
      "rm": "IS",
      "1234": 414094004
    },
    {
      "1": 3762,
      "roma": "BELMONTE IN SABINA",
      "rm": "RI",
      "1234": 412057005
    },
    {
      "1": 3763,
      "roma": "BELMONTE MEZZAGNO",
      "rm": "PA",
      "1234": 419082009
    },
    {
      "1": 3764,
      "roma": "BELPASSO",
      "rm": "CT",
      "1234": 419087007
    },
    {
      "1": 3765,
      "roma": "BELSITO",
      "rm": "CS",
      "1234": 418078014
    },
    {
      "1": 3766,
      "roma": "BELVEDERE DI SPINELLO",
      "rm": "KR",
      "1234": 418101001
    },
    {
      "1": 3767,
      "roma": "BELVEDERE LANGHE",
      "rm": "CN",
      "1234": 401004018
    },
    {
      "1": 3768,
      "roma": "BELVEDERE MARITTIMO",
      "rm": "CS",
      "1234": 418078015
    },
    {
      "1": 3769,
      "roma": "BELVEDERE OSTRENSE",
      "rm": "AN",
      "1234": 411042005
    },
    {
      "1": 3770,
      "roma": "BELVEGLIO",
      "rm": "AT",
      "1234": 401005008
    },
    {
      "1": 3771,
      "roma": "BELLINZAGO NOVARESE",
      "rm": "NO",
      "1234": 401003016
    },
    {
      "1": 3772,
      "roma": "BELLIZZI",
      "rm": "SA",
      "1234": 415065158
    },
    {
      "1": 3773,
      "roma": "BELLONA",
      "rm": "CE",
      "1234": 415061007
    },
    {
      "1": 3774,
      "roma": "BELCASTRO",
      "rm": "CZ",
      "1234": 418079009
    },
    {
      "1": 3775,
      "roma": "BENETUTTI",
      "rm": "SS",
      "1234": 420090008
    },
    {
      "1": 3776,
      "roma": "BENEVELLO",
      "rm": "CN",
      "1234": 401004020
    },
    {
      "1": 3777,
      "roma": "BENEVENTO",
      "rm": "BN",
      "1234": 415062008
    },
    {
      "1": 3778,
      "roma": "BENNA",
      "rm": "BI",
      "1234": 401096003
    },
    {
      "1": 3779,
      "roma": "BENTIVOGLIO",
      "rm": "BO",
      "1234": 408037005
    },
    {
      "1": 3780,
      "roma": "BERBENNO",
      "rm": "BG",
      "1234": 403016023
    },
    {
      "1": 3781,
      "roma": "BERBENNO DI VALTELLINA",
      "rm": "SO",
      "1234": 403014007
    },
    {
      "1": 3782,
      "roma": "BERCETO",
      "rm": "PR",
      "1234": 408034004
    },
    {
      "1": 3783,
      "roma": "BERCHIDDA",
      "rm": "SS",
      "1234": 420090009
    },
    {
      "1": 3784,
      "roma": "BEREGAZZO CON FIGLIARO",
      "rm": "CO",
      "1234": 403013022
    },
    {
      "1": 3785,
      "roma": "BEREGUARDO",
      "rm": "PV",
      "1234": 403018014
    },
    {
      "1": 3786,
      "roma": "BERGAMASCO",
      "rm": "AL",
      "1234": 401006015
    },
    {
      "1": 3787,
      "roma": "BERGAMO",
      "rm": "BG",
      "1234": 403016024
    },
    {
      "1": 3788,
      "roma": "BERGANTINO",
      "rm": "RO",
      "1234": 405029006
    },
    {
      "1": 3789,
      "roma": "BELLUSCO",
      "rm": "MB",
      "1234": 403108006
    },
    {
      "1": 3790,
      "roma": "BELVI",
      "rm": "NU",
      "1234": 420091007
    },
    {
      "1": 3791,
      "roma": "BEMA",
      "rm": "SO",
      "1234": 403014006
    },
    {
      "1": 3792,
      "roma": "BENE LARIO",
      "rm": "CO",
      "1234": 403013021
    },
    {
      "1": 3793,
      "roma": "BENE VAGIENNA",
      "rm": "CN",
      "1234": 401004019
    },
    {
      "1": 3794,
      "roma": "BENESTARE",
      "rm": "RC",
      "1234": 418080008
    },
    {
      "1": 3795,
      "roma": "BELLOSGUARDO",
      "rm": "SA",
      "1234": 415065015
    },
    {
      "1": 3796,
      "roma": "BERNALDA",
      "rm": "MT",
      "1234": 417077003
    },
    {
      "1": 3797,
      "roma": "BERNATE TICINO",
      "rm": "MI",
      "1234": 403015019
    },
    {
      "1": 3798,
      "roma": "BERNEZZO",
      "rm": "CN",
      "1234": 401004022
    },
    {
      "1": 3799,
      "roma": "BERTINORO",
      "rm": "FO",
      "1234": 408040003
    },
    {
      "1": 3800,
      "roma": "BERTIOLO",
      "rm": "UD",
      "1234": 406030010
    },
    {
      "1": 3801,
      "roma": "BERTONICO",
      "rm": "LO",
      "1234": 403098002
    },
    {
      "1": 3802,
      "roma": "BERZANO DI SAN PIETRO",
      "rm": "AT",
      "1234": 401005009
    },
    {
      "1": 3803,
      "roma": "BERZANO DI TORTONA",
      "rm": "AL",
      "1234": 401006016
    },
    {
      "1": 3804,
      "roma": "BERZO DEMO",
      "rm": "BS",
      "1234": 403017016
    },
    {
      "1": 3805,
      "roma": "BERZO INFERIORE",
      "rm": "BS",
      "1234": 403017017
    },
    {
      "1": 3806,
      "roma": "BERZO SAN FERMO",
      "rm": "BG",
      "1234": 403016025
    },
    {
      "1": 3807,
      "roma": "BERGEGGI",
      "rm": "SV",
      "1234": 407009010
    },
    {
      "1": 3808,
      "roma": "BERGOLO",
      "rm": "CN",
      "1234": 401004021
    },
    {
      "1": 3809,
      "roma": "BESANA IN BRIANZA",
      "rm": "MB",
      "1234": 403108008
    },
    {
      "1": 3810,
      "roma": "BERLINGO",
      "rm": "BS",
      "1234": 403017015
    },
    {
      "1": 3811,
      "roma": "BESOZZO",
      "rm": "VA",
      "1234": 403012013
    },
    {
      "1": 3812,
      "roma": "BESSUDE",
      "rm": "SS",
      "1234": 420090010
    },
    {
      "1": 3813,
      "roma": "BETTOLA",
      "rm": "PC",
      "1234": 408033004
    },
    {
      "1": 3814,
      "roma": "BETTONA",
      "rm": "PG",
      "1234": 410054003
    },
    {
      "1": 3815,
      "roma": "BEURA-CARDEZZA",
      "rm": "VB",
      "1234": 401103011
    },
    {
      "1": 3816,
      "roma": "BEVAGNA",
      "rm": "PG",
      "1234": 410054004
    },
    {
      "1": 3817,
      "roma": "BEVERINO",
      "rm": "SP",
      "1234": 407011003
    },
    {
      "1": 3818,
      "roma": "BEVILACQUA",
      "rm": "VR",
      "1234": 405023008
    },
    {
      "1": 3819,
      "roma": "BESANO",
      "rm": "VA",
      "1234": 403012011
    },
    {
      "1": 3820,
      "roma": "BESATE",
      "rm": "MI",
      "1234": 403015022
    },
    {
      "1": 3821,
      "roma": "BESENELLO",
      "rm": "TN",
      "1234": 404022013
    },
    {
      "1": 3822,
      "roma": "BESENZONE",
      "rm": "PC",
      "1234": 408033003
    },
    {
      "1": 3823,
      "roma": "BIANZE'",
      "rm": "VC",
      "1234": 401002011
    },
    {
      "1": 3824,
      "roma": "BERNAREGGIO",
      "rm": "MB",
      "1234": 403108007
    },
    {
      "1": 3825,
      "roma": "BESNATE",
      "rm": "VA",
      "1234": 403012012
    },
    {
      "1": 3826,
      "roma": "BIASSONO",
      "rm": "MB",
      "1234": 403108009
    },
    {
      "1": 3827,
      "roma": "BIBBIANO",
      "rm": "RE",
      "1234": 408035004
    },
    {
      "1": 3828,
      "roma": "BIBBIENA",
      "rm": "AR",
      "1234": 409051004
    },
    {
      "1": 3829,
      "roma": "BIBBONA",
      "rm": "LI",
      "1234": 409049001
    },
    {
      "1": 3830,
      "roma": "BIBIANA",
      "rm": "TO",
      "1234": 401001025
    },
    {
      "1": 3831,
      "roma": "BICCARI",
      "rm": "FG",
      "1234": 416071006
    },
    {
      "1": 3832,
      "roma": "BICINICCO",
      "rm": "UD",
      "1234": 406030011
    },
    {
      "1": 3833,
      "roma": "BIDONI'",
      "rm": "OR",
      "1234": 420095014
    },
    {
      "1": 3834,
      "roma": "BIELLA",
      "rm": "BI",
      "1234": 401096004
    },
    {
      "1": 3835,
      "roma": "BIENNO",
      "rm": "BS",
      "1234": 403017018
    },
    {
      "1": 3836,
      "roma": "BIANCAVILLA",
      "rm": "CT",
      "1234": 419087008
    },
    {
      "1": 3837,
      "roma": "BIANCHI",
      "rm": "CS",
      "1234": 418078016
    },
    {
      "1": 3838,
      "roma": "BIANCO",
      "rm": "RC",
      "1234": 418080009
    },
    {
      "1": 3839,
      "roma": "BIANDRATE",
      "rm": "NO",
      "1234": 401003018
    },
    {
      "1": 3840,
      "roma": "BIANDRONNO",
      "rm": "VA",
      "1234": 403012014
    },
    {
      "1": 3841,
      "roma": "BIANZANO",
      "rm": "BG",
      "1234": 403016026
    },
    {
      "1": 3842,
      "roma": "BINAGO",
      "rm": "CO",
      "1234": 403013023
    },
    {
      "1": 3843,
      "roma": "BIANZONE",
      "rm": "SO",
      "1234": 403014008
    },
    {
      "1": 3844,
      "roma": "BUGLIO IN MONTE",
      "rm": "SO",
      "1234": 403014010
    },
    {
      "1": 3845,
      "roma": "BUGNARA",
      "rm": "AQ",
      "1234": 413066012
    },
    {
      "1": 3846,
      "roma": "BRUZOLO",
      "rm": "TO",
      "1234": 401001040
    },
    {
      "1": 3847,
      "roma": "BRUZZANO ZEFFIRIO",
      "rm": "RC",
      "1234": 418080015
    },
    {
      "1": 3848,
      "roma": "BUBBIANO",
      "rm": "MI",
      "1234": 403015035
    },
    {
      "1": 3849,
      "roma": "BUBBIO",
      "rm": "AT",
      "1234": 401005011
    },
    {
      "1": 3850,
      "roma": "BUCCHERI",
      "rm": "SR",
      "1234": 419089003
    },
    {
      "1": 3851,
      "roma": "BULGAROGRASSO",
      "rm": "CO",
      "1234": 403013034
    },
    {
      "1": 3852,
      "roma": "BULTEI",
      "rm": "SS",
      "1234": 420090018
    },
    {
      "1": 3853,
      "roma": "BULZI",
      "rm": "SS",
      "1234": 420090019
    },
    {
      "1": 3854,
      "roma": "BUONABITACOLO",
      "rm": "SA",
      "1234": 415065018
    },
    {
      "1": 3855,
      "roma": "BUONALBERGO",
      "rm": "BN",
      "1234": 415062011
    },
    {
      "1": 3856,
      "roma": "BUONCONVENTO",
      "rm": "SI",
      "1234": 409052003
    },
    {
      "1": 3857,
      "roma": "BUONVICINO",
      "rm": "CS",
      "1234": 418078020
    },
    {
      "1": 3858,
      "roma": "BUDONI",
      "rm": "NU",
      "1234": 420091014
    },
    {
      "1": 3859,
      "roma": "BURCEI",
      "rm": "CA",
      "1234": 420092008
    },
    {
      "1": 3860,
      "roma": "BURGIO",
      "rm": "AG",
      "1234": 419084005
    },
    {
      "1": 3861,
      "roma": "BURGOS",
      "rm": "SS",
      "1234": 420090020
    },
    {
      "1": 3862,
      "roma": "BURAGO DI MOLGORA",
      "rm": "MB",
      "1234": 403108013
    },
    {
      "1": 3863,
      "roma": "BURIASCO",
      "rm": "TO",
      "1234": 401001041
    },
    {
      "1": 3864,
      "roma": "BUGUGGIATE",
      "rm": "VA",
      "1234": 403012025
    },
    {
      "1": 3865,
      "roma": "BUJA",
      "rm": "UD",
      "1234": 406030013
    },
    {
      "1": 3866,
      "roma": "BULCIAGO",
      "rm": "LC",
      "1234": 403097011
    },
    {
      "1": 3867,
      "roma": "BUSANO",
      "rm": "TO",
      "1234": 401001043
    },
    {
      "1": 3868,
      "roma": "BUSCA",
      "rm": "CN",
      "1234": 401004034
    },
    {
      "1": 3869,
      "roma": "BUSCATE",
      "rm": "MI",
      "1234": 403015038
    },
    {
      "1": 3870,
      "roma": "BUSCEMI",
      "rm": "SR",
      "1234": 419089004
    },
    {
      "1": 3871,
      "roma": "BUSETO PALIZZOLO",
      "rm": "TP",
      "1234": 419081002
    },
    {
      "1": 3872,
      "roma": "BUSSERO",
      "rm": "MI",
      "1234": 403015040
    },
    {
      "1": 3873,
      "roma": "BUSSETO",
      "rm": "PR",
      "1234": 408034007
    },
    {
      "1": 3874,
      "roma": "BUSSOLENGO",
      "rm": "VR",
      "1234": 405023015
    },
    {
      "1": 3875,
      "roma": "BUSSOLENO",
      "rm": "TO",
      "1234": 401001044
    },
    {
      "1": 3876,
      "roma": "BUSTO ARSIZIO",
      "rm": "VA",
      "1234": 403012026
    },
    {
      "1": 3877,
      "roma": "BUSNAGO",
      "rm": "MB",
      "1234": 403108051
    },
    {
      "1": 3878,
      "roma": "BUROLO",
      "rm": "TO",
      "1234": 401001042
    },
    {
      "1": 3879,
      "roma": "BURONZO",
      "rm": "VC",
      "1234": 401002021
    },
    {
      "1": 3880,
      "roma": "BUSACHI",
      "rm": "OR",
      "1234": 420095017
    },
    {
      "1": 3881,
      "roma": "BUSALLA",
      "rm": "GE",
      "1234": 407010006
    },
    {
      "1": 3882,
      "roma": "BUTTRIO",
      "rm": "UD",
      "1234": 406030014
    },
    {
      "1": 3883,
      "roma": "BUSSI SUL TIRINO",
      "rm": "PE",
      "1234": 413068005
    },
    {
      "1": 3884,
      "roma": "BUSSO",
      "rm": "CB",
      "1234": 414070005
    },
    {
      "1": 3885,
      "roma": "BUSTO GAROLFO",
      "rm": "MI",
      "1234": 403015041
    },
    {
      "1": 3886,
      "roma": "BUTERA",
      "rm": "CL",
      "1234": 419085003
    },
    {
      "1": 3887,
      "roma": "BUTI",
      "rm": "PI",
      "1234": 409050002
    },
    {
      "1": 3888,
      "roma": "BUTTAPIETRA",
      "rm": "VR",
      "1234": 405023016
    },
    {
      "1": 3889,
      "roma": "BUTTIGLIERA ALTA",
      "rm": "TO",
      "1234": 401001045
    },
    {
      "1": 3890,
      "roma": "BUTTIGLIERA D'ASTI",
      "rm": "AT",
      "1234": 401005012
    },
    {
      "1": 3891,
      "roma": "CADEGLIANO-VICONAGO",
      "rm": "VA",
      "1234": 403012027
    },
    {
      "1": 3892,
      "roma": "CADELBOSCO DI SOPRA",
      "rm": "RE",
      "1234": 408035008
    },
    {
      "1": 3893,
      "roma": "CADEO",
      "rm": "PC",
      "1234": 408033007
    },
    {
      "1": 3894,
      "roma": "CADONEGHE",
      "rm": "PD",
      "1234": 405028016
    },
    {
      "1": 3895,
      "roma": "CADORAGO",
      "rm": "CO",
      "1234": 403013036
    },
    {
      "1": 3896,
      "roma": "CAFASSE",
      "rm": "TO",
      "1234": 401001046
    },
    {
      "1": 3897,
      "roma": "CAGGIANO",
      "rm": "SA",
      "1234": 415065019
    },
    {
      "1": 3898,
      "roma": "CAGLI",
      "rm": "PS",
      "1234": 411041007
    },
    {
      "1": 3899,
      "roma": "CAGLIARI",
      "rm": "CA",
      "1234": 420092009
    },
    {
      "1": 3900,
      "roma": "CABELLA LIGURE",
      "rm": "AL",
      "1234": 401006025
    },
    {
      "1": 3901,
      "roma": "CABIATE",
      "rm": "CO",
      "1234": 403013035
    },
    {
      "1": 3902,
      "roma": "CABRAS",
      "rm": "OR",
      "1234": 420095018
    },
    {
      "1": 3903,
      "roma": "CACCAMO",
      "rm": "PA",
      "1234": 419082014
    },
    {
      "1": 3904,
      "roma": "CACCURI",
      "rm": "KR",
      "1234": 418101002
    },
    {
      "1": 3905,
      "roma": "CAGNO'",
      "rm": "TN",
      "1234": 404022030
    },
    {
      "1": 3906,
      "roma": "CAIANELLO",
      "rm": "CE",
      "1234": 415061008
    },
    {
      "1": 3907,
      "roma": "CAIAZZO",
      "rm": "CE",
      "1234": 415061009
    },
    {
      "1": 3908,
      "roma": "CAINES",
      "rm": "BZ",
      "1234": 404021014
    },
    {
      "1": 3909,
      "roma": "CAINO",
      "rm": "BS",
      "1234": 403017031
    },
    {
      "1": 3910,
      "roma": "CAIOLO",
      "rm": "SO",
      "1234": 403014011
    },
    {
      "1": 3911,
      "roma": "CAIRANO",
      "rm": "AV",
      "1234": 415064013
    },
    {
      "1": 3912,
      "roma": "CAIRATE",
      "rm": "VA",
      "1234": 403012029
    },
    {
      "1": 3913,
      "roma": "CAIRO MONTENOTTE",
      "rm": "SV",
      "1234": 407009015
    },
    {
      "1": 3914,
      "roma": "CAIVANO",
      "rm": "NA",
      "1234": 415063011
    },
    {
      "1": 3915,
      "roma": "CAERANO DI SAN MARCO",
      "rm": "TV",
      "1234": 405026006
    },
    {
      "1": 3916,
      "roma": "CALABRITTO",
      "rm": "AV",
      "1234": 415064014
    },
    {
      "1": 3917,
      "roma": "CALALZO DI CADORE",
      "rm": "BL",
      "1234": 405025008
    },
    {
      "1": 3918,
      "roma": "CALAMANDRANA",
      "rm": "AT",
      "1234": 401005013
    },
    {
      "1": 3919,
      "roma": "CAGLIO",
      "rm": "CO",
      "1234": 403013037
    },
    {
      "1": 3920,
      "roma": "CADERZONE TERME",
      "rm": "TN",
      "1234": 404022929
    },
    {
      "1": 3921,
      "roma": "CAGNANO AMITERNO",
      "rm": "AQ",
      "1234": 413066013
    },
    {
      "1": 3922,
      "roma": "CAGNANO VARANO",
      "rm": "FG",
      "1234": 416071008
    },
    {
      "1": 3923,
      "roma": "CALASETTA",
      "rm": "CA",
      "1234": 420092010
    },
    {
      "1": 3924,
      "roma": "CALATABIANO",
      "rm": "CT",
      "1234": 419087010
    },
    {
      "1": 3925,
      "roma": "CALCATA",
      "rm": "VT",
      "1234": 412056010
    },
    {
      "1": 3926,
      "roma": "CALCERANICA AL LAGO",
      "rm": "TN",
      "1234": 404022032
    },
    {
      "1": 3927,
      "roma": "CALCI",
      "rm": "PI",
      "1234": 409050003
    },
    {
      "1": 3928,
      "roma": "CALCIANO",
      "rm": "MT",
      "1234": 417077004
    },
    {
      "1": 3929,
      "roma": "CALCINAIA",
      "rm": "PI",
      "1234": 409050004
    },
    {
      "1": 3930,
      "roma": "CALCINATE",
      "rm": "BG",
      "1234": 403016043
    },
    {
      "1": 3931,
      "roma": "CALCINATO",
      "rm": "BS",
      "1234": 403017032
    },
    {
      "1": 3932,
      "roma": "CALCIO",
      "rm": "BG",
      "1234": 403016044
    },
    {
      "1": 3933,
      "roma": "CALDARO SULLA STRADA DEL VINO",
      "rm": "BZ",
      "1234": 404021015
    },
    {
      "1": 3934,
      "roma": "CALDAROLA",
      "rm": "MC",
      "1234": 411043006
    },
    {
      "1": 3935,
      "roma": "CALDERARA DI RENO",
      "rm": "BO",
      "1234": 408037009
    },
    {
      "1": 3936,
      "roma": "CALDES",
      "rm": "TN",
      "1234": 404022033
    },
    {
      "1": 3937,
      "roma": "CALDIERO",
      "rm": "VR",
      "1234": 405023017
    },
    {
      "1": 3938,
      "roma": "CALAMONACI",
      "rm": "AG",
      "1234": 419084006
    },
    {
      "1": 3939,
      "roma": "CALANGIANUS",
      "rm": "SS",
      "1234": 420090021
    },
    {
      "1": 3940,
      "roma": "CALANNA",
      "rm": "RC",
      "1234": 418080016
    },
    {
      "1": 3941,
      "roma": "CALASCA-CASTIGLIONE",
      "rm": "VB",
      "1234": 401103014
    },
    {
      "1": 3942,
      "roma": "CALASCIBETTA",
      "rm": "EN",
      "1234": 419086005
    },
    {
      "1": 3943,
      "roma": "CALASCIO",
      "rm": "AQ",
      "1234": 413066014
    },
    {
      "1": 3944,
      "roma": "CALICE AL CORNOVIGLIO",
      "rm": "SP",
      "1234": 407011008
    },
    {
      "1": 3945,
      "roma": "CALICE LIGURE",
      "rm": "SV",
      "1234": 407009016
    },
    {
      "1": 3946,
      "roma": "CALIMERA",
      "rm": "LE",
      "1234": 416075010
    },
    {
      "1": 3947,
      "roma": "CALITRI",
      "rm": "AV",
      "1234": 415064015
    },
    {
      "1": 3948,
      "roma": "CALIZZANO",
      "rm": "SV",
      "1234": 407009017
    },
    {
      "1": 3949,
      "roma": "CALLABIANA",
      "rm": "BI",
      "1234": 401096008
    },
    {
      "1": 3950,
      "roma": "CALLIANO",
      "rm": "TN",
      "1234": 404022035
    },
    {
      "1": 3952,
      "roma": "CALCO",
      "rm": "LC",
      "1234": 403097012
    },
    {
      "1": 3953,
      "roma": "CALOLZIOCORTE",
      "rm": "LC",
      "1234": 403097013
    },
    {
      "1": 3954,
      "roma": "CALOPEZZATI",
      "rm": "CS",
      "1234": 418078021
    },
    {
      "1": 3955,
      "roma": "CALOSSO",
      "rm": "AT",
      "1234": 401005015
    },
    {
      "1": 3956,
      "roma": "CALDOGNO",
      "rm": "VI",
      "1234": 405024018
    },
    {
      "1": 3957,
      "roma": "CALDONAZZO",
      "rm": "TN",
      "1234": 404022034
    },
    {
      "1": 3958,
      "roma": "CALENDASCO",
      "rm": "PC",
      "1234": 408033008
    },
    {
      "1": 3959,
      "roma": "CALENZANO",
      "rm": "FI",
      "1234": 409048005
    },
    {
      "1": 3960,
      "roma": "CALATAFIMI SEGESTA",
      "rm": "TP",
      "1234": 419081903
    },
    {
      "1": 3961,
      "roma": "CALESTANO",
      "rm": "PR",
      "1234": 408034008
    },
    {
      "1": 3962,
      "roma": "CALTO",
      "rm": "RO",
      "1234": 405029008
    },
    {
      "1": 3963,
      "roma": "CALTRANO",
      "rm": "VI",
      "1234": 405024019
    },
    {
      "1": 3964,
      "roma": "CALUSCO D'ADDA",
      "rm": "BG",
      "1234": 403016046
    },
    {
      "1": 3965,
      "roma": "CALUSO",
      "rm": "TO",
      "1234": 401001047
    },
    {
      "1": 3966,
      "roma": "CALVAGESE DELLA RIVIERA",
      "rm": "BS",
      "1234": 403017033
    },
    {
      "1": 3967,
      "roma": "CALVANICO",
      "rm": "SA",
      "1234": 415065020
    },
    {
      "1": 3968,
      "roma": "CALVATONE",
      "rm": "CR",
      "1234": 403019009
    },
    {
      "1": 3969,
      "roma": "CALVELLO",
      "rm": "PZ",
      "1234": 417076015
    },
    {
      "1": 3970,
      "roma": "CALVENE",
      "rm": "VI",
      "1234": 405024020
    },
    {
      "1": 3971,
      "roma": "CALVENZANO",
      "rm": "BG",
      "1234": 403016047
    },
    {
      "1": 3972,
      "roma": "CALVERA",
      "rm": "PZ",
      "1234": 417076016
    },
    {
      "1": 3973,
      "roma": "CALVI RISORTA",
      "rm": "CE",
      "1234": 415061010
    },
    {
      "1": 3974,
      "roma": "CALVIGNANO",
      "rm": "PV",
      "1234": 403018025
    },
    {
      "1": 3975,
      "roma": "CALVIGNASCO",
      "rm": "MI",
      "1234": 403015042
    },
    {
      "1": 3976,
      "roma": "CALVISANO",
      "rm": "BS",
      "1234": 403017034
    },
    {
      "1": 3977,
      "roma": "CALOVETO",
      "rm": "CS",
      "1234": 418078022
    },
    {
      "1": 3978,
      "roma": "CALTABELLOTTA",
      "rm": "AG",
      "1234": 419084007
    },
    {
      "1": 3979,
      "roma": "CALTAGIRONE",
      "rm": "CT",
      "1234": 419087011
    },
    {
      "1": 3980,
      "roma": "CALTANISSETTA",
      "rm": "CL",
      "1234": 419085004
    },
    {
      "1": 3981,
      "roma": "CALTAVUTURO",
      "rm": "PA",
      "1234": 419082015
    },
    {
      "1": 3982,
      "roma": "CALTIGNAGA",
      "rm": "NO",
      "1234": 401003030
    },
    {
      "1": 3983,
      "roma": "CAMANDONA",
      "rm": "BI",
      "1234": 401096009
    },
    {
      "1": 3984,
      "roma": "CAMASTRA",
      "rm": "AG",
      "1234": 419084008
    },
    {
      "1": 3985,
      "roma": "CAMBIAGO",
      "rm": "MI",
      "1234": 403015044
    },
    {
      "1": 3986,
      "roma": "CAMBIANO",
      "rm": "TO",
      "1234": 401001048
    },
    {
      "1": 3987,
      "roma": "CAMBIASCA",
      "rm": "VB",
      "1234": 401103015
    },
    {
      "1": 3988,
      "roma": "CAMBURZANO",
      "rm": "BI",
      "1234": 401096010
    },
    {
      "1": 3989,
      "roma": "CALVI",
      "rm": "BN",
      "1234": 415062012
    },
    {
      "1": 3990,
      "roma": "CALVI DELL'UMBRIA",
      "rm": "TR",
      "1234": 410055008
    },
    {
      "1": 3991,
      "roma": "CAMERANO",
      "rm": "AN",
      "1234": 411042006
    },
    {
      "1": 3992,
      "roma": "CAMERANO CASASCO",
      "rm": "AT",
      "1234": 401005016
    },
    {
      "1": 3993,
      "roma": "CAMERATA CORNELLO",
      "rm": "BG",
      "1234": 403016048
    },
    {
      "1": 3994,
      "roma": "CAMERATA NUOVA",
      "rm": "RM",
      "1234": 412058014
    },
    {
      "1": 3995,
      "roma": "CAMERATA PICENA",
      "rm": "AN",
      "1234": 411042007
    },
    {
      "1": 3996,
      "roma": "CALVIZZANO",
      "rm": "NA",
      "1234": 415063012
    },
    {
      "1": 3997,
      "roma": "CAMAGNA MONFERRATO",
      "rm": "AL",
      "1234": 401006026
    },
    {
      "1": 3998,
      "roma": "CAMAIORE",
      "rm": "LU",
      "1234": 409046005
    },
    {
      "1": 3999,
      "roma": "CAMINI",
      "rm": "RC",
      "1234": 418080017
    },
    {
      "1": 4000,
      "roma": "CAMINO",
      "rm": "AL",
      "1234": 401006027
    },
    {
      "1": 4001,
      "roma": "CAMINO AL TAGLIAMENTO",
      "rm": "UD",
      "1234": 406030015
    },
    {
      "1": 4002,
      "roma": "CAMISANO",
      "rm": "CR",
      "1234": 403019010
    },
    {
      "1": 4003,
      "roma": "CAMISANO VICENTINO",
      "rm": "VI",
      "1234": 405024021
    },
    {
      "1": 4004,
      "roma": "CAMMARATA",
      "rm": "AG",
      "1234": 419084009
    },
    {
      "1": 4005,
      "roma": "CAMERANA",
      "rm": "CN",
      "1234": 401004035
    },
    {
      "1": 4006,
      "roma": "CAMPAGNA LUPIA",
      "rm": "VE",
      "1234": 405027002
    },
    {
      "1": 4007,
      "roma": "CAMPAGNANO DI ROMA",
      "rm": "RM",
      "1234": 412058015
    },
    {
      "1": 4008,
      "roma": "CAMPAGNATICO",
      "rm": "GR",
      "1234": 409053002
    },
    {
      "1": 4009,
      "roma": "CAMPAGNOLA CREMASCA",
      "rm": "CR",
      "1234": 403019011
    },
    {
      "1": 4010,
      "roma": "CAMERI",
      "rm": "NO",
      "1234": 401003032
    },
    {
      "1": 4011,
      "roma": "CAMERINO",
      "rm": "MC",
      "1234": 411043007
    },
    {
      "1": 4012,
      "roma": "CAMEROTA",
      "rm": "SA",
      "1234": 415065021
    },
    {
      "1": 4013,
      "roma": "CAMIGLIANO",
      "rm": "CE",
      "1234": 415061011
    },
    {
      "1": 4014,
      "roma": "CAMPELLO SUL CLITUNNO",
      "rm": "PG",
      "1234": 410054005
    },
    {
      "1": 4015,
      "roma": "CAMPERTOGNO",
      "rm": "VC",
      "1234": 401002025
    },
    {
      "1": 4016,
      "roma": "CAMPI BISENZIO",
      "rm": "FI",
      "1234": 409048006
    },
    {
      "1": 4017,
      "roma": "CAMPI SALENTINA",
      "rm": "LE",
      "1234": 416075011
    },
    {
      "1": 4018,
      "roma": "CAMPARADA",
      "rm": "MB",
      "1234": 403108014
    },
    {
      "1": 4019,
      "roma": "CAMPIGLIA DEI BERICI",
      "rm": "VI",
      "1234": 405024022
    },
    {
      "1": 4020,
      "roma": "CAMPIGLIA MARITTIMA",
      "rm": "LI",
      "1234": 409049002
    },
    {
      "1": 4021,
      "roma": "CAMPIGLIONE FENILE",
      "rm": "TO",
      "1234": 401001049
    },
    {
      "1": 4022,
      "roma": "CAMPIONE D'ITALIA",
      "rm": "CO",
      "1234": 403013040
    },
    {
      "1": 4023,
      "roma": "CAMPITELLO DI FASSA",
      "rm": "TN",
      "1234": 404022036
    },
    {
      "1": 4024,
      "roma": "CAMOGLI",
      "rm": "GE",
      "1234": 407010007
    },
    {
      "1": 4025,
      "roma": "CAMPAGNA",
      "rm": "SA",
      "1234": 415065022
    },
    {
      "1": 4026,
      "roma": "CAMPO CALABRO",
      "rm": "RC",
      "1234": 418080018
    },
    {
      "1": 4027,
      "roma": "CAMPO DI GIOVE",
      "rm": "AQ",
      "1234": 413066015
    },
    {
      "1": 4028,
      "roma": "CAMPO DI TRENS",
      "rm": "BZ",
      "1234": 404021016
    },
    {
      "1": 4029,
      "roma": "CAMPO LIGURE",
      "rm": "GE",
      "1234": 407010008
    },
    {
      "1": 4030,
      "roma": "CAMPAGNOLA EMILIA",
      "rm": "RE",
      "1234": 408035009
    },
    {
      "1": 4031,
      "roma": "CAMPANA",
      "rm": "CS",
      "1234": 418078023
    },
    {
      "1": 4032,
      "roma": "CAMPEGINE",
      "rm": "RE",
      "1234": 408035010
    },
    {
      "1": 4033,
      "roma": "CAMPOBELLO DI LICATA",
      "rm": "AG",
      "1234": 419084010
    },
    {
      "1": 4034,
      "roma": "CAMPOBELLO DI MAZARA",
      "rm": "TP",
      "1234": 419081004
    },
    {
      "1": 4035,
      "roma": "CAMPOCHIARO",
      "rm": "CB",
      "1234": 414070007
    },
    {
      "1": 4036,
      "roma": "CAMPODARSEGO",
      "rm": "PD",
      "1234": 405028017
    },
    {
      "1": 4037,
      "roma": "CAMPODENNO",
      "rm": "TN",
      "1234": 404022037
    },
    {
      "1": 4038,
      "roma": "CAMPODIMELE",
      "rm": "LT",
      "1234": 412059003
    },
    {
      "1": 4039,
      "roma": "CAMPIGLIA CERVO",
      "rm": "BI",
      "1234": 401096086
    },
    {
      "1": 4040,
      "roma": "CAMPODIPIETRA",
      "rm": "CB",
      "1234": 414070008
    },
    {
      "1": 4041,
      "roma": "CAMPODOLCINO",
      "rm": "SO",
      "1234": 403014012
    },
    {
      "1": 4042,
      "roma": "CAMPODORO",
      "rm": "PD",
      "1234": 405028018
    },
    {
      "1": 4043,
      "roma": "CAMPOFELICE DI FITALIA",
      "rm": "PA",
      "1234": 419082016
    },
    {
      "1": 4044,
      "roma": "CAMPOFELICE DI ROCCELLA",
      "rm": "PA",
      "1234": 419082017
    },
    {
      "1": 4045,
      "roma": "CAMPLI",
      "rm": "TE",
      "1234": 413067008
    },
    {
      "1": 4046,
      "roma": "CAMPOFORMIDO",
      "rm": "UD",
      "1234": 406030016
    },
    {
      "1": 4047,
      "roma": "CAMPOFRANCO",
      "rm": "CL",
      "1234": 419085005
    },
    {
      "1": 4048,
      "roma": "CAMPOGALLIANO",
      "rm": "MO",
      "1234": 408036003
    },
    {
      "1": 4049,
      "roma": "CAMPOLATTARO",
      "rm": "BN",
      "1234": 415062013
    },
    {
      "1": 4050,
      "roma": "CAMPO NELL'ELBA",
      "rm": "LI",
      "1234": 409049003
    },
    {
      "1": 4051,
      "roma": "CAMPO SAN MARTINO",
      "rm": "PD",
      "1234": 405028020
    },
    {
      "1": 4052,
      "roma": "CAMPO TURES",
      "rm": "BZ",
      "1234": 404021017
    },
    {
      "1": 4053,
      "roma": "CAMPOBASSO",
      "rm": "CB",
      "1234": 414070006
    },
    {
      "1": 4054,
      "roma": "CAMPOMAGGIORE",
      "rm": "PZ",
      "1234": 417076017
    },
    {
      "1": 4055,
      "roma": "CAMPOMARINO",
      "rm": "CB",
      "1234": 414070010
    },
    {
      "1": 4056,
      "roma": "CAMPOMORONE",
      "rm": "GE",
      "1234": 407010009
    },
    {
      "1": 4057,
      "roma": "CAMPONOGARA",
      "rm": "VE",
      "1234": 405027004
    },
    {
      "1": 4058,
      "roma": "CAMPORA",
      "rm": "SA",
      "1234": 415065023
    },
    {
      "1": 4059,
      "roma": "CAMPOREALE",
      "rm": "PA",
      "1234": 419082019
    },
    {
      "1": 4060,
      "roma": "CAMPORGIANO",
      "rm": "LU",
      "1234": 409046006
    },
    {
      "1": 4061,
      "roma": "CAMPOROSSO",
      "rm": "IM",
      "1234": 407008011
    },
    {
      "1": 4062,
      "roma": "CAMPOFILONE",
      "rm": "FM",
      "1234": 411109004
    },
    {
      "1": 4063,
      "roma": "CAMPOROTONDO DI FIASTRONE",
      "rm": "MC",
      "1234": 411043008
    },
    {
      "1": 4064,
      "roma": "CAMPOROTONDO ETNEO",
      "rm": "CT",
      "1234": 419087012
    },
    {
      "1": 4065,
      "roma": "CAMPOFIORITO",
      "rm": "PA",
      "1234": 419082018
    },
    {
      "1": 4066,
      "roma": "CAMPOSANTO",
      "rm": "MO",
      "1234": 408036004
    },
    {
      "1": 4067,
      "roma": "CAMPOSPINOSO",
      "rm": "PV",
      "1234": 403018026
    },
    {
      "1": 4068,
      "roma": "CAMPOLI APPENNINO",
      "rm": "FR",
      "1234": 412060016
    },
    {
      "1": 4069,
      "roma": "CAMPOLI DEL MONTE TABURNO",
      "rm": "BN",
      "1234": 415062014
    },
    {
      "1": 4070,
      "roma": "CAMPOLIETO",
      "rm": "CB",
      "1234": 414070009
    },
    {
      "1": 4071,
      "roma": "CAMPOLONGO MAGGIORE",
      "rm": "VE",
      "1234": 405027003
    },
    {
      "1": 4072,
      "roma": "CANALE D'AGORDO",
      "rm": "BL",
      "1234": 405025023
    },
    {
      "1": 4073,
      "roma": "CANALE MONTERANO",
      "rm": "RM",
      "1234": 412058016
    },
    {
      "1": 4074,
      "roma": "CANAL SAN BOVO",
      "rm": "TN",
      "1234": 404022038
    },
    {
      "1": 4075,
      "roma": "CANARO",
      "rm": "RO",
      "1234": 405029009
    },
    {
      "1": 4076,
      "roma": "CANAZEI",
      "rm": "TN",
      "1234": 404022039
    },
    {
      "1": 4077,
      "roma": "CANCELLARA",
      "rm": "PZ",
      "1234": 417076018
    },
    {
      "1": 4078,
      "roma": "CANCELLO ED ARNONE",
      "rm": "CE",
      "1234": 415061012
    },
    {
      "1": 4079,
      "roma": "CANDA",
      "rm": "RO",
      "1234": 405029010
    },
    {
      "1": 4080,
      "roma": "CANDELA",
      "rm": "FG",
      "1234": 416071009
    },
    {
      "1": 4081,
      "roma": "CANDELO",
      "rm": "BI",
      "1234": 401096012
    },
    {
      "1": 4082,
      "roma": "CAMPOSAMPIERO",
      "rm": "PD",
      "1234": 405028019
    },
    {
      "1": 4083,
      "roma": "CAMPOSANO",
      "rm": "NA",
      "1234": 415063013
    },
    {
      "1": 4084,
      "roma": "CANDIANA",
      "rm": "PD",
      "1234": 405028021
    },
    {
      "1": 4085,
      "roma": "CANDIDA",
      "rm": "AV",
      "1234": 415064016
    },
    {
      "1": 4086,
      "roma": "CAMPOTOSTO",
      "rm": "AQ",
      "1234": 413066016
    },
    {
      "1": 4087,
      "roma": "CAMUGNANO",
      "rm": "BO",
      "1234": 408037010
    },
    {
      "1": 4088,
      "roma": "CANALE",
      "rm": "CN",
      "1234": 401004037
    },
    {
      "1": 4089,
      "roma": "CANICATTI'",
      "rm": "AG",
      "1234": 419084011
    },
    {
      "1": 4090,
      "roma": "CANICATTINI BAGNI",
      "rm": "SR",
      "1234": 419089005
    },
    {
      "1": 4091,
      "roma": "CANINO",
      "rm": "VT",
      "1234": 412056012
    },
    {
      "1": 4092,
      "roma": "CANISCHIO",
      "rm": "TO",
      "1234": 401001052
    },
    {
      "1": 4093,
      "roma": "CANISTRO",
      "rm": "AQ",
      "1234": 413066017
    },
    {
      "1": 4094,
      "roma": "CANNA",
      "rm": "CS",
      "1234": 418078024
    },
    {
      "1": 4095,
      "roma": "CANNALONGA",
      "rm": "SA",
      "1234": 415065024
    },
    {
      "1": 4096,
      "roma": "CANNARA",
      "rm": "PG",
      "1234": 410054006
    },
    {
      "1": 4097,
      "roma": "CANNERO RIVIERA",
      "rm": "VB",
      "1234": 401103016
    },
    {
      "1": 4098,
      "roma": "CANDIA CANAVESE",
      "rm": "TO",
      "1234": 401001050
    },
    {
      "1": 4099,
      "roma": "CANDIA LOMELLINA",
      "rm": "PV",
      "1234": 403018027
    },
    {
      "1": 4100,
      "roma": "CANDIDONI",
      "rm": "RC",
      "1234": 418080019
    },
    {
      "1": 4101,
      "roma": "CANDIOLO",
      "rm": "TO",
      "1234": 401001051
    },
    {
      "1": 4102,
      "roma": "CANEGRATE",
      "rm": "MI",
      "1234": 403015046
    },
    {
      "1": 4103,
      "roma": "CANELLI",
      "rm": "AT",
      "1234": 401005017
    },
    {
      "1": 4104,
      "roma": "CANEPINA",
      "rm": "VT",
      "1234": 412056011
    },
    {
      "1": 4105,
      "roma": "CANEVA",
      "rm": "PN",
      "1234": 406093009
    },
    {
      "1": 4106,
      "roma": "CANOSIO",
      "rm": "CN",
      "1234": 401004038
    },
    {
      "1": 4107,
      "roma": "CANOSSA",
      "rm": "RE",
      "1234": 408035502
    },
    {
      "1": 4108,
      "roma": "CANSANO",
      "rm": "AQ",
      "1234": 413066018
    },
    {
      "1": 4109,
      "roma": "CANTAGALLO",
      "rm": "PO",
      "1234": 409100001
    },
    {
      "1": 4110,
      "roma": "CANTALICE",
      "rm": "RI",
      "1234": 412057009
    },
    {
      "1": 4111,
      "roma": "CANTALUPA",
      "rm": "TO",
      "1234": 401001053
    },
    {
      "1": 4112,
      "roma": "CANTALUPO IN SABINA",
      "rm": "RI",
      "1234": 412057010
    },
    {
      "1": 4113,
      "roma": "CANTALUPO LIGURE",
      "rm": "AL",
      "1234": 401006028
    },
    {
      "1": 4114,
      "roma": "CANTALUPO NEL SANNIO",
      "rm": "IS",
      "1234": 414094005
    },
    {
      "1": 4115,
      "roma": "CANTARANA",
      "rm": "AT",
      "1234": 401005018
    },
    {
      "1": 4116,
      "roma": "CANNETO PAVESE",
      "rm": "PV",
      "1234": 403018029
    },
    {
      "1": 4117,
      "roma": "CANNETO SULL'OGLIO",
      "rm": "MN",
      "1234": 403020008
    },
    {
      "1": 4118,
      "roma": "CANTERANO",
      "rm": "RM",
      "1234": 412058017
    },
    {
      "1": 4119,
      "roma": "CANTIANO",
      "rm": "PS",
      "1234": 411041008
    },
    {
      "1": 4120,
      "roma": "CANNOBIO",
      "rm": "VB",
      "1234": 401103017
    },
    {
      "1": 4121,
      "roma": "CANNOLE",
      "rm": "LE",
      "1234": 416075012
    },
    {
      "1": 4122,
      "roma": "CANOLO",
      "rm": "RC",
      "1234": 418080020
    },
    {
      "1": 4123,
      "roma": "CANONICA D'ADDA",
      "rm": "BG",
      "1234": 403016049
    },
    {
      "1": 4124,
      "roma": "CANOSA SANNITA",
      "rm": "CH",
      "1234": 413069010
    },
    {
      "1": 4125,
      "roma": "CAPACCIO",
      "rm": "SA",
      "1234": 415065025
    },
    {
      "1": 4126,
      "roma": "CAPACI",
      "rm": "PA",
      "1234": 419082020
    },
    {
      "1": 4127,
      "roma": "CAPALBIO",
      "rm": "GR",
      "1234": 409053003
    },
    {
      "1": 4128,
      "roma": "CAPANNOLI",
      "rm": "PI",
      "1234": 409050005
    },
    {
      "1": 4129,
      "roma": "CAPANNORI",
      "rm": "LU",
      "1234": 409046007
    },
    {
      "1": 4130,
      "roma": "CAPENA",
      "rm": "RM",
      "1234": 412058018
    },
    {
      "1": 4131,
      "roma": "CAPERGNANICA",
      "rm": "CR",
      "1234": 403019012
    },
    {
      "1": 4132,
      "roma": "CAPESTRANO",
      "rm": "AQ",
      "1234": 413066019
    },
    {
      "1": 4133,
      "roma": "CAPIAGO INTIMIANO",
      "rm": "CO",
      "1234": 403013043
    },
    {
      "1": 4134,
      "roma": "CAPISTRANO",
      "rm": "VV",
      "1234": 418102005
    },
    {
      "1": 4135,
      "roma": "CAPISTRELLO",
      "rm": "AQ",
      "1234": 413066020
    },
    {
      "1": 4136,
      "roma": "CAPITIGNANO",
      "rm": "AQ",
      "1234": 413066021
    },
    {
      "1": 4137,
      "roma": "CANOSA DI PUGLIA",
      "rm": "BT",
      "1234": 416110004
    },
    {
      "1": 4138,
      "roma": "CANTELLO",
      "rm": "VA",
      "1234": 403012030
    },
    {
      "1": 4139,
      "roma": "CAPIZZONE",
      "rm": "BG",
      "1234": 403016050
    },
    {
      "1": 4140,
      "roma": "CANTOIRA",
      "rm": "TO",
      "1234": 401001054
    },
    {
      "1": 4141,
      "roma": "CANTU'",
      "rm": "CO",
      "1234": 403013041
    },
    {
      "1": 4142,
      "roma": "CANZANO",
      "rm": "TE",
      "1234": 413067009
    },
    {
      "1": 4143,
      "roma": "CANZO",
      "rm": "CO",
      "1234": 403013042
    },
    {
      "1": 4144,
      "roma": "CAORLE",
      "rm": "VE",
      "1234": 405027005
    },
    {
      "1": 4145,
      "roma": "CAORSO",
      "rm": "PC",
      "1234": 408033010
    },
    {
      "1": 4146,
      "roma": "CAPOLIVERI",
      "rm": "LI",
      "1234": 409049004
    },
    {
      "1": 4147,
      "roma": "CAPOLONA",
      "rm": "AR",
      "1234": 409051006
    },
    {
      "1": 4148,
      "roma": "CAPORCIANO",
      "rm": "AQ",
      "1234": 413066022
    },
    {
      "1": 4149,
      "roma": "CAPOSELE",
      "rm": "AV",
      "1234": 415064017
    },
    {
      "1": 4150,
      "roma": "CAPOTERRA",
      "rm": "CA",
      "1234": 420092011
    },
    {
      "1": 4151,
      "roma": "CAPOVALLE",
      "rm": "BS",
      "1234": 403017036
    },
    {
      "1": 4152,
      "roma": "CAPPADOCIA",
      "rm": "AQ",
      "1234": 413066023
    },
    {
      "1": 4153,
      "roma": "CAPPELLA CANTONE",
      "rm": "CR",
      "1234": 403019013
    },
    {
      "1": 4154,
      "roma": "CAPPELLA DE' PICENARDI",
      "rm": "CR",
      "1234": 403019014
    },
    {
      "1": 4155,
      "roma": "CAPPELLA MAGGIORE",
      "rm": "TV",
      "1234": 405026007
    },
    {
      "1": 4156,
      "roma": "CAPPELLE SUL TAVO",
      "rm": "PE",
      "1234": 413068006
    },
    {
      "1": 4157,
      "roma": "CAPRACOTTA",
      "rm": "IS",
      "1234": 414094006
    },
    {
      "1": 4158,
      "roma": "CAPIZZI",
      "rm": "ME",
      "1234": 419083008
    },
    {
      "1": 4159,
      "roma": "CAPRAIA ISOLA",
      "rm": "LI",
      "1234": 409049005
    },
    {
      "1": 4160,
      "roma": "CAPO D'ORLANDO",
      "rm": "ME",
      "1234": 419083009
    },
    {
      "1": 4161,
      "roma": "CAPO DI PONTE",
      "rm": "BS",
      "1234": 403017035
    },
    {
      "1": 4162,
      "roma": "CAPODIMONTE",
      "rm": "VT",
      "1234": 412056013
    },
    {
      "1": 4163,
      "roma": "CAPODRISE",
      "rm": "CE",
      "1234": 415061013
    },
    {
      "1": 4164,
      "roma": "CAPRAUNA",
      "rm": "CN",
      "1234": 401004039
    },
    {
      "1": 4165,
      "roma": "CAPRESE MICHELANGELO",
      "rm": "AR",
      "1234": 409051007
    },
    {
      "1": 4166,
      "roma": "CAPREZZO",
      "rm": "VB",
      "1234": 401103018
    },
    {
      "1": 4167,
      "roma": "CAPRI",
      "rm": "NA",
      "1234": 415063014
    },
    {
      "1": 4168,
      "roma": "CAPRI LEONE",
      "rm": "ME",
      "1234": 419083010
    },
    {
      "1": 4169,
      "roma": "CAPRIANA",
      "rm": "TN",
      "1234": 404022040
    },
    {
      "1": 4170,
      "roma": "CAPRIANO DEL COLLE",
      "rm": "BS",
      "1234": 403017037
    },
    {
      "1": 4171,
      "roma": "CAPRIATA D'ORBA",
      "rm": "AL",
      "1234": 401006029
    },
    {
      "1": 4172,
      "roma": "CAPRIATE SAN GERVASIO",
      "rm": "BG",
      "1234": 403016051
    },
    {
      "1": 4173,
      "roma": "CAPRIATI A VOLTURNO",
      "rm": "CE",
      "1234": 415061014
    },
    {
      "1": 4174,
      "roma": "CAPRIE",
      "rm": "TO",
      "1234": 401001055
    },
    {
      "1": 4175,
      "roma": "CAPRIGLIA IRPINA",
      "rm": "AV",
      "1234": 415064018
    },
    {
      "1": 4176,
      "roma": "CAPRIGLIO",
      "rm": "AT",
      "1234": 401005019
    },
    {
      "1": 4177,
      "roma": "CAPRAIA E LIMITE",
      "rm": "FI",
      "1234": 409048008
    },
    {
      "1": 4178,
      "roma": "CAPONAGO",
      "rm": "MB",
      "1234": 403108052
    },
    {
      "1": 4179,
      "roma": "CAPRALBA",
      "rm": "CR",
      "1234": 403019015
    },
    {
      "1": 4180,
      "roma": "CAPRANICA",
      "rm": "VT",
      "1234": 412056014
    },
    {
      "1": 4181,
      "roma": "CAPRANICA PRENESTINA",
      "rm": "RM",
      "1234": 412058019
    },
    {
      "1": 4182,
      "roma": "CAPRARICA DI LECCE",
      "rm": "LE",
      "1234": 416075013
    },
    {
      "1": 4183,
      "roma": "CAPRAROLA",
      "rm": "VT",
      "1234": 412056015
    },
    {
      "1": 4184,
      "roma": "CAPURSO",
      "rm": "BA",
      "1234": 416072014
    },
    {
      "1": 4185,
      "roma": "CARAFFA DEL BIANCO",
      "rm": "RC",
      "1234": 418080021
    },
    {
      "1": 4186,
      "roma": "CARAFFA DI CATANZARO",
      "rm": "CZ",
      "1234": 418079017
    },
    {
      "1": 4187,
      "roma": "CARAGLIO",
      "rm": "CN",
      "1234": 401004040
    },
    {
      "1": 4188,
      "roma": "CARAMAGNA PIEMONTE",
      "rm": "CN",
      "1234": 401004041
    },
    {
      "1": 4189,
      "roma": "CARAMANICO TERME",
      "rm": "PE",
      "1234": 413068007
    },
    {
      "1": 4190,
      "roma": "CARANO",
      "rm": "TN",
      "1234": 404022041
    },
    {
      "1": 4191,
      "roma": "CARAPELLE",
      "rm": "FG",
      "1234": 416071010
    },
    {
      "1": 4192,
      "roma": "CARAPELLE CALVISIO",
      "rm": "AQ",
      "1234": 413066024
    },
    {
      "1": 4193,
      "roma": "CARASCO",
      "rm": "GE",
      "1234": 407010010
    },
    {
      "1": 4194,
      "roma": "CARASSAI",
      "rm": "AP",
      "1234": 411044010
    },
    {
      "1": 4195,
      "roma": "CARATE URIO",
      "rm": "CO",
      "1234": 403013044
    },
    {
      "1": 4196,
      "roma": "CARAVAGGIO",
      "rm": "BG",
      "1234": 403016053
    },
    {
      "1": 4197,
      "roma": "CARAVATE",
      "rm": "VA",
      "1234": 403012031
    },
    {
      "1": 4198,
      "roma": "CARAVINO",
      "rm": "TO",
      "1234": 401001056
    },
    {
      "1": 4199,
      "roma": "CAPRILE",
      "rm": "BI",
      "1234": 401096013
    },
    {
      "1": 4200,
      "roma": "CAPRINO VERONESE",
      "rm": "VR",
      "1234": 405023018
    },
    {
      "1": 4201,
      "roma": "CAPRIOLO",
      "rm": "BS",
      "1234": 403017038
    },
    {
      "1": 4202,
      "roma": "CAPRIVA DEL FRIULI",
      "rm": "GO",
      "1234": 406031001
    },
    {
      "1": 4203,
      "roma": "CAPUA",
      "rm": "CE",
      "1234": 415061015
    },
    {
      "1": 4204,
      "roma": "CARBONARA SCRIVIA",
      "rm": "AL",
      "1234": 401006030
    },
    {
      "1": 4205,
      "roma": "CARBONATE",
      "rm": "CO",
      "1234": 403013045
    },
    {
      "1": 4206,
      "roma": "CARBONE",
      "rm": "PZ",
      "1234": 417076019
    },
    {
      "1": 4207,
      "roma": "CARBONERA",
      "rm": "TV",
      "1234": 405026008
    },
    {
      "1": 4208,
      "roma": "CARBONIA",
      "rm": "CA",
      "1234": 420092012
    },
    {
      "1": 4209,
      "roma": "CARCARE",
      "rm": "SV",
      "1234": 407009018
    },
    {
      "1": 4210,
      "roma": "CARCERI",
      "rm": "PD",
      "1234": 405028022
    },
    {
      "1": 4211,
      "roma": "CARCOFORO",
      "rm": "VC",
      "1234": 401002029
    },
    {
      "1": 4212,
      "roma": "CARDANO AL CAMPO",
      "rm": "VA",
      "1234": 403012032
    },
    {
      "1": 4213,
      "roma": "CARDE'",
      "rm": "CN",
      "1234": 401004042
    },
    {
      "1": 4214,
      "roma": "CARDEDU",
      "rm": "NU",
      "1234": 420091103
    },
    {
      "1": 4215,
      "roma": "CARDETO",
      "rm": "RC",
      "1234": 418080022
    },
    {
      "1": 4216,
      "roma": "CARAVONICA",
      "rm": "IM",
      "1234": 407008012
    },
    {
      "1": 4217,
      "roma": "CAPRINO BERGAMASCO",
      "rm": "BG",
      "1234": 403016052
    },
    {
      "1": 4218,
      "roma": "CARBONARA AL TICINO",
      "rm": "PV",
      "1234": 403018030
    },
    {
      "1": 4219,
      "roma": "CARBONARA DI NOLA",
      "rm": "NA",
      "1234": 415063015
    },
    {
      "1": 4220,
      "roma": "CARATE BRIANZA",
      "rm": "MB",
      "1234": 403108015
    },
    {
      "1": 4221,
      "roma": "CARENTINO",
      "rm": "AL",
      "1234": 401006031
    },
    {
      "1": 4222,
      "roma": "CARERI",
      "rm": "RC",
      "1234": 418080023
    },
    {
      "1": 4223,
      "roma": "CARESANA",
      "rm": "VC",
      "1234": 401002030
    },
    {
      "1": 4224,
      "roma": "CARESANABLOT",
      "rm": "VC",
      "1234": 401002031
    },
    {
      "1": 4225,
      "roma": "CAREZZANO",
      "rm": "AL",
      "1234": 401006032
    },
    {
      "1": 4226,
      "roma": "CARFIZZI",
      "rm": "KR",
      "1234": 418101003
    },
    {
      "1": 4227,
      "roma": "CARGEGHE",
      "rm": "SS",
      "1234": 420090022
    },
    {
      "1": 4228,
      "roma": "CARIATI",
      "rm": "CS",
      "1234": 418078025
    },
    {
      "1": 4229,
      "roma": "CARIFE",
      "rm": "AV",
      "1234": 415064019
    },
    {
      "1": 4230,
      "roma": "CARIGNANO",
      "rm": "TO",
      "1234": 401001058
    },
    {
      "1": 4231,
      "roma": "CARIMATE",
      "rm": "CO",
      "1234": 403013046
    },
    {
      "1": 4232,
      "roma": "CARINARO",
      "rm": "CE",
      "1234": 415061016
    },
    {
      "1": 4233,
      "roma": "CARINI",
      "rm": "PA",
      "1234": 419082021
    },
    {
      "1": 4234,
      "roma": "CARINOLA",
      "rm": "CE",
      "1234": 415061017
    },
    {
      "1": 4235,
      "roma": "CARDINALE",
      "rm": "CZ",
      "1234": 418079018
    },
    {
      "1": 4236,
      "roma": "CARBOGNANO",
      "rm": "VT",
      "1234": 412056016
    },
    {
      "1": 4237,
      "roma": "CAREMA",
      "rm": "TO",
      "1234": 401001057
    },
    {
      "1": 4238,
      "roma": "CARENNO",
      "rm": "LC",
      "1234": 403097014
    },
    {
      "1": 4239,
      "roma": "CARLOFORTE",
      "rm": "CA",
      "1234": 420092013
    },
    {
      "1": 4240,
      "roma": "CARLOPOLI",
      "rm": "CZ",
      "1234": 418079020
    },
    {
      "1": 4241,
      "roma": "CARMAGNOLA",
      "rm": "TO",
      "1234": 401001059
    },
    {
      "1": 4242,
      "roma": "CARMIANO",
      "rm": "LE",
      "1234": 416075014
    },
    {
      "1": 4243,
      "roma": "CARMIGNANO",
      "rm": "PO",
      "1234": 409100002
    },
    {
      "1": 4244,
      "roma": "CARMIGNANO DI BRENTA",
      "rm": "PD",
      "1234": 405028023
    },
    {
      "1": 4245,
      "roma": "CARNAGO",
      "rm": "VA",
      "1234": 403012033
    },
    {
      "1": 4246,
      "roma": "CAROBBIO DEGLI ANGELI",
      "rm": "BG",
      "1234": 403016055
    },
    {
      "1": 4247,
      "roma": "CAROLEI",
      "rm": "CS",
      "1234": 418078026
    },
    {
      "1": 4248,
      "roma": "CARONA",
      "rm": "BG",
      "1234": 403016056
    },
    {
      "1": 4249,
      "roma": "CARONIA",
      "rm": "ME",
      "1234": 419083011
    },
    {
      "1": 4250,
      "roma": "CARONNO PERTUSELLA",
      "rm": "VA",
      "1234": 403012034
    },
    {
      "1": 4251,
      "roma": "CARONNO VARESINO",
      "rm": "VA",
      "1234": 403012035
    },
    {
      "1": 4252,
      "roma": "CARDITO",
      "rm": "NA",
      "1234": 415063016
    },
    {
      "1": 4253,
      "roma": "CAREGGINE",
      "rm": "LU",
      "1234": 409046008
    },
    {
      "1": 4254,
      "roma": "CARLANTINO",
      "rm": "FG",
      "1234": 416071011
    },
    {
      "1": 4255,
      "roma": "CARLAZZO",
      "rm": "CO",
      "1234": 403013047
    },
    {
      "1": 4256,
      "roma": "CARLENTINI",
      "rm": "SR",
      "1234": 419089006
    },
    {
      "1": 4257,
      "roma": "CARLINO",
      "rm": "UD",
      "1234": 406030018
    },
    {
      "1": 4258,
      "roma": "CARPANZANO",
      "rm": "CS",
      "1234": 418078027
    },
    {
      "1": 4259,
      "roma": "CARNATE",
      "rm": "MB",
      "1234": 403108016
    },
    {
      "1": 4260,
      "roma": "CARPEGNA",
      "rm": "PS",
      "1234": 411041009
    },
    {
      "1": 4261,
      "roma": "CARPENEDOLO",
      "rm": "BS",
      "1234": 403017039
    },
    {
      "1": 4262,
      "roma": "CARPENETO",
      "rm": "AL",
      "1234": 401006033
    },
    {
      "1": 4263,
      "roma": "CARPI",
      "rm": "MO",
      "1234": 408036005
    },
    {
      "1": 4264,
      "roma": "CARPIANO",
      "rm": "MI",
      "1234": 403015050
    },
    {
      "1": 4265,
      "roma": "CARPIGNANO SALENTINO",
      "rm": "LE",
      "1234": 416075015
    },
    {
      "1": 4266,
      "roma": "CARPIGNANO SESIA",
      "rm": "NO",
      "1234": 401003036
    },
    {
      "1": 4267,
      "roma": "CARPINETI",
      "rm": "RE",
      "1234": 408035011
    },
    {
      "1": 4268,
      "roma": "CARPINETO DELLA NORA",
      "rm": "PE",
      "1234": 413068008
    },
    {
      "1": 4269,
      "roma": "CARPINETO ROMANO",
      "rm": "RM",
      "1234": 412058020
    },
    {
      "1": 4270,
      "roma": "CARPINETO SINELLO",
      "rm": "CH",
      "1234": 413069011
    },
    {
      "1": 4271,
      "roma": "CARPINO",
      "rm": "FG",
      "1234": 416071012
    },
    {
      "1": 4272,
      "roma": "CARPINONE",
      "rm": "IS",
      "1234": 414094008
    },
    {
      "1": 4273,
      "roma": "CARISIO",
      "rm": "VC",
      "1234": 401002032
    },
    {
      "1": 4274,
      "roma": "CARISOLO",
      "rm": "TN",
      "1234": 404022042
    },
    {
      "1": 4275,
      "roma": "CAROVILLI",
      "rm": "IS",
      "1234": 414094007
    },
    {
      "1": 4276,
      "roma": "CARPANETO PIACENTINO",
      "rm": "PC",
      "1234": 408033011
    },
    {
      "1": 4277,
      "roma": "CARRO",
      "rm": "SP",
      "1234": 407011009
    },
    {
      "1": 4278,
      "roma": "CARRODANO",
      "rm": "SP",
      "1234": 407011010
    },
    {
      "1": 4279,
      "roma": "CARROSIO",
      "rm": "AL",
      "1234": 401006035
    },
    {
      "1": 4280,
      "roma": "CARRU'",
      "rm": "CN",
      "1234": 401004043
    },
    {
      "1": 4281,
      "roma": "CARSOLI",
      "rm": "AQ",
      "1234": 413066025
    },
    {
      "1": 4282,
      "roma": "CARTIGLIANO",
      "rm": "VI",
      "1234": 405024025
    },
    {
      "1": 4283,
      "roma": "CARTIGNANO",
      "rm": "CN",
      "1234": 401004044
    },
    {
      "1": 4284,
      "roma": "CARTOCETO",
      "rm": "PS",
      "1234": 411041010
    },
    {
      "1": 4285,
      "roma": "CARTOSIO",
      "rm": "AL",
      "1234": 401006036
    },
    {
      "1": 4286,
      "roma": "CARTURA",
      "rm": "PD",
      "1234": 405028026
    },
    {
      "1": 4287,
      "roma": "CARUGATE",
      "rm": "MI",
      "1234": 403015051
    },
    {
      "1": 4288,
      "roma": "CARUGO",
      "rm": "CO",
      "1234": 403013048
    },
    {
      "1": 4289,
      "roma": "CARUNCHIO",
      "rm": "CH",
      "1234": 413069012
    },
    {
      "1": 4290,
      "roma": "CARVICO",
      "rm": "BG",
      "1234": 403016057
    },
    {
      "1": 4291,
      "roma": "CARZANO",
      "rm": "TN",
      "1234": 404022043
    },
    {
      "1": 4292,
      "roma": "CAROSINO",
      "rm": "TA",
      "1234": 416073002
    },
    {
      "1": 4293,
      "roma": "CAROVIGNO",
      "rm": "BR",
      "1234": 416074002
    },
    {
      "1": 4294,
      "roma": "CARRE'",
      "rm": "VI",
      "1234": 405024024
    },
    {
      "1": 4295,
      "roma": "CARREGA LIGURE",
      "rm": "AL",
      "1234": 401006034
    },
    {
      "1": 4296,
      "roma": "CASAL VELINO",
      "rm": "SA",
      "1234": 415065028
    },
    {
      "1": 4297,
      "roma": "CASALANGUIDA",
      "rm": "CH",
      "1234": 413069014
    },
    {
      "1": 4298,
      "roma": "CASALATTICO",
      "rm": "FR",
      "1234": 412060017
    },
    {
      "1": 4299,
      "roma": "CASALBELTRAME",
      "rm": "NO",
      "1234": 401003037
    },
    {
      "1": 4300,
      "roma": "CASALBORDINO",
      "rm": "CH",
      "1234": 413069015
    },
    {
      "1": 4301,
      "roma": "CASALBORE",
      "rm": "AV",
      "1234": 415064020
    },
    {
      "1": 4302,
      "roma": "CASALBORGONE",
      "rm": "TO",
      "1234": 401001060
    },
    {
      "1": 4303,
      "roma": "CASALBUONO",
      "rm": "SA",
      "1234": 415065026
    },
    {
      "1": 4304,
      "roma": "CASALBUTTANO ED UNITI",
      "rm": "CR",
      "1234": 403019016
    },
    {
      "1": 4305,
      "roma": "CASALCIPRANO",
      "rm": "CB",
      "1234": 414070012
    },
    {
      "1": 4306,
      "roma": "CASALDUNI",
      "rm": "BN",
      "1234": 415062015
    },
    {
      "1": 4307,
      "roma": "CASALE CORTE CERRO",
      "rm": "VB",
      "1234": 401103019
    },
    {
      "1": 4308,
      "roma": "CASALE CREMASCO-VIDOLASCO",
      "rm": "CR",
      "1234": 403019017
    },
    {
      "1": 4309,
      "roma": "CASALE DI SCODOSIA",
      "rm": "PD",
      "1234": 405028027
    },
    {
      "1": 4310,
      "roma": "CASALE LITTA",
      "rm": "VA",
      "1234": 403012036
    },
    {
      "1": 4311,
      "roma": "CASALE MARITTIMO",
      "rm": "PI",
      "1234": 409050006
    },
    {
      "1": 4312,
      "roma": "CASABONA",
      "rm": "KR",
      "1234": 418101004
    },
    {
      "1": 4313,
      "roma": "CARRARA",
      "rm": "MS",
      "1234": 409045003
    },
    {
      "1": 4314,
      "roma": "CASACANDITELLA",
      "rm": "CH",
      "1234": 413069013
    },
    {
      "1": 4315,
      "roma": "CASAGIOVE",
      "rm": "CE",
      "1234": 415061018
    },
    {
      "1": 4316,
      "roma": "CASAL CERMELLI",
      "rm": "AL",
      "1234": 401006037
    },
    {
      "1": 4317,
      "roma": "CASAL DI PRINCIPE",
      "rm": "CE",
      "1234": 415061019
    },
    {
      "1": 4318,
      "roma": "CASALETTO CEREDANO",
      "rm": "CR",
      "1234": 403019018
    },
    {
      "1": 4319,
      "roma": "CASALETTO DI SOPRA",
      "rm": "CR",
      "1234": 403019019
    },
    {
      "1": 4320,
      "roma": "CASALETTO LODIGIANO",
      "rm": "LO",
      "1234": 403098008
    },
    {
      "1": 4321,
      "roma": "CASALETTO SPARTANO",
      "rm": "SA",
      "1234": 415065027
    },
    {
      "1": 4322,
      "roma": "CASALETTO VAPRIO",
      "rm": "CR",
      "1234": 403019020
    },
    {
      "1": 4323,
      "roma": "CASALFIUMANESE",
      "rm": "BO",
      "1234": 408037012
    },
    {
      "1": 4324,
      "roma": "CASALGRANDE",
      "rm": "RE",
      "1234": 408035012
    },
    {
      "1": 4325,
      "roma": "CASALGRASSO",
      "rm": "CN",
      "1234": 401004045
    },
    {
      "1": 4326,
      "roma": "CASALINCONTRADA",
      "rm": "CH",
      "1234": 413069016
    },
    {
      "1": 4327,
      "roma": "CASALINO",
      "rm": "NO",
      "1234": 401003040
    },
    {
      "1": 4328,
      "roma": "CASALMAGGIORE",
      "rm": "CR",
      "1234": 403019021
    },
    {
      "1": 4329,
      "roma": "CASALMAIOCCO",
      "rm": "LO",
      "1234": 403098009
    },
    {
      "1": 4330,
      "roma": "CASALMORANO",
      "rm": "CR",
      "1234": 403019022
    },
    {
      "1": 4331,
      "roma": "CASALMORO",
      "rm": "MN",
      "1234": 403020010
    },
    {
      "1": 4332,
      "roma": "CASALNOCETO",
      "rm": "AL",
      "1234": 401006040
    },
    {
      "1": 4333,
      "roma": "CASALNUOVO DI NAPOLI",
      "rm": "NA",
      "1234": 415063017
    },
    {
      "1": 4334,
      "roma": "CASALNUOVO MONTEROTARO",
      "rm": "FG",
      "1234": 416071013
    },
    {
      "1": 4335,
      "roma": "CASALE MONFERRATO",
      "rm": "AL",
      "1234": 401006039
    },
    {
      "1": 4336,
      "roma": "CASACALENDA",
      "rm": "CB",
      "1234": 414070011
    },
    {
      "1": 4337,
      "roma": "CASALEGGIO BOIRO",
      "rm": "AL",
      "1234": 401006038
    },
    {
      "1": 4338,
      "roma": "CASALEGGIO NOVARA",
      "rm": "NO",
      "1234": 401003039
    },
    {
      "1": 4339,
      "roma": "CASALEONE",
      "rm": "VR",
      "1234": 405023019
    },
    {
      "1": 4340,
      "roma": "CASALUCE",
      "rm": "CE",
      "1234": 415061020
    },
    {
      "1": 4341,
      "roma": "CASALVECCHIO DI PUGLIA",
      "rm": "FG",
      "1234": 416071014
    },
    {
      "1": 4342,
      "roma": "CASALVECCHIO SICULO",
      "rm": "ME",
      "1234": 419083012
    },
    {
      "1": 4343,
      "roma": "CASALVIERI",
      "rm": "FR",
      "1234": 412060018
    },
    {
      "1": 4344,
      "roma": "CASALVOLONE",
      "rm": "NO",
      "1234": 401003041
    },
    {
      "1": 4345,
      "roma": "CASALZUIGNO",
      "rm": "VA",
      "1234": 403012037
    },
    {
      "1": 4346,
      "roma": "CASAMARCIANO",
      "rm": "NA",
      "1234": 415063018
    },
    {
      "1": 4347,
      "roma": "CASAMASSIMA",
      "rm": "BA",
      "1234": 416072015
    },
    {
      "1": 4348,
      "roma": "CASAMICCIOLA TERME",
      "rm": "NA",
      "1234": 415063019
    },
    {
      "1": 4349,
      "roma": "CASANDRINO",
      "rm": "NA",
      "1234": 415063020
    },
    {
      "1": 4350,
      "roma": "CASANOVA ELVO",
      "rm": "VC",
      "1234": 401002033
    },
    {
      "1": 4351,
      "roma": "CASANOVA LERRONE",
      "rm": "SV",
      "1234": 407009019
    },
    {
      "1": 4352,
      "roma": "CASANOVA LONATI",
      "rm": "PV",
      "1234": 403018031
    },
    {
      "1": 4353,
      "roma": "CASAPE",
      "rm": "RM",
      "1234": 412058021
    },
    {
      "1": 4354,
      "roma": "CASAPESENNA",
      "rm": "CE",
      "1234": 415061103
    },
    {
      "1": 4355,
      "roma": "CASALOLDO",
      "rm": "MN",
      "1234": 403020011
    },
    {
      "1": 4356,
      "roma": "CASALE SUL SILE",
      "rm": "TV",
      "1234": 405026009
    },
    {
      "1": 4357,
      "roma": "CASALECCHIO DI RENO",
      "rm": "BO",
      "1234": 408037011
    },
    {
      "1": 4358,
      "roma": "CASALPUSTERLENGO",
      "rm": "LO",
      "1234": 403098010
    },
    {
      "1": 4359,
      "roma": "CASALROMANO",
      "rm": "MN",
      "1234": 403020012
    },
    {
      "1": 4360,
      "roma": "CASALSERUGO",
      "rm": "PD",
      "1234": 405028028
    },
    {
      "1": 4361,
      "roma": "CASARILE",
      "rm": "MI",
      "1234": 403015055
    },
    {
      "1": 4362,
      "roma": "CASARSA DELLA DELIZIA",
      "rm": "PN",
      "1234": 406093010
    },
    {
      "1": 4363,
      "roma": "CASARZA LIGURE",
      "rm": "GE",
      "1234": 407010011
    },
    {
      "1": 4364,
      "roma": "CASASCO",
      "rm": "AL",
      "1234": 401006041
    },
    {
      "1": 4365,
      "roma": "CASATENOVO",
      "rm": "LC",
      "1234": 403097016
    },
    {
      "1": 4366,
      "roma": "CASATISMA",
      "rm": "PV",
      "1234": 403018032
    },
    {
      "1": 4367,
      "roma": "CASAVATORE",
      "rm": "NA",
      "1234": 415063021
    },
    {
      "1": 4368,
      "roma": "CASAZZA",
      "rm": "BG",
      "1234": 403016058
    },
    {
      "1": 4369,
      "roma": "CASCIA",
      "rm": "PG",
      "1234": 410054007
    },
    {
      "1": 4370,
      "roma": "CASCIAGO",
      "rm": "VA",
      "1234": 403012038
    },
    {
      "1": 4371,
      "roma": "CASCINA",
      "rm": "PI",
      "1234": 409050008
    },
    {
      "1": 4372,
      "roma": "CASAPINTA",
      "rm": "BI",
      "1234": 401096014
    },
    {
      "1": 4373,
      "roma": "CASAPULLA",
      "rm": "CE",
      "1234": 415061021
    },
    {
      "1": 4374,
      "roma": "CASARANO",
      "rm": "LE",
      "1234": 416075016
    },
    {
      "1": 4375,
      "roma": "CASARGO",
      "rm": "LC",
      "1234": 403097015
    },
    {
      "1": 4376,
      "roma": "CASELLE LURANI",
      "rm": "LO",
      "1234": 403098012
    },
    {
      "1": 4377,
      "roma": "CASELLE TORINESE",
      "rm": "TO",
      "1234": 401001063
    },
    {
      "1": 4378,
      "roma": "CASERTA",
      "rm": "CE",
      "1234": 415061022
    },
    {
      "1": 4379,
      "roma": "CASIER",
      "rm": "TV",
      "1234": 405026010
    },
    {
      "1": 4380,
      "roma": "CASIGNANA",
      "rm": "RC",
      "1234": 418080024
    },
    {
      "1": 4381,
      "roma": "CASINA",
      "rm": "RE",
      "1234": 408035013
    },
    {
      "1": 4382,
      "roma": "CASCIANA TERME LARI",
      "rm": "PI",
      "1234": 409050040
    },
    {
      "1": 4383,
      "roma": "CASIRATE D'ADDA",
      "rm": "BG",
      "1234": 403016059
    },
    {
      "1": 4384,
      "roma": "CASLINO D'ERBA",
      "rm": "CO",
      "1234": 403013052
    },
    {
      "1": 4385,
      "roma": "CASNATE CON BERNATE",
      "rm": "CO",
      "1234": 403013053
    },
    {
      "1": 4386,
      "roma": "CASCINETTE D'IVREA",
      "rm": "TO",
      "1234": 401001061
    },
    {
      "1": 4387,
      "roma": "CASAPROTA",
      "rm": "RI",
      "1234": 412057011
    },
    {
      "1": 4388,
      "roma": "CASELLA",
      "rm": "GE",
      "1234": 407010012
    },
    {
      "1": 4389,
      "roma": "CASELLE IN PITTARI",
      "rm": "SA",
      "1234": 415065029
    },
    {
      "1": 4390,
      "roma": "CASELLE LANDI",
      "rm": "LO",
      "1234": 403098011
    },
    {
      "1": 4391,
      "roma": "CASOLI",
      "rm": "CH",
      "1234": 413069017
    },
    {
      "1": 4392,
      "roma": "CASORATE PRIMO",
      "rm": "PV",
      "1234": 403018034
    },
    {
      "1": 4393,
      "roma": "CASORATE SEMPIONE",
      "rm": "VA",
      "1234": 403012039
    },
    {
      "1": 4394,
      "roma": "CASOREZZO",
      "rm": "MI",
      "1234": 403015058
    },
    {
      "1": 4395,
      "roma": "CASORIA",
      "rm": "NA",
      "1234": 415063023
    },
    {
      "1": 4396,
      "roma": "CASORZO",
      "rm": "AT",
      "1234": 401005020
    },
    {
      "1": 4397,
      "roma": "CASPERIA",
      "rm": "RI",
      "1234": 412057012
    },
    {
      "1": 4398,
      "roma": "CASPOGGIO",
      "rm": "SO",
      "1234": 403014013
    },
    {
      "1": 4399,
      "roma": "CASSACCO",
      "rm": "UD",
      "1234": 406030019
    },
    {
      "1": 4400,
      "roma": "CASSAGO BRIANZA",
      "rm": "LC",
      "1234": 403097017
    },
    {
      "1": 4401,
      "roma": "CASSANO ALL'IONIO",
      "rm": "CS",
      "1234": 418078029
    },
    {
      "1": 4402,
      "roma": "CASSANO D'ADDA",
      "rm": "MI",
      "1234": 403015059
    },
    {
      "1": 4403,
      "roma": "CASSANO DELLE MURGE",
      "rm": "BA",
      "1234": 416072016
    },
    {
      "1": 4404,
      "roma": "CASSANO IRPINO",
      "rm": "AV",
      "1234": 415064021
    },
    {
      "1": 4405,
      "roma": "CASNIGO",
      "rm": "BG",
      "1234": 403016060
    },
    {
      "1": 4406,
      "roma": "CASEI GEROLA",
      "rm": "PV",
      "1234": 403018033
    },
    {
      "1": 4407,
      "roma": "CASELETTE",
      "rm": "TO",
      "1234": 401001062
    },
    {
      "1": 4408,
      "roma": "CASOLA VALSENIO",
      "rm": "RA",
      "1234": 408039005
    },
    {
      "1": 4409,
      "roma": "CASOLE D'ELSA",
      "rm": "SI",
      "1234": 409052004
    },
    {
      "1": 4410,
      "roma": "CASSINA DE' PECCHI",
      "rm": "MI",
      "1234": 403015060
    },
    {
      "1": 4411,
      "roma": "CASSINA RIZZARDI",
      "rm": "CO",
      "1234": 403013055
    },
    {
      "1": 4412,
      "roma": "CASSINA VALSASSINA",
      "rm": "LC",
      "1234": 403097018
    },
    {
      "1": 4413,
      "roma": "CASSINASCO",
      "rm": "AT",
      "1234": 401005021
    },
    {
      "1": 4414,
      "roma": "CASSINE",
      "rm": "AL",
      "1234": 401006043
    },
    {
      "1": 4415,
      "roma": "CASSANO MAGNAGO",
      "rm": "VA",
      "1234": 403012040
    },
    {
      "1": 4416,
      "roma": "CASOLA DI NAPOLI",
      "rm": "NA",
      "1234": 415063022
    },
    {
      "1": 4417,
      "roma": "CASOLA IN LUNIGIANA",
      "rm": "MS",
      "1234": 409045004
    },
    {
      "1": 4418,
      "roma": "CASSARO",
      "rm": "SR",
      "1234": 419089007
    },
    {
      "1": 4419,
      "roma": "CASSIGLIO",
      "rm": "BG",
      "1234": 403016061
    },
    {
      "1": 4420,
      "roma": "CASSOLNOVO",
      "rm": "PV",
      "1234": 403018035
    },
    {
      "1": 4421,
      "roma": "CASTAGNARO",
      "rm": "VR",
      "1234": 405023020
    },
    {
      "1": 4422,
      "roma": "CASTAGNETO CARDUCCI",
      "rm": "LI",
      "1234": 409049006
    },
    {
      "1": 4423,
      "roma": "CASTAGNETO PO",
      "rm": "TO",
      "1234": 401001064
    },
    {
      "1": 4424,
      "roma": "CASTAGNITO",
      "rm": "CN",
      "1234": 401004046
    },
    {
      "1": 4425,
      "roma": "CASTAGNOLE DELLE LANZE",
      "rm": "AT",
      "1234": 401005022
    },
    {
      "1": 4426,
      "roma": "CASTAGNOLE MONFERRATO",
      "rm": "AT",
      "1234": 401005023
    },
    {
      "1": 4427,
      "roma": "CASTAGNOLE PIEMONTE",
      "rm": "TO",
      "1234": 401001065
    },
    {
      "1": 4428,
      "roma": "CASTANA",
      "rm": "PV",
      "1234": 403018036
    },
    {
      "1": 4429,
      "roma": "CASTANO PRIMO",
      "rm": "MI",
      "1234": 403015062
    },
    {
      "1": 4430,
      "roma": "CASTEGGIO",
      "rm": "PV",
      "1234": 403018037
    },
    {
      "1": 4431,
      "roma": "CASTEGNATO",
      "rm": "BS",
      "1234": 403017040
    },
    {
      "1": 4432,
      "roma": "CASTEGNERO",
      "rm": "VI",
      "1234": 405024027
    },
    {
      "1": 4433,
      "roma": "CASTEL BARONIA",
      "rm": "AV",
      "1234": 415064022
    },
    {
      "1": 4434,
      "roma": "CASTEL BOGLIONE",
      "rm": "AT",
      "1234": 401005024
    },
    {
      "1": 4435,
      "roma": "CASSANO VALCUVIA",
      "rm": "VA",
      "1234": 403012041
    },
    {
      "1": 4436,
      "roma": "CASSINO",
      "rm": "FR",
      "1234": 412060019
    },
    {
      "1": 4437,
      "roma": "CASSOLA",
      "rm": "VI",
      "1234": 405024026
    },
    {
      "1": 4438,
      "roma": "CASTEL D'AZZANO",
      "rm": "VR",
      "1234": 405023021
    },
    {
      "1": 4439,
      "roma": "CASTEL DEL GIUDICE",
      "rm": "IS",
      "1234": 414094009
    },
    {
      "1": 4440,
      "roma": "CASTEL DEL MONTE",
      "rm": "AQ",
      "1234": 413066026
    },
    {
      "1": 4441,
      "roma": "CASTEL DEL PIANO",
      "rm": "GR",
      "1234": 409053004
    },
    {
      "1": 4442,
      "roma": "CASTEL DEL RIO",
      "rm": "BO",
      "1234": 408037014
    },
    {
      "1": 4443,
      "roma": "CASTEL DI CASIO",
      "rm": "BO",
      "1234": 408037015
    },
    {
      "1": 4444,
      "roma": "CASTEL DI IERI",
      "rm": "AQ",
      "1234": 413066027
    },
    {
      "1": 4445,
      "roma": "CASTEL DI IUDICA",
      "rm": "CT",
      "1234": 419087013
    },
    {
      "1": 4446,
      "roma": "CASTEL DI LAMA",
      "rm": "AP",
      "1234": 411044011
    },
    {
      "1": 4447,
      "roma": "CASTEL DI LUCIO",
      "rm": "ME",
      "1234": 419083013
    },
    {
      "1": 4448,
      "roma": "CASTEL DI SANGRO",
      "rm": "AQ",
      "1234": 413066028
    },
    {
      "1": 4449,
      "roma": "CASTEL DI SASSO",
      "rm": "CE",
      "1234": 415061024
    },
    {
      "1": 4450,
      "roma": "CASTEL DI TORA",
      "rm": "RI",
      "1234": 412057013
    },
    {
      "1": 4451,
      "roma": "CASTEL BOLOGNESE",
      "rm": "RA",
      "1234": 408039006
    },
    {
      "1": 4452,
      "roma": "CASSINELLE",
      "rm": "AL",
      "1234": 401006044
    },
    {
      "1": 4453,
      "roma": "CASSINETTA DI LUGAGNANO",
      "rm": "MI",
      "1234": 403015061
    },
    {
      "1": 4454,
      "roma": "CASTEL D'AIANO",
      "rm": "BO",
      "1234": 408037013
    },
    {
      "1": 4455,
      "roma": "CASTEL D'ARIO",
      "rm": "MN",
      "1234": 403020014
    },
    {
      "1": 4456,
      "roma": "CASTEL GUELFO DI BOLOGNA",
      "rm": "BO",
      "1234": 408037016
    },
    {
      "1": 4457,
      "roma": "CASTEL MADAMA",
      "rm": "RM",
      "1234": 412058023
    },
    {
      "1": 4458,
      "roma": "CASTEL MAGGIORE",
      "rm": "BO",
      "1234": 408037019
    },
    {
      "1": 4459,
      "roma": "CASTEL MELLA",
      "rm": "BS",
      "1234": 403017042
    },
    {
      "1": 4460,
      "roma": "CASTEL MORRONE",
      "rm": "CE",
      "1234": 415061026
    },
    {
      "1": 4461,
      "roma": "CASTELPAGANO",
      "rm": "BN",
      "1234": 415062017
    },
    {
      "1": 4462,
      "roma": "CASTEL RITALDI",
      "rm": "PG",
      "1234": 410054008
    },
    {
      "1": 4463,
      "roma": "CASTEL ROCCHERO",
      "rm": "AT",
      "1234": 401005032
    },
    {
      "1": 4464,
      "roma": "CASTEL ROZZONE",
      "rm": "BG",
      "1234": 403016063
    },
    {
      "1": 4465,
      "roma": "CASTEL SAN GIORGIO",
      "rm": "SA",
      "1234": 415065034
    },
    {
      "1": 4466,
      "roma": "CASTEL SAN GIOVANNI",
      "rm": "PC",
      "1234": 408033013
    },
    {
      "1": 4467,
      "roma": "CASTEL IVANO",
      "rm": "TN",
      "1234": 404022240
    },
    {
      "1": 4468,
      "roma": "CASTEL SAN LORENZO",
      "rm": "SA",
      "1234": 415065035
    },
    {
      "1": 4469,
      "roma": "CASTEL FOCOGNANO",
      "rm": "AR",
      "1234": 409051008
    },
    {
      "1": 4470,
      "roma": "CASTEL CAMPAGNANO",
      "rm": "CE",
      "1234": 415061023
    },
    {
      "1": 4471,
      "roma": "CASTEL CASTAGNA",
      "rm": "TE",
      "1234": 413067010
    },
    {
      "1": 4472,
      "roma": "CASTEL GANDOLFO",
      "rm": "RM",
      "1234": 412058022
    },
    {
      "1": 4473,
      "roma": "CASTEL GIORGIO",
      "rm": "TR",
      "1234": 410055009
    },
    {
      "1": 4474,
      "roma": "CASTEL GOFFREDO",
      "rm": "MN",
      "1234": 403020015
    },
    {
      "1": 4475,
      "roma": "CASTEL SANT'ELIA",
      "rm": "VT",
      "1234": 412056017
    },
    {
      "1": 4476,
      "roma": "CASTELVETERE SUL CALORE",
      "rm": "AV",
      "1234": 415064024
    },
    {
      "1": 4477,
      "roma": "CASTEL VISCARDO",
      "rm": "TR",
      "1234": 410055010
    },
    {
      "1": 4478,
      "roma": "CASTEL VITTORIO",
      "rm": "IM",
      "1234": 407008015
    },
    {
      "1": 4479,
      "roma": "CASTEL VOLTURNO",
      "rm": "CE",
      "1234": 415061027
    },
    {
      "1": 4480,
      "roma": "CASTELBALDO",
      "rm": "PD",
      "1234": 405028029
    },
    {
      "1": 4481,
      "roma": "CASTELBELFORTE",
      "rm": "MN",
      "1234": 403020013
    },
    {
      "1": 4482,
      "roma": "CASTELBELLINO",
      "rm": "AN",
      "1234": 411042008
    },
    {
      "1": 4483,
      "roma": "CASTELBELLO CIARDES",
      "rm": "BZ",
      "1234": 404021018
    },
    {
      "1": 4484,
      "roma": "CASTELBIANCO",
      "rm": "SV",
      "1234": 407009020
    },
    {
      "1": 4485,
      "roma": "CASTELBOTTACCIO",
      "rm": "CB",
      "1234": 414070013
    },
    {
      "1": 4486,
      "roma": "CASTELBUONO",
      "rm": "PA",
      "1234": 419082022
    },
    {
      "1": 4487,
      "roma": "CASTELCIVITA",
      "rm": "SA",
      "1234": 415065030
    },
    {
      "1": 4488,
      "roma": "CASTELCOVATI",
      "rm": "BS",
      "1234": 403017041
    },
    {
      "1": 4489,
      "roma": "CASTELCUCCO",
      "rm": "TV",
      "1234": 405026011
    },
    {
      "1": 4490,
      "roma": "CASTELDACCIA",
      "rm": "PA",
      "1234": 419082023
    },
    {
      "1": 4491,
      "roma": "CASTEL SAN NICCOLO'",
      "rm": "AR",
      "1234": 409051010
    },
    {
      "1": 4492,
      "roma": "CASTEL FRENTANO",
      "rm": "CH",
      "1234": 413069018
    },
    {
      "1": 4493,
      "roma": "CASTEL GABBIANO",
      "rm": "CR",
      "1234": 403019024
    },
    {
      "1": 4494,
      "roma": "CASTEL SAN VINCENZO",
      "rm": "IS",
      "1234": 414094012
    },
    {
      "1": 4495,
      "roma": "CASTEL SANT'ANGELO",
      "rm": "RI",
      "1234": 412057015
    },
    {
      "1": 4496,
      "roma": "CASTELFONDO",
      "rm": "TN",
      "1234": 404022046
    },
    {
      "1": 4497,
      "roma": "CASTELFORTE",
      "rm": "LT",
      "1234": 412059004
    },
    {
      "1": 4498,
      "roma": "CASTELFRANCI",
      "rm": "AV",
      "1234": 415064023
    },
    {
      "1": 4499,
      "roma": "CASTELFRANCO DI SOTTO",
      "rm": "PI",
      "1234": 409050009
    },
    {
      "1": 4500,
      "roma": "CASTELFRANCO EMILIA",
      "rm": "MO",
      "1234": 408036006
    },
    {
      "1": 4501,
      "roma": "CASTELFRANCO IN MISCANO",
      "rm": "BN",
      "1234": 415062016
    },
    {
      "1": 4502,
      "roma": "CASTELFRANCO VENETO",
      "rm": "TV",
      "1234": 405026012
    },
    {
      "1": 4503,
      "roma": "CASTELGOMBERTO",
      "rm": "VI",
      "1234": 405024028
    },
    {
      "1": 4504,
      "roma": "CASTELGRANDE",
      "rm": "PZ",
      "1234": 417076021
    },
    {
      "1": 4505,
      "roma": "CASTELGUGLIELMO",
      "rm": "RO",
      "1234": 405029011
    },
    {
      "1": 4506,
      "roma": "CASTELGUIDONE",
      "rm": "CH",
      "1234": 413069019
    },
    {
      "1": 4507,
      "roma": "CASTELL'ALFERO",
      "rm": "AT",
      "1234": 401005025
    },
    {
      "1": 4508,
      "roma": "CASTELL'ARQUATO",
      "rm": "PC",
      "1234": 408033012
    },
    {
      "1": 4509,
      "roma": "CASTELL'AZZARA",
      "rm": "GR",
      "1234": 409053005
    },
    {
      "1": 4510,
      "roma": "CASTELL'UMBERTO",
      "rm": "ME",
      "1234": 419083014
    },
    {
      "1": 4511,
      "roma": "CASTELLABATE",
      "rm": "SA",
      "1234": 415065031
    },
    {
      "1": 4512,
      "roma": "CASTELLAFIUME",
      "rm": "AQ",
      "1234": 413066029
    },
    {
      "1": 4513,
      "roma": "CASTELDELCI",
      "rm": "RN",
      "1234": 408099021
    },
    {
      "1": 4514,
      "roma": "CASTELDELFINO",
      "rm": "CN",
      "1234": 401004047
    },
    {
      "1": 4515,
      "roma": "CASTEL SAN PIETRO ROMANO",
      "rm": "RM",
      "1234": 412058025
    },
    {
      "1": 4516,
      "roma": "CASTEL SAN PIETRO TERME",
      "rm": "BO",
      "1234": 408037020
    },
    {
      "1": 4517,
      "roma": "CASTELFIDARDO",
      "rm": "AN",
      "1234": 411042010
    },
    {
      "1": 4518,
      "roma": "CASTELFIORENTINO",
      "rm": "FI",
      "1234": 409048010
    },
    {
      "1": 4519,
      "roma": "CASTELLANA SICULA",
      "rm": "PA",
      "1234": 419082024
    },
    {
      "1": 4520,
      "roma": "CASTELLANETA",
      "rm": "TA",
      "1234": 416073003
    },
    {
      "1": 4521,
      "roma": "CASTELLANIA",
      "rm": "AL",
      "1234": 401006045
    },
    {
      "1": 4522,
      "roma": "CASTELLANZA",
      "rm": "VA",
      "1234": 403012042
    },
    {
      "1": 4523,
      "roma": "CASTELLAR GUIDOBONO",
      "rm": "AL",
      "1234": 401006046
    },
    {
      "1": 4524,
      "roma": "CASTELLARANO",
      "rm": "RE",
      "1234": 408035014
    },
    {
      "1": 4525,
      "roma": "CASTELLARO",
      "rm": "IM",
      "1234": 407008014
    },
    {
      "1": 4526,
      "roma": "CASTELLAZZO BORMIDA",
      "rm": "AL",
      "1234": 401006047
    },
    {
      "1": 4527,
      "roma": "CASTELLAZZO NOVARESE",
      "rm": "NO",
      "1234": 401003042
    },
    {
      "1": 4528,
      "roma": "CASTELLEONE",
      "rm": "CR",
      "1234": 403019025
    },
    {
      "1": 4529,
      "roma": "CASTELLEONE DI SUASA",
      "rm": "AN",
      "1234": 411042011
    },
    {
      "1": 4530,
      "roma": "CASTELLERO",
      "rm": "AT",
      "1234": 401005026
    },
    {
      "1": 4531,
      "roma": "CASTELLETTO CERVO",
      "rm": "BI",
      "1234": 401096015
    },
    {
      "1": 4532,
      "roma": "CASTELLALTO",
      "rm": "TE",
      "1234": 413067011
    },
    {
      "1": 4533,
      "roma": "CASTELLAMMARE DEL GOLFO",
      "rm": "TP",
      "1234": 419081005
    },
    {
      "1": 4534,
      "roma": "CASTELDIDONE",
      "rm": "CR",
      "1234": 403019023
    },
    {
      "1": 4535,
      "roma": "CASTELLAMONTE",
      "rm": "TO",
      "1234": 401001066
    },
    {
      "1": 4536,
      "roma": "CASTELLANA GROTTE",
      "rm": "BA",
      "1234": 416072017
    },
    {
      "1": 4537,
      "roma": "CASTELLETTO MONFERRATO",
      "rm": "AL",
      "1234": 401006051
    },
    {
      "1": 4538,
      "roma": "CASTELLETTO STURA",
      "rm": "CN",
      "1234": 401004049
    },
    {
      "1": 4539,
      "roma": "CASTELLETTO SOPRA TICINO",
      "rm": "NO",
      "1234": 401003043
    },
    {
      "1": 4540,
      "roma": "CASTELLETTO UZZONE",
      "rm": "CN",
      "1234": 401004050
    },
    {
      "1": 4541,
      "roma": "CASTELLI",
      "rm": "TE",
      "1234": 413067012
    },
    {
      "1": 4542,
      "roma": "CASTELLI CALEPIO",
      "rm": "BG",
      "1234": 403016062
    },
    {
      "1": 4543,
      "roma": "CASTELLINA IN CHIANTI",
      "rm": "SI",
      "1234": 409052005
    },
    {
      "1": 4544,
      "roma": "CASTELLINA MARITTIMA",
      "rm": "PI",
      "1234": 409050010
    },
    {
      "1": 4545,
      "roma": "CASTELLINO DEL BIFERNO",
      "rm": "CB",
      "1234": 414070014
    },
    {
      "1": 4546,
      "roma": "CASTELLINO TANARO",
      "rm": "CN",
      "1234": 401004052
    },
    {
      "1": 4547,
      "roma": "CASTELLIRI",
      "rm": "FR",
      "1234": 412060020
    },
    {
      "1": 4548,
      "roma": "CASTELLO CABIAGLIO",
      "rm": "VA",
      "1234": 403012043
    },
    {
      "1": 4549,
      "roma": "CASTEL CONDINO",
      "rm": "TN",
      "1234": 404022045
    },
    {
      "1": 4550,
      "roma": "CASTELLETTO D'ERRO",
      "rm": "AL",
      "1234": 401006048
    },
    {
      "1": 4551,
      "roma": "CASTELLETTO D'ORBA",
      "rm": "AL",
      "1234": 401006049
    },
    {
      "1": 4552,
      "roma": "CASTELFRANCO PIANDISCO'",
      "rm": "AR",
      "1234": 409051040
    },
    {
      "1": 4553,
      "roma": "CASTELLAMMARE DI STABIA",
      "rm": "NA",
      "1234": 415063024
    },
    {
      "1": 4554,
      "roma": "CASTELLETTO MERLI",
      "rm": "AL",
      "1234": 401006050
    },
    {
      "1": 4555,
      "roma": "CASTELLETTO MOLINA",
      "rm": "AT",
      "1234": 401005027
    },
    {
      "1": 4556,
      "roma": "CASTELLO DI CISTERNA",
      "rm": "NA",
      "1234": 415063025
    },
    {
      "1": 4557,
      "roma": "CASTELLO-MOLINA DI FIEMME",
      "rm": "TN",
      "1234": 404022047
    },
    {
      "1": 4558,
      "roma": "CASTELLO DI GODEGO",
      "rm": "TV",
      "1234": 405026013
    },
    {
      "1": 4559,
      "roma": "CASTELLO TESINO",
      "rm": "TN",
      "1234": 404022048
    },
    {
      "1": 4560,
      "roma": "CASTELLUCCHIO",
      "rm": "MN",
      "1234": 403020016
    },
    {
      "1": 4561,
      "roma": "CASTELLUCCIO DE' SAURI",
      "rm": "FG",
      "1234": 416071015
    },
    {
      "1": 4562,
      "roma": "CASTELLUCCIO INFERIORE",
      "rm": "PZ",
      "1234": 417076022
    },
    {
      "1": 4563,
      "roma": "CASTELLUCCIO SUPERIORE",
      "rm": "PZ",
      "1234": 417076023
    },
    {
      "1": 4564,
      "roma": "CASTELLUCCIO VALMAGGIORE",
      "rm": "FG",
      "1234": 416071016
    },
    {
      "1": 4565,
      "roma": "CASTELMAGNO",
      "rm": "CN",
      "1234": 401004053
    },
    {
      "1": 4566,
      "roma": "CASTELMARTE",
      "rm": "CO",
      "1234": 403013058
    },
    {
      "1": 4567,
      "roma": "CASTELMASSA",
      "rm": "RO",
      "1234": 405029012
    },
    {
      "1": 4568,
      "roma": "CASTELLO D'AGOGNA",
      "rm": "PV",
      "1234": 403018039
    },
    {
      "1": 4569,
      "roma": "CASTELLO D'ARGILE",
      "rm": "BO",
      "1234": 408037017
    },
    {
      "1": 4570,
      "roma": "CASTELLO DEL MATESE",
      "rm": "CE",
      "1234": 415061025
    },
    {
      "1": 4571,
      "roma": "CASTELLETTO DI BRANDUZZO",
      "rm": "PV",
      "1234": 403018038
    },
    {
      "1": 4572,
      "roma": "CASTELLO DI BRIANZA",
      "rm": "LC",
      "1234": 403097019
    },
    {
      "1": 4573,
      "roma": "CASTELLINALDO D'ALBA",
      "rm": "CN",
      "1234": 401004951
    },
    {
      "1": 4574,
      "roma": "CASTELNOVO DI SOTTO",
      "rm": "RE",
      "1234": 408035015
    },
    {
      "1": 4575,
      "roma": "CASTELNOVO NE' MONTI",
      "rm": "RE",
      "1234": 408035016
    },
    {
      "1": 4576,
      "roma": "CASTELNUOVO",
      "rm": "TN",
      "1234": 404022049
    },
    {
      "1": 4577,
      "roma": "CASTELNUOVO BELBO",
      "rm": "AT",
      "1234": 401005029
    },
    {
      "1": 4578,
      "roma": "CASTELNUOVO BERARDENGA",
      "rm": "SI",
      "1234": 409052006
    },
    {
      "1": 4579,
      "roma": "CASTELNUOVO BOCCA D'ADDA",
      "rm": "LO",
      "1234": 403098013
    },
    {
      "1": 4580,
      "roma": "CASTELNUOVO BORMIDA",
      "rm": "AL",
      "1234": 401006052
    },
    {
      "1": 4581,
      "roma": "CASTELNUOVO BOZZENTE",
      "rm": "CO",
      "1234": 403013059
    },
    {
      "1": 4582,
      "roma": "CASTELNUOVO CALCEA",
      "rm": "AT",
      "1234": 401005030
    },
    {
      "1": 4583,
      "roma": "CASTELNUOVO CILENTO",
      "rm": "SA",
      "1234": 415065032
    },
    {
      "1": 4584,
      "roma": "CASTELNUOVO DEL GARDA",
      "rm": "VR",
      "1234": 405023520
    },
    {
      "1": 4585,
      "roma": "CASTELNUOVO DELLA DAUNIA",
      "rm": "FG",
      "1234": 416071017
    },
    {
      "1": 4586,
      "roma": "CASTELNUOVO DI CEVA",
      "rm": "CN",
      "1234": 401004054
    },
    {
      "1": 4587,
      "roma": "CASTELMAURO",
      "rm": "CB",
      "1234": 414070015
    },
    {
      "1": 4588,
      "roma": "CASTELMEZZANO",
      "rm": "PZ",
      "1234": 417076024
    },
    {
      "1": 4589,
      "roma": "CASTELMOLA",
      "rm": "ME",
      "1234": 419083015
    },
    {
      "1": 4590,
      "roma": "CASTELLO DELL'ACQUA",
      "rm": "SO",
      "1234": 403014014
    },
    {
      "1": 4591,
      "roma": "CASTELLO DI ANNONE",
      "rm": "AT",
      "1234": 401005028
    },
    {
      "1": 4592,
      "roma": "CASTELNOVO BARIANO",
      "rm": "RO",
      "1234": 405029013
    },
    {
      "1": 4593,
      "roma": "CASTELNOVO DEL FRIULI",
      "rm": "PN",
      "1234": 406093011
    },
    {
      "1": 4594,
      "roma": "CASTELNUOVO MAGRA",
      "rm": "SP",
      "1234": 407011011
    },
    {
      "1": 4595,
      "roma": "CASTELNUOVO NIGRA",
      "rm": "TO",
      "1234": 401001067
    },
    {
      "1": 4596,
      "roma": "CASTELNUOVO PARANO",
      "rm": "FR",
      "1234": 412060021
    },
    {
      "1": 4597,
      "roma": "CASTELNUOVO RANGONE",
      "rm": "MO",
      "1234": 408036007
    },
    {
      "1": 4598,
      "roma": "CASTELNUOVO SCRIVIA",
      "rm": "AL",
      "1234": 401006053
    },
    {
      "1": 4599,
      "roma": "CASTELNUOVO DI VAL DI CECINA",
      "rm": "PI",
      "1234": 409050011
    },
    {
      "1": 4600,
      "roma": "CASTELPETROSO",
      "rm": "IS",
      "1234": 414094010
    },
    {
      "1": 4601,
      "roma": "CASTELPIZZUTO",
      "rm": "IS",
      "1234": 414094011
    },
    {
      "1": 4602,
      "roma": "CASTELPLANIO",
      "rm": "AN",
      "1234": 411042012
    },
    {
      "1": 4603,
      "roma": "CASTELPOTO",
      "rm": "BN",
      "1234": 415062018
    },
    {
      "1": 4604,
      "roma": "CASTELRAIMONDO",
      "rm": "MC",
      "1234": 411043009
    },
    {
      "1": 4605,
      "roma": "CASTELROTTO",
      "rm": "BZ",
      "1234": 404021019
    },
    {
      "1": 4606,
      "roma": "CASTELSANTANGELO SUL NERA",
      "rm": "MC",
      "1234": 411043010
    },
    {
      "1": 4607,
      "roma": "CASTELSARACENO",
      "rm": "PZ",
      "1234": 417076025
    },
    {
      "1": 4608,
      "roma": "CASTELSARDO",
      "rm": "SS",
      "1234": 420090023
    },
    {
      "1": 4609,
      "roma": "CASTELNUOVO DI CONZA",
      "rm": "SA",
      "1234": 415065033
    },
    {
      "1": 4610,
      "roma": "CASTELNUOVO DI FARFA",
      "rm": "RI",
      "1234": 412057014
    },
    {
      "1": 4611,
      "roma": "CASTELNUOVO DI GARFAGNANA",
      "rm": "LU",
      "1234": 409046009
    },
    {
      "1": 4612,
      "roma": "CASTELNOVETTO",
      "rm": "PV",
      "1234": 403018040
    },
    {
      "1": 4613,
      "roma": "CASTELNUOVO DON BOSCO",
      "rm": "AT",
      "1234": 401005031
    },
    {
      "1": 4614,
      "roma": "CASTELVECCHIO DI ROCCA BARBENA",
      "rm": "SV",
      "1234": 407009021
    },
    {
      "1": 4615,
      "roma": "CASTELVECCHIO SUBEQUO",
      "rm": "AQ",
      "1234": 413066031
    },
    {
      "1": 4616,
      "roma": "CASTELVENERE",
      "rm": "BN",
      "1234": 415062019
    },
    {
      "1": 4617,
      "roma": "CASTELVERDE",
      "rm": "CR",
      "1234": 403019026
    },
    {
      "1": 4618,
      "roma": "CASTELVERRINO",
      "rm": "IS",
      "1234": 414094013
    },
    {
      "1": 4619,
      "roma": "CASTELVETERE IN VAL FORTORE",
      "rm": "BN",
      "1234": 415062020
    },
    {
      "1": 4620,
      "roma": "CASTELVETRANO",
      "rm": "TP",
      "1234": 419081006
    },
    {
      "1": 4621,
      "roma": "CASTELVETRO DI MODENA",
      "rm": "MO",
      "1234": 408036008
    },
    {
      "1": 4622,
      "roma": "CASTELVETRO PIACENTINO",
      "rm": "PC",
      "1234": 408033014
    },
    {
      "1": 4623,
      "roma": "CASTELVISCONTI",
      "rm": "CR",
      "1234": 403019027
    },
    {
      "1": 4624,
      "roma": "CASTENASO",
      "rm": "BO",
      "1234": 408037021
    },
    {
      "1": 4625,
      "roma": "CASTENEDOLO",
      "rm": "BS",
      "1234": 403017043
    },
    {
      "1": 4626,
      "roma": "CASTIADAS",
      "rm": "CA",
      "1234": 420092106
    },
    {
      "1": 4627,
      "roma": "CASTIGLION FIBOCCHI",
      "rm": "AR",
      "1234": 409051011
    },
    {
      "1": 4628,
      "roma": "CASTIGLION FIORENTINO",
      "rm": "AR",
      "1234": 409051012
    },
    {
      "1": 4629,
      "roma": "CASTELSEPRIO",
      "rm": "VA",
      "1234": 403012044
    },
    {
      "1": 4630,
      "roma": "CASTELSILANO",
      "rm": "KR",
      "1234": 418101005
    },
    {
      "1": 4631,
      "roma": "CASTELNUOVO DI PORTO",
      "rm": "RM",
      "1234": 412058024
    },
    {
      "1": 4632,
      "roma": "CASTELVECCANA",
      "rm": "VA",
      "1234": 403012045
    },
    {
      "1": 4633,
      "roma": "CASTELVECCHIO CALVISIO",
      "rm": "AQ",
      "1234": 413066030
    },
    {
      "1": 4634,
      "roma": "CASTIGLIONE D'ORCIA",
      "rm": "SI",
      "1234": 409052007
    },
    {
      "1": 4635,
      "roma": "CASTIGLIONE DEI PEPOLI",
      "rm": "BO",
      "1234": 408037022
    },
    {
      "1": 4636,
      "roma": "CASTIGLIONE DEL GENOVESI",
      "rm": "SA",
      "1234": 415065036
    },
    {
      "1": 4637,
      "roma": "CASTIGLIONE DEL LAGO",
      "rm": "PG",
      "1234": 410054009
    },
    {
      "1": 4638,
      "roma": "CASTIGLIONE DELLA PESCAIA",
      "rm": "GR",
      "1234": 409053006
    },
    {
      "1": 4639,
      "roma": "CASTIGLIONE DELLE STIVIERE",
      "rm": "MN",
      "1234": 403020017
    },
    {
      "1": 4640,
      "roma": "CASTIGLIONE DI GARFAGNANA",
      "rm": "LU",
      "1234": 409046010
    },
    {
      "1": 4641,
      "roma": "CASTIGLIONE DI SICILIA",
      "rm": "CT",
      "1234": 419087014
    },
    {
      "1": 4642,
      "roma": "CASTIGLIONE FALLETTO",
      "rm": "CN",
      "1234": 401004055
    },
    {
      "1": 4643,
      "roma": "CASTIGLIONE IN TEVERINA",
      "rm": "VT",
      "1234": 412056018
    },
    {
      "1": 4644,
      "roma": "CASTIGLIONE MESSER MARINO",
      "rm": "CH",
      "1234": 413069020
    },
    {
      "1": 4645,
      "roma": "CASTIGLIONE MESSER RAIMONDO",
      "rm": "TE",
      "1234": 413067013
    },
    {
      "1": 4646,
      "roma": "CASTIGLIONE OLONA",
      "rm": "VA",
      "1234": 403012046
    },
    {
      "1": 4647,
      "roma": "CASTIGLIONE TINELLA",
      "rm": "CN",
      "1234": 401004056
    },
    {
      "1": 4648,
      "roma": "CASTIGLIONE TORINESE",
      "rm": "TO",
      "1234": 401001068
    },
    {
      "1": 4649,
      "roma": "CASTIGNANO",
      "rm": "AP",
      "1234": 411044012
    },
    {
      "1": 4650,
      "roma": "CASTILENTI",
      "rm": "TE",
      "1234": 413067014
    },
    {
      "1": 4651,
      "roma": "CASTINO",
      "rm": "CN",
      "1234": 401004057
    },
    {
      "1": 4652,
      "roma": "CASTIGLIONE A CASAURIA",
      "rm": "PE",
      "1234": 413068009
    },
    {
      "1": 4653,
      "roma": "CASTIGLIONE CHIAVARESE",
      "rm": "GE",
      "1234": 407010013
    },
    {
      "1": 4654,
      "roma": "CASTIGLIONE COSENTINO",
      "rm": "CS",
      "1234": 418078030
    },
    {
      "1": 4655,
      "roma": "CASTELSPINA",
      "rm": "AL",
      "1234": 401006054
    },
    {
      "1": 4656,
      "roma": "CASTELTERMINI",
      "rm": "AG",
      "1234": 419084012
    },
    {
      "1": 4657,
      "roma": "CASTO",
      "rm": "BS",
      "1234": 403017044
    },
    {
      "1": 4658,
      "roma": "CASTORANO",
      "rm": "AP",
      "1234": 411044013
    },
    {
      "1": 4659,
      "roma": "CASTREZZATO",
      "rm": "BS",
      "1234": 403017045
    },
    {
      "1": 4660,
      "roma": "CASTRI DI LECCE",
      "rm": "LE",
      "1234": 416075017
    },
    {
      "1": 4661,
      "roma": "CASTRIGNANO DE' GRECI",
      "rm": "LE",
      "1234": 416075018
    },
    {
      "1": 4662,
      "roma": "CASTRIGNANO DEL CAPO",
      "rm": "LE",
      "1234": 416075019
    },
    {
      "1": 4663,
      "roma": "CASTRO",
      "rm": "LE",
      "1234": 416075096
    },
    {
      "1": 4665,
      "roma": "CASTRO DEI VOLSCI",
      "rm": "FR",
      "1234": 412060023
    },
    {
      "1": 4666,
      "roma": "CASTROCARO TERME E TERRA DEL SOLE",
      "rm": "FO",
      "1234": 408040005
    },
    {
      "1": 4667,
      "roma": "CASTROCIELO",
      "rm": "FR",
      "1234": 412060022
    },
    {
      "1": 4668,
      "roma": "CASTROFILIPPO",
      "rm": "AG",
      "1234": 419084013
    },
    {
      "1": 4669,
      "roma": "CASTROLIBERO",
      "rm": "CS",
      "1234": 418078031
    },
    {
      "1": 4670,
      "roma": "CASTRONNO",
      "rm": "VA",
      "1234": 403012047
    },
    {
      "1": 4671,
      "roma": "CASTRONOVO DI SICILIA",
      "rm": "PA",
      "1234": 419082025
    },
    {
      "1": 4672,
      "roma": "CASTRONUOVO DI SANT'ANDREA",
      "rm": "PZ",
      "1234": 417076026
    },
    {
      "1": 4673,
      "roma": "CASTROPIGNANO",
      "rm": "CB",
      "1234": 414070016
    },
    {
      "1": 4674,
      "roma": "CASTROREALE",
      "rm": "ME",
      "1234": 419083016
    },
    {
      "1": 4675,
      "roma": "CASTIONE ANDEVENNO",
      "rm": "SO",
      "1234": 403014015
    },
    {
      "1": 4676,
      "roma": "CASTIONE DELLA PRESOLANA",
      "rm": "BG",
      "1234": 403016064
    },
    {
      "1": 4677,
      "roma": "CASTIGLIONE D'ADDA",
      "rm": "LO",
      "1234": 403098014
    },
    {
      "1": 4678,
      "roma": "CASTIRAGA VIDARDO",
      "rm": "LO",
      "1234": 403098015
    },
    {
      "1": 4679,
      "roma": "CATENANUOVA",
      "rm": "EN",
      "1234": 419086006
    },
    {
      "1": 4680,
      "roma": "CATIGNANO",
      "rm": "PE",
      "1234": 413068010
    },
    {
      "1": 4681,
      "roma": "CATTOLICA",
      "rm": "RN",
      "1234": 408099002
    },
    {
      "1": 4682,
      "roma": "CATTOLICA ERACLEA",
      "rm": "AG",
      "1234": 419084014
    },
    {
      "1": 4683,
      "roma": "CAULONIA",
      "rm": "RC",
      "1234": 418080025
    },
    {
      "1": 4684,
      "roma": "CAUTANO",
      "rm": "BN",
      "1234": 415062021
    },
    {
      "1": 4685,
      "roma": "CAVA DE' TIRRENI",
      "rm": "SA",
      "1234": 415065037
    },
    {
      "1": 4686,
      "roma": "CAVA MANARA",
      "rm": "PV",
      "1234": 403018041
    },
    {
      "1": 4687,
      "roma": "CAVAGLIA'",
      "rm": "BI",
      "1234": 401096016
    },
    {
      "1": 4688,
      "roma": "CAVAGLIETTO",
      "rm": "NO",
      "1234": 401003044
    },
    {
      "1": 4689,
      "roma": "CAVAGLIO D'AGOGNA",
      "rm": "NO",
      "1234": 401003045
    },
    {
      "1": 4690,
      "roma": "CASTROREGIO",
      "rm": "CS",
      "1234": 418078032
    },
    {
      "1": 4691,
      "roma": "CASTROVILLARI",
      "rm": "CS",
      "1234": 418078033
    },
    {
      "1": 4692,
      "roma": "CASTIONS DI STRADA",
      "rm": "UD",
      "1234": 406030020
    },
    {
      "1": 4693,
      "roma": "CATANZARO",
      "rm": "CZ",
      "1234": 418079023
    },
    {
      "1": 4694,
      "roma": "CAVALLERLEONE",
      "rm": "CN",
      "1234": 401004058
    },
    {
      "1": 4695,
      "roma": "CAVALLERMAGGIORE",
      "rm": "CN",
      "1234": 401004059
    },
    {
      "1": 4696,
      "roma": "CAVALLINO",
      "rm": "LE",
      "1234": 416075020
    },
    {
      "1": 4697,
      "roma": "CAVALLINO-TREPORTI",
      "rm": "VE",
      "1234": 405027044
    },
    {
      "1": 4698,
      "roma": "CAVALLIRIO",
      "rm": "NO",
      "1234": 401003047
    },
    {
      "1": 4699,
      "roma": "CAVARENO",
      "rm": "TN",
      "1234": 404022051
    },
    {
      "1": 4700,
      "roma": "CAVARGNA",
      "rm": "CO",
      "1234": 403013062
    },
    {
      "1": 4701,
      "roma": "CAVARIA CON PREMEZZO",
      "rm": "VA",
      "1234": 403012048
    },
    {
      "1": 4702,
      "roma": "CAVARZERE",
      "rm": "VE",
      "1234": 405027006
    },
    {
      "1": 4703,
      "roma": "CAVASO DEL TOMBA",
      "rm": "TV",
      "1234": 405026014
    },
    {
      "1": 4704,
      "roma": "CAVASSO NUOVO",
      "rm": "PN",
      "1234": 406093012
    },
    {
      "1": 4705,
      "roma": "CAVATORE",
      "rm": "AL",
      "1234": 401006055
    },
    {
      "1": 4706,
      "roma": "CAVAZZO CARNICO",
      "rm": "UD",
      "1234": 406030021
    },
    {
      "1": 4707,
      "roma": "CAVE",
      "rm": "RM",
      "1234": 412058026
    },
    {
      "1": 4708,
      "roma": "CATANIA",
      "rm": "CT",
      "1234": 419087015
    },
    {
      "1": 4709,
      "roma": "CAVALESE",
      "rm": "TN",
      "1234": 404022050
    },
    {
      "1": 4710,
      "roma": "CAVEZZO",
      "rm": "MO",
      "1234": 408036009
    },
    {
      "1": 4711,
      "roma": "CAVIZZANA",
      "rm": "TN",
      "1234": 404022054
    },
    {
      "1": 4712,
      "roma": "CAVOUR",
      "rm": "TO",
      "1234": 401001070
    },
    {
      "1": 4713,
      "roma": "CAVRIAGO",
      "rm": "RE",
      "1234": 408035017
    },
    {
      "1": 6216,
      "roma": "MERCATINO CONCA",
      "rm": "PS",
      "1234": 411041026
    },
    {
      "1": 6217,
      "roma": "MELITO IRPINO",
      "rm": "AV",
      "1234": 415064048
    },
    {
      "1": 6218,
      "roma": "MELIZZANO",
      "rm": "BN",
      "1234": 415062039
    },
    {
      "1": 6219,
      "roma": "MERCENASCO",
      "rm": "TO",
      "1234": 401001150
    },
    {
      "1": 6220,
      "roma": "MERCOGLIANO",
      "rm": "AV",
      "1234": 415064049
    },
    {
      "1": 6221,
      "roma": "MERETO DI TOMBA",
      "rm": "UD",
      "1234": 406030058
    },
    {
      "1": 6222,
      "roma": "MERGO",
      "rm": "AN",
      "1234": 411042024
    },
    {
      "1": 6223,
      "roma": "MERGOZZO",
      "rm": "VB",
      "1234": 401103044
    },
    {
      "1": 6224,
      "roma": "MERI'",
      "rm": "ME",
      "1234": 419083047
    },
    {
      "1": 6225,
      "roma": "MERLARA",
      "rm": "PD",
      "1234": 405028053
    },
    {
      "1": 6226,
      "roma": "MERLINO",
      "rm": "LO",
      "1234": 403098039
    },
    {
      "1": 6227,
      "roma": "MENDICINO",
      "rm": "CS",
      "1234": 418078079
    },
    {
      "1": 6228,
      "roma": "MENFI",
      "rm": "AG",
      "1234": 419084023
    },
    {
      "1": 6229,
      "roma": "MENTANA",
      "rm": "RM",
      "1234": 412058059
    },
    {
      "1": 6230,
      "roma": "MESE",
      "rm": "SO",
      "1234": 403014043
    },
    {
      "1": 6231,
      "roma": "MESENZANA",
      "rm": "VA",
      "1234": 403012102
    },
    {
      "1": 6232,
      "roma": "MESERO",
      "rm": "MI",
      "1234": 403015144
    },
    {
      "1": 6233,
      "roma": "MESOLA",
      "rm": "FE",
      "1234": 408038014
    },
    {
      "1": 6234,
      "roma": "MESORACA",
      "rm": "KR",
      "1234": 418101015
    },
    {
      "1": 6235,
      "roma": "MESSINA",
      "rm": "ME",
      "1234": 419083048
    },
    {
      "1": 6236,
      "roma": "MERCATO SAN SEVERINO",
      "rm": "SA",
      "1234": 415065067
    },
    {
      "1": 6237,
      "roma": "MERCATO SARACENO",
      "rm": "FO",
      "1234": 408040020
    },
    {
      "1": 6238,
      "roma": "MESTRINO",
      "rm": "PD",
      "1234": 405028054
    },
    {
      "1": 6239,
      "roma": "META",
      "rm": "NA",
      "1234": 415063046
    },
    {
      "1": 6240,
      "roma": "MEZZANA",
      "rm": "TN",
      "1234": 404022114
    },
    {
      "1": 6241,
      "roma": "MEZZANA BIGLI",
      "rm": "PV",
      "1234": 403018090
    },
    {
      "1": 6242,
      "roma": "MEZZANA MORTIGLIENGO",
      "rm": "BI",
      "1234": 401096033
    },
    {
      "1": 6243,
      "roma": "MERONE",
      "rm": "CO",
      "1234": 403013147
    },
    {
      "1": 6244,
      "roma": "MESAGNE",
      "rm": "BR",
      "1234": 416074010
    },
    {
      "1": 6245,
      "roma": "MEZZANINO",
      "rm": "PV",
      "1234": 403018092
    },
    {
      "1": 6246,
      "roma": "MEZZANO",
      "rm": "TN",
      "1234": 404022115
    },
    {
      "1": 6247,
      "roma": "MEZZAGO",
      "rm": "MB",
      "1234": 403108031
    },
    {
      "1": 6248,
      "roma": "MEZZOJUSO",
      "rm": "PA",
      "1234": 419082047
    },
    {
      "1": 6249,
      "roma": "MEZZOLDO",
      "rm": "BG",
      "1234": 403016134
    },
    {
      "1": 6250,
      "roma": "MEZZOLOMBARDO",
      "rm": "TN",
      "1234": 404022117
    },
    {
      "1": 6251,
      "roma": "MEZZOMERICO",
      "rm": "NO",
      "1234": 401003097
    },
    {
      "1": 6252,
      "roma": "MIAGLIANO",
      "rm": "BI",
      "1234": 401096034
    },
    {
      "1": 6253,
      "roma": "MIANE",
      "rm": "TV",
      "1234": 405026042
    },
    {
      "1": 6254,
      "roma": "MIASINO",
      "rm": "NO",
      "1234": 401003098
    },
    {
      "1": 6255,
      "roma": "MIAZZINA",
      "rm": "VB",
      "1234": 401103045
    },
    {
      "1": 6256,
      "roma": "MICIGLIANO",
      "rm": "RI",
      "1234": 412057037
    },
    {
      "1": 6257,
      "roma": "MEZZANA RABATTONE",
      "rm": "PV",
      "1234": 403018091
    },
    {
      "1": 6258,
      "roma": "MEZZANE DI SOTTO",
      "rm": "VR",
      "1234": 405023047
    },
    {
      "1": 6259,
      "roma": "MEZZANEGO",
      "rm": "GE",
      "1234": 407010034
    },
    {
      "1": 6260,
      "roma": "MIGLIANICO",
      "rm": "CH",
      "1234": 413069050
    },
    {
      "1": 6261,
      "roma": "MIGLIERINA",
      "rm": "CZ",
      "1234": 418079077
    },
    {
      "1": 6262,
      "roma": "MIGLIONICO",
      "rm": "MT",
      "1234": 417077015
    },
    {
      "1": 6263,
      "roma": "MIGNANEGO",
      "rm": "GE",
      "1234": 407010035
    },
    {
      "1": 6264,
      "roma": "MIGNANO MONTE LUNGO",
      "rm": "CE",
      "1234": 415061051
    },
    {
      "1": 6265,
      "roma": "MEZZENILE",
      "rm": "TO",
      "1234": 401001152
    },
    {
      "1": 6266,
      "roma": "MEZZOCORONA",
      "rm": "TN",
      "1234": 404022116
    },
    {
      "1": 6267,
      "roma": "MILANO",
      "rm": "MI",
      "1234": 403015146
    },
    {
      "1": 6268,
      "roma": "MILAZZO",
      "rm": "ME",
      "1234": 419083049
    },
    {
      "1": 6269,
      "roma": "MILENA",
      "rm": "CL",
      "1234": 419085010
    },
    {
      "1": 6270,
      "roma": "MILETO",
      "rm": "VV",
      "1234": 418102021
    },
    {
      "1": 6271,
      "roma": "MILIS",
      "rm": "OR",
      "1234": 420095027
    },
    {
      "1": 6272,
      "roma": "MILITELLO IN VAL DI CATANIA",
      "rm": "CT",
      "1234": 419087025
    },
    {
      "1": 6273,
      "roma": "MILITELLO ROSMARINO",
      "rm": "ME",
      "1234": 419083050
    },
    {
      "1": 6274,
      "roma": "MIGGIANO",
      "rm": "LE",
      "1234": 416075046
    },
    {
      "1": 6275,
      "roma": "MINEO",
      "rm": "CT",
      "1234": 419087027
    },
    {
      "1": 6276,
      "roma": "MINERBE",
      "rm": "VR",
      "1234": 405023048
    },
    {
      "1": 6277,
      "roma": "MINERBIO",
      "rm": "BO",
      "1234": 408037038
    },
    {
      "1": 6278,
      "roma": "MINERVINO DI LECCE",
      "rm": "LE",
      "1234": 416075047
    },
    {
      "1": 6279,
      "roma": "MINORI",
      "rm": "SA",
      "1234": 415065068
    },
    {
      "1": 6280,
      "roma": "MINTURNO",
      "rm": "LT",
      "1234": 412059014
    },
    {
      "1": 6281,
      "roma": "MINUCCIANO",
      "rm": "LU",
      "1234": 409046019
    },
    {
      "1": 6282,
      "roma": "MIOGLIA",
      "rm": "SV",
      "1234": 407009039
    },
    {
      "1": 6283,
      "roma": "MIRA",
      "rm": "VE",
      "1234": 405027023
    },
    {
      "1": 6284,
      "roma": "MIRABELLA ECLANO",
      "rm": "AV",
      "1234": 415064050
    },
    {
      "1": 6285,
      "roma": "MIRABELLA IMBACCARI",
      "rm": "CT",
      "1234": 419087028
    },
    {
      "1": 6286,
      "roma": "MIRABELLO MONFERRATO",
      "rm": "AL",
      "1234": 401006094
    },
    {
      "1": 6287,
      "roma": "MILLESIMO",
      "rm": "SV",
      "1234": 407009038
    },
    {
      "1": 6288,
      "roma": "MILO",
      "rm": "CT",
      "1234": 419087026
    },
    {
      "1": 6289,
      "roma": "MILZANO",
      "rm": "BS",
      "1234": 403017108
    },
    {
      "1": 6290,
      "roma": "MIRANDOLA",
      "rm": "MO",
      "1234": 408036022
    },
    {
      "1": 6291,
      "roma": "MIRANO",
      "rm": "VE",
      "1234": 405027024
    },
    {
      "1": 6292,
      "roma": "MIRTO",
      "rm": "ME",
      "1234": 419083051
    },
    {
      "1": 6293,
      "roma": "MISANO ADRIATICO",
      "rm": "RN",
      "1234": 408099005
    },
    {
      "1": 6294,
      "roma": "MISANO DI GERA D'ADDA",
      "rm": "BG",
      "1234": 403016135
    },
    {
      "1": 6295,
      "roma": "MINERVINO MURGE",
      "rm": "BT",
      "1234": 416110006
    },
    {
      "1": 6296,
      "roma": "MISSAGLIA",
      "rm": "LC",
      "1234": 403097049
    },
    {
      "1": 6297,
      "roma": "MISINTO",
      "rm": "MB",
      "1234": 403108032
    },
    {
      "1": 6298,
      "roma": "MISSANELLO",
      "rm": "PZ",
      "1234": 417076049
    },
    {
      "1": 6299,
      "roma": "MISTERBIANCO",
      "rm": "CT",
      "1234": 419087029
    },
    {
      "1": 6300,
      "roma": "MISTRETTA",
      "rm": "ME",
      "1234": 419083052
    },
    {
      "1": 6301,
      "roma": "MOASCA",
      "rm": "AT",
      "1234": 401005063
    },
    {
      "1": 6302,
      "roma": "MOCONESI",
      "rm": "GE",
      "1234": 407010036
    },
    {
      "1": 6303,
      "roma": "MIRABELLO SANNITICO",
      "rm": "CB",
      "1234": 414070038
    },
    {
      "1": 6304,
      "roma": "MIRADOLO TERME",
      "rm": "PV",
      "1234": 403018093
    },
    {
      "1": 6305,
      "roma": "MIRANDA",
      "rm": "IS",
      "1234": 414094027
    },
    {
      "1": 6306,
      "roma": "MODUGNO",
      "rm": "BA",
      "1234": 416072027
    },
    {
      "1": 6307,
      "roma": "MOENA",
      "rm": "TN",
      "1234": 404022118
    },
    {
      "1": 6308,
      "roma": "MOGGIO",
      "rm": "LC",
      "1234": 403097050
    },
    {
      "1": 6309,
      "roma": "MOGGIO UDINESE",
      "rm": "UD",
      "1234": 406030059
    },
    {
      "1": 6310,
      "roma": "MOGLIA",
      "rm": "MN",
      "1234": 403020035
    },
    {
      "1": 6311,
      "roma": "MISILMERI",
      "rm": "PA",
      "1234": 419082048
    },
    {
      "1": 6312,
      "roma": "MOGORELLA",
      "rm": "OR",
      "1234": 420095028
    },
    {
      "1": 6313,
      "roma": "MOGORO",
      "rm": "OR",
      "1234": 420095029
    },
    {
      "1": 6314,
      "roma": "MOIANO",
      "rm": "BN",
      "1234": 415062040
    },
    {
      "1": 6315,
      "roma": "MOIMACCO",
      "rm": "UD",
      "1234": 406030060
    },
    {
      "1": 6316,
      "roma": "MOIO ALCANTARA",
      "rm": "ME",
      "1234": 419083053
    },
    {
      "1": 6317,
      "roma": "MOIO DE' CALVI",
      "rm": "BG",
      "1234": 403016136
    },
    {
      "1": 6318,
      "roma": "MOIO DELLA CIVITELLA",
      "rm": "SA",
      "1234": 415065069
    },
    {
      "1": 6319,
      "roma": "MOIOLA",
      "rm": "CN",
      "1234": 401004123
    },
    {
      "1": 6320,
      "roma": "MOLA DI BARI",
      "rm": "BA",
      "1234": 416072028
    },
    {
      "1": 6321,
      "roma": "MOLARE",
      "rm": "AL",
      "1234": 401006095
    },
    {
      "1": 6322,
      "roma": "MODENA",
      "rm": "MO",
      "1234": 408036023
    },
    {
      "1": 6323,
      "roma": "MODICA",
      "rm": "RG",
      "1234": 419088006
    },
    {
      "1": 6324,
      "roma": "MODIGLIANA",
      "rm": "FO",
      "1234": 408040022
    },
    {
      "1": 6325,
      "roma": "MODOLO",
      "rm": "NU",
      "1234": 420091048
    },
    {
      "1": 6326,
      "roma": "MOLINA ATERNO",
      "rm": "AQ",
      "1234": 413066055
    },
    {
      "1": 6327,
      "roma": "MOLINARA",
      "rm": "BN",
      "1234": 415062041
    },
    {
      "1": 6328,
      "roma": "MOLINELLA",
      "rm": "BO",
      "1234": 408037039
    },
    {
      "1": 6329,
      "roma": "MOLINI DI TRIORA",
      "rm": "IM",
      "1234": 407008035
    },
    {
      "1": 6330,
      "roma": "MOGLIANO",
      "rm": "MC",
      "1234": 411043025
    },
    {
      "1": 6331,
      "roma": "MOGLIANO VENETO",
      "rm": "TV",
      "1234": 405026043
    },
    {
      "1": 6332,
      "roma": "MOLINO DEI TORTI",
      "rm": "AL",
      "1234": 401006096
    },
    {
      "1": 6333,
      "roma": "MOLISE",
      "rm": "CB",
      "1234": 414070039
    },
    {
      "1": 6334,
      "roma": "MOLITERNO",
      "rm": "PZ",
      "1234": 417076050
    },
    {
      "1": 6335,
      "roma": "MOLLIA",
      "rm": "VC",
      "1234": 401002078
    },
    {
      "1": 6336,
      "roma": "MOLOCHIO",
      "rm": "RC",
      "1234": 418080051
    },
    {
      "1": 6337,
      "roma": "MOLTENO",
      "rm": "LC",
      "1234": 403097051
    },
    {
      "1": 6338,
      "roma": "MOLTRASIO",
      "rm": "CO",
      "1234": 403013152
    },
    {
      "1": 6339,
      "roma": "MOLAZZANA",
      "rm": "LU",
      "1234": 409046020
    },
    {
      "1": 6340,
      "roma": "MOLFETTA",
      "rm": "BA",
      "1234": 416072029
    },
    {
      "1": 6341,
      "roma": "MOMBARUZZO",
      "rm": "AT",
      "1234": 401005065
    },
    {
      "1": 6342,
      "roma": "MOMBASIGLIO",
      "rm": "CN",
      "1234": 401004125
    },
    {
      "1": 6343,
      "roma": "MOMBELLO DI TORINO",
      "rm": "TO",
      "1234": 401001153
    },
    {
      "1": 6344,
      "roma": "MOMBELLO MONFERRATO",
      "rm": "AL",
      "1234": 401006097
    },
    {
      "1": 6345,
      "roma": "MOMBERCELLI",
      "rm": "AT",
      "1234": 401005066
    },
    {
      "1": 6346,
      "roma": "MOMPEO",
      "rm": "RI",
      "1234": 412057038
    },
    {
      "1": 6347,
      "roma": "MOMPERONE",
      "rm": "AL",
      "1234": 401006098
    },
    {
      "1": 6348,
      "roma": "MONACILIONI",
      "rm": "CB",
      "1234": 414070040
    },
    {
      "1": 6349,
      "roma": "MONALE",
      "rm": "AT",
      "1234": 401005067
    },
    {
      "1": 6350,
      "roma": "MONASTERACE",
      "rm": "RC",
      "1234": 418080052
    },
    {
      "1": 6351,
      "roma": "MONASTERO BORMIDA",
      "rm": "AT",
      "1234": 401005068
    },
    {
      "1": 6352,
      "roma": "MONASTERO DI LANZO",
      "rm": "TO",
      "1234": 401001155
    },
    {
      "1": 6353,
      "roma": "MONASTERO DI VASCO",
      "rm": "CN",
      "1234": 401004126
    },
    {
      "1": 6354,
      "roma": "MONASTEROLO CASOTTO",
      "rm": "CN",
      "1234": 401004127
    },
    {
      "1": 6355,
      "roma": "MONASTEROLO DEL CASTELLO",
      "rm": "BG",
      "1234": 403016137
    },
    {
      "1": 6356,
      "roma": "MONASTEROLO DI SAVIGLIANO",
      "rm": "CN",
      "1234": 401004128
    },
    {
      "1": 6357,
      "roma": "MOLVENO",
      "rm": "TN",
      "1234": 404022120
    },
    {
      "1": 6358,
      "roma": "MOMBALDONE",
      "rm": "AT",
      "1234": 401005064
    },
    {
      "1": 6359,
      "roma": "MOMBARCARO",
      "rm": "CN",
      "1234": 401004124
    },
    {
      "1": 6360,
      "roma": "MOMBAROCCIO",
      "rm": "PS",
      "1234": 411041027
    },
    {
      "1": 6361,
      "roma": "MONCALVO",
      "rm": "AT",
      "1234": 401005069
    },
    {
      "1": 6362,
      "roma": "MONCENISIO",
      "rm": "TO",
      "1234": 401001157
    },
    {
      "1": 6363,
      "roma": "MONCESTINO",
      "rm": "AL",
      "1234": 401006099
    },
    {
      "1": 6364,
      "roma": "MONCHIERO",
      "rm": "CN",
      "1234": 401004129
    },
    {
      "1": 6365,
      "roma": "MONCHIO DELLE CORTI",
      "rm": "PR",
      "1234": 408034022
    },
    {
      "1": 6366,
      "roma": "MOMO",
      "rm": "NO",
      "1234": 401003100
    },
    {
      "1": 6367,
      "roma": "MOMPANTERO",
      "rm": "TO",
      "1234": 401001154
    },
    {
      "1": 6368,
      "roma": "MONCUCCO TORINESE",
      "rm": "AT",
      "1234": 401005070
    },
    {
      "1": 6369,
      "roma": "MONDAINO",
      "rm": "RN",
      "1234": 408099006
    },
    {
      "1": 6370,
      "roma": "MONDAVIO",
      "rm": "PS",
      "1234": 411041028
    },
    {
      "1": 6371,
      "roma": "MONDOLFO",
      "rm": "PS",
      "1234": 411041029
    },
    {
      "1": 6372,
      "roma": "MONDOVI'",
      "rm": "CN",
      "1234": 401004130
    },
    {
      "1": 6373,
      "roma": "MONDRAGONE",
      "rm": "CE",
      "1234": 415061052
    },
    {
      "1": 6374,
      "roma": "MONEGLIA",
      "rm": "GE",
      "1234": 407010037
    },
    {
      "1": 6375,
      "roma": "MONESIGLIO",
      "rm": "CN",
      "1234": 401004131
    },
    {
      "1": 6376,
      "roma": "MONASTIER DI TREVISO",
      "rm": "TV",
      "1234": 405026044
    },
    {
      "1": 6377,
      "roma": "MONASTIR",
      "rm": "CA",
      "1234": 420092038
    },
    {
      "1": 6378,
      "roma": "MONCALIERI",
      "rm": "TO",
      "1234": 401001156
    },
    {
      "1": 6379,
      "roma": "MONGHIDORO",
      "rm": "BO",
      "1234": 408037040
    },
    {
      "1": 6380,
      "roma": "MONGIANA",
      "rm": "VV",
      "1234": 418102022
    },
    {
      "1": 6381,
      "roma": "MONGIARDINO LIGURE",
      "rm": "AL",
      "1234": 401006100
    },
    {
      "1": 6382,
      "roma": "MONCRIVELLO",
      "rm": "VC",
      "1234": 401002079
    },
    {
      "1": 6383,
      "roma": "MONGRANDO",
      "rm": "BI",
      "1234": 401096035
    },
    {
      "1": 6384,
      "roma": "MONGRASSANO",
      "rm": "CS",
      "1234": 418078080
    },
    {
      "1": 6385,
      "roma": "MONGUELFO",
      "rm": "BZ",
      "1234": 404021052
    },
    {
      "1": 6386,
      "roma": "MONGUZZO",
      "rm": "CO",
      "1234": 403013153
    },
    {
      "1": 6387,
      "roma": "MONIGA DEL GARDA",
      "rm": "BS",
      "1234": 403017109
    },
    {
      "1": 6388,
      "roma": "MONLEALE",
      "rm": "AL",
      "1234": 401006101
    },
    {
      "1": 6389,
      "roma": "MONNO",
      "rm": "BS",
      "1234": 403017110
    },
    {
      "1": 6390,
      "roma": "MONOPOLI",
      "rm": "BA",
      "1234": 416072030
    },
    {
      "1": 6391,
      "roma": "MONREALE",
      "rm": "PA",
      "1234": 419082049
    },
    {
      "1": 6392,
      "roma": "MONRUPINO",
      "rm": "TS",
      "1234": 406032002
    },
    {
      "1": 6393,
      "roma": "MONSAMPOLO DEL TRONTO",
      "rm": "AP",
      "1234": 411044031
    },
    {
      "1": 6394,
      "roma": "MONFALCONE",
      "rm": "GO",
      "1234": 406031012
    },
    {
      "1": 6395,
      "roma": "MONFORTE D'ALBA",
      "rm": "CN",
      "1234": 401004132
    },
    {
      "1": 6396,
      "roma": "MONFORTE SAN GIORGIO",
      "rm": "ME",
      "1234": 419083054
    },
    {
      "1": 6397,
      "roma": "MONFUMO",
      "rm": "TV",
      "1234": 405026045
    },
    {
      "1": 6398,
      "roma": "MONGARDINO",
      "rm": "AT",
      "1234": 401005071
    },
    {
      "1": 6399,
      "roma": "MONTABONE",
      "rm": "AT",
      "1234": 401005072
    },
    {
      "1": 6400,
      "roma": "MONTACUTO",
      "rm": "AL",
      "1234": 401006102
    },
    {
      "1": 6401,
      "roma": "MONTAFIA",
      "rm": "AT",
      "1234": 401005073
    },
    {
      "1": 6402,
      "roma": "MONTAGANO",
      "rm": "CB",
      "1234": 414070041
    },
    {
      "1": 6403,
      "roma": "MOTTA MONTECORVINO",
      "rm": "FG",
      "1234": 416071034
    },
    {
      "1": 6404,
      "roma": "MOTTA SAN GIOVANNI",
      "rm": "RC",
      "1234": 418080054
    },
    {
      "1": 6405,
      "roma": "MOTTA SANT'ANASTASIA",
      "rm": "CT",
      "1234": 419087030
    },
    {
      "1": 6406,
      "roma": "MOTTA SANTA LUCIA",
      "rm": "CZ",
      "1234": 418079083
    },
    {
      "1": 6407,
      "roma": "MORESCO",
      "rm": "FM",
      "1234": 411109028
    },
    {
      "1": 6408,
      "roma": "MORUZZO",
      "rm": "UD",
      "1234": 406030063
    },
    {
      "1": 6409,
      "roma": "MOSCAZZANO",
      "rm": "CR",
      "1234": 403019060
    },
    {
      "1": 6410,
      "roma": "MOSCIANO SANT'ANGELO",
      "rm": "TE",
      "1234": 413067030
    },
    {
      "1": 6411,
      "roma": "MOSCUFO",
      "rm": "PE",
      "1234": 413068025
    },
    {
      "1": 6412,
      "roma": "MOSO IN PASSIRIA",
      "rm": "BZ",
      "1234": 404021054
    },
    {
      "1": 6413,
      "roma": "MOSSA",
      "rm": "GO",
      "1234": 406031014
    },
    {
      "1": 6414,
      "roma": "MOZZO",
      "rm": "BG",
      "1234": 403016143
    },
    {
      "1": 6415,
      "roma": "MUCCIA",
      "rm": "MC",
      "1234": 411043034
    },
    {
      "1": 6416,
      "roma": "MUGGIA",
      "rm": "TS",
      "1234": 406032003
    },
    {
      "1": 6417,
      "roma": "MUGNANO DEL CARDINALE",
      "rm": "AV",
      "1234": 415064065
    },
    {
      "1": 6418,
      "roma": "MUGNANO DI NAPOLI",
      "rm": "NA",
      "1234": 415063048
    },
    {
      "1": 6419,
      "roma": "MONZA",
      "rm": "MB",
      "1234": 403108033
    },
    {
      "1": 6420,
      "roma": "MOTTA VISCONTI",
      "rm": "MI",
      "1234": 403015151
    },
    {
      "1": 6421,
      "roma": "MOTTAFOLLONE",
      "rm": "CS",
      "1234": 418078085
    },
    {
      "1": 6422,
      "roma": "MOTTALCIATA",
      "rm": "BI",
      "1234": 401096037
    },
    {
      "1": 6423,
      "roma": "MOTTEGGIANA",
      "rm": "MN",
      "1234": 403020037
    },
    {
      "1": 6424,
      "roma": "MOSCHIANO",
      "rm": "AV",
      "1234": 415064064
    },
    {
      "1": 6425,
      "roma": "MOZZAGROGNA",
      "rm": "CH",
      "1234": 413069056
    },
    {
      "1": 6426,
      "roma": "MOZZANICA",
      "rm": "BG",
      "1234": 403016142
    },
    {
      "1": 5563,
      "roma": "GARESSIO",
      "rm": "CN",
      "1234": 401004095
    },
    {
      "1": 5564,
      "roma": "GARGALLO",
      "rm": "NO",
      "1234": 401003070
    },
    {
      "1": 5565,
      "roma": "GAVOI",
      "rm": "NU",
      "1234": 420091028
    },
    {
      "1": 5566,
      "roma": "GAVORRANO",
      "rm": "GR",
      "1234": 409053010
    },
    {
      "1": 5567,
      "roma": "GAZOLDO DEGLI IPPOLITI",
      "rm": "MN",
      "1234": 403020024
    },
    {
      "1": 5568,
      "roma": "INTRODACQUA",
      "rm": "AQ",
      "1234": 413066048
    },
    {
      "1": 5569,
      "roma": "INVERIGO",
      "rm": "CO",
      "1234": 403013118
    },
    {
      "1": 5570,
      "roma": "IMPERIA",
      "rm": "IM",
      "1234": 407008031
    },
    {
      "1": 5571,
      "roma": "GUSSOLA",
      "rm": "CR",
      "1234": 403019052
    },
    {
      "1": 5572,
      "roma": "INARZO",
      "rm": "VA",
      "1234": 403012082
    },
    {
      "1": 5573,
      "roma": "INCISA SCAPACCINO",
      "rm": "AT",
      "1234": 401005058
    },
    {
      "1": 5574,
      "roma": "INZAGO",
      "rm": "MI",
      "1234": 403015114
    },
    {
      "1": 5575,
      "roma": "JOLANDA DI SAVOIA",
      "rm": "FE",
      "1234": 408038010
    },
    {
      "1": 5576,
      "roma": "IONADI",
      "rm": "VV",
      "1234": 418102017
    },
    {
      "1": 5577,
      "roma": "IRGOLI",
      "rm": "NU",
      "1234": 420091033
    },
    {
      "1": 5578,
      "roma": "IRMA",
      "rm": "BS",
      "1234": 403017084
    },
    {
      "1": 5579,
      "roma": "IRSINA",
      "rm": "MT",
      "1234": 417077013
    },
    {
      "1": 5580,
      "roma": "INVERNO E MONTELEONE",
      "rm": "PV",
      "1234": 403018077
    },
    {
      "1": 5581,
      "roma": "IMPRUNETA",
      "rm": "FI",
      "1234": 409048022
    },
    {
      "1": 5582,
      "roma": "INVERUNO",
      "rm": "MI",
      "1234": 403015113
    },
    {
      "1": 5583,
      "roma": "INVORIO",
      "rm": "NO",
      "1234": 401003082
    },
    {
      "1": 5584,
      "roma": "ISERA",
      "rm": "TN",
      "1234": 404022098
    },
    {
      "1": 5585,
      "roma": "ISERNIA",
      "rm": "IS",
      "1234": 414094023
    },
    {
      "1": 5586,
      "roma": "ISNELLO",
      "rm": "PA",
      "1234": 419082042
    },
    {
      "1": 5587,
      "roma": "ISOLA D'ASTI",
      "rm": "AT",
      "1234": 401005059
    },
    {
      "1": 5588,
      "roma": "ISOLA DEL CANTONE",
      "rm": "GE",
      "1234": 407010027
    },
    {
      "1": 5589,
      "roma": "ISOLA DEL GIGLIO",
      "rm": "GR",
      "1234": 409053012
    },
    {
      "1": 5590,
      "roma": "ISOLA DEL GRAN SASSO D'ITALIA",
      "rm": "TE",
      "1234": 413067026
    },
    {
      "1": 5591,
      "roma": "ISOLA DEL LIRI",
      "rm": "FR",
      "1234": 412060043
    },
    {
      "1": 5592,
      "roma": "ISOLA DEL PIANO",
      "rm": "PS",
      "1234": 411041021
    },
    {
      "1": 5593,
      "roma": "ISILI",
      "rm": "CA",
      "1234": 420092114
    },
    {
      "1": 5594,
      "roma": "ISOLA DELLA SCALA",
      "rm": "VR",
      "1234": 405023040
    },
    {
      "1": 5595,
      "roma": "ISOLA DELLE FEMMINE",
      "rm": "PA",
      "1234": 419082043
    },
    {
      "1": 5596,
      "roma": "ISASCA",
      "rm": "CN",
      "1234": 401004103
    },
    {
      "1": 5597,
      "roma": "INVERSO PINASCA",
      "rm": "TO",
      "1234": 401001122
    },
    {
      "1": 5598,
      "roma": "ISCHIA DI CASTRO",
      "rm": "VT",
      "1234": 412056031
    },
    {
      "1": 5599,
      "roma": "ISCHITELLA",
      "rm": "FG",
      "1234": 416071025
    },
    {
      "1": 5600,
      "roma": "ISEO",
      "rm": "BS",
      "1234": 403017085
    },
    {
      "1": 5601,
      "roma": "ISOLA SANT'ANTONIO",
      "rm": "AL",
      "1234": 401006087
    },
    {
      "1": 5602,
      "roma": "ISOLA VICENTINA",
      "rm": "VI",
      "1234": 405024048
    },
    {
      "1": 5603,
      "roma": "ISOLABELLA",
      "rm": "TO",
      "1234": 401001123
    },
    {
      "1": 5604,
      "roma": "ISOLABONA",
      "rm": "IM",
      "1234": 407008032
    },
    {
      "1": 5605,
      "roma": "ISOLE TREMITI",
      "rm": "FG",
      "1234": 416071026
    },
    {
      "1": 5606,
      "roma": "ISORELLA",
      "rm": "BS",
      "1234": 403017086
    },
    {
      "1": 5607,
      "roma": "ISPANI",
      "rm": "SA",
      "1234": 415065059
    },
    {
      "1": 5608,
      "roma": "ISPICA",
      "rm": "RG",
      "1234": 419088005
    },
    {
      "1": 5609,
      "roma": "ISPRA",
      "rm": "VA",
      "1234": 403012084
    },
    {
      "1": 5610,
      "roma": "ISSIGLIO",
      "rm": "TO",
      "1234": 401001124
    },
    {
      "1": 5611,
      "roma": "ISSIME",
      "rm": "AO",
      "1234": 402007036
    },
    {
      "1": 5612,
      "roma": "ISSO",
      "rm": "BG",
      "1234": 403016122
    },
    {
      "1": 5613,
      "roma": "ISSOGNE",
      "rm": "AO",
      "1234": 402007037
    },
    {
      "1": 5614,
      "roma": "ISOLA DI CAPO RIZZUTO",
      "rm": "KR",
      "1234": 418101013
    },
    {
      "1": 5615,
      "roma": "ISCA SULLO IONIO",
      "rm": "CZ",
      "1234": 418079063
    },
    {
      "1": 5616,
      "roma": "ISCHIA",
      "rm": "NA",
      "1234": 415063037
    },
    {
      "1": 5617,
      "roma": "ISOLA DOVARESE",
      "rm": "CR",
      "1234": 403019053
    },
    {
      "1": 5618,
      "roma": "ISOLA RIZZA",
      "rm": "VR",
      "1234": 405023041
    },
    {
      "1": 5619,
      "roma": "IVREA",
      "rm": "TO",
      "1234": 401001125
    },
    {
      "1": 5620,
      "roma": "IZANO",
      "rm": "CR",
      "1234": 403019054
    },
    {
      "1": 5621,
      "roma": "JACURSO",
      "rm": "CZ",
      "1234": 418079065
    },
    {
      "1": 5622,
      "roma": "JELSI",
      "rm": "CB",
      "1234": 414070030
    },
    {
      "1": 5623,
      "roma": "JENNE",
      "rm": "RM",
      "1234": 412058048
    },
    {
      "1": 5624,
      "roma": "JERAGO CON ORAGO",
      "rm": "VA",
      "1234": 403012085
    },
    {
      "1": 5625,
      "roma": "JERZU",
      "rm": "NU",
      "1234": 420091035
    },
    {
      "1": 5626,
      "roma": "JESI",
      "rm": "AN",
      "1234": 411042021
    },
    {
      "1": 5627,
      "roma": "JOPPOLO",
      "rm": "VV",
      "1234": 418102018
    },
    {
      "1": 5628,
      "roma": "JOPPOLO GIANCAXIO",
      "rm": "AG",
      "1234": 419084019
    },
    {
      "1": 5629,
      "roma": "JOVENCAN",
      "rm": "AO",
      "1234": 402007038
    },
    {
      "1": 5630,
      "roma": "ISTRANA",
      "rm": "TV",
      "1234": 405026035
    },
    {
      "1": 5631,
      "roma": "ISOLA DI FONDRA",
      "rm": "BG",
      "1234": 403016121
    },
    {
      "1": 5632,
      "roma": "ITTIREDDU",
      "rm": "SS",
      "1234": 420090032
    },
    {
      "1": 5633,
      "roma": "ITTIRI",
      "rm": "SS",
      "1234": 420090033
    },
    {
      "1": 5634,
      "roma": "LA MADDALENA",
      "rm": "SS",
      "1234": 420090035
    },
    {
      "1": 5635,
      "roma": "LA MAGDELEINE",
      "rm": "AO",
      "1234": 402007039
    },
    {
      "1": 5636,
      "roma": "LA MORRA",
      "rm": "CN",
      "1234": 401004105
    },
    {
      "1": 5637,
      "roma": "LA SALLE",
      "rm": "AO",
      "1234": 402007040
    },
    {
      "1": 5638,
      "roma": "LA SPEZIA",
      "rm": "SP",
      "1234": 407011015
    },
    {
      "1": 5639,
      "roma": "LA THUILE",
      "rm": "AO",
      "1234": 402007041
    },
    {
      "1": 5640,
      "roma": "LA VALLE",
      "rm": "BZ",
      "1234": 404021117
    },
    {
      "1": 5641,
      "roma": "LA VALLE AGORDINA",
      "rm": "BL",
      "1234": 405025027
    },
    {
      "1": 5642,
      "roma": "LABICO",
      "rm": "RM",
      "1234": 412058049
    },
    {
      "1": 5643,
      "roma": "LABRO",
      "rm": "RI",
      "1234": 412057032
    },
    {
      "1": 5644,
      "roma": "LACCHIARELLA",
      "rm": "MI",
      "1234": 403015115
    },
    {
      "1": 5645,
      "roma": "LACCO AMENO",
      "rm": "NA",
      "1234": 415063038
    },
    {
      "1": 5646,
      "roma": "LACEDONIA",
      "rm": "AV",
      "1234": 415064041
    },
    {
      "1": 5647,
      "roma": "LACES",
      "rm": "BZ",
      "1234": 404021037
    },
    {
      "1": 5648,
      "roma": "LACONI",
      "rm": "NU",
      "1234": 420091036
    },
    {
      "1": 5649,
      "roma": "LADISPOLI",
      "rm": "RM",
      "1234": 412058116
    },
    {
      "1": 5650,
      "roma": "ITALA",
      "rm": "ME",
      "1234": 419083036
    },
    {
      "1": 5651,
      "roma": "ITRI",
      "rm": "LT",
      "1234": 412059010
    },
    {
      "1": 5652,
      "roma": "LA CASSA",
      "rm": "TO",
      "1234": 401001126
    },
    {
      "1": 5653,
      "roma": "LA LOGGIA",
      "rm": "TO",
      "1234": 401001127
    },
    {
      "1": 5654,
      "roma": "LAGO",
      "rm": "CS",
      "1234": 418078062
    },
    {
      "1": 5655,
      "roma": "LAGONEGRO",
      "rm": "PZ",
      "1234": 417076039
    },
    {
      "1": 5656,
      "roma": "LAGOSANTO",
      "rm": "FE",
      "1234": 408038011
    },
    {
      "1": 5657,
      "roma": "LAGUNDO",
      "rm": "BZ",
      "1234": 404021038
    },
    {
      "1": 5658,
      "roma": "LAIGUEGLIA",
      "rm": "SV",
      "1234": 407009033
    },
    {
      "1": 5659,
      "roma": "LAINATE",
      "rm": "MI",
      "1234": 403015116
    },
    {
      "1": 5660,
      "roma": "LAINO",
      "rm": "CO",
      "1234": 403013120
    },
    {
      "1": 5661,
      "roma": "LAINO BORGO",
      "rm": "CS",
      "1234": 418078063
    },
    {
      "1": 5662,
      "roma": "LAINO CASTELLO",
      "rm": "CS",
      "1234": 418078064
    },
    {
      "1": 5663,
      "roma": "LAION",
      "rm": "BZ",
      "1234": 404021039
    },
    {
      "1": 5664,
      "roma": "LAIVES",
      "rm": "BZ",
      "1234": 404021040
    },
    {
      "1": 5665,
      "roma": "LAJATICO",
      "rm": "PI",
      "1234": 409050016
    },
    {
      "1": 5666,
      "roma": "LALLIO",
      "rm": "BG",
      "1234": 403016123
    },
    {
      "1": 5667,
      "roma": "LAMA DEI PELIGNI",
      "rm": "CH",
      "1234": 413069045
    },
    {
      "1": 5668,
      "roma": "LAMA MOCOGNO",
      "rm": "MO",
      "1234": 408036018
    },
    {
      "1": 5669,
      "roma": "LAERRU",
      "rm": "SS",
      "1234": 420090034
    },
    {
      "1": 5670,
      "roma": "L'AQUILA",
      "rm": "AQ",
      "1234": 413066049
    },
    {
      "1": 5671,
      "roma": "LAGLIO",
      "rm": "CO",
      "1234": 403013119
    },
    {
      "1": 5672,
      "roma": "LAGNASCO",
      "rm": "CN",
      "1234": 401004104
    },
    {
      "1": 5673,
      "roma": "LANCIANO",
      "rm": "CH",
      "1234": 413069046
    },
    {
      "1": 5674,
      "roma": "LANDIONA",
      "rm": "NO",
      "1234": 401003083
    },
    {
      "1": 5675,
      "roma": "LANDRIANO",
      "rm": "PV",
      "1234": 403018078
    },
    {
      "1": 5676,
      "roma": "LANGHIRANO",
      "rm": "PR",
      "1234": 408034018
    },
    {
      "1": 5677,
      "roma": "LANGOSCO",
      "rm": "PV",
      "1234": 403018079
    },
    {
      "1": 5678,
      "roma": "LANUSEI",
      "rm": "NU",
      "1234": 420091037
    },
    {
      "1": 5679,
      "roma": "LANUVIO",
      "rm": "RM",
      "1234": 412058050
    },
    {
      "1": 5680,
      "roma": "LANZADA",
      "rm": "SO",
      "1234": 403014036
    },
    {
      "1": 5681,
      "roma": "LANZO TORINESE",
      "rm": "TO",
      "1234": 401001128
    },
    {
      "1": 5682,
      "roma": "LAPIO",
      "rm": "AV",
      "1234": 415064042
    },
    {
      "1": 5683,
      "roma": "LAPPANO",
      "rm": "CS",
      "1234": 418078065
    },
    {
      "1": 5684,
      "roma": "LARCIANO",
      "rm": "PT",
      "1234": 409047006
    },
    {
      "1": 5685,
      "roma": "LAMBRUGO",
      "rm": "CO",
      "1234": 403013121
    },
    {
      "1": 5686,
      "roma": "LAGANADI",
      "rm": "RC",
      "1234": 418080041
    },
    {
      "1": 5687,
      "roma": "LAGHI",
      "rm": "VI",
      "1234": 405024049
    },
    {
      "1": 5688,
      "roma": "LAMPORECCHIO",
      "rm": "PT",
      "1234": 409047005
    },
    {
      "1": 5689,
      "roma": "LAMPORO",
      "rm": "VC",
      "1234": 401002067
    },
    {
      "1": 5690,
      "roma": "LANA",
      "rm": "BZ",
      "1234": 404021041
    },
    {
      "1": 5691,
      "roma": "LARINO",
      "rm": "CB",
      "1234": 414070031
    },
    {
      "1": 5692,
      "roma": "LAS PLASSAS",
      "rm": "CA",
      "1234": 420092034
    },
    {
      "1": 5693,
      "roma": "LASA",
      "rm": "BZ",
      "1234": 404021042
    },
    {
      "1": 5694,
      "roma": "LASCARI",
      "rm": "PA",
      "1234": 419082044
    },
    {
      "1": 5695,
      "roma": "LASNIGO",
      "rm": "CO",
      "1234": 403013123
    },
    {
      "1": 5696,
      "roma": "LASTEBASSE",
      "rm": "VI",
      "1234": 405024050
    },
    {
      "1": 5697,
      "roma": "LASTRA A SIGNA",
      "rm": "FI",
      "1234": 409048024
    },
    {
      "1": 5698,
      "roma": "LATERA",
      "rm": "VT",
      "1234": 412056032
    },
    {
      "1": 5699,
      "roma": "LATERZA",
      "rm": "TA",
      "1234": 416073009
    },
    {
      "1": 5700,
      "roma": "LATIANO",
      "rm": "BR",
      "1234": 416074009
    },
    {
      "1": 5701,
      "roma": "LATINA",
      "rm": "LT",
      "1234": 412059011
    },
    {
      "1": 5702,
      "roma": "LATISANA",
      "rm": "UD",
      "1234": 406030046
    },
    {
      "1": 5703,
      "roma": "LATRONICO",
      "rm": "PZ",
      "1234": 417076040
    },
    {
      "1": 5704,
      "roma": "LAMEZIA TERME",
      "rm": "CZ",
      "1234": 418079160
    },
    {
      "1": 5705,
      "roma": "LAMON",
      "rm": "BL",
      "1234": 405025026
    },
    {
      "1": 5706,
      "roma": "LAMPEDUSA E LINOSA",
      "rm": "AG",
      "1234": 419084020
    },
    {
      "1": 5707,
      "roma": "LAPEDONA",
      "rm": "FM",
      "1234": 411109009
    },
    {
      "1": 5708,
      "roma": "LARIANO",
      "rm": "RM",
      "1234": 412058115
    },
    {
      "1": 5709,
      "roma": "LAURENZANA",
      "rm": "PZ",
      "1234": 417076041
    },
    {
      "1": 5710,
      "roma": "LAURIA",
      "rm": "PZ",
      "1234": 417076042
    },
    {
      "1": 5711,
      "roma": "LAURIANO",
      "rm": "TO",
      "1234": 401001129
    },
    {
      "1": 5712,
      "roma": "LAURINO",
      "rm": "SA",
      "1234": 415065061
    },
    {
      "1": 5713,
      "roma": "LAURITO",
      "rm": "SA",
      "1234": 415065062
    },
    {
      "1": 5714,
      "roma": "LAURO",
      "rm": "AV",
      "1234": 415064043
    },
    {
      "1": 5715,
      "roma": "LAVAGNA",
      "rm": "GE",
      "1234": 407010028
    },
    {
      "1": 5716,
      "roma": "LAVAGNO",
      "rm": "VR",
      "1234": 405023042
    },
    {
      "1": 5717,
      "roma": "LAVARONE",
      "rm": "TN",
      "1234": 404022102
    },
    {
      "1": 5718,
      "roma": "LAVELLO",
      "rm": "PZ",
      "1234": 417076043
    },
    {
      "1": 5719,
      "roma": "LAVENA-PONTE TRESA",
      "rm": "VA",
      "1234": 403012086
    },
    {
      "1": 5720,
      "roma": "LAVENO-MOMBELLO",
      "rm": "VA",
      "1234": 403012087
    },
    {
      "1": 5721,
      "roma": "LAVENONE",
      "rm": "BS",
      "1234": 403017087
    },
    {
      "1": 5722,
      "roma": "LAVIANO",
      "rm": "SA",
      "1234": 415065063
    },
    {
      "1": 5723,
      "roma": "LAVIS",
      "rm": "TN",
      "1234": 404022103
    },
    {
      "1": 5724,
      "roma": "LATTARICO",
      "rm": "CS",
      "1234": 418078066
    },
    {
      "1": 5725,
      "roma": "LAUCO",
      "rm": "UD",
      "1234": 406030047
    },
    {
      "1": 5726,
      "roma": "LARDIRAGO",
      "rm": "PV",
      "1234": 403018080
    },
    {
      "1": 5727,
      "roma": "LAUREANA DI BORRELLO",
      "rm": "RC",
      "1234": 418080042
    },
    {
      "1": 5728,
      "roma": "LAUREGNO",
      "rm": "BZ",
      "1234": 404021043
    },
    {
      "1": 5729,
      "roma": "LAZZATE",
      "rm": "MB",
      "1234": 403108025
    },
    {
      "1": 5730,
      "roma": "LEFFE",
      "rm": "BG",
      "1234": 403016124
    },
    {
      "1": 5731,
      "roma": "LEGGIUNO",
      "rm": "VA",
      "1234": 403012088
    },
    {
      "1": 5732,
      "roma": "LEGNAGO",
      "rm": "VR",
      "1234": 405023044
    },
    {
      "1": 5733,
      "roma": "LEGNANO",
      "rm": "MI",
      "1234": 403015118
    },
    {
      "1": 5734,
      "roma": "LEGNARO",
      "rm": "PD",
      "1234": 405028044
    },
    {
      "1": 5735,
      "roma": "LEI",
      "rm": "NU",
      "1234": 420091038
    },
    {
      "1": 5736,
      "roma": "LEINI",
      "rm": "TO",
      "1234": 401001130
    },
    {
      "1": 5737,
      "roma": "LEIVI",
      "rm": "GE",
      "1234": 407010029
    },
    {
      "1": 5738,
      "roma": "LEMIE",
      "rm": "TO",
      "1234": 401001131
    },
    {
      "1": 5739,
      "roma": "LENDINARA",
      "rm": "RO",
      "1234": 405029029
    },
    {
      "1": 5740,
      "roma": "LENI",
      "rm": "ME",
      "1234": 419083037
    },
    {
      "1": 5741,
      "roma": "LAZISE",
      "rm": "VR",
      "1234": 405023043
    },
    {
      "1": 5742,
      "roma": "LAUREANA CILENTO",
      "rm": "SA",
      "1234": 415065060
    },
    {
      "1": 5743,
      "roma": "LECCO",
      "rm": "LC",
      "1234": 403097042
    },
    {
      "1": 5744,
      "roma": "LENTELLA",
      "rm": "CH",
      "1234": 413069047
    },
    {
      "1": 5745,
      "roma": "LENTINI",
      "rm": "SR",
      "1234": 419089011
    },
    {
      "1": 5746,
      "roma": "LEDRO",
      "rm": "TN",
      "1234": 404022229
    },
    {
      "1": 5747,
      "roma": "LEONESSA",
      "rm": "RI",
      "1234": 412057033
    },
    {
      "1": 5748,
      "roma": "LEONFORTE",
      "rm": "EN",
      "1234": 419086011
    },
    {
      "1": 5749,
      "roma": "LEPORANO",
      "rm": "TA",
      "1234": 416073010
    },
    {
      "1": 5750,
      "roma": "LEQUILE",
      "rm": "LE",
      "1234": 416075036
    },
    {
      "1": 5751,
      "roma": "LEQUIO BERRIA",
      "rm": "CN",
      "1234": 401004106
    },
    {
      "1": 5752,
      "roma": "LEQUIO TANARO",
      "rm": "CN",
      "1234": 401004107
    },
    {
      "1": 5753,
      "roma": "LERCARA FRIDDI",
      "rm": "PA",
      "1234": 419082045
    },
    {
      "1": 5754,
      "roma": "LERICI",
      "rm": "SP",
      "1234": 407011016
    },
    {
      "1": 5755,
      "roma": "LERMA",
      "rm": "AL",
      "1234": 401006088
    },
    {
      "1": 5756,
      "roma": "LESA",
      "rm": "NO",
      "1234": 401003084
    },
    {
      "1": 5757,
      "roma": "LESEGNO",
      "rm": "CN",
      "1234": 401004108
    },
    {
      "1": 5758,
      "roma": "LESIGNANO DE' BAGNI",
      "rm": "PR",
      "1234": 408034019
    },
    {
      "1": 5759,
      "roma": "LENNA",
      "rm": "BG",
      "1234": 403016125
    },
    {
      "1": 5760,
      "roma": "LENO",
      "rm": "BS",
      "1234": 403017088
    },
    {
      "1": 5761,
      "roma": "LECCE",
      "rm": "LE",
      "1234": 416075035
    },
    {
      "1": 5762,
      "roma": "LECCE NEI MARSI",
      "rm": "AQ",
      "1234": 413066050
    },
    {
      "1": 5763,
      "roma": "LESSOLO",
      "rm": "TO",
      "1234": 401001132
    },
    {
      "1": 5764,
      "roma": "LESTIZZA",
      "rm": "UD",
      "1234": 406030048
    },
    {
      "1": 5765,
      "roma": "LETINO",
      "rm": "CE",
      "1234": 415061044
    },
    {
      "1": 5766,
      "roma": "LETOJANNI",
      "rm": "ME",
      "1234": 419083038
    },
    {
      "1": 5767,
      "roma": "LETTERE",
      "rm": "NA",
      "1234": 415063039
    },
    {
      "1": 5768,
      "roma": "LETTOMANOPPELLO",
      "rm": "PE",
      "1234": 413068020
    },
    {
      "1": 5769,
      "roma": "LETTOPALENA",
      "rm": "CH",
      "1234": 413069048
    },
    {
      "1": 5770,
      "roma": "LESMO",
      "rm": "MB",
      "1234": 403108026
    },
    {
      "1": 5771,
      "roma": "LEVANTO",
      "rm": "SP",
      "1234": 407011017
    },
    {
      "1": 5772,
      "roma": "LEVATE",
      "rm": "BG",
      "1234": 403016126
    },
    {
      "1": 5773,
      "roma": "LEVERANO",
      "rm": "LE",
      "1234": 416075037
    },
    {
      "1": 5774,
      "roma": "LEVICE",
      "rm": "CN",
      "1234": 401004109
    },
    {
      "1": 5775,
      "roma": "LEVICO TERME",
      "rm": "TN",
      "1234": 404022104
    },
    {
      "1": 5776,
      "roma": "LEVONE",
      "rm": "TO",
      "1234": 401001133
    },
    {
      "1": 5777,
      "roma": "LENOLA",
      "rm": "LT",
      "1234": 412059012
    },
    {
      "1": 5778,
      "roma": "LENTA",
      "rm": "VC",
      "1234": 401002068
    },
    {
      "1": 5779,
      "roma": "LESINA",
      "rm": "FG",
      "1234": 416071027
    },
    {
      "1": 5780,
      "roma": "LIBRIZZI",
      "rm": "ME",
      "1234": 419083039
    },
    {
      "1": 5781,
      "roma": "LICATA",
      "rm": "AG",
      "1234": 419084021
    },
    {
      "1": 5782,
      "roma": "LICCIANA NARDI",
      "rm": "MS",
      "1234": 409045009
    },
    {
      "1": 6427,
      "roma": "MOZZATE",
      "rm": "CO",
      "1234": 403013159
    },
    {
      "1": 6428,
      "roma": "MOZZECANE",
      "rm": "VR",
      "1234": 405023051
    },
    {
      "1": 6429,
      "roma": "MUROS",
      "rm": "SS",
      "1234": 420090043
    },
    {
      "1": 6430,
      "roma": "MUSCOLINE",
      "rm": "BS",
      "1234": 403017116
    },
    {
      "1": 6431,
      "roma": "MUSEI",
      "rm": "CA",
      "1234": 420092040
    },
    {
      "1": 6432,
      "roma": "MUGGIO'",
      "rm": "MB",
      "1234": 403108034
    },
    {
      "1": 6433,
      "roma": "MULAZZANO",
      "rm": "LO",
      "1234": 403098041
    },
    {
      "1": 6434,
      "roma": "MULAZZO",
      "rm": "MS",
      "1234": 409045012
    },
    {
      "1": 6435,
      "roma": "MURA",
      "rm": "BS",
      "1234": 403017115
    },
    {
      "1": 6436,
      "roma": "MURAVERA",
      "rm": "CA",
      "1234": 420092039
    },
    {
      "1": 6437,
      "roma": "MOTTOLA",
      "rm": "TA",
      "1234": 416073019
    },
    {
      "1": 6438,
      "roma": "MURAZZANO",
      "rm": "CN",
      "1234": 401004145
    },
    {
      "1": 6439,
      "roma": "MURELLO",
      "rm": "CN",
      "1234": 401004146
    },
    {
      "1": 6440,
      "roma": "MURIALDO",
      "rm": "SV",
      "1234": 407009040
    },
    {
      "1": 6441,
      "roma": "MURISENGO",
      "rm": "AL",
      "1234": 401006113
    },
    {
      "1": 6442,
      "roma": "MURLO",
      "rm": "SI",
      "1234": 409052019
    },
    {
      "1": 6443,
      "roma": "MURO LECCESE",
      "rm": "LE",
      "1234": 416075051
    },
    {
      "1": 6444,
      "roma": "MURO LUCANO",
      "rm": "PZ",
      "1234": 417076053
    },
    {
      "1": 6445,
      "roma": "NARCAO",
      "rm": "CA",
      "1234": 420092041
    },
    {
      "1": 6446,
      "roma": "NARDO'",
      "rm": "LE",
      "1234": 416075052
    },
    {
      "1": 6447,
      "roma": "NARDODIPACE",
      "rm": "VV",
      "1234": 418102024
    },
    {
      "1": 6448,
      "roma": "NARNI",
      "rm": "TR",
      "1234": 410055022
    },
    {
      "1": 6449,
      "roma": "MUSILE DI PIAVE",
      "rm": "VE",
      "1234": 405027025
    },
    {
      "1": 6450,
      "roma": "MUSSO",
      "rm": "CO",
      "1234": 403013160
    },
    {
      "1": 6451,
      "roma": "MUSSOLENTE",
      "rm": "VI",
      "1234": 405024070
    },
    {
      "1": 6452,
      "roma": "MUSSOMELI",
      "rm": "CL",
      "1234": 419085012
    },
    {
      "1": 6453,
      "roma": "MUZZANA DEL TURGNANO",
      "rm": "UD",
      "1234": 406030064
    },
    {
      "1": 6454,
      "roma": "MUZZANO",
      "rm": "BI",
      "1234": 401096038
    },
    {
      "1": 6455,
      "roma": "MONTALCINO",
      "rm": "SI",
      "1234": 409052037
    },
    {
      "1": 6456,
      "roma": "NAGO-TORBOLE",
      "rm": "TN",
      "1234": 404022124
    },
    {
      "1": 6457,
      "roma": "NALLES",
      "rm": "BZ",
      "1234": 404021055
    },
    {
      "1": 6458,
      "roma": "NANTO",
      "rm": "VI",
      "1234": 405024071
    },
    {
      "1": 6459,
      "roma": "NAPOLI",
      "rm": "NA",
      "1234": 415063049
    },
    {
      "1": 6460,
      "roma": "NARBOLIA",
      "rm": "OR",
      "1234": 420095031
    },
    {
      "1": 6461,
      "roma": "NEIRONE",
      "rm": "GE",
      "1234": 407010041
    },
    {
      "1": 6462,
      "roma": "NEIVE",
      "rm": "CN",
      "1234": 401004148
    },
    {
      "1": 6463,
      "roma": "NEMBRO",
      "rm": "BG",
      "1234": 403016144
    },
    {
      "1": 6464,
      "roma": "NEMI",
      "rm": "RM",
      "1234": 412058070
    },
    {
      "1": 6465,
      "roma": "NEMOLI",
      "rm": "PZ",
      "1234": 417076054
    },
    {
      "1": 6466,
      "roma": "NEONELI",
      "rm": "OR",
      "1234": 420095032
    },
    {
      "1": 6467,
      "roma": "NARO",
      "rm": "AG",
      "1234": 419084026
    },
    {
      "1": 6468,
      "roma": "NARZOLE",
      "rm": "CN",
      "1234": 401004147
    },
    {
      "1": 6469,
      "roma": "NASINO",
      "rm": "SV",
      "1234": 407009041
    },
    {
      "1": 6470,
      "roma": "NASO",
      "rm": "ME",
      "1234": 419083060
    },
    {
      "1": 6471,
      "roma": "NATURNO",
      "rm": "BZ",
      "1234": 404021056
    },
    {
      "1": 6472,
      "roma": "NAVE",
      "rm": "BS",
      "1234": 403017117
    },
    {
      "1": 6473,
      "roma": "NAVELLI",
      "rm": "AQ",
      "1234": 413066058
    },
    {
      "1": 6474,
      "roma": "NAZ SCIAVES",
      "rm": "BZ",
      "1234": 404021057
    },
    {
      "1": 6475,
      "roma": "MONTORO",
      "rm": "AV",
      "1234": 415064121
    },
    {
      "1": 6476,
      "roma": "NAZZANO",
      "rm": "RM",
      "1234": 412058069
    },
    {
      "1": 6477,
      "roma": "NE",
      "rm": "GE",
      "1234": 407010040
    },
    {
      "1": 6478,
      "roma": "NEBBIUNO",
      "rm": "NO",
      "1234": 401003103
    },
    {
      "1": 6479,
      "roma": "NEVIGLIE",
      "rm": "CN",
      "1234": 401004149
    },
    {
      "1": 6480,
      "roma": "NIARDO",
      "rm": "BS",
      "1234": 403017118
    },
    {
      "1": 6481,
      "roma": "NIBBIOLA",
      "rm": "NO",
      "1234": 401003104
    },
    {
      "1": 6482,
      "roma": "NIBIONNO",
      "rm": "LC",
      "1234": 403097056
    },
    {
      "1": 6483,
      "roma": "NEPI",
      "rm": "VT",
      "1234": 412056039
    },
    {
      "1": 6484,
      "roma": "NERETO",
      "rm": "TE",
      "1234": 413067031
    },
    {
      "1": 6485,
      "roma": "NEROLA",
      "rm": "RM",
      "1234": 412058071
    },
    {
      "1": 6486,
      "roma": "NERVESA DELLA BATTAGLIA",
      "rm": "TV",
      "1234": 405026050
    },
    {
      "1": 6487,
      "roma": "NERVIANO",
      "rm": "MI",
      "1234": 403015154
    },
    {
      "1": 6488,
      "roma": "NESPOLO",
      "rm": "RI",
      "1234": 412057046
    },
    {
      "1": 6489,
      "roma": "NESSO",
      "rm": "CO",
      "1234": 403013161
    },
    {
      "1": 6490,
      "roma": "NETRO",
      "rm": "BI",
      "1234": 401096039
    },
    {
      "1": 6491,
      "roma": "NOVA MILANESE",
      "rm": "MB",
      "1234": 403108035
    },
    {
      "1": 6492,
      "roma": "NETTUNO",
      "rm": "RM",
      "1234": 412058072
    },
    {
      "1": 6493,
      "roma": "NEVIANO",
      "rm": "LE",
      "1234": 416075053
    },
    {
      "1": 6494,
      "roma": "NEVIANO DEGLI ARDUINI",
      "rm": "PR",
      "1234": 408034024
    },
    {
      "1": 6495,
      "roma": "NOASCA",
      "rm": "TO",
      "1234": 401001165
    },
    {
      "1": 6496,
      "roma": "NOCARA",
      "rm": "CS",
      "1234": 418078086
    },
    {
      "1": 6497,
      "roma": "NOCCIANO",
      "rm": "PE",
      "1234": 413068026
    },
    {
      "1": 6498,
      "roma": "NICHELINO",
      "rm": "TO",
      "1234": 401001164
    },
    {
      "1": 6499,
      "roma": "NICOLOSI",
      "rm": "CT",
      "1234": 419087031
    },
    {
      "1": 6500,
      "roma": "NICORVO",
      "rm": "PV",
      "1234": 403018103
    },
    {
      "1": 6501,
      "roma": "NICOSIA",
      "rm": "EN",
      "1234": 419086012
    },
    {
      "1": 6502,
      "roma": "NICOTERA",
      "rm": "VV",
      "1234": 418102025
    },
    {
      "1": 6503,
      "roma": "NIELLA BELBO",
      "rm": "CN",
      "1234": 401004150
    },
    {
      "1": 6504,
      "roma": "NIELLA TANARO",
      "rm": "CN",
      "1234": 401004151
    },
    {
      "1": 6505,
      "roma": "NOGAREDO",
      "rm": "TN",
      "1234": 404022127
    },
    {
      "1": 6506,
      "roma": "NIMIS",
      "rm": "UD",
      "1234": 406030065
    },
    {
      "1": 6507,
      "roma": "NISCEMI",
      "rm": "CL",
      "1234": 419085013
    },
    {
      "1": 6508,
      "roma": "NISSORIA",
      "rm": "EN",
      "1234": 419086013
    },
    {
      "1": 6509,
      "roma": "NIZZA DI SICILIA",
      "rm": "ME",
      "1234": 419083061
    },
    {
      "1": 6510,
      "roma": "NIZZA MONFERRATO",
      "rm": "AT",
      "1234": 401005080
    },
    {
      "1": 6511,
      "roma": "NOALE",
      "rm": "VE",
      "1234": 405027026
    },
    {
      "1": 6512,
      "roma": "NOMI",
      "rm": "TN",
      "1234": 404022128
    },
    {
      "1": 6513,
      "roma": "NONANTOLA",
      "rm": "MO",
      "1234": 408036027
    },
    {
      "1": 6514,
      "roma": "NONE",
      "rm": "TO",
      "1234": 401001168
    },
    {
      "1": 6515,
      "roma": "NONIO",
      "rm": "VB",
      "1234": 401103048
    },
    {
      "1": 6516,
      "roma": "NOCERA INFERIORE",
      "rm": "SA",
      "1234": 415065078
    },
    {
      "1": 6517,
      "roma": "NOCERA SUPERIORE",
      "rm": "SA",
      "1234": 415065079
    },
    {
      "1": 6518,
      "roma": "NOCERA TERINESE",
      "rm": "CZ",
      "1234": 418079087
    },
    {
      "1": 6519,
      "roma": "NOCERA UMBRA",
      "rm": "PG",
      "1234": 410054034
    },
    {
      "1": 6520,
      "roma": "NOCETO",
      "rm": "PR",
      "1234": 408034025
    },
    {
      "1": 6521,
      "roma": "NOCI",
      "rm": "BA",
      "1234": 416072031
    },
    {
      "1": 6522,
      "roma": "NOCIGLIA",
      "rm": "LE",
      "1234": 416075054
    },
    {
      "1": 6523,
      "roma": "NOEPOLI",
      "rm": "PZ",
      "1234": 417076055
    },
    {
      "1": 6524,
      "roma": "NOGARA",
      "rm": "VR",
      "1234": 405023053
    },
    {
      "1": 6525,
      "roma": "NOSATE",
      "rm": "MI",
      "1234": 403015155
    },
    {
      "1": 6526,
      "roma": "NOGAROLE ROCCA",
      "rm": "VR",
      "1234": 405023054
    },
    {
      "1": 6527,
      "roma": "NOGAROLE VICENTINO",
      "rm": "VI",
      "1234": 405024072
    },
    {
      "1": 6528,
      "roma": "NOICATTARO",
      "rm": "BA",
      "1234": 416072032
    },
    {
      "1": 6529,
      "roma": "NOLA",
      "rm": "NA",
      "1234": 415063050
    },
    {
      "1": 6530,
      "roma": "NOLE",
      "rm": "TO",
      "1234": 401001166
    },
    {
      "1": 6531,
      "roma": "NOLI",
      "rm": "SV",
      "1234": 407009042
    },
    {
      "1": 6532,
      "roma": "NOMAGLIO",
      "rm": "TO",
      "1234": 401001167
    },
    {
      "1": 6533,
      "roma": "MONTESCUDO-MONTE COLOMBO",
      "rm": "RN",
      "1234": 408099029
    },
    {
      "1": 6534,
      "roma": "NOVAFELTRIA",
      "rm": "RN",
      "1234": 408099023
    },
    {
      "1": 6535,
      "roma": "NOVALESA",
      "rm": "TO",
      "1234": 401001169
    },
    {
      "1": 6536,
      "roma": "NOVARA",
      "rm": "NO",
      "1234": 401003106
    },
    {
      "1": 6537,
      "roma": "NORAGUGUME",
      "rm": "NU",
      "1234": 420091050
    },
    {
      "1": 6538,
      "roma": "NORBELLO",
      "rm": "OR",
      "1234": 420095033
    },
    {
      "1": 6539,
      "roma": "NORCIA",
      "rm": "PG",
      "1234": 410054035
    },
    {
      "1": 6540,
      "roma": "NORMA",
      "rm": "LT",
      "1234": 412059016
    },
    {
      "1": 6541,
      "roma": "NOVENTA DI PIAVE",
      "rm": "VE",
      "1234": 405027027
    },
    {
      "1": 6542,
      "roma": "NOVENTA PADOVANA",
      "rm": "PD",
      "1234": 405028058
    },
    {
      "1": 6543,
      "roma": "NOVENTA VICENTINA",
      "rm": "VI",
      "1234": 405024074
    },
    {
      "1": 6544,
      "roma": "NOTARESCO",
      "rm": "TE",
      "1234": 413067032
    },
    {
      "1": 6545,
      "roma": "NOTO",
      "rm": "SR",
      "1234": 419089013
    },
    {
      "1": 6546,
      "roma": "NOVA LEVANTE",
      "rm": "BZ",
      "1234": 404021058
    },
    {
      "1": 6547,
      "roma": "NOVA PONENTE",
      "rm": "BZ",
      "1234": 404021059
    },
    {
      "1": 6548,
      "roma": "NOVA SIRI",
      "rm": "MT",
      "1234": 417077018
    },
    {
      "1": 6549,
      "roma": "NOVALEDO",
      "rm": "TN",
      "1234": 404022129
    },
    {
      "1": 6550,
      "roma": "NUGHEDU DI SAN NICOLO'",
      "rm": "SS",
      "1234": 420090044
    },
    {
      "1": 6551,
      "roma": "NUGHEDU SANTA VITTORIA",
      "rm": "OR",
      "1234": 420095034
    },
    {
      "1": 6552,
      "roma": "NOVARA DI SICILIA",
      "rm": "ME",
      "1234": 419083062
    },
    {
      "1": 6553,
      "roma": "NOVATE MEZZOLA",
      "rm": "SO",
      "1234": 403014046
    },
    {
      "1": 6554,
      "roma": "NOVATE MILANESE",
      "rm": "MI",
      "1234": 403015157
    },
    {
      "1": 6555,
      "roma": "NOVE",
      "rm": "VI",
      "1234": 405024073
    },
    {
      "1": 6556,
      "roma": "NOVEDRATE",
      "rm": "CO",
      "1234": 403013163
    },
    {
      "1": 6557,
      "roma": "NOVELLARA",
      "rm": "RE",
      "1234": 408035028
    },
    {
      "1": 6558,
      "roma": "NOVELLO",
      "rm": "CN",
      "1234": 401004152
    },
    {
      "1": 6559,
      "roma": "NURAGUS",
      "rm": "NU",
      "1234": 420091052
    },
    {
      "1": 6560,
      "roma": "NURALLAO",
      "rm": "NU",
      "1234": 420091053
    },
    {
      "1": 6561,
      "roma": "NURAMINIS",
      "rm": "CA",
      "1234": 420092042
    },
    {
      "1": 6562,
      "roma": "NOVI DI MODENA",
      "rm": "MO",
      "1234": 408036028
    },
    {
      "1": 6563,
      "roma": "NOVI LIGURE",
      "rm": "AL",
      "1234": 401006114
    },
    {
      "1": 6564,
      "roma": "NOVI VELIA",
      "rm": "SA",
      "1234": 415065080
    },
    {
      "1": 6565,
      "roma": "NOVIGLIO",
      "rm": "MI",
      "1234": 403015158
    },
    {
      "1": 6566,
      "roma": "NOVOLI",
      "rm": "LE",
      "1234": 416075055
    },
    {
      "1": 6567,
      "roma": "NUCETTO",
      "rm": "CN",
      "1234": 401004153
    },
    {
      "1": 6568,
      "roma": "NUXIS",
      "rm": "CA",
      "1234": 420092043
    },
    {
      "1": 6569,
      "roma": "NURRI",
      "rm": "CA",
      "1234": 420092117
    },
    {
      "1": 6570,
      "roma": "NULE",
      "rm": "SS",
      "1234": 420090045
    },
    {
      "1": 6571,
      "roma": "NULVI",
      "rm": "SS",
      "1234": 420090046
    },
    {
      "1": 6572,
      "roma": "NUMANA",
      "rm": "AN",
      "1234": 411042032
    },
    {
      "1": 6573,
      "roma": "NUORO",
      "rm": "NU",
      "1234": 420091051
    },
    {
      "1": 6574,
      "roma": "NURACHI",
      "rm": "OR",
      "1234": 420095035
    },
    {
      "1": 6575,
      "roma": "OFFAGNA",
      "rm": "AN",
      "1234": 411042033
    },
    {
      "1": 6576,
      "roma": "OFFANENGO",
      "rm": "CR",
      "1234": 403019062
    },
    {
      "1": 6577,
      "roma": "OFFIDA",
      "rm": "AP",
      "1234": 411044054
    },
    {
      "1": 6578,
      "roma": "NURECI",
      "rm": "OR",
      "1234": 420095036
    },
    {
      "1": 6579,
      "roma": "NUS",
      "rm": "AO",
      "1234": 402007045
    },
    {
      "1": 6580,
      "roma": "NUSCO",
      "rm": "AV",
      "1234": 415064066
    },
    {
      "1": 6581,
      "roma": "NUVOLENTO",
      "rm": "BS",
      "1234": 403017119
    },
    {
      "1": 6582,
      "roma": "NUVOLERA",
      "rm": "BS",
      "1234": 403017120
    },
    {
      "1": 6583,
      "roma": "OLBIA",
      "rm": "SS",
      "1234": 420090047
    },
    {
      "1": 6584,
      "roma": "OCCHIEPPO INFERIORE",
      "rm": "BI",
      "1234": 401096040
    },
    {
      "1": 6585,
      "roma": "OCCHIEPPO SUPERIORE",
      "rm": "BI",
      "1234": 401096041
    },
    {
      "1": 6586,
      "roma": "OCCHIOBELLO",
      "rm": "RO",
      "1234": 405029033
    },
    {
      "1": 6587,
      "roma": "OCCIMIANO",
      "rm": "AL",
      "1234": 401006115
    },
    {
      "1": 6588,
      "roma": "OCRE",
      "rm": "AQ",
      "1234": 413066059
    },
    {
      "1": 6589,
      "roma": "ODALENGO GRANDE",
      "rm": "AL",
      "1234": 401006116
    },
    {
      "1": 6590,
      "roma": "ODALENGO PICCOLO",
      "rm": "AL",
      "1234": 401006117
    },
    {
      "1": 6591,
      "roma": "ODERZO",
      "rm": "TV",
      "1234": 405026051
    },
    {
      "1": 6592,
      "roma": "ODOLO",
      "rm": "BS",
      "1234": 403017121
    },
    {
      "1": 6593,
      "roma": "OFENA",
      "rm": "AQ",
      "1234": 413066060
    },
    {
      "1": 6594,
      "roma": "OLGIATE OLONA",
      "rm": "VA",
      "1234": 403012108
    },
    {
      "1": 6595,
      "roma": "OLGINATE",
      "rm": "LC",
      "1234": 403097059
    },
    {
      "1": 6596,
      "roma": "OFFLAGA",
      "rm": "BS",
      "1234": 403017122
    },
    {
      "1": 6597,
      "roma": "OGGEBBIO",
      "rm": "VB",
      "1234": 401103049
    },
    {
      "1": 6598,
      "roma": "OGGIONA CON SANTO STEFANO",
      "rm": "VA",
      "1234": 403012107
    },
    {
      "1": 6599,
      "roma": "OGGIONO",
      "rm": "LC",
      "1234": 403097057
    },
    {
      "1": 6600,
      "roma": "OGLIANICO",
      "rm": "TO",
      "1234": 401001170
    },
    {
      "1": 6601,
      "roma": "OGLIASTRO CILENTO",
      "rm": "SA",
      "1234": 415065081
    },
    {
      "1": 6602,
      "roma": "OLCENENGO",
      "rm": "VC",
      "1234": 401002088
    },
    {
      "1": 6603,
      "roma": "OLDENICO",
      "rm": "VC",
      "1234": 401002089
    },
    {
      "1": 6604,
      "roma": "OLEGGIO",
      "rm": "NO",
      "1234": 401003108
    },
    {
      "1": 6605,
      "roma": "OLEGGIO CASTELLO",
      "rm": "NO",
      "1234": 401003109
    },
    {
      "1": 6606,
      "roma": "OLEVANO DI LOMELLINA",
      "rm": "PV",
      "1234": 403018104
    },
    {
      "1": 6607,
      "roma": "OLEVANO ROMANO",
      "rm": "RM",
      "1234": 412058073
    },
    {
      "1": 6608,
      "roma": "OLEVANO SUL TUSCIANO",
      "rm": "SA",
      "1234": 415065082
    },
    {
      "1": 6609,
      "roma": "OLGIATE COMASCO",
      "rm": "CO",
      "1234": 403013165
    },
    {
      "1": 6610,
      "roma": "OLGIATE MOLGORA",
      "rm": "LC",
      "1234": 403097058
    },
    {
      "1": 6611,
      "roma": "OLTRESSENDA ALTA",
      "rm": "BG",
      "1234": 403016147
    },
    {
      "1": 6612,
      "roma": "OLIENA",
      "rm": "NU",
      "1234": 420091055
    },
    {
      "1": 6613,
      "roma": "OLIVA GESSI",
      "rm": "PV",
      "1234": 403018105
    },
    {
      "1": 6614,
      "roma": "OLIVADI",
      "rm": "CZ",
      "1234": 418079088
    },
    {
      "1": 6615,
      "roma": "OLIVERI",
      "rm": "ME",
      "1234": 419083063
    },
    {
      "1": 6616,
      "roma": "OLIVETO CITRA",
      "rm": "SA",
      "1234": 415065083
    },
    {
      "1": 6617,
      "roma": "OLIVETO LARIO",
      "rm": "LC",
      "1234": 403097060
    },
    {
      "1": 6618,
      "roma": "OMIGNANO",
      "rm": "SA",
      "1234": 415065084
    },
    {
      "1": 6619,
      "roma": "OLIVETO LUCANO",
      "rm": "MT",
      "1234": 417077019
    },
    {
      "1": 6620,
      "roma": "OLIVETTA SAN MICHELE",
      "rm": "IM",
      "1234": 407008038
    },
    {
      "1": 6621,
      "roma": "OLIVOLA",
      "rm": "AL",
      "1234": 401006118
    },
    {
      "1": 6622,
      "roma": "OLLASTRA SIMAXIS",
      "rm": "OR",
      "1234": 420095037
    },
    {
      "1": 6623,
      "roma": "OLLOLAI",
      "rm": "NU",
      "1234": 420091056
    },
    {
      "1": 6624,
      "roma": "OLLOMONT",
      "rm": "AO",
      "1234": 402007046
    },
    {
      "1": 6625,
      "roma": "OLMEDO",
      "rm": "SS",
      "1234": 420090048
    },
    {
      "1": 6626,
      "roma": "OLMENETA",
      "rm": "CR",
      "1234": 403019063
    },
    {
      "1": 6627,
      "roma": "OLMO AL BREMBO",
      "rm": "BG",
      "1234": 403016145
    },
    {
      "1": 6628,
      "roma": "OLMO GENTILE",
      "rm": "AT",
      "1234": 401005081
    },
    {
      "1": 6629,
      "roma": "OLTRE IL COLLE",
      "rm": "BG",
      "1234": 403016146
    },
    {
      "1": 6630,
      "roma": "ONZO",
      "rm": "SV",
      "1234": 407009043
    },
    {
      "1": 6631,
      "roma": "OPERA",
      "rm": "MI",
      "1234": 403015159
    },
    {
      "1": 6632,
      "roma": "OPI",
      "rm": "AQ",
      "1234": 413066061
    },
    {
      "1": 6633,
      "roma": "OLTRONA DI SAN MAMETTE",
      "rm": "CO",
      "1234": 403013169
    },
    {
      "1": 6634,
      "roma": "OLZAI",
      "rm": "NU",
      "1234": 420091057
    },
    {
      "1": 6635,
      "roma": "OME",
      "rm": "BS",
      "1234": 403017123
    },
    {
      "1": 6636,
      "roma": "OMEGNA",
      "rm": "VB",
      "1234": 401103050
    },
    {
      "1": 6637,
      "roma": "ONANI",
      "rm": "NU",
      "1234": 420091058
    },
    {
      "1": 6638,
      "roma": "ONANO",
      "rm": "VT",
      "1234": 412056040
    },
    {
      "1": 6639,
      "roma": "ONCINO",
      "rm": "CN",
      "1234": 401004154
    },
    {
      "1": 6640,
      "roma": "ONETA",
      "rm": "BG",
      "1234": 403016148
    },
    {
      "1": 6641,
      "roma": "ONIFAI",
      "rm": "NU",
      "1234": 420091059
    },
    {
      "1": 6642,
      "roma": "ONIFERI",
      "rm": "NU",
      "1234": 420091060
    },
    {
      "1": 6643,
      "roma": "ONO SAN PIETRO",
      "rm": "BS",
      "1234": 403017124
    },
    {
      "1": 6644,
      "roma": "ONORE",
      "rm": "BG",
      "1234": 403016149
    },
    {
      "1": 6645,
      "roma": "ORICOLA",
      "rm": "AQ",
      "1234": 413066062
    },
    {
      "1": 5137,
      "roma": "DUSINO SAN MICHELE",
      "rm": "AT",
      "1234": 401005052
    },
    {
      "1": 5138,
      "roma": "EBOLI",
      "rm": "SA",
      "1234": 415065050
    },
    {
      "1": 5139,
      "roma": "EDOLO",
      "rm": "BS",
      "1234": 403017068
    },
    {
      "1": 5140,
      "roma": "DRUOGNO",
      "rm": "VB",
      "1234": 401103029
    },
    {
      "1": 5141,
      "roma": "ELICE",
      "rm": "PE",
      "1234": 413068018
    },
    {
      "1": 5142,
      "roma": "ELINI",
      "rm": "NU",
      "1234": 420091019
    },
    {
      "1": 5143,
      "roma": "ELLO",
      "rm": "LC",
      "1234": 403097033
    },
    {
      "1": 5144,
      "roma": "DUINO AURISINA",
      "rm": "TS",
      "1234": 406032001
    },
    {
      "1": 5145,
      "roma": "DUMENZA",
      "rm": "VA",
      "1234": 403012065
    },
    {
      "1": 5146,
      "roma": "DUNO",
      "rm": "VA",
      "1234": 403012066
    },
    {
      "1": 5147,
      "roma": "DURAZZANO",
      "rm": "BN",
      "1234": 415062028
    },
    {
      "1": 5148,
      "roma": "DURONIA",
      "rm": "CB",
      "1234": 414070022
    },
    {
      "1": 5149,
      "roma": "ENDINE GAIANO",
      "rm": "BG",
      "1234": 403016093
    },
    {
      "1": 5150,
      "roma": "ENEGO",
      "rm": "VI",
      "1234": 405024039
    },
    {
      "1": 5151,
      "roma": "ENEMONZO",
      "rm": "UD",
      "1234": 406030035
    },
    {
      "1": 5152,
      "roma": "ENNA",
      "rm": "EN",
      "1234": 419086009
    },
    {
      "1": 5153,
      "roma": "ENTRACQUE",
      "rm": "CN",
      "1234": 401004084
    },
    {
      "1": 5154,
      "roma": "EGNA",
      "rm": "BZ",
      "1234": 404021029
    },
    {
      "1": 5155,
      "roma": "EPISCOPIA",
      "rm": "PZ",
      "1234": 417076030
    },
    {
      "1": 5156,
      "roma": "ERACLEA",
      "rm": "VE",
      "1234": 405027013
    },
    {
      "1": 5157,
      "roma": "ERBA",
      "rm": "CO",
      "1234": 403013095
    },
    {
      "1": 5158,
      "roma": "ERBE'",
      "rm": "VR",
      "1234": 405023032
    },
    {
      "1": 5159,
      "roma": "ERBEZZO",
      "rm": "VR",
      "1234": 405023033
    },
    {
      "1": 5160,
      "roma": "ELMAS",
      "rm": "CA",
      "1234": 420092108
    },
    {
      "1": 5161,
      "roma": "ELVA",
      "rm": "CN",
      "1234": 401004083
    },
    {
      "1": 5162,
      "roma": "EMARESE",
      "rm": "AO",
      "1234": 402007025
    },
    {
      "1": 5163,
      "roma": "EMPOLI",
      "rm": "FI",
      "1234": 409048014
    },
    {
      "1": 5164,
      "roma": "ERTO E CASSO",
      "rm": "PN",
      "1234": 406093019
    },
    {
      "1": 5165,
      "roma": "ERULA",
      "rm": "SS",
      "1234": 420090088
    },
    {
      "1": 5166,
      "roma": "ERVE",
      "rm": "LC",
      "1234": 403097034
    },
    {
      "1": 5167,
      "roma": "ESANATOGLIA",
      "rm": "MC",
      "1234": 411043016
    },
    {
      "1": 5168,
      "roma": "ESCALAPLANO",
      "rm": "NU",
      "1234": 420091020
    },
    {
      "1": 5169,
      "roma": "ESCOLCA",
      "rm": "NU",
      "1234": 420091021
    },
    {
      "1": 5170,
      "roma": "ENTRATICO",
      "rm": "BG",
      "1234": 403016094
    },
    {
      "1": 5171,
      "roma": "ENVIE",
      "rm": "CN",
      "1234": 401004085
    },
    {
      "1": 5172,
      "roma": "ESINO LARIO",
      "rm": "LC",
      "1234": 403097035
    },
    {
      "1": 5173,
      "roma": "ESPERIA",
      "rm": "FR",
      "1234": 412060031
    },
    {
      "1": 5174,
      "roma": "ERBUSCO",
      "rm": "BS",
      "1234": 403017069
    },
    {
      "1": 5175,
      "roma": "ERCHIE",
      "rm": "BR",
      "1234": 416074006
    },
    {
      "1": 5176,
      "roma": "ERCOLANO",
      "rm": "NA",
      "1234": 415063064
    },
    {
      "1": 5177,
      "roma": "ERICE",
      "rm": "TP",
      "1234": 419081008
    },
    {
      "1": 5178,
      "roma": "ERLI",
      "rm": "SV",
      "1234": 407009028
    },
    {
      "1": 5179,
      "roma": "EUPILIO",
      "rm": "CO",
      "1234": 403013097
    },
    {
      "1": 5180,
      "roma": "EXILLES",
      "rm": "TO",
      "1234": 401001100
    },
    {
      "1": 5181,
      "roma": "FABBRICA CURONE",
      "rm": "AL",
      "1234": 401006067
    },
    {
      "1": 5182,
      "roma": "FABBRICO",
      "rm": "RE",
      "1234": 408035021
    },
    {
      "1": 5183,
      "roma": "ESINE",
      "rm": "BS",
      "1234": 403017070
    },
    {
      "1": 5184,
      "roma": "FABRIZIA",
      "rm": "VV",
      "1234": 418102010
    },
    {
      "1": 5185,
      "roma": "FABRO",
      "rm": "TR",
      "1234": 410055011
    },
    {
      "1": 5186,
      "roma": "FAEDIS",
      "rm": "UD",
      "1234": 406030036
    },
    {
      "1": 5187,
      "roma": "FAEDO",
      "rm": "TN",
      "1234": 404022080
    },
    {
      "1": 5188,
      "roma": "FAEDO VALTELLINO",
      "rm": "SO",
      "1234": 403014028
    },
    {
      "1": 5189,
      "roma": "FAENZA",
      "rm": "RA",
      "1234": 408039010
    },
    {
      "1": 5190,
      "roma": "ESPORLATU",
      "rm": "SS",
      "1234": 420090028
    },
    {
      "1": 5191,
      "roma": "ESTE",
      "rm": "PD",
      "1234": 405028037
    },
    {
      "1": 5192,
      "roma": "ESTERZILI",
      "rm": "NU",
      "1234": 420091022
    },
    {
      "1": 5193,
      "roma": "ETROUBLES",
      "rm": "AO",
      "1234": 402007026
    },
    {
      "1": 5194,
      "roma": "FAGNANO CASTELLO",
      "rm": "CS",
      "1234": 418078051
    },
    {
      "1": 5195,
      "roma": "FAGNANO OLONA",
      "rm": "VA",
      "1234": 403012067
    },
    {
      "1": 5196,
      "roma": "FAI DELLA PAGANELLA",
      "rm": "TN",
      "1234": 404022081
    },
    {
      "1": 5197,
      "roma": "FAICCHIO",
      "rm": "BN",
      "1234": 415062029
    },
    {
      "1": 5198,
      "roma": "FABBRICHE DI VERGEMOLI",
      "rm": "LU",
      "1234": 409046036
    },
    {
      "1": 5199,
      "roma": "FALCADE",
      "rm": "BL",
      "1234": 405025019
    },
    {
      "1": 5200,
      "roma": "FALCIANO DEL MASSICO",
      "rm": "CE",
      "1234": 415061101
    },
    {
      "1": 5201,
      "roma": "FALCONARA ALBANESE",
      "rm": "CS",
      "1234": 418078052
    },
    {
      "1": 5202,
      "roma": "FABRIANO",
      "rm": "AN",
      "1234": 411042017
    },
    {
      "1": 5203,
      "roma": "FABRICA DI ROMA",
      "rm": "VT",
      "1234": 412056024
    },
    {
      "1": 5204,
      "roma": "FALERNA",
      "rm": "CZ",
      "1234": 418079047
    },
    {
      "1": 5205,
      "roma": "FALLO",
      "rm": "CH",
      "1234": 413069104
    },
    {
      "1": 5206,
      "roma": "FAETO",
      "rm": "FG",
      "1234": 416071023
    },
    {
      "1": 5207,
      "roma": "FAGAGNA",
      "rm": "UD",
      "1234": 406030037
    },
    {
      "1": 5208,
      "roma": "FAGGETO LARIO",
      "rm": "CO",
      "1234": 403013098
    },
    {
      "1": 5209,
      "roma": "FAGGIANO",
      "rm": "TA",
      "1234": 416073005
    },
    {
      "1": 5210,
      "roma": "FAGNANO ALTO",
      "rm": "AQ",
      "1234": 413066042
    },
    {
      "1": 5211,
      "roma": "FANO",
      "rm": "PS",
      "1234": 411041013
    },
    {
      "1": 5212,
      "roma": "FANO ADRIANO",
      "rm": "TE",
      "1234": 413067024
    },
    {
      "1": 5213,
      "roma": "FARA FILIORUM PETRI",
      "rm": "CH",
      "1234": 413069030
    },
    {
      "1": 5214,
      "roma": "FARA GERA D'ADDA",
      "rm": "BG",
      "1234": 403016096
    },
    {
      "1": 5215,
      "roma": "FARA IN SABINA",
      "rm": "RI",
      "1234": 412057027
    },
    {
      "1": 5216,
      "roma": "FARA NOVARESE",
      "rm": "NO",
      "1234": 401003065
    },
    {
      "1": 5217,
      "roma": "FALERONE",
      "rm": "FM",
      "1234": 411109005
    },
    {
      "1": 5218,
      "roma": "FALCONARA MARITTIMA",
      "rm": "AN",
      "1234": 411042018
    },
    {
      "1": 5219,
      "roma": "FALCONE",
      "rm": "ME",
      "1234": 419083019
    },
    {
      "1": 5220,
      "roma": "FALERIA",
      "rm": "VT",
      "1234": 412056025
    },
    {
      "1": 5221,
      "roma": "FARDELLA",
      "rm": "PZ",
      "1234": 417076031
    },
    {
      "1": 5222,
      "roma": "FARIGLIANO",
      "rm": "CN",
      "1234": 401004086
    },
    {
      "1": 5223,
      "roma": "FARINDOLA",
      "rm": "PE",
      "1234": 413068019
    },
    {
      "1": 5224,
      "roma": "FARINI",
      "rm": "PC",
      "1234": 408033019
    },
    {
      "1": 5225,
      "roma": "FARNESE",
      "rm": "VT",
      "1234": 412056026
    },
    {
      "1": 5226,
      "roma": "FALOPPIO",
      "rm": "CO",
      "1234": 403013099
    },
    {
      "1": 5227,
      "roma": "FALVATERRA",
      "rm": "FR",
      "1234": 412060032
    },
    {
      "1": 5228,
      "roma": "FALZES",
      "rm": "BZ",
      "1234": 404021030
    },
    {
      "1": 5229,
      "roma": "FANANO",
      "rm": "MO",
      "1234": 408036011
    },
    {
      "1": 5230,
      "roma": "FANNA",
      "rm": "PN",
      "1234": 406093020
    },
    {
      "1": 5231,
      "roma": "FAULE",
      "rm": "CN",
      "1234": 401004087
    },
    {
      "1": 5232,
      "roma": "FAVALE DI MALVARO",
      "rm": "GE",
      "1234": 407010023
    },
    {
      "1": 5233,
      "roma": "FAVARA",
      "rm": "AG",
      "1234": 419084017
    },
    {
      "1": 5234,
      "roma": "FAVIGNANA",
      "rm": "TP",
      "1234": 419081009
    },
    {
      "1": 5235,
      "roma": "FAVRIA",
      "rm": "TO",
      "1234": 401001101
    },
    {
      "1": 5236,
      "roma": "FARA OLIVANA CON SOLA",
      "rm": "BG",
      "1234": 403016097
    },
    {
      "1": 5237,
      "roma": "FARA SAN MARTINO",
      "rm": "CH",
      "1234": 413069031
    },
    {
      "1": 5238,
      "roma": "FARA VICENTINO",
      "rm": "VI",
      "1234": 405024040
    },
    {
      "1": 5239,
      "roma": "FELINO",
      "rm": "PR",
      "1234": 408034013
    },
    {
      "1": 5240,
      "roma": "FELITTO",
      "rm": "SA",
      "1234": 415065051
    },
    {
      "1": 5241,
      "roma": "FELIZZANO",
      "rm": "AL",
      "1234": 401006068
    },
    {
      "1": 5242,
      "roma": "FELTRE",
      "rm": "BL",
      "1234": 405025021
    },
    {
      "1": 5243,
      "roma": "FENEGRO'",
      "rm": "CO",
      "1234": 403013100
    },
    {
      "1": 5244,
      "roma": "FARRA D'ISONZO",
      "rm": "GO",
      "1234": 406031005
    },
    {
      "1": 5245,
      "roma": "FARRA DI SOLIGO",
      "rm": "TV",
      "1234": 405026026
    },
    {
      "1": 5246,
      "roma": "FASANO",
      "rm": "BR",
      "1234": 416074007
    },
    {
      "1": 5247,
      "roma": "FASCIA",
      "rm": "GE",
      "1234": 407010022
    },
    {
      "1": 5248,
      "roma": "FAUGLIA",
      "rm": "PI",
      "1234": 409050014
    },
    {
      "1": 5249,
      "roma": "FERENTILLO",
      "rm": "TR",
      "1234": 410055012
    },
    {
      "1": 5250,
      "roma": "FERENTINO",
      "rm": "FR",
      "1234": 412060033
    },
    {
      "1": 5251,
      "roma": "FERLA",
      "rm": "SR",
      "1234": 419089008
    },
    {
      "1": 5252,
      "roma": "FERMIGNANO",
      "rm": "PS",
      "1234": 411041014
    },
    {
      "1": 5253,
      "roma": "FERNO",
      "rm": "VA",
      "1234": 403012068
    },
    {
      "1": 5254,
      "roma": "FEROLETO ANTICO",
      "rm": "CZ",
      "1234": 418079048
    },
    {
      "1": 5255,
      "roma": "FEROLETO DELLA CHIESA",
      "rm": "RC",
      "1234": 418080032
    },
    {
      "1": 5256,
      "roma": "FERRANDINA",
      "rm": "MT",
      "1234": 417077008
    },
    {
      "1": 5257,
      "roma": "FERRARA",
      "rm": "FE",
      "1234": 408038008
    },
    {
      "1": 5258,
      "roma": "FEISOGLIO",
      "rm": "CN",
      "1234": 401004088
    },
    {
      "1": 5259,
      "roma": "FELETTO",
      "rm": "TO",
      "1234": 401001102
    },
    {
      "1": 5260,
      "roma": "FERRAZZANO",
      "rm": "CB",
      "1234": 414070023
    },
    {
      "1": 5261,
      "roma": "FERRERA DI VARESE",
      "rm": "VA",
      "1234": 403012069
    },
    {
      "1": 5262,
      "roma": "FERRERA ERBOGNONE",
      "rm": "PV",
      "1234": 403018062
    },
    {
      "1": 5263,
      "roma": "FERRERE",
      "rm": "AT",
      "1234": 401005053
    },
    {
      "1": 5264,
      "roma": "FERRIERE",
      "rm": "PC",
      "1234": 408033020
    },
    {
      "1": 5265,
      "roma": "FENESTRELLE",
      "rm": "TO",
      "1234": 401001103
    },
    {
      "1": 5266,
      "roma": "FENIS",
      "rm": "AO",
      "1234": 402007027
    },
    {
      "1": 5267,
      "roma": "FIAVE'",
      "rm": "TN",
      "1234": 404022083
    },
    {
      "1": 5268,
      "roma": "FICARAZZI",
      "rm": "PA",
      "1234": 419082035
    },
    {
      "1": 5269,
      "roma": "FICAROLO",
      "rm": "RO",
      "1234": 405029021
    },
    {
      "1": 5270,
      "roma": "FICARRA",
      "rm": "ME",
      "1234": 419083020
    },
    {
      "1": 5271,
      "roma": "FICULLE",
      "rm": "TR",
      "1234": 410055013
    },
    {
      "1": 5272,
      "roma": "FIDENZA",
      "rm": "PR",
      "1234": 408034014
    },
    {
      "1": 5273,
      "roma": "FIE' ALLO SCILIAR",
      "rm": "BZ",
      "1234": 404021031
    },
    {
      "1": 5274,
      "roma": "FIEROZZO",
      "rm": "TN",
      "1234": 404022085
    },
    {
      "1": 5275,
      "roma": "FIESCO",
      "rm": "CR",
      "1234": 403019043
    },
    {
      "1": 5276,
      "roma": "FIESOLE",
      "rm": "FI",
      "1234": 409048015
    },
    {
      "1": 5277,
      "roma": "FERRARA DI MONTE BALDO",
      "rm": "VR",
      "1234": 405023034
    },
    {
      "1": 5278,
      "roma": "FIGINO SERENZA",
      "rm": "CO",
      "1234": 403013101
    },
    {
      "1": 5279,
      "roma": "FIGLINE VEGLIATURO",
      "rm": "CS",
      "1234": 418078053
    },
    {
      "1": 5280,
      "roma": "FERRUZZANO",
      "rm": "RC",
      "1234": 418080033
    },
    {
      "1": 5281,
      "roma": "FIAMIGNANO",
      "rm": "RI",
      "1234": 412057028
    },
    {
      "1": 5282,
      "roma": "FIANO",
      "rm": "TO",
      "1234": 401001104
    },
    {
      "1": 5283,
      "roma": "FIANO ROMANO",
      "rm": "RM",
      "1234": 412058036
    },
    {
      "1": 5284,
      "roma": "FIASTRA",
      "rm": "MC",
      "1234": 411043017
    },
    {
      "1": 5285,
      "roma": "FILANDARI",
      "rm": "VV",
      "1234": 418102012
    },
    {
      "1": 5286,
      "roma": "FILATTIERA",
      "rm": "MS",
      "1234": 409045006
    },
    {
      "1": 5287,
      "roma": "FILETTINO",
      "rm": "FR",
      "1234": 412060034
    },
    {
      "1": 5288,
      "roma": "FILETTO",
      "rm": "CH",
      "1234": 413069032
    },
    {
      "1": 5289,
      "roma": "FILIANO",
      "rm": "PZ",
      "1234": 417076032
    },
    {
      "1": 5290,
      "roma": "FILIGHERA",
      "rm": "PV",
      "1234": 403018063
    },
    {
      "1": 5291,
      "roma": "FILIGNANO",
      "rm": "IS",
      "1234": 414094019
    },
    {
      "1": 5292,
      "roma": "FILOGASO",
      "rm": "VV",
      "1234": 418102013
    },
    {
      "1": 5293,
      "roma": "FIESSE",
      "rm": "BS",
      "1234": 403017071
    },
    {
      "1": 5294,
      "roma": "FIESSO D'ARTICO",
      "rm": "VE",
      "1234": 405027014
    },
    {
      "1": 5295,
      "roma": "FIESSO UMBERTIANO",
      "rm": "RO",
      "1234": 405029022
    },
    {
      "1": 5296,
      "roma": "FINALE LIGURE",
      "rm": "SV",
      "1234": 407009029
    },
    {
      "1": 5297,
      "roma": "FIGLINE E INCISA VALDARNO",
      "rm": "FI",
      "1234": 409048052
    },
    {
      "1": 5298,
      "roma": "FILACCIANO",
      "rm": "RM",
      "1234": 412058037
    },
    {
      "1": 5299,
      "roma": "FILADELFIA",
      "rm": "VV",
      "1234": 418102011
    },
    {
      "1": 5300,
      "roma": "FILAGO",
      "rm": "BG",
      "1234": 403016098
    },
    {
      "1": 5301,
      "roma": "FIORENZUOLA D'ARDA",
      "rm": "PC",
      "1234": 408033021
    },
    {
      "1": 5302,
      "roma": "FIRENZE",
      "rm": "FI",
      "1234": 409048017
    },
    {
      "1": 5303,
      "roma": "FIRENZUOLA",
      "rm": "FI",
      "1234": 409048018
    },
    {
      "1": 5304,
      "roma": "FIRMO",
      "rm": "CS",
      "1234": 418078054
    },
    {
      "1": 5305,
      "roma": "FISCIANO",
      "rm": "SA",
      "1234": 415065052
    },
    {
      "1": 5306,
      "roma": "FIUGGI",
      "rm": "FR",
      "1234": 412060035
    },
    {
      "1": 5307,
      "roma": "FIUMALBO",
      "rm": "MO",
      "1234": 408036014
    },
    {
      "1": 5308,
      "roma": "FILOTTRANO",
      "rm": "AN",
      "1234": 411042019
    },
    {
      "1": 5309,
      "roma": "FINALE EMILIA",
      "rm": "MO",
      "1234": 408036012
    },
    {
      "1": 5310,
      "roma": "FIUME VENETO",
      "rm": "PN",
      "1234": 406093021
    },
    {
      "1": 5311,
      "roma": "FIUMEDINISI",
      "rm": "ME",
      "1234": 419083021
    },
    {
      "1": 5312,
      "roma": "FISCAGLIA",
      "rm": "FE",
      "1234": 408038027
    },
    {
      "1": 5313,
      "roma": "FIUMEFREDDO BRUZIO",
      "rm": "CS",
      "1234": 418078055
    },
    {
      "1": 5314,
      "roma": "FINO DEL MONTE",
      "rm": "BG",
      "1234": 403016099
    },
    {
      "1": 5315,
      "roma": "FINO MORNASCO",
      "rm": "CO",
      "1234": 403013102
    },
    {
      "1": 5316,
      "roma": "FIORANO AL SERIO",
      "rm": "BG",
      "1234": 403016100
    },
    {
      "1": 5317,
      "roma": "FIORANO CANAVESE",
      "rm": "TO",
      "1234": 401001105
    },
    {
      "1": 5318,
      "roma": "FIORANO MODENESE",
      "rm": "MO",
      "1234": 408036013
    },
    {
      "1": 5319,
      "roma": "FLAIBANO",
      "rm": "UD",
      "1234": 406030039
    },
    {
      "1": 5320,
      "roma": "FLERO",
      "rm": "BS",
      "1234": 403017072
    },
    {
      "1": 5321,
      "roma": "FLORESTA",
      "rm": "ME",
      "1234": 419083022
    },
    {
      "1": 5322,
      "roma": "FLORIDIA",
      "rm": "SR",
      "1234": 419089009
    },
    {
      "1": 5323,
      "roma": "FLORINAS",
      "rm": "SS",
      "1234": 420090029
    },
    {
      "1": 5324,
      "roma": "FLUMERI",
      "rm": "AV",
      "1234": 415064032
    },
    {
      "1": 5325,
      "roma": "FLUMINIMAGGIORE",
      "rm": "CA",
      "1234": 420092021
    },
    {
      "1": 5326,
      "roma": "FLUSSIO",
      "rm": "NU",
      "1234": 420091023
    },
    {
      "1": 5327,
      "roma": "FIUMARA",
      "rm": "RC",
      "1234": 418080034
    },
    {
      "1": 5328,
      "roma": "FOGLIANISE",
      "rm": "BN",
      "1234": 415062030
    },
    {
      "1": 5329,
      "roma": "FOGLIANO REDIPUGLIA",
      "rm": "GO",
      "1234": 406031006
    },
    {
      "1": 5330,
      "roma": "FOGLIZZO",
      "rm": "TO",
      "1234": 401001106
    },
    {
      "1": 5331,
      "roma": "FIUMEFREDDO DI SICILIA",
      "rm": "CT",
      "1234": 419087016
    },
    {
      "1": 5332,
      "roma": "FIUMICINO",
      "rm": "RM",
      "1234": 412058120
    },
    {
      "1": 5333,
      "roma": "FIUMINATA",
      "rm": "MC",
      "1234": 411043019
    },
    {
      "1": 5334,
      "roma": "FIVIZZANO",
      "rm": "MS",
      "1234": 409045007
    },
    {
      "1": 5335,
      "roma": "FOLLINA",
      "rm": "TV",
      "1234": 405026027
    },
    {
      "1": 5336,
      "roma": "FOLLO",
      "rm": "SP",
      "1234": 407011013
    },
    {
      "1": 5337,
      "roma": "FOLLONICA",
      "rm": "GR",
      "1234": 409053009
    },
    {
      "1": 5338,
      "roma": "FOMBIO",
      "rm": "LO",
      "1234": 403098026
    },
    {
      "1": 5339,
      "roma": "FONDACHELLI-FANTINA",
      "rm": "ME",
      "1234": 419083023
    },
    {
      "1": 5340,
      "roma": "FONDI",
      "rm": "LT",
      "1234": 412059007
    },
    {
      "1": 5341,
      "roma": "FONDO",
      "rm": "TN",
      "1234": 404022088
    },
    {
      "1": 5342,
      "roma": "FOBELLO",
      "rm": "VC",
      "1234": 401002057
    },
    {
      "1": 5343,
      "roma": "FOGGIA",
      "rm": "FG",
      "1234": 416071024
    },
    {
      "1": 5344,
      "roma": "FONTANA LIRI",
      "rm": "FR",
      "1234": 412060036
    },
    {
      "1": 5345,
      "roma": "FONTANAFREDDA",
      "rm": "PN",
      "1234": 406093022
    },
    {
      "1": 5346,
      "roma": "FONTANAROSA",
      "rm": "AV",
      "1234": 415064033
    },
    {
      "1": 5347,
      "roma": "FOIANO DELLA CHIANA",
      "rm": "AR",
      "1234": 409051018
    },
    {
      "1": 5348,
      "roma": "FOIANO DI VAL FORTORE",
      "rm": "BN",
      "1234": 415062031
    },
    {
      "1": 5349,
      "roma": "FOLGARIA",
      "rm": "TN",
      "1234": 404022087
    },
    {
      "1": 5350,
      "roma": "FOLIGNANO",
      "rm": "AP",
      "1234": 411044020
    },
    {
      "1": 5351,
      "roma": "FOLIGNO",
      "rm": "PG",
      "1234": 410054018
    },
    {
      "1": 5352,
      "roma": "FONTANILE",
      "rm": "AT",
      "1234": 401005054
    },
    {
      "1": 5353,
      "roma": "FONTANIVA",
      "rm": "PD",
      "1234": 405028038
    },
    {
      "1": 5354,
      "roma": "FONTE",
      "rm": "TV",
      "1234": 405026029
    },
    {
      "1": 5355,
      "roma": "FONTE NUOVA",
      "rm": "RM",
      "1234": 412058122
    },
    {
      "1": 5356,
      "roma": "FONTECCHIO",
      "rm": "AQ",
      "1234": 413066043
    },
    {
      "1": 5357,
      "roma": "FONTECHIARI",
      "rm": "FR",
      "1234": 412060037
    },
    {
      "1": 5358,
      "roma": "FONTEGRECA",
      "rm": "CE",
      "1234": 415061034
    },
    {
      "1": 5359,
      "roma": "FONTENO",
      "rm": "BG",
      "1234": 403016102
    },
    {
      "1": 5360,
      "roma": "FONTEVIVO",
      "rm": "PR",
      "1234": 408034016
    },
    {
      "1": 5361,
      "roma": "FONZASO",
      "rm": "BL",
      "1234": 405025022
    },
    {
      "1": 5362,
      "roma": "FONNI",
      "rm": "NU",
      "1234": 420091024
    },
    {
      "1": 5363,
      "roma": "FONTAINEMORE",
      "rm": "AO",
      "1234": 402007028
    },
    {
      "1": 5364,
      "roma": "FORCHIA",
      "rm": "BN",
      "1234": 415062032
    },
    {
      "1": 5365,
      "roma": "FORCOLA",
      "rm": "SO",
      "1234": 403014029
    },
    {
      "1": 5366,
      "roma": "FORDONGIANUS",
      "rm": "OR",
      "1234": 420095020
    },
    {
      "1": 5367,
      "roma": "FONTANELICE",
      "rm": "BO",
      "1234": 408037026
    },
    {
      "1": 5368,
      "roma": "FONTANELLA",
      "rm": "BG",
      "1234": 403016101
    },
    {
      "1": 5369,
      "roma": "FONTANELLATO",
      "rm": "PR",
      "1234": 408034015
    },
    {
      "1": 5370,
      "roma": "FONTANELLE",
      "rm": "TV",
      "1234": 405026028
    },
    {
      "1": 5371,
      "roma": "FONTANETO D'AGOGNA",
      "rm": "NO",
      "1234": 401003066
    },
    {
      "1": 5372,
      "roma": "FONTANETTO PO",
      "rm": "VC",
      "1234": 401002058
    },
    {
      "1": 5373,
      "roma": "FONTANIGORDA",
      "rm": "GE",
      "1234": 407010024
    },
    {
      "1": 5374,
      "roma": "FORLI' DEL SANNIO",
      "rm": "IS",
      "1234": 414094020
    },
    {
      "1": 5375,
      "roma": "FORLI'",
      "rm": "FO",
      "1234": 408040012
    },
    {
      "1": 5376,
      "roma": "FORLIMPOPOLI",
      "rm": "FO",
      "1234": 408040013
    },
    {
      "1": 5377,
      "roma": "FORMAZZA",
      "rm": "VB",
      "1234": 401103031
    },
    {
      "1": 5378,
      "roma": "FORMELLO",
      "rm": "RM",
      "1234": 412058038
    },
    {
      "1": 5379,
      "roma": "FORMIA",
      "rm": "LT",
      "1234": 412059008
    },
    {
      "1": 5380,
      "roma": "FORMICOLA",
      "rm": "CE",
      "1234": 415061035
    },
    {
      "1": 5381,
      "roma": "FORMIGARA",
      "rm": "CR",
      "1234": 403019044
    },
    {
      "1": 5382,
      "roma": "FORMIGINE",
      "rm": "MO",
      "1234": 408036015
    },
    {
      "1": 5383,
      "roma": "FOPPOLO",
      "rm": "BG",
      "1234": 403016103
    },
    {
      "1": 5384,
      "roma": "FORANO",
      "rm": "RI",
      "1234": 412057029
    },
    {
      "1": 5385,
      "roma": "FORCE",
      "rm": "AP",
      "1234": 411044021
    },
    {
      "1": 5386,
      "roma": "FORNELLI",
      "rm": "IS",
      "1234": 414094021
    },
    {
      "1": 5387,
      "roma": "FORNI AVOLTRI",
      "rm": "UD",
      "1234": 406030040
    },
    {
      "1": 5388,
      "roma": "FORNI DI SOPRA",
      "rm": "UD",
      "1234": 406030041
    },
    {
      "1": 5389,
      "roma": "FORENZA",
      "rm": "PZ",
      "1234": 417076033
    },
    {
      "1": 5390,
      "roma": "FORESTO SPARSO",
      "rm": "BG",
      "1234": 403016104
    },
    {
      "1": 5391,
      "roma": "FORGARIA NEL FRIULI",
      "rm": "UD",
      "1234": 406030137
    },
    {
      "1": 5392,
      "roma": "FORINO",
      "rm": "AV",
      "1234": 415064034
    },
    {
      "1": 5393,
      "roma": "FORIO",
      "rm": "NA",
      "1234": 415063031
    },
    {
      "1": 5394,
      "roma": "FORNOVO SAN GIOVANNI",
      "rm": "BG",
      "1234": 403016105
    },
    {
      "1": 5395,
      "roma": "FORNOVO DI TARO",
      "rm": "PR",
      "1234": 408034017
    },
    {
      "1": 5396,
      "roma": "FORTE DEI MARMI",
      "rm": "LU",
      "1234": 409046013
    },
    {
      "1": 5397,
      "roma": "FORTEZZA",
      "rm": "BZ",
      "1234": 404021032
    },
    {
      "1": 5398,
      "roma": "FORTUNAGO",
      "rm": "PV",
      "1234": 403018064
    },
    {
      "1": 5399,
      "roma": "FORZA D'AGRO'",
      "rm": "ME",
      "1234": 419083024
    },
    {
      "1": 5400,
      "roma": "FOSCIANDORA",
      "rm": "LU",
      "1234": 409046014
    },
    {
      "1": 5401,
      "roma": "FOSDINOVO",
      "rm": "MS",
      "1234": 409045008
    },
    {
      "1": 5402,
      "roma": "FOSSA",
      "rm": "AQ",
      "1234": 413066044
    },
    {
      "1": 5403,
      "roma": "FORMIGLIANA",
      "rm": "VC",
      "1234": 401002059
    },
    {
      "1": 5404,
      "roma": "FORNACE",
      "rm": "TN",
      "1234": 404022089
    },
    {
      "1": 5405,
      "roma": "FOSSALTO",
      "rm": "CB",
      "1234": 414070024
    },
    {
      "1": 5406,
      "roma": "FOSSANO",
      "rm": "CN",
      "1234": 401004089
    },
    {
      "1": 5407,
      "roma": "FOSSATO DI VICO",
      "rm": "PG",
      "1234": 410054019
    },
    {
      "1": 5408,
      "roma": "FOSSATO SERRALTA",
      "rm": "CZ",
      "1234": 418079052
    },
    {
      "1": 5409,
      "roma": "FORNI DI SOTTO",
      "rm": "UD",
      "1234": 406030042
    },
    {
      "1": 5410,
      "roma": "FORNO CANAVESE",
      "rm": "TO",
      "1234": 401001107
    },
    {
      "1": 5411,
      "roma": "FRACONALTO",
      "rm": "AL",
      "1234": 401006069
    },
    {
      "1": 5412,
      "roma": "FRAGAGNANO",
      "rm": "TA",
      "1234": 416073006
    },
    {
      "1": 5413,
      "roma": "FRAGNETO L'ABATE",
      "rm": "BN",
      "1234": 415062033
    },
    {
      "1": 5414,
      "roma": "FRAGNETO MONFORTE",
      "rm": "BN",
      "1234": 415062034
    },
    {
      "1": 5415,
      "roma": "FRAINE",
      "rm": "CH",
      "1234": 413069034
    },
    {
      "1": 5416,
      "roma": "FRAMURA",
      "rm": "SP",
      "1234": 407011014
    },
    {
      "1": 5417,
      "roma": "FRANCAVILLA AL MARE",
      "rm": "CH",
      "1234": 413069035
    },
    {
      "1": 5418,
      "roma": "FRANCAVILLA ANGITOLA",
      "rm": "VV",
      "1234": 418102014
    },
    {
      "1": 5419,
      "roma": "FRANCAVILLA BISIO",
      "rm": "AL",
      "1234": 401006070
    },
    {
      "1": 5420,
      "roma": "FOSSACESIA",
      "rm": "CH",
      "1234": 413069033
    },
    {
      "1": 5421,
      "roma": "FOSSALTA DI PIAVE",
      "rm": "VE",
      "1234": 405027015
    },
    {
      "1": 5422,
      "roma": "FOSSALTA DI PORTOGRUARO",
      "rm": "VE",
      "1234": 405027016
    },
    {
      "1": 5423,
      "roma": "FRANCAVILLA MARITTIMA",
      "rm": "CS",
      "1234": 418078056
    },
    {
      "1": 5424,
      "roma": "FRANCICA",
      "rm": "VV",
      "1234": 418102015
    },
    {
      "1": 5425,
      "roma": "FRANCOFONTE",
      "rm": "SR",
      "1234": 419089010
    },
    {
      "1": 5426,
      "roma": "FOSSO'",
      "rm": "VE",
      "1234": 405027017
    },
    {
      "1": 5427,
      "roma": "FOSSOMBRONE",
      "rm": "PS",
      "1234": 411041015
    },
    {
      "1": 5428,
      "roma": "FOZA",
      "rm": "VI",
      "1234": 405024041
    },
    {
      "1": 5429,
      "roma": "FRABOSA SOPRANA",
      "rm": "CN",
      "1234": 401004090
    },
    {
      "1": 5430,
      "roma": "FRABOSA SOTTANA",
      "rm": "CN",
      "1234": 401004091
    },
    {
      "1": 5431,
      "roma": "FRASCINETO",
      "rm": "CS",
      "1234": 418078057
    },
    {
      "1": 5432,
      "roma": "FRASSILONGO",
      "rm": "TN",
      "1234": 404022090
    },
    {
      "1": 5433,
      "roma": "FRASSINELLE POLESINE",
      "rm": "RO",
      "1234": 405029023
    },
    {
      "1": 5434,
      "roma": "FRASSINELLO MONFERRATO",
      "rm": "AL",
      "1234": 401006072
    },
    {
      "1": 5435,
      "roma": "FRASSINETO PO",
      "rm": "AL",
      "1234": 401006073
    },
    {
      "1": 5436,
      "roma": "FRASSINETTO",
      "rm": "TO",
      "1234": 401001108
    },
    {
      "1": 5437,
      "roma": "FRASSINO",
      "rm": "CN",
      "1234": 401004092
    },
    {
      "1": 5438,
      "roma": "FRASSINORO",
      "rm": "MO",
      "1234": 408036016
    },
    {
      "1": 5439,
      "roma": "FRASSO SABINO",
      "rm": "RI",
      "1234": 412057030
    },
    {
      "1": 5440,
      "roma": "FRANCAVILLA DI SICILIA",
      "rm": "ME",
      "1234": 419083025
    },
    {
      "1": 5441,
      "roma": "FRANCAVILLA FONTANA",
      "rm": "BR",
      "1234": 416074008
    },
    {
      "1": 5442,
      "roma": "FRANCAVILLA IN SINNI",
      "rm": "PZ",
      "1234": 417076034
    },
    {
      "1": 5443,
      "roma": "FRATTAMAGGIORE",
      "rm": "NA",
      "1234": 415063032
    },
    {
      "1": 5444,
      "roma": "FRATTAMINORE",
      "rm": "NA",
      "1234": 415063033
    },
    {
      "1": 5445,
      "roma": "FRATTE ROSA",
      "rm": "PS",
      "1234": 411041016
    },
    {
      "1": 5446,
      "roma": "FRAZZANO'",
      "rm": "ME",
      "1234": 419083026
    },
    {
      "1": 5447,
      "roma": "FRANCOLISE",
      "rm": "CE",
      "1234": 415061036
    },
    {
      "1": 5448,
      "roma": "FRASCARO",
      "rm": "AL",
      "1234": 401006071
    },
    {
      "1": 5449,
      "roma": "FRASCAROLO",
      "rm": "PV",
      "1234": 403018065
    },
    {
      "1": 5450,
      "roma": "FRASCATI",
      "rm": "RM",
      "1234": 412058039
    },
    {
      "1": 5451,
      "roma": "FRANCAVILLA D'ETE",
      "rm": "FM",
      "1234": 411109007
    },
    {
      "1": 5452,
      "roma": "FRINCO",
      "rm": "AT",
      "1234": 401005055
    },
    {
      "1": 5453,
      "roma": "FRISA",
      "rm": "CH",
      "1234": 413069037
    },
    {
      "1": 5454,
      "roma": "FRISANCO",
      "rm": "PN",
      "1234": 406093024
    },
    {
      "1": 5455,
      "roma": "FRONT",
      "rm": "TO",
      "1234": 401001109
    },
    {
      "1": 5456,
      "roma": "FRONTINO",
      "rm": "PS",
      "1234": 411041017
    },
    {
      "1": 5457,
      "roma": "FRONTONE",
      "rm": "PS",
      "1234": 411041018
    },
    {
      "1": 5458,
      "roma": "FROSINONE",
      "rm": "FR",
      "1234": 412060038
    },
    {
      "1": 5459,
      "roma": "FRASSO TELESINO",
      "rm": "BN",
      "1234": 415062035
    },
    {
      "1": 5460,
      "roma": "FRATTA POLESINE",
      "rm": "RO",
      "1234": 405029024
    },
    {
      "1": 5461,
      "roma": "FRATTA TODINA",
      "rm": "PG",
      "1234": 410054020
    },
    {
      "1": 5462,
      "roma": "FRUGAROLO",
      "rm": "AL",
      "1234": 401006075
    },
    {
      "1": 5463,
      "roma": "FUCECCHIO",
      "rm": "FI",
      "1234": 409048019
    },
    {
      "1": 5464,
      "roma": "FREGONA",
      "rm": "TV",
      "1234": 405026030
    },
    {
      "1": 5465,
      "roma": "FRESAGRANDINARIA",
      "rm": "CH",
      "1234": 413069036
    },
    {
      "1": 5466,
      "roma": "FRESONARA",
      "rm": "AL",
      "1234": 401006074
    },
    {
      "1": 5467,
      "roma": "FRIGENTO",
      "rm": "AV",
      "1234": 415064035
    },
    {
      "1": 5468,
      "roma": "FRIGNANO",
      "rm": "CE",
      "1234": 415061037
    },
    {
      "1": 5469,
      "roma": "FURCI SICULO",
      "rm": "ME",
      "1234": 419083027
    },
    {
      "1": 5470,
      "roma": "FURNARI",
      "rm": "ME",
      "1234": 419083028
    },
    {
      "1": 5471,
      "roma": "FURORE",
      "rm": "SA",
      "1234": 415065053
    },
    {
      "1": 5472,
      "roma": "FURTEI",
      "rm": "CA",
      "1234": 420092022
    },
    {
      "1": 5473,
      "roma": "FUSCALDO",
      "rm": "CS",
      "1234": 418078058
    },
    {
      "1": 5474,
      "roma": "FUSIGNANO",
      "rm": "RA",
      "1234": 408039011
    },
    {
      "1": 5475,
      "roma": "FUSINE",
      "rm": "SO",
      "1234": 403014030
    },
    {
      "1": 5476,
      "roma": "FUTANI",
      "rm": "SA",
      "1234": 415065054
    },
    {
      "1": 5477,
      "roma": "GABBIONETA BINANUOVA",
      "rm": "CR",
      "1234": 403019045
    },
    {
      "1": 5478,
      "roma": "GABIANO",
      "rm": "AL",
      "1234": 401006077
    },
    {
      "1": 5479,
      "roma": "FROSOLONE",
      "rm": "IS",
      "1234": 414094022
    },
    {
      "1": 5480,
      "roma": "FROSSASCO",
      "rm": "TO",
      "1234": 401001110
    },
    {
      "1": 5481,
      "roma": "GABY",
      "rm": "AO",
      "1234": 402007029
    },
    {
      "1": 5482,
      "roma": "GADESCO PIEVE DELMONA",
      "rm": "CR",
      "1234": 403019046
    },
    {
      "1": 5483,
      "roma": "FUIPIANO VALLE IMAGNA",
      "rm": "BG",
      "1234": 403016106
    },
    {
      "1": 5484,
      "roma": "FUMANE",
      "rm": "VR",
      "1234": 405023035
    },
    {
      "1": 5485,
      "roma": "FUMONE",
      "rm": "FR",
      "1234": 412060039
    },
    {
      "1": 5486,
      "roma": "FUNES",
      "rm": "BZ",
      "1234": 404021033
    },
    {
      "1": 5487,
      "roma": "FURCI",
      "rm": "CH",
      "1234": 413069038
    },
    {
      "1": 5488,
      "roma": "GAGLIANO ATERNO",
      "rm": "AQ",
      "1234": 413066045
    },
    {
      "1": 5489,
      "roma": "GAGLIANO CASTELFERRATO",
      "rm": "EN",
      "1234": 419086010
    },
    {
      "1": 5490,
      "roma": "GAGLIANO DEL CAPO",
      "rm": "LE",
      "1234": 416075028
    },
    {
      "1": 5491,
      "roma": "GAGLIATO",
      "rm": "CZ",
      "1234": 418079055
    },
    {
      "1": 5492,
      "roma": "GAGLIOLE",
      "rm": "MC",
      "1234": 411043020
    },
    {
      "1": 5493,
      "roma": "GAIARINE",
      "rm": "TV",
      "1234": 405026031
    },
    {
      "1": 5494,
      "roma": "GAIBA",
      "rm": "RO",
      "1234": 405029025
    },
    {
      "1": 5495,
      "roma": "GAIOLA",
      "rm": "CN",
      "1234": 401004093
    },
    {
      "1": 5496,
      "roma": "GAIOLE IN CHIANTI",
      "rm": "SI",
      "1234": 409052013
    },
    {
      "1": 5497,
      "roma": "GAIRO",
      "rm": "NU",
      "1234": 420091026
    },
    {
      "1": 5498,
      "roma": "GABICCE MARE",
      "rm": "PS",
      "1234": 411041019
    },
    {
      "1": 5499,
      "roma": "GALATONE",
      "rm": "LE",
      "1234": 416075030
    },
    {
      "1": 5500,
      "roma": "GALATRO",
      "rm": "RC",
      "1234": 418080035
    },
    {
      "1": 5501,
      "roma": "GALBIATE",
      "rm": "LC",
      "1234": 403097036
    },
    {
      "1": 5502,
      "roma": "GADONI",
      "rm": "NU",
      "1234": 420091025
    },
    {
      "1": 5503,
      "roma": "GAETA",
      "rm": "LT",
      "1234": 412059009
    },
    {
      "1": 5504,
      "roma": "GAGGI",
      "rm": "ME",
      "1234": 419083029
    },
    {
      "1": 5505,
      "roma": "GAGGIANO",
      "rm": "MI",
      "1234": 403015103
    },
    {
      "1": 5506,
      "roma": "GAGGIO MONTANO",
      "rm": "BO",
      "1234": 408037027
    },
    {
      "1": 5507,
      "roma": "GAGLIANICO",
      "rm": "BI",
      "1234": 401096026
    },
    {
      "1": 5508,
      "roma": "GALLIATE LOMBARDO",
      "rm": "VA",
      "1234": 403012071
    },
    {
      "1": 5509,
      "roma": "GALLIAVOLA",
      "rm": "PV",
      "1234": 403018066
    },
    {
      "1": 5510,
      "roma": "GALLICANO",
      "rm": "LU",
      "1234": 409046015
    },
    {
      "1": 5511,
      "roma": "GALLICANO NEL LAZIO",
      "rm": "RM",
      "1234": 412058040
    },
    {
      "1": 5512,
      "roma": "GALLICCHIO",
      "rm": "PZ",
      "1234": 417076035
    },
    {
      "1": 5513,
      "roma": "GALLIERA",
      "rm": "BO",
      "1234": 408037028
    },
    {
      "1": 5514,
      "roma": "GALLIERA VENETA",
      "rm": "PD",
      "1234": 405028039
    },
    {
      "1": 5515,
      "roma": "GALLINARO",
      "rm": "FR",
      "1234": 412060040
    },
    {
      "1": 5516,
      "roma": "GALLIO",
      "rm": "VI",
      "1234": 405024042
    },
    {
      "1": 5517,
      "roma": "GALLIPOLI",
      "rm": "LE",
      "1234": 416075031
    },
    {
      "1": 5518,
      "roma": "GAIS",
      "rm": "BZ",
      "1234": 404021034
    },
    {
      "1": 5519,
      "roma": "GALATI MAMERTINO",
      "rm": "ME",
      "1234": 419083030
    },
    {
      "1": 5520,
      "roma": "GALATINA",
      "rm": "LE",
      "1234": 416075029
    },
    {
      "1": 5521,
      "roma": "GALTELLI",
      "rm": "NU",
      "1234": 420091027
    },
    {
      "1": 5522,
      "roma": "GALZIGNANO TERME",
      "rm": "PD",
      "1234": 405028040
    },
    {
      "1": 5523,
      "roma": "GAMALERO",
      "rm": "AL",
      "1234": 401006078
    },
    {
      "1": 5524,
      "roma": "GALEATA",
      "rm": "FO",
      "1234": 408040014
    },
    {
      "1": 5525,
      "roma": "GALGAGNANO",
      "rm": "LO",
      "1234": 403098027
    },
    {
      "1": 5526,
      "roma": "GALLARATE",
      "rm": "VA",
      "1234": 403012070
    },
    {
      "1": 5527,
      "roma": "GALLESE",
      "rm": "VT",
      "1234": 412056027
    },
    {
      "1": 5528,
      "roma": "GALLIATE",
      "rm": "NO",
      "1234": 401003068
    },
    {
      "1": 5529,
      "roma": "GAMBERALE",
      "rm": "CH",
      "1234": 413069039
    },
    {
      "1": 5530,
      "roma": "GAMBETTOLA",
      "rm": "FO",
      "1234": 408040015
    },
    {
      "1": 5531,
      "roma": "GAMBOLO'",
      "rm": "PV",
      "1234": 403018068
    },
    {
      "1": 5532,
      "roma": "GAMBUGLIANO",
      "rm": "VI",
      "1234": 405024044
    },
    {
      "1": 5533,
      "roma": "GANDELLINO",
      "rm": "BG",
      "1234": 403016107
    },
    {
      "1": 5534,
      "roma": "GANDINO",
      "rm": "BG",
      "1234": 403016108
    },
    {
      "1": 5535,
      "roma": "GANDOSSO",
      "rm": "BG",
      "1234": 403016109
    },
    {
      "1": 5536,
      "roma": "GANGI",
      "rm": "PA",
      "1234": 419082036
    },
    {
      "1": 5537,
      "roma": "GALLO MATESE",
      "rm": "CE",
      "1234": 415061038
    },
    {
      "1": 5538,
      "roma": "GALLODORO",
      "rm": "ME",
      "1234": 419083031
    },
    {
      "1": 5539,
      "roma": "GALLUCCIO",
      "rm": "CE",
      "1234": 415061039
    },
    {
      "1": 5540,
      "roma": "GARBAGNATE MILANESE",
      "rm": "MI",
      "1234": 403015105
    },
    {
      "1": 5541,
      "roma": "GARBAGNATE MONASTERO",
      "rm": "LC",
      "1234": 403097037
    },
    {
      "1": 5542,
      "roma": "GAMBARA",
      "rm": "BS",
      "1234": 403017073
    },
    {
      "1": 5543,
      "roma": "GAMBARANA",
      "rm": "PV",
      "1234": 403018067
    },
    {
      "1": 5544,
      "roma": "GAMBASCA",
      "rm": "CN",
      "1234": 401004094
    },
    {
      "1": 5545,
      "roma": "GAMBASSI TERME",
      "rm": "FI",
      "1234": 409048020
    },
    {
      "1": 5546,
      "roma": "GAMBATESA",
      "rm": "CB",
      "1234": 414070025
    },
    {
      "1": 5547,
      "roma": "GAMBELLARA",
      "rm": "VI",
      "1234": 405024043
    },
    {
      "1": 5548,
      "roma": "GARGAZZONE",
      "rm": "BZ",
      "1234": 404021035
    },
    {
      "1": 5549,
      "roma": "GARGNANO",
      "rm": "BS",
      "1234": 403017076
    },
    {
      "1": 5550,
      "roma": "GARLASCO",
      "rm": "PV",
      "1234": 403018069
    },
    {
      "1": 5551,
      "roma": "GARLATE",
      "rm": "LC",
      "1234": 403097038
    },
    {
      "1": 5552,
      "roma": "GARLENDA",
      "rm": "SV",
      "1234": 407009030
    },
    {
      "1": 5553,
      "roma": "GARZENO",
      "rm": "CO",
      "1234": 403013106
    },
    {
      "1": 5554,
      "roma": "GARZIGLIANA",
      "rm": "TO",
      "1234": 401001111
    },
    {
      "1": 5555,
      "roma": "GASPERINA",
      "rm": "CZ",
      "1234": 418079056
    },
    {
      "1": 5556,
      "roma": "GARAGUSO",
      "rm": "MT",
      "1234": 417077009
    },
    {
      "1": 5557,
      "roma": "GARBAGNA",
      "rm": "AL",
      "1234": 401006079
    },
    {
      "1": 5558,
      "roma": "GARBAGNA NOVARESE",
      "rm": "NO",
      "1234": 401003069
    },
    {
      "1": 5559,
      "roma": "GATTINARA",
      "rm": "VC",
      "1234": 401002061
    },
    {
      "1": 5560,
      "roma": "GARDA",
      "rm": "VR",
      "1234": 405023036
    },
    {
      "1": 5561,
      "roma": "GARDONE RIVIERA",
      "rm": "BS",
      "1234": 403017074
    },
    {
      "1": 5562,
      "roma": "GARDONE VAL TROMPIA",
      "rm": "BS",
      "1234": 403017075
    },
    {
      "1": 5784,
      "roma": "LICODIA EUBEA",
      "rm": "CT",
      "1234": 419087020
    },
    {
      "1": 5785,
      "roma": "LIERNA",
      "rm": "LC",
      "1234": 403097043
    },
    {
      "1": 5786,
      "roma": "LIGNANA",
      "rm": "VC",
      "1234": 401002070
    },
    {
      "1": 5787,
      "roma": "LIGNANO-SABBIADORO",
      "rm": "UD",
      "1234": 406030049
    },
    {
      "1": 5788,
      "roma": "LILLIANES",
      "rm": "AO",
      "1234": 402007042
    },
    {
      "1": 5789,
      "roma": "LIMANA",
      "rm": "BL",
      "1234": 405025029
    },
    {
      "1": 5790,
      "roma": "LIMATOLA",
      "rm": "BN",
      "1234": 415062038
    },
    {
      "1": 5791,
      "roma": "LEZZENO",
      "rm": "CO",
      "1234": 403013126
    },
    {
      "1": 5792,
      "roma": "LIMENA",
      "rm": "PD",
      "1234": 405028045
    },
    {
      "1": 5793,
      "roma": "LIMIDO COMASCO",
      "rm": "CO",
      "1234": 403013128
    },
    {
      "1": 5794,
      "roma": "LIMINA",
      "rm": "ME",
      "1234": 419083040
    },
    {
      "1": 5795,
      "roma": "LIMONE PIEMONTE",
      "rm": "CN",
      "1234": 401004110
    },
    {
      "1": 5796,
      "roma": "LIMONE SUL GARDA",
      "rm": "BS",
      "1234": 403017089
    },
    {
      "1": 5797,
      "roma": "LIMOSANO",
      "rm": "CB",
      "1234": 414070032
    },
    {
      "1": 5798,
      "roma": "LINAROLO",
      "rm": "PV",
      "1234": 403018081
    },
    {
      "1": 5799,
      "roma": "LINGUAGLOSSA",
      "rm": "CT",
      "1234": 419087021
    },
    {
      "1": 5800,
      "roma": "LIONI",
      "rm": "AV",
      "1234": 415064044
    },
    {
      "1": 5801,
      "roma": "LIPARI",
      "rm": "ME",
      "1234": 419083041
    },
    {
      "1": 5802,
      "roma": "LIMBIATE",
      "rm": "MB",
      "1234": 403108027
    },
    {
      "1": 5803,
      "roma": "LIPOMO",
      "rm": "CO",
      "1234": 403013129
    },
    {
      "1": 5804,
      "roma": "LIRIO",
      "rm": "PV",
      "1234": 403018082
    },
    {
      "1": 5805,
      "roma": "LISCATE",
      "rm": "MI",
      "1234": 403015122
    },
    {
      "1": 5806,
      "roma": "LISCIA",
      "rm": "CH",
      "1234": 413069049
    },
    {
      "1": 5807,
      "roma": "LIMBADI",
      "rm": "VV",
      "1234": 418102019
    },
    {
      "1": 5808,
      "roma": "LIBERI",
      "rm": "CE",
      "1234": 415061045
    },
    {
      "1": 5809,
      "roma": "LIVERI",
      "rm": "NA",
      "1234": 415063040
    },
    {
      "1": 5810,
      "roma": "LIVIGNO",
      "rm": "SO",
      "1234": 403014037
    },
    {
      "1": 5811,
      "roma": "LIVINALLONGO DEL COL DI LANA",
      "rm": "BL",
      "1234": 405025030
    },
    {
      "1": 5812,
      "roma": "LIVO",
      "rm": "TN",
      "1234": 404022106
    },
    {
      "1": 5814,
      "roma": "LIVORNO",
      "rm": "LI",
      "1234": 409049009
    },
    {
      "1": 5815,
      "roma": "LIVORNO FERRARIS",
      "rm": "VC",
      "1234": 401002071
    },
    {
      "1": 5816,
      "roma": "LIVRAGA",
      "rm": "LO",
      "1234": 403098030
    },
    {
      "1": 5817,
      "roma": "LIZZANELLO",
      "rm": "LE",
      "1234": 416075038
    },
    {
      "1": 5818,
      "roma": "LIZZANO",
      "rm": "TA",
      "1234": 416073011
    },
    {
      "1": 5819,
      "roma": "LIZZANO IN BELVEDERE",
      "rm": "BO",
      "1234": 408037033
    },
    {
      "1": 5820,
      "roma": "LISSONE",
      "rm": "MB",
      "1234": 403108028
    },
    {
      "1": 5821,
      "roma": "LOANO",
      "rm": "SV",
      "1234": 407009034
    },
    {
      "1": 5822,
      "roma": "LOAZZOLO",
      "rm": "AT",
      "1234": 401005060
    },
    {
      "1": 5823,
      "roma": "LISCIANO NICCONE",
      "rm": "PG",
      "1234": 410054025
    },
    {
      "1": 5824,
      "roma": "LISIO",
      "rm": "CN",
      "1234": 401004111
    },
    {
      "1": 5825,
      "roma": "LOCATE VARESINO",
      "rm": "CO",
      "1234": 403013131
    },
    {
      "1": 5826,
      "roma": "LOCATELLO",
      "rm": "BG",
      "1234": 403016127
    },
    {
      "1": 5827,
      "roma": "LOCERI",
      "rm": "NU",
      "1234": 420091039
    },
    {
      "1": 5828,
      "roma": "LOCOROTONDO",
      "rm": "BA",
      "1234": 416072025
    },
    {
      "1": 5829,
      "roma": "LOCRI",
      "rm": "RC",
      "1234": 418080043
    },
    {
      "1": 5830,
      "roma": "LOCULI",
      "rm": "NU",
      "1234": 420091040
    },
    {
      "1": 5831,
      "roma": "LODE'",
      "rm": "NU",
      "1234": 420091041
    },
    {
      "1": 5832,
      "roma": "LODI",
      "rm": "LO",
      "1234": 403098031
    },
    {
      "1": 5833,
      "roma": "LODI VECCHIO",
      "rm": "LO",
      "1234": 403098032
    },
    {
      "1": 5834,
      "roma": "LODINE",
      "rm": "NU",
      "1234": 420091104
    },
    {
      "1": 5835,
      "roma": "LODRINO",
      "rm": "BS",
      "1234": 403017090
    },
    {
      "1": 5836,
      "roma": "LOGRATO",
      "rm": "BS",
      "1234": 403017091
    },
    {
      "1": 5837,
      "roma": "LOIANO",
      "rm": "BO",
      "1234": 408037034
    },
    {
      "1": 5838,
      "roma": "LOIRI PORTO SAN PAOLO",
      "rm": "SS",
      "1234": 420090084
    },
    {
      "1": 5839,
      "roma": "LOCANA",
      "rm": "TO",
      "1234": 401001134
    },
    {
      "1": 5840,
      "roma": "LOCATE DI TRIULZI",
      "rm": "MI",
      "1234": 403015125
    },
    {
      "1": 5841,
      "roma": "LOMAZZO",
      "rm": "CO",
      "1234": 403013133
    },
    {
      "1": 5842,
      "roma": "LOMBARDORE",
      "rm": "TO",
      "1234": 401001135
    },
    {
      "1": 5843,
      "roma": "LOMBRIASCO",
      "rm": "TO",
      "1234": 401001136
    },
    {
      "1": 5844,
      "roma": "LOMELLO",
      "rm": "PV",
      "1234": 403018083
    },
    {
      "1": 5845,
      "roma": "LONA-LASES",
      "rm": "TN",
      "1234": 404022108
    },
    {
      "1": 5846,
      "roma": "LONATE CEPPINO",
      "rm": "VA",
      "1234": 403012089
    },
    {
      "1": 5847,
      "roma": "LONATE POZZOLO",
      "rm": "VA",
      "1234": 403012090
    },
    {
      "1": 5848,
      "roma": "LONDA",
      "rm": "FI",
      "1234": 409048025
    },
    {
      "1": 5849,
      "roma": "LONGANO",
      "rm": "IS",
      "1234": 414094024
    },
    {
      "1": 5850,
      "roma": "LONGARE",
      "rm": "VI",
      "1234": 405024051
    },
    {
      "1": 5851,
      "roma": "LONGHENA",
      "rm": "BS",
      "1234": 403017093
    },
    {
      "1": 5852,
      "roma": "LONGI",
      "rm": "ME",
      "1234": 419083042
    },
    {
      "1": 5853,
      "roma": "LONGIANO",
      "rm": "FO",
      "1234": 408040018
    },
    {
      "1": 5854,
      "roma": "LONGARONE",
      "rm": "BL",
      "1234": 405025071
    },
    {
      "1": 5855,
      "roma": "LOMAGNA",
      "rm": "LC",
      "1234": 403097044
    },
    {
      "1": 5856,
      "roma": "LONIGO",
      "rm": "VI",
      "1234": 405024052
    },
    {
      "1": 5857,
      "roma": "LORANZE'",
      "rm": "TO",
      "1234": 401001137
    },
    {
      "1": 5858,
      "roma": "LOREGGIA",
      "rm": "PD",
      "1234": 405028046
    },
    {
      "1": 5859,
      "roma": "LOREGLIA",
      "rm": "VB",
      "1234": 401103038
    },
    {
      "1": 5860,
      "roma": "LORENZAGO DI CADORE",
      "rm": "BL",
      "1234": 405025032
    },
    {
      "1": 5861,
      "roma": "LOREO",
      "rm": "RO",
      "1234": 405029030
    },
    {
      "1": 5862,
      "roma": "LORETO",
      "rm": "AN",
      "1234": 411042022
    },
    {
      "1": 5863,
      "roma": "LORETO APRUTINO",
      "rm": "PE",
      "1234": 413068021
    },
    {
      "1": 5864,
      "roma": "LORIA",
      "rm": "TV",
      "1234": 405026036
    },
    {
      "1": 5865,
      "roma": "LORO CIUFFENNA",
      "rm": "AR",
      "1234": 409051020
    },
    {
      "1": 5866,
      "roma": "LORO PICENO",
      "rm": "MC",
      "1234": 411043022
    },
    {
      "1": 5867,
      "roma": "LORSICA",
      "rm": "GE",
      "1234": 407010030
    },
    {
      "1": 5868,
      "roma": "LOSINE",
      "rm": "BS",
      "1234": 403017094
    },
    {
      "1": 5869,
      "roma": "LOTZORAI",
      "rm": "NU",
      "1234": 420091042
    },
    {
      "1": 5870,
      "roma": "LONGOBARDI",
      "rm": "CS",
      "1234": 418078067
    },
    {
      "1": 5871,
      "roma": "LONGONE AL SEGRINO",
      "rm": "CO",
      "1234": 403013134
    },
    {
      "1": 5872,
      "roma": "LONGONE SABINO",
      "rm": "RI",
      "1234": 412057034
    },
    {
      "1": 5873,
      "roma": "LOZZA",
      "rm": "VA",
      "1234": 403012091
    },
    {
      "1": 5874,
      "roma": "LOZZO ATESTINO",
      "rm": "PD",
      "1234": 405028047
    },
    {
      "1": 5875,
      "roma": "LOZZO DI CADORE",
      "rm": "BL",
      "1234": 405025033
    },
    {
      "1": 5876,
      "roma": "LOZZOLO",
      "rm": "VC",
      "1234": 401002072
    },
    {
      "1": 5877,
      "roma": "LUBRIANO",
      "rm": "VT",
      "1234": 412056033
    },
    {
      "1": 5878,
      "roma": "LUCCA",
      "rm": "LU",
      "1234": 409046017
    },
    {
      "1": 5879,
      "roma": "LUCCA SICULA",
      "rm": "AG",
      "1234": 419084022
    },
    {
      "1": 5880,
      "roma": "LUCERA",
      "rm": "FG",
      "1234": 416071028
    },
    {
      "1": 5881,
      "roma": "LUCIGNANO",
      "rm": "AR",
      "1234": 409051021
    },
    {
      "1": 5882,
      "roma": "LUCINASCO",
      "rm": "IM",
      "1234": 407008033
    },
    {
      "1": 5883,
      "roma": "LUCITO",
      "rm": "CB",
      "1234": 414070033
    },
    {
      "1": 5884,
      "roma": "LUCO DEI MARSI",
      "rm": "AQ",
      "1234": 413066051
    },
    {
      "1": 5885,
      "roma": "LUCOLI",
      "rm": "AQ",
      "1234": 413066052
    },
    {
      "1": 5886,
      "roma": "LUGAGNANO VAL D'ARDA",
      "rm": "PC",
      "1234": 408033026
    },
    {
      "1": 5887,
      "roma": "LONGOBUCCO",
      "rm": "CS",
      "1234": 418078068
    },
    {
      "1": 5888,
      "roma": "LOVERO",
      "rm": "SO",
      "1234": 403014038
    },
    {
      "1": 5889,
      "roma": "LOZIO",
      "rm": "BS",
      "1234": 403017095
    },
    {
      "1": 5890,
      "roma": "LUINO",
      "rm": "VA",
      "1234": 403012092
    },
    {
      "1": 5891,
      "roma": "LUISAGO",
      "rm": "CO",
      "1234": 403013135
    },
    {
      "1": 5892,
      "roma": "LULA",
      "rm": "NU",
      "1234": 420091043
    },
    {
      "1": 5893,
      "roma": "LUMARZO",
      "rm": "GE",
      "1234": 407010031
    },
    {
      "1": 5894,
      "roma": "LUMEZZANE",
      "rm": "BS",
      "1234": 403017096
    },
    {
      "1": 5895,
      "roma": "LUNAMATRONA",
      "rm": "CA",
      "1234": 420092035
    },
    {
      "1": 5896,
      "roma": "LUNANO",
      "rm": "PS",
      "1234": 411041022
    },
    {
      "1": 5897,
      "roma": "LUNGAVILLA",
      "rm": "PV",
      "1234": 403018084
    },
    {
      "1": 5898,
      "roma": "LUNGRO",
      "rm": "CS",
      "1234": 418078069
    },
    {
      "1": 5899,
      "roma": "LUOGOSANO",
      "rm": "AV",
      "1234": 415064045
    },
    {
      "1": 5900,
      "roma": "LUOGOSANTO",
      "rm": "SS",
      "1234": 420090036
    },
    {
      "1": 5901,
      "roma": "LUPARA",
      "rm": "CB",
      "1234": 414070034
    },
    {
      "1": 5902,
      "roma": "LURAGO D'ERBA",
      "rm": "CO",
      "1234": 403013136
    },
    {
      "1": 5903,
      "roma": "LURAGO MARINONE",
      "rm": "CO",
      "1234": 403013137
    },
    {
      "1": 5904,
      "roma": "LURANO",
      "rm": "BG",
      "1234": 403016129
    },
    {
      "1": 5905,
      "roma": "LURAS",
      "rm": "SS",
      "1234": 420090037
    },
    {
      "1": 5906,
      "roma": "LURATE CACCIVIO",
      "rm": "CO",
      "1234": 403013138
    },
    {
      "1": 5907,
      "roma": "LUGNANO IN TEVERINA",
      "rm": "TR",
      "1234": 410055016
    },
    {
      "1": 5908,
      "roma": "LOVERE",
      "rm": "BG",
      "1234": 403016128
    },
    {
      "1": 5909,
      "roma": "LUGO DI VICENZA",
      "rm": "VI",
      "1234": 405024053
    },
    {
      "1": 5910,
      "roma": "LUSERNA",
      "rm": "TN",
      "1234": 404022109
    },
    {
      "1": 5911,
      "roma": "LUSERNA SAN GIOVANNI",
      "rm": "TO",
      "1234": 401001139
    },
    {
      "1": 5912,
      "roma": "LUSERNETTA",
      "rm": "TO",
      "1234": 401001140
    },
    {
      "1": 5913,
      "roma": "LUSEVERA",
      "rm": "UD",
      "1234": 406030051
    },
    {
      "1": 5914,
      "roma": "LUSIA",
      "rm": "RO",
      "1234": 405029031
    },
    {
      "1": 5915,
      "roma": "LUSIGLIE'",
      "rm": "TO",
      "1234": 401001141
    },
    {
      "1": 5916,
      "roma": "LUSON",
      "rm": "BZ",
      "1234": 404021044
    },
    {
      "1": 5917,
      "roma": "LUSTRA",
      "rm": "SA",
      "1234": 415065064
    },
    {
      "1": 5918,
      "roma": "LUVINATE",
      "rm": "VA",
      "1234": 403012093
    },
    {
      "1": 5919,
      "roma": "LUZZANA",
      "rm": "BG",
      "1234": 403016130
    },
    {
      "1": 5920,
      "roma": "LUZZARA",
      "rm": "RE",
      "1234": 408035026
    },
    {
      "1": 5921,
      "roma": "LUZZI",
      "rm": "CS",
      "1234": 418078070
    },
    {
      "1": 5922,
      "roma": "LUSCIANO",
      "rm": "CE",
      "1234": 415061046
    },
    {
      "1": 5923,
      "roma": "LUGO",
      "rm": "RA",
      "1234": 408039012
    },
    {
      "1": 5924,
      "roma": "MONTE SAN PIETRANGELI",
      "rm": "FM",
      "1234": 411109023
    },
    {
      "1": 5925,
      "roma": "MONTE URANO",
      "rm": "FM",
      "1234": 411109024
    },
    {
      "1": 5926,
      "roma": "MACCHIA VALFORTORE",
      "rm": "CB",
      "1234": 414070035
    },
    {
      "1": 5927,
      "roma": "MACCHIAGODENA",
      "rm": "IS",
      "1234": 414094026
    },
    {
      "1": 5928,
      "roma": "MACELLO",
      "rm": "TO",
      "1234": 401001142
    },
    {
      "1": 5929,
      "roma": "MACERATA",
      "rm": "MC",
      "1234": 411043023
    },
    {
      "1": 5930,
      "roma": "MACERATA CAMPANIA",
      "rm": "CE",
      "1234": 415061047
    },
    {
      "1": 5931,
      "roma": "MACERATA FELTRIA",
      "rm": "PS",
      "1234": 411041023
    },
    {
      "1": 5932,
      "roma": "MACLODIO",
      "rm": "BS",
      "1234": 403017097
    },
    {
      "1": 5933,
      "roma": "MACOMER",
      "rm": "NU",
      "1234": 420091044
    },
    {
      "1": 5934,
      "roma": "MACRA",
      "rm": "CN",
      "1234": 401004112
    },
    {
      "1": 5935,
      "roma": "MACUGNAGA",
      "rm": "VB",
      "1234": 401103039
    },
    {
      "1": 5936,
      "roma": "MADDALONI",
      "rm": "CE",
      "1234": 415061048
    },
    {
      "1": 5937,
      "roma": "MADESIMO",
      "rm": "SO",
      "1234": 403014035
    },
    {
      "1": 5938,
      "roma": "MACCASTORNA",
      "rm": "LO",
      "1234": 403098033
    },
    {
      "1": 5939,
      "roma": "MACCHIA D'ISERNIA",
      "rm": "IS",
      "1234": 414094025
    },
    {
      "1": 5940,
      "roma": "MADONNA DEL SASSO",
      "rm": "VB",
      "1234": 401103040
    },
    {
      "1": 5941,
      "roma": "MONTE VIDON COMBATTE",
      "rm": "FM",
      "1234": 411109025
    },
    {
      "1": 5942,
      "roma": "MAENZA",
      "rm": "LT",
      "1234": 412059013
    },
    {
      "1": 5943,
      "roma": "MAFALDA",
      "rm": "CB",
      "1234": 414070036
    },
    {
      "1": 5944,
      "roma": "MAGASA",
      "rm": "BS",
      "1234": 403017098
    },
    {
      "1": 5945,
      "roma": "MAGENTA",
      "rm": "MI",
      "1234": 403015130
    },
    {
      "1": 5946,
      "roma": "MAGGIORA",
      "rm": "NO",
      "1234": 401003088
    },
    {
      "1": 5947,
      "roma": "MAGHERNO",
      "rm": "PV",
      "1234": 403018085
    },
    {
      "1": 5948,
      "roma": "MAGIONE",
      "rm": "PG",
      "1234": 410054026
    },
    {
      "1": 5949,
      "roma": "MAGISANO",
      "rm": "CZ",
      "1234": 418079068
    },
    {
      "1": 5950,
      "roma": "MAGLIANO ALFIERI",
      "rm": "CN",
      "1234": 401004113
    },
    {
      "1": 5951,
      "roma": "MAGLIANO ALPI",
      "rm": "CN",
      "1234": 401004114
    },
    {
      "1": 5952,
      "roma": "MAGLIANO DE' MARSI",
      "rm": "AQ",
      "1234": 413066053
    },
    {
      "1": 5953,
      "roma": "MAGLIANO IN TOSCANA",
      "rm": "GR",
      "1234": 409053013
    },
    {
      "1": 5954,
      "roma": "MAGLIANO ROMANO",
      "rm": "RM",
      "1234": 412058052
    },
    {
      "1": 5955,
      "roma": "MAGLIANO VETERE",
      "rm": "SA",
      "1234": 415065065
    },
    {
      "1": 5956,
      "roma": "MAGLIE",
      "rm": "LE",
      "1234": 416075039
    },
    {
      "1": 5957,
      "roma": "MADIGNANO",
      "rm": "CR",
      "1234": 403019055
    },
    {
      "1": 5958,
      "roma": "MADONE",
      "rm": "BG",
      "1234": 403016131
    },
    {
      "1": 5959,
      "roma": "MAGNAGO",
      "rm": "MI",
      "1234": 403015131
    },
    {
      "1": 5960,
      "roma": "MAGNANO",
      "rm": "BI",
      "1234": 401096030
    },
    {
      "1": 5961,
      "roma": "MAGNANO IN RIVIERA",
      "rm": "UD",
      "1234": 406030052
    },
    {
      "1": 5962,
      "roma": "MAGLIANO DI TENNA",
      "rm": "FM",
      "1234": 411109010
    },
    {
      "1": 5963,
      "roma": "MAGOMADAS",
      "rm": "NU",
      "1234": 420091045
    },
    {
      "1": 5964,
      "roma": "MAGRE' SULLA STRADA DEL VINO",
      "rm": "BZ",
      "1234": 404021045
    },
    {
      "1": 5965,
      "roma": "MAGREGLIO",
      "rm": "CO",
      "1234": 403013139
    },
    {
      "1": 5966,
      "roma": "MAIDA",
      "rm": "CZ",
      "1234": 418079069
    },
    {
      "1": 5967,
      "roma": "MAIERA'",
      "rm": "CS",
      "1234": 418078071
    },
    {
      "1": 5968,
      "roma": "MAIERATO",
      "rm": "VV",
      "1234": 418102020
    },
    {
      "1": 5969,
      "roma": "MAGLIANO SABINA",
      "rm": "RI",
      "1234": 412057035
    },
    {
      "1": 5970,
      "roma": "MAIORI",
      "rm": "SA",
      "1234": 415065066
    },
    {
      "1": 5971,
      "roma": "MAIRAGO",
      "rm": "LO",
      "1234": 403098034
    },
    {
      "1": 5972,
      "roma": "MAGLIOLO",
      "rm": "SV",
      "1234": 407009035
    },
    {
      "1": 5973,
      "roma": "MAGLIONE",
      "rm": "TO",
      "1234": 401001143
    },
    {
      "1": 5974,
      "roma": "MAGNACAVALLO",
      "rm": "MN",
      "1234": 403020029
    },
    {
      "1": 5975,
      "roma": "MAJANO",
      "rm": "UD",
      "1234": 406030053
    },
    {
      "1": 5976,
      "roma": "MALAGNINO",
      "rm": "CR",
      "1234": 403019056
    },
    {
      "1": 5977,
      "roma": "MALALBERGO",
      "rm": "BO",
      "1234": 408037035
    },
    {
      "1": 5978,
      "roma": "MAIOLO",
      "rm": "RN",
      "1234": 408099022
    },
    {
      "1": 5979,
      "roma": "MALBORGHETTO-VALBRUNA",
      "rm": "UD",
      "1234": 406030054
    },
    {
      "1": 5980,
      "roma": "MALCESINE",
      "rm": "VR",
      "1234": 405023045
    },
    {
      "1": 5981,
      "roma": "MALE'",
      "rm": "TN",
      "1234": 404022110
    },
    {
      "1": 5982,
      "roma": "MALEGNO",
      "rm": "BS",
      "1234": 403017100
    },
    {
      "1": 5983,
      "roma": "MALEO",
      "rm": "LO",
      "1234": 403098035
    },
    {
      "1": 5984,
      "roma": "MAIOLATI SPONTINI",
      "rm": "AN",
      "1234": 411042023
    },
    {
      "1": 5985,
      "roma": "MALETTO",
      "rm": "CT",
      "1234": 419087022
    },
    {
      "1": 5986,
      "roma": "MALFA",
      "rm": "ME",
      "1234": 419083043
    },
    {
      "1": 5987,
      "roma": "MALGESSO",
      "rm": "VA",
      "1234": 403012095
    },
    {
      "1": 5988,
      "roma": "MAIRANO",
      "rm": "BS",
      "1234": 403017099
    },
    {
      "1": 5989,
      "roma": "MAISSANA",
      "rm": "SP",
      "1234": 407011018
    },
    {
      "1": 5990,
      "roma": "MALITO",
      "rm": "CS",
      "1234": 418078072
    },
    {
      "1": 5991,
      "roma": "MALLARE",
      "rm": "SV",
      "1234": 407009036
    },
    {
      "1": 5992,
      "roma": "MALLES VENOSTA",
      "rm": "BZ",
      "1234": 404021046
    },
    {
      "1": 5993,
      "roma": "MALNATE",
      "rm": "VA",
      "1234": 403012096
    },
    {
      "1": 5994,
      "roma": "MALO",
      "rm": "VI",
      "1234": 405024055
    },
    {
      "1": 5995,
      "roma": "MALONNO",
      "rm": "BS",
      "1234": 403017101
    },
    {
      "1": 5996,
      "roma": "MALOSCO",
      "rm": "TN",
      "1234": 404022111
    },
    {
      "1": 5997,
      "roma": "MONTE VIDON CORRADO",
      "rm": "FM",
      "1234": 411109026
    },
    {
      "1": 5998,
      "roma": "MALTIGNANO",
      "rm": "AP",
      "1234": 411044027
    },
    {
      "1": 5999,
      "roma": "MALVAGNA",
      "rm": "ME",
      "1234": 419083044
    },
    {
      "1": 6000,
      "roma": "MALVICINO",
      "rm": "AL",
      "1234": 401006090
    },
    {
      "1": 6001,
      "roma": "MALESCO",
      "rm": "VB",
      "1234": 401103041
    },
    {
      "1": 6646,
      "roma": "ORIGGIO",
      "rm": "VA",
      "1234": 403012109
    },
    {
      "1": 6647,
      "roma": "OPPEANO",
      "rm": "VR",
      "1234": 405023055
    },
    {
      "1": 6648,
      "roma": "OPPIDO LUCANO",
      "rm": "PZ",
      "1234": 417076056
    },
    {
      "1": 6649,
      "roma": "OPPIDO MAMERTINA",
      "rm": "RC",
      "1234": 418080055
    },
    {
      "1": 6650,
      "roma": "ORA",
      "rm": "BZ",
      "1234": 404021060
    },
    {
      "1": 6651,
      "roma": "ORANI",
      "rm": "NU",
      "1234": 420091061
    },
    {
      "1": 6652,
      "roma": "ORATINO",
      "rm": "CB",
      "1234": 414070049
    },
    {
      "1": 6653,
      "roma": "ORBASSANO",
      "rm": "TO",
      "1234": 401001171
    },
    {
      "1": 6654,
      "roma": "ORBETELLO",
      "rm": "GR",
      "1234": 409053018
    },
    {
      "1": 6655,
      "roma": "ORISTANO",
      "rm": "OR",
      "1234": 420095038
    },
    {
      "1": 6656,
      "roma": "ORCIANO PISANO",
      "rm": "PI",
      "1234": 409050023
    },
    {
      "1": 6657,
      "roma": "ORCO FEGLINO",
      "rm": "SV",
      "1234": 407009044
    },
    {
      "1": 6658,
      "roma": "ORDONA",
      "rm": "FG",
      "1234": 416071063
    },
    {
      "1": 6659,
      "roma": "ORERO",
      "rm": "GE",
      "1234": 407010042
    },
    {
      "1": 6660,
      "roma": "ORGIANO",
      "rm": "VI",
      "1234": 405024075
    },
    {
      "1": 6661,
      "roma": "ORGOSOLO",
      "rm": "NU",
      "1234": 420091062
    },
    {
      "1": 6662,
      "roma": "ORIA",
      "rm": "BR",
      "1234": 416074011
    },
    {
      "1": 6663,
      "roma": "ORRIA",
      "rm": "SA",
      "1234": 415065085
    },
    {
      "1": 6664,
      "roma": "ORSAGO",
      "rm": "TV",
      "1234": 405026053
    },
    {
      "1": 6665,
      "roma": "ORSARA BORMIDA",
      "rm": "AL",
      "1234": 401006119
    },
    {
      "1": 6666,
      "roma": "ORSARA DI PUGLIA",
      "rm": "FG",
      "1234": 416071035
    },
    {
      "1": 6667,
      "roma": "ORINO",
      "rm": "VA",
      "1234": 403012110
    },
    {
      "1": 6668,
      "roma": "ORIO AL SERIO",
      "rm": "BG",
      "1234": 403016150
    },
    {
      "1": 6669,
      "roma": "ORIO CANAVESE",
      "rm": "TO",
      "1234": 401001172
    },
    {
      "1": 6670,
      "roma": "ORIO LITTA",
      "rm": "LO",
      "1234": 403098042
    },
    {
      "1": 6671,
      "roma": "ORIOLO",
      "rm": "CS",
      "1234": 418078087
    },
    {
      "1": 6672,
      "roma": "ORIOLO ROMANO",
      "rm": "VT",
      "1234": 412056041
    },
    {
      "1": 6673,
      "roma": "ORMEA",
      "rm": "CN",
      "1234": 401004155
    },
    {
      "1": 6674,
      "roma": "ORMELLE",
      "rm": "TV",
      "1234": 405026052
    },
    {
      "1": 6675,
      "roma": "ORNAVASSO",
      "rm": "VB",
      "1234": 401103051
    },
    {
      "1": 6676,
      "roma": "ORNAGO",
      "rm": "MB",
      "1234": 403108036
    },
    {
      "1": 6677,
      "roma": "ORNICA",
      "rm": "BG",
      "1234": 403016151
    },
    {
      "1": 6678,
      "roma": "OROSEI",
      "rm": "NU",
      "1234": 420091063
    },
    {
      "1": 6679,
      "roma": "OROTELLI",
      "rm": "NU",
      "1234": 420091064
    },
    {
      "1": 6680,
      "roma": "ORTUCCHIO",
      "rm": "AQ",
      "1234": 413066064
    },
    {
      "1": 6681,
      "roma": "ORTUERI",
      "rm": "NU",
      "1234": 420091066
    },
    {
      "1": 6682,
      "roma": "ORUNE",
      "rm": "NU",
      "1234": 420091067
    },
    {
      "1": 6683,
      "roma": "ORVIETO",
      "rm": "TR",
      "1234": 410055023
    },
    {
      "1": 6684,
      "roma": "ORVINIO",
      "rm": "RI",
      "1234": 412057047
    },
    {
      "1": 6685,
      "roma": "ORSENIGO",
      "rm": "CO",
      "1234": 403013170
    },
    {
      "1": 6686,
      "roma": "ORSOGNA",
      "rm": "CH",
      "1234": 413069057
    },
    {
      "1": 6687,
      "roma": "ORSOMARSO",
      "rm": "CS",
      "1234": 418078088
    },
    {
      "1": 6688,
      "roma": "ORTA DI ATELLA",
      "rm": "CE",
      "1234": 415061053
    },
    {
      "1": 6689,
      "roma": "ORTA NOVA",
      "rm": "FG",
      "1234": 416071036
    },
    {
      "1": 6690,
      "roma": "ORTA SAN GIULIO",
      "rm": "NO",
      "1234": 401003112
    },
    {
      "1": 6691,
      "roma": "ORTACESUS",
      "rm": "CA",
      "1234": 420092044
    },
    {
      "1": 6692,
      "roma": "ORTELLE",
      "rm": "LE",
      "1234": 416075056
    },
    {
      "1": 6693,
      "roma": "ORTIGNANO RAGGIOLO",
      "rm": "AR",
      "1234": 409051027
    },
    {
      "1": 6694,
      "roma": "ORTISEI",
      "rm": "BZ",
      "1234": 404021061
    },
    {
      "1": 6695,
      "roma": "ORTONA",
      "rm": "CH",
      "1234": 413069058
    },
    {
      "1": 6696,
      "roma": "ORTONA DEI MARSI",
      "rm": "AQ",
      "1234": 413066063
    },
    {
      "1": 6697,
      "roma": "ORTEZZANO",
      "rm": "FM",
      "1234": 411109029
    },
    {
      "1": 6698,
      "roma": "ORTOVERO",
      "rm": "SV",
      "1234": 407009045
    },
    {
      "1": 6699,
      "roma": "OSPEDALETTO LODIGIANO",
      "rm": "LO",
      "1234": 403098043
    },
    {
      "1": 6700,
      "roma": "OSPITALE DI CADORE",
      "rm": "BL",
      "1234": 405025035
    },
    {
      "1": 6701,
      "roma": "OSPITALETTO BRESCIANO",
      "rm": "BS",
      "1234": 403017127
    },
    {
      "1": 6702,
      "roma": "OSSAGO LODIGIANO",
      "rm": "LO",
      "1234": 403098044
    },
    {
      "1": 6703,
      "roma": "ORZINUOVI",
      "rm": "BS",
      "1234": 403017125
    },
    {
      "1": 6704,
      "roma": "ORZIVECCHI",
      "rm": "BS",
      "1234": 403017126
    },
    {
      "1": 6705,
      "roma": "OSASCO",
      "rm": "TO",
      "1234": 401001173
    },
    {
      "1": 6706,
      "roma": "OSASIO",
      "rm": "TO",
      "1234": 401001174
    },
    {
      "1": 6707,
      "roma": "OSCHIRI",
      "rm": "SS",
      "1234": 420090049
    },
    {
      "1": 6708,
      "roma": "OSIDDA",
      "rm": "NU",
      "1234": 420091068
    },
    {
      "1": 6709,
      "roma": "OSIGLIA",
      "rm": "SV",
      "1234": 407009046
    },
    {
      "1": 6710,
      "roma": "OSILO",
      "rm": "SS",
      "1234": 420090050
    },
    {
      "1": 6711,
      "roma": "ORTE",
      "rm": "VT",
      "1234": 412056042
    },
    {
      "1": 6712,
      "roma": "OSIO SOPRA",
      "rm": "BG",
      "1234": 403016152
    },
    {
      "1": 6713,
      "roma": "OSIO SOTTO",
      "rm": "BG",
      "1234": 403016153
    },
    {
      "1": 6714,
      "roma": "OSNAGO",
      "rm": "LC",
      "1234": 403097061
    },
    {
      "1": 6715,
      "roma": "OSOPPO",
      "rm": "UD",
      "1234": 406030066
    },
    {
      "1": 6716,
      "roma": "OSPEDALETTI",
      "rm": "IM",
      "1234": 407008039
    },
    {
      "1": 6717,
      "roma": "OSPEDALETTO",
      "rm": "TN",
      "1234": 404022130
    },
    {
      "1": 6718,
      "roma": "OSPEDALETTO D'ALPINOLO",
      "rm": "AV",
      "1234": 415064067
    },
    {
      "1": 6719,
      "roma": "OSPEDALETTO EUGANEO",
      "rm": "PD",
      "1234": 405028059
    },
    {
      "1": 6720,
      "roma": "OTTANA",
      "rm": "NU",
      "1234": 420091070
    },
    {
      "1": 6721,
      "roma": "OTTATI",
      "rm": "SA",
      "1234": 415065086
    },
    {
      "1": 6722,
      "roma": "OTTAVIANO",
      "rm": "NA",
      "1234": 415063051
    },
    {
      "1": 6723,
      "roma": "OTTIGLIO",
      "rm": "AL",
      "1234": 401006120
    },
    {
      "1": 6724,
      "roma": "OTTOBIANO",
      "rm": "PV",
      "1234": 403018106
    },
    {
      "1": 6725,
      "roma": "OTTONE",
      "rm": "PC",
      "1234": 408033030
    },
    {
      "1": 6726,
      "roma": "OSSANA",
      "rm": "TN",
      "1234": 404022131
    },
    {
      "1": 6727,
      "roma": "OSSI",
      "rm": "SS",
      "1234": 420090051
    },
    {
      "1": 6728,
      "roma": "OSIMO",
      "rm": "AN",
      "1234": 411042034
    },
    {
      "1": 6729,
      "roma": "OSINI",
      "rm": "NU",
      "1234": 420091069
    },
    {
      "1": 6730,
      "roma": "OSSONA",
      "rm": "MI",
      "1234": 403015164
    },
    {
      "1": 6731,
      "roma": "OSTANA",
      "rm": "CN",
      "1234": 401004156
    },
    {
      "1": 6732,
      "roma": "OSTELLATO",
      "rm": "FE",
      "1234": 408038017
    },
    {
      "1": 6733,
      "roma": "OSTIANO",
      "rm": "CR",
      "1234": 403019064
    },
    {
      "1": 6734,
      "roma": "OSTIGLIA",
      "rm": "MN",
      "1234": 403020038
    },
    {
      "1": 6735,
      "roma": "OSTRA",
      "rm": "AN",
      "1234": 411042035
    },
    {
      "1": 6736,
      "roma": "OSTRA VETERE",
      "rm": "AN",
      "1234": 411042036
    },
    {
      "1": 6737,
      "roma": "OSTUNI",
      "rm": "BR",
      "1234": 416074012
    },
    {
      "1": 6738,
      "roma": "OTRANTO",
      "rm": "LE",
      "1234": 416075057
    },
    {
      "1": 6739,
      "roma": "OTRICOLI",
      "rm": "TR",
      "1234": 410055024
    },
    {
      "1": 6740,
      "roma": "PADERNA",
      "rm": "AL",
      "1234": 401006124
    },
    {
      "1": 6741,
      "roma": "PADERNO D'ADDA",
      "rm": "LC",
      "1234": 403097062
    },
    {
      "1": 6742,
      "roma": "OULX",
      "rm": "TO",
      "1234": 401001175
    },
    {
      "1": 6743,
      "roma": "OVADA",
      "rm": "AL",
      "1234": 401006121
    },
    {
      "1": 6744,
      "roma": "OVARO",
      "rm": "UD",
      "1234": 406030067
    },
    {
      "1": 6745,
      "roma": "OVIGLIO",
      "rm": "AL",
      "1234": 401006122
    },
    {
      "1": 6746,
      "roma": "OVINDOLI",
      "rm": "AQ",
      "1234": 413066065
    },
    {
      "1": 6747,
      "roma": "OVODDA",
      "rm": "NU",
      "1234": 420091071
    },
    {
      "1": 6748,
      "roma": "OYACE",
      "rm": "AO",
      "1234": 402007047
    },
    {
      "1": 6749,
      "roma": "OSSIMO",
      "rm": "BS",
      "1234": 403017128
    },
    {
      "1": 6750,
      "roma": "OZZANO DELL'EMILIA",
      "rm": "BO",
      "1234": 408037046
    },
    {
      "1": 6751,
      "roma": "OZZANO MONFERRATO",
      "rm": "AL",
      "1234": 401006123
    },
    {
      "1": 6752,
      "roma": "OZZERO",
      "rm": "MI",
      "1234": 403015165
    },
    {
      "1": 6753,
      "roma": "PABILLONIS",
      "rm": "CA",
      "1234": 420092045
    },
    {
      "1": 6754,
      "roma": "PACE DEL MELA",
      "rm": "ME",
      "1234": 419083064
    },
    {
      "1": 6755,
      "roma": "PACECO",
      "rm": "TP",
      "1234": 419081013
    },
    {
      "1": 6756,
      "roma": "PACENTRO",
      "rm": "AQ",
      "1234": 413066066
    },
    {
      "1": 6757,
      "roma": "PACHINO",
      "rm": "SR",
      "1234": 419089014
    },
    {
      "1": 6758,
      "roma": "PACIANO",
      "rm": "PG",
      "1234": 410054036
    },
    {
      "1": 6759,
      "roma": "PADENGHE SUL GARDA",
      "rm": "BS",
      "1234": 403017129
    },
    {
      "1": 6760,
      "roma": "PAGLIETA",
      "rm": "CH",
      "1234": 413069059
    },
    {
      "1": 6761,
      "roma": "PAGNACCO",
      "rm": "UD",
      "1234": 406030068
    },
    {
      "1": 6762,
      "roma": "PAGNO",
      "rm": "CN",
      "1234": 401004158
    },
    {
      "1": 6763,
      "roma": "PAGNONA",
      "rm": "LC",
      "1234": 403097063
    },
    {
      "1": 6764,
      "roma": "PADERNO DUGNANO",
      "rm": "MI",
      "1234": 403015166
    },
    {
      "1": 6765,
      "roma": "PADERNO FRANCIACORTA",
      "rm": "BS",
      "1234": 403017130
    },
    {
      "1": 6766,
      "roma": "PADERNO PONCHIELLI",
      "rm": "CR",
      "1234": 403019065
    },
    {
      "1": 6767,
      "roma": "PADOVA",
      "rm": "PD",
      "1234": 405028060
    },
    {
      "1": 6768,
      "roma": "PADRIA",
      "rm": "SS",
      "1234": 420090053
    },
    {
      "1": 6769,
      "roma": "PADRU",
      "rm": "SS",
      "1234": 420090090
    },
    {
      "1": 6770,
      "roma": "OZEGNA",
      "rm": "TO",
      "1234": 401001176
    },
    {
      "1": 6771,
      "roma": "OZIERI",
      "rm": "SS",
      "1234": 420090052
    },
    {
      "1": 6772,
      "roma": "PAESANA",
      "rm": "CN",
      "1234": 401004157
    },
    {
      "1": 6773,
      "roma": "PAESE",
      "rm": "TV",
      "1234": 405026055
    },
    {
      "1": 6774,
      "roma": "PAGANI",
      "rm": "SA",
      "1234": 415065088
    },
    {
      "1": 6775,
      "roma": "PAGANICO SABINO",
      "rm": "RI",
      "1234": 412057048
    },
    {
      "1": 6776,
      "roma": "PAGAZZANO",
      "rm": "BG",
      "1234": 403016154
    },
    {
      "1": 6777,
      "roma": "PAGLIARA",
      "rm": "ME",
      "1234": 419083065
    },
    {
      "1": 6778,
      "roma": "PALAZZAGO",
      "rm": "BG",
      "1234": 403016156
    },
    {
      "1": 6779,
      "roma": "PALAZZO ADRIANO",
      "rm": "PA",
      "1234": 419082052
    },
    {
      "1": 6780,
      "roma": "PALAZZO CANAVESE",
      "rm": "TO",
      "1234": 401001177
    },
    {
      "1": 6781,
      "roma": "PALAZZO PIGNANO",
      "rm": "CR",
      "1234": 403019066
    },
    {
      "1": 6782,
      "roma": "PALAZZO SAN GERVASIO",
      "rm": "PZ",
      "1234": 417076057
    },
    {
      "1": 6783,
      "roma": "PAGO DEL VALLO DI LAURO",
      "rm": "AV",
      "1234": 415064068
    },
    {
      "1": 6784,
      "roma": "PAGO VEIANO",
      "rm": "BN",
      "1234": 415062046
    },
    {
      "1": 6785,
      "roma": "PAISCO LOVENO",
      "rm": "BS",
      "1234": 403017131
    },
    {
      "1": 6786,
      "roma": "PADULA",
      "rm": "SA",
      "1234": 415065087
    },
    {
      "1": 6787,
      "roma": "PADULI",
      "rm": "BN",
      "1234": 415062045
    },
    {
      "1": 6788,
      "roma": "PALADINA",
      "rm": "BG",
      "1234": 403016155
    },
    {
      "1": 6789,
      "roma": "PALAGANO",
      "rm": "MO",
      "1234": 408036029
    },
    {
      "1": 6790,
      "roma": "PALAGIANELLO",
      "rm": "TA",
      "1234": 416073020
    },
    {
      "1": 6791,
      "roma": "PALAGIANO",
      "rm": "TA",
      "1234": 416073021
    },
    {
      "1": 6792,
      "roma": "PALAGONIA",
      "rm": "CT",
      "1234": 419087032
    },
    {
      "1": 6793,
      "roma": "PALAIA",
      "rm": "PI",
      "1234": 409050024
    },
    {
      "1": 6794,
      "roma": "PALANZANO",
      "rm": "PR",
      "1234": 408034026
    },
    {
      "1": 6795,
      "roma": "PALATA",
      "rm": "CB",
      "1234": 414070050
    },
    {
      "1": 6796,
      "roma": "PALAU",
      "rm": "SS",
      "1234": 420090054
    },
    {
      "1": 6797,
      "roma": "PALLARE",
      "rm": "SV",
      "1234": 407009047
    },
    {
      "1": 6798,
      "roma": "PALMA CAMPANIA",
      "rm": "NA",
      "1234": 415063052
    },
    {
      "1": 6799,
      "roma": "PALMA DI MONTECHIARO",
      "rm": "AG",
      "1234": 419084027
    },
    {
      "1": 6800,
      "roma": "PALMANOVA",
      "rm": "UD",
      "1234": 406030070
    },
    {
      "1": 6801,
      "roma": "PALMARIGGI",
      "rm": "LE",
      "1234": 416075058
    },
    {
      "1": 6802,
      "roma": "PALAZZOLO ACREIDE",
      "rm": "SR",
      "1234": 419089015
    },
    {
      "1": 6803,
      "roma": "PALAZZOLO DELLO STELLA",
      "rm": "UD",
      "1234": 406030069
    },
    {
      "1": 6804,
      "roma": "PALAZZOLO SULL'OGLIO",
      "rm": "BS",
      "1234": 403017133
    },
    {
      "1": 6805,
      "roma": "PALAZZOLO VERCELLESE",
      "rm": "VC",
      "1234": 401002090
    },
    {
      "1": 6806,
      "roma": "PALAZZUOLO SUL SENIO",
      "rm": "FI",
      "1234": 409048031
    },
    {
      "1": 6807,
      "roma": "PALENA",
      "rm": "CH",
      "1234": 413069060
    },
    {
      "1": 6808,
      "roma": "PAITONE",
      "rm": "BS",
      "1234": 403017132
    },
    {
      "1": 6809,
      "roma": "PALESTRINA",
      "rm": "RM",
      "1234": 412058074
    },
    {
      "1": 6810,
      "roma": "PALESTRO",
      "rm": "PV",
      "1234": 403018107
    },
    {
      "1": 6811,
      "roma": "PALIANO",
      "rm": "FR",
      "1234": 412060046
    },
    {
      "1": 6812,
      "roma": "PALIZZI",
      "rm": "RC",
      "1234": 418080056
    },
    {
      "1": 6813,
      "roma": "PALLAGORIO",
      "rm": "KR",
      "1234": 418101016
    },
    {
      "1": 6814,
      "roma": "PALLANZENO",
      "rm": "VB",
      "1234": 401103052
    },
    {
      "1": 6815,
      "roma": "PANCHIA'",
      "rm": "TN",
      "1234": 404022134
    },
    {
      "1": 6816,
      "roma": "PANDINO",
      "rm": "CR",
      "1234": 403019067
    },
    {
      "1": 6817,
      "roma": "PANETTIERI",
      "rm": "CS",
      "1234": 418078090
    },
    {
      "1": 6818,
      "roma": "PANICALE",
      "rm": "PG",
      "1234": 410054037
    },
    {
      "1": 6819,
      "roma": "PANNARANO",
      "rm": "BN",
      "1234": 415062047
    },
    {
      "1": 6820,
      "roma": "PALMAS ARBOREA",
      "rm": "OR",
      "1234": 420095039
    },
    {
      "1": 6821,
      "roma": "PALMI",
      "rm": "RC",
      "1234": 418080057
    },
    {
      "1": 6822,
      "roma": "PALMIANO",
      "rm": "AP",
      "1234": 411044056
    },
    {
      "1": 6823,
      "roma": "PALMOLI",
      "rm": "CH",
      "1234": 413069061
    },
    {
      "1": 6824,
      "roma": "PALO DEL COLLE",
      "rm": "BA",
      "1234": 416072033
    },
    {
      "1": 6825,
      "roma": "PALOMBARA SABINA",
      "rm": "RM",
      "1234": 412058075
    },
    {
      "1": 6826,
      "roma": "PALOMBARO",
      "rm": "CH",
      "1234": 413069062
    },
    {
      "1": 6827,
      "roma": "PALERMITI",
      "rm": "CZ",
      "1234": 418079089
    },
    {
      "1": 6828,
      "roma": "PALERMO",
      "rm": "PA",
      "1234": 419082053
    },
    {
      "1": 6829,
      "roma": "PALU'",
      "rm": "VR",
      "1234": 405023056
    },
    {
      "1": 6830,
      "roma": "PALU' DEL FERSINA",
      "rm": "TN",
      "1234": 404022133
    },
    {
      "1": 6831,
      "roma": "PALUDI",
      "rm": "CS",
      "1234": 418078089
    },
    {
      "1": 6832,
      "roma": "PALUZZA",
      "rm": "UD",
      "1234": 406030071
    },
    {
      "1": 6833,
      "roma": "PAMPARATO",
      "rm": "CN",
      "1234": 401004159
    },
    {
      "1": 6834,
      "roma": "PANCALIERI",
      "rm": "TO",
      "1234": 401001178
    },
    {
      "1": 6835,
      "roma": "PANCARANA",
      "rm": "PV",
      "1234": 403018108
    },
    {
      "1": 6836,
      "roma": "PARELLA",
      "rm": "TO",
      "1234": 401001179
    },
    {
      "1": 6837,
      "roma": "PARENTI",
      "rm": "CS",
      "1234": 418078093
    },
    {
      "1": 6838,
      "roma": "PARETE",
      "rm": "CE",
      "1234": 415061054
    },
    {
      "1": 6839,
      "roma": "PARETO",
      "rm": "AL",
      "1234": 401006125
    },
    {
      "1": 6840,
      "roma": "PARGHELIA",
      "rm": "VV",
      "1234": 418102026
    },
    {
      "1": 6841,
      "roma": "PORTO SANT'ELPIDIO",
      "rm": "FM",
      "1234": 411109034
    },
    {
      "1": 6842,
      "roma": "PANNI",
      "rm": "FG",
      "1234": 416071037
    },
    {
      "1": 6843,
      "roma": "PANTELLERIA",
      "rm": "TP",
      "1234": 419081014
    },
    {
      "1": 6844,
      "roma": "PANTIGLIATE",
      "rm": "MI",
      "1234": 403015167
    },
    {
      "1": 6845,
      "roma": "PAOLA",
      "rm": "CS",
      "1234": 418078091
    },
    {
      "1": 6846,
      "roma": "PALOMONTE",
      "rm": "SA",
      "1234": 415065089
    },
    {
      "1": 6847,
      "roma": "PALOSCO",
      "rm": "BG",
      "1234": 403016157
    },
    {
      "1": 6848,
      "roma": "PAPOZZE",
      "rm": "RO",
      "1234": 405029034
    },
    {
      "1": 6849,
      "roma": "PARABIAGO",
      "rm": "MI",
      "1234": 403015168
    },
    {
      "1": 6850,
      "roma": "PARABITA",
      "rm": "LE",
      "1234": 416075059
    },
    {
      "1": 6851,
      "roma": "PARATICO",
      "rm": "BS",
      "1234": 403017134
    },
    {
      "1": 6852,
      "roma": "PARCINES",
      "rm": "BZ",
      "1234": 404021062
    },
    {
      "1": 6853,
      "roma": "PASIAN DI PRATO",
      "rm": "UD",
      "1234": 406030072
    },
    {
      "1": 6854,
      "roma": "PASIANO DI PORDENONE",
      "rm": "PN",
      "1234": 406093029
    },
    {
      "1": 6855,
      "roma": "PASPARDO",
      "rm": "BS",
      "1234": 403017135
    },
    {
      "1": 6856,
      "roma": "PARLASCO",
      "rm": "LC",
      "1234": 403097064
    },
    {
      "1": 6857,
      "roma": "PREDAIA",
      "rm": "TN",
      "1234": 404022230
    },
    {
      "1": 6858,
      "roma": "PARMA",
      "rm": "PR",
      "1234": 408034027
    },
    {
      "1": 6859,
      "roma": "PAOLISI",
      "rm": "BN",
      "1234": 415062048
    },
    {
      "1": 6860,
      "roma": "PAPASIDERO",
      "rm": "CS",
      "1234": 418078092
    },
    {
      "1": 6861,
      "roma": "PARRANO",
      "rm": "TR",
      "1234": 410055025
    },
    {
      "1": 6002,
      "roma": "MAMMOLA",
      "rm": "RC",
      "1234": 418080044
    },
    {
      "1": 6003,
      "roma": "MAMOIADA",
      "rm": "NU",
      "1234": 420091046
    },
    {
      "1": 6004,
      "roma": "MANCIANO",
      "rm": "GR",
      "1234": 409053014
    },
    {
      "1": 6005,
      "roma": "MANDANICI",
      "rm": "ME",
      "1234": 419083045
    },
    {
      "1": 6006,
      "roma": "MALGRATE",
      "rm": "LC",
      "1234": 403097045
    },
    {
      "1": 6007,
      "roma": "MANDELLO DEL LARIO",
      "rm": "LC",
      "1234": 403097046
    },
    {
      "1": 6008,
      "roma": "MANDELLO VITTA",
      "rm": "NO",
      "1234": 401003090
    },
    {
      "1": 6009,
      "roma": "MANDURIA",
      "rm": "TA",
      "1234": 416073012
    },
    {
      "1": 6010,
      "roma": "MANERBA DEL GARDA",
      "rm": "BS",
      "1234": 403017102
    },
    {
      "1": 6011,
      "roma": "MANERBIO",
      "rm": "BS",
      "1234": 403017103
    },
    {
      "1": 6012,
      "roma": "MANFREDONIA",
      "rm": "FG",
      "1234": 416071029
    },
    {
      "1": 6013,
      "roma": "MANGO",
      "rm": "CN",
      "1234": 401004115
    },
    {
      "1": 6014,
      "roma": "MANGONE",
      "rm": "CS",
      "1234": 418078075
    },
    {
      "1": 6015,
      "roma": "MACHERIO",
      "rm": "MB",
      "1234": 403108029
    },
    {
      "1": 6016,
      "roma": "MANIACE",
      "rm": "CT",
      "1234": 419087057
    },
    {
      "1": 6017,
      "roma": "MANIAGO",
      "rm": "PN",
      "1234": 406093025
    },
    {
      "1": 6018,
      "roma": "MALVITO",
      "rm": "CS",
      "1234": 418078073
    },
    {
      "1": 6019,
      "roma": "MANOPPELLO",
      "rm": "PE",
      "1234": 413068022
    },
    {
      "1": 6020,
      "roma": "MANSUE'",
      "rm": "TV",
      "1234": 405026037
    },
    {
      "1": 6021,
      "roma": "MANTA",
      "rm": "CN",
      "1234": 401004116
    },
    {
      "1": 6022,
      "roma": "MANTELLO",
      "rm": "SO",
      "1234": 403014039
    },
    {
      "1": 6023,
      "roma": "MANTOVA",
      "rm": "MN",
      "1234": 403020030
    },
    {
      "1": 6024,
      "roma": "MANDAS",
      "rm": "CA",
      "1234": 420092036
    },
    {
      "1": 6025,
      "roma": "MANDATORICCIO",
      "rm": "CS",
      "1234": 418078074
    },
    {
      "1": 6026,
      "roma": "MANDELA",
      "rm": "RM",
      "1234": 412058053
    },
    {
      "1": 6027,
      "roma": "MANZIANA",
      "rm": "RM",
      "1234": 412058054
    },
    {
      "1": 6028,
      "roma": "MAPELLO",
      "rm": "BG",
      "1234": 403016132
    },
    {
      "1": 6029,
      "roma": "MARA",
      "rm": "SS",
      "1234": 420090038
    },
    {
      "1": 6030,
      "roma": "MARACALAGONIS",
      "rm": "CA",
      "1234": 420092037
    },
    {
      "1": 6031,
      "roma": "MARANELLO",
      "rm": "MO",
      "1234": 408036019
    },
    {
      "1": 6032,
      "roma": "MARANO DI NAPOLI",
      "rm": "NA",
      "1234": 415063041
    },
    {
      "1": 6033,
      "roma": "MARANO DI VALPOLICELLA",
      "rm": "VR",
      "1234": 405023046
    },
    {
      "1": 6034,
      "roma": "MARANO EQUO",
      "rm": "RM",
      "1234": 412058055
    },
    {
      "1": 6035,
      "roma": "MARANO LAGUNARE",
      "rm": "UD",
      "1234": 406030056
    },
    {
      "1": 6036,
      "roma": "MARANO MARCHESATO",
      "rm": "CS",
      "1234": 418078076
    },
    {
      "1": 6037,
      "roma": "MARANO PRINCIPATO",
      "rm": "CS",
      "1234": 418078077
    },
    {
      "1": 6038,
      "roma": "MARANO SUL PANARO",
      "rm": "MO",
      "1234": 408036020
    },
    {
      "1": 6039,
      "roma": "MAPPANO",
      "rm": "TO",
      "1234": 401001316
    },
    {
      "1": 6040,
      "roma": "MARANO TICINO",
      "rm": "NO",
      "1234": 401003091
    },
    {
      "1": 6041,
      "roma": "MANOCALZATI",
      "rm": "AV",
      "1234": 415064046
    },
    {
      "1": 6042,
      "roma": "MARANZANA",
      "rm": "AT",
      "1234": 401005061
    },
    {
      "1": 6043,
      "roma": "MARATEA",
      "rm": "PZ",
      "1234": 417076044
    },
    {
      "1": 6044,
      "roma": "MARCALLO CON CASONE",
      "rm": "MI",
      "1234": 403015134
    },
    {
      "1": 6045,
      "roma": "MARCARIA",
      "rm": "MN",
      "1234": 403020031
    },
    {
      "1": 6046,
      "roma": "MANZANO",
      "rm": "UD",
      "1234": 406030055
    },
    {
      "1": 6047,
      "roma": "MARCETELLI",
      "rm": "RI",
      "1234": 412057036
    },
    {
      "1": 6048,
      "roma": "MARCHENO",
      "rm": "BS",
      "1234": 403017104
    },
    {
      "1": 6049,
      "roma": "MARCHIROLO",
      "rm": "VA",
      "1234": 403012097
    },
    {
      "1": 6050,
      "roma": "MARCIANA",
      "rm": "LI",
      "1234": 409049010
    },
    {
      "1": 6051,
      "roma": "MARCIANA MARINA",
      "rm": "LI",
      "1234": 409049011
    },
    {
      "1": 6052,
      "roma": "MARCIANISE",
      "rm": "CE",
      "1234": 415061049
    },
    {
      "1": 6053,
      "roma": "MARCIANO DELLA CHIANA",
      "rm": "AR",
      "1234": 409051022
    },
    {
      "1": 6054,
      "roma": "MARCIGNAGO",
      "rm": "PV",
      "1234": 403018086
    },
    {
      "1": 6055,
      "roma": "MARCON",
      "rm": "VE",
      "1234": 405027020
    },
    {
      "1": 6056,
      "roma": "MARANO VICENTINO",
      "rm": "VI",
      "1234": 405024056
    },
    {
      "1": 6057,
      "roma": "MAREBBE",
      "rm": "BZ",
      "1234": 404021047
    },
    {
      "1": 6058,
      "roma": "MARENE",
      "rm": "CN",
      "1234": 401004117
    },
    {
      "1": 6059,
      "roma": "MARENO DI PIAVE",
      "rm": "TV",
      "1234": 405026038
    },
    {
      "1": 6060,
      "roma": "MARENTINO",
      "rm": "TO",
      "1234": 401001144
    },
    {
      "1": 6061,
      "roma": "MARETTO",
      "rm": "AT",
      "1234": 401005062
    },
    {
      "1": 6062,
      "roma": "MARCEDUSA",
      "rm": "CZ",
      "1234": 418079071
    },
    {
      "1": 6063,
      "roma": "MARCELLINA",
      "rm": "RM",
      "1234": 412058056
    },
    {
      "1": 6064,
      "roma": "MARCELLINARA",
      "rm": "CZ",
      "1234": 418079072
    },
    {
      "1": 6065,
      "roma": "MARIANA MANTOVANA",
      "rm": "MN",
      "1234": 403020032
    },
    {
      "1": 6066,
      "roma": "MARIANO COMENSE",
      "rm": "CO",
      "1234": 403013143
    },
    {
      "1": 6067,
      "roma": "MARIANO DEL FRIULI",
      "rm": "GO",
      "1234": 406031010
    },
    {
      "1": 6068,
      "roma": "MARIANOPOLI",
      "rm": "CL",
      "1234": 419085008
    },
    {
      "1": 6069,
      "roma": "MARIGLIANELLA",
      "rm": "NA",
      "1234": 415063042
    },
    {
      "1": 6070,
      "roma": "MARIGLIANO",
      "rm": "NA",
      "1234": 415063043
    },
    {
      "1": 6071,
      "roma": "MARINA DI GIOIOSA IONICA",
      "rm": "RC",
      "1234": 418080045
    },
    {
      "1": 6072,
      "roma": "MARINEO",
      "rm": "PA",
      "1234": 419082046
    },
    {
      "1": 6073,
      "roma": "MARINO",
      "rm": "RM",
      "1234": 412058057
    },
    {
      "1": 6074,
      "roma": "MARLENGO",
      "rm": "BZ",
      "1234": 404021048
    },
    {
      "1": 6075,
      "roma": "MACCAGNO CON PINO E VEDDASCA",
      "rm": "VA",
      "1234": 403012142
    },
    {
      "1": 6076,
      "roma": "MARMENTINO",
      "rm": "BS",
      "1234": 403017105
    },
    {
      "1": 6077,
      "roma": "MARMIROLO",
      "rm": "MN",
      "1234": 403020033
    },
    {
      "1": 6078,
      "roma": "MARGHERITA DI SAVOIA",
      "rm": "BT",
      "1234": 416110005
    },
    {
      "1": 6079,
      "roma": "MARMORA",
      "rm": "CN",
      "1234": 401004119
    },
    {
      "1": 6080,
      "roma": "MARNATE",
      "rm": "VA",
      "1234": 403012098
    },
    {
      "1": 6081,
      "roma": "MARGARITA",
      "rm": "CN",
      "1234": 401004118
    },
    {
      "1": 6082,
      "roma": "MARGNO",
      "rm": "LC",
      "1234": 403097047
    },
    {
      "1": 6083,
      "roma": "MAROSTICA",
      "rm": "VI",
      "1234": 405024057
    },
    {
      "1": 6084,
      "roma": "MARRADI",
      "rm": "FI",
      "1234": 409048026
    },
    {
      "1": 6085,
      "roma": "MARRUBIU",
      "rm": "OR",
      "1234": 420095025
    },
    {
      "1": 6086,
      "roma": "MARSAGLIA",
      "rm": "CN",
      "1234": 401004120
    },
    {
      "1": 6087,
      "roma": "MARSALA",
      "rm": "TP",
      "1234": 419081011
    },
    {
      "1": 6088,
      "roma": "MARSCIANO",
      "rm": "PG",
      "1234": 410054027
    },
    {
      "1": 6089,
      "roma": "MARSICO NUOVO",
      "rm": "PZ",
      "1234": 417076045
    },
    {
      "1": 6090,
      "roma": "MARSICOVETERE",
      "rm": "PZ",
      "1234": 417076046
    },
    {
      "1": 6091,
      "roma": "MARTA",
      "rm": "VT",
      "1234": 412056034
    },
    {
      "1": 6092,
      "roma": "MARTANO",
      "rm": "LE",
      "1234": 416075040
    },
    {
      "1": 6093,
      "roma": "MARTELLAGO",
      "rm": "VE",
      "1234": 405027021
    },
    {
      "1": 6094,
      "roma": "MARLIANA",
      "rm": "PT",
      "1234": 409047007
    },
    {
      "1": 6095,
      "roma": "MARTIGNANA DI PO",
      "rm": "CR",
      "1234": 403019057
    },
    {
      "1": 6096,
      "roma": "MARTIGNANO",
      "rm": "LE",
      "1234": 416075041
    },
    {
      "1": 6097,
      "roma": "MARTINA FRANCA",
      "rm": "TA",
      "1234": 416073013
    },
    {
      "1": 6098,
      "roma": "MONTE GRIMANO TERME",
      "rm": "PS",
      "1234": 411041935
    },
    {
      "1": 6099,
      "roma": "MARTINENGO",
      "rm": "BG",
      "1234": 403016133
    },
    {
      "1": 6100,
      "roma": "MARTINIANA PO",
      "rm": "CN",
      "1234": 401004121
    },
    {
      "1": 6101,
      "roma": "MARONE",
      "rm": "BS",
      "1234": 403017106
    },
    {
      "1": 6102,
      "roma": "MAROPATI",
      "rm": "RC",
      "1234": 418080046
    },
    {
      "1": 6103,
      "roma": "MARTONE",
      "rm": "RC",
      "1234": 418080047
    },
    {
      "1": 6104,
      "roma": "MARUDO",
      "rm": "LO",
      "1234": 403098036
    },
    {
      "1": 6105,
      "roma": "MARUGGIO",
      "rm": "TA",
      "1234": 416073014
    },
    {
      "1": 6106,
      "roma": "MARZABOTTO",
      "rm": "BO",
      "1234": 408037036
    },
    {
      "1": 6107,
      "roma": "MARZANO",
      "rm": "PV",
      "1234": 403018087
    },
    {
      "1": 6108,
      "roma": "MARZANO APPIO",
      "rm": "CE",
      "1234": 415061050
    },
    {
      "1": 6109,
      "roma": "MARZANO DI NOLA",
      "rm": "AV",
      "1234": 415064047
    },
    {
      "1": 6110,
      "roma": "MARZI",
      "rm": "CS",
      "1234": 418078078
    },
    {
      "1": 6111,
      "roma": "MARZIO",
      "rm": "VA",
      "1234": 403012099
    },
    {
      "1": 6112,
      "roma": "MASAINAS",
      "rm": "CA",
      "1234": 420092103
    },
    {
      "1": 6113,
      "roma": "MARTELLO",
      "rm": "BZ",
      "1234": 404021049
    },
    {
      "1": 6114,
      "roma": "MARTIGNACCO",
      "rm": "UD",
      "1234": 406030057
    },
    {
      "1": 6115,
      "roma": "MASCALI",
      "rm": "CT",
      "1234": 419087023
    },
    {
      "1": 6116,
      "roma": "MASCALUCIA",
      "rm": "CT",
      "1234": 419087024
    },
    {
      "1": 6117,
      "roma": "MASCHITO",
      "rm": "PZ",
      "1234": 417076047
    },
    {
      "1": 6118,
      "roma": "MASCIAGO PRIMO",
      "rm": "VA",
      "1234": 403012100
    },
    {
      "1": 6119,
      "roma": "MASER",
      "rm": "TV",
      "1234": 405026039
    },
    {
      "1": 6120,
      "roma": "MASERA",
      "rm": "VB",
      "1234": 401103042
    },
    {
      "1": 6121,
      "roma": "MADRUZZO",
      "rm": "TN",
      "1234": 404022243
    },
    {
      "1": 6122,
      "roma": "MARTINSICURO",
      "rm": "TE",
      "1234": 413067047
    },
    {
      "1": 6123,
      "roma": "MARTIRANO",
      "rm": "CZ",
      "1234": 418079073
    },
    {
      "1": 6124,
      "roma": "MARTIRANO LOMBARDO",
      "rm": "CZ",
      "1234": 418079074
    },
    {
      "1": 6125,
      "roma": "MARTIS",
      "rm": "SS",
      "1234": 420090039
    },
    {
      "1": 6126,
      "roma": "MASI TORELLO",
      "rm": "FE",
      "1234": 408038012
    },
    {
      "1": 6127,
      "roma": "MASIO",
      "rm": "AL",
      "1234": 401006091
    },
    {
      "1": 6128,
      "roma": "MASLIANICO",
      "rm": "CO",
      "1234": 403013144
    },
    {
      "1": 6129,
      "roma": "MASONE",
      "rm": "GE",
      "1234": 407010032
    },
    {
      "1": 6130,
      "roma": "MASSA",
      "rm": "MS",
      "1234": 409045010
    },
    {
      "1": 6131,
      "roma": "MASSA D'ALBE",
      "rm": "AQ",
      "1234": 413066054
    },
    {
      "1": 6132,
      "roma": "MASSA DI SOMMA",
      "rm": "NA",
      "1234": 415063092
    },
    {
      "1": 6133,
      "roma": "MASSA E COZZILE",
      "rm": "PT",
      "1234": 409047008
    },
    {
      "1": 6134,
      "roma": "MASATE",
      "rm": "MI",
      "1234": 403015136
    },
    {
      "1": 6135,
      "roma": "MASSA LOMBARDA",
      "rm": "RA",
      "1234": 408039013
    },
    {
      "1": 6136,
      "roma": "MASSA LUBRENSE",
      "rm": "NA",
      "1234": 415063044
    },
    {
      "1": 6137,
      "roma": "MASSA MARITTIMA",
      "rm": "GR",
      "1234": 409053015
    },
    {
      "1": 6138,
      "roma": "MASSA MARTANA",
      "rm": "PG",
      "1234": 410054028
    },
    {
      "1": 6139,
      "roma": "MASSAFRA",
      "rm": "TA",
      "1234": 416073015
    },
    {
      "1": 6140,
      "roma": "MASSALENGO",
      "rm": "LO",
      "1234": 403098037
    },
    {
      "1": 6141,
      "roma": "MASERA' DI PADOVA",
      "rm": "PD",
      "1234": 405028048
    },
    {
      "1": 6142,
      "roma": "MASERADA SUL PIAVE",
      "rm": "TV",
      "1234": 405026040
    },
    {
      "1": 6143,
      "roma": "MASI",
      "rm": "PD",
      "1234": 405028049
    },
    {
      "1": 6144,
      "roma": "MASSAROSA",
      "rm": "LU",
      "1234": 409046018
    },
    {
      "1": 6145,
      "roma": "MASSAZZA",
      "rm": "BI",
      "1234": 401096031
    },
    {
      "1": 6146,
      "roma": "MASSELLO",
      "rm": "TO",
      "1234": 401001145
    },
    {
      "1": 6147,
      "roma": "MASSERANO",
      "rm": "BI",
      "1234": 401096032
    },
    {
      "1": 6148,
      "roma": "MASSIGNANO",
      "rm": "AP",
      "1234": 411044029
    },
    {
      "1": 6149,
      "roma": "MASSINO VISCONTI",
      "rm": "NO",
      "1234": 401003093
    },
    {
      "1": 6150,
      "roma": "MASSIOLA",
      "rm": "VB",
      "1234": 401103043
    },
    {
      "1": 6151,
      "roma": "MASULLAS",
      "rm": "OR",
      "1234": 420095026
    },
    {
      "1": 6152,
      "roma": "MATELICA",
      "rm": "MC",
      "1234": 411043024
    },
    {
      "1": 6153,
      "roma": "MATERA",
      "rm": "MT",
      "1234": 417077014
    },
    {
      "1": 6154,
      "roma": "MATHI",
      "rm": "TO",
      "1234": 401001146
    },
    {
      "1": 6155,
      "roma": "MASSA FERMANA",
      "rm": "FM",
      "1234": 411109011
    },
    {
      "1": 6156,
      "roma": "MASSANZAGO",
      "rm": "PD",
      "1234": 405028050
    },
    {
      "1": 6157,
      "roma": "MATTIE",
      "rm": "TO",
      "1234": 401001147
    },
    {
      "1": 6158,
      "roma": "MATTINATA",
      "rm": "FG",
      "1234": 416071031
    },
    {
      "1": 6159,
      "roma": "MAZARA DEL VALLO",
      "rm": "TP",
      "1234": 419081012
    },
    {
      "1": 6160,
      "roma": "MAZZANO",
      "rm": "BS",
      "1234": 403017107
    },
    {
      "1": 6161,
      "roma": "MAZZANO ROMANO",
      "rm": "RM",
      "1234": 412058058
    },
    {
      "1": 6162,
      "roma": "MASSIMENO",
      "rm": "TN",
      "1234": 404022112
    },
    {
      "1": 6163,
      "roma": "MASSIMINO",
      "rm": "SV",
      "1234": 407009037
    },
    {
      "1": 6164,
      "roma": "MAZZARRA' SANT'ANDREA",
      "rm": "ME",
      "1234": 419083046
    },
    {
      "1": 6165,
      "roma": "MAZZARRONE",
      "rm": "CT",
      "1234": 419087056
    },
    {
      "1": 6166,
      "roma": "MAZZE'",
      "rm": "TO",
      "1234": 401001148
    },
    {
      "1": 6167,
      "roma": "MAZZIN",
      "rm": "TN",
      "1234": 404022113
    },
    {
      "1": 6168,
      "roma": "MAZZO DI VALTELLINA",
      "rm": "SO",
      "1234": 403014040
    },
    {
      "1": 6169,
      "roma": "MEANA DI SUSA",
      "rm": "TO",
      "1234": 401001149
    },
    {
      "1": 6170,
      "roma": "MEANA SARDO",
      "rm": "NU",
      "1234": 420091047
    },
    {
      "1": 6171,
      "roma": "MATINO",
      "rm": "LE",
      "1234": 416075042
    },
    {
      "1": 6172,
      "roma": "MATRICE",
      "rm": "CB",
      "1234": 414070037
    },
    {
      "1": 6173,
      "roma": "MEDA",
      "rm": "MB",
      "1234": 403108030
    },
    {
      "1": 6174,
      "roma": "MEDE",
      "rm": "PV",
      "1234": 403018088
    },
    {
      "1": 6175,
      "roma": "MEDEA",
      "rm": "GO",
      "1234": 406031011
    },
    {
      "1": 6176,
      "roma": "MEDESANO",
      "rm": "PR",
      "1234": 408034020
    },
    {
      "1": 6177,
      "roma": "MEDICINA",
      "rm": "BO",
      "1234": 408037037
    },
    {
      "1": 6178,
      "roma": "MEDIGLIA",
      "rm": "MI",
      "1234": 403015139
    },
    {
      "1": 6179,
      "roma": "MEDOLAGO",
      "rm": "BG",
      "1234": 403016250
    },
    {
      "1": 6180,
      "roma": "MEDOLE",
      "rm": "MN",
      "1234": 403020034
    },
    {
      "1": 6181,
      "roma": "MEDOLLA",
      "rm": "MO",
      "1234": 408036021
    },
    {
      "1": 6182,
      "roma": "MAZZARINO",
      "rm": "CL",
      "1234": 419085009
    },
    {
      "1": 6183,
      "roma": "MEGLIADINO SAN VITALE",
      "rm": "PD",
      "1234": 405028052
    },
    {
      "1": 6184,
      "roma": "MEINA",
      "rm": "NO",
      "1234": 401003095
    },
    {
      "1": 6185,
      "roma": "MELARA",
      "rm": "RO",
      "1234": 405029032
    },
    {
      "1": 6186,
      "roma": "MELAZZO",
      "rm": "AL",
      "1234": 401006092
    },
    {
      "1": 6187,
      "roma": "MELDOLA",
      "rm": "FO",
      "1234": 408040019
    },
    {
      "1": 6188,
      "roma": "MELE",
      "rm": "GE",
      "1234": 407010033
    },
    {
      "1": 6189,
      "roma": "MELEGNANO",
      "rm": "MI",
      "1234": 403015140
    },
    {
      "1": 6190,
      "roma": "MELFI",
      "rm": "PZ",
      "1234": 417076048
    },
    {
      "1": 6191,
      "roma": "MELICUCCA'",
      "rm": "RC",
      "1234": 418080048
    },
    {
      "1": 6192,
      "roma": "MELICUCCO",
      "rm": "RC",
      "1234": 418080049
    },
    {
      "1": 6193,
      "roma": "MELILLI",
      "rm": "SR",
      "1234": 419089012
    },
    {
      "1": 6194,
      "roma": "MELISSA",
      "rm": "KR",
      "1234": 418101014
    },
    {
      "1": 6195,
      "roma": "MELISSANO",
      "rm": "LE",
      "1234": 416075044
    },
    {
      "1": 6196,
      "roma": "MELITO DI NAPOLI",
      "rm": "NA",
      "1234": 415063045
    },
    {
      "1": 6197,
      "roma": "MELITO DI PORTO SALVO",
      "rm": "RC",
      "1234": 418080050
    },
    {
      "1": 6198,
      "roma": "MEDUNA DI LIVENZA",
      "rm": "TV",
      "1234": 405026041
    },
    {
      "1": 6199,
      "roma": "MEDUNO",
      "rm": "PN",
      "1234": 406093026
    },
    {
      "1": 6200,
      "roma": "MELLE",
      "rm": "CN",
      "1234": 401004122
    },
    {
      "1": 6201,
      "roma": "MELLO",
      "rm": "SO",
      "1234": 403014041
    },
    {
      "1": 6202,
      "roma": "MELPIGNANO",
      "rm": "LE",
      "1234": 416075045
    },
    {
      "1": 6203,
      "roma": "MELTINA",
      "rm": "BZ",
      "1234": 404021050
    },
    {
      "1": 6204,
      "roma": "MELZO",
      "rm": "MI",
      "1234": 403015142
    },
    {
      "1": 6205,
      "roma": "MENAGGIO",
      "rm": "CO",
      "1234": 403013145
    },
    {
      "1": 6206,
      "roma": "MENCONICO",
      "rm": "PV",
      "1234": 403018089
    },
    {
      "1": 6207,
      "roma": "MENDATICA",
      "rm": "IM",
      "1234": 407008034
    },
    {
      "1": 6208,
      "roma": "MELENDUGNO",
      "rm": "LE",
      "1234": 416075043
    },
    {
      "1": 6209,
      "roma": "MELETI",
      "rm": "LO",
      "1234": 403098038
    },
    {
      "1": 6210,
      "roma": "MEOLO",
      "rm": "VE",
      "1234": 405027022
    },
    {
      "1": 6211,
      "roma": "MERANA",
      "rm": "AL",
      "1234": 401006093
    },
    {
      "1": 6212,
      "roma": "MERANO",
      "rm": "BZ",
      "1234": 404021051
    },
    {
      "1": 6213,
      "roma": "MERATE",
      "rm": "LC",
      "1234": 403097048
    },
    {
      "1": 6214,
      "roma": "MERCALLO",
      "rm": "VA",
      "1234": 403012101
    },
    {
      "1": 6215,
      "roma": "MERCATELLO SUL METAURO",
      "rm": "PS",
      "1234": 411041025
    },
    {
      "1": 7865,
      "roma": "SANT'OMOBONO TERME",
      "rm": "BG",
      "1234": 403016992
    },
    {
      "1": 7866,
      "roma": "SAN MARTINO DALL'ARGINE",
      "rm": "MN",
      "1234": 403020059
    },
    {
      "1": 7867,
      "roma": "SAN MARTINO DEL LAGO",
      "rm": "CR",
      "1234": 403019091
    },
    {
      "1": 7868,
      "roma": "SAN MARTINO DI FINITA",
      "rm": "CS",
      "1234": 418078124
    },
    {
      "1": 7869,
      "roma": "SAN MARTINO DI LUPARI",
      "rm": "PD",
      "1234": 405028077
    },
    {
      "1": 7870,
      "roma": "SAN MARTINO DI VENEZZE",
      "rm": "RO",
      "1234": 405029044
    },
    {
      "1": 7871,
      "roma": "SAN MARTINO IN BADIA",
      "rm": "BZ",
      "1234": 404021082
    },
    {
      "1": 7872,
      "roma": "SAN MAURO LA BRUCA",
      "rm": "SA",
      "1234": 415065124
    },
    {
      "1": 7873,
      "roma": "SAN MARTINO IN PASSIRIA",
      "rm": "BZ",
      "1234": 404021083
    },
    {
      "1": 7874,
      "roma": "SAN MARTINO IN PENSILIS",
      "rm": "CB",
      "1234": 414070069
    },
    {
      "1": 7875,
      "roma": "SAN MARTINO IN RIO",
      "rm": "RE",
      "1234": 408035037
    },
    {
      "1": 7876,
      "roma": "SAN MARTINO IN STRADA",
      "rm": "LO",
      "1234": 403098048
    },
    {
      "1": 7877,
      "roma": "SAN MARTINO SANNITA",
      "rm": "BN",
      "1234": 415062065
    },
    {
      "1": 7878,
      "roma": "SAN MICHELE DI GANZARIA",
      "rm": "CT",
      "1234": 419087043
    },
    {
      "1": 7879,
      "roma": "SAN MARTINO SULLA MARRUCINA",
      "rm": "CH",
      "1234": 413069082
    },
    {
      "1": 7880,
      "roma": "SAN MARTINO VALLE CAUDINA",
      "rm": "AV",
      "1234": 415064083
    },
    {
      "1": 7881,
      "roma": "SAN MARZANO DI SAN GIUSEPPE",
      "rm": "TA",
      "1234": 416073025
    },
    {
      "1": 7882,
      "roma": "SAN MARZANO OLIVETO",
      "rm": "AT",
      "1234": 401005100
    },
    {
      "1": 7883,
      "roma": "SAN MARZANO SUL SARNO",
      "rm": "SA",
      "1234": 415065122
    },
    {
      "1": 7884,
      "roma": "SAN MASSIMO",
      "rm": "CB",
      "1234": 414070070
    },
    {
      "1": 7885,
      "roma": "SAN MAURIZIO CANAVESE",
      "rm": "TO",
      "1234": 401001248
    },
    {
      "1": 7886,
      "roma": "SAN MAURIZIO D'OPAGLIO",
      "rm": "NO",
      "1234": 401003133
    },
    {
      "1": 7887,
      "roma": "SAN MAURO CASTELVERDE",
      "rm": "PA",
      "1234": 419082065
    },
    {
      "1": 7888,
      "roma": "SAN MAURO CILENTO",
      "rm": "SA",
      "1234": 415065123
    },
    {
      "1": 7889,
      "roma": "SAN MAURO DI SALINE",
      "rm": "VR",
      "1234": 405023074
    },
    {
      "1": 7890,
      "roma": "SAN MAURO FORTE",
      "rm": "MT",
      "1234": 417077026
    },
    {
      "1": 7891,
      "roma": "SAN NICOLA BARONIA",
      "rm": "AV",
      "1234": 415064085
    },
    {
      "1": 7892,
      "roma": "SAN MAURO MARCHESATO",
      "rm": "KR",
      "1234": 418101020
    },
    {
      "1": 7893,
      "roma": "SAN MAURO PASCOLI",
      "rm": "FO",
      "1234": 408040041
    },
    {
      "1": 7894,
      "roma": "SAN MAURO TORINESE",
      "rm": "TO",
      "1234": 401001249
    },
    {
      "1": 7895,
      "roma": "SAN MICHELE AL TAGLIAMENTO",
      "rm": "VE",
      "1234": 405027034
    },
    {
      "1": 7896,
      "roma": "SAN MICHELE ALL'ADIGE",
      "rm": "TN",
      "1234": 404022167
    },
    {
      "1": 7897,
      "roma": "SAN NICOLO' GERREI",
      "rm": "CA",
      "1234": 420092058
    },
    {
      "1": 7898,
      "roma": "SAN MICHELE DI SERINO",
      "rm": "AV",
      "1234": 415064084
    },
    {
      "1": 7899,
      "roma": "SAN MICHELE MONDOVI'",
      "rm": "CN",
      "1234": 401004210
    },
    {
      "1": 7900,
      "roma": "SAN MICHELE SALENTINO",
      "rm": "BR",
      "1234": 416074014
    },
    {
      "1": 7901,
      "roma": "SAN MINIATO",
      "rm": "PI",
      "1234": 409050032
    },
    {
      "1": 7902,
      "roma": "SAN NAZZARO",
      "rm": "BN",
      "1234": 415062066
    },
    {
      "1": 7903,
      "roma": "SAN NAZZARO SESIA",
      "rm": "NO",
      "1234": 401003134
    },
    {
      "1": 7904,
      "roma": "SAN NAZZARO VAL CAVARGNA",
      "rm": "CO",
      "1234": 403013207
    },
    {
      "1": 7905,
      "roma": "SAN NICOLA ARCELLA",
      "rm": "CS",
      "1234": 418078125
    },
    {
      "1": 7906,
      "roma": "SAN NICOLA DA CRISSA",
      "rm": "VV",
      "1234": 418102035
    },
    {
      "1": 7907,
      "roma": "SAN NICOLA DELL'ALTO",
      "rm": "KR",
      "1234": 418101021
    },
    {
      "1": 7908,
      "roma": "SAN NICOLA LA STRADA",
      "rm": "CE",
      "1234": 415061078
    },
    {
      "1": 7909,
      "roma": "SAN NICOLA MANFREDI",
      "rm": "BN",
      "1234": 415062067
    },
    {
      "1": 7910,
      "roma": "SAN NICOLO' D'ARCIDANO",
      "rm": "OR",
      "1234": 420095046
    },
    {
      "1": 7911,
      "roma": "SAN NICOLO' DI COMELICO",
      "rm": "BL",
      "1234": 405025046
    },
    {
      "1": 7912,
      "roma": "SAN PANCRAZIO",
      "rm": "BZ",
      "1234": 404021084
    },
    {
      "1": 7913,
      "roma": "SAN PANCRAZIO SALENTINO",
      "rm": "BR",
      "1234": 416074015
    },
    {
      "1": 7914,
      "roma": "SAN PAOLO",
      "rm": "BS",
      "1234": 403017138
    },
    {
      "1": 7915,
      "roma": "SAN PAOLO ALBANESE",
      "rm": "PZ",
      "1234": 417076020
    },
    {
      "1": 7916,
      "roma": "SAN PAOLO BEL SITO",
      "rm": "NA",
      "1234": 415063069
    },
    {
      "1": 7917,
      "roma": "SAN PAOLO D'ARGON",
      "rm": "BG",
      "1234": 403016189
    },
    {
      "1": 7918,
      "roma": "SAN PAOLO DI CIVITATE",
      "rm": "FG",
      "1234": 416071050
    },
    {
      "1": 7919,
      "roma": "SAN PAOLO DI JESI",
      "rm": "AN",
      "1234": 411042042
    },
    {
      "1": 7920,
      "roma": "SAN PIETRO DI CADORE",
      "rm": "BL",
      "1234": 405025047
    },
    {
      "1": 7921,
      "roma": "SAN PAOLO SOLBRITO",
      "rm": "AT",
      "1234": 401005101
    },
    {
      "1": 7922,
      "roma": "SAN PELLEGRINO TERME",
      "rm": "BG",
      "1234": 403016190
    },
    {
      "1": 7923,
      "roma": "SAN PIER D'ISONZO",
      "rm": "GO",
      "1234": 406031021
    },
    {
      "1": 7924,
      "roma": "SAN PIER NICETO",
      "rm": "ME",
      "1234": 419083080
    },
    {
      "1": 7925,
      "roma": "SAN PIERO PATTI",
      "rm": "ME",
      "1234": 419083081
    },
    {
      "1": 7926,
      "roma": "SAN PIETRO A MAIDA",
      "rm": "CZ",
      "1234": 418079114
    },
    {
      "1": 7927,
      "roma": "SAN PIETRO AL NATISONE",
      "rm": "UD",
      "1234": 406030103
    },
    {
      "1": 7928,
      "roma": "SAN PIETRO AL TANAGRO",
      "rm": "SA",
      "1234": 415065125
    },
    {
      "1": 7929,
      "roma": "SAN PIETRO APOSTOLO",
      "rm": "CZ",
      "1234": 418079115
    },
    {
      "1": 7930,
      "roma": "SAN PIETRO AVELLANA",
      "rm": "IS",
      "1234": 414094043
    },
    {
      "1": 7931,
      "roma": "SAN PIETRO CLARENZA",
      "rm": "CT",
      "1234": 419087044
    },
    {
      "1": 7932,
      "roma": "SAN POLO MATESE",
      "rm": "CB",
      "1234": 414070071
    },
    {
      "1": 7933,
      "roma": "SAN PIETRO DI CARIDA'",
      "rm": "RC",
      "1234": 418080075
    },
    {
      "1": 7934,
      "roma": "SAN PIETRO DI FELETTO",
      "rm": "TV",
      "1234": 405026073
    },
    {
      "1": 7935,
      "roma": "SAN PIETRO DI MORUBIO",
      "rm": "VR",
      "1234": 405023075
    },
    {
      "1": 7936,
      "roma": "SAN PIETRO IN AMANTEA",
      "rm": "CS",
      "1234": 418078126
    },
    {
      "1": 7937,
      "roma": "SAN PIETRO IN CARIANO",
      "rm": "VR",
      "1234": 405023076
    },
    {
      "1": 7938,
      "roma": "SAN PIETRO IN CASALE",
      "rm": "BO",
      "1234": 408037055
    },
    {
      "1": 7939,
      "roma": "SAN PIETRO IN CERRO",
      "rm": "PC",
      "1234": 408033041
    },
    {
      "1": 7940,
      "roma": "SAN PIETRO IN GU",
      "rm": "PD",
      "1234": 405028078
    },
    {
      "1": 7941,
      "roma": "SAN PIETRO IN GUARANO",
      "rm": "CS",
      "1234": 418078127
    },
    {
      "1": 7942,
      "roma": "SAN PIETRO IN LAMA",
      "rm": "LE",
      "1234": 416075071
    },
    {
      "1": 7943,
      "roma": "SAN PIETRO INFINE",
      "rm": "CE",
      "1234": 415061079
    },
    {
      "1": 7944,
      "roma": "SAN PIETRO MOSEZZO",
      "rm": "NO",
      "1234": 401003135
    },
    {
      "1": 7945,
      "roma": "SAN PIETRO MUSSOLINO",
      "rm": "VI",
      "1234": 405024094
    },
    {
      "1": 7946,
      "roma": "SAN PIETRO VAL LEMINA",
      "rm": "TO",
      "1234": 401001250
    },
    {
      "1": 7947,
      "roma": "SAN PIETRO VERNOTICO",
      "rm": "BR",
      "1234": 416074016
    },
    {
      "1": 7948,
      "roma": "SAN PIETRO VIMINARIO",
      "rm": "PD",
      "1234": 405028079
    },
    {
      "1": 7949,
      "roma": "SAN PIO DELLE CAMERE",
      "rm": "AQ",
      "1234": 413066088
    },
    {
      "1": 7950,
      "roma": "SAN POLO D'ENZA",
      "rm": "RE",
      "1234": 408035038
    },
    {
      "1": 7951,
      "roma": "SAN POLO DEI CAVALIERI",
      "rm": "RM",
      "1234": 412058096
    },
    {
      "1": 7952,
      "roma": "SAN POLO DI PIAVE",
      "rm": "TV",
      "1234": 405026074
    },
    {
      "1": 7953,
      "roma": "SAN SEBASTIANO AL VESUVIO",
      "rm": "NA",
      "1234": 415063070
    },
    {
      "1": 7954,
      "roma": "SAN PONSO",
      "rm": "TO",
      "1234": 401001251
    },
    {
      "1": 7955,
      "roma": "SAN POSSIDONIO",
      "rm": "MO",
      "1234": 408036038
    },
    {
      "1": 7956,
      "roma": "SAN POTITO SANNITICO",
      "rm": "CE",
      "1234": 415061080
    },
    {
      "1": 7957,
      "roma": "SAN POTITO ULTRA",
      "rm": "AV",
      "1234": 415064086
    },
    {
      "1": 7958,
      "roma": "SAN PRISCO",
      "rm": "CE",
      "1234": 415061081
    },
    {
      "1": 7959,
      "roma": "SAN PROCOPIO",
      "rm": "RC",
      "1234": 418080076
    },
    {
      "1": 7960,
      "roma": "SAN PROSPERO",
      "rm": "MO",
      "1234": 408036039
    },
    {
      "1": 7961,
      "roma": "SAN QUIRICO D'ORCIA",
      "rm": "SI",
      "1234": 409052030
    },
    {
      "1": 7962,
      "roma": "SAN QUIRINO",
      "rm": "PN",
      "1234": 406093040
    },
    {
      "1": 7963,
      "roma": "SAN RAFFAELE CIMENA",
      "rm": "TO",
      "1234": 401001252
    },
    {
      "1": 7964,
      "roma": "SANREMO",
      "rm": "IM",
      "1234": 407008055
    },
    {
      "1": 7965,
      "roma": "SAN ROBERTO",
      "rm": "RC",
      "1234": 418080077
    },
    {
      "1": 7966,
      "roma": "SAN ROCCO AL PORTO",
      "rm": "LO",
      "1234": 403098049
    },
    {
      "1": 7967,
      "roma": "SAN ROMANO IN GARFAGNANA",
      "rm": "LU",
      "1234": 409046027
    },
    {
      "1": 7968,
      "roma": "SAN RUFO",
      "rm": "SA",
      "1234": 415065126
    },
    {
      "1": 7969,
      "roma": "SAN SALVATORE DI FITALIA",
      "rm": "ME",
      "1234": 419083082
    },
    {
      "1": 7970,
      "roma": "SAN SALVATORE MONFERRATO",
      "rm": "AL",
      "1234": 401006154
    },
    {
      "1": 7971,
      "roma": "SAN SALVATORE TELESINO",
      "rm": "BN",
      "1234": 415062068
    },
    {
      "1": 7972,
      "roma": "SAN SALVO",
      "rm": "CH",
      "1234": 413069083
    },
    {
      "1": 7973,
      "roma": "SAN STINO DI LIVENZA",
      "rm": "VE",
      "1234": 405027936
    },
    {
      "1": 7974,
      "roma": "SAN VALENTINO TORIO",
      "rm": "SA",
      "1234": 415065132
    },
    {
      "1": 7975,
      "roma": "SAN SEBASTIANO CURONE",
      "rm": "AL",
      "1234": 401006155
    },
    {
      "1": 7976,
      "roma": "SAN SEBASTIANO DA PO",
      "rm": "TO",
      "1234": 401001253
    },
    {
      "1": 7977,
      "roma": "SAN SECONDO DI PINEROLO",
      "rm": "TO",
      "1234": 401001254
    },
    {
      "1": 7978,
      "roma": "SAN SECONDO PARMENSE",
      "rm": "PR",
      "1234": 408034033
    },
    {
      "1": 7979,
      "roma": "SAN SEVERINO LUCANO",
      "rm": "PZ",
      "1234": 417076078
    },
    {
      "1": 7980,
      "roma": "SAN SEVERINO MARCHE",
      "rm": "MC",
      "1234": 411043047
    },
    {
      "1": 7981,
      "roma": "SAN SEVERO",
      "rm": "FG",
      "1234": 416071051
    },
    {
      "1": 7982,
      "roma": "SAN SIRO",
      "rm": "CO",
      "1234": 403013248
    },
    {
      "1": 7983,
      "roma": "SAN SOSSIO BARONIA",
      "rm": "AV",
      "1234": 415064087
    },
    {
      "1": 7984,
      "roma": "SAN SOSTENE",
      "rm": "CZ",
      "1234": 418079116
    },
    {
      "1": 7985,
      "roma": "SAN SOSTI",
      "rm": "CS",
      "1234": 418078128
    },
    {
      "1": 7986,
      "roma": "SAN SPERATE",
      "rm": "CA",
      "1234": 420092059
    },
    {
      "1": 7987,
      "roma": "SAN TAMMARO",
      "rm": "CE",
      "1234": 415061085
    },
    {
      "1": 7988,
      "roma": "SAN TEODORO",
      "rm": "NU",
      "1234": 420091076
    },
    {
      "1": 7990,
      "roma": "SAN TOMASO AGORDINO",
      "rm": "BL",
      "1234": 405025049
    },
    {
      "1": 7991,
      "roma": "SAN VALENTINO IN ABRUZZO CITERIORE",
      "rm": "PE",
      "1234": 413068038
    },
    {
      "1": 7992,
      "roma": "SAN VITO LO CAPO",
      "rm": "TP",
      "1234": 419081020
    },
    {
      "1": 7993,
      "roma": "SAN VENANZO",
      "rm": "TR",
      "1234": 410055030
    },
    {
      "1": 7994,
      "roma": "SAN VENDEMIANO",
      "rm": "TV",
      "1234": 405026076
    },
    {
      "1": 7995,
      "roma": "SAN VERO MILIS",
      "rm": "OR",
      "1234": 420095050
    },
    {
      "1": 7996,
      "roma": "SAN VINCENZO",
      "rm": "LI",
      "1234": 409049018
    },
    {
      "1": 7997,
      "roma": "SAN VINCENZO LA COSTA",
      "rm": "CS",
      "1234": 418078135
    },
    {
      "1": 7998,
      "roma": "SAN VINCENZO VALLE ROVETO",
      "rm": "AQ",
      "1234": 413066092
    },
    {
      "1": 7999,
      "roma": "SAN VITALIANO",
      "rm": "NA",
      "1234": 415063075
    },
    {
      "1": 8000,
      "roma": "SAN VITO",
      "rm": "CA",
      "1234": 420092064
    },
    {
      "1": 8001,
      "roma": "SAN VITO AL TAGLIAMENTO",
      "rm": "PN",
      "1234": 406093041
    },
    {
      "1": 8002,
      "roma": "SAN VITO AL TORRE",
      "rm": "UD",
      "1234": 406030105
    },
    {
      "1": 8003,
      "roma": "SAN VITO CHIETINO",
      "rm": "CH",
      "1234": 413069086
    },
    {
      "1": 8004,
      "roma": "SAN VITO DEI NORMANNI",
      "rm": "BR",
      "1234": 416074017
    },
    {
      "1": 8005,
      "roma": "SAN VITO DI CADORE",
      "rm": "BL",
      "1234": 405025051
    },
    {
      "1": 8006,
      "roma": "SAN VITO DI FAGAGNA",
      "rm": "UD",
      "1234": 406030106
    },
    {
      "1": 8007,
      "roma": "SAN VITO DI LEGUZZANO",
      "rm": "VI",
      "1234": 405024096
    },
    {
      "1": 8008,
      "roma": "SANNICANDRO DI BARI",
      "rm": "BA",
      "1234": 416072040
    },
    {
      "1": 8009,
      "roma": "SAN NICANDRO GARGANICO",
      "rm": "FG",
      "1234": 416071049
    },
    {
      "1": 8010,
      "roma": "SANNICOLA",
      "rm": "LE",
      "1234": 416075070
    },
    {
      "1": 8011,
      "roma": "SAN VITO ROMANO",
      "rm": "RM",
      "1234": 412058100
    },
    {
      "1": 8012,
      "roma": "SAN VITO SULLO IONIO",
      "rm": "CZ",
      "1234": 418079122
    },
    {
      "1": 8013,
      "roma": "SAN VITTORE DEL LAZIO",
      "rm": "FR",
      "1234": 412060070
    },
    {
      "1": 8014,
      "roma": "SAN VITTORE OLONA",
      "rm": "MI",
      "1234": 403015201
    },
    {
      "1": 8015,
      "roma": "SAN ZENO DI MONTAGNA",
      "rm": "VR",
      "1234": 405023079
    },
    {
      "1": 8016,
      "roma": "SAN ZENO NAVIGLIO",
      "rm": "BS",
      "1234": 403017173
    },
    {
      "1": 8017,
      "roma": "SAN ZENONE AL PO",
      "rm": "PV",
      "1234": 403018145
    },
    {
      "1": 8018,
      "roma": "SAN ZENONE DEGLI EZZELINI",
      "rm": "TV",
      "1234": 405026077
    },
    {
      "1": 8019,
      "roma": "SANARICA",
      "rm": "LE",
      "1234": 416075067
    },
    {
      "1": 8020,
      "roma": "SANDIGLIANO",
      "rm": "BI",
      "1234": 401096059
    },
    {
      "1": 8021,
      "roma": "SANDRIGO",
      "rm": "VI",
      "1234": 405024091
    },
    {
      "1": 8022,
      "roma": "SANFRE'",
      "rm": "CN",
      "1234": 401004208
    },
    {
      "1": 8023,
      "roma": "SANFRONT",
      "rm": "CN",
      "1234": 401004209
    },
    {
      "1": 8024,
      "roma": "SANGANO",
      "rm": "TO",
      "1234": 401001241
    },
    {
      "1": 8025,
      "roma": "SANGIANO",
      "rm": "VA",
      "1234": 403012141
    },
    {
      "1": 8026,
      "roma": "SANGINETO",
      "rm": "CS",
      "1234": 418078117
    },
    {
      "1": 8027,
      "roma": "SANGUINETTO",
      "rm": "VR",
      "1234": 405023072
    },
    {
      "1": 8028,
      "roma": "SANLURI",
      "rm": "CA",
      "1234": 420092057
    },
    {
      "1": 8029,
      "roma": "SANNAZZARO DE' BURGONDI",
      "rm": "PV",
      "1234": 403018138
    },
    {
      "1": 8030,
      "roma": "SANT'ALESSIO CON VIALONE",
      "rm": "PV",
      "1234": 403018141
    },
    {
      "1": 8031,
      "roma": "SANT'ALESSIO D'ASPROMONTE",
      "rm": "RC",
      "1234": 418080080
    },
    {
      "1": 8032,
      "roma": "SANSEPOLCRO",
      "rm": "AR",
      "1234": 409051034
    },
    {
      "1": 8033,
      "roma": "SANT'AGAPITO",
      "rm": "IS",
      "1234": 414094044
    },
    {
      "1": 8034,
      "roma": "SANT'AGATA BOLOGNESE",
      "rm": "BO",
      "1234": 408037056
    },
    {
      "1": 8035,
      "roma": "SANT'AGATA DE' GOTI",
      "rm": "BN",
      "1234": 415062070
    },
    {
      "1": 8036,
      "roma": "SANT'AGATA DEL BIANCO",
      "rm": "RC",
      "1234": 418080079
    },
    {
      "1": 8037,
      "roma": "SAN ZENONE AL LAMBRO",
      "rm": "MI",
      "1234": 403015202
    },
    {
      "1": 8038,
      "roma": "SANT'AGATA DI MILITELLO",
      "rm": "ME",
      "1234": 419083084
    },
    {
      "1": 5783,
      "roma": "LICENZA",
      "rm": "RM",
      "1234": 412058051
    },
    {
      "1": 8040,
      "roma": "SANT'AGATA FOSSILI",
      "rm": "AL",
      "1234": 401006156
    },
    {
      "1": 8041,
      "roma": "SANT'AGATA LI BATTIATI",
      "rm": "CT",
      "1234": 419087045
    },
    {
      "1": 8042,
      "roma": "SANT'AGATA SUL SANTERNO",
      "rm": "RA",
      "1234": 408039017
    },
    {
      "1": 8043,
      "roma": "SANT'AGNELLO",
      "rm": "NA",
      "1234": 415063071
    },
    {
      "1": 8044,
      "roma": "SANT'ALBANO STURA",
      "rm": "CN",
      "1234": 401004211
    },
    {
      "1": 8045,
      "roma": "SANT'ANGELO DEI LOMBARDI",
      "rm": "AV",
      "1234": 415064092
    },
    {
      "1": 8046,
      "roma": "SANT'ANGELO DEL PESCO",
      "rm": "IS",
      "1234": 414094046
    },
    {
      "1": 8047,
      "roma": "SANT'ANGELO DI BROLO",
      "rm": "ME",
      "1234": 419083088
    },
    {
      "1": 8048,
      "roma": "SANT'ALESSIO SICULO",
      "rm": "ME",
      "1234": 419083085
    },
    {
      "1": 8049,
      "roma": "SANT'ALFIO",
      "rm": "CT",
      "1234": 419087046
    },
    {
      "1": 8050,
      "roma": "SANT'AGATA FELTRIA",
      "rm": "RN",
      "1234": 408099026
    },
    {
      "1": 8051,
      "roma": "SANT'AMBROGIO DI TORINO",
      "rm": "TO",
      "1234": 401001255
    },
    {
      "1": 8052,
      "roma": "SANT'AMBROGIO DI VALPOLICELLA",
      "rm": "VR",
      "1234": 405023077
    },
    {
      "1": 8053,
      "roma": "SANT'AMBROGIO SUL GARIGLIANO",
      "rm": "FR",
      "1234": 412060065
    },
    {
      "1": 8054,
      "roma": "SANT'ANASTASIA",
      "rm": "NA",
      "1234": 415063072
    },
    {
      "1": 8055,
      "roma": "SANT'AGATA DI ESARO",
      "rm": "CS",
      "1234": 418078131
    },
    {
      "1": 8056,
      "roma": "SANT'ANDREA DEL GARIGLIANO",
      "rm": "FR",
      "1234": 412060066
    },
    {
      "1": 8057,
      "roma": "SANT'ANDREA DI CONZA",
      "rm": "AV",
      "1234": 415064089
    },
    {
      "1": 8058,
      "roma": "SANT'ANDREA FRIUS",
      "rm": "CA",
      "1234": 420092061
    },
    {
      "1": 8059,
      "roma": "SANT'ANGELO A CUPOLO",
      "rm": "BN",
      "1234": 415062071
    },
    {
      "1": 8060,
      "roma": "SANT'ANGELO A FASANELLA",
      "rm": "SA",
      "1234": 415065128
    },
    {
      "1": 8061,
      "roma": "SANT'ANGELO A SCALA",
      "rm": "AV",
      "1234": 415064091
    },
    {
      "1": 8062,
      "roma": "SANT'ANGELO ALL'ESCA",
      "rm": "AV",
      "1234": 415064090
    },
    {
      "1": 8063,
      "roma": "SANT'ANGELO D'ALIFE",
      "rm": "CE",
      "1234": 415061086
    },
    {
      "1": 8064,
      "roma": "SANT'ARCANGELO",
      "rm": "PZ",
      "1234": 417076080
    },
    {
      "1": 8065,
      "roma": "SANT'ARCANGELO TRIMONTE",
      "rm": "BN",
      "1234": 415062078
    },
    {
      "1": 8066,
      "roma": "SANT'ARPINO",
      "rm": "CE",
      "1234": 415061087
    },
    {
      "1": 8067,
      "roma": "SANT'ANGELO DI PIOVE DI SACCO",
      "rm": "PD",
      "1234": 405028082
    },
    {
      "1": 8068,
      "roma": "SANT'ANGELO IN PONTANO",
      "rm": "MC",
      "1234": 411043048
    },
    {
      "1": 8069,
      "roma": "SANT'ANGELO IN VADO",
      "rm": "PS",
      "1234": 411041057
    },
    {
      "1": 8070,
      "roma": "SANT'ANGELO LE FRATTE",
      "rm": "PZ",
      "1234": 417076079
    },
    {
      "1": 8071,
      "roma": "SANT'ANGELO LIMOSANO",
      "rm": "CB",
      "1234": 414070073
    },
    {
      "1": 8072,
      "roma": "SANT'ANGELO LODIGIANO",
      "rm": "LO",
      "1234": 403098050
    },
    {
      "1": 8073,
      "roma": "SANT'ANATOLIA DI NARCO",
      "rm": "PG",
      "1234": 410054045
    },
    {
      "1": 8074,
      "roma": "SANT'ANDREA APOSTOLO DELLO IONIO",
      "rm": "CZ",
      "1234": 418079118
    },
    {
      "1": 8075,
      "roma": "SANT'ANGELO MUXARO",
      "rm": "AG",
      "1234": 419084039
    },
    {
      "1": 8076,
      "roma": "SANT'ANGELO ROMANO",
      "rm": "RM",
      "1234": 412058098
    },
    {
      "1": 8077,
      "roma": "SANT'ANNA ARRESI",
      "rm": "CA",
      "1234": 420092062
    },
    {
      "1": 8078,
      "roma": "SANT'ANNA D'ALFAEDO",
      "rm": "VR",
      "1234": 405023078
    },
    {
      "1": 8079,
      "roma": "SANT'ANTIMO",
      "rm": "NA",
      "1234": 415063073
    },
    {
      "1": 8080,
      "roma": "SANT'ANTIOCO",
      "rm": "CA",
      "1234": 420092063
    },
    {
      "1": 8081,
      "roma": "SANT'ANTONINO DI SUSA",
      "rm": "TO",
      "1234": 401001256
    },
    {
      "1": 8082,
      "roma": "SANT'ANTONIO ABATE",
      "rm": "NA",
      "1234": 415063074
    },
    {
      "1": 8083,
      "roma": "SANT'ANTONIO DI GALLURA",
      "rm": "SS",
      "1234": 420090085
    },
    {
      "1": 8084,
      "roma": "SANT'APOLLINARE",
      "rm": "FR",
      "1234": 412060067
    },
    {
      "1": 8085,
      "roma": "SANT'IPPOLITO",
      "rm": "PS",
      "1234": 411041058
    },
    {
      "1": 8086,
      "roma": "SANT'OLCESE",
      "rm": "GE",
      "1234": 407010055
    },
    {
      "1": 8087,
      "roma": "SANT'OMERO",
      "rm": "TE",
      "1234": 413067039
    },
    {
      "1": 8088,
      "roma": "SANT'ARSENIO",
      "rm": "SA",
      "1234": 415065129
    },
    {
      "1": 8089,
      "roma": "SANT'EGIDIO ALLA VIBRATA",
      "rm": "TE",
      "1234": 413067038
    },
    {
      "1": 8090,
      "roma": "SANT'EGIDIO DEL MONTE ALBINO",
      "rm": "SA",
      "1234": 415065130
    },
    {
      "1": 8091,
      "roma": "SANT'ELENA",
      "rm": "PD",
      "1234": 405028083
    },
    {
      "1": 8092,
      "roma": "SANT'ELENA SANNITA",
      "rm": "IS",
      "1234": 414094047
    },
    {
      "1": 8093,
      "roma": "SANT'ELIA A PIANISI",
      "rm": "CB",
      "1234": 414070074
    },
    {
      "1": 8094,
      "roma": "SANT'ANGELO LOMELLINA",
      "rm": "PV",
      "1234": 403018144
    },
    {
      "1": 8095,
      "roma": "SANT'EUFEMIA A MAIELLA",
      "rm": "PE",
      "1234": 413068037
    },
    {
      "1": 8096,
      "roma": "SANT'EUFEMIA D'ASPROMONTE",
      "rm": "RC",
      "1234": 418080081
    },
    {
      "1": 8097,
      "roma": "SANT'EUSANIO DEL SANGRO",
      "rm": "CH",
      "1234": 413069085
    },
    {
      "1": 8098,
      "roma": "SANT'EUSANIO FORCONESE",
      "rm": "AQ",
      "1234": 413066090
    },
    {
      "1": 8099,
      "roma": "SANT'ILARIO D'ENZA",
      "rm": "RE",
      "1234": 408035039
    },
    {
      "1": 8100,
      "roma": "SANT'ILARIO DELLO IONIO",
      "rm": "RC",
      "1234": 418080082
    },
    {
      "1": 8101,
      "roma": "SANTA ELISABETTA",
      "rm": "AG",
      "1234": 419084037
    },
    {
      "1": 8102,
      "roma": "SANTA FIORA",
      "rm": "GR",
      "1234": 409053022
    },
    {
      "1": 8103,
      "roma": "SANTA FLAVIA",
      "rm": "PA",
      "1234": 419082067
    },
    {
      "1": 8104,
      "roma": "SANTA GIULETTA",
      "rm": "PV",
      "1234": 403018140
    },
    {
      "1": 8105,
      "roma": "SANT'ONOFRIO",
      "rm": "VV",
      "1234": 418102036
    },
    {
      "1": 8106,
      "roma": "SANT'ORESTE",
      "rm": "RM",
      "1234": 412058099
    },
    {
      "1": 8107,
      "roma": "SANT'ORSOLA TERME",
      "rm": "TN",
      "1234": 404022168
    },
    {
      "1": 8108,
      "roma": "SANT'URBANO",
      "rm": "PD",
      "1234": 405028084
    },
    {
      "1": 8109,
      "roma": "SANTA BRIGIDA",
      "rm": "BG",
      "1234": 403016191
    },
    {
      "1": 8110,
      "roma": "SANT'ELPIDIO A MARE",
      "rm": "FM",
      "1234": 411109037
    },
    {
      "1": 8111,
      "roma": "SANT'ELIA FIUMERAPIDO",
      "rm": "FR",
      "1234": 412060068
    },
    {
      "1": 8112,
      "roma": "SANTA CESAREA TERME",
      "rm": "LE",
      "1234": 416075072
    },
    {
      "1": 8113,
      "roma": "SANTA CRISTINA D'ASPROMONTE",
      "rm": "RC",
      "1234": 418080078
    },
    {
      "1": 8114,
      "roma": "SANTA CRISTINA E BISSONE",
      "rm": "PV",
      "1234": 403018139
    },
    {
      "1": 8115,
      "roma": "SANTA CRISTINA GELA",
      "rm": "PA",
      "1234": 419082066
    },
    {
      "1": 8116,
      "roma": "SANTA CRISTINA VALGARDENA",
      "rm": "BZ",
      "1234": 404021085
    },
    {
      "1": 8117,
      "roma": "SANTA CROCE CAMERINA",
      "rm": "RG",
      "1234": 419088010
    },
    {
      "1": 8118,
      "roma": "SANTA CROCE DEL SANNIO",
      "rm": "BN",
      "1234": 415062069
    },
    {
      "1": 8119,
      "roma": "SANTA CROCE DI MAGLIANO",
      "rm": "CB",
      "1234": 414070072
    },
    {
      "1": 8120,
      "roma": "SANTA CROCE SULL'ARNO",
      "rm": "PI",
      "1234": 409050033
    },
    {
      "1": 8121,
      "roma": "SANTA DOMENICA TALAO",
      "rm": "CS",
      "1234": 418078130
    },
    {
      "1": 8122,
      "roma": "SANTA DOMENICA VITTORIA",
      "rm": "ME",
      "1234": 419083083
    },
    {
      "1": 8123,
      "roma": "SANTA MARIA COGHINAS",
      "rm": "SS",
      "1234": 420090087
    },
    {
      "1": 8124,
      "roma": "SANTA MARIA DEL CEDRO",
      "rm": "CS",
      "1234": 418078132
    },
    {
      "1": 8125,
      "roma": "SANTA MARIA DEL MOLISE",
      "rm": "IS",
      "1234": 414094045
    },
    {
      "1": 8126,
      "roma": "SANTA GIUSTA",
      "rm": "OR",
      "1234": 420095047
    },
    {
      "1": 8127,
      "roma": "SANTA GIUSTINA",
      "rm": "BL",
      "1234": 405025048
    },
    {
      "1": 8128,
      "roma": "SANTA GIUSTINA IN COLLE",
      "rm": "PD",
      "1234": 405028080
    },
    {
      "1": 8129,
      "roma": "SANTA LUCE",
      "rm": "PI",
      "1234": 409050034
    },
    {
      "1": 8130,
      "roma": "SANTA CATERINA ALBANESE",
      "rm": "CS",
      "1234": 418078129
    },
    {
      "1": 8131,
      "roma": "SANTA CATERINA DELLO IONIO",
      "rm": "CZ",
      "1234": 418079117
    },
    {
      "1": 8132,
      "roma": "SANTA CATERINA VILLARMOSA",
      "rm": "CL",
      "1234": 419085017
    },
    {
      "1": 8133,
      "roma": "SANTA LUCIA DI SERINO",
      "rm": "AV",
      "1234": 415064088
    },
    {
      "1": 8134,
      "roma": "SANTA MARGHERITA DI BELICE",
      "rm": "AG",
      "1234": 419084038
    },
    {
      "1": 8135,
      "roma": "SANTA MARGHERITA DI STAFFORA",
      "rm": "PV",
      "1234": 403018142
    },
    {
      "1": 8136,
      "roma": "SANTA MARGHERITA LIGURE",
      "rm": "GE",
      "1234": 407010054
    },
    {
      "1": 8137,
      "roma": "SANTA MARIA A MONTE",
      "rm": "PI",
      "1234": 409050035
    },
    {
      "1": 8138,
      "roma": "SANTA MARIA A VICO",
      "rm": "CE",
      "1234": 415061082
    },
    {
      "1": 8139,
      "roma": "SANTA MARIA CAPUA VETERE",
      "rm": "CE",
      "1234": 415061083
    },
    {
      "1": 8140,
      "roma": "SANTA MARINELLA",
      "rm": "RM",
      "1234": 412058097
    },
    {
      "1": 8141,
      "roma": "SANTA NINFA",
      "rm": "TP",
      "1234": 419081019
    },
    {
      "1": 8142,
      "roma": "SANTA PAOLINA",
      "rm": "AV",
      "1234": 415064093
    },
    {
      "1": 8143,
      "roma": "SANTA SEVERINA",
      "rm": "KR",
      "1234": 418101022
    },
    {
      "1": 8144,
      "roma": "SANTA MARIA DELLA VERSA",
      "rm": "PV",
      "1234": 403018143
    },
    {
      "1": 8145,
      "roma": "SANTA MARIA DI LICODIA",
      "rm": "CT",
      "1234": 419087047
    },
    {
      "1": 8146,
      "roma": "SANTA MARIA DI SALA",
      "rm": "VE",
      "1234": 405027035
    },
    {
      "1": 8147,
      "roma": "SANTA LUCIA DEL MELA",
      "rm": "ME",
      "1234": 419083086
    },
    {
      "1": 8148,
      "roma": "SANTA LUCIA DI PIAVE",
      "rm": "TV",
      "1234": 405026075
    },
    {
      "1": 8149,
      "roma": "SANTA MARIA LA CARITA'",
      "rm": "NA",
      "1234": 415063090
    },
    {
      "1": 8150,
      "roma": "SANTA MARIA LA FOSSA",
      "rm": "CE",
      "1234": 415061084
    },
    {
      "1": 8151,
      "roma": "SANTA MARIA LA LONGA",
      "rm": "UD",
      "1234": 406030104
    },
    {
      "1": 8152,
      "roma": "SANTA MARIA MAGGIORE",
      "rm": "VB",
      "1234": 401103062
    },
    {
      "1": 8153,
      "roma": "SANTA MARIA NUOVA",
      "rm": "AN",
      "1234": 411042043
    },
    {
      "1": 8154,
      "roma": "SANTA MARINA",
      "rm": "SA",
      "1234": 415065127
    },
    {
      "1": 8155,
      "roma": "SANTA MARINA SALINA",
      "rm": "ME",
      "1234": 419083087
    },
    {
      "1": 8156,
      "roma": "SANTO STEFANO AL MARE",
      "rm": "IM",
      "1234": 407008056
    },
    {
      "1": 8157,
      "roma": "SANTO STEFANO BELBO",
      "rm": "CN",
      "1234": 401004213
    },
    {
      "1": 8158,
      "roma": "SANTO STEFANO D'AVETO",
      "rm": "GE",
      "1234": 407010056
    },
    {
      "1": 8159,
      "roma": "SANTA SOFIA",
      "rm": "FO",
      "1234": 408040043
    },
    {
      "1": 8160,
      "roma": "SANTA SOFIA D'EPIRO",
      "rm": "CS",
      "1234": 418078133
    },
    {
      "1": 8161,
      "roma": "SANTA TERESA DI RIVA",
      "rm": "ME",
      "1234": 419083089
    },
    {
      "1": 8162,
      "roma": "SANTA TERESA GALLURA",
      "rm": "SS",
      "1234": 420090063
    },
    {
      "1": 8163,
      "roma": "SANTA VENERINA",
      "rm": "CT",
      "1234": 419087048
    },
    {
      "1": 8164,
      "roma": "SANTA MARIA HOE'",
      "rm": "LC",
      "1234": 403097074
    },
    {
      "1": 8165,
      "roma": "SANTA MARIA IMBARO",
      "rm": "CH",
      "1234": 413069084
    },
    {
      "1": 8166,
      "roma": "SANTA VITTORIA IN MATENANO",
      "rm": "FM",
      "1234": 411109036
    },
    {
      "1": 8167,
      "roma": "SANTARCANGELO DI ROMAGNA",
      "rm": "RN",
      "1234": 408099018
    },
    {
      "1": 8168,
      "roma": "SANTE MARIE",
      "rm": "AQ",
      "1234": 413066089
    },
    {
      "1": 8169,
      "roma": "SANTENA",
      "rm": "TO",
      "1234": 401001257
    },
    {
      "1": 8170,
      "roma": "SANTERAMO IN COLLE",
      "rm": "BA",
      "1234": 416072041
    },
    {
      "1": 8171,
      "roma": "SANTHIA'",
      "rm": "VC",
      "1234": 401002133
    },
    {
      "1": 8172,
      "roma": "SANTI COSMA E DAMIANO",
      "rm": "LT",
      "1234": 412059026
    },
    {
      "1": 8173,
      "roma": "SANZENO",
      "rm": "TN",
      "1234": 404022169
    },
    {
      "1": 8174,
      "roma": "SAONARA",
      "rm": "PD",
      "1234": 405028085
    },
    {
      "1": 8175,
      "roma": "SAPONARA",
      "rm": "ME",
      "1234": 419083092
    },
    {
      "1": 8176,
      "roma": "SANTO STEFANO DEL SOLE",
      "rm": "AV",
      "1234": 415064095
    },
    {
      "1": 8177,
      "roma": "SIURGUS DONIGALA",
      "rm": "CA",
      "1234": 420092081
    },
    {
      "1": 8178,
      "roma": "SOLAROLO RAINERIO",
      "rm": "CR",
      "1234": 403019096
    },
    {
      "1": 8179,
      "roma": "SOLARUSSA",
      "rm": "OR",
      "1234": 420095062
    },
    {
      "1": 8180,
      "roma": "SOLBIATE ARNO",
      "rm": "VA",
      "1234": 403012121
    },
    {
      "1": 8181,
      "roma": "SOLBIATE OLONA",
      "rm": "VA",
      "1234": 403012122
    },
    {
      "1": 8182,
      "roma": "SOLDANO",
      "rm": "IM",
      "1234": 407008058
    },
    {
      "1": 8183,
      "roma": "SOAVE",
      "rm": "VR",
      "1234": 405023081
    },
    {
      "1": 8184,
      "roma": "SIRMIONE",
      "rm": "BS",
      "1234": 403017179
    },
    {
      "1": 8185,
      "roma": "SIROLO",
      "rm": "AN",
      "1234": 411042048
    },
    {
      "1": 8186,
      "roma": "SIRONE",
      "rm": "LC",
      "1234": 403097075
    },
    {
      "1": 8187,
      "roma": "TRE VILLE",
      "rm": "TN",
      "1234": 404022247
    },
    {
      "1": 8188,
      "roma": "SIRTORI",
      "rm": "LC",
      "1234": 403097076
    },
    {
      "1": 8189,
      "roma": "SOLARINO",
      "rm": "SR",
      "1234": 419089018
    },
    {
      "1": 8190,
      "roma": "SOLARO",
      "rm": "MI",
      "1234": 403015213
    },
    {
      "1": 8191,
      "roma": "SOLAROLO",
      "rm": "RA",
      "1234": 408039018
    },
    {
      "1": 8192,
      "roma": "SOMAGLIA",
      "rm": "LO",
      "1234": 403098054
    },
    {
      "1": 8193,
      "roma": "SOMANO",
      "rm": "CN",
      "1234": 401004221
    },
    {
      "1": 8194,
      "roma": "SOMMA LOMBARDO",
      "rm": "VA",
      "1234": 403012123
    },
    {
      "1": 8195,
      "roma": "SOLEMINIS",
      "rm": "CA",
      "1234": 420092082
    },
    {
      "1": 8196,
      "roma": "SOLERO",
      "rm": "AL",
      "1234": 401006163
    },
    {
      "1": 8197,
      "roma": "SOLESINO",
      "rm": "PD",
      "1234": 405028087
    },
    {
      "1": 8198,
      "roma": "SOCCHIEVE",
      "rm": "UD",
      "1234": 406030110
    },
    {
      "1": 8199,
      "roma": "SODDI",
      "rm": "OR",
      "1234": 420095078
    },
    {
      "1": 8200,
      "roma": "SOGLIANO AL RUBICONE",
      "rm": "FO",
      "1234": 408040046
    },
    {
      "1": 8201,
      "roma": "SOGLIANO CAVOUR",
      "rm": "LE",
      "1234": 416075075
    },
    {
      "1": 8202,
      "roma": "SOGLIO",
      "rm": "AT",
      "1234": 401005107
    },
    {
      "1": 8203,
      "roma": "SOIANO DEL LAGO",
      "rm": "BS",
      "1234": 403017180
    },
    {
      "1": 8204,
      "roma": "SOLAGNA",
      "rm": "VI",
      "1234": 405024101
    },
    {
      "1": 8205,
      "roma": "SMERILLO",
      "rm": "FM",
      "1234": 411109039
    },
    {
      "1": 8206,
      "roma": "SOLTO COLLINA",
      "rm": "BG",
      "1234": 403016200
    },
    {
      "1": 8207,
      "roma": "SOLZA",
      "rm": "BG",
      "1234": 403016251
    },
    {
      "1": 8208,
      "roma": "SORA",
      "rm": "FR",
      "1234": 412060074
    },
    {
      "1": 8209,
      "roma": "SORAGNA",
      "rm": "PR",
      "1234": 408034036
    },
    {
      "1": 8210,
      "roma": "SORANO",
      "rm": "GR",
      "1234": 409053026
    },
    {
      "1": 8211,
      "roma": "SORBO SAN BASILE",
      "rm": "CZ",
      "1234": 418079134
    },
    {
      "1": 8212,
      "roma": "SOMMA VESUVIANA",
      "rm": "NA",
      "1234": 415063079
    },
    {
      "1": 8213,
      "roma": "SOMMACAMPAGNA",
      "rm": "VR",
      "1234": 405023082
    },
    {
      "1": 8214,
      "roma": "SOMMARIVA DEL BOSCO",
      "rm": "CN",
      "1234": 401004222
    },
    {
      "1": 8215,
      "roma": "SOLETO",
      "rm": "LE",
      "1234": 416075076
    },
    {
      "1": 8216,
      "roma": "SOLFERINO",
      "rm": "MN",
      "1234": 403020063
    },
    {
      "1": 8217,
      "roma": "SOLIERA",
      "rm": "MO",
      "1234": 408036044
    },
    {
      "1": 8218,
      "roma": "SOLIGNANO",
      "rm": "PR",
      "1234": 408034035
    },
    {
      "1": 6862,
      "roma": "PARRE",
      "rm": "BG",
      "1234": 403016158
    },
    {
      "1": 6863,
      "roma": "PARTANNA",
      "rm": "TP",
      "1234": 419081015
    },
    {
      "1": 6864,
      "roma": "PARTINICO",
      "rm": "PA",
      "1234": 419082054
    },
    {
      "1": 6865,
      "roma": "PARUZZARO",
      "rm": "NO",
      "1234": 401003114
    },
    {
      "1": 6866,
      "roma": "PARZANICA",
      "rm": "BG",
      "1234": 403016159
    },
    {
      "1": 6867,
      "roma": "PATRICA",
      "rm": "FR",
      "1234": 412060048
    },
    {
      "1": 6868,
      "roma": "PATTADA",
      "rm": "SS",
      "1234": 420090055
    },
    {
      "1": 6869,
      "roma": "PATTI",
      "rm": "ME",
      "1234": 419083066
    },
    {
      "1": 6870,
      "roma": "PATU'",
      "rm": "LE",
      "1234": 416075060
    },
    {
      "1": 6871,
      "roma": "PAU",
      "rm": "OR",
      "1234": 420095040
    },
    {
      "1": 6872,
      "roma": "PASSERANO MARMORITO",
      "rm": "AT",
      "1234": 401005082
    },
    {
      "1": 6873,
      "roma": "PASSIGNANO SUL TRASIMENO",
      "rm": "PG",
      "1234": 410054038
    },
    {
      "1": 6874,
      "roma": "PASSIRANO",
      "rm": "BS",
      "1234": 403017136
    },
    {
      "1": 6875,
      "roma": "PASTENA",
      "rm": "FR",
      "1234": 412060047
    },
    {
      "1": 6876,
      "roma": "PARODI LIGURE",
      "rm": "AL",
      "1234": 401006126
    },
    {
      "1": 6877,
      "roma": "PAROLDO",
      "rm": "CN",
      "1234": 401004160
    },
    {
      "1": 6878,
      "roma": "PAROLISE",
      "rm": "AV",
      "1234": 415064069
    },
    {
      "1": 6879,
      "roma": "PARONA",
      "rm": "PV",
      "1234": 403018109
    },
    {
      "1": 6880,
      "roma": "PASTURANA",
      "rm": "AL",
      "1234": 401006127
    },
    {
      "1": 6881,
      "roma": "PASTURO",
      "rm": "LC",
      "1234": 403097065
    },
    {
      "1": 6882,
      "roma": "PATERNO",
      "rm": "PZ",
      "1234": 417076100
    },
    {
      "1": 6883,
      "roma": "PATERNO CALABRO",
      "rm": "CS",
      "1234": 418078094
    },
    {
      "1": 6884,
      "roma": "PATERNO'",
      "rm": "CT",
      "1234": 419087033
    },
    {
      "1": 6885,
      "roma": "PATERNOPOLI",
      "rm": "AV",
      "1234": 415064070
    },
    {
      "1": 6886,
      "roma": "PECETTO TORINESE",
      "rm": "TO",
      "1234": 401001183
    },
    {
      "1": 6887,
      "roma": "PEDARA",
      "rm": "CT",
      "1234": 419087034
    },
    {
      "1": 6888,
      "roma": "PEDAVENA",
      "rm": "BL",
      "1234": 405025036
    },
    {
      "1": 6889,
      "roma": "PAULARO",
      "rm": "UD",
      "1234": 406030073
    },
    {
      "1": 6890,
      "roma": "PAULI ARBAREI",
      "rm": "CA",
      "1234": 420092046
    },
    {
      "1": 6891,
      "roma": "PAULILATINO",
      "rm": "OR",
      "1234": 420095041
    },
    {
      "1": 6892,
      "roma": "PAULLO",
      "rm": "MI",
      "1234": 403015169
    },
    {
      "1": 6893,
      "roma": "PASTORANO",
      "rm": "CE",
      "1234": 415061055
    },
    {
      "1": 6894,
      "roma": "PASTRENGO",
      "rm": "VR",
      "1234": 405023057
    },
    {
      "1": 6895,
      "roma": "PAVIA DI UDINE",
      "rm": "UD",
      "1234": 406030074
    },
    {
      "1": 6896,
      "roma": "PAVONE CANAVESE",
      "rm": "TO",
      "1234": 401001181
    },
    {
      "1": 6897,
      "roma": "PAVONE DEL MELLA",
      "rm": "BS",
      "1234": 403017137
    },
    {
      "1": 6898,
      "roma": "PAVULLO NEL FRIGNANO",
      "rm": "MO",
      "1234": 408036030
    },
    {
      "1": 6899,
      "roma": "PAZZANO",
      "rm": "RC",
      "1234": 418080058
    },
    {
      "1": 6900,
      "roma": "PECCIOLI",
      "rm": "PI",
      "1234": 409050025
    },
    {
      "1": 6901,
      "roma": "PECETTO DI VALENZA",
      "rm": "AL",
      "1234": 401006128
    },
    {
      "1": 6902,
      "roma": "PELLEGRINO PARMENSE",
      "rm": "PR",
      "1234": 408034028
    },
    {
      "1": 6903,
      "roma": "PELLEZZANO",
      "rm": "SA",
      "1234": 415065090
    },
    {
      "1": 6904,
      "roma": "PELLIZZANO",
      "rm": "TN",
      "1234": 404022137
    },
    {
      "1": 6905,
      "roma": "PELUGO",
      "rm": "TN",
      "1234": 404022138
    },
    {
      "1": 6906,
      "roma": "PENANGO",
      "rm": "AT",
      "1234": 401005083
    },
    {
      "1": 6907,
      "roma": "PENNA IN TEVERINA",
      "rm": "TR",
      "1234": 410055026
    },
    {
      "1": 6908,
      "roma": "PEDEMONTE",
      "rm": "VI",
      "1234": 405024076
    },
    {
      "1": 6909,
      "roma": "PEDEROBBA",
      "rm": "TV",
      "1234": 405026056
    },
    {
      "1": 6910,
      "roma": "PEDESINA",
      "rm": "SO",
      "1234": 403014047
    },
    {
      "1": 6911,
      "roma": "PEDIVIGLIANO",
      "rm": "CS",
      "1234": 418078096
    },
    {
      "1": 6912,
      "roma": "PAUPISI",
      "rm": "BN",
      "1234": 415062049
    },
    {
      "1": 6913,
      "roma": "PAVAROLO",
      "rm": "TO",
      "1234": 401001180
    },
    {
      "1": 6914,
      "roma": "PAVIA",
      "rm": "PV",
      "1234": 403018110
    },
    {
      "1": 6915,
      "roma": "PEGLIO",
      "rm": "CO",
      "1234": 403013178
    },
    {
      "1": 6916,
      "roma": "PEGOGNAGA",
      "rm": "MN",
      "1234": 403020039
    },
    {
      "1": 6917,
      "roma": "PEIA",
      "rm": "BG",
      "1234": 403016161
    },
    {
      "1": 6918,
      "roma": "PEIO",
      "rm": "TN",
      "1234": 404022136
    },
    {
      "1": 6919,
      "roma": "PELAGO",
      "rm": "FI",
      "1234": 409048032
    },
    {
      "1": 6920,
      "roma": "PELLA",
      "rm": "NO",
      "1234": 401003115
    },
    {
      "1": 6921,
      "roma": "PERETO",
      "rm": "AQ",
      "1234": 413066067
    },
    {
      "1": 6922,
      "roma": "PERFUGAS",
      "rm": "SS",
      "1234": 420090056
    },
    {
      "1": 6923,
      "roma": "PERGINE VALSUGANA",
      "rm": "TN",
      "1234": 404022139
    },
    {
      "1": 6924,
      "roma": "PERGOLA",
      "rm": "PS",
      "1234": 411041043
    },
    {
      "1": 6925,
      "roma": "PERINALDO",
      "rm": "IM",
      "1234": 407008040
    },
    {
      "1": 6926,
      "roma": "PERITO",
      "rm": "SA",
      "1234": 415065092
    },
    {
      "1": 6927,
      "roma": "PERLEDO",
      "rm": "LC",
      "1234": 403097067
    },
    {
      "1": 6928,
      "roma": "PENNA SAN GIOVANNI",
      "rm": "MC",
      "1234": 411043035
    },
    {
      "1": 6929,
      "roma": "PENNA SANT'ANDREA",
      "rm": "TE",
      "1234": 413067033
    },
    {
      "1": 6930,
      "roma": "PENNADOMO",
      "rm": "CH",
      "1234": 413069063
    },
    {
      "1": 6931,
      "roma": "PENNAPIEDIMONTE",
      "rm": "CH",
      "1234": 413069064
    },
    {
      "1": 6932,
      "roma": "PEDASO",
      "rm": "FM",
      "1234": 411109030
    },
    {
      "1": 6933,
      "roma": "PEDRENGO",
      "rm": "BG",
      "1234": 403016160
    },
    {
      "1": 6935,
      "roma": "PERAROLO DI CADORE",
      "rm": "BL",
      "1234": 405025037
    },
    {
      "1": 6936,
      "roma": "PERCA",
      "rm": "BZ",
      "1234": 404021063
    },
    {
      "1": 6937,
      "roma": "PERCILE",
      "rm": "RM",
      "1234": 412058076
    },
    {
      "1": 6938,
      "roma": "PERDASDEFOGU",
      "rm": "NU",
      "1234": 420091072
    },
    {
      "1": 6939,
      "roma": "PERDAXIUS",
      "rm": "CA",
      "1234": 420092047
    },
    {
      "1": 6940,
      "roma": "PERDIFUMO",
      "rm": "SA",
      "1234": 415065091
    },
    {
      "1": 6941,
      "roma": "PERTICA ALTA",
      "rm": "BS",
      "1234": 403017139
    },
    {
      "1": 6942,
      "roma": "PERTICA BASSA",
      "rm": "BS",
      "1234": 403017140
    },
    {
      "1": 6943,
      "roma": "PERTOSA",
      "rm": "SA",
      "1234": 415065093
    },
    {
      "1": 6944,
      "roma": "PERTUSIO",
      "rm": "TO",
      "1234": 401001187
    },
    {
      "1": 6945,
      "roma": "PERUGIA",
      "rm": "PG",
      "1234": 410054039
    },
    {
      "1": 6946,
      "roma": "PESARO",
      "rm": "PS",
      "1234": 411041044
    },
    {
      "1": 6947,
      "roma": "PERLETTO",
      "rm": "CN",
      "1234": 401004161
    },
    {
      "1": 6948,
      "roma": "PERLO",
      "rm": "CN",
      "1234": 401004162
    },
    {
      "1": 6949,
      "roma": "PERLOZ",
      "rm": "AO",
      "1234": 402007048
    },
    {
      "1": 6950,
      "roma": "PENNE",
      "rm": "PE",
      "1234": 413068027
    },
    {
      "1": 6951,
      "roma": "PENTONE",
      "rm": "CZ",
      "1234": 418079092
    },
    {
      "1": 6952,
      "roma": "PENNABILLI",
      "rm": "RN",
      "1234": 408099024
    },
    {
      "1": 6953,
      "roma": "PERANO",
      "rm": "CH",
      "1234": 413069065
    },
    {
      "1": 6954,
      "roma": "PERRERO",
      "rm": "TO",
      "1234": 401001186
    },
    {
      "1": 6955,
      "roma": "PERSICO DOSIMO",
      "rm": "CR",
      "1234": 403019068
    },
    {
      "1": 6956,
      "roma": "PERTENGO",
      "rm": "VC",
      "1234": 401002091
    },
    {
      "1": 6957,
      "roma": "PESCO SANNITA",
      "rm": "BN",
      "1234": 415062050
    },
    {
      "1": 6958,
      "roma": "PESCOCOSTANZO",
      "rm": "AQ",
      "1234": 413066070
    },
    {
      "1": 6959,
      "roma": "PESCOLANCIANO",
      "rm": "IS",
      "1234": 414094032
    },
    {
      "1": 6960,
      "roma": "PESCOPAGANO",
      "rm": "PZ",
      "1234": 417076058
    },
    {
      "1": 6961,
      "roma": "PESCOPENNATARO",
      "rm": "IS",
      "1234": 414094033
    },
    {
      "1": 6962,
      "roma": "PESCOROCCHIANO",
      "rm": "RI",
      "1234": 412057049
    },
    {
      "1": 6963,
      "roma": "PESCOSANSONESCO",
      "rm": "PE",
      "1234": 413068029
    },
    {
      "1": 6964,
      "roma": "PESCOSOLIDO",
      "rm": "FR",
      "1234": 412060049
    },
    {
      "1": 6965,
      "roma": "PESCAGLIA",
      "rm": "LU",
      "1234": 409046022
    },
    {
      "1": 6966,
      "roma": "PESCANTINA",
      "rm": "VR",
      "1234": 405023058
    },
    {
      "1": 6967,
      "roma": "PESCARA",
      "rm": "PE",
      "1234": 413068028
    },
    {
      "1": 6968,
      "roma": "PERNUMIA",
      "rm": "PD",
      "1234": 405028061
    },
    {
      "1": 6969,
      "roma": "PERO",
      "rm": "MI",
      "1234": 403015170
    },
    {
      "1": 6970,
      "roma": "PEROSA ARGENTINA",
      "rm": "TO",
      "1234": 401001184
    },
    {
      "1": 6971,
      "roma": "PEROSA CANAVESE",
      "rm": "TO",
      "1234": 401001185
    },
    {
      "1": 6972,
      "roma": "PESCHICI",
      "rm": "FG",
      "1234": 416071038
    },
    {
      "1": 6973,
      "roma": "PESCHIERA BORROMEO",
      "rm": "MI",
      "1234": 403015171
    },
    {
      "1": 6974,
      "roma": "PESCHIERA DEL GARDA",
      "rm": "VR",
      "1234": 405023059
    },
    {
      "1": 6975,
      "roma": "PESCIA",
      "rm": "PT",
      "1234": 409047012
    },
    {
      "1": 6976,
      "roma": "PESCINA",
      "rm": "AQ",
      "1234": 413066069
    },
    {
      "1": 6977,
      "roma": "PETRIZZI",
      "rm": "CZ",
      "1234": 418079094
    },
    {
      "1": 6978,
      "roma": "PETRONA'",
      "rm": "CZ",
      "1234": 418079095
    },
    {
      "1": 6979,
      "roma": "PETROSINO",
      "rm": "TP",
      "1234": 419081024
    },
    {
      "1": 6980,
      "roma": "PETRURO IRPINO",
      "rm": "AV",
      "1234": 415064071
    },
    {
      "1": 6981,
      "roma": "PETTENASCO",
      "rm": "NO",
      "1234": 401003116
    },
    {
      "1": 6982,
      "roma": "PETTINENGO",
      "rm": "BI",
      "1234": 401096042
    },
    {
      "1": 6983,
      "roma": "PETTINEO",
      "rm": "ME",
      "1234": 419083067
    },
    {
      "1": 6984,
      "roma": "PETTORANELLO DEL MOLISE",
      "rm": "IS",
      "1234": 414094034
    },
    {
      "1": 6985,
      "roma": "PESSANO CON BORNAGO",
      "rm": "MI",
      "1234": 403015172
    },
    {
      "1": 6986,
      "roma": "PESSINA CREMONESE",
      "rm": "CR",
      "1234": 403019070
    },
    {
      "1": 6987,
      "roma": "PESCAROLO ED UNITI",
      "rm": "CR",
      "1234": 403019069
    },
    {
      "1": 6988,
      "roma": "PESCASSEROLI",
      "rm": "AQ",
      "1234": 413066068
    },
    {
      "1": 6989,
      "roma": "PESCATE",
      "rm": "LC",
      "1234": 403097068
    },
    {
      "1": 6990,
      "roma": "PESCHE",
      "rm": "IS",
      "1234": 414094031
    },
    {
      "1": 6991,
      "roma": "PETRALIA SOPRANA",
      "rm": "PA",
      "1234": 419082055
    },
    {
      "1": 6992,
      "roma": "PETRALIA SOTTANA",
      "rm": "PA",
      "1234": 419082056
    },
    {
      "1": 6993,
      "roma": "PETRELLA SALTO",
      "rm": "RI",
      "1234": 412057050
    },
    {
      "1": 6994,
      "roma": "PETRELLA TIFERNINA",
      "rm": "CB",
      "1234": 414070052
    },
    {
      "1": 6995,
      "roma": "PETRIANO",
      "rm": "PS",
      "1234": 411041045
    },
    {
      "1": 6996,
      "roma": "PETRIOLO",
      "rm": "MC",
      "1234": 411043036
    },
    {
      "1": 6997,
      "roma": "PIAGGINE",
      "rm": "SA",
      "1234": 415065095
    },
    {
      "1": 6998,
      "roma": "PIAN CAMUNO",
      "rm": "BS",
      "1234": 403017142
    },
    {
      "1": 6999,
      "roma": "PIANA CRIXIA",
      "rm": "SV",
      "1234": 407009048
    },
    {
      "1": 7000,
      "roma": "PIANA DEGLI ALBANESI",
      "rm": "PA",
      "1234": 419082057
    },
    {
      "1": 7001,
      "roma": "PIANA DI MONTE VERNA",
      "rm": "CE",
      "1234": 415061056
    },
    {
      "1": 7002,
      "roma": "PETTORANO SUL GIZIO",
      "rm": "AQ",
      "1234": 413066071
    },
    {
      "1": 7003,
      "roma": "PETTORAZZA GRIMANI",
      "rm": "RO",
      "1234": 405029035
    },
    {
      "1": 7004,
      "roma": "PEVERAGNO",
      "rm": "CN",
      "1234": 401004163
    },
    {
      "1": 7005,
      "roma": "PESSINETTO",
      "rm": "TO",
      "1234": 401001188
    },
    {
      "1": 7006,
      "roma": "PETACCIATO",
      "rm": "CB",
      "1234": 414070051
    },
    {
      "1": 7007,
      "roma": "PETILIA POLICASTRO",
      "rm": "KR",
      "1234": 418101017
    },
    {
      "1": 7008,
      "roma": "PETINA",
      "rm": "SA",
      "1234": 415065094
    },
    {
      "1": 7009,
      "roma": "PIACENZA",
      "rm": "PC",
      "1234": 408033032
    },
    {
      "1": 7010,
      "roma": "PETRITOLI",
      "rm": "FM",
      "1234": 411109031
    },
    {
      "1": 7011,
      "roma": "PIACENZA D'ADIGE",
      "rm": "PD",
      "1234": 405028062
    },
    {
      "1": 7012,
      "roma": "PIANIGA",
      "rm": "VE",
      "1234": 405027028
    },
    {
      "1": 7013,
      "roma": "PIANO DI SORRENTO",
      "rm": "NA",
      "1234": 415063053
    },
    {
      "1": 7014,
      "roma": "PIANOPOLI",
      "rm": "CZ",
      "1234": 418079096
    },
    {
      "1": 7015,
      "roma": "PIANORO",
      "rm": "BO",
      "1234": 408037047
    },
    {
      "1": 7016,
      "roma": "PIANSANO",
      "rm": "VT",
      "1234": 412056043
    },
    {
      "1": 7017,
      "roma": "PIANTEDO",
      "rm": "SO",
      "1234": 403014048
    },
    {
      "1": 7018,
      "roma": "PIARIO",
      "rm": "BG",
      "1234": 403016163
    },
    {
      "1": 7019,
      "roma": "PIASCO",
      "rm": "CN",
      "1234": 401004166
    },
    {
      "1": 7020,
      "roma": "PIANCASTAGNAIO",
      "rm": "SI",
      "1234": 409052020
    },
    {
      "1": 7021,
      "roma": "PEZZANA",
      "rm": "VC",
      "1234": 401002093
    },
    {
      "1": 7022,
      "roma": "PEZZAZE",
      "rm": "BS",
      "1234": 403017141
    },
    {
      "1": 7023,
      "roma": "PEZZOLO VALLE UZZONE",
      "rm": "CN",
      "1234": 401004164
    },
    {
      "1": 7024,
      "roma": "PIANENGO",
      "rm": "CR",
      "1234": 403019072
    },
    {
      "1": 7025,
      "roma": "PIANEZZA",
      "rm": "TO",
      "1234": 401001189
    },
    {
      "1": 7026,
      "roma": "PIANEZZE",
      "rm": "VI",
      "1234": 405024077
    },
    {
      "1": 7027,
      "roma": "PIANFEI",
      "rm": "CN",
      "1234": 401004165
    },
    {
      "1": 7028,
      "roma": "PIANICO",
      "rm": "BG",
      "1234": 403016162
    },
    {
      "1": 7029,
      "roma": "PICERNO",
      "rm": "PZ",
      "1234": 417076059
    },
    {
      "1": 7030,
      "roma": "PICINISCO",
      "rm": "FR",
      "1234": 412060050
    },
    {
      "1": 7031,
      "roma": "PICO",
      "rm": "FR",
      "1234": 412060051
    },
    {
      "1": 7032,
      "roma": "PIEA",
      "rm": "AT",
      "1234": 401005084
    },
    {
      "1": 7033,
      "roma": "PIEDICAVALLO",
      "rm": "BI",
      "1234": 401096044
    },
    {
      "1": 7034,
      "roma": "PIEDIMONTE ETNEO",
      "rm": "CT",
      "1234": 419087035
    },
    {
      "1": 7035,
      "roma": "PIEDIMONTE MATESE",
      "rm": "CE",
      "1234": 415061057
    },
    {
      "1": 7036,
      "roma": "PIEDIMONTE SAN GERMANO",
      "rm": "FR",
      "1234": 412060052
    },
    {
      "1": 7037,
      "roma": "PIATEDA",
      "rm": "SO",
      "1234": 403014049
    },
    {
      "1": 7038,
      "roma": "PIATTO",
      "rm": "BI",
      "1234": 401096043
    },
    {
      "1": 7039,
      "roma": "PIANCOGNO",
      "rm": "BS",
      "1234": 403017206
    },
    {
      "1": 7040,
      "roma": "PIANDIMELETO",
      "rm": "PS",
      "1234": 411041047
    },
    {
      "1": 7041,
      "roma": "PIANE CRATI",
      "rm": "CS",
      "1234": 418078097
    },
    {
      "1": 7042,
      "roma": "PIANELLA",
      "rm": "PE",
      "1234": 413068030
    },
    {
      "1": 7043,
      "roma": "PIANELLO DEL LARIO",
      "rm": "CO",
      "1234": 403013183
    },
    {
      "1": 7044,
      "roma": "PIANELLO VAL TIDONE",
      "rm": "PC",
      "1234": 408033033
    },
    {
      "1": 7045,
      "roma": "PIAZZOLA SUL BRENTA",
      "rm": "PD",
      "1234": 405028063
    },
    {
      "1": 7046,
      "roma": "PIAZZOLO",
      "rm": "BG",
      "1234": 403016166
    },
    {
      "1": 7047,
      "roma": "PICCIANO",
      "rm": "PE",
      "1234": 413068031
    },
    {
      "1": 7048,
      "roma": "PIETRACATELLA",
      "rm": "CB",
      "1234": 414070053
    },
    {
      "1": 7049,
      "roma": "PIETRACUPA",
      "rm": "CB",
      "1234": 414070054
    },
    {
      "1": 7050,
      "roma": "PIETRADEFUSI",
      "rm": "AV",
      "1234": 415064072
    },
    {
      "1": 7051,
      "roma": "PIETRAFERRAZZANA",
      "rm": "CH",
      "1234": 413069103
    },
    {
      "1": 7052,
      "roma": "PIETRAFITTA",
      "rm": "CS",
      "1234": 418078098
    },
    {
      "1": 7053,
      "roma": "PIETRAGALLA",
      "rm": "PZ",
      "1234": 417076060
    },
    {
      "1": 7054,
      "roma": "PIETRALUNGA",
      "rm": "PG",
      "1234": 410054041
    },
    {
      "1": 7055,
      "roma": "PIETRAMELARA",
      "rm": "CE",
      "1234": 415061058
    },
    {
      "1": 7056,
      "roma": "PIETRAMONTECORVINO",
      "rm": "FG",
      "1234": 416071039
    },
    {
      "1": 7057,
      "roma": "PIETRANICO",
      "rm": "PE",
      "1234": 413068032
    },
    {
      "1": 7058,
      "roma": "PIETRAPAOLA",
      "rm": "CS",
      "1234": 418078099
    },
    {
      "1": 7059,
      "roma": "PIEDIMULERA",
      "rm": "VB",
      "1234": 401103053
    },
    {
      "1": 7060,
      "roma": "PIAZZA AL SERCHIO",
      "rm": "LU",
      "1234": 409046023
    },
    {
      "1": 7061,
      "roma": "PIAZZA ARMERINA",
      "rm": "EN",
      "1234": 419086014
    },
    {
      "1": 7062,
      "roma": "PIAZZA BREMBANA",
      "rm": "BG",
      "1234": 403016164
    },
    {
      "1": 7063,
      "roma": "PIAZZATORRE",
      "rm": "BG",
      "1234": 403016165
    },
    {
      "1": 7064,
      "roma": "PIETRA LIGURE",
      "rm": "SV",
      "1234": 407009049
    },
    {
      "1": 7065,
      "roma": "PIETRA MARAZZI",
      "rm": "AL",
      "1234": 401006129
    },
    {
      "1": 7066,
      "roma": "PIETRABBONDANTE",
      "rm": "IS",
      "1234": 414094035
    },
    {
      "1": 7067,
      "roma": "PIETRABRUNA",
      "rm": "IM",
      "1234": 407008041
    },
    {
      "1": 7068,
      "roma": "PIETRACAMELA",
      "rm": "TE",
      "1234": 413067034
    },
    {
      "1": 7069,
      "roma": "PIEVE DI CADORE",
      "rm": "BL",
      "1234": 405025039
    },
    {
      "1": 7070,
      "roma": "PIEVE DI BONO-PREZZO",
      "rm": "TN",
      "1234": 404022234
    },
    {
      "1": 7071,
      "roma": "PIEVE DI CENTO",
      "rm": "BO",
      "1234": 408037048
    },
    {
      "1": 7072,
      "roma": "PIEVE DI SOLIGO",
      "rm": "TV",
      "1234": 405026057
    },
    {
      "1": 7073,
      "roma": "PIEVE DI TECO",
      "rm": "IM",
      "1234": 407008042
    },
    {
      "1": 7074,
      "roma": "PIEVE EMANUELE",
      "rm": "MI",
      "1234": 403015173
    },
    {
      "1": 7075,
      "roma": "PIEVE FISSIRAGA",
      "rm": "LO",
      "1234": 403098045
    },
    {
      "1": 7076,
      "roma": "PIETRAPERTOSA",
      "rm": "PZ",
      "1234": 417076061
    },
    {
      "1": 7077,
      "roma": "PIETRAPERZIA",
      "rm": "EN",
      "1234": 419086015
    },
    {
      "1": 7078,
      "roma": "PIEGARO",
      "rm": "PG",
      "1234": 410054040
    },
    {
      "1": 7079,
      "roma": "PIENZA",
      "rm": "SI",
      "1234": 409052021
    },
    {
      "1": 7080,
      "roma": "PIERANICA",
      "rm": "CR",
      "1234": 403019073
    },
    {
      "1": 7081,
      "roma": "PIETRA DE' GIORGI",
      "rm": "PV",
      "1234": 403018111
    },
    {
      "1": 7082,
      "roma": "PIEVE A NIEVOLE",
      "rm": "PT",
      "1234": 409047013
    },
    {
      "1": 7083,
      "roma": "PIEVE ALBIGNOLA",
      "rm": "PV",
      "1234": 403018112
    },
    {
      "1": 7084,
      "roma": "PIEVE D'OLMI",
      "rm": "CR",
      "1234": 403019074
    },
    {
      "1": 7085,
      "roma": "PIEVE DEL CAIRO",
      "rm": "PV",
      "1234": 403018113
    },
    {
      "1": 7086,
      "roma": "PIGNA",
      "rm": "IM",
      "1234": 407008043
    },
    {
      "1": 7087,
      "roma": "PIGNATARO INTERAMNA",
      "rm": "FR",
      "1234": 412060054
    },
    {
      "1": 7088,
      "roma": "PIGNATARO MAGGIORE",
      "rm": "CE",
      "1234": 415061060
    },
    {
      "1": 7089,
      "roma": "PIGNOLA",
      "rm": "PZ",
      "1234": 417076062
    },
    {
      "1": 7090,
      "roma": "PIGNONE",
      "rm": "SP",
      "1234": 407011021
    },
    {
      "1": 7091,
      "roma": "PIGRA",
      "rm": "CO",
      "1234": 403013184
    },
    {
      "1": 7092,
      "roma": "PILA",
      "rm": "VC",
      "1234": 401002096
    },
    {
      "1": 7093,
      "roma": "PIMENTEL",
      "rm": "CA",
      "1234": 420092048
    },
    {
      "1": 7094,
      "roma": "PIMONTE",
      "rm": "NA",
      "1234": 415063054
    },
    {
      "1": 7095,
      "roma": "PINAROLO PO",
      "rm": "PV",
      "1234": 403018115
    },
    {
      "1": 7096,
      "roma": "PIEVE FOSCIANA",
      "rm": "LU",
      "1234": 409046025
    },
    {
      "1": 7097,
      "roma": "PIETRAPORZIO",
      "rm": "CN",
      "1234": 401004167
    },
    {
      "1": 7098,
      "roma": "PIETRAROIA",
      "rm": "BN",
      "1234": 415062051
    },
    {
      "1": 7099,
      "roma": "PIETRARUBBIA",
      "rm": "PS",
      "1234": 411041048
    },
    {
      "1": 7100,
      "roma": "PIETRASANTA",
      "rm": "LU",
      "1234": 409046024
    },
    {
      "1": 7101,
      "roma": "PIETRASTORNINA",
      "rm": "AV",
      "1234": 415064073
    },
    {
      "1": 7102,
      "roma": "PIETRAVAIRANO",
      "rm": "CE",
      "1234": 415061059
    },
    {
      "1": 7103,
      "roma": "PIETRELCINA",
      "rm": "BN",
      "1234": 415062052
    },
    {
      "1": 7104,
      "roma": "PIEVE VERGONTE",
      "rm": "VB",
      "1234": 401103054
    },
    {
      "1": 7105,
      "roma": "PIEVEPELAGO",
      "rm": "MO",
      "1234": 408036031
    },
    {
      "1": 7106,
      "roma": "PIGLIO",
      "rm": "FR",
      "1234": 412060053
    },
    {
      "1": 7107,
      "roma": "PIOBESI TORINESE",
      "rm": "TO",
      "1234": 401001193
    },
    {
      "1": 7108,
      "roma": "PIODE",
      "rm": "VC",
      "1234": 401002097
    },
    {
      "1": 7109,
      "roma": "PIOLTELLO",
      "rm": "MI",
      "1234": 403015175
    },
    {
      "1": 7110,
      "roma": "PIOMBINO",
      "rm": "LI",
      "1234": 409049012
    },
    {
      "1": 7111,
      "roma": "PIOMBINO DESE",
      "rm": "PD",
      "1234": 405028064
    },
    {
      "1": 7112,
      "roma": "PIORACO",
      "rm": "MC",
      "1234": 411043039
    },
    {
      "1": 7113,
      "roma": "PIOSSASCO",
      "rm": "TO",
      "1234": 401001194
    },
    {
      "1": 7114,
      "roma": "PIOVA' MASSAIA",
      "rm": "AT",
      "1234": 401005086
    },
    {
      "1": 7115,
      "roma": "PIOVE DI SACCO",
      "rm": "PD",
      "1234": 405028065
    },
    {
      "1": 7116,
      "roma": "PIOVENE ROCCHETTE",
      "rm": "VI",
      "1234": 405024078
    },
    {
      "1": 7117,
      "roma": "PINASCA",
      "rm": "TO",
      "1234": 401001190
    },
    {
      "1": 7118,
      "roma": "PIEVE LIGURE",
      "rm": "GE",
      "1234": 407010043
    },
    {
      "1": 7119,
      "roma": "PIEVE PORTO MORONE",
      "rm": "PV",
      "1234": 403018114
    },
    {
      "1": 7120,
      "roma": "PIEVE SAN GIACOMO",
      "rm": "CR",
      "1234": 403019075
    },
    {
      "1": 7121,
      "roma": "PIEVE SANTO STEFANO",
      "rm": "AR",
      "1234": 409051030
    },
    {
      "1": 7122,
      "roma": "PIEVE TESINO",
      "rm": "TN",
      "1234": 404022142
    },
    {
      "1": 7123,
      "roma": "PIEVE TORINA",
      "rm": "MC",
      "1234": 411043038
    },
    {
      "1": 7124,
      "roma": "PINZANO AL TAGLIAMENTO",
      "rm": "PN",
      "1234": 406093030
    },
    {
      "1": 7125,
      "roma": "PINZOLO",
      "rm": "TN",
      "1234": 404022143
    },
    {
      "1": 7126,
      "roma": "PIOBBICO",
      "rm": "PS",
      "1234": 411041049
    },
    {
      "1": 7127,
      "roma": "PIOBESI D'ALBA",
      "rm": "CN",
      "1234": 401004168
    },
    {
      "1": 7128,
      "roma": "PISOGNE",
      "rm": "BS",
      "1234": 403017143
    },
    {
      "1": 7129,
      "roma": "PISONIANO",
      "rm": "RM",
      "1234": 412058077
    },
    {
      "1": 7130,
      "roma": "PISTICCI",
      "rm": "MT",
      "1234": 417077020
    },
    {
      "1": 7131,
      "roma": "PISTOIA",
      "rm": "PT",
      "1234": 409047014
    },
    {
      "1": 7132,
      "roma": "PITIGLIANO",
      "rm": "GR",
      "1234": 409053019
    },
    {
      "1": 7133,
      "roma": "PIOZZANO",
      "rm": "PC",
      "1234": 408033034
    },
    {
      "1": 7134,
      "roma": "PINCARA",
      "rm": "RO",
      "1234": 405029036
    },
    {
      "1": 7135,
      "roma": "PINEROLO",
      "rm": "TO",
      "1234": 401001191
    },
    {
      "1": 7136,
      "roma": "PINETO",
      "rm": "TE",
      "1234": 413067035
    },
    {
      "1": 7137,
      "roma": "PINO D'ASTI",
      "rm": "AT",
      "1234": 401005085
    },
    {
      "1": 7138,
      "roma": "PINO TORINESE",
      "rm": "TO",
      "1234": 401001192
    },
    {
      "1": 7139,
      "roma": "PISCINAS",
      "rm": "CA",
      "1234": 420092107
    },
    {
      "1": 7140,
      "roma": "PISCIOTTA",
      "rm": "SA",
      "1234": 415065096
    },
    {
      "1": 7141,
      "roma": "PLACANICA",
      "rm": "RC",
      "1234": 418080059
    },
    {
      "1": 7142,
      "roma": "PLATACI",
      "rm": "CS",
      "1234": 418078100
    },
    {
      "1": 7143,
      "roma": "PLATANIA",
      "rm": "CZ",
      "1234": 418079099
    },
    {
      "1": 7144,
      "roma": "PLATI'",
      "rm": "RC",
      "1234": 418080060
    },
    {
      "1": 7145,
      "roma": "PLAUS",
      "rm": "BZ",
      "1234": 404021064
    },
    {
      "1": 7146,
      "roma": "PLESIO",
      "rm": "CO",
      "1234": 403013185
    },
    {
      "1": 7147,
      "roma": "PLOAGHE",
      "rm": "SS",
      "1234": 420090057
    },
    {
      "1": 7148,
      "roma": "PIUBEGA",
      "rm": "MN",
      "1234": 403020041
    },
    {
      "1": 7149,
      "roma": "PIOZZO",
      "rm": "CN",
      "1234": 401004169
    },
    {
      "1": 7150,
      "roma": "PIRAINO",
      "rm": "ME",
      "1234": 419083068
    },
    {
      "1": 7151,
      "roma": "PISA",
      "rm": "PI",
      "1234": 409050026
    },
    {
      "1": 7152,
      "roma": "PISANO",
      "rm": "NO",
      "1234": 401003119
    },
    {
      "1": 7153,
      "roma": "PISCINA",
      "rm": "TO",
      "1234": 401001195
    },
    {
      "1": 7154,
      "roma": "PIZZOLI",
      "rm": "AQ",
      "1234": 413066072
    },
    {
      "1": 7155,
      "roma": "PIZZONE",
      "rm": "IS",
      "1234": 414094036
    },
    {
      "1": 7156,
      "roma": "PIZZONI",
      "rm": "VV",
      "1234": 418102028
    },
    {
      "1": 7157,
      "roma": "POGGIO BUSTONE",
      "rm": "RI",
      "1234": 412057051
    },
    {
      "1": 7158,
      "roma": "POGGIO CATINO",
      "rm": "RI",
      "1234": 412057052
    },
    {
      "1": 7159,
      "roma": "POGGIO IMPERIALE",
      "rm": "FG",
      "1234": 416071040
    },
    {
      "1": 7160,
      "roma": "POGGIO MIRTETO",
      "rm": "RI",
      "1234": 412057053
    },
    {
      "1": 7161,
      "roma": "POGGIO MOIANO",
      "rm": "RI",
      "1234": 412057054
    },
    {
      "1": 7162,
      "roma": "POGGIO NATIVO",
      "rm": "RI",
      "1234": 412057055
    },
    {
      "1": 7163,
      "roma": "POGGIO PICENZE",
      "rm": "AQ",
      "1234": 413066073
    },
    {
      "1": 7164,
      "roma": "POGGIO RENATICO",
      "rm": "FE",
      "1234": 408038018
    },
    {
      "1": 7165,
      "roma": "POGGIO RUSCO",
      "rm": "MN",
      "1234": 403020042
    },
    {
      "1": 7166,
      "roma": "PIURO",
      "rm": "SO",
      "1234": 403014050
    },
    {
      "1": 7167,
      "roma": "PIVERONE",
      "rm": "TO",
      "1234": 401001196
    },
    {
      "1": 7168,
      "roma": "PIZZALE",
      "rm": "PV",
      "1234": 403018116
    },
    {
      "1": 7169,
      "roma": "PIZZIGHETTONE",
      "rm": "CR",
      "1234": 403019076
    },
    {
      "1": 7170,
      "roma": "PIZZO",
      "rm": "VV",
      "1234": 418102027
    },
    {
      "1": 7171,
      "roma": "PIZZOFERRATO",
      "rm": "CH",
      "1234": 413069066
    },
    {
      "1": 7172,
      "roma": "POGGIARDO",
      "rm": "LE",
      "1234": 416075061
    },
    {
      "1": 7173,
      "roma": "POGGIBONSI",
      "rm": "SI",
      "1234": 409052022
    },
    {
      "1": 7174,
      "roma": "POGGIO A CAIANO",
      "rm": "PO",
      "1234": 409100004
    },
    {
      "1": 7175,
      "roma": "POGNANO",
      "rm": "BG",
      "1234": 403016167
    },
    {
      "1": 7176,
      "roma": "POGNO",
      "rm": "NO",
      "1234": 401003120
    },
    {
      "1": 7177,
      "roma": "POIRINO",
      "rm": "TO",
      "1234": 401001197
    },
    {
      "1": 7178,
      "roma": "POJANA MAGGIORE",
      "rm": "VI",
      "1234": 405024079
    },
    {
      "1": 7179,
      "roma": "POLAVENO",
      "rm": "BS",
      "1234": 403017144
    },
    {
      "1": 7180,
      "roma": "POLCENIGO",
      "rm": "PN",
      "1234": 406093031
    },
    {
      "1": 7181,
      "roma": "POGGIO TORRIANA",
      "rm": "RN",
      "1234": 408099028
    },
    {
      "1": 7182,
      "roma": "POLESELLA",
      "rm": "RO",
      "1234": 405029037
    },
    {
      "1": 7183,
      "roma": "PLODIO",
      "rm": "SV",
      "1234": 407009050
    },
    {
      "1": 7184,
      "roma": "POCAPAGLIA",
      "rm": "CN",
      "1234": 401004170
    },
    {
      "1": 7185,
      "roma": "POCENIA",
      "rm": "UD",
      "1234": 406030075
    },
    {
      "1": 7186,
      "roma": "PODENZANA",
      "rm": "MS",
      "1234": 409045013
    },
    {
      "1": 7187,
      "roma": "PODENZANO",
      "rm": "PC",
      "1234": 408033035
    },
    {
      "1": 7188,
      "roma": "POFI",
      "rm": "FR",
      "1234": 412060055
    },
    {
      "1": 7189,
      "roma": "POGGIORSINI",
      "rm": "BA",
      "1234": 416072034
    },
    {
      "1": 7190,
      "roma": "POGGIRIDENTI",
      "rm": "SO",
      "1234": 403014051
    },
    {
      "1": 7191,
      "roma": "POGLIANO MILANESE",
      "rm": "MI",
      "1234": 403015176
    },
    {
      "1": 7192,
      "roma": "POGNANA LARIO",
      "rm": "CO",
      "1234": 403013186
    },
    {
      "1": 7193,
      "roma": "POLLENA TROCCHIA",
      "rm": "NA",
      "1234": 415063056
    },
    {
      "1": 7194,
      "roma": "POLLENZA",
      "rm": "MC",
      "1234": 411043041
    },
    {
      "1": 7195,
      "roma": "POLLICA",
      "rm": "SA",
      "1234": 415065098
    },
    {
      "1": 7196,
      "roma": "POLLINA",
      "rm": "PA",
      "1234": 419082059
    },
    {
      "1": 7197,
      "roma": "POLLONE",
      "rm": "BI",
      "1234": 401096046
    },
    {
      "1": 7198,
      "roma": "POLLUTRI",
      "rm": "CH",
      "1234": 413069068
    },
    {
      "1": 7199,
      "roma": "POLONGHERA",
      "rm": "CN",
      "1234": 401004171
    },
    {
      "1": 7200,
      "roma": "POLPENAZZE",
      "rm": "BS",
      "1234": 403017145
    },
    {
      "1": 7201,
      "roma": "POLVERARA",
      "rm": "PD",
      "1234": 405028066
    },
    {
      "1": 7202,
      "roma": "POLVERIGI",
      "rm": "AN",
      "1234": 411042038
    },
    {
      "1": 7203,
      "roma": "POMARANCE",
      "rm": "PI",
      "1234": 409050027
    },
    {
      "1": 7204,
      "roma": "POGGIO SAN LORENZO",
      "rm": "RI",
      "1234": 412057056
    },
    {
      "1": 7205,
      "roma": "POGGIO SAN MARCELLO",
      "rm": "AN",
      "1234": 411042037
    },
    {
      "1": 7206,
      "roma": "POGGIO SAN VICINO",
      "rm": "MC",
      "1234": 411043040
    },
    {
      "1": 7207,
      "roma": "POGGIO SANNITA",
      "rm": "IS",
      "1234": 414094037
    },
    {
      "1": 7208,
      "roma": "POGGIODOMO",
      "rm": "PG",
      "1234": 410054042
    },
    {
      "1": 7209,
      "roma": "POGGIOFIORITO",
      "rm": "CH",
      "1234": 413069067
    },
    {
      "1": 7210,
      "roma": "POGGIOMARINO",
      "rm": "NA",
      "1234": 415063055
    },
    {
      "1": 7211,
      "roma": "POGGIOREALE",
      "rm": "TP",
      "1234": 419081016
    },
    {
      "1": 7212,
      "roma": "POLISTENA",
      "rm": "RC",
      "1234": 418080061
    },
    {
      "1": 7213,
      "roma": "POLIZZI GENEROSA",
      "rm": "PA",
      "1234": 419082058
    },
    {
      "1": 7214,
      "roma": "POLLA",
      "rm": "SA",
      "1234": 415065097
    },
    {
      "1": 7215,
      "roma": "POLLEIN",
      "rm": "AO",
      "1234": 402007049
    },
    {
      "1": 7216,
      "roma": "POMPONESCO",
      "rm": "MN",
      "1234": 403020043
    },
    {
      "1": 7217,
      "roma": "POMPU",
      "rm": "OR",
      "1234": 420095042
    },
    {
      "1": 7218,
      "roma": "PONCARALE",
      "rm": "BS",
      "1234": 403017147
    },
    {
      "1": 7219,
      "roma": "PONDERANO",
      "rm": "BI",
      "1234": 401096047
    },
    {
      "1": 7220,
      "roma": "PONNA",
      "rm": "CO",
      "1234": 403013187
    },
    {
      "1": 7221,
      "roma": "PONSACCO",
      "rm": "PI",
      "1234": 409050028
    },
    {
      "1": 7222,
      "roma": "PONSO",
      "rm": "PD",
      "1234": 405028067
    },
    {
      "1": 7223,
      "roma": "PONT CANAVESE",
      "rm": "TO",
      "1234": 401001199
    },
    {
      "1": 7224,
      "roma": "PONT-SAINT-MARTIN",
      "rm": "AO",
      "1234": 402007052
    },
    {
      "1": 7225,
      "roma": "POLI",
      "rm": "RM",
      "1234": 412058078
    },
    {
      "1": 7226,
      "roma": "POLIA",
      "rm": "VV",
      "1234": 418102029
    },
    {
      "1": 7227,
      "roma": "POLICORO",
      "rm": "MT",
      "1234": 417077021
    },
    {
      "1": 7228,
      "roma": "POLIGNANO A MARE",
      "rm": "BA",
      "1234": 416072035
    },
    {
      "1": 7229,
      "roma": "POLINAGO",
      "rm": "MO",
      "1234": 408036032
    },
    {
      "1": 7230,
      "roma": "POLINO",
      "rm": "TR",
      "1234": 410055027
    },
    {
      "1": 7231,
      "roma": "POMPEI",
      "rm": "NA",
      "1234": 415063058
    },
    {
      "1": 7232,
      "roma": "POMPEIANA",
      "rm": "IM",
      "1234": 407008044
    },
    {
      "1": 7233,
      "roma": "POMPIANO",
      "rm": "BS",
      "1234": 403017146
    },
    {
      "1": 7234,
      "roma": "PONTE NELLE ALPI",
      "rm": "BL",
      "1234": 405025040
    },
    {
      "1": 7235,
      "roma": "PONTE NIZZA",
      "rm": "PV",
      "1234": 403018117
    },
    {
      "1": 7236,
      "roma": "PONTE NOSSA",
      "rm": "BG",
      "1234": 403016168
    },
    {
      "1": 7237,
      "roma": "PONTE SAN NICOLO'",
      "rm": "PD",
      "1234": 405028069
    },
    {
      "1": 7238,
      "roma": "PONTE SAN PIETRO",
      "rm": "BG",
      "1234": 403016170
    },
    {
      "1": 7239,
      "roma": "PONTEBBA",
      "rm": "UD",
      "1234": 406030076
    },
    {
      "1": 7240,
      "roma": "PONTECAGNANO FAIANO",
      "rm": "SA",
      "1234": 415065099
    },
    {
      "1": 7241,
      "roma": "PONTECCHIO POLESINE",
      "rm": "RO",
      "1234": 405029038
    },
    {
      "1": 7242,
      "roma": "PONTECHIANALE",
      "rm": "CN",
      "1234": 401004172
    },
    {
      "1": 7243,
      "roma": "PONTECORVO",
      "rm": "FR",
      "1234": 412060056
    },
    {
      "1": 7244,
      "roma": "PONTECURONE",
      "rm": "AL",
      "1234": 401006132
    },
    {
      "1": 7245,
      "roma": "PONTEDASSIO",
      "rm": "IM",
      "1234": 407008045
    },
    {
      "1": 7246,
      "roma": "PONTASSIEVE",
      "rm": "FI",
      "1234": 409048033
    },
    {
      "1": 7247,
      "roma": "POLESINE ZIBELLO",
      "rm": "PR",
      "1234": 408034050
    },
    {
      "1": 7248,
      "roma": "POMARETTO",
      "rm": "TO",
      "1234": 401001198
    },
    {
      "1": 7249,
      "roma": "POMARICO",
      "rm": "MT",
      "1234": 417077022
    },
    {
      "1": 7250,
      "roma": "POMARO MONFERRATO",
      "rm": "AL",
      "1234": 401006131
    },
    {
      "1": 7251,
      "roma": "POMAROLO",
      "rm": "TN",
      "1234": 404022144
    },
    {
      "1": 7252,
      "roma": "POMBIA",
      "rm": "NO",
      "1234": 401003121
    },
    {
      "1": 7253,
      "roma": "POMEZIA",
      "rm": "RM",
      "1234": 412058079
    },
    {
      "1": 7254,
      "roma": "POMIGLIANO D'ARCO",
      "rm": "NA",
      "1234": 415063057
    },
    {
      "1": 7255,
      "roma": "PONTE GARDENA",
      "rm": "BZ",
      "1234": 404021065
    },
    {
      "1": 7256,
      "roma": "PONTE IN VALTELLINA",
      "rm": "SO",
      "1234": 403014052
    },
    {
      "1": 7257,
      "roma": "PONTE LAMBRO",
      "rm": "CO",
      "1234": 403013188
    },
    {
      "1": 7258,
      "roma": "PONTIDA",
      "rm": "BG",
      "1234": 403016171
    },
    {
      "1": 7259,
      "roma": "PONTINIA",
      "rm": "LT",
      "1234": 412059017
    },
    {
      "1": 7260,
      "roma": "PONTINVREA",
      "rm": "SV",
      "1234": 407009051
    },
    {
      "1": 7261,
      "roma": "PONTIROLO NUOVO",
      "rm": "BG",
      "1234": 403016172
    },
    {
      "1": 7262,
      "roma": "PONTOGLIO",
      "rm": "BS",
      "1234": 403017150
    },
    {
      "1": 7263,
      "roma": "PONTREMOLI",
      "rm": "MS",
      "1234": 409045014
    },
    {
      "1": 7264,
      "roma": "PONZA",
      "rm": "LT",
      "1234": 412059018
    },
    {
      "1": 7265,
      "roma": "PONZANO MONFERRATO",
      "rm": "AL",
      "1234": 401006135
    },
    {
      "1": 7266,
      "roma": "PONZANO ROMANO",
      "rm": "RM",
      "1234": 412058080
    },
    {
      "1": 7267,
      "roma": "PONZANO VENETO",
      "rm": "TV",
      "1234": 405026059
    },
    {
      "1": 7268,
      "roma": "PONTBOSET",
      "rm": "AO",
      "1234": 402007050
    },
    {
      "1": 7269,
      "roma": "PONTE",
      "rm": "BN",
      "1234": 415062053
    },
    {
      "1": 7270,
      "roma": "PONTE BUGGIANESE",
      "rm": "PT",
      "1234": 409047016
    },
    {
      "1": 7271,
      "roma": "PONTE DELL'OLIO",
      "rm": "PC",
      "1234": 408033036
    },
    {
      "1": 7272,
      "roma": "PONTE DI LEGNO",
      "rm": "BS",
      "1234": 403017148
    },
    {
      "1": 7273,
      "roma": "PONTE DI PIAVE",
      "rm": "TV",
      "1234": 405026058
    },
    {
      "1": 7274,
      "roma": "PONTEY",
      "rm": "AO",
      "1234": 402007051
    },
    {
      "1": 7275,
      "roma": "PONTI",
      "rm": "AL",
      "1234": 401006134
    },
    {
      "1": 7276,
      "roma": "PONTI SUL MINCIO",
      "rm": "MN",
      "1234": 403020044
    },
    {
      "1": 7277,
      "roma": "PORDENONE",
      "rm": "PN",
      "1234": 406093033
    },
    {
      "1": 7278,
      "roma": "PORLEZZA",
      "rm": "CO",
      "1234": 403013189
    },
    {
      "1": 7279,
      "roma": "PORNASSIO",
      "rm": "IM",
      "1234": 407008046
    },
    {
      "1": 7280,
      "roma": "PORPETTO",
      "rm": "UD",
      "1234": 406030077
    },
    {
      "1": 7281,
      "roma": "PONTEDERA",
      "rm": "PI",
      "1234": 409050029
    },
    {
      "1": 7282,
      "roma": "PONTELANDOLFO",
      "rm": "BN",
      "1234": 415062054
    },
    {
      "1": 7283,
      "roma": "PONTELATONE",
      "rm": "CE",
      "1234": 415061061
    },
    {
      "1": 7284,
      "roma": "PONTELONGO",
      "rm": "PD",
      "1234": 405028068
    },
    {
      "1": 7285,
      "roma": "PONTENURE",
      "rm": "PC",
      "1234": 408033037
    },
    {
      "1": 7286,
      "roma": "PONTERANICA",
      "rm": "BG",
      "1234": 403016169
    },
    {
      "1": 7287,
      "roma": "RIESE PIO X",
      "rm": "TV",
      "1234": 405026068
    },
    {
      "1": 7288,
      "roma": "RIESI",
      "rm": "CL",
      "1234": 419085015
    },
    {
      "1": 7289,
      "roma": "RIETI",
      "rm": "RI",
      "1234": 412057059
    },
    {
      "1": 7290,
      "roma": "RIFIANO",
      "rm": "BZ",
      "1234": 404021073
    },
    {
      "1": 7291,
      "roma": "RIFREDDO",
      "rm": "CN",
      "1234": 401004181
    },
    {
      "1": 7292,
      "roma": "RHEMES-SAINT-GEORGES",
      "rm": "AO",
      "1234": 402007056
    },
    {
      "1": 7293,
      "roma": "RHO",
      "rm": "MI",
      "1234": 403015182
    },
    {
      "1": 7294,
      "roma": "RIACE",
      "rm": "RC",
      "1234": 418080064
    },
    {
      "1": 7295,
      "roma": "RIALTO",
      "rm": "SV",
      "1234": 407009053
    },
    {
      "1": 7296,
      "roma": "RIANO",
      "rm": "RM",
      "1234": 412058081
    },
    {
      "1": 7297,
      "roma": "RIARDO",
      "rm": "CE",
      "1234": 415061068
    },
    {
      "1": 7298,
      "roma": "RIBERA",
      "rm": "AG",
      "1234": 419084033
    },
    {
      "1": 7299,
      "roma": "RIBORDONE",
      "rm": "TO",
      "1234": 401001212
    },
    {
      "1": 7300,
      "roma": "RICADI",
      "rm": "VV",
      "1234": 418102030
    },
    {
      "1": 7301,
      "roma": "RESUTTANO",
      "rm": "CL",
      "1234": 419085014
    },
    {
      "1": 7302,
      "roma": "RENATE",
      "rm": "MB",
      "1234": 403108037
    },
    {
      "1": 7303,
      "roma": "RETORBIDO",
      "rm": "PV",
      "1234": 403018121
    },
    {
      "1": 7304,
      "roma": "REVELLO",
      "rm": "CN",
      "1234": 401004180
    },
    {
      "1": 7305,
      "roma": "RICCO' DEL GOLFO DI SPEZIA",
      "rm": "SP",
      "1234": 407011023
    },
    {
      "1": 7306,
      "roma": "RICENGO",
      "rm": "CR",
      "1234": 403019079
    },
    {
      "1": 7307,
      "roma": "RICIGLIANO",
      "rm": "SA",
      "1234": 415065105
    },
    {
      "1": 7308,
      "roma": "RIOLA SARDO",
      "rm": "OR",
      "1234": 420095043
    },
    {
      "1": 7309,
      "roma": "RIOLO TERME",
      "rm": "RA",
      "1234": 408039015
    },
    {
      "1": 7310,
      "roma": "RIOLUNATO",
      "rm": "MO",
      "1234": 408036035
    },
    {
      "1": 7311,
      "roma": "RIOMAGGIORE",
      "rm": "SP",
      "1234": 407011024
    },
    {
      "1": 7312,
      "roma": "RIONERO IN VULTURE",
      "rm": "PZ",
      "1234": 417076066
    },
    {
      "1": 7313,
      "roma": "RIGNANO FLAMINIO",
      "rm": "RM",
      "1234": 412058082
    },
    {
      "1": 7314,
      "roma": "RIGNANO GARGANICO",
      "rm": "FG",
      "1234": 416071041
    },
    {
      "1": 7315,
      "roma": "RIGNANO SULL'ARNO",
      "rm": "FI",
      "1234": 409048036
    },
    {
      "1": 7316,
      "roma": "RIGOLATO",
      "rm": "UD",
      "1234": 406030094
    },
    {
      "1": 7317,
      "roma": "RIMELLA",
      "rm": "VC",
      "1234": 401002113
    },
    {
      "1": 7318,
      "roma": "RIMINI",
      "rm": "RN",
      "1234": 408099014
    },
    {
      "1": 7319,
      "roma": "RICALDONE",
      "rm": "AL",
      "1234": 401006143
    },
    {
      "1": 7320,
      "roma": "RICCIA",
      "rm": "CB",
      "1234": 414070057
    },
    {
      "1": 7321,
      "roma": "RICCIONE",
      "rm": "RN",
      "1234": 408099013
    },
    {
      "1": 7322,
      "roma": "RIO SALICETO",
      "rm": "RE",
      "1234": 408035034
    },
    {
      "1": 7323,
      "roma": "RIOFREDDO",
      "rm": "RM",
      "1234": 412058083
    },
    {
      "1": 7324,
      "roma": "RITTANA",
      "rm": "CN",
      "1234": 401004182
    },
    {
      "1": 7325,
      "roma": "RIVA DEL GARDA",
      "rm": "TN",
      "1234": 404022153
    },
    {
      "1": 7326,
      "roma": "RIVA DI SOLTO",
      "rm": "BG",
      "1234": 403016180
    },
    {
      "1": 7327,
      "roma": "RIVA LIGURE",
      "rm": "IM",
      "1234": 407008050
    },
    {
      "1": 7328,
      "roma": "RIVA PRESSO CHIERI",
      "rm": "TO",
      "1234": 401001215
    },
    {
      "1": 7329,
      "roma": "RIONERO SANNITICO",
      "rm": "IS",
      "1234": 414094039
    },
    {
      "1": 7330,
      "roma": "RIPA TEATINA",
      "rm": "CH",
      "1234": 413069072
    },
    {
      "1": 7331,
      "roma": "RIPABOTTONI",
      "rm": "CB",
      "1234": 414070058
    },
    {
      "1": 7332,
      "roma": "RIPACANDIDA",
      "rm": "PZ",
      "1234": 417076067
    },
    {
      "1": 7333,
      "roma": "RIPALIMOSANI",
      "rm": "CB",
      "1234": 414070059
    },
    {
      "1": 7334,
      "roma": "RIPALTA ARPINA",
      "rm": "CR",
      "1234": 403019080
    },
    {
      "1": 7335,
      "roma": "RIPALTA CREMASCA",
      "rm": "CR",
      "1234": 403019081
    },
    {
      "1": 7336,
      "roma": "RIO DI PUSTERIA",
      "rm": "BZ",
      "1234": 404021074
    },
    {
      "1": 7337,
      "roma": "RIPE SAN GINESIO",
      "rm": "MC",
      "1234": 411043045
    },
    {
      "1": 7338,
      "roma": "RIPI",
      "rm": "FR",
      "1234": 412060058
    },
    {
      "1": 7339,
      "roma": "RIPOSTO",
      "rm": "CT",
      "1234": 419087039
    },
    {
      "1": 7340,
      "roma": "RIVISONDOLI",
      "rm": "AQ",
      "1234": 413066078
    },
    {
      "1": 7341,
      "roma": "RIVODUTRI",
      "rm": "RI",
      "1234": 412057060
    },
    {
      "1": 7342,
      "roma": "RIVOLI",
      "rm": "TO",
      "1234": 401001219
    },
    {
      "1": 7343,
      "roma": "RIVOLI VERONESE",
      "rm": "VR",
      "1234": 405023062
    },
    {
      "1": 7344,
      "roma": "RIVOLTA D'ADDA",
      "rm": "CR",
      "1234": 403019084
    },
    {
      "1": 7345,
      "roma": "RIVALBA",
      "rm": "TO",
      "1234": 401001213
    },
    {
      "1": 7346,
      "roma": "RIVALTA BORMIDA",
      "rm": "AL",
      "1234": 401006144
    },
    {
      "1": 7347,
      "roma": "RIVALTA DI TORINO",
      "rm": "TO",
      "1234": 401001214
    },
    {
      "1": 7348,
      "roma": "RIVAMONTE AGORDINO",
      "rm": "BL",
      "1234": 405025043
    },
    {
      "1": 7349,
      "roma": "RIVARA",
      "rm": "TO",
      "1234": 401001216
    },
    {
      "1": 7350,
      "roma": "RIVAROLO CANAVESE",
      "rm": "TO",
      "1234": 401001217
    },
    {
      "1": 7351,
      "roma": "RIVAROLO DEL RE ED UNITI",
      "rm": "CR",
      "1234": 403019083
    },
    {
      "1": 7352,
      "roma": "RIPALTA GUERINA",
      "rm": "CR",
      "1234": 403019082
    },
    {
      "1": 7353,
      "roma": "RIPARBELLA",
      "rm": "PI",
      "1234": 409050030
    },
    {
      "1": 7354,
      "roma": "RIPATRANSONE",
      "rm": "AP",
      "1234": 411044063
    },
    {
      "1": 7355,
      "roma": "RIVELLO",
      "rm": "PZ",
      "1234": 417076068
    },
    {
      "1": 7356,
      "roma": "RIVANAZZANO TERME",
      "rm": "PV",
      "1234": 403018999
    },
    {
      "1": 7357,
      "roma": "RIVERGARO",
      "rm": "PC",
      "1234": 408033038
    },
    {
      "1": 7358,
      "roma": "ROBECCO PAVESE",
      "rm": "PV",
      "1234": 403018124
    },
    {
      "1": 7359,
      "roma": "ROBECCO SUL NAVIGLIO",
      "rm": "MI",
      "1234": 403015184
    },
    {
      "1": 7360,
      "roma": "ROBELLA",
      "rm": "AT",
      "1234": 401005092
    },
    {
      "1": 7361,
      "roma": "ROBILANTE",
      "rm": "CN",
      "1234": 401004185
    },
    {
      "1": 7362,
      "roma": "ROBURENT",
      "rm": "CN",
      "1234": 401004186
    },
    {
      "1": 7363,
      "roma": "ROCCA CANAVESE",
      "rm": "TO",
      "1234": 401001221
    },
    {
      "1": 7364,
      "roma": "ROCCA CANTERANO",
      "rm": "RM",
      "1234": 412058084
    },
    {
      "1": 7365,
      "roma": "ROCCA CIGLIE'",
      "rm": "CN",
      "1234": 401004188
    },
    {
      "1": 7366,
      "roma": "RIZZICONI",
      "rm": "RC",
      "1234": 418080065
    },
    {
      "1": 7367,
      "roma": "ROANA",
      "rm": "VI",
      "1234": 405024085
    },
    {
      "1": 7368,
      "roma": "ROASCHIA",
      "rm": "CN",
      "1234": 401004183
    },
    {
      "1": 7369,
      "roma": "ROASCIO",
      "rm": "CN",
      "1234": 401004184
    },
    {
      "1": 7370,
      "roma": "ROASIO",
      "rm": "VC",
      "1234": 401002116
    },
    {
      "1": 7371,
      "roma": "ROATTO",
      "rm": "AT",
      "1234": 401005091
    },
    {
      "1": 7372,
      "roma": "ROBASSOMERO",
      "rm": "TO",
      "1234": 401001220
    },
    {
      "1": 7373,
      "roma": "RIVAROLO MANTOVANO",
      "rm": "MN",
      "1234": 403020050
    },
    {
      "1": 7374,
      "roma": "RIVARONE",
      "rm": "AL",
      "1234": 401006145
    },
    {
      "1": 7375,
      "roma": "RIVAROSSA",
      "rm": "TO",
      "1234": 401001218
    },
    {
      "1": 7376,
      "roma": "RIVE",
      "rm": "VC",
      "1234": 401002115
    },
    {
      "1": 7377,
      "roma": "RIVE D'ARCANO",
      "rm": "UD",
      "1234": 406030095
    },
    {
      "1": 7378,
      "roma": "ROBECCHETTO CON INDUNO",
      "rm": "MI",
      "1234": 403015183
    },
    {
      "1": 7379,
      "roma": "ROBECCO D'OGLIO",
      "rm": "CR",
      "1234": 403019085
    },
    {
      "1": 7380,
      "roma": "RIVIGNANO TEOR",
      "rm": "UD",
      "1234": 406030188
    },
    {
      "1": 7381,
      "roma": "ROCCA PRIORA",
      "rm": "RM",
      "1234": 412058088
    },
    {
      "1": 7382,
      "roma": "ROCCA SAN CASCIANO",
      "rm": "FO",
      "1234": 408040036
    },
    {
      "1": 7383,
      "roma": "ROCCA SAN FELICE",
      "rm": "AV",
      "1234": 415064079
    },
    {
      "1": 7384,
      "roma": "ROCCA SAN GIOVANNI",
      "rm": "CH",
      "1234": 413069074
    },
    {
      "1": 7385,
      "roma": "ROCCA SANTA MARIA",
      "rm": "TE",
      "1234": 413067036
    },
    {
      "1": 7386,
      "roma": "ROCCA SANTO STEFANO",
      "rm": "RM",
      "1234": 412058089
    },
    {
      "1": 7387,
      "roma": "ROCCA SINIBALDA",
      "rm": "RI",
      "1234": 412057062
    },
    {
      "1": 7388,
      "roma": "ROCCA SUSELLA",
      "rm": "PV",
      "1234": 403018126
    },
    {
      "1": 7389,
      "roma": "ROCCA D'ARAZZO",
      "rm": "AT",
      "1234": 401005093
    },
    {
      "1": 7390,
      "roma": "ROCCA D'EVANDRO",
      "rm": "CE",
      "1234": 415061069
    },
    {
      "1": 7391,
      "roma": "ROCCA DE' BALDI",
      "rm": "CN",
      "1234": 401004189
    },
    {
      "1": 7392,
      "roma": "ROCCA DE' GIORGI",
      "rm": "PV",
      "1234": 403018125
    },
    {
      "1": 7393,
      "roma": "ROCCA DI BOTTE",
      "rm": "AQ",
      "1234": 413066080
    },
    {
      "1": 7394,
      "roma": "ROCCA DI CAMBIO",
      "rm": "AQ",
      "1234": 413066081
    },
    {
      "1": 7395,
      "roma": "ROCCA DI CAVE",
      "rm": "RM",
      "1234": 412058085
    },
    {
      "1": 7396,
      "roma": "ROCCA DI MEZZO",
      "rm": "AQ",
      "1234": 413066082
    },
    {
      "1": 7397,
      "roma": "ROBBIATE",
      "rm": "LC",
      "1234": 403097071
    },
    {
      "1": 7398,
      "roma": "ROBBIO",
      "rm": "PV",
      "1234": 403018123
    },
    {
      "1": 7399,
      "roma": "ROCCA MASSIMA",
      "rm": "LT",
      "1234": 412059022
    },
    {
      "1": 7400,
      "roma": "ROCCA PIA",
      "rm": "AQ",
      "1234": 413066083
    },
    {
      "1": 7401,
      "roma": "ROCCA PIETORE",
      "rm": "BL",
      "1234": 405025044
    },
    {
      "1": 7402,
      "roma": "ROCCAGORGA",
      "rm": "LT",
      "1234": 412059021
    },
    {
      "1": 7403,
      "roma": "ROCCALBEGNA",
      "rm": "GR",
      "1234": 409053020
    },
    {
      "1": 7404,
      "roma": "ROCCALUMERA",
      "rm": "ME",
      "1234": 419083072
    },
    {
      "1": 7405,
      "roma": "ROCCAMANDOLFI",
      "rm": "IS",
      "1234": 414094040
    },
    {
      "1": 7406,
      "roma": "ROCCAMENA",
      "rm": "PA",
      "1234": 419082061
    },
    {
      "1": 7407,
      "roma": "ROCCABASCERANA",
      "rm": "AV",
      "1234": 415064078
    },
    {
      "1": 7408,
      "roma": "ROCCABERNARDA",
      "rm": "KR",
      "1234": 418101018
    },
    {
      "1": 7409,
      "roma": "ROCCABIANCA",
      "rm": "PR",
      "1234": 408034030
    },
    {
      "1": 7410,
      "roma": "ROCCABRUNA",
      "rm": "CN",
      "1234": 401004187
    },
    {
      "1": 7411,
      "roma": "ROCCACASALE",
      "rm": "AQ",
      "1234": 413066079
    },
    {
      "1": 7412,
      "roma": "ROCCA D'ARCE",
      "rm": "FR",
      "1234": 412060059
    },
    {
      "1": 7413,
      "roma": "ROCCADASPIDE",
      "rm": "SA",
      "1234": 415065106
    },
    {
      "1": 7414,
      "roma": "ROCCA DI NETO",
      "rm": "KR",
      "1234": 418101019
    },
    {
      "1": 7415,
      "roma": "ROCCA DI PAPA",
      "rm": "RM",
      "1234": 412058086
    },
    {
      "1": 7416,
      "roma": "ROCCA GRIMALDA",
      "rm": "AL",
      "1234": 401006147
    },
    {
      "1": 7417,
      "roma": "ROCCA IMPERIALE",
      "rm": "CS",
      "1234": 418078103
    },
    {
      "1": 7418,
      "roma": "ROCCAFORZATA",
      "rm": "TA",
      "1234": 416073023
    },
    {
      "1": 7419,
      "roma": "ROCCAFRANCA",
      "rm": "BS",
      "1234": 403017162
    },
    {
      "1": 7420,
      "roma": "ROCCAGIOVINE",
      "rm": "RM",
      "1234": 412058087
    },
    {
      "1": 7421,
      "roma": "ROCCAGLORIOSA",
      "rm": "SA",
      "1234": 415065107
    },
    {
      "1": 7422,
      "roma": "ROCCASPINALVETI",
      "rm": "CH",
      "1234": 413069076
    },
    {
      "1": 7423,
      "roma": "ROCCASTRADA",
      "rm": "GR",
      "1234": 409053021
    },
    {
      "1": 7424,
      "roma": "ROCCAVALDINA",
      "rm": "ME",
      "1234": 419083073
    },
    {
      "1": 7425,
      "roma": "ROCCAVERANO",
      "rm": "AT",
      "1234": 401005094
    },
    {
      "1": 7426,
      "roma": "ROCCAVIGNALE",
      "rm": "SV",
      "1234": 407009054
    },
    {
      "1": 7427,
      "roma": "ROCCAVIONE",
      "rm": "CN",
      "1234": 401004192
    },
    {
      "1": 7428,
      "roma": "ROCCAVIVARA",
      "rm": "CB",
      "1234": 414070060
    },
    {
      "1": 7429,
      "roma": "ROCCELLA IONICA",
      "rm": "RC",
      "1234": 418080067
    },
    {
      "1": 7430,
      "roma": "ROCCAMONFINA",
      "rm": "CE",
      "1234": 415061070
    },
    {
      "1": 7431,
      "roma": "ROCCAMONTEPIANO",
      "rm": "CH",
      "1234": 413069073
    },
    {
      "1": 7432,
      "roma": "ROCCAMORICE",
      "rm": "PE",
      "1234": 413068034
    },
    {
      "1": 7433,
      "roma": "ROCCANOVA",
      "rm": "PZ",
      "1234": 417076069
    },
    {
      "1": 7434,
      "roma": "ROCCANTICA",
      "rm": "RI",
      "1234": 412057061
    },
    {
      "1": 7435,
      "roma": "ROCCAPALUMBA",
      "rm": "PA",
      "1234": 419082062
    },
    {
      "1": 7436,
      "roma": "ROCCAPIEMONTE",
      "rm": "SA",
      "1234": 415065108
    },
    {
      "1": 7437,
      "roma": "ROCCARAINOLA",
      "rm": "NA",
      "1234": 415063065
    },
    {
      "1": 7438,
      "roma": "ROCCAFIORITA",
      "rm": "ME",
      "1234": 419083071
    },
    {
      "1": 7439,
      "roma": "ROCCAFLUVIONE",
      "rm": "AP",
      "1234": 411044064
    },
    {
      "1": 7440,
      "roma": "ROCCAFORTE DEL GRECO",
      "rm": "RC",
      "1234": 418080066
    },
    {
      "1": 7441,
      "roma": "ROCCAFORTE LIGURE",
      "rm": "AL",
      "1234": 401006146
    },
    {
      "1": 7442,
      "roma": "ROCCAFORTE MONDOVI'",
      "rm": "CN",
      "1234": 401004190
    },
    {
      "1": 7443,
      "roma": "ROCCASECCA DEI VOLSCI",
      "rm": "LT",
      "1234": 412059023
    },
    {
      "1": 7444,
      "roma": "ROCCASICURA",
      "rm": "IS",
      "1234": 414094041
    },
    {
      "1": 7445,
      "roma": "ROCCASPARVERA",
      "rm": "CN",
      "1234": 401004191
    },
    {
      "1": 7446,
      "roma": "RODDINO",
      "rm": "CN",
      "1234": 401004195
    },
    {
      "1": 7447,
      "roma": "RODELLO",
      "rm": "CN",
      "1234": 401004196
    },
    {
      "1": 7448,
      "roma": "RODENGO",
      "rm": "BZ",
      "1234": 404021075
    },
    {
      "1": 7449,
      "roma": "RODENGO-SAIANO",
      "rm": "BS",
      "1234": 403017163
    },
    {
      "1": 7450,
      "roma": "RODERO",
      "rm": "CO",
      "1234": 403013197
    },
    {
      "1": 7451,
      "roma": "RODI GARGANICO",
      "rm": "FG",
      "1234": 416071043
    },
    {
      "1": 7452,
      "roma": "RODI' MILICI",
      "rm": "ME",
      "1234": 419083075
    },
    {
      "1": 7453,
      "roma": "ROCCELLA VALDEMONE",
      "rm": "ME",
      "1234": 419083074
    },
    {
      "1": 7454,
      "roma": "ROCCHETTA A VOLTURNO",
      "rm": "IS",
      "1234": 414094042
    },
    {
      "1": 7455,
      "roma": "ROCCHETTA BELBO",
      "rm": "CN",
      "1234": 401004193
    },
    {
      "1": 7456,
      "roma": "ROCCHETTA DI VARA",
      "rm": "SP",
      "1234": 407011025
    },
    {
      "1": 7457,
      "roma": "ROCCHETTA E CROCE",
      "rm": "CE",
      "1234": 415061072
    },
    {
      "1": 7458,
      "roma": "ROCCARASO",
      "rm": "AQ",
      "1234": 413066084
    },
    {
      "1": 7459,
      "roma": "ROCCAROMANA",
      "rm": "CE",
      "1234": 415061071
    },
    {
      "1": 7460,
      "roma": "ROCCASCALEGNA",
      "rm": "CH",
      "1234": 413069075
    },
    {
      "1": 7461,
      "roma": "ROCCASECCA",
      "rm": "FR",
      "1234": 412060060
    },
    {
      "1": 7462,
      "roma": "RODANO",
      "rm": "MI",
      "1234": 403015185
    },
    {
      "1": 7463,
      "roma": "RODDI",
      "rm": "CN",
      "1234": 401004194
    },
    {
      "1": 7464,
      "roma": "ROLETTO",
      "rm": "TO",
      "1234": 401001222
    },
    {
      "1": 7465,
      "roma": "ROLO",
      "rm": "RE",
      "1234": 408035035
    },
    {
      "1": 7466,
      "roma": "ROMA",
      "rm": "RM",
      "1234": 412058091
    },
    {
      "1": 7467,
      "roma": "ROMAGNANO AL MONTE",
      "rm": "SA",
      "1234": 415065110
    },
    {
      "1": 7468,
      "roma": "ROMAGNANO SESIA",
      "rm": "NO",
      "1234": 401003130
    },
    {
      "1": 7469,
      "roma": "RODIGO",
      "rm": "MN",
      "1234": 403020051
    },
    {
      "1": 7470,
      "roma": "ROE' VOLCIANO",
      "rm": "BS",
      "1234": 403017164
    },
    {
      "1": 7471,
      "roma": "ROFRANO",
      "rm": "SA",
      "1234": 415065109
    },
    {
      "1": 7472,
      "roma": "ROGENO",
      "rm": "LC",
      "1234": 403097072
    },
    {
      "1": 7473,
      "roma": "ROGGIANO GRAVINA",
      "rm": "CS",
      "1234": 418078104
    },
    {
      "1": 7474,
      "roma": "ROCCHETTA LIGURE",
      "rm": "AL",
      "1234": 401006148
    },
    {
      "1": 7475,
      "roma": "ROCCHETTA NERVINA",
      "rm": "IM",
      "1234": 407008051
    },
    {
      "1": 7476,
      "roma": "ROCCHETTA PALAFEA",
      "rm": "AT",
      "1234": 401005095
    },
    {
      "1": 7477,
      "roma": "ROCCHETTA SANT'ANTONIO",
      "rm": "FG",
      "1234": 416071042
    },
    {
      "1": 7478,
      "roma": "ROCCHETTA TANARO",
      "rm": "AT",
      "1234": 401005096
    },
    {
      "1": 7479,
      "roma": "ROIATE",
      "rm": "RM",
      "1234": 412058090
    },
    {
      "1": 7480,
      "roma": "ROIO DEL SANGRO",
      "rm": "CH",
      "1234": 413069077
    },
    {
      "1": 7481,
      "roma": "ROISAN",
      "rm": "AO",
      "1234": 402007057
    },
    {
      "1": 7482,
      "roma": "RONAGO",
      "rm": "CO",
      "1234": 403013199
    },
    {
      "1": 7483,
      "roma": "RONCA'",
      "rm": "VR",
      "1234": 405023063
    },
    {
      "1": 7484,
      "roma": "RONCADE",
      "rm": "TV",
      "1234": 405026069
    },
    {
      "1": 7485,
      "roma": "RONCADELLE",
      "rm": "BS",
      "1234": 403017165
    },
    {
      "1": 7486,
      "roma": "RONCARO",
      "rm": "PV",
      "1234": 403018129
    },
    {
      "1": 7487,
      "roma": "RONCEGNO",
      "rm": "TN",
      "1234": 404022156
    },
    {
      "1": 7488,
      "roma": "ROMAGNESE",
      "rm": "PV",
      "1234": 403018128
    },
    {
      "1": 7489,
      "roma": "ROMALLO",
      "rm": "TN",
      "1234": 404022154
    },
    {
      "1": 7490,
      "roma": "ROMANA",
      "rm": "SS",
      "1234": 420090061
    },
    {
      "1": 7491,
      "roma": "ROMANENGO",
      "rm": "CR",
      "1234": 403019086
    },
    {
      "1": 7492,
      "roma": "ROMANO CANAVESE",
      "rm": "TO",
      "1234": 401001223
    },
    {
      "1": 7493,
      "roma": "ROMANO D'EZZELINO",
      "rm": "VI",
      "1234": 405024086
    },
    {
      "1": 7494,
      "roma": "ROMANO DI LOMBARDIA",
      "rm": "BG",
      "1234": 403016183
    },
    {
      "1": 7495,
      "roma": "ROGHUDI",
      "rm": "RC",
      "1234": 418080068
    },
    {
      "1": 7496,
      "roma": "ROGLIANO",
      "rm": "CS",
      "1234": 418078105
    },
    {
      "1": 7497,
      "roma": "ROGNANO",
      "rm": "PV",
      "1234": 403018127
    },
    {
      "1": 7498,
      "roma": "ROGNO",
      "rm": "BG",
      "1234": 403016182
    },
    {
      "1": 7499,
      "roma": "ROGOLO",
      "rm": "SO",
      "1234": 403014056
    },
    {
      "1": 7500,
      "roma": "ROMENO",
      "rm": "TN",
      "1234": 404022155
    },
    {
      "1": 7501,
      "roma": "ROMENTINO",
      "rm": "NO",
      "1234": 401003131
    },
    {
      "1": 7502,
      "roma": "ROMETTA",
      "rm": "ME",
      "1234": 419083076
    },
    {
      "1": 7503,
      "roma": "RONDANINA",
      "rm": "GE",
      "1234": 407010050
    },
    {
      "1": 7504,
      "roma": "RONDISSONE",
      "rm": "TO",
      "1234": 401001225
    },
    {
      "1": 7505,
      "roma": "RONSECCO",
      "rm": "VC",
      "1234": 401002118
    },
    {
      "1": 7506,
      "roma": "RONCELLO",
      "rm": "MB",
      "1234": 403108055
    },
    {
      "1": 7507,
      "roma": "RONCHI DEI LEGIONARI",
      "rm": "GO",
      "1234": 406031016
    },
    {
      "1": 7508,
      "roma": "RONCHI VALSUGANA",
      "rm": "TN",
      "1234": 404022157
    },
    {
      "1": 7509,
      "roma": "RONCHIS",
      "rm": "UD",
      "1234": 406030097
    },
    {
      "1": 7510,
      "roma": "RONCIGLIONE",
      "rm": "VT",
      "1234": 412056045
    },
    {
      "1": 7511,
      "roma": "RONCO ALL'ADIGE",
      "rm": "VR",
      "1234": 405023064
    },
    {
      "1": 7512,
      "roma": "RONCO BIELLESE",
      "rm": "BI",
      "1234": 401096053
    },
    {
      "1": 7513,
      "roma": "ROMANS D'ISONZO",
      "rm": "GO",
      "1234": 406031015
    },
    {
      "1": 7514,
      "roma": "ROMBIOLO",
      "rm": "VV",
      "1234": 418102031
    },
    {
      "1": 7515,
      "roma": "RONCOFERRARO",
      "rm": "MN",
      "1234": 403020052
    },
    {
      "1": 7516,
      "roma": "RONCOFREDDO",
      "rm": "FO",
      "1234": 408040037
    },
    {
      "1": 7517,
      "roma": "RONCOLA",
      "rm": "BG",
      "1234": 403016185
    },
    {
      "1": 7518,
      "roma": "ROSETO CAPO SPULICO",
      "rm": "CS",
      "1234": 418078107
    },
    {
      "1": 7519,
      "roma": "ROSETO DEGLI ABRUZZI",
      "rm": "TE",
      "1234": 413067037
    },
    {
      "1": 7520,
      "roma": "ROSETO VALFORTORE",
      "rm": "FG",
      "1234": 416071044
    },
    {
      "1": 7521,
      "roma": "ROSIGNANO MARITTIMO",
      "rm": "LI",
      "1234": 409049017
    },
    {
      "1": 7522,
      "roma": "ROSIGNANO MONFERRATO",
      "rm": "AL",
      "1234": 401006149
    },
    {
      "1": 7523,
      "roma": "RONZO-CHIENIS",
      "rm": "TN",
      "1234": 404022135
    },
    {
      "1": 7524,
      "roma": "RONZONE",
      "rm": "TN",
      "1234": 404022159
    },
    {
      "1": 7525,
      "roma": "ROPPOLO",
      "rm": "BI",
      "1234": 401096054
    },
    {
      "1": 7526,
      "roma": "RORA'",
      "rm": "TO",
      "1234": 401001226
    },
    {
      "1": 7527,
      "roma": "RORETO CHISONE",
      "rm": "TO",
      "1234": 401001227
    },
    {
      "1": 7528,
      "roma": "ROSA'",
      "rm": "VI",
      "1234": 405024087
    },
    {
      "1": 7529,
      "roma": "ROSARNO",
      "rm": "RC",
      "1234": 418080069
    },
    {
      "1": 7530,
      "roma": "ROSASCO",
      "rm": "PV",
      "1234": 403018130
    },
    {
      "1": 7531,
      "roma": "RONCO CANAVESE",
      "rm": "TO",
      "1234": 401001224
    },
    {
      "1": 7532,
      "roma": "RONCO SCRIVIA",
      "rm": "GE",
      "1234": 407010049
    },
    {
      "1": 7533,
      "roma": "RONCOBELLO",
      "rm": "BG",
      "1234": 403016184
    },
    {
      "1": 7534,
      "roma": "ROSCIANO",
      "rm": "PE",
      "1234": 413068035
    },
    {
      "1": 7535,
      "roma": "ROSCIGNO",
      "rm": "SA",
      "1234": 415065111
    },
    {
      "1": 7536,
      "roma": "ROSE",
      "rm": "CS",
      "1234": 418078106
    },
    {
      "1": 7537,
      "roma": "ROSELLO",
      "rm": "CH",
      "1234": 413069078
    },
    {
      "1": 7538,
      "roma": "ROTONDI",
      "rm": "AV",
      "1234": 415064080
    },
    {
      "1": 7539,
      "roma": "ROTTOFRENO",
      "rm": "PC",
      "1234": 408033039
    },
    {
      "1": 7540,
      "roma": "ROTZO",
      "rm": "VI",
      "1234": 405024089
    },
    {
      "1": 7541,
      "roma": "ROURE",
      "rm": "TO",
      "1234": 401001571
    },
    {
      "1": 7542,
      "roma": "ROSOLINA",
      "rm": "RO",
      "1234": 405029040
    },
    {
      "1": 7543,
      "roma": "ROSOLINI",
      "rm": "SR",
      "1234": 419089016
    },
    {
      "1": 7544,
      "roma": "ROSORA",
      "rm": "AN",
      "1234": 411042040
    },
    {
      "1": 7545,
      "roma": "ROSSA",
      "rm": "VC",
      "1234": 401002121
    },
    {
      "1": 7546,
      "roma": "ROSSANA",
      "rm": "CN",
      "1234": 401004197
    },
    {
      "1": 7547,
      "roma": "ROSSANO VENETO",
      "rm": "VI",
      "1234": 405024088
    },
    {
      "1": 7548,
      "roma": "ROSSIGLIONE",
      "rm": "GE",
      "1234": 407010051
    },
    {
      "1": 7549,
      "roma": "ROSATE",
      "rm": "MI",
      "1234": 403015188
    },
    {
      "1": 7550,
      "roma": "ROSAZZA",
      "rm": "BI",
      "1234": 401096055
    },
    {
      "1": 7551,
      "roma": "SAN MARCELLO PITEGLIO",
      "rm": "PT",
      "1234": 409047024
    },
    {
      "1": 7552,
      "roma": "ROTA GRECA",
      "rm": "CS",
      "1234": 418078109
    },
    {
      "1": 7553,
      "roma": "ROTELLA",
      "rm": "AP",
      "1234": 411044065
    },
    {
      "1": 7554,
      "roma": "ROTELLO",
      "rm": "CB",
      "1234": 414070061
    },
    {
      "1": 7555,
      "roma": "ROTONDA",
      "rm": "PZ",
      "1234": 417076070
    },
    {
      "1": 7556,
      "roma": "ROTONDELLA",
      "rm": "MT",
      "1234": 417077023
    },
    {
      "1": 7557,
      "roma": "ROVESCALA",
      "rm": "PV",
      "1234": 403018131
    },
    {
      "1": 7558,
      "roma": "ROVETTA",
      "rm": "BG",
      "1234": 403016187
    },
    {
      "1": 7559,
      "roma": "ROVIANO",
      "rm": "RM",
      "1234": 412058092
    },
    {
      "1": 7560,
      "roma": "ROVASENDA",
      "rm": "VC",
      "1234": 401002122
    },
    {
      "1": 7561,
      "roma": "ROVATO",
      "rm": "BS",
      "1234": 403017166
    },
    {
      "1": 7562,
      "roma": "ROVEGNO",
      "rm": "GE",
      "1234": 407010052
    },
    {
      "1": 7563,
      "roma": "ROVELLASCA",
      "rm": "CO",
      "1234": 403013201
    },
    {
      "1": 7564,
      "roma": "ROVELLO PORRO",
      "rm": "CO",
      "1234": 403013202
    },
    {
      "1": 7565,
      "roma": "ROVERBELLA",
      "rm": "MN",
      "1234": 403020053
    },
    {
      "1": 7566,
      "roma": "ROSTA",
      "rm": "TO",
      "1234": 401001228
    },
    {
      "1": 7567,
      "roma": "ROTA D'IMAGNA",
      "rm": "BG",
      "1234": 403016186
    },
    {
      "1": 7568,
      "roma": "ROVEREDO IN PIANO",
      "rm": "PN",
      "1234": 406093036
    },
    {
      "1": 7569,
      "roma": "ROVERETO",
      "rm": "TN",
      "1234": 404022161
    },
    {
      "1": 7570,
      "roma": "RUMO",
      "rm": "TN",
      "1234": 404022163
    },
    {
      "1": 7571,
      "roma": "RUOTI",
      "rm": "PZ",
      "1234": 417076071
    },
    {
      "1": 7572,
      "roma": "ROVIGO",
      "rm": "RO",
      "1234": 405029041
    },
    {
      "1": 7573,
      "roma": "ROVITO",
      "rm": "CS",
      "1234": 418078110
    },
    {
      "1": 7574,
      "roma": "ROVOLON",
      "rm": "PD",
      "1234": 405028071
    },
    {
      "1": 7575,
      "roma": "ROZZANO",
      "rm": "MI",
      "1234": 403015189
    },
    {
      "1": 7576,
      "roma": "RUBANO",
      "rm": "PD",
      "1234": 405028072
    },
    {
      "1": 7577,
      "roma": "RUBIANA",
      "rm": "TO",
      "1234": 401001229
    },
    {
      "1": 7578,
      "roma": "RUBIERA",
      "rm": "RE",
      "1234": 408035036
    },
    {
      "1": 7579,
      "roma": "ROVERCHIARA",
      "rm": "VR",
      "1234": 405023065
    },
    {
      "1": 7580,
      "roma": "ROVERE' DELLA LUNA",
      "rm": "TN",
      "1234": 404022160
    },
    {
      "1": 7581,
      "roma": "ROVERE' VERONESE",
      "rm": "VR",
      "1234": 405023067
    },
    {
      "1": 7582,
      "roma": "ROVEREDO DI GUA'",
      "rm": "VR",
      "1234": 405023066
    },
    {
      "1": 7583,
      "roma": "RUFFIA",
      "rm": "CN",
      "1234": 401004198
    },
    {
      "1": 7584,
      "roma": "RUFINA",
      "rm": "FI",
      "1234": 409048037
    },
    {
      "1": 7585,
      "roma": "RUINAS",
      "rm": "OR",
      "1234": 420095044
    },
    {
      "1": 7586,
      "roma": "SACCOLONGO",
      "rm": "PD",
      "1234": 405028073
    },
    {
      "1": 7587,
      "roma": "SACILE",
      "rm": "PN",
      "1234": 406093037
    },
    {
      "1": 7588,
      "roma": "SACROFANO",
      "rm": "RM",
      "1234": 412058093
    },
    {
      "1": 7589,
      "roma": "RUSSI",
      "rm": "RA",
      "1234": 408039016
    },
    {
      "1": 7590,
      "roma": "RUTIGLIANO",
      "rm": "BA",
      "1234": 416072037
    },
    {
      "1": 7591,
      "roma": "RUTINO",
      "rm": "SA",
      "1234": 415065112
    },
    {
      "1": 7592,
      "roma": "RUVIANO",
      "rm": "CE",
      "1234": 415061073
    },
    {
      "1": 7593,
      "roma": "RUFFRE'-MENDOLA",
      "rm": "TN",
      "1234": 404022962
    },
    {
      "1": 7594,
      "roma": "RUVO DEL MONTE",
      "rm": "PZ",
      "1234": 417076072
    },
    {
      "1": 7595,
      "roma": "RUVO DI PUGLIA",
      "rm": "BA",
      "1234": 416072038
    },
    {
      "1": 7596,
      "roma": "RUDA",
      "rm": "UD",
      "1234": 406030098
    },
    {
      "1": 7597,
      "roma": "RUDIANO",
      "rm": "BS",
      "1234": 403017167
    },
    {
      "1": 7598,
      "roma": "RUEGLIO",
      "rm": "TO",
      "1234": 401001230
    },
    {
      "1": 7599,
      "roma": "RUFFANO",
      "rm": "LE",
      "1234": 416075064
    },
    {
      "1": 7600,
      "roma": "SABBIO CHIESE",
      "rm": "BS",
      "1234": 403017168
    },
    {
      "1": 7601,
      "roma": "SABBIONETA",
      "rm": "MN",
      "1234": 403020054
    },
    {
      "1": 7602,
      "roma": "SACCO",
      "rm": "SA",
      "1234": 415065113
    },
    {
      "1": 7603,
      "roma": "SAINT-RHEMY",
      "rm": "AO",
      "1234": 402007064
    },
    {
      "1": 7604,
      "roma": "SAINT-VINCENT",
      "rm": "AO",
      "1234": 402007065
    },
    {
      "1": 7605,
      "roma": "SALA BAGANZA",
      "rm": "PR",
      "1234": 408034031
    },
    {
      "1": 7606,
      "roma": "SADALI",
      "rm": "NU",
      "1234": 420091074
    },
    {
      "1": 7607,
      "roma": "SAGAMA",
      "rm": "NU",
      "1234": 420091075
    },
    {
      "1": 7608,
      "roma": "SAGLIANO MICCA",
      "rm": "BI",
      "1234": 401096056
    },
    {
      "1": 7609,
      "roma": "SAGRADO",
      "rm": "GO",
      "1234": 406031017
    },
    {
      "1": 7610,
      "roma": "SAGRON MIS",
      "rm": "TN",
      "1234": 404022164
    },
    {
      "1": 7611,
      "roma": "SABAUDIA",
      "rm": "LT",
      "1234": 412059024
    },
    {
      "1": 7612,
      "roma": "SAINT-MARCEL",
      "rm": "AO",
      "1234": 402007060
    },
    {
      "1": 7613,
      "roma": "SAINT-NICOLAS",
      "rm": "AO",
      "1234": 402007061
    },
    {
      "1": 7614,
      "roma": "SAINT-OYEN",
      "rm": "AO",
      "1234": 402007062
    },
    {
      "1": 7615,
      "roma": "SAINT-PIERRE",
      "rm": "AO",
      "1234": 402007063
    },
    {
      "1": 7616,
      "roma": "SALE SAN GIOVANNI",
      "rm": "CN",
      "1234": 401004200
    },
    {
      "1": 7617,
      "roma": "SALEMI",
      "rm": "TP",
      "1234": 419081018
    },
    {
      "1": 7618,
      "roma": "SALENTO",
      "rm": "SA",
      "1234": 415065115
    },
    {
      "1": 7619,
      "roma": "SALERANO CANAVESE",
      "rm": "TO",
      "1234": 401001233
    },
    {
      "1": 7620,
      "roma": "SALERANO SUL LAMBRO",
      "rm": "LO",
      "1234": 403098046
    },
    {
      "1": 7621,
      "roma": "SALA BIELLESE",
      "rm": "BI",
      "1234": 401096057
    },
    {
      "1": 7622,
      "roma": "SALA BOLOGNESE",
      "rm": "BO",
      "1234": 408037050
    },
    {
      "1": 7623,
      "roma": "SALA COMACINA",
      "rm": "CO",
      "1234": 403013203
    },
    {
      "1": 7624,
      "roma": "SALA CONSILINA",
      "rm": "SA",
      "1234": 415065114
    },
    {
      "1": 7625,
      "roma": "SALA MONFERRATO",
      "rm": "AL",
      "1234": 401006150
    },
    {
      "1": 7626,
      "roma": "SALANDRA",
      "rm": "MT",
      "1234": 417077024
    },
    {
      "1": 7627,
      "roma": "SALAPARUTA",
      "rm": "TP",
      "1234": 419081017
    },
    {
      "1": 7628,
      "roma": "SALARA",
      "rm": "RO",
      "1234": 405029042
    },
    {
      "1": 7629,
      "roma": "SALASCO",
      "rm": "VC",
      "1234": 401002126
    },
    {
      "1": 7630,
      "roma": "SALASSA",
      "rm": "TO",
      "1234": 401001231
    },
    {
      "1": 7631,
      "roma": "SAINT-CHRISTOPHE",
      "rm": "AO",
      "1234": 402007058
    },
    {
      "1": 7632,
      "roma": "SAINT-DENIS",
      "rm": "AO",
      "1234": 402007059
    },
    {
      "1": 7633,
      "roma": "SALE",
      "rm": "AL",
      "1234": 401006151
    },
    {
      "1": 7634,
      "roma": "SALE DELLE LANGHE",
      "rm": "CN",
      "1234": 401004199
    },
    {
      "1": 7635,
      "roma": "SALE MARASINO",
      "rm": "BS",
      "1234": 403017169
    },
    {
      "1": 7636,
      "roma": "SALTRIO",
      "rm": "VA",
      "1234": 403012117
    },
    {
      "1": 7637,
      "roma": "SALUDECIO",
      "rm": "RN",
      "1234": 408099015
    },
    {
      "1": 7638,
      "roma": "SALUGGIA",
      "rm": "VC",
      "1234": 401002128
    },
    {
      "1": 7639,
      "roma": "SALUSSOLA",
      "rm": "BI",
      "1234": 401096058
    },
    {
      "1": 7640,
      "roma": "SALERNO",
      "rm": "SA",
      "1234": 415065116
    },
    {
      "1": 7641,
      "roma": "SALGAREDA",
      "rm": "TV",
      "1234": 405026070
    },
    {
      "1": 7642,
      "roma": "SALI VERCELLESE",
      "rm": "VC",
      "1234": 401002127
    },
    {
      "1": 7643,
      "roma": "SALICE SALENTINO",
      "rm": "LE",
      "1234": 416075065
    },
    {
      "1": 7644,
      "roma": "SALICETO",
      "rm": "CN",
      "1234": 401004201
    },
    {
      "1": 7645,
      "roma": "SALISANO",
      "rm": "RI",
      "1234": 412057063
    },
    {
      "1": 7646,
      "roma": "SALBERTRAND",
      "rm": "TO",
      "1234": 401001232
    },
    {
      "1": 7647,
      "roma": "SALCEDO",
      "rm": "VI",
      "1234": 405024090
    },
    {
      "1": 7648,
      "roma": "SALCITO",
      "rm": "CB",
      "1234": 414070062
    },
    {
      "1": 7649,
      "roma": "SALORNO",
      "rm": "BZ",
      "1234": 404021076
    },
    {
      "1": 7650,
      "roma": "SALSOMAGGIORE TERME",
      "rm": "PR",
      "1234": 408034032
    },
    {
      "1": 7651,
      "roma": "SAMMICHELE DI BARI",
      "rm": "BA",
      "1234": 416072039
    },
    {
      "1": 7652,
      "roma": "SAMO",
      "rm": "RC",
      "1234": 418080070
    },
    {
      "1": 7653,
      "roma": "SALUZZO",
      "rm": "CN",
      "1234": 401004203
    },
    {
      "1": 7654,
      "roma": "SALVE",
      "rm": "LE",
      "1234": 416075066
    },
    {
      "1": 7655,
      "roma": "SALVIROLA",
      "rm": "CR",
      "1234": 403019087
    },
    {
      "1": 7656,
      "roma": "SALVITELLE",
      "rm": "SA",
      "1234": 415065117
    },
    {
      "1": 7657,
      "roma": "SALZA DI PINEROLO",
      "rm": "TO",
      "1234": 401001234
    },
    {
      "1": 7658,
      "roma": "SALZA IRPINA",
      "rm": "AV",
      "1234": 415064081
    },
    {
      "1": 7659,
      "roma": "SALZANO",
      "rm": "VE",
      "1234": 405027032
    },
    {
      "1": 7660,
      "roma": "SAMARATE",
      "rm": "VA",
      "1234": 403012118
    },
    {
      "1": 7661,
      "roma": "SALIZZOLE",
      "rm": "VR",
      "1234": 405023068
    },
    {
      "1": 7662,
      "roma": "SALLE",
      "rm": "PE",
      "1234": 413068036
    },
    {
      "1": 7663,
      "roma": "SALMOUR",
      "rm": "CN",
      "1234": 401004202
    },
    {
      "1": 7664,
      "roma": "SALO'",
      "rm": "BS",
      "1234": 403017170
    },
    {
      "1": 7665,
      "roma": "SAMBUCA DI SICILIA",
      "rm": "AG",
      "1234": 419084034
    },
    {
      "1": 7666,
      "roma": "SAMBUCA PISTOIESE",
      "rm": "PT",
      "1234": 409047018
    },
    {
      "1": 7667,
      "roma": "SAMBUCI",
      "rm": "RM",
      "1234": 412058094
    },
    {
      "1": 7668,
      "roma": "SAMBUCO",
      "rm": "CN",
      "1234": 401004204
    },
    {
      "1": 7669,
      "roma": "SAN BENEDETTO DEI MARSI",
      "rm": "AQ",
      "1234": 413066085
    },
    {
      "1": 7670,
      "roma": "SAN BENEDETTO DEL TRONTO",
      "rm": "AP",
      "1234": 411044066
    },
    {
      "1": 7671,
      "roma": "SAN BENEDETTO IN PERILLIS",
      "rm": "AQ",
      "1234": 413066086
    },
    {
      "1": 7672,
      "roma": "SAN BENEDETTO PO",
      "rm": "MN",
      "1234": 403020055
    },
    {
      "1": 7673,
      "roma": "SAN BENEDETTO ULLANO",
      "rm": "CS",
      "1234": 418078112
    },
    {
      "1": 7674,
      "roma": "SAN BENEDETTO VAL DI SAMBRO",
      "rm": "BO",
      "1234": 408037051
    },
    {
      "1": 7675,
      "roma": "SAMOLACO",
      "rm": "SO",
      "1234": 403014057
    },
    {
      "1": 7676,
      "roma": "SAMONE",
      "rm": "TN",
      "1234": 404022165
    },
    {
      "1": 7678,
      "roma": "SAMPEYRE",
      "rm": "CN",
      "1234": 401004205
    },
    {
      "1": 7679,
      "roma": "SAMUGHEO",
      "rm": "OR",
      "1234": 420095045
    },
    {
      "1": 7680,
      "roma": "SAMASSI",
      "rm": "CA",
      "1234": 420092052
    },
    {
      "1": 7681,
      "roma": "SAMATZAI",
      "rm": "CA",
      "1234": 420092053
    },
    {
      "1": 7682,
      "roma": "SAN BASILE",
      "rm": "CS",
      "1234": 418078111
    },
    {
      "1": 7683,
      "roma": "SAN BASILIO",
      "rm": "CA",
      "1234": 420092054
    },
    {
      "1": 7684,
      "roma": "SAN BASSANO",
      "rm": "CR",
      "1234": 403019088
    },
    {
      "1": 7685,
      "roma": "SAN BELLINO",
      "rm": "RO",
      "1234": 405029043
    },
    {
      "1": 7686,
      "roma": "SAN BENEDETTO BELBO",
      "rm": "CN",
      "1234": 401004206
    },
    {
      "1": 7687,
      "roma": "SAN CATALDO",
      "rm": "CL",
      "1234": 419085016
    },
    {
      "1": 7688,
      "roma": "SAN CESAREO",
      "rm": "RM",
      "1234": 412058119
    },
    {
      "1": 7689,
      "roma": "SAN CESARIO DI LECCE",
      "rm": "LE",
      "1234": 416075068
    },
    {
      "1": 7690,
      "roma": "SAN CESARIO SUL PANARO",
      "rm": "MO",
      "1234": 408036036
    },
    {
      "1": 7691,
      "roma": "SAN CHIRICO NUOVO",
      "rm": "PZ",
      "1234": 417076073
    },
    {
      "1": 7692,
      "roma": "SAN CHIRICO RAPARO",
      "rm": "PZ",
      "1234": 417076074
    },
    {
      "1": 7693,
      "roma": "SAN BENIGNO CANAVESE",
      "rm": "TO",
      "1234": 401001236
    },
    {
      "1": 7694,
      "roma": "SAN BERNARDINO VERBANO",
      "rm": "VB",
      "1234": 401103061
    },
    {
      "1": 7695,
      "roma": "SAN BIAGIO DELLA CIMA",
      "rm": "IM",
      "1234": 407008053
    },
    {
      "1": 7696,
      "roma": "SAN BIAGIO DI CALLALTA",
      "rm": "TV",
      "1234": 405026071
    },
    {
      "1": 7697,
      "roma": "SAN BIAGIO PLATANI",
      "rm": "AG",
      "1234": 419084035
    },
    {
      "1": 7698,
      "roma": "SAN BIAGIO SARACINISCO",
      "rm": "FR",
      "1234": 412060061
    },
    {
      "1": 7699,
      "roma": "SAN BIASE",
      "rm": "CB",
      "1234": 414070063
    },
    {
      "1": 7700,
      "roma": "SAN BONIFACIO",
      "rm": "VR",
      "1234": 405023069
    },
    {
      "1": 7701,
      "roma": "SAN BUONO",
      "rm": "CH",
      "1234": 413069079
    },
    {
      "1": 7702,
      "roma": "SAN BARTOLOMEO AL MARE",
      "rm": "IM",
      "1234": 407008052
    },
    {
      "1": 7703,
      "roma": "SAN BARTOLOMEO IN GALDO",
      "rm": "BN",
      "1234": 415062057
    },
    {
      "1": 7704,
      "roma": "SAN BARTOLOMEO VAL CAVARGNA",
      "rm": "CO",
      "1234": 403013204
    },
    {
      "1": 7705,
      "roma": "SAN CARLO CANAVESE",
      "rm": "TO",
      "1234": 401001237
    },
    {
      "1": 7706,
      "roma": "SAN CASCIANO DEI BAGNI",
      "rm": "SI",
      "1234": 409052027
    },
    {
      "1": 7707,
      "roma": "SAN CASCIANO IN VAL DI PESA",
      "rm": "FI",
      "1234": 409048038
    },
    {
      "1": 7708,
      "roma": "SAN CASSIANO",
      "rm": "LE",
      "1234": 416075095
    },
    {
      "1": 7709,
      "roma": "SAN DANIELE DEL FRIULI",
      "rm": "UD",
      "1234": 406030099
    },
    {
      "1": 7710,
      "roma": "SAN DANIELE PO",
      "rm": "CR",
      "1234": 403019089
    },
    {
      "1": 7711,
      "roma": "SAN DEMETRIO CORONE",
      "rm": "CS",
      "1234": 418078114
    },
    {
      "1": 7712,
      "roma": "SAN DEMETRIO NE' VESTINI",
      "rm": "AQ",
      "1234": 413066087
    },
    {
      "1": 7713,
      "roma": "SAN DIDERO",
      "rm": "TO",
      "1234": 401001239
    },
    {
      "1": 7714,
      "roma": "SAN CIPIRELLO",
      "rm": "PA",
      "1234": 419082063
    },
    {
      "1": 7715,
      "roma": "SAN CIPRIANO D'AVERSA",
      "rm": "CE",
      "1234": 415061074
    },
    {
      "1": 7716,
      "roma": "SAN CIPRIANO PICENTINO",
      "rm": "SA",
      "1234": 415065118
    },
    {
      "1": 7717,
      "roma": "SAN CIPRIANO PO",
      "rm": "PV",
      "1234": 403018133
    },
    {
      "1": 7718,
      "roma": "SAN CLEMENTE",
      "rm": "RN",
      "1234": 408099016
    },
    {
      "1": 7719,
      "roma": "SAN COLOMBANO AL LAMBRO",
      "rm": "MI",
      "1234": 403015191
    },
    {
      "1": 7720,
      "roma": "SAN COLOMBANO BELMONTE",
      "rm": "TO",
      "1234": 401001238
    },
    {
      "1": 7721,
      "roma": "SAN COLOMBANO CERTENOLI",
      "rm": "GE",
      "1234": 407010053
    },
    {
      "1": 7722,
      "roma": "SAN CONO",
      "rm": "CT",
      "1234": 419087040
    },
    {
      "1": 7723,
      "roma": "SAN COSMO ALBANESE",
      "rm": "CS",
      "1234": 418078113
    },
    {
      "1": 7724,
      "roma": "SAN CALOGERO",
      "rm": "VV",
      "1234": 418102032
    },
    {
      "1": 7725,
      "roma": "SAN CANDIDO",
      "rm": "BZ",
      "1234": 404021077
    },
    {
      "1": 7726,
      "roma": "SAN CANZIAN D'ISONZO",
      "rm": "GO",
      "1234": 406031018
    },
    {
      "1": 7727,
      "roma": "SAN CRISTOFORO",
      "rm": "AL",
      "1234": 401006152
    },
    {
      "1": 7728,
      "roma": "SAN DAMIANO AL COLLE",
      "rm": "PV",
      "1234": 403018134
    },
    {
      "1": 7729,
      "roma": "SAN DAMIANO D'ASTI",
      "rm": "AT",
      "1234": 401005097
    },
    {
      "1": 7730,
      "roma": "SAN DAMIANO MACRA",
      "rm": "CN",
      "1234": 401004207
    },
    {
      "1": 7731,
      "roma": "SAN FILI",
      "rm": "CS",
      "1234": 418078116
    },
    {
      "1": 7732,
      "roma": "SAN FILIPPO DEL MELA",
      "rm": "ME",
      "1234": 419083077
    },
    {
      "1": 7733,
      "roma": "SAN FIOR",
      "rm": "TV",
      "1234": 405026072
    },
    {
      "1": 7734,
      "roma": "SAN FIORANO",
      "rm": "LO",
      "1234": 403098047
    },
    {
      "1": 7735,
      "roma": "SAN FLORIANO DEL COLLIO",
      "rm": "GO",
      "1234": 406031019
    },
    {
      "1": 7736,
      "roma": "SAN DONA' DI PIAVE",
      "rm": "VE",
      "1234": 405027033
    },
    {
      "1": 7737,
      "roma": "SAN DONACI",
      "rm": "BR",
      "1234": 416074013
    },
    {
      "1": 7738,
      "roma": "SAN DONATO DI LECCE",
      "rm": "LE",
      "1234": 416075069
    },
    {
      "1": 7739,
      "roma": "SAN DONATO DI NINEA",
      "rm": "CS",
      "1234": 418078115
    },
    {
      "1": 7740,
      "roma": "SAN DONATO MILANESE",
      "rm": "MI",
      "1234": 403015192
    },
    {
      "1": 7741,
      "roma": "SAN DONATO VAL DI COMINO",
      "rm": "FR",
      "1234": 412060062
    },
    {
      "1": 7742,
      "roma": "SAN DORLIGO DELLA VALLE",
      "rm": "TS",
      "1234": 406032004
    },
    {
      "1": 7743,
      "roma": "SAN FELE",
      "rm": "PZ",
      "1234": 417076076
    },
    {
      "1": 7744,
      "roma": "SAN COSTANTINO ALBANESE",
      "rm": "PZ",
      "1234": 417076075
    },
    {
      "1": 7745,
      "roma": "SAN COSTANTINO CALABRO",
      "rm": "VV",
      "1234": 418102033
    },
    {
      "1": 7746,
      "roma": "SAN COSTANZO",
      "rm": "PS",
      "1234": 411041051
    },
    {
      "1": 7747,
      "roma": "SAN FELICE SUL PANARO",
      "rm": "MO",
      "1234": 408036037
    },
    {
      "1": 7748,
      "roma": "SAN FERDINANDO",
      "rm": "RC",
      "1234": 418080097
    },
    {
      "1": 7749,
      "roma": "SAN FERMO DELLA BATTAGLIA",
      "rm": "CO",
      "1234": 403013206
    },
    {
      "1": 7750,
      "roma": "SAN GIACOMO DEGLI SCHIAVONI",
      "rm": "CB",
      "1234": 414070065
    },
    {
      "1": 7751,
      "roma": "SAN GIACOMO DELLE SEGNATE",
      "rm": "MN",
      "1234": 403020056
    },
    {
      "1": 7752,
      "roma": "SAN GIACOMO FILIPPO",
      "rm": "SO",
      "1234": 403014058
    },
    {
      "1": 7753,
      "roma": "SAN GIACOMO VERCELLESE",
      "rm": "VC",
      "1234": 401002035
    },
    {
      "1": 7754,
      "roma": "SAN GILLIO",
      "rm": "TO",
      "1234": 401001243
    },
    {
      "1": 7755,
      "roma": "SAN FLORO",
      "rm": "CZ",
      "1234": 418079108
    },
    {
      "1": 7756,
      "roma": "SAN FRANCESCO AL CAMPO",
      "rm": "TO",
      "1234": 401001240
    },
    {
      "1": 7757,
      "roma": "SAN FRATELLO",
      "rm": "ME",
      "1234": 419083078
    },
    {
      "1": 7758,
      "roma": "SAN GAVINO MONREALE",
      "rm": "CA",
      "1234": 420092055
    },
    {
      "1": 7759,
      "roma": "SAN GEMINI",
      "rm": "TR",
      "1234": 410055029
    },
    {
      "1": 7760,
      "roma": "SAN GENESIO ATESINO",
      "rm": "BZ",
      "1234": 404021079
    },
    {
      "1": 7761,
      "roma": "SAN GENESIO ED UNITI",
      "rm": "PV",
      "1234": 403018135
    },
    {
      "1": 7762,
      "roma": "SAN GENNARO VESUVIANO",
      "rm": "NA",
      "1234": 415063066
    },
    {
      "1": 7763,
      "roma": "SAN FELICE A CANCELLO",
      "rm": "CE",
      "1234": 415061075
    },
    {
      "1": 7764,
      "roma": "SAN FELICE CIRCEO",
      "rm": "LT",
      "1234": 412059025
    },
    {
      "1": 7765,
      "roma": "SAN FELICE DEL BENACO",
      "rm": "BS",
      "1234": 403017171
    },
    {
      "1": 7766,
      "roma": "SAN FELICE DEL MOLISE",
      "rm": "CB",
      "1234": 414070064
    },
    {
      "1": 7767,
      "roma": "SAN GERVASIO BRESCIANO",
      "rm": "BS",
      "1234": 403017172
    },
    {
      "1": 7768,
      "roma": "SAN FERDINANDO DI PUGLIA",
      "rm": "BT",
      "1234": 416110007
    },
    {
      "1": 7769,
      "roma": "SAN GIORGIO LA MOLARA",
      "rm": "BN",
      "1234": 415062059
    },
    {
      "1": 7770,
      "roma": "SAN GIORGIO LUCANO",
      "rm": "MT",
      "1234": 417077025
    },
    {
      "1": 7771,
      "roma": "SAN GIORGIO MONFERRATO",
      "rm": "AL",
      "1234": 401006153
    },
    {
      "1": 7772,
      "roma": "SAN GIORGIO MORGETO",
      "rm": "RC",
      "1234": 418080071
    },
    {
      "1": 7773,
      "roma": "SAN GIORGIO PIACENTINO",
      "rm": "PC",
      "1234": 408033040
    },
    {
      "1": 7774,
      "roma": "SAN GIMIGNANO",
      "rm": "SI",
      "1234": 409052028
    },
    {
      "1": 7775,
      "roma": "SAN GINESIO",
      "rm": "MC",
      "1234": 411043046
    },
    {
      "1": 7776,
      "roma": "SAN GIORGIO A CREMANO",
      "rm": "NA",
      "1234": 415063067
    },
    {
      "1": 7777,
      "roma": "SAN GIORGIO A LIRI",
      "rm": "FR",
      "1234": 412060063
    },
    {
      "1": 7778,
      "roma": "SAN GIORGIO ALBANESE",
      "rm": "CS",
      "1234": 418078118
    },
    {
      "1": 7779,
      "roma": "SAN GIORGIO CANAVESE",
      "rm": "TO",
      "1234": 401001244
    },
    {
      "1": 7780,
      "roma": "SAN GIORGIO DEL SANNIO",
      "rm": "BN",
      "1234": 415062058
    },
    {
      "1": 7781,
      "roma": "SAN GIORGIO DELLA RICHINVELDA",
      "rm": "PN",
      "1234": 406093038
    },
    {
      "1": 7782,
      "roma": "SAN GIORGIO DELLE PERTICHE",
      "rm": "PD",
      "1234": 405028075
    },
    {
      "1": 7783,
      "roma": "SAN GERMANO CHISONE",
      "rm": "TO",
      "1234": 401001242
    },
    {
      "1": 7784,
      "roma": "SAN GERMANO VERCELLESE",
      "rm": "VC",
      "1234": 401002131
    },
    {
      "1": 7785,
      "roma": "SAN GIORGIO DI NOGARO",
      "rm": "UD",
      "1234": 406030100
    },
    {
      "1": 7786,
      "roma": "SAN GIORGIO DI PIANO",
      "rm": "BO",
      "1234": 408037052
    },
    {
      "1": 7787,
      "roma": "SAN GIORGIO IN BOSCO",
      "rm": "PD",
      "1234": 405028076
    },
    {
      "1": 7788,
      "roma": "SAN GIORGIO IONICO",
      "rm": "TA",
      "1234": 416073024
    },
    {
      "1": 7789,
      "roma": "SEREGNO",
      "rm": "MB",
      "1234": 403108039
    },
    {
      "1": 7790,
      "roma": "SAN GIOVANNI IN PERSICETO",
      "rm": "BO",
      "1234": 408037053
    },
    {
      "1": 7791,
      "roma": "SAN GIOVANNI INCARICO",
      "rm": "FR",
      "1234": 412060064
    },
    {
      "1": 7792,
      "roma": "SAN GIOVANNI LA PUNTA",
      "rm": "CT",
      "1234": 419087041
    },
    {
      "1": 7793,
      "roma": "SAN GIORGIO SCARAMPI",
      "rm": "AT",
      "1234": 401005098
    },
    {
      "1": 7794,
      "roma": "SAN GIORGIO SU LEGNANO",
      "rm": "MI",
      "1234": 403015194
    },
    {
      "1": 7795,
      "roma": "SAN GIORIO DI SUSA",
      "rm": "TO",
      "1234": 401001245
    },
    {
      "1": 7796,
      "roma": "SAN GIOVANNI A PIRO",
      "rm": "SA",
      "1234": 415065119
    },
    {
      "1": 7797,
      "roma": "SAN GIOVANNI AL NATISONE",
      "rm": "UD",
      "1234": 406030101
    },
    {
      "1": 7798,
      "roma": "SAN GIOVANNI BIANCO",
      "rm": "BG",
      "1234": 403016188
    },
    {
      "1": 7799,
      "roma": "SAN GIOVANNI DEL DOSSO",
      "rm": "MN",
      "1234": 403020058
    },
    {
      "1": 7800,
      "roma": "SAN GIORGIO DI LOMELLINA",
      "rm": "PV",
      "1234": 403018136
    },
    {
      "1": 7801,
      "roma": "SAN GIOVANNI GEMINI",
      "rm": "AG",
      "1234": 419084036
    },
    {
      "1": 7802,
      "roma": "SAN GIOVANNI ILARIONE",
      "rm": "VR",
      "1234": 405023070
    },
    {
      "1": 7803,
      "roma": "SAN GIOVANNI IN CROCE",
      "rm": "CR",
      "1234": 403019090
    },
    {
      "1": 7804,
      "roma": "SAN GIOVANNI IN FIORE",
      "rm": "CS",
      "1234": 418078119
    },
    {
      "1": 7805,
      "roma": "SAN GIOVANNI IN GALDO",
      "rm": "CB",
      "1234": 414070066
    },
    {
      "1": 7806,
      "roma": "SAN GIOVANNI IN MARIGNANO",
      "rm": "RN",
      "1234": 408099017
    },
    {
      "1": 7807,
      "roma": "SAN GREGORIO DA SASSOLA",
      "rm": "RM",
      "1234": 412058095
    },
    {
      "1": 7808,
      "roma": "SAN GREGORIO DI CATANIA",
      "rm": "CT",
      "1234": 419087042
    },
    {
      "1": 7809,
      "roma": "SAN GREGORIO MAGNO",
      "rm": "SA",
      "1234": 415065120
    },
    {
      "1": 7810,
      "roma": "SAN LORENZO DORSINO",
      "rm": "TN",
      "1234": 404022231
    },
    {
      "1": 7811,
      "roma": "SAN GIOVANNI LIPIONI",
      "rm": "CH",
      "1234": 413069080
    },
    {
      "1": 7812,
      "roma": "SAN GIOVANNI LUPATOTO",
      "rm": "VR",
      "1234": 405023071
    },
    {
      "1": 7813,
      "roma": "SAN GIOVANNI ROTONDO",
      "rm": "FG",
      "1234": 416071046
    },
    {
      "1": 7814,
      "roma": "SAN GIOVANNI SUERGIU",
      "rm": "CA",
      "1234": 420092056
    },
    {
      "1": 7815,
      "roma": "SAN GIOVANNI TEATINO",
      "rm": "CH",
      "1234": 413069081
    },
    {
      "1": 7816,
      "roma": "SAN GIOVANNI VALDARNO",
      "rm": "AR",
      "1234": 409051033
    },
    {
      "1": 7817,
      "roma": "SAN GIULIANO DEL SANNIO",
      "rm": "CB",
      "1234": 414070067
    },
    {
      "1": 7818,
      "roma": "SAN GIULIANO DI PUGLIA",
      "rm": "CB",
      "1234": 414070068
    },
    {
      "1": 7819,
      "roma": "SAN GIULIANO MILANESE",
      "rm": "MI",
      "1234": 403015195
    },
    {
      "1": 7820,
      "roma": "SAN GIULIANO TERME",
      "rm": "PI",
      "1234": 409050031
    },
    {
      "1": 7821,
      "roma": "SAN GIOVANNI DI GERACE",
      "rm": "RC",
      "1234": 418080072
    },
    {
      "1": 7822,
      "roma": "SAN GIUSEPPE IATO",
      "rm": "PA",
      "1234": 419082064
    },
    {
      "1": 7823,
      "roma": "SAN GIUSEPPE VESUVIANO",
      "rm": "NA",
      "1234": 415063068
    },
    {
      "1": 7824,
      "roma": "SAN GIUSTINO",
      "rm": "PG",
      "1234": 410054044
    },
    {
      "1": 7825,
      "roma": "SAN GIUSTO CANAVESE",
      "rm": "TO",
      "1234": 401001246
    },
    {
      "1": 7826,
      "roma": "SAN GODENZO",
      "rm": "FI",
      "1234": 409048039
    },
    {
      "1": 7827,
      "roma": "SAN GREGORIO D'IPPONA",
      "rm": "VV",
      "1234": 418102034
    },
    {
      "1": 7828,
      "roma": "SAN LORENZO ISONTINO",
      "rm": "GO",
      "1234": 406031020
    },
    {
      "1": 7829,
      "roma": "SAN LORENZO MAGGIORE",
      "rm": "BN",
      "1234": 415062062
    },
    {
      "1": 7830,
      "roma": "SAN GREGORIO MATESE",
      "rm": "CE",
      "1234": 415061076
    },
    {
      "1": 7831,
      "roma": "SAN GREGORIO NELLE ALPI",
      "rm": "BL",
      "1234": 405025045
    },
    {
      "1": 7832,
      "roma": "SAN LAZZARO DI SAVENA",
      "rm": "BO",
      "1234": 408037054
    },
    {
      "1": 7833,
      "roma": "SAN LEONARDO",
      "rm": "UD",
      "1234": 406030102
    },
    {
      "1": 7834,
      "roma": "SAN LEONARDO IN PASSIRIA",
      "rm": "BZ",
      "1234": 404021080
    },
    {
      "1": 7835,
      "roma": "SAN LEUCIO DEL SANNIO",
      "rm": "BN",
      "1234": 415062060
    },
    {
      "1": 7836,
      "roma": "SAN LORENZELLO",
      "rm": "BN",
      "1234": 415062061
    },
    {
      "1": 7837,
      "roma": "SAN LORENZO",
      "rm": "RC",
      "1234": 418080073
    },
    {
      "1": 7838,
      "roma": "SAN LORENZO BELLIZZI",
      "rm": "CS",
      "1234": 418078120
    },
    {
      "1": 7839,
      "roma": "SAN LORENZO DEL VALLO",
      "rm": "CS",
      "1234": 418078121
    },
    {
      "1": 7840,
      "roma": "SAN LORENZO DI SEBATO",
      "rm": "BZ",
      "1234": 404021081
    },
    {
      "1": 7841,
      "roma": "SAN LORENZO IN CAMPO",
      "rm": "PS",
      "1234": 411041054
    },
    {
      "1": 7842,
      "roma": "SAN MARTINO AL TAGLIAMENTO",
      "rm": "PN",
      "1234": 406093039
    },
    {
      "1": 7843,
      "roma": "SAN MARTINO ALFIERI",
      "rm": "AT",
      "1234": 401005099
    },
    {
      "1": 7844,
      "roma": "SAN LORENZO NUOVO",
      "rm": "VT",
      "1234": 412056047
    },
    {
      "1": 7845,
      "roma": "SAN LEO",
      "rm": "RN",
      "1234": 408099025
    },
    {
      "1": 7846,
      "roma": "SAN LUCA",
      "rm": "RC",
      "1234": 418080074
    },
    {
      "1": 7847,
      "roma": "SAN LUCIDO",
      "rm": "CS",
      "1234": 418078122
    },
    {
      "1": 7848,
      "roma": "SAN LUPO",
      "rm": "BN",
      "1234": 415062063
    },
    {
      "1": 7849,
      "roma": "SAN MANGO D'AQUINO",
      "rm": "CZ",
      "1234": 418079110
    },
    {
      "1": 7850,
      "roma": "SAN MANGO PIEMONTE",
      "rm": "SA",
      "1234": 415065121
    },
    {
      "1": 7851,
      "roma": "SAN MANGO SUL CALORE",
      "rm": "AV",
      "1234": 415064082
    },
    {
      "1": 7852,
      "roma": "SAN MARCELLINO",
      "rm": "CE",
      "1234": 415061077
    },
    {
      "1": 7853,
      "roma": "SAN MARCELLO",
      "rm": "AN",
      "1234": 411042041
    },
    {
      "1": 7854,
      "roma": "SAN MARCO ARGENTANO",
      "rm": "CS",
      "1234": 418078123
    },
    {
      "1": 7855,
      "roma": "SAN LORENZO AL MARE",
      "rm": "IM",
      "1234": 407008054
    },
    {
      "1": 7856,
      "roma": "SAN MARCO D'ALUNZIO",
      "rm": "ME",
      "1234": 419083079
    },
    {
      "1": 7857,
      "roma": "SAN MARCO DEI CAVOTI",
      "rm": "BN",
      "1234": 415062064
    },
    {
      "1": 7858,
      "roma": "SAN MARCO EVANGELISTA",
      "rm": "CE",
      "1234": 415061104
    },
    {
      "1": 7859,
      "roma": "SAN MARCO IN LAMIS",
      "rm": "FG",
      "1234": 416071047
    },
    {
      "1": 7860,
      "roma": "SAN MARCO LA CATOLA",
      "rm": "FG",
      "1234": 416071048
    },
    {
      "1": 7861,
      "roma": "SAN MARTINO SICCOMARIO",
      "rm": "PV",
      "1234": 403018137
    },
    {
      "1": 7862,
      "roma": "SAN MARTINO BUON ALBERGO",
      "rm": "VR",
      "1234": 405023073
    },
    {
      "1": 7863,
      "roma": "SAN MARTINO CANAVESE",
      "rm": "TO",
      "1234": 401001247
    },
    {
      "1": 7864,
      "roma": "SAN MARTINO D'AGRI",
      "rm": "PZ",
      "1234": 417076077
    },
    {
      "1": 8804,
      "roma": "VALDERICE",
      "rm": "TP",
      "1234": 419081022
    },
    {
      "1": 8805,
      "roma": "VALDIDENTRO",
      "rm": "SO",
      "1234": 403014071
    },
    {
      "1": 8806,
      "roma": "VALDIERI",
      "rm": "CN",
      "1234": 401004233
    },
    {
      "1": 8807,
      "roma": "VALDINA",
      "rm": "ME",
      "1234": 419083103
    },
    {
      "1": 8808,
      "roma": "VALDISOTTO",
      "rm": "SO",
      "1234": 403014072
    },
    {
      "1": 8809,
      "roma": "VALDOBBIADENE",
      "rm": "TV",
      "1234": 405026087
    },
    {
      "1": 8810,
      "roma": "VALDUGGIA",
      "rm": "VC",
      "1234": 401002152
    },
    {
      "1": 8811,
      "roma": "VALEGGIO",
      "rm": "PV",
      "1234": 403018167
    },
    {
      "1": 8812,
      "roma": "VALEGGIO SUL MINCIO",
      "rm": "VR",
      "1234": 405023089
    },
    {
      "1": 8813,
      "roma": "VALENTANO",
      "rm": "VT",
      "1234": 412056053
    },
    {
      "1": 8814,
      "roma": "VALENZA",
      "rm": "AL",
      "1234": 401006177
    },
    {
      "1": 8815,
      "roma": "VALENZANO",
      "rm": "BA",
      "1234": 416072048
    },
    {
      "1": 8816,
      "roma": "VALERA FRATTA",
      "rm": "LO",
      "1234": 403098059
    },
    {
      "1": 8817,
      "roma": "VALDAONE",
      "rm": "TN",
      "1234": 404022232
    },
    {
      "1": 8818,
      "roma": "VALFABBRICA",
      "rm": "PG",
      "1234": 410054057
    },
    {
      "1": 8819,
      "roma": "VALFLORIANA",
      "rm": "TN",
      "1234": 404022209
    },
    {
      "1": 8820,
      "roma": "VALFURVA",
      "rm": "SO",
      "1234": 403014073
    },
    {
      "1": 8821,
      "roma": "VALGANNA",
      "rm": "VA",
      "1234": 403012131
    },
    {
      "1": 8822,
      "roma": "VALGIOIE",
      "rm": "TO",
      "1234": 401001285
    },
    {
      "1": 8823,
      "roma": "VALGOGLIO",
      "rm": "BG",
      "1234": 403016225
    },
    {
      "1": 8824,
      "roma": "VALGRANA",
      "rm": "CN",
      "1234": 401004234
    },
    {
      "1": 8825,
      "roma": "VALGREGHENTINO",
      "rm": "LC",
      "1234": 403097082
    },
    {
      "1": 8826,
      "roma": "VALGRISENCHE",
      "rm": "AO",
      "1234": 402007068
    },
    {
      "1": 8827,
      "roma": "VALGUARNERA CAROPEPE",
      "rm": "EN",
      "1234": 419086019
    },
    {
      "1": 8828,
      "roma": "VALLADA AGORDINA",
      "rm": "BL",
      "1234": 405025062
    },
    {
      "1": 8829,
      "roma": "VALLANZENGO",
      "rm": "BI",
      "1234": 401096072
    },
    {
      "1": 8830,
      "roma": "VALLECROSIA",
      "rm": "IM",
      "1234": 407008063
    },
    {
      "1": 8831,
      "roma": "VALLARSA",
      "rm": "TN",
      "1234": 404022210
    },
    {
      "1": 8832,
      "roma": "VALLATA",
      "rm": "AV",
      "1234": 415064114
    },
    {
      "1": 8833,
      "roma": "VALLE AGRICOLA",
      "rm": "CE",
      "1234": 415061096
    },
    {
      "1": 8834,
      "roma": "VALLE AURINA",
      "rm": "BZ",
      "1234": 404021108
    },
    {
      "1": 8835,
      "roma": "VALLE CASTELLANA",
      "rm": "TE",
      "1234": 413067046
    },
    {
      "1": 8836,
      "roma": "VALLE DELL'ANGELO",
      "rm": "SA",
      "1234": 415065153
    },
    {
      "1": 8837,
      "roma": "VILLE D'ANAUNIA",
      "rm": "TN",
      "1234": 404022249
    },
    {
      "1": 8838,
      "roma": "VALLE DI CASIES",
      "rm": "BZ",
      "1234": 404021109
    },
    {
      "1": 8839,
      "roma": "VALLE DI MADDALONI",
      "rm": "CE",
      "1234": 415061097
    },
    {
      "1": 8840,
      "roma": "VALLE LOMELLINA",
      "rm": "PV",
      "1234": 403018168
    },
    {
      "1": 8841,
      "roma": "VALLE SALIMBENE",
      "rm": "PV",
      "1234": 403018169
    },
    {
      "1": 8842,
      "roma": "VALLE SAN NICOLAO",
      "rm": "BI",
      "1234": 401096074
    },
    {
      "1": 8843,
      "roma": "VALLEBONA",
      "rm": "IM",
      "1234": 407008062
    },
    {
      "1": 8844,
      "roma": "VALLECORSA",
      "rm": "FR",
      "1234": 412060082
    },
    {
      "1": 8845,
      "roma": "VALLO TORINESE",
      "rm": "TO",
      "1234": 401001286
    },
    {
      "1": 8846,
      "roma": "VALLEDOLMO",
      "rm": "PA",
      "1234": 419082076
    },
    {
      "1": 8847,
      "roma": "VALLEDORIA",
      "rm": "SS",
      "1234": 420090079
    },
    {
      "1": 8848,
      "roma": "VALLEFIORITA",
      "rm": "CZ",
      "1234": 418079151
    },
    {
      "1": 8849,
      "roma": "VALLELONGA",
      "rm": "VV",
      "1234": 418102045
    },
    {
      "1": 8850,
      "roma": "VALLELUNGA PRATAMENO",
      "rm": "CL",
      "1234": 419085021
    },
    {
      "1": 8851,
      "roma": "VALLEMAIO",
      "rm": "FR",
      "1234": 412060083
    },
    {
      "1": 8852,
      "roma": "VALLE DI CADORE",
      "rm": "BL",
      "1234": 405025063
    },
    {
      "1": 8853,
      "roma": "VALLEFOGLIA",
      "rm": "PS",
      "1234": 411041068
    },
    {
      "1": 8854,
      "roma": "VALLIO TERME",
      "rm": "BS",
      "1234": 403017999
    },
    {
      "1": 8855,
      "roma": "VALLEPIETRA",
      "rm": "RM",
      "1234": 412058108
    },
    {
      "1": 8856,
      "roma": "VALLERANO",
      "rm": "VT",
      "1234": 412056054
    },
    {
      "1": 8857,
      "roma": "VALLERMOSA",
      "rm": "CA",
      "1234": 420092091
    },
    {
      "1": 8858,
      "roma": "VALLEROTONDA",
      "rm": "FR",
      "1234": 412060084
    },
    {
      "1": 8859,
      "roma": "VALLESACCARDA",
      "rm": "AV",
      "1234": 415064115
    },
    {
      "1": 8860,
      "roma": "VALLEVE",
      "rm": "BG",
      "1234": 403016226
    },
    {
      "1": 8861,
      "roma": "VALLI DEL PASUBIO",
      "rm": "VI",
      "1234": 405024113
    },
    {
      "1": 8862,
      "roma": "VALLINFREDA",
      "rm": "RM",
      "1234": 412058109
    },
    {
      "1": 8039,
      "roma": "SANT'AGATA DI PUGLIA",
      "rm": "FG",
      "1234": 416071052
    },
    {
      "1": 8863,
      "roma": "VALLO DELLA LUCANIA",
      "rm": "SA",
      "1234": 415065154
    },
    {
      "1": 8864,
      "roma": "VALLO DI NERA",
      "rm": "PG",
      "1234": 410054058
    },
    {
      "1": 8865,
      "roma": "VALSTRONA",
      "rm": "VB",
      "1234": 401103069
    },
    {
      "1": 8866,
      "roma": "VALLORIATE",
      "rm": "CN",
      "1234": 401004235
    },
    {
      "1": 8867,
      "roma": "VALMACCA",
      "rm": "AL",
      "1234": 401006178
    },
    {
      "1": 8868,
      "roma": "VALMADRERA",
      "rm": "LC",
      "1234": 403097083
    },
    {
      "1": 8869,
      "roma": "VALMONTONE",
      "rm": "RM",
      "1234": 412058110
    },
    {
      "1": 8870,
      "roma": "MONTALTO CARPASIO",
      "rm": "IM",
      "1234": 407008068
    },
    {
      "1": 8871,
      "roma": "CENTRO VALLE INTELVI",
      "rm": "CO",
      "1234": 403013254
    },
    {
      "1": 8872,
      "roma": "VALVARRONE",
      "rm": "LC",
      "1234": 403097093
    },
    {
      "1": 8873,
      "roma": "CASTELGERUNDO",
      "rm": "LO",
      "1234": 403098062
    },
    {
      "1": 8874,
      "roma": "BORGO MANTOVANO",
      "rm": "MN",
      "1234": 403020072
    },
    {
      "1": 8875,
      "roma": "ALLUVIONI PIOVERA",
      "rm": "AL",
      "1234": 401006192
    },
    {
      "1": 8876,
      "roma": "ALTO SERMENZA",
      "rm": "VC",
      "1234": 401002170
    },
    {
      "1": 8877,
      "roma": "CELLIO CON BREIA",
      "rm": "VC",
      "1234": 401002171
    },
    {
      "1": 8878,
      "roma": "LATERINA PERGINE VALDARNO",
      "rm": "AR",
      "1234": 409051042
    },
    {
      "1": 8879,
      "roma": "RIO",
      "rm": "LI",
      "1234": 409049021
    },
    {
      "1": 8880,
      "roma": "CASSANO SPINOLA",
      "rm": "AL",
      "1234": 401006191
    },
    {
      "1": 8881,
      "roma": "SERMIDE E FELONICA",
      "rm": "MN",
      "1234": 403020961
    },
    {
      "1": 8882,
      "roma": "SAPPADA",
      "rm": "UD",
      "1234": 405030189
    },
    {
      "1": 8883,
      "roma": "FIUMICELLO VILLA VICENTINA",
      "rm": "UD",
      "1234": 406030190
    },
    {
      "1": 8884,
      "roma": "TREPPO LIGOSULLO",
      "rm": "UD",
      "1234": 406030191
    },
    {
      "1": 8885,
      "roma": "SORAGA DI FASSA",
      "rm": "TN",
      "1234": 404022976
    },
    {
      "1": 8886,
      "roma": "BARBARANO MOSSANO",
      "rm": "VI",
      "1234": 405024124
    },
    {
      "1": 8887,
      "roma": "BORGO VENETO",
      "rm": "PD",
      "1234": 405028107
    },
    {
      "1": 8888,
      "roma": "CORIGLIANO-ROSSANO",
      "rm": "CS",
      "1234": 418078157
    },
    {
      "1": 8889,
      "roma": "TORRE DE' BUSI",
      "rm": "BG",
      "1234": 403016215
    },
    {
      "1": 8890,
      "roma": "SAN GIOVANNI DI FASSA-SEN JAN",
      "rm": "TN",
      "1234": 404022950
    },
    {
      "1": 8891,
      "roma": "TRESIGNANA",
      "rm": "FE",
      "1234": 408038030
    },
    {
      "1": 8892,
      "roma": "RIVA DEL PO",
      "rm": "FE",
      "1234": 408038029
    },
    {
      "1": 8893,
      "roma": "SORBOLO MEZZANI",
      "rm": "PR",
      "1234": 408034051
    },
    {
      "1": 8894,
      "roma": "SOLBIATE CON CAGNO",
      "rm": "CO",
      "1234": 403013255
    },
    {
      "1": 8895,
      "roma": "BORGOCARBONARA",
      "rm": "MN",
      "1234": 403020073
    },
    {
      "1": 8896,
      "roma": "SASSOCORVARO AUDITORE",
      "rm": "PS",
      "1234": 411041071
    },
    {
      "1": 8897,
      "roma": "VAL DI CHY",
      "rm": "TO",
      "1234": 401001317
    },
    {
      "1": 8898,
      "roma": "VALLE CANNOBINA",
      "rm": "VB",
      "1234": 401103079
    },
    {
      "1": 8899,
      "roma": "BARBERINO TAVARNELLE",
      "rm": "FI",
      "1234": 409048054
    },
    {
      "1": 8900,
      "roma": "TERRE D'ADIGE",
      "rm": "TN",
      "1234": 404022251
    },
    {
      "1": 8901,
      "roma": "PIADENA DRIZZONA",
      "rm": "CR",
      "1234": 403019116
    },
    {
      "1": 8902,
      "roma": "QUAREGNA CERRETO",
      "rm": "BI",
      "1234": 401096087
    },
    {
      "1": 8903,
      "roma": "VALDILANA",
      "rm": "BI",
      "1234": 401096088
    },
    {
      "1": 8904,
      "roma": "GATTICO-VERUNO",
      "rm": "NO",
      "1234": 401003166
    },
    {
      "1": 8905,
      "roma": "VALCHIUSA",
      "rm": "TO",
      "1234": 401001318
    },
    {
      "1": 8906,
      "roma": "COLLI VERDI",
      "rm": "PV",
      "1234": 403018193
    },
    {
      "1": 8907,
      "roma": "SAN GIORGIO BIGARELLO",
      "rm": "MN",
      "1234": 403020957
    },
    {
      "1": 8908,
      "roma": "BORGO VALBELLUNA",
      "rm": "BL",
      "1234": 405025074
    },
    {
      "1": 8909,
      "roma": "PIEVE DEL GRAPPA",
      "rm": "TV",
      "1234": 405026096
    },
    {
      "1": 8910,
      "roma": "VALBRENTA",
      "rm": "VI",
      "1234": 405024125
    },
    {
      "1": 8911,
      "roma": "LU E CUCCARO MONFERRATO",
      "rm": "AL",
      "1234": 401006193
    },
    {
      "1": 8912,
      "roma": "VERMEZZO CON ZELO",
      "rm": "MI",
      "1234": 403015251
    },
    {
      "1": 8913,
      "roma": "CADREZZATE CON OSMATE",
      "rm": "VA",
      "1234": 403012143
    },
    {
      "1": 8914,
      "roma": "COLCERESA",
      "rm": "VI",
      "1234": 405024126
    },
    {
      "1": 8915,
      "roma": "LUSIANA CONCO",
      "rm": "VI",
      "1234": 405024127
    },
    {
      "1": 8916,
      "roma": "NEGRAR DI VALPOLICELLA",
      "rm": "VR",
      "1234": 405023952
    },
    {
      "1": 8219,
      "roma": "SOLOFRA",
      "rm": "AV",
      "1234": 415064101
    },
    {
      "1": 8220,
      "roma": "SOLONGHELLO",
      "rm": "AL",
      "1234": 401006164
    },
    {
      "1": 8221,
      "roma": "SOLOPACA",
      "rm": "BN",
      "1234": 415062073
    },
    {
      "1": 8222,
      "roma": "SONICO",
      "rm": "BS",
      "1234": 403017181
    },
    {
      "1": 8223,
      "roma": "SONNINO",
      "rm": "LT",
      "1234": 412059029
    },
    {
      "1": 8224,
      "roma": "SORICO",
      "rm": "CO",
      "1234": 403013216
    },
    {
      "1": 8225,
      "roma": "SORISO",
      "rm": "NO",
      "1234": 401003140
    },
    {
      "1": 8226,
      "roma": "SORISOLE",
      "rm": "BG",
      "1234": 403016202
    },
    {
      "1": 8227,
      "roma": "SORMANO",
      "rm": "CO",
      "1234": 403013217
    },
    {
      "1": 8228,
      "roma": "SORRADILE",
      "rm": "OR",
      "1234": 420095063
    },
    {
      "1": 8229,
      "roma": "SORRENTO",
      "rm": "NA",
      "1234": 415063080
    },
    {
      "1": 8230,
      "roma": "SORSO",
      "rm": "SS",
      "1234": 420090069
    },
    {
      "1": 8231,
      "roma": "SORTINO",
      "rm": "SR",
      "1234": 419089019
    },
    {
      "1": 8232,
      "roma": "SORBO SERPICO",
      "rm": "AV",
      "1234": 415064102
    },
    {
      "1": 8233,
      "roma": "SOMMARIVA PERNO",
      "rm": "CN",
      "1234": 401004223
    },
    {
      "1": 8234,
      "roma": "SOMMATINO",
      "rm": "CL",
      "1234": 419085019
    },
    {
      "1": 8235,
      "roma": "SOMMO",
      "rm": "PV",
      "1234": 403018151
    },
    {
      "1": 8236,
      "roma": "SONA",
      "rm": "VR",
      "1234": 405023083
    },
    {
      "1": 8237,
      "roma": "SONCINO",
      "rm": "CR",
      "1234": 403019097
    },
    {
      "1": 8238,
      "roma": "SONDALO",
      "rm": "SO",
      "1234": 403014060
    },
    {
      "1": 8239,
      "roma": "SONDRIO",
      "rm": "SO",
      "1234": 403014061
    },
    {
      "1": 8240,
      "roma": "SONGAVAZZO",
      "rm": "BG",
      "1234": 403016201
    },
    {
      "1": 8241,
      "roma": "SORIANO CALABRO",
      "rm": "VV",
      "1234": 418102040
    },
    {
      "1": 8242,
      "roma": "SORIANO NEL CIMINO",
      "rm": "VT",
      "1234": 412056048
    },
    {
      "1": 8243,
      "roma": "SOVICO",
      "rm": "MB",
      "1234": 403108041
    },
    {
      "1": 8244,
      "roma": "SOVIZZO",
      "rm": "VI",
      "1234": 405024103
    },
    {
      "1": 8245,
      "roma": "SOVRAMONTE",
      "rm": "BL",
      "1234": 405025058
    },
    {
      "1": 8246,
      "roma": "SOZZAGO",
      "rm": "NO",
      "1234": 401003141
    },
    {
      "1": 8247,
      "roma": "SPADAFORA",
      "rm": "ME",
      "1234": 419083096
    },
    {
      "1": 8248,
      "roma": "SPADOLA",
      "rm": "VV",
      "1234": 418102041
    },
    {
      "1": 8249,
      "roma": "SOSPIRO",
      "rm": "CR",
      "1234": 403019099
    },
    {
      "1": 8250,
      "roma": "SOSPIROLO",
      "rm": "BL",
      "1234": 405025056
    },
    {
      "1": 8251,
      "roma": "SOSSANO",
      "rm": "VI",
      "1234": 405024102
    },
    {
      "1": 8252,
      "roma": "SORDEVOLO",
      "rm": "BI",
      "1234": 401096063
    },
    {
      "1": 8253,
      "roma": "SORDIO",
      "rm": "LO",
      "1234": 403098055
    },
    {
      "1": 8254,
      "roma": "SORESINA",
      "rm": "CR",
      "1234": 403019098
    },
    {
      "1": 8255,
      "roma": "SORGA'",
      "rm": "VR",
      "1234": 405023084
    },
    {
      "1": 8256,
      "roma": "SORGONO",
      "rm": "NU",
      "1234": 420091086
    },
    {
      "1": 8257,
      "roma": "SORI",
      "rm": "GE",
      "1234": 407010060
    },
    {
      "1": 8258,
      "roma": "SORIANELLO",
      "rm": "VV",
      "1234": 418102039
    },
    {
      "1": 8259,
      "roma": "SOVERZENE",
      "rm": "BL",
      "1234": 405025057
    },
    {
      "1": 8260,
      "roma": "SOVICILLE",
      "rm": "SI",
      "1234": 409052034
    },
    {
      "1": 8261,
      "roma": "SPIGNO SATURNIA",
      "rm": "LT",
      "1234": 412059031
    },
    {
      "1": 8262,
      "roma": "SPILAMBERTO",
      "rm": "MO",
      "1234": 408036045
    },
    {
      "1": 8263,
      "roma": "SPILIMBERGO",
      "rm": "PN",
      "1234": 406093044
    },
    {
      "1": 8264,
      "roma": "SPILINGA",
      "rm": "VV",
      "1234": 418102042
    },
    {
      "1": 8265,
      "roma": "SPINADESCO",
      "rm": "CR",
      "1234": 403019100
    },
    {
      "1": 8266,
      "roma": "SPINEA",
      "rm": "VE",
      "1234": 405027038
    },
    {
      "1": 8267,
      "roma": "SPINEDA",
      "rm": "CR",
      "1234": 403019101
    },
    {
      "1": 8268,
      "roma": "SPARANISE",
      "rm": "CE",
      "1234": 415061089
    },
    {
      "1": 8269,
      "roma": "SPARONE",
      "rm": "TO",
      "1234": 401001267
    },
    {
      "1": 8270,
      "roma": "SPECCHIA",
      "rm": "LE",
      "1234": 416075077
    },
    {
      "1": 8271,
      "roma": "SPELLO",
      "rm": "PG",
      "1234": 410054050
    },
    {
      "1": 8272,
      "roma": "SOSTEGNO",
      "rm": "BI",
      "1234": 401096064
    },
    {
      "1": 8273,
      "roma": "SOTTO IL MONTE GIOVANNI XXIII",
      "rm": "BG",
      "1234": 403016203
    },
    {
      "1": 8274,
      "roma": "SOVER",
      "rm": "TN",
      "1234": 404022177
    },
    {
      "1": 8275,
      "roma": "SOVERATO",
      "rm": "CZ",
      "1234": 418079137
    },
    {
      "1": 8276,
      "roma": "SOVERE",
      "rm": "BG",
      "1234": 403016204
    },
    {
      "1": 8277,
      "roma": "SOVERIA MANNELLI",
      "rm": "CZ",
      "1234": 418079138
    },
    {
      "1": 8278,
      "roma": "SOVERIA SIMERI",
      "rm": "CZ",
      "1234": 418079139
    },
    {
      "1": 8279,
      "roma": "SPIAZZO",
      "rm": "TN",
      "1234": 404022179
    },
    {
      "1": 8280,
      "roma": "SPIGNO MONFERRATO",
      "rm": "AL",
      "1234": 401006165
    },
    {
      "1": 8281,
      "roma": "SPORMAGGIORE",
      "rm": "TN",
      "1234": 404022180
    },
    {
      "1": 8282,
      "roma": "SPORMINORE",
      "rm": "TN",
      "1234": 404022181
    },
    {
      "1": 8283,
      "roma": "SPOTORNO",
      "rm": "SV",
      "1234": 407009057
    },
    {
      "1": 8284,
      "roma": "SPRESIANO",
      "rm": "TV",
      "1234": 405026082
    },
    {
      "1": 8285,
      "roma": "SPINAZZOLA",
      "rm": "BT",
      "1234": 416110008
    },
    {
      "1": 8286,
      "roma": "SPRIANA",
      "rm": "SO",
      "1234": 403014062
    },
    {
      "1": 8287,
      "roma": "SQUILLACE",
      "rm": "CZ",
      "1234": 418079142
    },
    {
      "1": 8288,
      "roma": "SQUINZANO",
      "rm": "LE",
      "1234": 416075079
    },
    {
      "1": 8289,
      "roma": "SPINETE",
      "rm": "CB",
      "1234": 414070076
    },
    {
      "1": 8290,
      "roma": "SPINETO SCRIVIA",
      "rm": "AL",
      "1234": 401006166
    },
    {
      "1": 8291,
      "roma": "SPINETOLI",
      "rm": "AP",
      "1234": 411044071
    },
    {
      "1": 8292,
      "roma": "SPERLINGA",
      "rm": "EN",
      "1234": 419086017
    },
    {
      "1": 8293,
      "roma": "SPERLONGA",
      "rm": "LT",
      "1234": 412059030
    },
    {
      "1": 8294,
      "roma": "SPERONE",
      "rm": "AV",
      "1234": 415064103
    },
    {
      "1": 8295,
      "roma": "SPESSA",
      "rm": "PV",
      "1234": 403018152
    },
    {
      "1": 8296,
      "roma": "SPEZZANO ALBANESE",
      "rm": "CS",
      "1234": 418078142
    },
    {
      "1": 8297,
      "roma": "SPEZZANO DELLA SILA",
      "rm": "CS",
      "1234": 418078143
    },
    {
      "1": 8298,
      "roma": "SPOLTORE",
      "rm": "PE",
      "1234": 413068041
    },
    {
      "1": 8299,
      "roma": "SPONGANO",
      "rm": "LE",
      "1234": 416075078
    },
    {
      "1": 8300,
      "roma": "STELLA",
      "rm": "SV",
      "1234": 407009058
    },
    {
      "1": 8301,
      "roma": "STELLA CILENTO",
      "rm": "SA",
      "1234": 415065144
    },
    {
      "1": 8302,
      "roma": "STELLANELLO",
      "rm": "SV",
      "1234": 407009059
    },
    {
      "1": 8303,
      "roma": "STELVIO",
      "rm": "BZ",
      "1234": 404021095
    },
    {
      "1": 8304,
      "roma": "STENICO",
      "rm": "TN",
      "1234": 404022182
    },
    {
      "1": 8305,
      "roma": "STERNATIA",
      "rm": "LE",
      "1234": 416075080
    },
    {
      "1": 8306,
      "roma": "STEZZANO",
      "rm": "BG",
      "1234": 403016207
    },
    {
      "1": 8307,
      "roma": "STIENTA",
      "rm": "RO",
      "1234": 405029045
    },
    {
      "1": 8308,
      "roma": "STAFFOLO",
      "rm": "AN",
      "1234": 411042049
    },
    {
      "1": 8309,
      "roma": "STAGNO LOMBARDO",
      "rm": "CR",
      "1234": 403019103
    },
    {
      "1": 8310,
      "roma": "STAITI",
      "rm": "RC",
      "1234": 418080090
    },
    {
      "1": 8311,
      "roma": "SPINO D'ADDA",
      "rm": "CR",
      "1234": 403019102
    },
    {
      "1": 8312,
      "roma": "SPINONE AL LAGO",
      "rm": "BG",
      "1234": 403016205
    },
    {
      "1": 8313,
      "roma": "SPINOSO",
      "rm": "PZ",
      "1234": 417076086
    },
    {
      "1": 8314,
      "roma": "SPIRANO",
      "rm": "BG",
      "1234": 403016206
    },
    {
      "1": 8315,
      "roma": "SPOLETO",
      "rm": "PG",
      "1234": 410054051
    },
    {
      "1": 8316,
      "roma": "STEFANACONI",
      "rm": "VV",
      "1234": 418102043
    },
    {
      "1": 8317,
      "roma": "STRAMBINELLO",
      "rm": "TO",
      "1234": 401001268
    },
    {
      "1": 8318,
      "roma": "STRAMBINO",
      "rm": "TO",
      "1234": 401001269
    },
    {
      "1": 8319,
      "roma": "STRANGOLAGALLI",
      "rm": "FR",
      "1234": 412060075
    },
    {
      "1": 8320,
      "roma": "STREGNA",
      "rm": "UD",
      "1234": 406030111
    },
    {
      "1": 8321,
      "roma": "STREMBO",
      "rm": "TN",
      "1234": 404022184
    },
    {
      "1": 8322,
      "roma": "STRESA",
      "rm": "VB",
      "1234": 401103064
    },
    {
      "1": 8323,
      "roma": "STIGLIANO",
      "rm": "MT",
      "1234": 417077027
    },
    {
      "1": 8324,
      "roma": "STIGNANO",
      "rm": "RC",
      "1234": 418080091
    },
    {
      "1": 8325,
      "roma": "STILO",
      "rm": "RC",
      "1234": 418080092
    },
    {
      "1": 8326,
      "roma": "STALETTI",
      "rm": "CZ",
      "1234": 418079143
    },
    {
      "1": 8327,
      "roma": "STANGHELLA",
      "rm": "PD",
      "1234": 405028088
    },
    {
      "1": 8328,
      "roma": "STARANZANO",
      "rm": "GO",
      "1234": 406031023
    },
    {
      "1": 8329,
      "roma": "STATTE",
      "rm": "TA",
      "1234": 416073029
    },
    {
      "1": 8330,
      "roma": "STAZZANO",
      "rm": "AL",
      "1234": 401006167
    },
    {
      "1": 8331,
      "roma": "STAZZEMA",
      "rm": "LU",
      "1234": 409046030
    },
    {
      "1": 8332,
      "roma": "STAZZONA",
      "rm": "CO",
      "1234": 403013218
    },
    {
      "1": 8333,
      "roma": "STRADELLA",
      "rm": "PV",
      "1234": 403018153
    },
    {
      "1": 8334,
      "roma": "STURNO",
      "rm": "AV",
      "1234": 415064104
    },
    {
      "1": 8335,
      "roma": "SUARDI",
      "rm": "PV",
      "1234": 403018154
    },
    {
      "1": 8336,
      "roma": "SUBBIANO",
      "rm": "AR",
      "1234": 409051037
    },
    {
      "1": 8337,
      "roma": "SUBIACO",
      "rm": "RM",
      "1234": 412058103
    },
    {
      "1": 8338,
      "roma": "SUCCIVO",
      "rm": "CE",
      "1234": 415061090
    },
    {
      "1": 8339,
      "roma": "SUEGLIO",
      "rm": "LC",
      "1234": 403097077
    },
    {
      "1": 8340,
      "roma": "STREVI",
      "rm": "AL",
      "1234": 401006168
    },
    {
      "1": 8341,
      "roma": "STRIANO",
      "rm": "NA",
      "1234": 415063081
    },
    {
      "1": 8342,
      "roma": "STRONA",
      "rm": "BI",
      "1234": 401096065
    },
    {
      "1": 8343,
      "roma": "STIMIGLIANO",
      "rm": "RI",
      "1234": 412057066
    },
    {
      "1": 8344,
      "roma": "STINTINO",
      "rm": "SS",
      "1234": 420090089
    },
    {
      "1": 8345,
      "roma": "STIO",
      "rm": "SA",
      "1234": 415065145
    },
    {
      "1": 8346,
      "roma": "STORNARA",
      "rm": "FG",
      "1234": 416071054
    },
    {
      "1": 8347,
      "roma": "STORNARELLA",
      "rm": "FG",
      "1234": 416071055
    },
    {
      "1": 8348,
      "roma": "STORO",
      "rm": "TN",
      "1234": 404022183
    },
    {
      "1": 8349,
      "roma": "STRA",
      "rm": "VE",
      "1234": 405027039
    },
    {
      "1": 8350,
      "roma": "SURANO",
      "rm": "LE",
      "1234": 416075082
    },
    {
      "1": 8351,
      "roma": "SURBO",
      "rm": "LE",
      "1234": 416075083
    },
    {
      "1": 8352,
      "roma": "SUSA",
      "rm": "TO",
      "1234": 401001270
    },
    {
      "1": 8353,
      "roma": "SUSEGANA",
      "rm": "TV",
      "1234": 405026083
    },
    {
      "1": 8354,
      "roma": "SUSTINENTE",
      "rm": "MN",
      "1234": 403020064
    },
    {
      "1": 8355,
      "roma": "SUTERA",
      "rm": "CL",
      "1234": 419085020
    },
    {
      "1": 8356,
      "roma": "SUTRI",
      "rm": "VT",
      "1234": 412056049
    },
    {
      "1": 8357,
      "roma": "SUTRIO",
      "rm": "UD",
      "1234": 406030112
    },
    {
      "1": 8358,
      "roma": "SUELLI",
      "rm": "CA",
      "1234": 420092083
    },
    {
      "1": 8359,
      "roma": "SUELLO",
      "rm": "LC",
      "1234": 403097078
    },
    {
      "1": 8360,
      "roma": "SUISIO",
      "rm": "BG",
      "1234": 403016209
    },
    {
      "1": 8361,
      "roma": "STRONCONE",
      "rm": "TR",
      "1234": 410055031
    },
    {
      "1": 8362,
      "roma": "STRONGOLI",
      "rm": "KR",
      "1234": 418101025
    },
    {
      "1": 8363,
      "roma": "STROPPIANA",
      "rm": "VC",
      "1234": 401002142
    },
    {
      "1": 8364,
      "roma": "STROPPO",
      "rm": "CN",
      "1234": 401004224
    },
    {
      "1": 8365,
      "roma": "STROZZA",
      "rm": "BG",
      "1234": 403016208
    },
    {
      "1": 8366,
      "roma": "SUPINO",
      "rm": "FR",
      "1234": 412060076
    },
    {
      "1": 8367,
      "roma": "TAGLIOLO MONFERRATO",
      "rm": "AL",
      "1234": 401006169
    },
    {
      "1": 8368,
      "roma": "TAIBON AGORDINO",
      "rm": "BL",
      "1234": 405025059
    },
    {
      "1": 8369,
      "roma": "TAINO",
      "rm": "VA",
      "1234": 403012125
    },
    {
      "1": 8370,
      "roma": "TAIPANA",
      "rm": "UD",
      "1234": 406030113
    },
    {
      "1": 8371,
      "roma": "SUVERETO",
      "rm": "LI",
      "1234": 409049020
    },
    {
      "1": 8372,
      "roma": "SULBIATE",
      "rm": "MB",
      "1234": 403108042
    },
    {
      "1": 8373,
      "roma": "SUZZARA",
      "rm": "MN",
      "1234": 403020065
    },
    {
      "1": 8374,
      "roma": "SULMONA",
      "rm": "AQ",
      "1234": 413066098
    },
    {
      "1": 8375,
      "roma": "SULZANO",
      "rm": "BS",
      "1234": 403017182
    },
    {
      "1": 8376,
      "roma": "SUMIRAGO",
      "rm": "VA",
      "1234": 403012124
    },
    {
      "1": 8377,
      "roma": "SUMMONTE",
      "rm": "AV",
      "1234": 415064105
    },
    {
      "1": 8378,
      "roma": "SUNI",
      "rm": "NU",
      "1234": 420091087
    },
    {
      "1": 8379,
      "roma": "SUNO",
      "rm": "NO",
      "1234": 401003143
    },
    {
      "1": 8380,
      "roma": "SUPERSANO",
      "rm": "LE",
      "1234": 416075081
    },
    {
      "1": 8381,
      "roma": "TARANTA PELIGNA",
      "rm": "CH",
      "1234": 413069089
    },
    {
      "1": 8382,
      "roma": "TARANTASCA",
      "rm": "CN",
      "1234": 401004225
    },
    {
      "1": 8383,
      "roma": "TARANTO",
      "rm": "TA",
      "1234": 416073027
    },
    {
      "1": 8384,
      "roma": "TARCENTO",
      "rm": "UD",
      "1234": 406030116
    },
    {
      "1": 8385,
      "roma": "TARQUINIA",
      "rm": "VT",
      "1234": 412056050
    },
    {
      "1": 8386,
      "roma": "TALAMONA",
      "rm": "SO",
      "1234": 403014063
    },
    {
      "1": 8387,
      "roma": "TALANA",
      "rm": "NU",
      "1234": 420091088
    },
    {
      "1": 8388,
      "roma": "TALEGGIO",
      "rm": "BG",
      "1234": 403016210
    },
    {
      "1": 8389,
      "roma": "TALLA",
      "rm": "AR",
      "1234": 409051038
    },
    {
      "1": 8390,
      "roma": "TALAMELLO",
      "rm": "RN",
      "1234": 408099027
    },
    {
      "1": 8391,
      "roma": "TALMASSONS",
      "rm": "UD",
      "1234": 406030114
    },
    {
      "1": 8392,
      "roma": "TAMBRE",
      "rm": "BL",
      "1234": 405025060
    },
    {
      "1": 8393,
      "roma": "TACENO",
      "rm": "LC",
      "1234": 403097079
    },
    {
      "1": 8394,
      "roma": "TADASUNI",
      "rm": "OR",
      "1234": 420095064
    },
    {
      "1": 8395,
      "roma": "TAGGIA",
      "rm": "IM",
      "1234": 407008059
    },
    {
      "1": 8396,
      "roma": "TAGLIACOZZO",
      "rm": "AQ",
      "1234": 413066099
    },
    {
      "1": 8397,
      "roma": "TAGLIO DI PO",
      "rm": "RO",
      "1234": 405029046
    },
    {
      "1": 8398,
      "roma": "TAVERNA",
      "rm": "CZ",
      "1234": 418079146
    },
    {
      "1": 8399,
      "roma": "TAVERNERIO",
      "rm": "CO",
      "1234": 403013222
    },
    {
      "1": 8400,
      "roma": "TAVERNOLA BERGAMASCA",
      "rm": "BG",
      "1234": 403016211
    },
    {
      "1": 8401,
      "roma": "TAVERNOLE SUL MELLA",
      "rm": "BS",
      "1234": 403017183
    },
    {
      "1": 8402,
      "roma": "TAVIANO",
      "rm": "LE",
      "1234": 416075085
    },
    {
      "1": 8403,
      "roma": "TAVIGLIANO",
      "rm": "BI",
      "1234": 401096066
    },
    {
      "1": 8404,
      "roma": "TARSIA",
      "rm": "CS",
      "1234": 418078145
    },
    {
      "1": 8405,
      "roma": "TARTANO",
      "rm": "SO",
      "1234": 403014064
    },
    {
      "1": 8406,
      "roma": "TARVISIO",
      "rm": "UD",
      "1234": 406030117
    },
    {
      "1": 8407,
      "roma": "TARZO",
      "rm": "TV",
      "1234": 405026084
    },
    {
      "1": 8408,
      "roma": "TASSAROLO",
      "rm": "AL",
      "1234": 401006170
    },
    {
      "1": 8409,
      "roma": "TAURANO",
      "rm": "AV",
      "1234": 415064106
    },
    {
      "1": 8410,
      "roma": "TAURASI",
      "rm": "AV",
      "1234": 415064107
    },
    {
      "1": 8411,
      "roma": "TAORMINA",
      "rm": "ME",
      "1234": 419083097
    },
    {
      "1": 8412,
      "roma": "TARANO",
      "rm": "RI",
      "1234": 412057067
    },
    {
      "1": 8413,
      "roma": "TAVENNA",
      "rm": "CB",
      "1234": 414070077
    },
    {
      "1": 8414,
      "roma": "TEMPIO PAUSANIA",
      "rm": "SS",
      "1234": 420090070
    },
    {
      "1": 8415,
      "roma": "TEMU'",
      "rm": "BS",
      "1234": 403017184
    },
    {
      "1": 8416,
      "roma": "TENNA",
      "rm": "TN",
      "1234": 404022190
    },
    {
      "1": 8417,
      "roma": "TENNO",
      "rm": "TN",
      "1234": 404022191
    },
    {
      "1": 8418,
      "roma": "TEOLO",
      "rm": "PD",
      "1234": 405028089
    },
    {
      "1": 8419,
      "roma": "TAVOLETO",
      "rm": "PS",
      "1234": 411041064
    },
    {
      "1": 8420,
      "roma": "TAVULLIA",
      "rm": "PS",
      "1234": 411041065
    },
    {
      "1": 8421,
      "roma": "TEANA",
      "rm": "PZ",
      "1234": 417076087
    },
    {
      "1": 8422,
      "roma": "TEANO",
      "rm": "CE",
      "1234": 415061091
    },
    {
      "1": 8423,
      "roma": "TAURIANOVA",
      "rm": "RC",
      "1234": 418080093
    },
    {
      "1": 8424,
      "roma": "TAURISANO",
      "rm": "LE",
      "1234": 416075084
    },
    {
      "1": 8425,
      "roma": "TAVAGNACCO",
      "rm": "UD",
      "1234": 406030118
    },
    {
      "1": 8426,
      "roma": "TAVAGNASCO",
      "rm": "TO",
      "1234": 401001271
    },
    {
      "1": 8427,
      "roma": "TAVAZZANO CON VILLAVESCO",
      "rm": "LO",
      "1234": 403098056
    },
    {
      "1": 8428,
      "roma": "TERNATE",
      "rm": "VA",
      "1234": 403012126
    },
    {
      "1": 8429,
      "roma": "TERNENGO",
      "rm": "BI",
      "1234": 401096067
    },
    {
      "1": 8430,
      "roma": "TERNI",
      "rm": "TR",
      "1234": 410055032
    },
    {
      "1": 8431,
      "roma": "TERNO D'ISOLA",
      "rm": "BG",
      "1234": 403016213
    },
    {
      "1": 8432,
      "roma": "TERRACINA",
      "rm": "LT",
      "1234": 412059032
    },
    {
      "1": 8433,
      "roma": "TEORA",
      "rm": "AV",
      "1234": 415064108
    },
    {
      "1": 8434,
      "roma": "TERAMO",
      "rm": "TE",
      "1234": 413067041
    },
    {
      "1": 8435,
      "roma": "TERDOBBIATE",
      "rm": "NO",
      "1234": 401003144
    },
    {
      "1": 8436,
      "roma": "TERELLE",
      "rm": "FR",
      "1234": 412060077
    },
    {
      "1": 8437,
      "roma": "TERENTO",
      "rm": "BZ",
      "1234": 404021096
    },
    {
      "1": 8438,
      "roma": "TERENZO",
      "rm": "PR",
      "1234": 408034038
    },
    {
      "1": 8439,
      "roma": "TEGGIANO",
      "rm": "SA",
      "1234": 415065146
    },
    {
      "1": 8440,
      "roma": "TEGLIO",
      "rm": "SO",
      "1234": 403014065
    },
    {
      "1": 8441,
      "roma": "TEGLIO VENETO",
      "rm": "VE",
      "1234": 405027040
    },
    {
      "1": 8442,
      "roma": "TELESE TERME",
      "rm": "BN",
      "1234": 415062074
    },
    {
      "1": 8443,
      "roma": "TELGATE",
      "rm": "BG",
      "1234": 403016212
    },
    {
      "1": 8444,
      "roma": "TELTI",
      "rm": "SS",
      "1234": 420090080
    },
    {
      "1": 8445,
      "roma": "TELVE",
      "rm": "TN",
      "1234": 404022188
    },
    {
      "1": 8446,
      "roma": "TELVE DI SOPRA",
      "rm": "TN",
      "1234": 404022189
    },
    {
      "1": 8447,
      "roma": "TERMOLI",
      "rm": "CB",
      "1234": 414070078
    },
    {
      "1": 8448,
      "roma": "TERRAZZO",
      "rm": "VR",
      "1234": 405023085
    },
    {
      "1": 8449,
      "roma": "TERRICCIOLA",
      "rm": "PI",
      "1234": 409050036
    },
    {
      "1": 8450,
      "roma": "TERRUGGIA",
      "rm": "AL",
      "1234": 401006171
    },
    {
      "1": 8451,
      "roma": "TERTENIA",
      "rm": "NU",
      "1234": 420091089
    },
    {
      "1": 8452,
      "roma": "TERZIGNO",
      "rm": "NA",
      "1234": 415063082
    },
    {
      "1": 8453,
      "roma": "TERZO",
      "rm": "AL",
      "1234": 401006172
    },
    {
      "1": 8454,
      "roma": "TERRAGNOLO",
      "rm": "TN",
      "1234": 404022193
    },
    {
      "1": 8455,
      "roma": "TERRALBA",
      "rm": "OR",
      "1234": 420095065
    },
    {
      "1": 8456,
      "roma": "TERRANOVA DA SIBARI",
      "rm": "CS",
      "1234": 418078146
    },
    {
      "1": 8457,
      "roma": "TERRANOVA DEI PASSERINI",
      "rm": "LO",
      "1234": 403098057
    },
    {
      "1": 8458,
      "roma": "TERRANOVA DI POLLINO",
      "rm": "PZ",
      "1234": 417076088
    },
    {
      "1": 8459,
      "roma": "TERGU",
      "rm": "SS",
      "1234": 420090086
    },
    {
      "1": 8460,
      "roma": "TERLANO",
      "rm": "BZ",
      "1234": 404021097
    },
    {
      "1": 8461,
      "roma": "TERLIZZI",
      "rm": "BA",
      "1234": 416072043
    },
    {
      "1": 8462,
      "roma": "TERME VIGLIATORE",
      "rm": "ME",
      "1234": 419083106
    },
    {
      "1": 8463,
      "roma": "TERMENO SULLA STRADA DEL VINO",
      "rm": "BZ",
      "1234": 404021098
    },
    {
      "1": 8464,
      "roma": "TERMINI IMERESE",
      "rm": "PA",
      "1234": 419082070
    },
    {
      "1": 8465,
      "roma": "TIANA",
      "rm": "NU",
      "1234": 420091091
    },
    {
      "1": 8466,
      "roma": "TICENGO",
      "rm": "CR",
      "1234": 403019104
    },
    {
      "1": 8467,
      "roma": "TICINETO",
      "rm": "AL",
      "1234": 401006173
    },
    {
      "1": 8468,
      "roma": "TIGGIANO",
      "rm": "LE",
      "1234": 416075086
    },
    {
      "1": 8469,
      "roma": "TIGLIETO",
      "rm": "GE",
      "1234": 407010061
    },
    {
      "1": 8470,
      "roma": "TIGLIOLE",
      "rm": "AT",
      "1234": 401005108
    },
    {
      "1": 8471,
      "roma": "TERZO DI AQUILEIA",
      "rm": "UD",
      "1234": 406030120
    },
    {
      "1": 8472,
      "roma": "TERZOLAS",
      "rm": "TN",
      "1234": 404022195
    },
    {
      "1": 8473,
      "roma": "TERZORIO",
      "rm": "IM",
      "1234": 407008060
    },
    {
      "1": 8474,
      "roma": "TESERO",
      "rm": "TN",
      "1234": 404022196
    },
    {
      "1": 8475,
      "roma": "TESIMO",
      "rm": "BZ",
      "1234": 404021099
    },
    {
      "1": 8476,
      "roma": "TESSENNANO",
      "rm": "VT",
      "1234": 412056051
    },
    {
      "1": 8477,
      "roma": "TERRANOVA SAPPO MINULIO",
      "rm": "RC",
      "1234": 418080094
    },
    {
      "1": 8478,
      "roma": "TERRANUOVA BRACCIOLINI",
      "rm": "AR",
      "1234": 409051039
    },
    {
      "1": 8479,
      "roma": "TERRASINI",
      "rm": "PA",
      "1234": 419082071
    },
    {
      "1": 8480,
      "roma": "TERRASSA PADOVANA",
      "rm": "PD",
      "1234": 405028090
    },
    {
      "1": 8481,
      "roma": "TERRAVECCHIA",
      "rm": "CS",
      "1234": 418078147
    },
    {
      "1": 8482,
      "roma": "TOANO",
      "rm": "RE",
      "1234": 408035041
    },
    {
      "1": 8483,
      "roma": "TOCCO CAUDIO",
      "rm": "BN",
      "1234": 415062075
    },
    {
      "1": 8484,
      "roma": "TOCCO DA CASAURIA",
      "rm": "PE",
      "1234": 413068042
    },
    {
      "1": 8485,
      "roma": "TOCENO",
      "rm": "VB",
      "1234": 401103065
    },
    {
      "1": 8486,
      "roma": "TODI",
      "rm": "PG",
      "1234": 410054052
    },
    {
      "1": 8487,
      "roma": "TOFFIA",
      "rm": "RI",
      "1234": 412057068
    },
    {
      "1": 8488,
      "roma": "TOIRANO",
      "rm": "SV",
      "1234": 407009061
    },
    {
      "1": 8489,
      "roma": "TIGNALE",
      "rm": "BS",
      "1234": 403017185
    },
    {
      "1": 8490,
      "roma": "TINNURA",
      "rm": "NU",
      "1234": 420091092
    },
    {
      "1": 8491,
      "roma": "TIONE DEGLI ABRUZZI",
      "rm": "AQ",
      "1234": 413066100
    },
    {
      "1": 8492,
      "roma": "TIONE DI TRENTO",
      "rm": "TN",
      "1234": 404022199
    },
    {
      "1": 8493,
      "roma": "TESTICO",
      "rm": "SV",
      "1234": 407009060
    },
    {
      "1": 8494,
      "roma": "TETI",
      "rm": "NU",
      "1234": 420091090
    },
    {
      "1": 8495,
      "roma": "TEULADA",
      "rm": "CA",
      "1234": 420092084
    },
    {
      "1": 8496,
      "roma": "TEVEROLA",
      "rm": "CE",
      "1234": 415061092
    },
    {
      "1": 8497,
      "roma": "TEZZE SUL BRENTA",
      "rm": "VI",
      "1234": 405024104
    },
    {
      "1": 8498,
      "roma": "THIENE",
      "rm": "VI",
      "1234": 405024105
    },
    {
      "1": 8499,
      "roma": "THIESI",
      "rm": "SS",
      "1234": 420090071
    },
    {
      "1": 8500,
      "roma": "TIZZANO VAL PARMA",
      "rm": "PR",
      "1234": 408034039
    },
    {
      "1": 8501,
      "roma": "TORA E PICCILLI",
      "rm": "CE",
      "1234": 415061093
    },
    {
      "1": 8502,
      "roma": "TORANO CASTELLO",
      "rm": "CS",
      "1234": 418078148
    },
    {
      "1": 8503,
      "roma": "TORANO NUOVO",
      "rm": "TE",
      "1234": 413067042
    },
    {
      "1": 8504,
      "roma": "TORBOLE CASAGLIA",
      "rm": "BS",
      "1234": 403017186
    },
    {
      "1": 8505,
      "roma": "TORCEGNO",
      "rm": "TN",
      "1234": 404022202
    },
    {
      "1": 8506,
      "roma": "TORCHIARA",
      "rm": "SA",
      "1234": 415065147
    },
    {
      "1": 8507,
      "roma": "TOLENTINO",
      "rm": "MC",
      "1234": 411043053
    },
    {
      "1": 8508,
      "roma": "TOLFA",
      "rm": "RM",
      "1234": 412058105
    },
    {
      "1": 8509,
      "roma": "TOLLEGNO",
      "rm": "BI",
      "1234": 401096068
    },
    {
      "1": 8510,
      "roma": "TOLLO",
      "rm": "CH",
      "1234": 413069090
    },
    {
      "1": 8511,
      "roma": "TOLMEZZO",
      "rm": "UD",
      "1234": 406030121
    },
    {
      "1": 8512,
      "roma": "TIRANO",
      "rm": "SO",
      "1234": 403014066
    },
    {
      "1": 8513,
      "roma": "TIRES",
      "rm": "BZ",
      "1234": 404021100
    },
    {
      "1": 8514,
      "roma": "TIRIOLO",
      "rm": "CZ",
      "1234": 418079147
    },
    {
      "1": 8515,
      "roma": "TIROLO",
      "rm": "BZ",
      "1234": 404021101
    },
    {
      "1": 8516,
      "roma": "TISSI",
      "rm": "SS",
      "1234": 420090072
    },
    {
      "1": 8517,
      "roma": "TITO",
      "rm": "PZ",
      "1234": 417076089
    },
    {
      "1": 8518,
      "roma": "TIVOLI",
      "rm": "RM",
      "1234": 412058104
    },
    {
      "1": 8519,
      "roma": "TONEZZA DEL CIMONE",
      "rm": "VI",
      "1234": 405024106
    },
    {
      "1": 8520,
      "roma": "TORNOLO",
      "rm": "PR",
      "1234": 408034040
    },
    {
      "1": 8521,
      "roma": "TORO",
      "rm": "CB",
      "1234": 414070080
    },
    {
      "1": 8522,
      "roma": "TORPE'",
      "rm": "NU",
      "1234": 420091094
    },
    {
      "1": 8523,
      "roma": "TORRACA",
      "rm": "SA",
      "1234": 415065148
    },
    {
      "1": 8524,
      "roma": "TORRALBA",
      "rm": "SS",
      "1234": 420090073
    },
    {
      "1": 8525,
      "roma": "TORRAZZA COSTE",
      "rm": "PV",
      "1234": 403018155
    },
    {
      "1": 8526,
      "roma": "TORCHIAROLO",
      "rm": "BR",
      "1234": 416074018
    },
    {
      "1": 8527,
      "roma": "TORELLA DE' LOMBARDI",
      "rm": "AV",
      "1234": 415064109
    },
    {
      "1": 8528,
      "roma": "TORELLA DEL SANNIO",
      "rm": "CB",
      "1234": 414070079
    },
    {
      "1": 8529,
      "roma": "TORGIANO",
      "rm": "PG",
      "1234": 410054053
    },
    {
      "1": 8530,
      "roma": "TORGNON",
      "rm": "AO",
      "1234": 402007067
    },
    {
      "1": 8531,
      "roma": "TORINO",
      "rm": "TO",
      "1234": 401001272
    },
    {
      "1": 8532,
      "roma": "TORINO DI SANGRO",
      "rm": "CH",
      "1234": 413069091
    },
    {
      "1": 8533,
      "roma": "TOLVE",
      "rm": "PZ",
      "1234": 417076090
    },
    {
      "1": 8534,
      "roma": "TOMBOLO",
      "rm": "PD",
      "1234": 405028091
    },
    {
      "1": 8535,
      "roma": "TON",
      "rm": "TN",
      "1234": 404022200
    },
    {
      "1": 8536,
      "roma": "TONARA",
      "rm": "NU",
      "1234": 420091093
    },
    {
      "1": 8537,
      "roma": "TONCO",
      "rm": "AT",
      "1234": 401005109
    },
    {
      "1": 8538,
      "roma": "TONENGO",
      "rm": "AT",
      "1234": 401005110
    },
    {
      "1": 8539,
      "roma": "TORNO",
      "rm": "CO",
      "1234": 403013223
    },
    {
      "1": 8540,
      "roma": "TORRE DE' ROVERI",
      "rm": "BG",
      "1234": 403016216
    },
    {
      "1": 8541,
      "roma": "TORRE DEI NEGRI",
      "rm": "PV",
      "1234": 403018158
    },
    {
      "1": 8542,
      "roma": "TORRE DEL GRECO",
      "rm": "NA",
      "1234": 415063084
    },
    {
      "1": 8543,
      "roma": "TORRE DI MOSTO",
      "rm": "VE",
      "1234": 405027041
    },
    {
      "1": 8544,
      "roma": "TORRAZZA PIEMONTE",
      "rm": "TO",
      "1234": 401001273
    },
    {
      "1": 8545,
      "roma": "TORRAZZO",
      "rm": "BI",
      "1234": 401096069
    },
    {
      "1": 8546,
      "roma": "TORRE ANNUNZIATA",
      "rm": "NA",
      "1234": 415063083
    },
    {
      "1": 8547,
      "roma": "TORRE BERETTI E CASTELLARO",
      "rm": "PV",
      "1234": 403018156
    },
    {
      "1": 8548,
      "roma": "TORRE BOLDONE",
      "rm": "BG",
      "1234": 403016214
    },
    {
      "1": 8549,
      "roma": "TORRE BORMIDA",
      "rm": "CN",
      "1234": 401004226
    },
    {
      "1": 8550,
      "roma": "TORITTO",
      "rm": "BA",
      "1234": 416072044
    },
    {
      "1": 8551,
      "roma": "TORLINO VIMERCATI",
      "rm": "CR",
      "1234": 403019105
    },
    {
      "1": 8552,
      "roma": "TORNACO",
      "rm": "NO",
      "1234": 401003146
    },
    {
      "1": 8553,
      "roma": "TORNARECCIO",
      "rm": "CH",
      "1234": 413069092
    },
    {
      "1": 8554,
      "roma": "TORNATA",
      "rm": "CR",
      "1234": 403019106
    },
    {
      "1": 8555,
      "roma": "TORNIMPARTE",
      "rm": "AQ",
      "1234": 413066101
    },
    {
      "1": 8556,
      "roma": "TORRE DE' PICENARDI",
      "rm": "CR",
      "1234": 403019107
    },
    {
      "1": 8557,
      "roma": "TORRE SAN PATRIZIO",
      "rm": "FM",
      "1234": 411109040
    },
    {
      "1": 8558,
      "roma": "TORRECUSO",
      "rm": "BN",
      "1234": 415062076
    },
    {
      "1": 8559,
      "roma": "TORREGLIA",
      "rm": "PD",
      "1234": 405028092
    },
    {
      "1": 8560,
      "roma": "TORREGROTTA",
      "rm": "ME",
      "1234": 419083098
    },
    {
      "1": 8561,
      "roma": "TORREMAGGIORE",
      "rm": "FG",
      "1234": 416071056
    },
    {
      "1": 8562,
      "roma": "TORRENOVA",
      "rm": "ME",
      "1234": 419083108
    },
    {
      "1": 8563,
      "roma": "TORRESINA",
      "rm": "CN",
      "1234": 401004229
    },
    {
      "1": 8564,
      "roma": "TORRETTA",
      "rm": "PA",
      "1234": 419082072
    },
    {
      "1": 8565,
      "roma": "TORREVECCHIA PIA",
      "rm": "PV",
      "1234": 403018160
    },
    {
      "1": 8566,
      "roma": "TORRE DI RUGGIERO",
      "rm": "CZ",
      "1234": 418079148
    },
    {
      "1": 8567,
      "roma": "TORRE DI SANTA MARIA",
      "rm": "SO",
      "1234": 403014067
    },
    {
      "1": 8568,
      "roma": "TORRE LE NOCELLE",
      "rm": "AV",
      "1234": 415064110
    },
    {
      "1": 8569,
      "roma": "TORRE MONDOVI'",
      "rm": "CN",
      "1234": 401004227
    },
    {
      "1": 8570,
      "roma": "TORRE ORSAIA",
      "rm": "SA",
      "1234": 415065149
    },
    {
      "1": 8571,
      "roma": "TORRE PALLAVICINA",
      "rm": "BG",
      "1234": 403016217
    },
    {
      "1": 8572,
      "roma": "TORRE PELLICE",
      "rm": "TO",
      "1234": 401001275
    },
    {
      "1": 8573,
      "roma": "TORRE CAJETANI",
      "rm": "FR",
      "1234": 412060078
    },
    {
      "1": 8574,
      "roma": "TORRE CANAVESE",
      "rm": "TO",
      "1234": 401001274
    },
    {
      "1": 8575,
      "roma": "TORRE D'ARESE",
      "rm": "PV",
      "1234": 403018157
    },
    {
      "1": 8576,
      "roma": "TORRE D'ISOLA",
      "rm": "PV",
      "1234": 403018159
    },
    {
      "1": 8577,
      "roma": "TORRE DE' PASSERI",
      "rm": "PE",
      "1234": 413068043
    },
    {
      "1": 8578,
      "roma": "TORREBRUNA",
      "rm": "CH",
      "1234": 413069093
    },
    {
      "1": 8579,
      "roma": "TORRIONI",
      "rm": "AV",
      "1234": 415064111
    },
    {
      "1": 8580,
      "roma": "TORRITA DI SIENA",
      "rm": "SI",
      "1234": 409052035
    },
    {
      "1": 8581,
      "roma": "TORRITA TIBERINA",
      "rm": "RM",
      "1234": 412058106
    },
    {
      "1": 8582,
      "roma": "TORTOLI'",
      "rm": "NU",
      "1234": 420091095
    },
    {
      "1": 8583,
      "roma": "TORTONA",
      "rm": "AL",
      "1234": 401006174
    },
    {
      "1": 8584,
      "roma": "TORTORA",
      "rm": "CS",
      "1234": 418078149
    },
    {
      "1": 8585,
      "roma": "TORTORELLA",
      "rm": "SA",
      "1234": 415065150
    },
    {
      "1": 8586,
      "roma": "TORTORETO",
      "rm": "TE",
      "1234": 413067044
    },
    {
      "1": 8587,
      "roma": "TORREVECCHIA TEATINA",
      "rm": "CH",
      "1234": 413069094
    },
    {
      "1": 8588,
      "roma": "TORRI DEL BENACO",
      "rm": "VR",
      "1234": 405023086
    },
    {
      "1": 8589,
      "roma": "TORRI DI QUARTESOLO",
      "rm": "VI",
      "1234": 405024108
    },
    {
      "1": 8590,
      "roma": "TORRI IN SABINA",
      "rm": "RI",
      "1234": 412057070
    },
    {
      "1": 8591,
      "roma": "TORRICE",
      "rm": "FR",
      "1234": 412060079
    },
    {
      "1": 8592,
      "roma": "TORRE SAN GIORGIO",
      "rm": "CN",
      "1234": 401004228
    },
    {
      "1": 8593,
      "roma": "TORRE SANTA SUSANNA",
      "rm": "BR",
      "1234": 416074019
    },
    {
      "1": 8594,
      "roma": "TORREANO",
      "rm": "UD",
      "1234": 406030122
    },
    {
      "1": 8595,
      "roma": "TORREBELVICINO",
      "rm": "VI",
      "1234": 405024107
    },
    {
      "1": 8596,
      "roma": "TORRILE",
      "rm": "PR",
      "1234": 408034041
    },
    {
      "1": 8597,
      "roma": "TRAMONTI DI SOTTO",
      "rm": "PN",
      "1234": 406093046
    },
    {
      "1": 8598,
      "roma": "TRAMUTOLA",
      "rm": "PZ",
      "1234": 417076091
    },
    {
      "1": 8599,
      "roma": "TRANA",
      "rm": "TO",
      "1234": 401001276
    },
    {
      "1": 8600,
      "roma": "TORTORICI",
      "rm": "ME",
      "1234": 419083099
    },
    {
      "1": 8601,
      "roma": "TORVISCOSA",
      "rm": "UD",
      "1234": 406030123
    },
    {
      "1": 8602,
      "roma": "TOSCOLANO MADERNO",
      "rm": "BS",
      "1234": 403017187
    },
    {
      "1": 8603,
      "roma": "TOSSICIA",
      "rm": "TE",
      "1234": 413067045
    },
    {
      "1": 8604,
      "roma": "TOVO DI SANT'AGATA",
      "rm": "SO",
      "1234": 403014068
    },
    {
      "1": 8605,
      "roma": "TOVO SAN GIACOMO",
      "rm": "SV",
      "1234": 407009062
    },
    {
      "1": 8606,
      "roma": "TORRICELLA",
      "rm": "TA",
      "1234": 416073028
    },
    {
      "1": 8607,
      "roma": "TORRICELLA DEL PIZZO",
      "rm": "CR",
      "1234": 403019108
    },
    {
      "1": 8608,
      "roma": "TORRICELLA IN SABINA",
      "rm": "RI",
      "1234": 412057069
    },
    {
      "1": 8609,
      "roma": "TORRICELLA PELIGNA",
      "rm": "CH",
      "1234": 413069095
    },
    {
      "1": 8610,
      "roma": "TORRICELLA SICURA",
      "rm": "TE",
      "1234": 413067043
    },
    {
      "1": 8611,
      "roma": "TORRICELLA VERZATE",
      "rm": "PV",
      "1234": 403018161
    },
    {
      "1": 8612,
      "roma": "TORRIGLIA",
      "rm": "GE",
      "1234": 407010062
    },
    {
      "1": 8613,
      "roma": "TRAMONTI",
      "rm": "SA",
      "1234": 415065151
    },
    {
      "1": 8614,
      "roma": "TRAMONTI DI SOPRA",
      "rm": "PN",
      "1234": 406093045
    },
    {
      "1": 8615,
      "roma": "TRAVERSELLA",
      "rm": "TO",
      "1234": 401001278
    },
    {
      "1": 8616,
      "roma": "TRAVERSETOLO",
      "rm": "PR",
      "1234": 408034042
    },
    {
      "1": 8617,
      "roma": "TRAVES",
      "rm": "TO",
      "1234": 401001279
    },
    {
      "1": 8618,
      "roma": "TRAVESIO",
      "rm": "PN",
      "1234": 406093047
    },
    {
      "1": 8619,
      "roma": "TRANI",
      "rm": "BT",
      "1234": 416110009
    },
    {
      "1": 8620,
      "roma": "TRAVO",
      "rm": "PC",
      "1234": 408033043
    },
    {
      "1": 8621,
      "roma": "TREBASELEGHE",
      "rm": "PD",
      "1234": 405028093
    },
    {
      "1": 8622,
      "roma": "TRAONA",
      "rm": "SO",
      "1234": 403014069
    },
    {
      "1": 8623,
      "roma": "TRAPANI",
      "rm": "TP",
      "1234": 419081021
    },
    {
      "1": 8624,
      "roma": "TRAPPETO",
      "rm": "PA",
      "1234": 419082074
    },
    {
      "1": 8625,
      "roma": "TRAREGO-VIGGIONA",
      "rm": "VB",
      "1234": 401103066
    },
    {
      "1": 8626,
      "roma": "TRASACCO",
      "rm": "AQ",
      "1234": 413066102
    },
    {
      "1": 8627,
      "roma": "TRASAGHIS",
      "rm": "UD",
      "1234": 406030124
    },
    {
      "1": 8628,
      "roma": "TRASQUERA",
      "rm": "VB",
      "1234": 401103067
    },
    {
      "1": 8629,
      "roma": "TRABIA",
      "rm": "PA",
      "1234": 419082073
    },
    {
      "1": 8630,
      "roma": "TRADATE",
      "rm": "VA",
      "1234": 403012127
    },
    {
      "1": 8631,
      "roma": "TRAMATZA",
      "rm": "OR",
      "1234": 420095066
    },
    {
      "1": 8632,
      "roma": "TRAMBILENO",
      "rm": "TN",
      "1234": 404022203
    },
    {
      "1": 8633,
      "roma": "TREMESTIERI ETNEO",
      "rm": "CT",
      "1234": 419087051
    },
    {
      "1": 8634,
      "roma": "TRECASTELLI",
      "rm": "AN",
      "1234": 411042050
    },
    {
      "1": 8635,
      "roma": "TREMOSINE",
      "rm": "BS",
      "1234": 403017189
    },
    {
      "1": 8636,
      "roma": "TREBISACCE",
      "rm": "CS",
      "1234": 418078150
    },
    {
      "1": 8637,
      "roma": "TRECASE",
      "rm": "NA",
      "1234": 415063091
    },
    {
      "1": 8638,
      "roma": "TRECASTAGNI",
      "rm": "CT",
      "1234": 419087050
    },
    {
      "1": 8639,
      "roma": "TRECATE",
      "rm": "NO",
      "1234": 401003149
    },
    {
      "1": 8640,
      "roma": "TRECCHINA",
      "rm": "PZ",
      "1234": 417076092
    },
    {
      "1": 8641,
      "roma": "TRATALIAS",
      "rm": "CA",
      "1234": 420092085
    },
    {
      "1": 8642,
      "roma": "TRAVACO' SICCOMARIO",
      "rm": "PV",
      "1234": 403018162
    },
    {
      "1": 8643,
      "roma": "TRAVAGLIATO",
      "rm": "BS",
      "1234": 403017188
    },
    {
      "1": 8644,
      "roma": "TRAVEDONA-MONATE",
      "rm": "VA",
      "1234": 403012128
    },
    {
      "1": 8645,
      "roma": "TREIA",
      "rm": "MC",
      "1234": 411043054
    },
    {
      "1": 8646,
      "roma": "TREISO",
      "rm": "CN",
      "1234": 401004230
    },
    {
      "1": 8647,
      "roma": "TRESIVIO",
      "rm": "SO",
      "1234": 403014070
    },
    {
      "1": 8648,
      "roma": "TRESNURAGHES",
      "rm": "OR",
      "1234": 420095067
    },
    {
      "1": 8649,
      "roma": "TRENTINARA",
      "rm": "SA",
      "1234": 415065152
    },
    {
      "1": 8650,
      "roma": "TRENTO",
      "rm": "TN",
      "1234": 404022205
    },
    {
      "1": 8651,
      "roma": "TRENTOLA-DUCENTA",
      "rm": "CE",
      "1234": 415061094
    },
    {
      "1": 8652,
      "roma": "TRENZANO",
      "rm": "BS",
      "1234": 403017190
    },
    {
      "1": 8653,
      "roma": "TRECENTA",
      "rm": "RO",
      "1234": 405029047
    },
    {
      "1": 8654,
      "roma": "TREDOZIO",
      "rm": "FO",
      "1234": 408040049
    },
    {
      "1": 8655,
      "roma": "TREGLIO",
      "rm": "CH",
      "1234": 413069096
    },
    {
      "1": 8656,
      "roma": "TREGNAGO",
      "rm": "VR",
      "1234": 405023087
    },
    {
      "1": 8657,
      "roma": "TRESCORE BALNEARIO",
      "rm": "BG",
      "1234": 403016218
    },
    {
      "1": 8658,
      "roma": "TRESCORE CREMASCO",
      "rm": "CR",
      "1234": 403019109
    },
    {
      "1": 8659,
      "roma": "TRIBANO",
      "rm": "PD",
      "1234": 405028094
    },
    {
      "1": 8660,
      "roma": "TRIBIANO",
      "rm": "MI",
      "1234": 403015222
    },
    {
      "1": 8661,
      "roma": "TRIBOGNA",
      "rm": "GE",
      "1234": 407010063
    },
    {
      "1": 8662,
      "roma": "TRICARICO",
      "rm": "MT",
      "1234": 417077028
    },
    {
      "1": 8663,
      "roma": "TRICASE",
      "rm": "LE",
      "1234": 416075088
    },
    {
      "1": 8664,
      "roma": "TREVENZUOLO",
      "rm": "VR",
      "1234": 405023088
    },
    {
      "1": 8665,
      "roma": "TREVI",
      "rm": "PG",
      "1234": 410054054
    },
    {
      "1": 8666,
      "roma": "TREVI NEL LAZIO",
      "rm": "FR",
      "1234": 412060080
    },
    {
      "1": 8667,
      "roma": "TREVICO",
      "rm": "AV",
      "1234": 415064112
    },
    {
      "1": 8668,
      "roma": "TREVIGLIO",
      "rm": "BG",
      "1234": 403016219
    },
    {
      "1": 8669,
      "roma": "TREVIGNANO",
      "rm": "TV",
      "1234": 405026085
    },
    {
      "1": 8670,
      "roma": "TREVIGNANO ROMANO",
      "rm": "RM",
      "1234": 412058107
    },
    {
      "1": 8671,
      "roma": "TREVILLE",
      "rm": "AL",
      "1234": 401006175
    },
    {
      "1": 8672,
      "roma": "TREVIOLO",
      "rm": "BG",
      "1234": 403016220
    },
    {
      "1": 8673,
      "roma": "TREPPO GRANDE",
      "rm": "UD",
      "1234": 406030126
    },
    {
      "1": 8674,
      "roma": "TREPUZZI",
      "rm": "LE",
      "1234": 416075087
    },
    {
      "1": 8675,
      "roma": "TREQUANDA",
      "rm": "SI",
      "1234": 409052036
    },
    {
      "1": 8676,
      "roma": "TRESANA",
      "rm": "MS",
      "1234": 409045015
    },
    {
      "1": 8677,
      "roma": "TREZZO TINELLA",
      "rm": "CN",
      "1234": 401004231
    },
    {
      "1": 8678,
      "roma": "TREZZONE",
      "rm": "CO",
      "1234": 403013226
    },
    {
      "1": 8679,
      "roma": "TRIVENTO",
      "rm": "CB",
      "1234": 414070081
    },
    {
      "1": 8680,
      "roma": "TRIVIGLIANO",
      "rm": "FR",
      "1234": 412060081
    },
    {
      "1": 8681,
      "roma": "TRIVIGNANO UDINESE",
      "rm": "UD",
      "1234": 406030128
    },
    {
      "1": 8682,
      "roma": "TRIVIGNO",
      "rm": "PZ",
      "1234": 417076093
    },
    {
      "1": 8683,
      "roma": "TRICERRO",
      "rm": "VC",
      "1234": 401002147
    },
    {
      "1": 8684,
      "roma": "TRIUGGIO",
      "rm": "MB",
      "1234": 403108043
    },
    {
      "1": 8685,
      "roma": "TRICESIMO",
      "rm": "UD",
      "1234": 406030127
    },
    {
      "1": 8686,
      "roma": "TRIEI",
      "rm": "NU",
      "1234": 420091097
    },
    {
      "1": 8687,
      "roma": "TRIESTE",
      "rm": "TS",
      "1234": 406032006
    },
    {
      "1": 8688,
      "roma": "TRIGGIANO",
      "rm": "BA",
      "1234": 416072046
    },
    {
      "1": 8689,
      "roma": "TRIGOLO",
      "rm": "CR",
      "1234": 403019110
    },
    {
      "1": 8690,
      "roma": "TREVISO",
      "rm": "TV",
      "1234": 405026086
    },
    {
      "1": 8691,
      "roma": "TREVISO BRESCIANO",
      "rm": "BS",
      "1234": 403017191
    },
    {
      "1": 8692,
      "roma": "TREZZANO ROSA",
      "rm": "MI",
      "1234": 403015219
    },
    {
      "1": 8693,
      "roma": "TREZZANO SUL NAVIGLIO",
      "rm": "MI",
      "1234": 403015220
    },
    {
      "1": 8694,
      "roma": "TREZZO SULL'ADDA",
      "rm": "MI",
      "1234": 403015221
    },
    {
      "1": 8695,
      "roma": "TRIPI",
      "rm": "ME",
      "1234": 419083100
    },
    {
      "1": 8696,
      "roma": "TRISOBBIO",
      "rm": "AL",
      "1234": 401006176
    },
    {
      "1": 8697,
      "roma": "TRISSINO",
      "rm": "VI",
      "1234": 405024110
    },
    {
      "1": 8698,
      "roma": "TUBRE",
      "rm": "BZ",
      "1234": 404021103
    },
    {
      "1": 8699,
      "roma": "TUFARA",
      "rm": "CB",
      "1234": 414070082
    },
    {
      "1": 8700,
      "roma": "TUFILLO",
      "rm": "CH",
      "1234": 413069097
    },
    {
      "1": 8701,
      "roma": "TUFINO",
      "rm": "NA",
      "1234": 415063085
    },
    {
      "1": 8702,
      "roma": "TRIVOLZIO",
      "rm": "PV",
      "1234": 403018163
    },
    {
      "1": 8703,
      "roma": "TRINITAPOLI",
      "rm": "BT",
      "1234": 416110010
    },
    {
      "1": 8704,
      "roma": "TRODENA",
      "rm": "BZ",
      "1234": 404021102
    },
    {
      "1": 8705,
      "roma": "TROFARELLO",
      "rm": "TO",
      "1234": 401001280
    },
    {
      "1": 8706,
      "roma": "TROIA",
      "rm": "FG",
      "1234": 416071058
    },
    {
      "1": 8707,
      "roma": "TROINA",
      "rm": "EN",
      "1234": 419086018
    },
    {
      "1": 8708,
      "roma": "TROMELLO",
      "rm": "PV",
      "1234": 403018164
    },
    {
      "1": 8709,
      "roma": "TRINITA'",
      "rm": "CN",
      "1234": 401004232
    },
    {
      "1": 8710,
      "roma": "TRINITA' D'AGULTU E VIGNOLA",
      "rm": "SS",
      "1234": 420090074
    },
    {
      "1": 8711,
      "roma": "TRINO",
      "rm": "VC",
      "1234": 401002148
    },
    {
      "1": 8712,
      "roma": "TRIORA",
      "rm": "IM",
      "1234": 407008061
    },
    {
      "1": 8713,
      "roma": "TROPEA",
      "rm": "VV",
      "1234": 418102044
    },
    {
      "1": 8714,
      "roma": "TROVO",
      "rm": "PV",
      "1234": 403018165
    },
    {
      "1": 8715,
      "roma": "TRUCCAZZANO",
      "rm": "MI",
      "1234": 403015224
    },
    {
      "1": 8716,
      "roma": "TURRIVALIGNANI",
      "rm": "PE",
      "1234": 413068044
    },
    {
      "1": 8717,
      "roma": "TURSI",
      "rm": "MT",
      "1234": 417077029
    },
    {
      "1": 8718,
      "roma": "TUSA",
      "rm": "ME",
      "1234": 419083101
    },
    {
      "1": 8719,
      "roma": "TUSCANIA",
      "rm": "VT",
      "1234": 412056052
    },
    {
      "1": 8720,
      "roma": "TUFO",
      "rm": "AV",
      "1234": 415064113
    },
    {
      "1": 8721,
      "roma": "TUGLIE",
      "rm": "LE",
      "1234": 416075089
    },
    {
      "1": 8722,
      "roma": "TUILI",
      "rm": "CA",
      "1234": 420092086
    },
    {
      "1": 8723,
      "roma": "TULA",
      "rm": "SS",
      "1234": 420090075
    },
    {
      "1": 8724,
      "roma": "TUORO SUL TRASIMENO",
      "rm": "PG",
      "1234": 410054055
    },
    {
      "1": 8725,
      "roma": "TREMEZZINA",
      "rm": "CO",
      "1234": 403013252
    },
    {
      "1": 8726,
      "roma": "TURANIA",
      "rm": "RI",
      "1234": 412057071
    },
    {
      "1": 8727,
      "roma": "TURANO LODIGIANO",
      "rm": "LO",
      "1234": 403098058
    },
    {
      "1": 8728,
      "roma": "TRONTANO",
      "rm": "VB",
      "1234": 401103068
    },
    {
      "1": 8729,
      "roma": "TRONZANO LAGO MAGGIORE",
      "rm": "VA",
      "1234": 403012129
    },
    {
      "1": 8730,
      "roma": "TRONZANO VERCELLESE",
      "rm": "VC",
      "1234": 401002150
    },
    {
      "1": 8731,
      "roma": "TURI",
      "rm": "BA",
      "1234": 416072047
    },
    {
      "1": 8732,
      "roma": "TURRI",
      "rm": "CA",
      "1234": 420092087
    },
    {
      "1": 8733,
      "roma": "TURRIACO",
      "rm": "GO",
      "1234": 406031024
    },
    {
      "1": 8734,
      "roma": "UMBERTIDE",
      "rm": "PG",
      "1234": 410054056
    },
    {
      "1": 8735,
      "roma": "UMBRIATICO",
      "rm": "KR",
      "1234": 418101026
    },
    {
      "1": 8736,
      "roma": "UBIALE CLANEZZO",
      "rm": "BG",
      "1234": 403016221
    },
    {
      "1": 8737,
      "roma": "UBOLDO",
      "rm": "VA",
      "1234": 403012130
    },
    {
      "1": 8738,
      "roma": "UCRIA",
      "rm": "ME",
      "1234": 419083102
    },
    {
      "1": 8739,
      "roma": "UDINE",
      "rm": "UD",
      "1234": 406030129
    },
    {
      "1": 8740,
      "roma": "VALVASONE ARZENE",
      "rm": "PN",
      "1234": 406093053
    },
    {
      "1": 8741,
      "roma": "UGENTO",
      "rm": "LE",
      "1234": 416075090
    },
    {
      "1": 8742,
      "roma": "UGGIANO LA CHIESA",
      "rm": "LE",
      "1234": 416075091
    },
    {
      "1": 8743,
      "roma": "TURATE",
      "rm": "CO",
      "1234": 403013227
    },
    {
      "1": 8744,
      "roma": "TURBIGO",
      "rm": "MI",
      "1234": 403015226
    },
    {
      "1": 8745,
      "roma": "ULA' TIRSO",
      "rm": "OR",
      "1234": 420095068
    },
    {
      "1": 8746,
      "roma": "ULASSAI",
      "rm": "NU",
      "1234": 420091098
    },
    {
      "1": 8747,
      "roma": "ULTIMO",
      "rm": "BZ",
      "1234": 404021104
    },
    {
      "1": 8748,
      "roma": "USELLUS",
      "rm": "OR",
      "1234": 420095070
    },
    {
      "1": 8749,
      "roma": "USINI",
      "rm": "SS",
      "1234": 420090077
    },
    {
      "1": 8750,
      "roma": "URAGO D'OGLIO",
      "rm": "BS",
      "1234": 403017192
    },
    {
      "1": 8751,
      "roma": "URAS",
      "rm": "OR",
      "1234": 420095069
    },
    {
      "1": 8752,
      "roma": "URBANA",
      "rm": "PD",
      "1234": 405028095
    },
    {
      "1": 8753,
      "roma": "URBANIA",
      "rm": "PS",
      "1234": 411041066
    },
    {
      "1": 8754,
      "roma": "URBE",
      "rm": "SV",
      "1234": 407009063
    },
    {
      "1": 8755,
      "roma": "URBINO",
      "rm": "PS",
      "1234": 411041067
    },
    {
      "1": 8756,
      "roma": "URBISAGLIA",
      "rm": "MC",
      "1234": 411043055
    },
    {
      "1": 8757,
      "roma": "URGNANO",
      "rm": "BG",
      "1234": 403016222
    },
    {
      "1": 8758,
      "roma": "VALLELAGHI",
      "rm": "TN",
      "1234": 404022248
    },
    {
      "1": 8759,
      "roma": "UGGIATE-TREVANO",
      "rm": "CO",
      "1234": 403013228
    },
    {
      "1": 8760,
      "roma": "URURI",
      "rm": "CB",
      "1234": 414070083
    },
    {
      "1": 8761,
      "roma": "URZULEI",
      "rm": "NU",
      "1234": 420091099
    },
    {
      "1": 8762,
      "roma": "USCIO",
      "rm": "GE",
      "1234": 407010064
    },
    {
      "1": 8763,
      "roma": "VAGLIO BASILICATA",
      "rm": "PZ",
      "1234": 417076094
    },
    {
      "1": 8764,
      "roma": "VAGLIO SERRA",
      "rm": "AT",
      "1234": 401005111
    },
    {
      "1": 8765,
      "roma": "USSANA",
      "rm": "CA",
      "1234": 420092088
    },
    {
      "1": 8766,
      "roma": "USSARAMANNA",
      "rm": "CA",
      "1234": 420092089
    },
    {
      "1": 8767,
      "roma": "USSASSAI",
      "rm": "NU",
      "1234": 420091100
    },
    {
      "1": 8768,
      "roma": "USSEAUX",
      "rm": "TO",
      "1234": 401001281
    },
    {
      "1": 8769,
      "roma": "USSEGLIO",
      "rm": "TO",
      "1234": 401001282
    },
    {
      "1": 8770,
      "roma": "USSITA",
      "rm": "MC",
      "1234": 411043056
    },
    {
      "1": 8771,
      "roma": "USTICA",
      "rm": "PA",
      "1234": 419082075
    },
    {
      "1": 8772,
      "roma": "UTA",
      "rm": "CA",
      "1234": 420092090
    },
    {
      "1": 8773,
      "roma": "UZZANO",
      "rm": "PT",
      "1234": 409047021
    },
    {
      "1": 8774,
      "roma": "VACCARIZZO ALBANESE",
      "rm": "CS",
      "1234": 418078152
    },
    {
      "1": 8775,
      "roma": "URI",
      "rm": "SS",
      "1234": 420090076
    },
    {
      "1": 8776,
      "roma": "USMATE VELATE",
      "rm": "MB",
      "1234": 403108044
    },
    {
      "1": 8777,
      "roma": "VACRI",
      "rm": "CH",
      "1234": 413069098
    },
    {
      "1": 8778,
      "roma": "VADENA",
      "rm": "BZ",
      "1234": 404021105
    },
    {
      "1": 8779,
      "roma": "VADO LIGURE",
      "rm": "SV",
      "1234": 407009064
    },
    {
      "1": 8780,
      "roma": "VAGLI SOTTO",
      "rm": "LU",
      "1234": 409046031
    },
    {
      "1": 8781,
      "roma": "VAGLIA",
      "rm": "FI",
      "1234": 409048046
    },
    {
      "1": 8782,
      "roma": "VALBRONA",
      "rm": "CO",
      "1234": 403013229
    },
    {
      "1": 8783,
      "roma": "VAIANO",
      "rm": "PO",
      "1234": 409100006
    },
    {
      "1": 8784,
      "roma": "VAIANO CREMASCO",
      "rm": "CR",
      "1234": 403019111
    },
    {
      "1": 8785,
      "roma": "VAIE",
      "rm": "TO",
      "1234": 401001283
    },
    {
      "1": 8786,
      "roma": "VAILATE",
      "rm": "CR",
      "1234": 403019112
    },
    {
      "1": 8787,
      "roma": "VAIRANO PATENORA",
      "rm": "CE",
      "1234": 415061095
    },
    {
      "1": 8788,
      "roma": "VAJONT",
      "rm": "PN",
      "1234": 406093052
    },
    {
      "1": 8789,
      "roma": "VAL DELLA TORRE",
      "rm": "TO",
      "1234": 401001284
    },
    {
      "1": 8790,
      "roma": "VAL DI NIZZA",
      "rm": "PV",
      "1234": 403018166
    },
    {
      "1": 8791,
      "roma": "VAL DI VIZZE",
      "rm": "BZ",
      "1234": 404021107
    },
    {
      "1": 8792,
      "roma": "VACONE",
      "rm": "RI",
      "1234": 412057072
    },
    {
      "1": 8793,
      "roma": "VAL MASINO",
      "rm": "SO",
      "1234": 403014074
    },
    {
      "1": 8794,
      "roma": "VAL REZZO",
      "rm": "CO",
      "1234": 403013233
    },
    {
      "1": 8795,
      "roma": "VAL BREMBILLA",
      "rm": "BG",
      "1234": 403016253
    },
    {
      "1": 8796,
      "roma": "VALBONDIONE",
      "rm": "BG",
      "1234": 403016223
    },
    {
      "1": 8797,
      "roma": "VALBREMBO",
      "rm": "BG",
      "1234": 403016224
    },
    {
      "1": 8798,
      "roma": "VALBREVENNA",
      "rm": "GE",
      "1234": 407010065
    },
    {
      "1": 8799,
      "roma": "VALFENERA",
      "rm": "AT",
      "1234": 401005112
    },
    {
      "1": 8800,
      "roma": "VALDAGNO",
      "rm": "VI",
      "1234": 405024111
    },
    {
      "1": 8801,
      "roma": "VALDAORA",
      "rm": "BZ",
      "1234": 404021106
    },
    {
      "1": 8802,
      "roma": "VALDASTICO",
      "rm": "VI",
      "1234": 405024112
    },
    {
      "1": 8803,
      "roma": "VALDENGO",
      "rm": "BI",
      "1234": 401096071
    }
   ]