import React from 'react';
import styled from 'styled-components';
import { AppContext } from '../state/AppProvider';
import Task from './Task';
import { NavBar, CButton, CenteredP } from './generics/generics';

export const AnchorBack = styled.span`
  display: block;
  font-family: 'Pacifico', cursive;
  font-size: 20px;
  margin-bottom: 1em;
  cursor: pointer;
`;

export const TaskList = styled.div`
    margin: 2em auto;
`;


function goBack(setPage) {
  setPage('Projects')
}

export default function () {
    return(
      <AppContext.Consumer>
          {({currentTasks, anchorBack, setPage}) => (
            currentTasks ?
            (
              <TaskList>
                <NavBar>
                  <AnchorBack onClick={() => goBack(setPage)}>
                    {anchorBack ? anchorBack : 'Back'}
                  </AnchorBack>
                </NavBar>
                {
                  currentTasks.length > 0 ? currentTasks.map((ct, index) => {
                    return <Task task={ct} key={index}/>
                })
                :
                 <CenteredP>
                   Al momento non ci sono ancora attività legate a questo progetto
                    <br/><br/><br/>
                    <CButton>Be the first!</CButton>
                 </CenteredP>
                 }
              </TaskList>
            ) : window.location.href = '/'
          )}
      </AppContext.Consumer>
    )
}
