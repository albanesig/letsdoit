import styled from 'styled-components';
import { mainGreen } from '../../style/styles'

export const NavBar = styled.div`
    display: grid;
    grid-template-columns: 70% 10% 10% 10%;
`;

export const ContentBox = styled.div`
  margin-bottom: 1em;
  padding: 0.1em 1em 1em;
  border: 1px solid ${mainGreen};
  border-radius: 15px;
`;

export const CenteredP = styled.p`
  text-align: center;
`;

export const CButton = styled.button`
  background-color: white;
  border: 1px solid dodgerblue;
  color: dodgerblue;
  padding: 10px;
  font-size: 16px;
  border-radius: 5px;
  outline: 0;
  &:hover {
    background-color: dodgerblue;
    color: white;
  };
`;

export const CButtonSQUARE = styled.button`
  background-color: white;
  border: 2px solid dodgerblue;
  color: dodgerblue;
  padding: 10px;
  font-size: 16px;
  outline: 0;
  &:hover {
    background-color: dodgerblue;
    color: white;
  };
`;
