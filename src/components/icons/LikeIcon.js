import React from 'react';
import styled from 'styled-components';

const LikeIcon = styled.i`
  color: #20c997;
  margin-top: 0.5em;
`;

export default function () {
  return <LikeIcon className="far fa-heart"/>
}
