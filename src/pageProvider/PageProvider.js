import React from 'react';
import { AppContext } from '../state/AppProvider';
import WelcomePage from '../components/welcomePage/WelcomePage';
import ProjectList from '../components/ProjectList';
import TaskList from '../components/TaskList'

export default function (){

    let content = '';
    return(
        <AppContext.Consumer>
            {({page}) => {
                if(page === 'WelcomePage'){
                    content =  <WelcomePage/>
                }
                if(page === 'Projects'){
                    content = <ProjectList/>
                }
                if(page === 'TaskList'){
                    content  = <TaskList/>
                }
                return content;
            }}
        </AppContext.Consumer>
    )
}
