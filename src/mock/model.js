
export const projectList = [
  {
    projectId: 1,
    title: 'Plastic collection',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    tasks: []
  },
  {
    projectId: 2,
    title: 'Save the oceans',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    tasks: []
  },
  {
    projectId: 3,
    title: 'Protect Rain forest',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    tasks: []
  }
];

export const tasks = [
  {
    taskId: 1,
    projectId: 1,
    title: 'Remove plastic from Sardinia coasts',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    startDate: '10/06/2019',
    needs: [],
    sponsor: [],
    likes: 0
  },
  {
    taskId: 2,
    projectId: 1,
    title: 'Fiumicino beach plastic collection!',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    startDate: '10/06/2019',
    needs: [],
    sponsor: [],
    likes: 0
  },
  {
    taskId: 3,
    projectId: 1,
    title: 'Plastic assault! (Venezia, Canali di Venezia)',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    startDate: '10/06/2019',
    needs: [],
    sponsor: [],
    likes: 0
  },
  {
    taskId: 4,
    projectId: 2,
    title: 'Protect turtles in Sicily',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    startDate: '10/06/2019',
    needs: [],
    sponsor: [],
    likes: 0
  },
  {
    taskId: 5,
    projectId: 2,
    title: '',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    startDate: '10/06/2019',
    needs: [],
    sponsor: [],
    likes: 0
  },
  {
    taskId: 6,
    projectId: 2,
    title: 'Remove plastic from Sardinia coasts',
    description: 'Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
    startDate: '10/06/2019',
    needs: [],
    sponsor: [],
    likes: 0
  },
];
