import React from 'react';
import styled from 'styled-components';
import { AppContext } from '../../state/AppProvider';
import {Button} from "reactstrap";
// import _ from 'lodash'


export const SearchInput = styled.input`
  text-align: center;
`;

/*
const handleFilter = _.debounce((inputValue, countries, setFilteredCountries) => {
  let filteredCountries = [];
  countries.map(c => {
   return (c.roma).includes(inputValue) ?
   filteredCountries.push(c) : null;
  });
  setFilteredCountries(filteredCountries)
}, 500);


function filterCountries(e, countries, setFilteredCountries){
  //FIXme, veloce formattazione del testo, i comuni sono tutti uppercase
  let inputValue = ((e.target.value).toUpperCase()).trim();
  console.log(inputValue);
  if(!inputValue){
    setFilteredCountries(null);
    return
  }
  handleFilter(inputValue, countries, setFilteredCountries);
}
  onKeyUp={(event) => filterCountries(event, countries, setFilteredCountries)}
*/

function handleChange(event, setCountry) {
  let inputValue = event.target.value;
  setCountry(inputValue)
}

export default function () {
  return(
    <AppContext.Consumer>
      {({setCountry, findEvents}) => (
        <div>
          <SearchInput
            placeholder='AREA GEOGRAFICA'
            onChange={(event) => handleChange(event, setCountry)}
          />
          <Button onClick={() => findEvents()}>FIND</Button>
        </div>
      )}
    </AppContext.Consumer>
  )
};
