import React from 'react';
import styled from 'styled-components';
import Project from "./Project";
import {AppContext} from "../state/AppProvider";


export const ProjectList = styled.div`
`;

export default function () {
  return(
    <AppContext.Consumer>
      {({projectList}) => (
        <ProjectList>
          {
            projectList ? projectList.map((pg, index) => {
              return <Project project={pg} key={index}/>
            }) : 'Loading projects...'
          }
        </ProjectList>
      )}
    </AppContext.Consumer>
  )
}
