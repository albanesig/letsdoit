import React from 'react';
import {projectList, tasks as taskList} from "../mock/model";

export const AppContext = React.createContext();

const tasks = taskList;

export class AppProvider extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      page: 'WelcomePage',
      setPage: this.setPage,
      fetchProjectTasks: this.fetchProjectTasks,
      setCountry: this.setCountry,
      findEvents: this.findEvents
    }
  }

  componentDidMount = () => {
    this.fetchProjects();
  };

  fetchProjects = () => {
    this.setState({projectList})
  };

  setPage = (page) => this.setState({page: page});

  fetchProjectTasks = (project) => {
    let currentTasks;
    currentTasks = tasks.filter(t => {
      return t.projectId === project.projectId
    });
    let anchorBack = project.title;
    this.setState({
      currentTasks,
      anchorBack,
      page: 'TaskList'
    })
  };

  setCountry = (inputValue) => {
    let country = {name : inputValue};
    this.setState({country})
  };

  findEvents = () => {
    //Todo ex. find events by country
    this.setPage('Projects')
  };

  render(){

    return(
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    )
  }
}
