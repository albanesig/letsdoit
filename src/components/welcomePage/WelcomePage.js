import React from 'react';
import styled from 'styled-components';
import SearchArea from '../searchArea/SearchArea';

export const WelcomePage = styled.div`
  text-align: center;
`;

export const ButtonsLayout = styled.div`
  margin: 2em auto;
`;

export default function () {
    return(
    <WelcomePage>
      Welcome to Clean Please!
      <ButtonsLayout>
        <SearchArea/>
      </ButtonsLayout>
      <p>
        Quante volte ci siamo trovati di fronte ad una situazione che ci ha fatto schifo. E altrettante volte abbiamo pensato 'caspita se solo qualcuno facesse qualcosa...'
        Abbiamo deciso di rispondere al silenzioso appello del nostro pianeta, come insegna il Mahatma Gandhi: "Sii il cambiamento che vuoi vedere nel mondo". (Blocco con citazione)
        CleanPls nasce con lo scopo di condividere e supportare quelle piccole grandi iniziative che vogliono fare qualcosa di attivo. Con CleanPls possiamo unire le nostre forze, essere quelle gocce che tutte insieme formano un oceano (pulito!)
        Ognuno di noi può fare qualcosa, partecipare ad un'attività già organizzata, sostenerla, oppure proporre una nuova idea!
      </p>
    </WelcomePage>
    )
}