import React from 'react';
import styled from 'styled-components';
import ShareIcon from './icons/ShareIcon';
import LikeIcon from './icons/LikeIcon';
import { mainGreen } from '../style/styles';
import { ContentBox } from './generics/generics';


export const Title = styled.h2`
  color: ${mainGreen};
`;

export const Paragraph = styled.p`
  padding: 1em;
`;

export const LayoutIcons = styled.div`
  display: grid;
  grid-gap: 15px;
  grid-template-columns: 20px 20px 80px;
`;


export default function ({task}) {
    return(
      <ContentBox>
        <Title>{task.title}</Title>
        <Paragraph>
        {task.description}
        </Paragraph>
        <LayoutIcons>
          <LikeIcon/>
          <ShareIcon/>
        </LayoutIcons>
      </ContentBox>
    )
}
