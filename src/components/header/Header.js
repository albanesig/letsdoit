import React from 'react';
import styled from 'styled-components';
import { AppContext } from '../../state/AppProvider';

const Header = styled.div`
  padding-bottom: 20px;
  text-align: center;
`;

const HeaderTitle = styled.h1`
  color: dodgerblue;
  cursor: pointer;
`;

function setWelcomePage(setPage){
  setPage({page: 'WelcomePage'})
}

export default function () {
  return(
    <AppContext.Consumer>
      {({setPage}) => (
      <Header>
        <HeaderTitle onClick={() => setWelcomePage(setPage)}>Lets do it!</HeaderTitle>
      </Header>
      )}
    </AppContext.Consumer>
  )
}
